
function main (flagData, tools)  
	flagData.IncrementGlobalFlag("killed_raptors", 1)
	if (flagData.ReadGlobalFlag("killed_raptors") == 10) then
		tools.LogText("From the foliage you hear a raptor chirp that is answered by a distant cry, as if a cry for help has been answered or the presence of a threat has been met with escalation, you can sense that this hunt is a game with different rules going forwards now")
	end
	return true;
end  