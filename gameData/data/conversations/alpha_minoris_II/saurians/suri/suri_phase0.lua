function main(flags, player)
	--check for absence of scales

	if (player:HasPart("scales")) then
		return 0;
	end
	
	return 1;
end