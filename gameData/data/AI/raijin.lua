function combat(controllable, sensor,hostile)

	controllable.SetValue(0,1)
	controllable.SetValue(1,hostile.GetPosition().x)
	controllable.SetValue(2,hostile.GetPosition().y)	
	distance = hostile.GetDistance(controllable:GetPosition())
	if (distance < 4) then
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			if (sensor.PathAwayFromActor(controllable,hostile,5)) then	
				controllable.FollowPath()
			end		
		end
	else 
		if (distance < 6) then
		controllable.DoMove(hostile,0)		
		controllable.RemovePath();
		else
			if (controllable.HasPath()) then
				controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,hostile)) then	
					controllable.FollowPath()
				end		
			end
		end
	end
end

function pursue(controllable,sensor)
	sensor.PathToPosition(controllable,controllable.GetValue(1),controllable.GetValue(2))
	controllable.SetValue(0,0)
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else
			if (controllable.GetValue(0)==1) then
				pursue(controllable, sensor)
			else
				direction=math.random(0,8)
				controllable.Move(direction);				
			end		
		end	
	end
	

	return true;
end