
function victimize(controllable,sensor,victim)
if (victim.GetDistance(controllable:GetPosition())<2) then
		controllable.StartObserverVore("test_observer_vore",victim)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,victim)) then
	
				controllable.FollowPath()
			end
		end
	end
end

function main (controllable, sensor)  
	victim = sensor.GetActor(controllable,"test dummy",8, true, false);
	
	if not (victim== nil) and not (controllable.HasObserverVore()) then
		victimize(controllable,sensor, victim)
	else
		controllable.Delay(10)
	end
	
	return true;
end