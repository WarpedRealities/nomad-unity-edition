function combat(controllable, sensor,hostile)

	controllable.SetValue(0,1)
	controllable.SetValue(1,hostile.GetPosition().x)
	controllable.SetValue(2,hostile.GetPosition().y)	
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		if (hostile.HasStatus(20)) then
			if (hostile.GetBindOrigin() == controllable.GetUID()) then
				controllable.SetValue(3,1)
				controllable.DoMove(hostile,1)		
			else
				direction=math.random(0,8)
				controllable.Move(direction);	
			end
		else 
			if (controllable.GetValue(3) == 0) then
				controllable.DoMove(hostile,0)
			else
				controllable.SetValue(3,0)
				controllable.Delay(10)
			end
		end
	else
		if (hostile.HasStatus(20)) then
			direction=math.random(0,8)
			controllable.Move(direction);	
		else
			if (controllable.HasPath()) then
				controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,hostile)) then	
					controllable.FollowPath()
				end		
			end	
		end
	end
end

function pursue(controllable,sensor)
	sensor.PathToPosition(controllable,controllable.GetValue(1),controllable.GetValue(2))
	controllable.SetValue(0,0)
end


function victimize(hostile, victim, controllable, sensor) 
	if (controllable.HasObserverVore()) then
		return false;
	end
	if (victim==nil) then
		return false;
	end
	if (hostile==nil) and not (victim==nil) then
		controllable.StartObserverVore("lamia_lamia",victim);
		return true;
	end
	if (hostile.GetDistance(controllable:GetPosition())>3) then
		controllable.StartObserverVore("lamia_lamia",victim);
		return true;		
	end
		
	return false;

end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	victim = sensor:GetActor(controllable,"wild lamia",2, true, false);	
	if not victimize(hostile,victim,controllable, sensor) then
		if not (hostile== nil) and not (controllable.GetPeace()) then
			combat(controllable,sensor, hostile)
		else
			if (controllable.HasPath()) then
				controllable.FollowPath()		
			else
				if (controllable.GetValue(0)==1) then
					pursue(controllable, sensor)
				else
					direction=math.random(0,8)
					controllable.Move(direction);	
				end
			end	
		end
	end	

	return true;
end  
