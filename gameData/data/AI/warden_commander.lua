function combat(controllable, sensor,hostile)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,1)
		
		controllable.RemovePath();
	else			
		controllable.DoMove(hostile,0)
	end
end

function stationKeeping(controllable,sensor)
if (controllable:getFlag("office")==1)  then
		x=7
		y=22
		pos=controllable:getPosition()
		if not (pos.x==x) or not (pos.y==y) then	
			if controllable:HasPath() then
				controllable:FollowPath()
			else
				sensor:PathToPosition(controllable,x,y)
			end
		end
	end

end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	player=sensor:GetPlayer(controllable,10,false)
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
		
	else
		if not (player==nil) and controllable.GetFlag("talked") == 0 then
			controllable.StartConversation()
		else	
			stationKeeping(controllable, sensor)
		end
	end
	

	return true;
end  