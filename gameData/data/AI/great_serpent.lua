function combat(controllable, sensor,hostile)
	rand=math.random(0,8)
	if (rand==1 and controllable.GetActor().GetHealth()<0.5) then
		controllable.SetValue(0,15)		
		if (sensor.PathAwayFromActor(controllable,hostile,5)) then		
			controllable.FollowPath()
		end		
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else
			if (rand==1) then
				if (sensor.PathToActor(controllable,hostile)) then	
					controllable.FollowPath()
				end			
			else
				controllable.DoMove(hostile,0)	
			end
		end
	end
end

function run(controllable, sensor, hostile)
	escapeValue = controllable.GetValue(0)	
	if (controllable.HasPath()) then
			controllable.FollowPath()
	else
		if (not (hostile== nil)) and sensor.PathAwayFromActor(controllable,hostile,5) then		
			controllable.FollowPath()
		end
	end
	escapeValue = escapeValue -1
	controllable.SetValue(0,escapeValue)	
end

function idle(controllable, sensor)
	health = controllable.GetActor().GetHealth()
	if (health<1) then
		controllable.DoSelfMove(1)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else
			direction=math.random(0,8)
			controllable.Move(direction);				
		end		
	end
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	escape = controllable.GetValue(0)
	if (escape > 0) then
		run(controllable,sensor,hostile)
	else
		if not (hostile== nil) and not (controllable.GetPeace()) then
			combat(controllable,sensor, hostile)
		else
			idle(controllable,sensor)

		end	
	end
	return true;
end  

function start( controllable, sensor)
	controllable.SetValue(0,0)	
	return true;
end