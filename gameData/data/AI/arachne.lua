function combat(controllable, sensor,hostile)

	controllable.SetValue(0,1)
	controllable.SetValue(1,hostile.GetPosition().x)
	controllable.SetValue(2,hostile.GetPosition().y)		
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		diceroll = math.random(0,4)
		if (diceroll==1 and not hostile.HasStatus(11)) then
			controllable.DoMove(hostile,1)		
			controllable.RemovePath();	
		else
			if (controllable.HasPath()) then
				controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,hostile)) then	
					controllable.FollowPath()
					controllable.FastMove()
				end		
			end	
		
		end

	end
end

function pursue(controllable,sensor)
	sensor.PathToPosition(controllable,controllable.GetValue(1),controllable.GetValue(2))
	controllable.SetValue(0,0)
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else
			if (controllable.GetValue(0)==1) then
				pursue(controllable, sensor)
			else
				direction=math.random(0,8)
				controllable.Move(direction);				
			end		
		end	
	end
	

	return true;
end  
