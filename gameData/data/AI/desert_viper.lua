function combat(controllable, sensor,hostile)

	if (hostile.GetDistance(controllable:GetPosition())<2) then

		if (controllable.GetActor().HasStatus(22)) then
	
			controllable.GetActor():RemoveStatus(22)
		else
	
			controllable.DoMove(hostile,0)		
			controllable.RemovePath();				
		end
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then	
				controllable.FollowPath()
				controllable.FastMove()
			end
		end
	end
end

function peace(controllable, sensor)
		if (controllable.GetActor().HasStatus(22) == false) then
			controllable.DoSelfMove(1)		
		else
			controllable.Delay(5)
		end
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		peace(controllable,sensor)
	end

	return true;
end  
