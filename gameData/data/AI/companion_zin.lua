function combat(controllable, sensor,hostile)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		if (controllable.GetRank() == 1) then
			controllable.DoMove(hostile,1)		
		else
			controllable.DoMove(hostile,0)		
		end		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then	
				controllable.FollowPath()
				controllable.FastMove()
			end		
		end
	end
end

function Follow(controllable, sensor)
	player=sensor:GetPlayer(controllable,32,true)
	if not (player== nil) then
		if (player.GetDistance(controllable:GetPosition())>2) then
			if (sensor.PathToActor(controllable,player)) then	
				controllable.FollowPath()

			end		
		end		
	end
end

function main(controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else
			if (controllable.IsCompanion()) then
				Follow(controllable,sensor)
			else
				controllable.Delay(10)			
			end

		end	
	end
	

	return true;
end  
