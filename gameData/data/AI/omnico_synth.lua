function combat(controllable, sensor,hostile)
	controllable.SetValue(0,1)
	controllable.SetValue(2,hostile.GetPosition().x)
	controllable.SetValue(3,hostile.GetPosition().y)	
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then
	
				controllable.FollowPath()
			end
		end
	end
end

function pursue(controllable,sensor)
	sensor.PathToPosition(controllable,controllable.GetValue(2),controllable.GetValue(3))
	controllable.SetValue(0,0)
end

function robotMove(controllable,sensor)
		
	if (controllable.HasPath()) then
		controllable.FollowPath()		
	else
		if (controllable.GetValue(0)==1) then
			pursue(controllable, sensor)
		else
			direction=math.random(0,8)
			controllable.Move(direction);				
		end		
	end	
end

function voiceGreet(controllable,sensor)
	controllable:SetValue(1,1)
	a=math.random(0,8)
	if (a==0) then
		sensor:DrawText("synth:hello ma'am, let me help you")
	end
	if (a==1) then
		sensor:DrawText("synth:ma'am, please remain calm, this is a product demonstration")
	end
	if (a==2) then
		sensor:DrawText("synth:that's the omnico way")
	end
	if (a==3) then
		sensor:DrawText("synth:if you like your experience please consider making a purchase")
	end
	if (a==4) then
		sensor:DrawText("synth:choose OCC synths for your predation needs")
	end
	if (a==5) then
		sensor:DrawText("synth:a new customer, please hold still for the demonstration")
	end
	if (a==6) then
		sensor:DrawText("synth:I hope you find my insides to your liking ma'am")
	end
	if (a==7) then
		sensor:DrawText("synth:you look like you need some time inside me ma'am")
	end
end

function voiceQuery(controllable,sensor)
	a=math.random(0,6)
	if (a==0) then
		sensor:DrawText("synth:Where did you go?")
	end
	if (a==1) then
		sensor:DrawText("synth:I could of sworn I saw a customer")
	end
	if (a==2) then
		sensor:DrawText("synth:Here at Omnico we value your custom")
	end
	if (a==3) then
		sensor:DrawText("synth:Is anyone there?")
	end
	if (a==4) then
		sensor:DrawText("synth:Ma'am please don't hide from me")
	end
	if (a==5) then
		sensor:DrawText("synth:Ma'am?")
	end
end

function victimize(controllable,sensor)
	victim = sensor:GetActor(controllable,"Synth",8, true, false);
	if not (victim==nil) then
		if (victim.GetDistance(controllable:GetPosition())<2) then
		controllable.StartObserverVore("synthoid_creation",victim)
		
		controllable.RemovePath();
		else
			if (controllable.HasPath()) then

				controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,victim)) then		
					controllable.FollowPath()
				end
			end
		end		
	
		return true;
	end
	return false;
end

function main (controllable, sensor)  
	
	if (sensor:ReadGlobalFlag("OMNICO_IIA_MALWARE") == 1) then
		if (victimize(controllable,sensor) == true) then
			return true;
		end	
	end
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
		if (controllable:GetValue(1)==0) then
			voiceGreet(controllable,sensor)
		end
	else
		robotMove(controllable,sensor)
		if (controllable:GetValue(1) > 0) then
			controllable:SetValue(1, controllable:GetValue(1)+1)
			if (controllable:GetValue(1)== 10) then
				voiceQuery(controllable,sensor)			
			end
		end
	end
	

	return true;
end