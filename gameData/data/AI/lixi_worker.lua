function combat(controllable, sensor,hostile)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)		
		controllable.RemovePath();
	else	
		if (controllable.HasPath()) then
			controllable.FollowPath()	
		else
			if (sensor.PathToActor(controllable,hostile)) then
	
				controllable.FollowPath()
			end
		end	
	end
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			direction=math.random(0,8)
			controllable.Move(direction);		
		end
	end

	return true;
end