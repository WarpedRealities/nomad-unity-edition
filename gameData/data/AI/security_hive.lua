

function main (controllable, sensor)  	
	threshold = sensor.ReadGlobal(0)
	sensor.Log("threshold is"..threshold)
	diceroll = math.random(0,20)
	if (diceroll>threshold) then
		controllable.DoSelfMove(0)
	else
		controllable.Delay(100)	
		threshold = threshold - 1
		sensor.SetGlobal(0,threshold)	
	end

end  

function start( controllable, sensor)
	sensor.SetGlobal(0,0)		
	return true;
end