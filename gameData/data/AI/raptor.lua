function combat(controllable, sensor,hostile)
	value=sensor.ReadGlobal(0)
	if (value>5) then
		value=value+2	
	else
		value=value+1	
	end
	sensor.SetGlobal(0,value)
	controllable.SetValue(0,1)
	controllable.SetValue(1,hostile.GetPosition().x)
	controllable.SetValue(2,hostile.GetPosition().y)		
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			if (value>5) then
				if (sensor.PathToActor(controllable,hostile)) then	
					controllable.FollowPath()
					if (hostile.GetDistance(controllable:GetPosition())>4) then
						controllable.FastMove()				
					end
				end		
			else
				if (sensor.PathAwayFromActor(controllable,hostile,5)) then		
					controllable.FollowPath()
				end			
			end

		end
	end
end

function pursue(controllable,sensor)
	sensor.PathToPosition(controllable,controllable.GetValue(1),controllable.GetValue(2))
	controllable.SetValue(0,0)
end


function hunt(controllable,sensor)
	sensor.PathToPosition(controllable, sensor.ReadGlobal(2),sensor.ReadGlobal(3))

end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else
			if (sensor.ReadGlobal(0) > 5) then
				if (controllable.GetValue(0)==1) then
					pursue(controllable, sensor)	
				end
				if (sensor.ReadGlobal(1)==1) then 
					hunt(controllable, sensor)
				end

			else
				direction=math.random(0,8)
				controllable.Move(direction);				
			end		
		end	
	end
	

	return true;
end  

function tick(sensor)
	if (sensor.ReadGlobal(0) > 0) then
		value=sensor.ReadGlobal(0)
		value=value-1
		sensor.SetGlobal(0,value)
	end
	return true;
end