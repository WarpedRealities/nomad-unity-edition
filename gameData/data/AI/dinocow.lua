function combat(controllable, sensor,hostile)
	value = controllable.GetValue(0,1)
	if (hostile.GetDistance(controllable:GetPosition())<6 or controllable.GetActor().GetHealth()<1) then
		if (hostile.GetDistance(controllable:GetPosition())<2 and value < 15) then
			value = 15;
		end
		value= value+1
		if (value==10 or value==15) then
			sensor.DrawText("The Arkosaur bellows and stomps in agitation")
		end
	else
		if (value > 0) then
			value= value-1
			if (value==0) then
				sensor.DrawText("The Arkosaur settles down and resumes mastication")
			end
		end
	end
	controllable.SetValue(0,value)
	if (value>20) then
		if (hostile.GetDistance(controllable:GetPosition())<2) then
			controllable.DoMove(hostile,0)
			
			controllable.RemovePath();
		else
			if (controllable.HasPath()) then
				controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,hostile)) then	
					controllable.FollowPath()
					controllable.FastMove()
				end		
			end
		end	
	else
		direction=math.random(0,16)
		if (direction<8) then
			controllable.Move(direction);				
		else
			controllable.Delay(10)
		end
	end

end

function deaggro(controllable,sensor)
	value = controllable.GetValue(0,1)
	if (value>0) then
		value=value-1
		controllable.SetValue(0,value)		
	end
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else

			direction=math.random(0,16)
			if (direction<8) then
				controllable.Move(direction);				
			else
				deaggro(controllable,sensor)
				controllable.Delay(10)
			end
		end	
	end
	

	return true;
end  

function start( controllable, sensor)
	controllable.SetValue(0,0)	
	return true;
end