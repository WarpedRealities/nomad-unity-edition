function combat(controllable, sensor,hostile)
	if (hostile.HasStatus(50)) then
		if (controllable.HasPath()) then
			controllable.FollowPath()	
		else
			if (sensor.PathAwayFromActor(controllable,hostile,5)) then		
				controllable.FollowPath()
				controllable.FastMove()
			end			
		end			
	else
		if (hostile.GetDistance(controllable:GetPosition())<2) then
			if (controllable.GetActor().HasStatus(22) == true) then

				controllable.GetActor():RemoveStatus(22)	
			else

				controllable.DoMove(hostile,0)		
				controllable.RemovePath();									
			end
		else	
			if (controllable.HasPath()) then
				controllable.FollowPath()	
			else
				if (sensor.PathToActor(controllable,hostile)) then
		
					controllable.FollowPath()
				end
			end	
		end		
	end
	
end

function peace(controllable, sensor)
		if (controllable.GetActor().HasStatus(22) == false) then
			controllable.DoSelfMove(1)
			
		else
			direction=math.random(0,8)
			controllable.Move(direction);	
		end
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			peace(controllable,sensor)	
		end
	end

	return true;
end