function combat(controllable, sensor,hostile)
	sensor.SetGlobal(0,15)
	controllable.SetValue(0,1)
	controllable.SetValue(2,hostile.GetPosition().x)
	controllable.SetValue(3,hostile.GetPosition().y)		
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		if (controllable.GetActor().HasStatus(22)) then	
			controllable.GetActor():RemoveStatus(22)
			sensor:DrawText("the raptor huntress lets out a shriek as she attacks out of nowhere")			
		else	
			if (hostile.HasStatus(11)) then
				controllable.DoMove(hostile,2)		
			else
				controllable.DoMove(hostile,0)		
			end		
			controllable.RemovePath()
			sensor.SetGlobal(1,1)
			sensor.SetGlobal(2,hostile.GetPosition().x)
			sensor.SetGlobal(3,hostile.GetPosition().y)					
		end	
	
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then	
				controllable.FollowPath()
				if (hostile.GetDistance(controllable:GetPosition())>4) then
					controllable.FastMove()				
				end
			end		
		end
	end
end

function pursue(controllable,sensor)
	sensor.PathToPosition(controllable,controllable.GetValue(1),controllable.GetValue(2))
	controllable.SetValue(0,0)
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.GetValue(0)==1) then
			pursue(controllable, sensor)
		else
			if (controllable.GetActor().HasStatus(22) == false) and 
			not (sensor.IsTileVisible(controllable:GetPosition().x, controllable:GetPosition().y)) then
				sensor.SetGlobal(1,0)				
				controllable.DoSelfMove(1)			
			end
			if (controllable.HasPath()) then
				controllable.FollowPath()		
			else	
				direction=math.random(0,8)
				controllable.Move(direction)				
			end						
		end				
	end

	return true;
end  
