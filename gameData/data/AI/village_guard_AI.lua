
function combat(controllable,sensor, hostile)
		if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			random= math.random(0,4)
			if (random<3) then
				controllable.DoMove(hostile,1)			
			else
				if (sensor.PathToActor(controllable,hostile)) then
		
					controllable.FollowPath()
				end			
			end

		end
	end
end

function guardswoman(controllable,sensor)
	player = sensor:GetPlayer(controllable,10,false)
	if not (player==nil) then
		if (player.GetDistance(controllable:GetPosition())<2) then	
			controllable.StartConversation()
		else
			if (controllable.HasPath()) then
				controllable.FollowPath()
			else		
				if (sensor.PathToActor(controllable,player)) then
					controllable.FollowPath()
				end
			end
		end
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else		
			crime=sensor:GetCrimePosition(controllable)
			if (sensor:PathTo(controllable,crime.x,crime.y,8)) then
				controllable.FollowPath()
			end
		end
	end
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor,hostile)
	else
		if (sensor:isCrimeReported(controllable)) then
			guardswoman(controllable,sensor)
		else
		direction=math.random(0,8)
		controllable.Move(direction);	
		controllable.Delay(20)		
		end	
	end
	
	return true;
end  