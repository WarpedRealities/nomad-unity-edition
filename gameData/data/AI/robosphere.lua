function combat(controllable, sensor,hostile)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		value = controllable:GetValue(0)
		value = value + 1
		if (value > 2) then
			controllable.DoMove(hostile,1)
			value=0;
		else
			controllable.DoMove(hostile,0)	
		end
		controllable:SetValue(0,value)
	else
		controllable.DoMove(hostile,0)		
	end
end

function callout(controllable,sensor)
	a=math.random(0,8)
	if (a==0) then
		sensor:DrawText("robosphere: scanning area")
	end
	if (a==1) then
		sensor:DrawText("robosphere: self check okay")
	end
	if (a==2) then
		sensor:DrawText("robosphere: no targets detected")
	end
	if (a==3) then
		sensor:DrawText("robosphere: directive, patrol, eliminate")
	end

end

function idle(controllable, sensor)

	direction=math.random(0,8)
	controllable.Move(direction);	
	a=math.random(0,8)
	if (a==1) then
		callout(controllable,sensor)
	end
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor,hostile)
	else
		idle(controllable, sensor)
	end
	

	return true;
end  