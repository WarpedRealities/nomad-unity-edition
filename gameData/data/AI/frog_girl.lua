function combat(controllable, sensor,hostile)
	distance = hostile.GetDistance(controllable:GetPosition())
	roll = math.random(0,2)
	
	if (distance<2) then
		state = controllable:GetValue(0)
		if (state == 0) then
			state = math.random(1,2)
			controllable:SetValue(0,state)
		end
		if (state == 1) then
			controllable.DoMove(hostile,0)
				
		end
		if (state == 2) then
			controllable.DoMove(hostile,1)
				
		end

		controllable.RemovePath();
	else
		if (roll == 1 and distance > 4 and distance < 7) then
			controllable.DoMove(hostile,2)	
		else
		
			if (controllable.HasPath()) then

				controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,hostile)) then
		
					controllable.FollowPath()
				end
			end				
		end

	end
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		direction=math.random(0,8)
		controllable.Move(direction);		
	end
	

	return true;
end  