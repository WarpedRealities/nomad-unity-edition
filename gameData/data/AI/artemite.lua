function combat(controllable, sensor,hostile)
	controllable.SetValue(0,1)
	controllable.DoMove(hostile,0)			
end

function idle(controllable, sensor)

	if (controllable.GetValue(0)==1) then
		if (controllable.GetValue(1)==1) then	
		sensor.PathToPosition(controllable,controllable.GetValue(2),controllable.GetValue(3))
		controllable.SetValue(1,0)
		else
			if (controllable.HasPath()) then
				controllable.FollowPath()		
			else	
				direction=math.random(0,8)
				controllable.Move(direction)				
			end					
		end		
	else
		controllable.Delay(10)						
	end
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		controllable.DoMove(hostile,0)		
	else
		idle(controllable, sensor)
	end	

	return true;
end  