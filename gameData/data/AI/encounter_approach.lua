function approach(controllable,sensor,player)
	if (player.GetDistance(controllable:GetPosition())<2) then
		controllable.StartConversation()
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,player)) then	
					controllable.FollowPath()
					controllable.FastMove()
				end		
			end
		end	

end

function main (controllable, sensor) 
	player = sensor.GetPlayer(controllable,10,false)
	if not (player==nil) and controllable.GetFlag("talked") == 0 then
		approach(controllable,sensor,player)
	else

		controllable.Delay(10)

	end
end  