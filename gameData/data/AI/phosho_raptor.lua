function combat(controllable, sensor,hostile)
	controllable.SetValue(0,1)
	controllable.SetValue(1,hostile.GetPosition().x)
	controllable.SetValue(2,hostile.GetPosition().y)
	aggression = controllable.GetValue(3)
	if (hostile.GetDistance(controllable:GetPosition())<2) then

		aggression = aggression + 1
		if (controllable.GetActor().GetHealth()<0.5) and (controllable.GetValue(4) == 0) then
			aggression = 4
			controllable.SetValue(4,1)
		end
		if (aggression < 5) then
			controllable.DoMove(hostile,0)	
		end
		if (aggression == 5) then
			controllable.DoMove(hostile,1)	
			aggression = 0
		end
		if (aggression == 4) then
			sensor.DrawText("The Phosho raptor hisses and rears up, incensed")		
		end	
		if (aggression == 3) then
			sensor.DrawText("The Phosho raptor hisses")		
		end			
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then
	
				controllable.FollowPath()
			end
		end
		if (aggression > 0) then
			aggression = aggression - 1
		end
	end
	controllable.SetValue(3,aggression)
end

function pursue(controllable,sensor)
	sensor.PathToPosition(controllable,controllable.GetValue(1),controllable.GetValue(2))
	controllable.SetValue(0,0)
end


function main (controllable, sensor)  
	distance = 5;
	if (controllable.GetActor().GetHealth()<1) then
		distance = 10;
	end
	hostile=sensor:GetHostile(controllable,distance,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.GetValue(0)==1) then
			pursue(controllable, sensor)
		else
			direction=math.random(0,8)
			controllable.Move(direction);				
		end				
	end
	

	return true;
end  