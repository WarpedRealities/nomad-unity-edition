
function combat(controllable,sensor, hostile)
		if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			random= math.random(0,4)
			if (random<3) then
				controllable.DoMove(hostile,1)			
			else
				if (sensor.PathToActor(controllable,hostile)) then
		
					controllable.FollowPath()
				end			
			end

		end
	end
end

function victimize(controllable,sensor,hostile,victim)
	if (controllable.HasObserverVore()) then
		return false;
	end
	if (victim==nil) then
		return false;
	end
	if (hostile==nil) and not (victim==nil) then
		controllable.StartObserverVore("elf_raider_saurian_soldier",victim);
		return true;
	end
	if (hostile.GetDistance(controllable:GetPosition())>3) then
		controllable.StartObserverVore("elf_raider_saurian_soldier",victim);
		return true;		
	end
		
	return false;
end

function patrol(controllable,sensor)
	if (controllable.HasPath()) then
		controllable.FollowPath()
	else
		if (sensor.PathToPosition(controllable,sensor.ReadGlobal(2),sensor.ReadGlobal(3))) then
			controllable.FollowPath()
		end
	end	
end

function main (controllable, sensor)  
	victim = sensor:GetActor(controllable,"Soldier",2, true, false);		
	hostile=sensor:GetHostile(controllable,10,false)
	if not (victimize(controllable, sensor, hostile, victim)) then	
		if not (hostile== nil) and not (controllable.GetPeace()) then
			combat(controllable,sensor,hostile)
		else			
			patrol(controllable, sensor)
		end
	end
	return true;
end  

function tick(sensor)
	if (sensor.ReadGlobal(0) > 0) then
		value=sensor.ReadGlobal(0)
		value=value-1
		sensor.SetGlobal(0,value)
	else
		x = math.random(4,60)
		y = math.random(4,60)
		sensor.SetGlobal(2,x)
		sensor.SetGlobal(3,y)	
		sensor.SetGlobal(0,40)		
	end
	return true;
end