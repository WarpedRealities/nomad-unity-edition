function combat(controllable, sensor,hostile)
	value=sensor.ReadGlobal(0)
	if (value>5) then
		value=value+2	
	else
		value=value+1	
	end
	sensor.SetGlobal(0,value)
	
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then	
				controllable.FollowPath()
				if (hostile.GetDistance(controllable:GetPosition())>4) then
					controllable.FastMove()				
				end
			end		
		end
	end
end

function hunt(controllable,sensor)
	sensor.PathToPosition(controllable, sensor.ReadGlobal(2),sensor.ReadGlobal(3))	
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else
			if (sensor.ReadGlobal(0) > 5) then
				if (sensor.ReadGlobal(1)==1) then 
					hunt(controllable, sensor)
				end
			else
				direction=math.random(0,8)
				controllable.Move(direction);				
			end		
		end	
	end
	

	return true;
end  
