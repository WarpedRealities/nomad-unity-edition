function witnessRetreat(controllable,sensor)
	if (controllable.HasPath()) then
		controllable.FollowPath()
	else
		player = sensor:GetPlayer(controllable,10,false)
		if not (player == nil) then
			if (sensor.PathAwayFromActor(controllable,player,8)) then	
				controllable.FollowPath()
			end
		else
			if (controllable.ReportCrime()) then
				controllable.ClearWitness()
			end
		end
	end
end

function combat(controllable,sensor,hostile)
	health=controllable.GetStat(0)
	maximum=controllable.GetStatMax(0)
	if (health<maximum/2) then
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			if (sensor.PathAwayFromActor(controllable,hostile,8)) then	
				controllable.FollowPath()
			end
		end			
	else
		if (hostile.GetDistance(controllable:GetPosition())<2) then
			controllable.DoMove(hostile,0)	
			controllable.RemovePath();
		else		
			if (controllable.HasPath()) then
				controllable.FollowPath()
			else	
				if (sensor.PathToActor(controllable,hostile)) then
					controllable.FollowPath()
				end		
			end
		end
	end
end

function main (controllable, sensor)  	
	hostile=sensor:GetHostile(controllable,10,false)	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor,hostile)
	else
		if (controllable.IsWitness()) then
			witnessRetreat(controllable,sensor)
		else
			direction=math.random(0,8)
			controllable.Move(direction);	
			controllable.Delay(20)			
		end
	end
	return true;
end  