function combat(controllable, sensor,hostile)

	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then	
				controllable.FollowPath()
			end
		end
	end
end

function patrol(controllable,sensor)
	x = math.random(0,32)
	y = math.random(0,32)
	if (sensor.CanWalk(x,y)) then
		controllable.SetValue(0,10)	
		controllable.SetValue(1,x)
		controllable.SetValue(2,y)			
	end
	value=sensor.ReadGlobal(0)
	value=value+1
	sensor.SetGlobal(0,value)
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else
			if (controllable.GetValue(0)==0) then
				patrol(controllable,sensor)					

			else
				sensor.PathToPosition(controllable,controllable.GetValue(1),controllable.GetValue(2))
				p = controllable.GetValue(0)
				p = p -1 
				controllable.SetValue(0,p)	
			end
		end	

	end
	
	return true;
end  
