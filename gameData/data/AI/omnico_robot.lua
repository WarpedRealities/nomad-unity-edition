function combat(controllable, sensor,hostile)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then
	
				controllable.FollowPath()
			end
		end
	end
end

function robotWalk(controllable)

	stageValue=controllable:GetValue(0)
	direction = stageValue*2;
	if (not controllable.Move(direction)) then
		stageValue=stageValue+1
		if (stageValue > 3 ) then
			stageValue = 0
		end
	end
	controllable:SetValue(0,stageValue)
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		controllable.DoMove(hostile,0)		
	else
		robotWalk(controllable)
	end
	

	return true;
end  