function combat(controllable, sensor,hostile)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then
	
				controllable.FollowPath()
			end
		end
	end
end

function robotWalk(controllable)

	stageValue=controllable:GetValue(0)
	stepValue=controllable:GetValue(1)	
	stepValue = stepValue+1
	if stepValue > 8 then
		stepValue = 0
		stageValue = stageValue+1
		if stageValue > 3 then
			stageValue = 0
		end
	end 
	if stageValue == 0 then
		controllable.Move(0);		
	end
	if stageValue == 1 then
		controllable.Move(2);			
	end
	if stageValue == 2 then
		controllable.Move(4);		
	end	
	if stageValue == 3 then
		controllable.Move(6);		
	end
	controllable:SetValue(0,stageValue)
	controllable:SetValue(1,stepValue)

end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		controllable.DoMove(hostile,0)		
	else
		robotWalk(controllable)
	end
	

	return true;
end  