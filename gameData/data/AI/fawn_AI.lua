
function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			if (sensor.PathAwayFromActor(controllable,hostile,5)) then
	
				controllable.FollowPath()
			end
		end
	else
		direction=math.random(0,8)
		controllable.Move(direction);		
	end
	

	return true;
end  