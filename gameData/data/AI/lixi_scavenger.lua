
function combat(controllable, sensor, hostile)
	--set the hostile target for investigation
	controllable.SetValue(1,1)	
	controllable.SetValue(4,hostile.GetPosition().x)
	controllable.SetValue(5,hostile.GetPosition().y)
	roll=math.random(0,6)
	curlDelay = controllable.GetValue(6);
	health = controllable.GetActor().GetHealth()
	if (curlDelay==0 and health<1) and (roll<2) then
		controllable.DoSelfMove(1)	
		controllable.SetValue(6, 6)
		controllable.Delay(10)			
	else
		if (hostile.GetDistance(controllable:GetPosition())<2) then
			curlDelay = curlDelay - 1
			controllable.SetValue(6, curlDelay)
			controllable.DoMove(hostile,0)		
			controllable.RemovePath();
		else
			if (roll <2) then
				controllable.DoMove(hostile,2)	
			else
				if (controllable.HasPath()) then
					controllable.FollowPath()
				else
					if (sensor.PathToActor(controllable,hostile)) then	
						controllable.FollowPath()
						controllable.FastMove()
					end		
				end			
			end
		end		
	end

end

function active(controllable, sensor, hostile)
	--set awake

	controllable.SetValue(0,1)
	if not (hostile==nil) then

		if (controllable.GetActor().HasStatus(30) == true) then
			controllable.Delay(10)		
		else
			combat(controllable, sensor, hostile)		
		end
	else

		if (controllable.HasPath()) then
			controllable.FollowPath()	
		else
			chase = controllable.GetValue(1);
			if (chase == 1) then
				controllable.SetValue(1,0)
				sensor.PathToPosition(controllable,controllable.GetValue(4),controllable.GetValue(5))				
			else
				sensor.PathToPosition(controllable,controllable.GetValue(2),controllable.GetValue(3))			
			end
		end			
	
	end
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	awake = controllable.GetValue(0);
	if (awake == 1) or not (hostile==nil) then
		active(controllable, sensor, hostile)
	else
		controllable.Delay(10)	
		controllable.SetValue(2,controllable:GetPosition().x)
		controllable.SetValue(3,controllable:GetPosition().y)			
	end

	return true;
end