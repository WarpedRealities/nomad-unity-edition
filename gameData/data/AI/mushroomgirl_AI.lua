function combat(controllable, sensor,hostile)
	distance=hostile.GetDistance(controllable:GetPosition())
	if (distance<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		rand = math.random(0,3)
		if (rand<2) and (distance<10) then 
			controllable.DoMove(hostile,0)				
		else
			if (controllable.HasPath()) then
				controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,hostile)) then
					controllable.FollowPath()
				end		
			end
		end
	end	
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		direction=math.random(0,8)
		controllable.Move(direction);		
	end
	

	return true;
end  