function combat(controllable, sensor,hostile)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		if (controllable.HasPath()) then

			controllable.FollowPath()
		else
			if (sensor.PathToActor(controllable,hostile)) then
	
				controllable.FollowPath()
			end
		end
	end
end

function victimize(hostile, victim, controllable, sensor) 
	if (controllable.HasObserverVore()) then
		return false;
	end
	if (victim==nil) then
		return false;
	end
	if (hostile==nil) and not (victim==nil) then
		controllable.StartObserverVore("harpy_harpy",victim);
		return true;
	end
	if (hostile.GetDistance(controllable:GetPosition())>3) then
		controllable.StartObserverVore("harpy_harpy",victim);
		return true;		
	end
		
	return false;

end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	victim = sensor:GetActor(controllable,"harpy",2, true, false);	
	if not victimize(hostile,victim,controllable, sensor) then
		if not (hostile== nil) and not (controllable.GetPeace()) then
			combat(controllable,sensor, hostile)
		else
			direction=math.random(0,8)
			controllable.Move(direction);		
		end	
	end
	return true;
end  