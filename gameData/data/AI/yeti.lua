function combat(controllable, sensor,hostile)
	jump = controllable:GetValue(0)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,1)
		
		controllable.RemovePath();
	else
		if (hostile.GetDistance(controllable:GetPosition())<6) and
		(jump == 0) then
			controllable.DoMove(hostile,1)	
			controllable.SetValue(0,1)
		else
			if (controllable.HasPath()) then
				controllable.DoMove(hostile,0)	
			else
				if (sensor.PathToActor(controllable,hostile)) then
		
					controllable.FollowPath()
				end
			end	
		end

	end
end

function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()
		else
			direction=math.random(0,8)
			controllable.Move(direction);		
		end
	end

	return true;
end