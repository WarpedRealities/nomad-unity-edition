function combat(controllable, sensor,hostile)
	if (hostile.GetDistance(controllable:GetPosition())<2) then
		controllable.DoMove(hostile,0)
		
		controllable.RemovePath();
	else
		diceroll = math.random(0,4)
		if (diceroll==1 and not hostile.HasStatus(11)) then
			controllable.DoMove(hostile,1)		
			controllable.RemovePath();	
		else
			if (controllable.HasPath()) then

				controllable.FollowPath()
			else
				if (sensor.PathToActor(controllable,hostile)) then
		
					controllable.FollowPath()
				end
			end
		end
	end
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,true)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		controllable.Delay(10)
	end
	

	return true;
end  