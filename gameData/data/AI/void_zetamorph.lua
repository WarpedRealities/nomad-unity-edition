
function alert(controllable,sensor)
	a=math.random(0,8)
	if (a==0) then
		sensor:DrawText("A presence has become aware of you")
	end
	if (a==1) then
		sensor:DrawText("You sense psychic emanations")
	end
	if (a==2) then
		sensor:DrawText("A mental tendril brushes across your mind")
	end
	if (a==3) then
		sensor:DrawText("You feel as if you are watched")
	end
	if (a==4) then
		sensor:DrawText("You sense the attention of some entity upon you")
	end
	if (a==5) then
		sensor:DrawText("You are noticed. You are seen.")
	end
	if (a==6) then
		sensor:DrawText("A psycher has detected you")
	end
	if (a==7) then
		sensor:DrawText("You hear whisperings in the corner of your thoughts")
	end
end

function combat(controllable, sensor,hostile)
	sighted = sensor:GetHostile(controllable,10,false)

	if not (sighted==nil) then

		if (sensor.PathAwayFromActor(controllable,sighted,15)) then		
			controllable.FollowPath()

			controllable.SetValue(1,15)	
	
		return true;
		end		
	else
		value = controllable.GetValue(0);
		if (value==0) then
			alert(controllable,sensor)
		end
		if (value < 5 ) then
			direction=math.random(0,8)
			controllable.Move(direction);	
		end
		if (value >=5) and (value<15) then
			controllable.DoMove(hostile,0)	
		end
		if (value >=15) and (value<25) then
			controllable.DoMove(hostile,1)	
		end	
		if (value >=25) then
			controllable.DoMove(hostile,2)	
		end		
		value = value+1
		controllable.SetValue(0,value)		
	end
 
end

function escapeFunction(controllable, sensor, hostile)
		
	if (controllable.HasPath()) then
		controllable.FollowPath()		
	else
		if not (hostile==nil) and (sensor.PathAwayFromActor(controllable,hostile,15)) then		
			controllable.FollowPath()
		else
			direction=math.random(0,8)
			controllable.Move(direction);		
		end		
	end		
	
	controllable.SetValue(1,controllable.GetValue(1)-1)	
end

function main (controllable, sensor)  


	hostile = sensor:GetHostile(controllable,10,true)
	if (controllable.GetValue(1)> 0) then
		escapeFunction(controllable, sensor, hostile)
		return true;
	end
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		if (controllable.HasPath()) then
			controllable.FollowPath()		
		else			
			controllable.SetValue(0,0)	
			direction=math.random(0,8)
			controllable.Move(direction);		
		end
	end			


	return true;
end  

function start( controllable, sensor)
	controllable.SetValue(0,0)	
	return true;
end