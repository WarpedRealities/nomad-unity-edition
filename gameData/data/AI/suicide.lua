function combat(controllable, sensor,hostile)

	controllable.DoSelfMove(0)	
end


function main (controllable, sensor)  
	
	hostile=sensor:GetHostile(controllable,10,false)
	
	if not (hostile== nil) and not (controllable.GetPeace()) then
		combat(controllable,sensor, hostile)
	else
		controllable.Delay(10)
	end

	return true;
end  
