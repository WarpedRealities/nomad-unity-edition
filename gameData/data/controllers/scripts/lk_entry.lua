
function main (controllable, sensor)  
	if not controllable.IsAlive() and not sensor.Encounter() then
		controllable.SetInGame(false)
	end

	return true;
end  