
function main (controllable, sensor)  
	hostile = sensor:GetNearestShip(controllable)
	if not (hostile == nil) then
		angle = controllable.GetRelativeFacing(hostile.GetPosition());
		if (angle > 15) then
			controllable:SetTurn(1);
		end
		if (angle < -15) then
			controllable:SetTurn(-1);
		end
		if (angle > -15 and angle < 15) then
			controllable:SetTurn(0);
		end
		controllable:SetThrust(1.0);

		controllable:Attack(0,hostile);
	end
	return true;
end  