
function main (controllable, sensor)  
	hostile = sensor:GetNearestShip(controllable)
	if not (hostile == nil) then
		angle = controllable.GetRelativeFacing(hostile.GetPosition());
		if (angle > 15) then
			controllable:ModTurn(0.5);
		end
		if (angle < -15) then
			controllable:ModTurn(-0.5);
		end
		if (angle > -15 and angle < 15) then
			controllable:SetTurn(0);
		end
		if (angle> -45 and angle<45) then
			controllable:SetThrust(0.5);
		else
			controllable:SetThrust(1.0);
		end
		controllable:Attack(0,hostile);
	end
	nearest= sensor:GetNearestHostile(controllable);
	if not (nearest == nil) then
		controllable:Attack(1,nearest);	
	end
	return true;
end  