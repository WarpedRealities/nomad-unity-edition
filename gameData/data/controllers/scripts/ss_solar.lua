
function main (controllable, sensor)  
	player = sensor.DetectPlayer(controllable);
	
	if not (player == nil) then
		controllable.MoveTowards(player.GetPosition().x, player.GetPosition().y);
	else
		--controllable.Wait(10)
		controllable.Orbit(0,0,60)
	end
	return true;
end  