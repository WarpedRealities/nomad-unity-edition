using System.Collections;
using System.Collections.Generic;
using System.Xml;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HelpPanel : MonoBehaviour
{

    public TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        XmlDocument document = FileTools.GetXmlDocument("help");
        XmlElement root = (XmlElement)document.FirstChild.NextSibling;
        text.text = root.InnerText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ExitClick()
    {
        SceneManager.LoadScene("ViewScene");
    }
}
