﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NameInputPanel : MonoBehaviour
{

    public InputField inputField;
    private static string DEFAULTNAME = "Nomad";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ConfirmButton()
    {
        string name = inputField.text.Length > 0 ? inputField.text : DEFAULTNAME;

        GlobalGameState.GetInstance().GetPlayer().SetName(name);
        GameObject panel = transform.parent.Find("ChargenPanel").gameObject;
        panel.SetActive(true);
        panel.SendMessage("FinalizeChargen");
    }
}
