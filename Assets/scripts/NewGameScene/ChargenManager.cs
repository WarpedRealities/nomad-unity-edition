﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargenManager 
{
    List<ChargenPhase> phases;
    int phaseIndex, choiceIndex;

    public ChargenManager()
    {
        phaseIndex = 0;

        phases = ChargenLoader.LoadPhases();
        choiceIndex = phases[phaseIndex].GetSelection();
    }

    public ChargenPhase GetPhase()
    {
        return phases[phaseIndex];
    }

    public int GetIndex()
    {
        return phaseIndex;
    }

    internal void SetSelect(int index)
    {
        choiceIndex = index;
    }

    public int GetSelect()
    {
        return choiceIndex;
    }

    public void NextPhase()
    {
        phases[phaseIndex].MakeSelection(choiceIndex);
        phaseIndex++;
        choiceIndex = phases[phaseIndex].GetSelection();
    }

    public void MakeChoice()
    {
        phases[phaseIndex].MakeSelection(choiceIndex);
    }
    public void LastPhase()
    {
        phaseIndex--;
        choiceIndex = phases[phaseIndex].GetSelection();
   
    }
    public bool isFinalPhase()
    {
        return phaseIndex == phases.Count - 1;
    }

    internal void ProcessPhases()
    {
        for (int i = 0; i < phases.Count; i++)
        {
        
            phases[i].ApplySelection(GlobalGameState.GetInstance().GetPlayer());
        }
        ((PlayerRPG)GlobalGameState.GetInstance().GetPlayer().GetRPG()).CalcStats();
        ((PlayerRPG)GlobalGameState.GetInstance().GetPlayer().GetRPG()).CalcEquipment();
        GlobalGameState.GetInstance().SetPlaying(true);
       
    }
}
