﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargenPerk : ChargenPhase
{
    public class ChargenPerkChoice
    {
        private string name;
        private string perk;
        private string description;

        public ChargenPerkChoice(string name, string perk, string description)
        {
            this.name = name;
            this.perk = perk;
            this.description = description;
        }

        public string GetName()
        {
            return name;
        }

        public string GetPerk()
        {
            return perk;
        }

        public string GetDescription()
        {
            return description;
        }
    };
    private int index;
    string phaseName;
    List<ChargenPerkChoice> choices;
    List<string> choiceStrings;

    public ChargenPerk(string name, List<ChargenPerk.ChargenPerkChoice> choices)
    {
        index = -1;
        this.phaseName = name;
        this.choices = choices;
        this.choiceStrings = new List<string>();
        for (int i = 0; i < choices.Count; i++)
        {
            this.choiceStrings.Add(this.choices[i].GetName());
        }
    }

    public void ApplySelection(Player player)
    {
        Perk p = GlobalGameState.GetInstance().GetUniverse().GetPerkLibrary().GetPerk(this.choices[index].GetPerk());
        if (p != null)
        {
            ((PlayerRPG)player.GetRPG()).AddPerk(p);
        }
    }

    public string GetDescription(int index)
    {
        return this.choices[index].GetDescription();
    }

    public string GetName()
    {
        return this.phaseName;
    }

    public List<string> GetOptions()
    {
        return choiceStrings;
    }

    public int GetSelection()
    {
        return index;
    }

    public void MakeSelection(int index)
    {
        this.index = index;
    }
}
