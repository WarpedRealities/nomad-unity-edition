﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ChargenPhase
{

    string GetName();

    List<string> GetOptions();

    string GetDescription(int index);

    void MakeSelection(int index);

    void ApplySelection(Player player);

    int GetSelection();
}
