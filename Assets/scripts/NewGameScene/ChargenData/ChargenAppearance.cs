﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;



public class ChargenAppearance : ChargenPhase
{
    public class ChargenAppearanceChoice
    {
        public string name, description;
        public Mutator mutator;
        public ChargenAppearanceChoice(string name, string description, Mutator mutator)
        {
            this.name = name;
            this.description = Regex.Replace(description, @"[\t\r\n]", string.Empty);
            this.mutator = mutator;
        }

        internal void Apply(Player player)
        {
            mutator.ApplyEffect(player,null);
        }
    }

    List<ChargenAppearanceChoice> choices;
    List<string> choiceStrings;
    string name;
    private int index;

    public ChargenAppearance(string name, List<ChargenAppearanceChoice> choices)
    {
        index = -1;
        this.name = name;
        this.choices = choices;
        this.choiceStrings = new List<string>();
        for (int i = 0; i < choices.Count; i++)
        {
            this.choiceStrings.Add(this.choices[i].name);
        }
    }


    public void ApplySelection(Player player)
    {
        this.choices[this.index].Apply(player);
        //throw new System.NotImplementedException();
    }

    public string GetDescription(int index)
    {
        return this.choices[index].description;
    }

    public string GetName()
    {
        return name;
    }

    public List<string> GetOptions()
    {
        return this.choiceStrings;
    }

    public void MakeSelection(int index)
    {
        this.index = index;

    }

    public int GetSelection()
    {
     
        return index;
    }
}
