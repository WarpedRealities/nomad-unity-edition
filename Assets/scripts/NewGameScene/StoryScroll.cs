using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryScroll : MonoBehaviour
{
    public ScrollRect scrollRect;
    public RectTransform contentPanel;
    private readonly static float SPEED = 4;
    private float position = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        position += Time.deltaTime*SPEED;
        contentPanel.anchoredPosition = new Vector2(0, position);
    }
}
