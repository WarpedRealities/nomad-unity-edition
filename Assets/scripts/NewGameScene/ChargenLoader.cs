﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class ChargenLoader 
{
 

    public static List<ChargenPhase> LoadPhases()
    {
        List<ChargenPhase> chargenPhases = new List<ChargenPhase>();

        XmlDocument document = FileTools.GetXmlDocument("chargen");
        XmlNode root = document.FirstChild.NextSibling;
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("perkPhase".Equals(xmlElement.Name))
                {
                    chargenPhases.Add(BuildPerkPhase(xmlElement));
                }
                if ("lookPhase".Equals(xmlElement.Name))
                {
                    chargenPhases.Add(BuildLookPhase(xmlElement));
                }
            }

        }

        return chargenPhases;
    }

    private static ChargenAppearance BuildLookPhase(XmlElement xmlElement)
    {
        //name
        string name = xmlElement.GetAttribute("name");
        List<ChargenAppearance.ChargenAppearanceChoice> choices = new List<ChargenAppearance.ChargenAppearanceChoice>();

        for (int i = 0; i < xmlElement.ChildNodes.Count; i++)
        {
            if (xmlElement.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                
                XmlElement element = (XmlElement)xmlElement.ChildNodes[i];
                if ("choice".Equals(element.Name))
                {

                    choices.Add(BuildAppearanceChoice(element));
                }
            }
        }

        //populate phase with mutators
        return new ChargenAppearance(name, choices);
    }

    private static ChargenAppearance.ChargenAppearanceChoice BuildAppearanceChoice(XmlElement element)
    {
        string name= element.GetAttribute("name");
        string description=null;
        Mutator mutator=null;
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("mutator".Equals(e.Name))
                {
                    mutator = MutatorBuilder.BuildMutator(e);
                }
            }
        }
        return new ChargenAppearance.ChargenAppearanceChoice(name, description, mutator);
     
    }

    private static ChargenPerk BuildPerkPhase(XmlElement xmlElement)
    {
        string name = xmlElement.GetAttribute("name");
        List<ChargenPerk.ChargenPerkChoice> choices = new List<ChargenPerk.ChargenPerkChoice>();
        for (int i = 0; i < xmlElement.ChildNodes.Count; i++)
        {
            if (xmlElement.ChildNodes[i].NodeType == XmlNodeType.Element)
            {

                XmlElement element = (XmlElement)xmlElement.ChildNodes[i];
                if ("choice".Equals(element.Name))
                {
                    choices.Add(BuildPerkChoice(element));
                }
            }
        }
        return new ChargenPerk(name, choices);
    }

    private static ChargenPerk.ChargenPerkChoice BuildPerkChoice(XmlElement element)
    {
        return new ChargenPerk.ChargenPerkChoice(element.GetAttribute("name"), element.GetAttribute("perk"), element.InnerText);
    }
}
