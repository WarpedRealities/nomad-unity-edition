﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChargenPanel : MonoBehaviour
{
    ChargenManager chargenManager;
    public NuList choiceList;
    public Text phaseText, selectionText,descriptionText;
    public Button backButton, nextButton;
    private bool finished = false;
    // Start is called before the first frame update
    void Start()
    {
        GlobalGameState.GetInstance().NewGame();
        chargenManager = new ChargenManager();
        ListCallback listCallback = ListSelect;
        choiceList.SetCallback(listCallback);
        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (!finished)
        {
            finished = true;
            UpdateUI();
        }
    }

    void UpdateUI()
    {
        choiceList.SetList(chargenManager.GetPhase().GetOptions());
        phaseText.text = "Phase:"+chargenManager.GetIndex() +" name:" + chargenManager.GetPhase().GetName();
        descriptionText.text = "";
        selectionText.text = "";
        nextButton.interactable = chargenManager.GetSelect() != -1;
        if (chargenManager.GetSelect() != -1)
        {
            descriptionText.text = chargenManager.GetPhase().GetDescription(chargenManager.GetSelect());
            selectionText.text = chargenManager.GetPhase().GetOptions()[chargenManager.GetSelect()];
        }
        nextButton.GetComponentInChildren<Text>().text = chargenManager.isFinalPhase() ? "Finish" : "Next";
    }

    void ListSelect(int index)
    {
        chargenManager.SetSelect(index);
        descriptionText.text = chargenManager.GetPhase().GetDescription(index);
        selectionText.text = chargenManager.GetPhase().GetOptions()[index];
        nextButton.interactable = true;
    }

    public void ClickNext()
    {
        if (chargenManager.isFinalPhase())
        {
            chargenManager.MakeChoice();
            //make the name panel appear
            transform.parent.Find("NamePanel").gameObject.SetActive(true);
            //make this panel disappear
            this.gameObject.SetActive(false);
        }
        else
        {
            chargenManager.NextPhase();
        }
        UpdateUI();
    }

    public void ClickBack()
    {
        if (chargenManager.GetIndex() > 0)
        {
            chargenManager.LastPhase();
            UpdateUI();
        }

    }

    public void ClickExit()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void FinalizeChargen()
    {
        //apply all phases!
        chargenManager.ProcessPhases();
        SceneManager.LoadScene("ViewScene");
    }
}
