﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class StarSystemContents 
{
    List<Entity> entities;
    private float[] baseColour;
    private float[] highlightColour;
    SpaceGrid spaceGrid;
    DistressData distressData;

    public void setEntities(List<Entity> entities)
    {
        this.entities = entities;
    }

    internal SpaceGrid GetSpaceGrid()
    {
        return spaceGrid;
    }

    internal float[] GetColor()
    {
        return this.baseColour;
    }

    internal float[] GetHighlight()
    {
        return this.highlightColour;
    }

    public List<Entity> GetEntities()
    {
        return entities;
    }

    internal void PostLoad()
    {
        for (int i = 0; i < entities.Count; i++)
        {
            entities[i].PostLoad();
        }
    }

    public void SetColours(float[] baseColour, float[] highlight)
    {
        this.baseColour = baseColour;
        this.highlightColour = highlight;
    }

    internal void SetSpaceGrid(SpaceGrid spaceGrid)
    {
        this.spaceGrid = spaceGrid;
    }

    public void SetDistressData(DistressData distressData)
    {
        this.distressData = distressData;
    }

    public DistressData GetDistressData()
    {
        return this.distressData;
    }
}
