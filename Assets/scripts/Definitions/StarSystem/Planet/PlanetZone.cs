﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PlanetZone 
{
    private Zone zone;
    private Vector2S position;
    private ZoneType zoneType;

    public PlanetZone(Vector2S position, ZoneType zoneType, Zone zone)
    {
        this.position = position;
        this.zoneType = zoneType;
        this.zone = zone;
    }

    public Zone GetZone()
    {
        return zone;
    }

    public ZoneType GetZoneType()
    {
        return zoneType;
    }

    public Vector2S GetPosition()
    {
        return position;
    }
}
