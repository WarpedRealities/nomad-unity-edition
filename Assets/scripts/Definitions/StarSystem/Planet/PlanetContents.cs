﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class PlanetContents 
{
    List<PlanetZone> planetZones;
    List<LandingSite> landingSites;

    public PlanetContents(List<PlanetZone> zones) 
    {
        planetZones = zones;
    }

    public List<PlanetZone> GetZones()
    {
        return planetZones;
    }

    internal Zone getZone(string name, Entity entity)
    {
        for (int i = 0; i < planetZones.Count; i++)
        {
            if (planetZones[i].GetZone().getName().Equals(name))
            {
                if (planetZones[i].GetZone().GetContents() == null)
                {
                    planetZones[i].GetZone().SetEntity(entity);
                }
                return planetZones[i].GetZone();
            }
        }

        if (landingSites != null)
        {
            for (int i = 0; i < landingSites.Count; i++)
            {
            
                if (landingSites[i].GetSpaceship().GetZone(name) != null)
                {
                    return landingSites[i].GetSpaceship().GetZone(name);
                }
            }
        }
        return null;
    }

    internal void SetLandingSites(List<LandingSite> landingSites)
    {
        this.landingSites = landingSites;
    }

    public List<LandingSite> GetLandingSites()
    {
        return this.landingSites;
    }

    internal void PostLoad()
    {
        for (int i = 0; i < this.planetZones.Count; i++)
        {
            if (planetZones[i].GetZone().IsGenerated() && planetZones[i].GetZone().GetContents() != null)
            {

                planetZones[i].GetZone().GetContents().PostLoad();
            }

        }
        if (landingSites != null)
        {
            for (int i = 0; i < this.landingSites.Count; i++)
            {
                if (landingSites[i] != null && landingSites[i].GetSpaceship() != null)
                {
                    landingSites[i].GetSpaceship().PostLoad();
                }
            }
        }

    }
    
}
