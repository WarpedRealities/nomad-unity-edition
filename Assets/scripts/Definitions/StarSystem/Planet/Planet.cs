﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Planet : Entity 
{
    protected PlanetContents contents;
    protected bool isGenerated;

    public Planet(Vector2Int position,string filename)
    {
        this.isGenerated = false;
        this.position = position;
        this.name = filename;
    }


    public override ENTITYTYPE GetEntityType()
    {
        return ENTITYTYPE.PLANET;
    }

    public override bool NeedsGeneration()
    {
        return !isGenerated;
    }

    public override string getSprite()
    {

        return "worlds/"+this.sprite;
    }

    public override void generateForSolar()
    {
        PlanetContentTool contentTool = new PlanetContentTool();
        this.sprite = contentTool.generateSolar(name, out size);
        isGenerated = true;
    }

    public PlanetContents GetContents()
    {
        return contents;
    }

    public override Zone GetZone(string name)
    {
 
        return contents.getZone(name,this);
    }

    public override bool isAlwaysVisible()
    {
        return true;
    }

    public override void generate()
    {
        PlanetContentTool contentTool = new PlanetContentTool();
        contents = contentTool.generateContents(name, this);
    }

    public override LandingSite GetLandingSite(Zone zone)
    {        
        if (contents==null || contents.GetLandingSites() == null)
        {
            return null;
        }
        for (int i = 0; i < contents.GetLandingSites().Count; i++)
        {
            LandingSite landingSite = contents.GetLandingSites()[i];
            if (zone.getName().Equals(landingSite.GetZone()))
            {
                return landingSite;
            }
        }
        return null;
    }

    public override LandingSite GetLandingSite(Spaceship ship)
    {
        if (contents.GetLandingSites() == null)
        {
            return null;
        }
        for (int i = 0; i < contents.GetLandingSites().Count; i++)
        {
            LandingSite landingSite = contents.GetLandingSites()[i];
            if (ship.Equals(landingSite.GetSpaceship()))
            {
                return landingSite;
            }
        }
        return null;
    }

    public override Spaceship GetShip(string name)
    {
        for (int i = 0; i < contents.GetLandingSites().Count; i++)
        {
            if (contents.GetLandingSites()[i]!=null &&
                contents.GetLandingSites()[i].GetSpaceship().getName().Equals(name))
            {
                return contents.GetLandingSites()[i].GetSpaceship();
            }
        }
        return null;
    }

    public override void RemoveLandingSite(LandingSite landingSite)
    {
        contents.GetLandingSites().Remove(landingSite);
    }

    public override Zone GetZone(int index)
    {
        return contents.GetZones()[index].GetZone();
    }

    internal override void PostLoad()
    {
        if (contents != null)
        {
            contents.PostLoad();
        }
    }

    internal bool CanLand()
    {
        return this.contents.GetZones().Count > 0;
    }

    internal LandingSite GetLandingSite(string shipName)
    {
        if (contents != null)
        {
            for (int i = 0; i < contents.GetLandingSites().Count; i++)
            {
                if (contents.GetLandingSites()[i].GetSpaceship().getName().Equals(shipName))
                {
                    return contents.GetLandingSites()[i];
                }
            }
        }
        return null;
    }
}
