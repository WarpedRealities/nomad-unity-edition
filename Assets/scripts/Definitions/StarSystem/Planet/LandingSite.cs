﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization;

[Serializable]
public class LandingSite 
{

    String zone;
    Spaceship spaceship;
    bool surfaceLanding;
    Vector2S position;

    public LandingSite(string zone, Spaceship spaceship, bool surface=true)
    {
        this.zone = zone;
        this.spaceship = spaceship;
        this.surfaceLanding = surface;
    }


    public bool IsSurface()
    {
        return surfaceLanding;
    }

    public Spaceship GetSpaceship()
    {
        return spaceship;
    }

    public string GetZone()
    {
        return zone;
    }

    public Vector2S GetPosition()
    {
        return position;
    }

    public void SetPosition(Vector2S position)
    {
        this.position = position;
    }

    internal void SetSpaceship(Spaceship spaceship)
    {
        this.spaceship = spaceship;
    }
}
