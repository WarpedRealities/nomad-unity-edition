﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


[Serializable]
public class DistressData {

    List<DistressCall> distressCalls;

    public DistressData(List<DistressCall> distressCalls)
    {
        this.distressCalls = distressCalls;
    }

    public List<DistressCall> GetDistressCalls()
    {
        return distressCalls;
    }
}
