using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpaceGrid
{
    SpaceTile[][] spaceTiles;

    public SpaceGrid()
    {
        spaceTiles = new SpaceTile[256][];
        for (int i = 0; i < spaceTiles.Length; i++)
        {
            spaceTiles[i] = new SpaceTile[256];
        }
    }

    public int GetObfuscation(int x, int y)
    {
        x = GetOffSetPosition(x);
        y = GetOffSetPosition(y);
        if (x < 0 || x >= 256 || y < 0 || y >= 256)
        {
            return 0;
        }
        if (spaceTiles[x][y] != null)
        {
            return spaceTiles[x][y].GetObfuscation();
        }
        return 0;
    }

    public int GetHazard(int x, int y)
    {
        x = GetOffSetPosition(x);
        y = GetOffSetPosition(y);
        if (x<0 || x >= 256 || y<0 || y>=256)
        {
            return 0;
        }
        if (spaceTiles[x][y] != null)
        {
            return spaceTiles[x][y].GetHazard();
        }
        return 0;
    }

    private int GetOffSetPosition(int value)
    {
        return value + 128;
    }

    internal void ModTile(int x, int y, int hazard, int obfuscation, bool additive)
    {
        if (spaceTiles[x][y] != null)
        {
            if (additive)
            {
                spaceTiles[x][y].ModHazard(hazard);
                spaceTiles[x][y].ModObfuscation(obfuscation);
            }
            else
            {
                spaceTiles[x][y].SetHazard(hazard);
                spaceTiles[x][y].SetObfuscation(obfuscation);
            }
        }
        else
        {
            spaceTiles[x][y] = new SpaceTile(hazard, obfuscation);
        }
    }
}
