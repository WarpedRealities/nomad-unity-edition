using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpaceTile
{
    private int hazard, obfuscation;


    public SpaceTile(int hazard, int obfuscation)
    {
        this.hazard = hazard;
        this.obfuscation = obfuscation;
    }

    public int GetHazard()
    {
        return hazard;
    }
    public int GetObfuscation()
    {
        return obfuscation;
    }

    public void SetHazard(int haz)
    {
        this.hazard = haz;
    }
    public void SetObfuscation(int ob)
    {
        this.obfuscation = ob;
    }
    public void ModObfuscation(int ob)
    {
        this.obfuscation += ob;
    }
    public void ModHazard(int haz)
    {
        this.hazard += haz;
    }
}
