﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Star_Entity : Entity
{
    protected float intensity;

    public Star_Entity()
    {

    }


    public Star_Entity(Vector2Int position, float size, float intensity, string sprite)
    {
        this.position = position;
        this.size = size;
        this.intensity = intensity;
        this.sprite = sprite;
        this.name = sprite;
    }

    public override void generate()
    {
    }

    public override bool isAlwaysVisible()
    {
        return true;
    }

    public override ENTITYTYPE GetEntityType()
    {
        return ENTITYTYPE.STAR;
    }

    public override LandingSite GetLandingSite(Zone zone)
    {
        throw new NotImplementedException();
    }

    public override LandingSite GetLandingSite(Spaceship ship)
    {
        throw new NotImplementedException();
    }

    public override Spaceship GetShip(string name)
    {
        throw new NotImplementedException();
    }

    public override string getSprite()
    {
        return "stars/" + sprite;
    }

    public override Zone GetZone(string name)
    {
        return null;
    }

    public override Zone GetZone(int index)
    {
        throw new NotImplementedException();
    }

    public override void RemoveLandingSite(LandingSite landingSite)
    {
        throw new NotImplementedException();
    }
}
