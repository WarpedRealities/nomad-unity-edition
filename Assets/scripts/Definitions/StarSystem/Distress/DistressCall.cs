﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using MoonSharp.Interpreter;


public class DistressLua
{
    public Script conditionLua, distressLua;
    public DynValue conditionFunction, distressFunction;
    public object[] arguments;

    public DistressLua(string conditionScript, string eventScript)
    {
        this.conditionLua = new Script();
        this.distressLua = new Script();
        String fileContents = FileTools.GetFileRaw("scripts/distress/" + conditionScript + ".lua");

        this.conditionLua.DoString(fileContents);
        fileContents = FileTools.GetFileRaw("scripts/distress/" + eventScript + ".lua");
        this.distressLua.DoString(fileContents);
        conditionFunction = this.conditionLua.Globals.Get("main");
        distressFunction = this.distressLua.Globals.Get("main");
        arguments = new object[3];
    }
}

[Serializable]
public class DistressCall
{
    int chance;
    string conditionScript;
    string eventScript;
    [NonSerialized]
    DistressLua distressLua;
    public DistressCall(int chance, string condition, string script) {
        this.chance = chance;
        this.conditionScript = condition;
        this.eventScript = script;
    }

    public int GetChance()
    {
        return chance;
    }

    public string GetCondition()
    {
        return conditionScript;
    }

    public string GetScript()
    {
        return eventScript;
    }

    internal bool RunCondition(LuaSolarSensor luaSolarSensor, FlagDataAdv luaFlagField)
    {
        if (distressLua == null)
        {
            distressLua = new DistressLua(conditionScript, eventScript);
        }

        distressLua.arguments[0] = luaSolarSensor;
        distressLua.arguments[1] = luaFlagField;
        DynValue rValue = this.distressLua.conditionLua.Call(distressLua.conditionFunction, distressLua.arguments);
        return rValue.Boolean;
    }

    internal void RunEvent(LuaSolarSensor luaSolarSensor, FlagDataAdv luaFlagData)
    {
        if (distressLua == null)
        {
            distressLua = new DistressLua(conditionScript, eventScript);
        }

        distressLua.arguments[0] = luaSolarSensor;
        distressLua.arguments[1] = luaFlagData;
        DynValue rValue = this.distressLua.distressLua.Call(distressLua.distressFunction, distressLua.arguments);

    }
}