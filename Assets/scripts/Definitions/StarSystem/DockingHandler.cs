﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class DockingHandler : Entity
{
    LandingSite[] landingSites = new LandingSite[2];

    public DockingHandler(Vector2 position)
    {
        this.name = "DOCKINGHANDLER";
        this.position = position;
    }

    public override void generate()
    {
       // throw new NotImplementedException();
    }

    public override ENTITYTYPE GetEntityType()
    {
        return ENTITYTYPE.HANDLER;
    }

    public void Land(Spaceship spaceship, int index, String zone)
    {
        this.landingSites[index] = new LandingSite(zone, spaceship, false);
    }

    public override LandingSite GetLandingSite(Zone zone)
    {
        for (int i = 0; i < 2; i++)
        {
            if (landingSites[i] != null &&
                landingSites[i].GetZone().Equals(zone.getName()))
            {
                return landingSites[i];
            }
        }
        return null;
    }

    public override Zone GetZone(string name)
    {
        for (int i = 0; i < 2; i++) {
            if (landingSites[i]!=null &&
                landingSites[i].GetSpaceship().GetZone(0).getName().Equals(name))
            {
                return landingSites[i].GetSpaceship().GetZone(0);
            }
        }
        return null;
    }

    public override LandingSite GetLandingSite(Spaceship ship)
    {
        for (int i = 0; i < 2; i++)
        {
            if (landingSites[i] != null &&
                landingSites[i].GetSpaceship().Equals(ship))
            {
                return landingSites[i];
            }
        }
        return null;
    }

    public override Spaceship GetShip(string name)
    {
        for (int i = 0; i < 2; i++)
        {
            if (landingSites[i] != null &&
                landingSites[i].GetSpaceship().getName().Equals(name))
            {
                return landingSites[i].GetSpaceship();
            }
        }
        return null;
    }

    public override bool isAlwaysVisible()
    {
        return false;
    }

    public override void RemoveLandingSite(LandingSite landingSite)
    {
        //demolish the whole thing
        for (int i = 0; i < landingSites.Length; i++)
        {
            if (landingSite != landingSites[i])
            {

                Vector2Int p = new Vector2Int((int)this.GetPosition().x, (int)this.GetPosition().y);
                GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Add(landingSites[i].GetSpaceship());
                landingSites[i].GetSpaceship().setPosition(p);
                landingSites[i].GetSpaceship().SetState(ShipState.SPACE);
            }
        }
        GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Remove(this);
    }
}
