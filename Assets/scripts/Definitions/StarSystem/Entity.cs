﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public abstract class Entity
{
    [NonSerialized]
    protected Vector2 position;
    private Vector2SF serializePosition;
    protected string sprite;
    protected float size=1;
    protected string name;
    protected bool visible;

    [OnSerializing()]
    internal void OnSerialize(StreamingContext streamingContext)
    {
        serializePosition = new Vector2SF(position.x, position.y);
    }

    internal virtual void PostLoad()
    {

    }

    public virtual bool GetInGame()
    {
        return true;
    }

    public abstract LandingSite GetLandingSite(Zone zone);
    public abstract LandingSite GetLandingSite(Spaceship ship);
    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        position = new Vector2(serializePosition.x, serializePosition.y);
    }

    internal bool GetVisible()
    {
        return visible;
    }

    public void SetVisible(bool visible)
    {
        this.visible = visible;
    }

    virtual public string getSprite()
    {
        return this.sprite;
    }

    public string getName()
    {
        return name;
    }

    virtual public bool NeedsGeneration()
    {
        return false;
    }

    public abstract void RemoveLandingSite(LandingSite landingSite);

    virtual public void generateForSolar()
    {

    }

    public abstract Spaceship GetShip(string name);

    public abstract ENTITYTYPE GetEntityType(); 

    public Vector2 GetPosition()
    {
        return position;
    }

    virtual public float getSize()
    {
        return this.size;
    }

    virtual public float getRotation()
    {
        return 0;
    }

    abstract public bool isAlwaysVisible(); 

    public virtual int GetDetection()
    {
        return 0;
    }

    public virtual Zone GetZone(string name)
    {
        return null;
    }

    public virtual Zone GetZone(int index)
    {
        return null;
    }

    abstract public void generate();

    internal virtual void SetDetected(bool isDetected)
    {

    }
}
