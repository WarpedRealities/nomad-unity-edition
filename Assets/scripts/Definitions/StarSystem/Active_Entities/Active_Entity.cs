﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Active_Entity : Entity
{
    protected int facing;
    protected bool inGame = true;
    protected Base_Captain controller;
    protected CommonStats commonStats;
    protected SolarStats solarStats;
    [NonSerialized]
    protected LuaActiveEntity luaEntity;
    public override float getRotation()
    {
        return facing * 45;
    }

    public void setPosition(Vector2Int position)
    {
        this.position = position;
    }

    internal void SetFacing(int facing)
    {
        this.facing = facing;
    }

    public int GetFacing()
    {
        return facing;
    }

    public LuaActiveEntity GetLuaEntity()
    {
        if (luaEntity == null)
        {
            luaEntity = new LuaActiveEntity(this);
        }
        return luaEntity;
    }

    public override bool isAlwaysVisible()
    {       
        return false;
    }

    public override bool GetInGame()
    {
        return inGame;
    }
    public override void generate()
    {

    }


    public void setSolarStats(SolarStats solarStats)
    {
        this.solarStats = solarStats;
    }

    public SolarStats GetSolarStats()
    {
        return solarStats;
    }

    internal void SetInGame(bool value)
    {
        inGame = value;
    }

    public void SetController(Base_Captain controller)
    {
        this.controller = controller;
    }

    public Base_Captain GetController()
    {
        return controller;
    }

    public CommonStats GetCommon()
    {
        return commonStats;
    }

    internal override void SetDetected(bool isDetected)
    {
        SetVisible(isDetected);
    }
}
