﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class SpaceshipGenerator
{
    private const string FILEPREFIX = "/ships/";

    public Spaceship GenerateShip(string file,UIDGenerator uIDGenerator)
    {
        XmlDocument document = FileTools.GetXmlDocument(FILEPREFIX + file);
        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        SpaceshipStats spaceshipStats = null;
        Zone zone = null;
        String sprite = rootXML.GetAttribute("sprite");
        String name = rootXML.GetAttribute("name")+"_"+
            uIDGenerator.GetShipID();
;        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];

                if ("stats".Equals(element.Name))
                {
                    spaceshipStats = generateStats(element);
                }

                if ("layout".Equals(element.Name))
                {
                    zone = new Zone(name);
                    zone.SetContents(new ZoneContentsGenerator().generateContents(element, null,null,null));
                }

            }
        }
        return new Spaceship(name,file, sprite, spaceshipStats,zone);
    }

    private SpaceshipStats generateStats(XmlElement rootXML)
    {
        int hp=0, width=0, height=0, detection=0, armour=0;
        float cruise=0, thrust=0, fuelCost=0, manouver=0;
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("width".Equals(element.Name))
                {
                    width = int.Parse(element.GetAttribute("value"));
                }
                if ("height".Equals(element.Name))
                {
                    height = int.Parse(element.GetAttribute("value"));
                }
                if ("hull".Equals(element.Name))
                {
                    hp = int.Parse(element.GetAttribute("value"));
                }
                if ("detection".Equals(element.Name))
                {
                    detection = int.Parse(element.GetAttribute("value"));
                }
                if ("cruise".Equals(element.Name))
                {
                    cruise = float.Parse(element.GetAttribute("value"));
                }
                if ("thrust".Equals(element.Name))
                {
                    thrust = float.Parse(element.GetAttribute("value"));
                }
                if ("fuelCost".Equals(element.Name))
                {
                    fuelCost = float.Parse(element.GetAttribute("value"));
                }
                if ("manouver".Equals(element.Name))
                {
                    manouver = float.Parse(element.GetAttribute("value"));
                }
                if ("armour".Equals(element.Name))
                {
                    armour = int.Parse(element.GetAttribute("value"));
                }
            }
        }
        return new SpaceshipStats(hp,width,height,cruise,thrust,fuelCost,manouver, detection, armour);
    }
}
