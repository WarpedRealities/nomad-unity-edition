using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class CreatureGeneratorTool
{
    private const string FILEPREFIX = "/creatures/";
    internal Creature GenerateCreature(string filename)
    {
        XmlDocument document = FileTools.GetXmlDocument(FILEPREFIX + filename);
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        String sprite = FILEPREFIX+rootXML.GetAttribute("sprite");
        String name = rootXML.GetAttribute("name");
        float size = float.Parse(rootXML.GetAttribute("size"));
        CommonStats commonStats = null;
        SolarStats solarStats = null;
        CombatStats combatStats = null;
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];

                if ("solar".Equals(element.Name))
                {
                    solarStats = BuildSolarStats(element);
                }
                if ("common".Equals(element.Name))
                {
                    commonStats = BuildCommonStats(element);
                }
                if ("combat".Equals(element.Name))
                {
                    combatStats = BuildCombatStats(element);
                }
            }
        }
        return new Creature(commonStats, solarStats, combatStats, size, sprite);
    }

    private CombatStats BuildCombatStats(XmlElement rootXML)
    {
        float speed = 0;
        float manouver = 0;
        float countermeasures = 0;
        float armour = 0;
        int pilot = 0;
        int gun = 0;
        int tech = 0;
        int sci = 0;
        List<WeaponSystem> weapons = new List<WeaponSystem>();
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("skills".Equals(element.Name))
                {
                    int.TryParse(element.GetAttribute("pilot"), out pilot);
                    int.TryParse(element.GetAttribute("gunnery"), out gun);
                    int.TryParse(element.GetAttribute("tech"), out tech);
                    int.TryParse(element.GetAttribute("science"), out sci);
                }
                if ("stats".Equals(element.Name))
                {
                    float.TryParse(element.GetAttribute("speed"), out speed);
                    float.TryParse(element.GetAttribute("manouver"), out manouver);
                    float.TryParse(element.GetAttribute("countermeasures"), out countermeasures);
                    float.TryParse(element.GetAttribute("armour"), out armour);
                }
                if ("weapon".Equals(element.Name))
                {
                    weapons.Add(BuildWeapon(element));
                }
            }

        }
        return new CombatStats(speed, manouver, countermeasures, armour, pilot, gun, tech, sci, weapons);
    }

    private WeaponSystem BuildWeapon(XmlElement rootXML)
    {
        CommonWeapon weapon = null;
        String name = rootXML.GetAttribute("name");
        FIRINGARC arc = EnumTools.strToFiringArc(rootXML.GetAttribute("arc"));
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("directWeapon".Equals(element.Name))
                {
                    weapon = ShipSystemBuilder.BuildDirectWeapon(element);
                }
                if ("strikeWeapon".Equals(element.Name))
                {
                    weapon = ShipSystemBuilder.BuildStrikeWeapon(element);
                }
                
            }
        }
        weapon.SetEmitter(new Vector2FS(float.Parse(rootXML.GetAttribute("x")), float.Parse(rootXML.GetAttribute("x"))));
        WeaponSystem weaponSystem = new WeaponSystem(name, arc, weapon);
        return weaponSystem;
    }

    private CommonWeapon BuildStrikeWeapon(XmlElement rootXML)
    {
        throw new NotImplementedException();
    }

    private CommonWeapon BuildDirectWeapon(XmlElement rootXML)
    {
        throw new NotImplementedException();
    }

    private SolarStats BuildSolarStats(XmlElement root)
    {
        float cruise = float.Parse(root.GetAttribute("speed"));
        float sensors = float.Parse(root.GetAttribute("sensors"));
        float detection = float.Parse(root.GetAttribute("detection"));
        return new SolarStats(cruise, 0, sensors, detection, 0, 0);
    }

    private CommonStats BuildCommonStats(XmlElement root)
    {
        int hp = int.Parse(root.GetAttribute("health"));
        return new CommonStats(hp, hp);
    }
}
