using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices.WindowsRuntime;

[Serializable]
public class Creature : Active_Entity
{
    private CombatStats combatStats;
    public Creature(CommonStats commonStats, SolarStats solarStats, CombatStats combatStats, float size, String sprite)
    {
        this.sprite = sprite;
        this.commonStats = commonStats;
        this.solarStats = solarStats;
        this.combatStats = combatStats;
        this.size = size;
    }

    public override ENTITYTYPE GetEntityType()
    {
        return ENTITYTYPE.CREATURE;
    }

    public override LandingSite GetLandingSite(Zone zone)
    {
        throw new NotImplementedException();
    }

    public override LandingSite GetLandingSite(Spaceship ship)
    {
        throw new NotImplementedException();
    }

    public override Spaceship GetShip(string name)
    {
        throw new NotImplementedException();
    }

    public override void RemoveLandingSite(LandingSite landingSite)
    {
        throw new NotImplementedException();
    }

    internal void Interact(Spaceship playerShip, DelegateLogText logText, SolarSceneController solarSceneController)
    {
        if (this.GetController() != null)
        {

            //need to run the get controller
            SolarScriptRunnerTool solarScriptRunnerTool = new SolarScriptRunnerTool(
                GlobalGameState.GetInstance().getCurrentSystem());
            solarScriptRunnerTool.RunScript(this.GetController().GetData().GetContactScript(), this);
        }
    }

    public override int GetDetection()
    {

        if (this.GetSolarStats() != null)
        {
            return (int)this.GetSolarStats().GetDetection();
        }
        return 1;

    }

    internal CombatStats GetCombatStats()
    {
        return combatStats;
    }
}