using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NPC_Skills
{
    int piloting, gunnery, science, tech;

    public NPC_Skills(int piloting, int gunnery, int science, int tech)
    {
        this.piloting = piloting;
        this.gunnery = gunnery;
        this.science = science;
        this.tech = tech;
    }

    public int GetPiloting()
    {
        return piloting;
    }
    public int GetGunnery()
    {
        return gunnery;
    }
    public int GetScience()
    {
        return science;
    }
    public int GetTech()
    {
        return tech;
    }

}

[Serializable]
public class NPC_Captain : Base_Captain
{
    private NPC_Skills skills;
    private ActiveData activeData;

    public NPC_Captain(ActiveData activeData, NPC_Skills skills)
    {

        this.skills = skills;
        this.activeData = activeData;
    }

    public NPC_Skills GetSkills()
    {
        return skills;
    }

    public override void Update(int duration)
    {
        throw new System.NotImplementedException();
    }

    public override ActiveData GetData()
    {
        return activeData;
    }
}
