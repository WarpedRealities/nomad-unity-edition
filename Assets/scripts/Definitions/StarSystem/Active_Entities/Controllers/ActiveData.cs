﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ActiveData 
{
    FlagField flagField;
    int[] memoryValues;

    Faction faction;

    int experienceValue;

    string solarScript;
    string entryScript;
    string contactScript;
    string combatScript;
    string winScript;
    string lossScript;


    public ActiveData(int experience, Faction faction, string solar, string combat, string entry, string contact, string win, string lose)
    {
        flagField = new FlagField();
        memoryValues = new int[16];
        this.experienceValue = experience;
        this.faction = faction;
        this.solarScript = solar;
        this.combatScript = combat;
        this.entryScript = entry;
        this.contactScript = contact;
        this.winScript = win;
        this.lossScript = lose;
    }

    public string GetEntryScript()
    {
        return entryScript;
    }
    public string GetSolarScript()
    {
        return solarScript;
    }
    public string GetCombatScript()
    {
        return combatScript;
    }
    public string GetContactScript()
    {
        return contactScript;
    }
    public string GetWinScript()
    {
        return winScript;
    }
    public string GetLossScript()
    {
        return lossScript;
    }

    public FlagField GetFlagField()
    {
        return flagField;
    }

    public Faction GetFaction()
    {
        return faction;
    }
}
