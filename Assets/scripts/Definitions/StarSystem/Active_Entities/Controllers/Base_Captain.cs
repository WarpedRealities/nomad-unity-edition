﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Base_Captain 
{

    protected int busyTicks;

    [NonSerialized]
    protected LuaSolarControllable luaControllable;

    public abstract void Update(int duration);

    public bool isReady()
    {
        return busyTicks <= 0;
    }

    public int GetTicks()
    {
        return busyTicks;
    }

    public void AddBusyTicks(int ticks)
    {
        busyTicks += ticks;
    }

    public virtual ActiveData  GetData()
    {
        return null;
    }

    public virtual void OnEntry(Active_Entity active_Entity)
    {

    }

    public LuaSolarControllable GetControllable()
    {
        return luaControllable;
    }

    public void SetControllable(LuaSolarControllable luaControllable)
    {
        this.luaControllable = luaControllable;
    }

    internal void RemoveTicks(int duration)
    {
        this.busyTicks -= duration;
    }
}
