﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class SpaceshipResourceStats 
{
    Dictionary<string, ShipResource> resourceDictionary;
    List<ShipResource> resourceList;
    List<ShipConverter> converterList;
    List<ShipMagazine> magazineList;

    public SpaceshipResourceStats(Dictionary<string, ShipResource> res,List<ShipResource>resList, List<ShipConverter> converters, List<ShipMagazine> magazines) 
    {
        this.resourceDictionary = res;
        this.resourceList = resList;
        this.magazineList = magazines;
        this.converterList = converters;
    }

    public List<ShipResource> GetResourceList()
    {
        return resourceList;
    }

    public List<ShipConverter> GetConverterList()
    {
        return converterList;
    }

    internal ShipResource GetResource(string resourceName)
    {
        if (resourceDictionary.ContainsKey(resourceName))
        {
            return resourceDictionary[resourceName];
        }
        return null;
    }

    internal List<ShipMagazine> GetMagazineList()
    {
        return magazineList;
    }

    internal ShipMagazine GetCompatibleMagazine(string[] itemCodes)
    {

        for (int i = 0; i < magazineList.Count; i++)
        {

            if (ContainedString(itemCodes, magazineList[i].containedItem) && 
                magazineList[i].amount> 0) {
                return magazineList[i];
            }
        }
        return null;
    }

    internal ShipMagazine GetMagazine(string ammoCode)
    {
        for (int i = 0; i < magazineList.Count; i++)
        {
            if (ammoCode.Equals(magazineList[i].containedItem) && magazineList[i].amount>0)
            {
                return magazineList[i];
            }
        }
        return null;
    }

    private bool ContainedString(string[] itemCodes, string containedItem)
    {
        for (int i = 0; i < itemCodes.Length; i++)
        {
            if (itemCodes[i].Equals(containedItem))
            {
                return true;
            }
        }
        return false;
    }
}
