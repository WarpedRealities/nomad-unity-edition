﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SpaceshipStats 
{
    float fuelCost;
    float baseCruise;
    float baseThrust;
    float manouver;
    int hullPoints;
    int baseDetection;
    int armour;
    private int width, height;


    public SpaceshipStats(int hp, int width, int height, float cruise, float thrust, float fuelEfficiency, float manouver, int detection, int armour)
    {
        this.hullPoints = hp;
        this.width = width;
        this.height = height;
        this.baseCruise = cruise;
        this.baseThrust = thrust;
        this.fuelCost = fuelEfficiency;
        this.manouver = manouver;
        this.baseDetection = detection;
    }


    public int GetWidth()
    {
        return width;
    }
    public int GetHeight()
    {
        return height;
    }

    internal int GetHP()
    {
        return hullPoints;
    }

    internal float GetFuelCost()
    {
        return fuelCost;
    }

    internal float GetCruise()
    {
        return baseCruise;
    }

    internal int GetDetection()
    {
        return baseDetection;
    }

    public float GetThrust()
    {
        return baseThrust;
    }
    public float GetManouver()
    {
        return manouver;
    } 
    public float GetArmour()
    {
        return armour;
    }

    internal void ModifyHP(int mod)
    {
        this.hullPoints += mod;
    }
}
