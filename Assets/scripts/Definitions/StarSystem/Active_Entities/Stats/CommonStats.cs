﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CommonStats 
{
    private int HP, HPMAX;

    public CommonStats(int HPMAX, int HP)
    {
        this.HPMAX = HPMAX;
        this.HP = HP;
    }

    public int GetHp()
    {
        return HP;
    }

    public void ModHP(int hp)
    {
        this.HP += hp;
    }

    public int GetMaxHp()
    {
        return HPMAX;
    }
}
