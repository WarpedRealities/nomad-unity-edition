﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CombatStats 
{
    private float combatSpeed;
    private float manouverability;
    private float countermeasures;
    private float armour;
    private int piloting, gunnery, tech, science;
    private List<WeaponSystem> weapons;


    public CombatStats(float speed, float manouver, float countermeasures, float armour, int pilot, int gun, int tech, int sci, List<WeaponSystem> weapons)
    {
        this.combatSpeed = speed;
        this.manouverability = manouver;
        this.countermeasures = countermeasures;
        this.armour = armour;
        this.piloting = pilot;
        this.gunnery = gun;
        this.tech = tech;
        this.science = sci;
        this.weapons = weapons;
    }

    public float GetDefence()
    {
        return manouverability + countermeasures + piloting;
    }

    public float GetDynamicDefence(float thrust)
    {
        return ((manouverability + piloting) * thrust)+countermeasures;
    }

    internal void SetSkills(int piloting, int gunnery, int tech, int science)
    {
        this.piloting = piloting;
        this.gunnery = gunnery;
        this.tech = tech;
        this.science = science;
    }

    public int GetGunnery()
    {
        return gunnery;
    }

    public List<WeaponSystem> GetWeaponSystems()
    {
        return weapons;
    }

    internal float GetSpeed()
    {
        return combatSpeed;
    }

    public float GetManouver()
    {
        return manouverability+piloting;
    }

    public float GetArmour()
    {
        return armour;
    }

    public float GetCounterMeasures()
    {
        return countermeasures;
    }

    public bool CheckWeaponUseable(WeaponSystem weapon, SpaceshipResourceStats resourceStats)
    {
        if (weapon.GetMagazine() == 0)
        {
            return false;
        }
        if (resourceStats == null)
        {
            return true;
        }
        for (int i = 0; i < weapon.GetWeapon().GetWeaponCosts().Length; i++)
        {
            WeaponCost weaponCost = weapon.GetWeapon().GetWeaponCosts()[i];
            if (weaponCost is ResourceCost)
            {
                ResourceCost resourceCost = (ResourceCost)weaponCost;
                ShipResource resource= resourceStats.GetResource(resourceCost.GetResource());
                if (resource==null || resourceCost.GetAmount()> resource.amount)
                {
                    return false;
                }
            }
            if (weaponCost is MagazineCost)
            {
                MagazineCost magazineCost = (MagazineCost)weaponCost;
                if (weapon.GetAmmoType() == null)
                {
                    return false;
                }
                ShipMagazine shipMagazine = resourceStats.GetMagazine(weapon.GetAmmoType().GetCode());

                if (shipMagazine==null|| magazineCost.GetAmount() > shipMagazine.amount)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public bool CheckWeaponUsable(int index, SpaceshipResourceStats resourceStats)
    {
        return CheckWeaponUseable(this.weapons[index], resourceStats);
    }
}
