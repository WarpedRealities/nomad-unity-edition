﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
//stats for being able to operate in solar view, need another for space combat and one for shared stats
public class SolarStats 
{
    private float cruiseSpeed;
    private float efficiency;
    private float sensors;
    private float detectionDifficulty;
    private int capacity;
    private float ftl;

    public SolarStats(float cruise, float efficiency, float sensors, float detectionDifficulty, int capacity, float ftl)
    {
        this.cruiseSpeed = cruise;
        this.efficiency = efficiency;
        this.sensors = sensors;
        this.detectionDifficulty = detectionDifficulty;
        this.capacity = capacity;
        this.ftl = ftl;
    }

    public float GetEfficiency()
    {
        return efficiency;
    }

    public float GetCruise()
    {
        return cruiseSpeed;
    }

    internal void ModSensors(int modValue)
    {
        this.sensors += modValue;
    }

    public float GetSensors()
    {
        return this.sensors;
    }
    public float GetDetection()
    {
        return this.detectionDifficulty;
    }

    internal int GetCapacity()
    {
        return capacity;
    }

    public float GetFTL()
    {
        return ftl;
    }
}
