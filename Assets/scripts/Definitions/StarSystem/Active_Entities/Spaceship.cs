﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices.WindowsRuntime;

[Serializable]
public class Spaceship : Active_Entity
{
    //no delayed content creation for spaceships
  
    private Zone shipInterior;

    private CombatStats combatStats;
    private SpaceshipResourceStats resourceStats;
    private SpaceshipStats spaceshipStats;
    private WarpHandler warpHandler;
    private ShipState shipState;
    private string filename;
    private ShipAnimation animation;

    public Spaceship(string name, string filename, string sprite, SpaceshipStats spaceshipStats, Zone zone)
    {
        this.name = name;
        this.filename = filename;
        this.sprite = sprite;
        this.spaceshipStats = spaceshipStats;
        this.shipInterior = zone;

    }

    public ShipState GetState()
    {
        return shipState;
    }

    public void SetState(ShipState state)
    {
        this.shipState = state;
    }

    public SpaceshipStats GetSpaceshipStats()
    {
        return spaceshipStats;
    }

    public override string getSprite()
    {
        return "ships/" + sprite;
    }

    public override ENTITYTYPE GetEntityType()
    {
        return ENTITYTYPE.SHIP;
    }

    public override LandingSite GetLandingSite(Zone zone)
    {
        throw new NotImplementedException();
    }

    public override Zone GetZone(string name)
    {
       if (shipInterior.getName().Equals(name))
        {
            return shipInterior;
        }
        return null;
    }

    internal string GetFile()
    {
        return filename;
    }

    public override Spaceship GetShip(string name)
    {
        return this;
    }

    public SpaceshipResourceStats GetResources()
    {
        return resourceStats;
    }

    public void SetResources(SpaceshipResourceStats resourceStats)
    {
        this.resourceStats = resourceStats;
    }

    public void SetCommons(CommonStats commonstats)
    {
        this.commonStats = commonstats;
    }

    public CombatStats GetCombatStats()
    {
        return combatStats;
    }


    public override LandingSite GetLandingSite(Spaceship ship)
    {
        throw new NotImplementedException();
    }

    public override void RemoveLandingSite(LandingSite landingSite)
    {
        throw new NotImplementedException();
    }

    public override Zone GetZone(int index)
    {
        return shipInterior;
    }

    internal override void PostLoad()
    {
        if (shipInterior.GetContents() != null)
        {
           shipInterior.GetContents().PostLoad();
        }
    }

    internal void SetCombatStats(CombatStats combatStats)
    {
        this.combatStats = combatStats;
    }

    internal void Interact(Spaceship playerShip, DelegateLogText logText, SolarSceneController solarSceneController)
    {

        if (playerShip == this)
        {
            return;
        }
        if (this.GetController() != null)
        {

            //need to run the get controller
            SolarScriptRunnerTool solarScriptRunnerTool = new SolarScriptRunnerTool(
                GlobalGameState.GetInstance().getCurrentSystem());
            solarScriptRunnerTool.RunScript(this.GetController().GetData().GetContactScript(), this);
        }
        else
        {
            LaunchLandTool landTool = new LaunchLandTool();
            ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(playerShip);
            landTool.Dock(shipAnalysisTool, this);
        }

    }

    public WarpHandler GetWarpHandler()
    {
        return warpHandler;
    }

    internal void SetWarpHandler(WarpHandler warp)
    {
        this.warpHandler = warp;
    }

    public override int GetDetection()
    {
        if (this.warpHandler != null && this.warpHandler.state==WarpState.CHARGING)
        {
            return (int)this.GetSolarStats().GetDetection() + 20;
        }
        if (this.GetSolarStats() != null)
        {
            return (int)this.GetSolarStats().GetDetection();
        }
        else
        {
            return this.GetSpaceshipStats().GetDetection();
        }

    }

    public ShipAnimation GetAnimation()
    {
        return this.animation;
    }

    public void SetAnimation(ShipAnimation animation)
    {
        this.animation = animation; 
    }
}
