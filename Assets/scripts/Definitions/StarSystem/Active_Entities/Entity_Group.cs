﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_Group : Active_Entity
{
    public List<Active_Entity> groupedEntities;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override Zone GetZone(string name)
    {
        return null;
    }

    public override ENTITYTYPE GetEntityType()
    {
        return ENTITYTYPE.GROUP;
    }

    public override LandingSite GetLandingSite(Zone zone)
    {
        throw new System.NotImplementedException();
    }

    public override Spaceship GetShip(string name)
    {
        throw new System.NotImplementedException();
    }

    public override LandingSite GetLandingSite(Spaceship ship)
    {
        throw new System.NotImplementedException();
    }

    public override void RemoveLandingSite(LandingSite landingSite)
    {
        throw new System.NotImplementedException();
    }

    public override Zone GetZone(int index)
    {
        throw new System.NotImplementedException();
    }
}
