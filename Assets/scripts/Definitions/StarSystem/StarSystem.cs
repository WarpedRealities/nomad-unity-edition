﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class StarSystem 
{
    private StarSystemContents systemContents;
    private Vector2Int position;
    private Vector2 viewPos;
    private string name;

    public StarSystem(Vector2Int position, Vector2 viewPos, string name)
    {
        this.position = position;
        this.viewPos = viewPos;
        this.name = name;
    }

    public Vector2Int GetPosition()
    {
        return position;
    }

    public Vector2 GetViewPosition()
    {
        return viewPos;
    }
    public string GetName()
    {
        return this.name;
    }

    public void setContents(StarSystemContents starSystemContents)
    {
        this.systemContents = starSystemContents;
    }

    public StarSystemContents GetSystemContents()
    {
        return systemContents;
    }

    public Entity getEntity(string name)
    {
        for (int i = 0; i < systemContents.GetEntities().Count; i++)
        {
            Entity entity = systemContents.GetEntities()[i];
            if (entity.getName()!=null && entity.getName().Equals(name))
            {
                return entity;
            }
        }
        return null;
    }

    internal void RunStartScripts(SolarScriptRunnerTool scriptRunnerTool)
    {
        
        for (int i = 0; i < systemContents.GetEntities().Count; i++)
        {
            if (systemContents.GetEntities()[i] is Active_Entity)
            {
                Active_Entity active_Entity = (Active_Entity)systemContents.GetEntities()[i];
                if (active_Entity.GetController() is NPC_Captain)
                {
                    if (active_Entity.GetController().GetControllable()==null)
                    {
                        if (active_Entity is Spaceship)
                        {
                            active_Entity.GetController().SetControllable(
                                new LuaSolarControllable((Spaceship)active_Entity));
                        }
                        if (active_Entity is Creature)
                        {
                            active_Entity.GetController().SetControllable(
                                new LuaSolarControllable((Creature)active_Entity));
                        }
                    }
                    string entryScript = active_Entity.GetController().GetData().GetEntryScript();
                    if (entryScript.Length > 3)
                    {
                        scriptRunnerTool.RunScript(entryScript, active_Entity);
                    }
                }
            }
        }
    }
}
