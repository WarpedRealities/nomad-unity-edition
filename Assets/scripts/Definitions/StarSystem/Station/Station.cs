﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Station : Entity
{
    int detection;
    bool detected;
    StationContents contents;
    protected bool isGenerated;

    public Station(Vector2Int position, string fname)
    {
        this.position = position;
        this.name = fname;
    }
    public override void generateForSolar()
    {
        StationTool contentTool = new StationTool();
        this.sprite = contentTool.generateSolar(name, out size, out detection);
        isGenerated = true;
    }

    public override string getSprite()
    {
        return "stations/" + this.sprite;
    }
    public override int GetDetection()
    {
        return detection;
    }

    internal override void SetDetected(bool isDetected)
    {
        if (isDetected)
        {
            detected = true;
        }
    }

    override public bool NeedsGeneration()
    {
        return !isGenerated;
    }

    public override bool isAlwaysVisible()
    {
        return detected;
    }

    public override void generate()
    {
        StationTool stationTool = new StationTool();
        contents= stationTool.GenerateContents(name, this);

    }

    internal override void PostLoad()
    {
        if (contents != null)
        {
            contents.PostLoad();
        }
    }

    public override Zone GetZone(string name)
    {

        return contents.getZone(name, this);
    }


    public override ENTITYTYPE GetEntityType()
    {
        return ENTITYTYPE.STATION;
    }

    public override LandingSite GetLandingSite(Zone zone)
    {   
        /*
        for (int i = 0; i < contents.GetLandingSites().Count; i++)
        {
            LandingSite landingSite = contents.GetLandingSites()[i];
            if (zone.getName().Equals(landingSite.GetZone()))
            {
                return landingSite;
            }
        }
        */
        return null;
    }

    public override LandingSite GetLandingSite(Spaceship ship)
    {
        for (int i = 0; i < contents.GetLandingSites().Count; i++)
        {
            LandingSite landingSite = contents.GetLandingSites()[i];
            if (ship.Equals(landingSite.GetSpaceship()))
            {
                return landingSite;
            }
        }
        return null;
    }

    public override Spaceship GetShip(string name)
    {
        for (int i = 0; i < contents.GetLandingSites().Count; i++)
        {
            if (contents.GetLandingSites()[i] != null &&
                contents.GetLandingSites()[i].GetSpaceship().getName().Equals(name))
            {
                return contents.GetLandingSites()[i].GetSpaceship();
            }
        }
        return null;
    }

    public override void RemoveLandingSite(LandingSite landingSite)
    {
        landingSite.SetSpaceship(null);
    }

    internal StationContents GetContents()
    {
        return this.contents;
    }

    internal LandingSite GetLandingSite(string shipName)
    {
       if (this.contents != null)
        {
            for (int i = 0; i < contents.GetLandingSites().Count; i++)
            {
                if (contents.GetLandingSites()[i].GetSpaceship().getName().Equals(shipName))
                {
                    return contents.GetLandingSites()[i];
                }
            }
        }
        return null;
    }
}
