﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class StationContents
{
    List<Zone> stationZones;
    List<LandingSite> landingSites;

    public void SetZones(List<Zone> zones)
    {
        this.stationZones = zones;
    }
    public void SetLandingSites(List<LandingSite> landingSites)
    {
        this.landingSites = landingSites;
    }

    internal List<LandingSite> GetLandingSites()
    {
        return this.landingSites;
    }

    internal Zone getZone(string name, Station station)
    {

        for (int i = 0; i < stationZones.Count; i++)
        {
            if (stationZones[i].getName().Equals(name))
            {
                if (stationZones[i].GetContents() == null)
                {
                    stationZones[i].SetEntity(station);
                }
                return stationZones[i];
            }
        }
        for (int i = 0; i < landingSites.Count; i++)
        {
            if (landingSites[i].GetSpaceship()!=null &&
                landingSites[i].GetSpaceship().GetZone(name)!=null)
            {
                return landingSites[i].GetSpaceship().GetZone(name);
            }
        }
        return null;
    }

    internal void PostLoad()
    {
        for (int i = 0; i < this.stationZones.Count; i++)
        {
            if (stationZones[i].IsGenerated() && stationZones[i].GetContents() != null)
            {
                stationZones[i].GetContents().PostLoad();
            }

        }
        for (int i = 0; i < this.landingSites.Count; i++)
        {
            if (landingSites[i] != null && landingSites[i].GetSpaceship() != null)
            {
                landingSites[i].GetSpaceship().PostLoad();
            }
        }
    }
}
