using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CargoEffect : SignatureEffect
{
    ItemList itemList;
    String description;
    public CargoEffect(ItemList itemList, String description)
    {
        this.itemList = itemList;
        this.description = description;
    }

    public override bool ApplyEffect(Spaceship playerShip, DelegateLogText spaceLog)
    {
        spaceLog(description);
        List<WidgetContainer> containers = CargoTool.ContainerScan(playerShip.GetZone(0));// new List<WidgetContainer>();
        if (containers.Count == 0)
        {
            spaceLog("No cargo capacity for retrieval");
            return false;
        }
        float freeAmount = CargoTool.CountSpace(containers);
        if (freeAmount< itemList.GetItem(0).GetDef().GetWeight())
        {
            spaceLog("No cargo capacity for retrieval");
            return false;
        }

        AddItems(containers, spaceLog, freeAmount);

        return true;
    }

    private void AddItems(List<WidgetContainer> containers, DelegateLogText spaceLog, float free)
    {
        Debug.Log("add items free is " + free);
        int count = itemList.GetItemCount();
        for (int i = 0; i < count; i++)
        {
            Item item = itemList.GetItem(i);
            if (item != null)
            {
                if (item is ItemStack)
                {
                    Debug.Log("stack");
                    free -= CargoTool.AddStack((ItemStack)item, containers, spaceLog);
                }
                else
                {
                    Debug.Log("item");
                    free -= CargoTool.AddItems(item, containers, spaceLog);
                }
                if (free <= 0)
                {
                    return;
                }
            }
        }
    }

}
