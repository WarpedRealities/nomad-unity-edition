﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

[Serializable]
public class MonsterEffect : SignatureEffect
{

    string[] monsterFiles;
    string description;
    public MonsterEffect(string description, string [] monsterFiles)
    {
        this.description = description;
        this.monsterFiles = monsterFiles;
    }



    public override bool ApplyEffect(Spaceship playerShip, DelegateLogText spaceLog)
    {
        PlaceMonsters(playerShip.GetZone(0));
        TextLog.Log(description);
        LaunchLandTool launchLandTool = new LaunchLandTool();
        ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(playerShip);
        shipAnalysisTool.SuppressReformers();
        launchLandTool.RelinquishControl(shipAnalysisTool);

        return true;

    }

    private void PlaceMonsters(Zone zone)
    {
        Vector2Int p = GetPortal(zone);
        Vector2Int[] positions = GetPositions(p,zone);
        for (int i = 0; i < monsterFiles.Length; i++)
        {
            if (positions[i].x > 0 || positions[i].y > 0)
            {
                Vector2Int placement = positions[i];
                XmlDocument doc = FileTools.GetXmlDocument("npcs/" + monsterFiles[i]);
                XmlElement rootXML = (XmlElement)doc.FirstChild.NextSibling;
                NPC npc = new NPC(rootXML, monsterFiles[i]);
                zone.GetContents().GetActors().Add(new NPC(npc, placement));
            }
        }
    }

    private Vector2Int[] GetPositions(Vector2Int p, Zone zone)
    {
        Vector2Int[] pos = new Vector2Int[9];
        pos[0] = p;
        int index = 0;
        for (int i = 0; i < 8; i++)
        {
            Vector2Int test = GeometryTools.GetPosition(p.x, p.y, i);
            Tile t = zone.GetContents().GetTile(test.x, test.y);
            if (t!=null && t.canWalk())
            {
                pos[index] = test;
                index++;
            }
        }
        return pos;
    }

    private Vector2Int GetPortal(Zone zone)
    {
        for (int i = 0; i < zone.GetContents().GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zone.GetContents().GetZoneParameters().GetHeight(); j++)
            {
                Tile t = zone.GetContents().GetTile(i, j);
                if (t!=null && t.GetWidget()!=null && t.GetWidget() is WidgetPortal)
                {
                    WidgetPortal portal = (WidgetPortal)t.GetWidget();
                    Vector2Int p= new Vector2Int(i, j);

                    return GeometryTools.GetPosition(p.x, p.y, portal.getFacing());
                }
            }
        }
        return new Vector2Int(zone.GetContents().GetZoneParameters().GetWidth()/2,zone.GetContents().GetZoneParameters().GetHeight()/2);
    }
}
