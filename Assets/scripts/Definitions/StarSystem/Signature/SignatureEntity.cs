using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SignatureEntity : Entity
{
    int detection;
    bool detected;

    SignatureEffect effect;

    public SignatureEntity(int detection, Vector2Int position, SignatureEffect effect)
    {
        this.effect = effect;
        this.position = position;
        this.detection = detection;
    }

    public override void generate()
    {
        throw new System.NotImplementedException();
    }

    public void Interact(Spaceship playerShip, DelegateLogText logText, SolarControl solarControl)
    {
        if (this.effect.ApplyEffect(playerShip, logText))
        {
            //destroy this entity
            GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Remove(this);
            solarControl.RefreshSprites();
        }
    }

    public override int GetDetection()
    {
        return detection;
    }

    public override ENTITYTYPE GetEntityType()
    {
        return ENTITYTYPE.SIGNATURE;
    }

    public override LandingSite GetLandingSite(Zone zone)
    {
        throw new System.NotImplementedException();
    }

    public override LandingSite GetLandingSite(Spaceship ship)
    {
        throw new System.NotImplementedException();
    }

    public override Spaceship GetShip(string name)
    {
        throw new System.NotImplementedException();
    }

    public override bool isAlwaysVisible()
    {
        return detected;
    }

    public override void RemoveLandingSite(LandingSite landingSite)
    {
        throw new System.NotImplementedException();
    }

}
