﻿using UnityEngine;
using System.Collections;

public interface MutatorEffect
{
    void ApplyEffect(Player player, ViewInterface view);
}
