﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutatorAddPartEffector : MutatorEffect
{
    private bool remove;
    private string part;
    private string effectString;

    public MutatorAddPartEffector(string part, bool remove, string effectString)
    {
        this.part = part;
        this.remove = remove;
        this.effectString = effectString;
    }

    public void ApplyEffect(Player player, ViewInterface view)
    {
        if (remove)
        {
            player.GetAppearance().RemovePart(part);
        }
        else
        {
            player.GetAppearance().AddPart(BodyPartLoader.LoadPart(part));
        }

    }
}
