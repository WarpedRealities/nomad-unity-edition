﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutatorModifierEffect : MutatorEffect
{
    private const int INCREASE = 1;
    private const int SET = 0;
    private const int DECREASE = -1;

    private int value, mode;
    private string part, variable,effectString;


    public MutatorModifierEffect(string part, string variable,string effectString, int value, int mode)
    {
        this.effectString = effectString;
        this.part = part;
        this.variable = variable;
        this.value = value;
        this.mode = mode;
    }

    public void ApplyEffect(Player player, ViewInterface view)
    {
        int initialValue = player.GetAppearance().GetPart(part).GetValue(variable);
        if (mode == 0)
        {
            player.GetAppearance().GetPart(part).SetValue(variable,value);
        }
        if (mode == 1)
        {
            player.GetAppearance().GetPart(part).SetValue(variable, initialValue + value);
        }
        if (mode == -1)
        {
            player.GetAppearance().GetPart(part).SetValue(variable, initialValue - value);
        }
        if (view!=null && effectString != null)
        {

        }
    }
}
