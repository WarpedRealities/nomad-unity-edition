﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class MutatorBuilder 
{
  
    static public Mutator BuildMutator(XmlElement root)
    {
        int effectCount = int.Parse(root.GetAttribute("effectCount"));
        int conditionCount = 0;
        int.TryParse(root.GetAttribute("conditionCount"),out conditionCount);
        MutatorEffect[] effects=new MutatorEffect[effectCount];
        MutatorCondition[] conditions = conditionCount>0 ? new MutatorCondition[conditionCount]: null;

        int effectIndex = 0, conditionIndex = 0;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)root.ChildNodes[i];
                if ("valueCondition".Equals(xmlElement.Name))
                {
                    conditions[conditionIndex] = BuildValueCondition(xmlElement);
                    conditionIndex++;
                }
                if ("partCondition".Equals(xmlElement.Name))
                {
                    conditions[conditionIndex] = BuildPartCondition(xmlElement);
                    conditionIndex++;
                }
                if ("partEffect".Equals(xmlElement.Name))
                {
                    effects[effectIndex] = BuildPartEffect(xmlElement);
                    effectIndex++;
                }
                if ("modifierEffect".Equals(xmlElement.Name))
                {
                    effects[effectIndex] = BuildModifierEffect(xmlElement);
                    effectIndex++;
                }
            }

        }

        return new Mutator(conditions, effects);
    }

    private static MutatorEffect BuildModifierEffect(XmlElement xmlElement)
    {
        int effectMode = ParseModeEffect(xmlElement.GetAttribute("mode"));
        int value = int.Parse(xmlElement.GetAttribute("value"));
        string part = xmlElement.GetAttribute("part");
        string variable = xmlElement.GetAttribute("variable");
        string effectText = xmlElement.InnerText;
        return new MutatorModifierEffect(part,variable,effectText,value,effectMode);
    }

    private static int ParseModeEffect(string text)
    {
        if ("add".Equals(text))
        {
            return 1;
        }
        if ("subtract".Equals(text))
        {
            return -1;
        }
        if ("set".Equals(text))
        {
            return 0;
        }
        return 0;
    }

    private static MutatorEffect BuildPartEffect(XmlElement xmlElement)
    {
        string part = xmlElement.GetAttribute("part");
        bool remove = "remove".Equals(xmlElement.GetAttribute("change"));
        string effectString = xmlElement.InnerText;
        return new MutatorAddPartEffector(part, remove, effectString);
    }

    private static MutatorCondition BuildPartCondition(XmlElement xmlElement)
    {
        throw new NotImplementedException();
    }

    private static MutatorCondition BuildValueCondition(XmlElement xmlElement)
    {
        throw new NotImplementedException();
    }
}
