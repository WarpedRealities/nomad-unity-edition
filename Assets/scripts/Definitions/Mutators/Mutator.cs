﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mutator
{
    private MutatorCondition[] conditions;
    private MutatorEffect[] effects;

    public Mutator(MutatorCondition[] conditions, MutatorEffect[] effects)
    {
        this.conditions = conditions;
        this.effects = effects;
    }

    public void ApplyEffect(Player player, ViewInterface view)
    {
        //later apply the conditionsl


        for (int i = 0; i < effects.Length; i++)
        {
            effects[i].ApplyEffect(player,view);
        }
    }

}
