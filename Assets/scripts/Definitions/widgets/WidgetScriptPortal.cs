﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetScriptPortal : WidgetPortal
{
    bool triggered=false;
    string scriptFile;

    public WidgetScriptPortal(string destination, int id, int facing, string scriptFile) : base(destination, id, facing)
    {
        this.scriptFile = scriptFile;
    }

    public override void Step(ViewInterface viewInterface)
    {
        Debug.Log("step destination " + destination);
        if (!triggered)
        {
            GenericScriptRunner.GetInstance().RunScript(scriptFile, viewInterface);
            triggered = true;
        }
        //run script first time
        viewInterface.ZoneTransition(destination, identifier);
    }
}
