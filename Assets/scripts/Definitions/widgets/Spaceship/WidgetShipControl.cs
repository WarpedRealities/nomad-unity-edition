﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetShipControl : Widget_Breakable
{

    public WidgetShipControl(int sprite, string description, string name, BreakableData breakableData) : base(sprite, description, name, breakableData,true)
    {

    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        //run the fast forward tool!
        ShipSystemFastForwardTool shipSystemFastForwardTool = new ShipSystemFastForwardTool(null, GlobalGameState.GetInstance().getCurrentZone());
        shipSystemFastForwardTool.RunFastForward(GlobalGameState.GetInstance().GetUniverse().GetClock());
        //then start a screen
        viewInterface.SetScreen(ScreenID.SPACESHIP);
        return false;
    }

    public override bool CanInteract()
    {
        return true;
    }

}
