﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class RepairRequirement
{
    int count;
    string itemCode;
    public RepairRequirement(int count, string itemCode)
    {
        this.count = count;
        this.itemCode = itemCode;
    }

    public int GetCount()
    {
        return count;
    }
    public string GetCode()
    {
        return itemCode;
    }
}


[Serializable]
public class WidgetShipSystem : Widget_Breakable
{
    float[] color = new float[] { 1, 0.5F, 0.5F, 1 };
    long lastInteraction;
    ShipSystem[] systems;
    bool damaged=false;
    RepairRequirement repairRequirement;
    public WidgetShipSystem(int sprite, string description, string name, BreakableData breakableData, ShipSystem []systems,RepairRequirement repair) : base(sprite, description, name, breakableData,true)
    {
        this.repairRequirement = repair;
        this.systems = systems;
        lastInteraction = GlobalGameState.GetInstance().GetUniverse().GetClock();
    }

    public bool IsDamaged()
    {
        return damaged;
    }
    public void SetDamaged(bool value)
    {
        this.damaged = value;
    }
    public long GetLastInteraction()
    {
        return lastInteraction;
    }

    public void SetLastInteraction(long clock)
    {
        lastInteraction = clock;
    }
    public RepairRequirement GetRequirement()
    {
        return this.repairRequirement;
    }
    public ShipSystem [] GetSystems()
    {
        return systems;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        if (damaged)
        {
            viewInterface.SetScreen(new ScreenDataShipSystem(this, player));
        }
        else
        {
            //run the fast forward tool!
            ShipSystemFastForwardTool shipSystemFastForwardTool = new ShipSystemFastForwardTool(this, GlobalGameState.GetInstance().getCurrentZone());
            shipSystemFastForwardTool.RunFastForward(GlobalGameState.GetInstance().GetUniverse().GetClock());
            //then start a screen
            viewInterface.SetScreen(new ScreenDataShipSystem(this, player));
        }
        return false;
    }

    public override bool CanInteract()
    {
        return true;
    }

    public void RunSystems(long duration, Dictionary<string, ShipResource> resources)
    {
       for (int i = 0; i < systems.Length; i++)
        {
            systems[i].Run(duration, resources);
        }
    }

    public override bool IsColoured()
    {   
        return damaged;
    }
    public override float[] GetColour()
    {
        return color;
    }
}

