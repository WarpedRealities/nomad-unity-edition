﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetSlot : Widget
{
    SLOTTYPE slotType;
    MountParameters mountParameters;
    Widget_Breakable containedWidget;
    string componentID;

    public WidgetSlot(SLOTTYPE slotType, MountParameters mountParameters)
    {
      
        this.mountParameters=mountParameters;
        this.canWalk = false;
        this.slotType = slotType;
        this.widgetSprite = 11 + (int)slotType;
        if (slotType == SLOTTYPE.WEAPON && mountParameters != null)
        {
            widgetSprite += (int)mountParameters.GetWeaponSize();
        }
        
    }

    public Widget_Breakable GetContainedWidget()
    {
        return containedWidget;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        if (containedWidget != null)
        {
            return containedWidget.Interact(viewInterface, player);
        }
        else
        {
            viewInterface.SetScreen(new ScreenDataSlot(this));
            return true;
        }
    }

    public SLOTTYPE GetSlotType()
    {
        return slotType;
    }

    public override string getDescription()
    {
        if (containedWidget != null)
        {
            if (mountParameters != null)
            {
                return containedWidget.getDescription()+" facing:"+
                    BuildFacing(mountParameters.GetFacing())+
                    " with "+BuildFiringArc(mountParameters.GetArc())+" arc";
            }
            else
            {
                return containedWidget.getDescription();
            }

        }
        return BuildDescription();
    }

    static public string BuildFiringArc(FIRINGARC firingArc)
    {
        switch (firingArc)
        {
            case FIRINGARC.NARROW:
                return "narrow";
            case FIRINGARC.NORMAL:
                return "normal";
            case FIRINGARC.WIDE:
                return "wide";
            case FIRINGARC.CIRCLE:
                return "circle";
        }
        return "undefined";
    }

    internal void AddComponent(ItemComponent item)
    {
        //build widget
        Widget widget = WidgetBuilder.BuildWidget(item.GetComponent());
        //place widget in slot
        containedWidget = (Widget_Breakable)widget;
        if (widget is WidgetShipSystem)
        {
            //need to run the fastforward tool on the zone
            ShipSystemFastForwardTool shipSystemFastForwardTool = new ShipSystemFastForwardTool((WidgetShipSystem)widget, GlobalGameState.GetInstance().getCurrentZone());
            shipSystemFastForwardTool.RunFastForward(GlobalGameState.GetInstance().GetUniverse().GetClock());
            SetupSystem((WidgetShipSystem)widget);
        }

        componentID = item.GetCodeName();
    }

    void SetupSystem(WidgetShipSystem widgetShipSystem)
    {
        for (int i = 0; i < widgetShipSystem.GetSystems().Length; i++)
        {
            if (widgetShipSystem.GetSystems()[i].GetSystemType() == SHIPSYSTEMTYPE.WEAPON)
            {
                WeaponSystem weaponSystem = (WeaponSystem)widgetShipSystem.GetSystems()[i];
                CommonWeapon weapon = weaponSystem.GetWeapon();
                weapon.Setup(weaponSystem.GetArc(), mountParameters.GetArc(), mountParameters.GetFacing(), mountParameters.GetEmitter());
            }
        }
    }

    internal void SetContainedWidget(Widget_Breakable widget)
    {
        containedWidget = widget;
        if (widget is WidgetShipSystem)
        {
            SetupSystem((WidgetShipSystem)widget);
        }
    }

    public string GetComponent()
    {
        return componentID;
    }

    public void SetComponent(string componentID)
    {
        this.componentID = componentID;
    }

    public override int getSprite()
    {
        if (containedWidget != null)
        {
            return containedWidget.getSprite();
        }
        return widgetSprite;
    }

    public MountParameters GetParams()
    {
        return mountParameters;
    }

    static public string BuildFacing(int facing)
    {
        switch (facing)
        {
            case 0:
                return "Fore";
            case 1:
                return "Fore Starboard";
            case 2:
                return "Starboard";
            case 3:
                return "Starboard Stern";
            case 4:
                return "Stern";
            case 5:
                return "Port Stern";
            case 6:
                return "Port";
            case 7:
                return "Port Fore";
        }
        return "undefined";
    }

    private string BuildDescription()
    {
        switch (slotType)
        {
            case SLOTTYPE.NORMAL:
                return "module slot";
            case SLOTTYPE.DRIVE:
                return "drive slot";
            case SLOTTYPE.SUPPORT:
                return "support slot";
            case SLOTTYPE.WEAPON:
                return BuildWeaponDescription();
        }
        return "";
    }

    private string BuildWeaponDescription()
    {
        switch (mountParameters.GetWeaponSize())
        {
            case SHIPWEAPONSIZE.LIGHT:
                return "light weapon mount facing " + BuildFacing(mountParameters.GetFacing())+ 
                    " with a "+ BuildFiringArc(mountParameters.GetArc()) +" arc";
            case SHIPWEAPONSIZE.MEDIUM:
                return "medium weapon mount facing " + BuildFacing(mountParameters.GetFacing())+
                                        " with a " + BuildFiringArc(mountParameters.GetArc()) + " arc";
            case SHIPWEAPONSIZE.HEAVY:
                return "heavy weapon mount facing " + BuildFacing(mountParameters.GetFacing())+
                                        " with a " + BuildFiringArc(mountParameters.GetArc()) + " arc";
        }
        return "";
    }

    public override Widget Clone()
    {
        throw new System.NotImplementedException();
    }

    public override void Regeneration(long clock)
    {

    }

    public override void Step(ViewInterface viewInterface)
    {
  
    }

    public override bool IsColoured()
    {
        if (containedWidget != null)
        {
            return containedWidget.IsColoured();
        }
        return false;
    }
    public override float[] GetColour()
    {
        if (containedWidget != null)
        {
            return containedWidget.GetColour();
        }
        return null;
    }
    public override bool CanInteract()
    {
        return true;
    }
}
