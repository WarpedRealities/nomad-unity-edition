﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class WidgetDamage : Widget
{
    private const string DESCRIPTION = " points of spacecraft hull damage";
    private const int SPRITENUM = 22;
    int damage;

    public WidgetDamage(int damage)
    {
        this.widgetSprite = SPRITENUM;
        this.damage = damage;
        this.canWalk = true;
    }

    public int GetDamage()
    {
        return damage;
    }

    public override Widget Clone()
    {
        return new WidgetDamage(damage);
    }

    public override void Regeneration(long clock)
    {
        
    }

    public override void Step(ViewInterface viewInterface)
    {

    }
    public override bool CanInteract()
    {
        return true;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        if (player.GetInventory().CountItem("00SCRAP") >= 2)
        {
            ExtractItems(player);
            Repair(viewInterface,player);
            return true;
        }
        else
        {
            viewInterface.DrawText("You do not have enough scrap to make repairs to this damage");
            return true;
        }

    }

    private void Repair(ViewInterface viewInterface, Player player)
    {
        int repair = DiceRoller.GetInstance().RollDice(6)+ player.GetRPG().getSkill(SK.TECH);
        damage -= repair;
        viewInterface.DrawText("You have repaired " + repair + " points of damage");
        if (damage <= 0)
        {
            viewInterface.RemoveWidget(this);
        }
    }

    private void ExtractItems(Player player)
    {
        int amount = 2;
        for (int i = 0; i < player.GetInventory().GetItemCount(); i++)
        {
            Item item = player.GetInventory().GetItem(i);
            if (item.GetDef().GetCodeName().Equals("00SCRAP"))
            {
                player.GetInventory().RemoveItem(item);
                amount--;
                if (amount <= 0)
                {
                    return;
                }
            }
        }
    }

    public override string getDescription()
    {
        return damage+DESCRIPTION;
    }

    internal void SetDamage(int v)
    {
        damage = v;
    }
}
