﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MountParameters 
{
    SHIPWEAPONSIZE weaponSize;
    FIRINGARC firingArc;
    int facing;
    Vector2FS emitter;

    public MountParameters(SHIPWEAPONSIZE weaponSize, FIRINGARC firingArc, int facing, Vector2FS emitter)
    {
        this.facing = facing;
        this.firingArc = firingArc;
        this.weaponSize = weaponSize;
        this.emitter = emitter;
    }

    public int GetFacing()
    {
        return facing;
    }

    public SHIPWEAPONSIZE GetWeaponSize()
    {
        return weaponSize;
    }
    public FIRINGARC GetArc()
    {
        return firingArc;
    }

    public Vector2FS GetEmitter()
    {
        return emitter;
    }
}
