﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class ResourceSystem : ShipSystem
{
    float amount;
    int capacity;
    string contentID, displayName;

    public ResourceSystem(string contentID,string displayName, int capacity)
    {
        systemType = SHIPSYSTEMTYPE.RESOURCE;
        this.contentID = contentID;
        this.displayName = displayName;
        this.capacity = capacity;
    }

    public float GetAmount()
    {
        return amount;
    }

    public void SetAmount(float amount)
    {
        this.amount = amount;
    }

    public int GetCapacity()
    {
        return capacity;
    }

    internal string GetDisplay()
    {
        return displayName;
    }

    public string GetContentId()
    {
        return contentID;
    }

    public override string  ToString()
    {
        return "resource system containing resource type " + contentID+" amount:"+amount+"/"+capacity;
    }

    public override void Run(long duration, Dictionary<string, ShipResource> resources)
    {
  
    }

    public override bool AddItem(Item item)
    {
        ResourceConversion resourceConversion = GlobalGameState.GetInstance().GetUniverse().GetResourceConversionLibrary().GetConversion(contentID);
        if (resourceConversion == null)
        {
            return false;
        }
        float value = resourceConversion.GetConversion(item.GetDef().GetCodeName());
        if (value > 0 && capacity-amount >= value)
        {
            this.amount += value;
            return true;
        }
        return false;
    }
}
