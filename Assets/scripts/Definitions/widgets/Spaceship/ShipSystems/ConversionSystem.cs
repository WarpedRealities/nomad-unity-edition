﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class ConversionSystem : ShipSystem
{
    float conversionRate;
    float conversionRatio;
    string resourceTo;
    string resourceFrom;
    string description;
    bool active;

    public ConversionSystem(float conversionRate, float conversionRatio, string resourceTo, string resourceFrom, string desc)
    {
        active = true;
        this.description = desc.Replace("\n","");
        this.resourceFrom = resourceFrom;
        this.resourceTo = resourceTo;
        this.conversionRate = conversionRate;
        this.conversionRatio = conversionRatio;
        systemType = SHIPSYSTEMTYPE.CONVERTER;
    }

    public float GetRate()
    {
        return conversionRate;
    }

    public float GetRatio()
    {
        return conversionRatio;
    }

    public string GetFrom()
    {
        return resourceFrom;
    }

    public string GetTo()
    {
        return resourceTo;
    }

    internal string GetDescription()
    {
        return this.description;
    }

    public bool IsActive()
    {
        return active;
    }

    public void SetActive(bool active)
    {
        this.active = active;
    }

    public override bool AddItem(Item item)
    {
        return false;
    }

    private ShipResource GetFrom(string from, Dictionary<string, ShipResource> resources)
    {
        if ("RES_NOTHING".Equals(from))
        {
            ShipResource shipResource = new ShipResource();
            shipResource.amount = 999;
            shipResource.capacity = 999;
            shipResource.resource = "RES_NOTHING";
            shipResource.originalAmount = 999;
            shipResource.displayName = "nothing";
            return shipResource;
        }

        return resources[this.resourceFrom];
    }

    public override void Run(long duration, Dictionary<string, ShipResource> resources)
    {
        if (active)
        {
            ShipResource from = GetFrom(this.resourceFrom, resources);
            ShipResource to = resources[this.resourceTo];

            //calc time to maximum to resource
            long timeFull = FullTime(to);
            //calc time to run out of from resource
            long timeDepletion = DepletionTime(from);
            //pick the shortest time
            long actualDuration = duration;
            if (timeFull < actualDuration)
                actualDuration = timeFull;
            if (timeDepletion < actualDuration)
                actualDuration = timeDepletion;
            //run
            from.amount -= this.conversionRate * actualDuration;
            to.amount += this.conversionRate * actualDuration * this.conversionRatio;
        }
   
    }

    private long DepletionTime(ShipResource from)
    {
        return (long) (from.amount / conversionRate);
    }

    private long FullTime(ShipResource to)
    {
        return (long)((to.capacity - to.amount) / (conversionRate * conversionRatio));
    }
}
