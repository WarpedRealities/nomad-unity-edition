﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public abstract class ShipSystem 
{
    protected SHIPSYSTEMTYPE systemType;



    public SHIPSYSTEMTYPE GetSystemType()
    {
        return systemType;
    }

    public abstract void Run(long duration, Dictionary<string, ShipResource> resources);

    public abstract bool AddItem(Item item); 
}
