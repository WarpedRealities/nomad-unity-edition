﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class WeaponSystem : ShipSystem
{
    CommonWeapon weapon;
    int facing;
    FIRINGARC arc;
    string name;
    Accept ammoType;
    int magazine, reload;

    public WeaponSystem(string name, FIRINGARC arc, CommonWeapon weapon)
    {
        this.systemType = SHIPSYSTEMTYPE.WEAPON;
        this.arc = arc;
        this.name = name;
        this.weapon = weapon;
    }


    public void InitializeMagazine()
    {
        magazine = weapon.GetMagazine();
        reload = weapon.GetReload();
    }

    public int GetFacing()
    {
        return this.facing;
    }

    public string GetName()
    {
        return this.name;
    }

    public int GetMagazine()
    {
        return magazine;
    }

    public int GetReload()
    {
        return reload;
    }

    public CommonWeapon GetWeapon()
    {
        return weapon;
    }

    public Accept GetAmmoType()
    {
        return this.ammoType;
    }

    public void SetammoType(Accept ammoType)
    {
        this.ammoType = ammoType;
    }

    internal void InitializeAmmoType(BattlerData battlerData)
    {
        //if there's no magazine costs..then ignore this
        if (!weapon.HasMagazineCosts())
        {

            return;
        }
        //if there's an existing one, check if ammo exists of that kind in a magazine
        Accept accept = this.ammoType;
        SpaceshipResourceStats spaceshipResourceStats = ((Spaceship)battlerData.GetActive_Entity()).GetResources();
        if (accept != null)
        {
            ShipMagazine shipMagazine = spaceshipResourceStats.GetMagazine(accept.GetCode());
            if (shipMagazine != null)
            {
     
                return;
            }
            else
            {

                accept = null;
            }
        }
        //if there's no ammo of that kind, clear and go onwards
        MagazineCost magazineCost = weapon.GetMagazineCost();
        //pick first magazine that's compatible and full, set the string to that
        for (int i = 0; i < magazineCost.GetAccept().Length; i++)
        {
            ShipMagazine shipMagazine = spaceshipResourceStats.GetMagazine(magazineCost.GetAccept()[i].GetCode());
            if (shipMagazine!=null)
            {
                accept = magazineCost.GetAccept()[i];
                ammoType = accept;
                return;
            }

        }
    }

    public override bool AddItem(Item item)
    {
        return false;
    }

    public override void Run(long duration, Dictionary<string, ShipResource> resources)
    {

    }

    internal FIRINGARC GetArc()
    {
        return arc;
    }

    internal void HandleReload()
    {
        reload--;
        if (reload <= 0)
        {
            magazine = weapon.GetMagazine();
            reload = weapon.GetReload();
        }
    }

    internal string GetString()
    {
        if (ammoType != null)
        {
            return GetName()+ " " + ammoType.GetDisplayName();
        }
        else
        {
            return GetName();
        }
    }

    internal void UseMagazine(int value)
    {
       this.magazine-=value;
    }
}
