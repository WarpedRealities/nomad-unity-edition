﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class MagazineSystem : ShipSystem
{

    int capacity, amount;
    string itemCode;
    string[] acceptCodes;

    public MagazineSystem(int capacity, string[] acceptCodes)
    {
        this.capacity = capacity;
        this.acceptCodes = acceptCodes;
        systemType = SHIPSYSTEMTYPE.MAGAZINE;
    }
    public MagazineSystem(int capacity, string[] acceptCodes, string itemCode, int amount)
    {
        this.capacity = capacity;
        this.acceptCodes = acceptCodes;
        this.amount = amount;
        this.itemCode = itemCode;
        systemType = SHIPSYSTEMTYPE.MAGAZINE;
    }

    public string GetAmmo()
    {
        if (itemCode == null)
        {
            return "empty";
        }
        return GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(itemCode).GetName();
    }

    public string GetAmmoCode()
    {
        return itemCode;
    }

    internal Item TakeItem()
    {
        if (amount > 0)
        {
            amount--;
            Item item = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(itemCode);
            if (amount == 0)
            {
                this.itemCode = null;
            }
            return item;
        }
        return null;
    }

    internal void ClearCode()
    {
        if (amount == 0)
        {
            itemCode = null;
        }
    }

    internal Item TakeAll()
    {
        if (amount > 0)
        {
            int c = amount;
            amount = 0;
            return new ItemStack(GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(itemCode), c);
        }
        return null;
    }

    public int GetAmount()
    {
        return amount;
    }
    public int GetCapacity()
    {
        return capacity;
    }

    public void SetCode(string itemCode)
    {
        this.itemCode = itemCode;
    }
    public void SetAmount(int amount)
    {
        this.amount = amount;
        if (this.amount == 0)
        {
            itemCode = null;
        }
    }

    public override bool AddItem(Item item)
    {
        if (this.amount == 0)
        {
            if (AcceptCheck(item.GetDef().GetCodeName())) {
                this.itemCode = item.GetDef().GetCodeName();
                this.amount++;
                return true;
            }
        }
        else
        {
            if (item.GetDef().GetCodeName().Equals(itemCode) && this.amount < this.capacity)
            {
                this.amount++;
                return true;
            }
        }
        return false;
    }

    private bool AcceptCheck(string code)
    {
        for (int i = 0; i < acceptCodes.Length; i++)
        {
            if (acceptCodes[i].Equals(code))
            {
                return true;
            }
        }
        return false;
    }

    public override void Run(long duration, Dictionary<string, ShipResource> resources)
    {

    }
}
