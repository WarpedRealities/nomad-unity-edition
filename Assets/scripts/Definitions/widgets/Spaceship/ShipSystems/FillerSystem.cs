﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class FillerSystem : ShipSystem
{
    string resourceID, displayName;
    float resourceRatio, fillRate;
    int itemCount;
    Item[] items;

    public FillerSystem(string resourceID, string displayName, float resourceRatio, float fillRate, int itemCount)
    {
        
        systemType = SHIPSYSTEMTYPE.FILLER;
        this.displayName = displayName;
        this.resourceID = resourceID;
        this.fillRate = fillRate;
        this.resourceRatio = resourceRatio;
        this.itemCount = itemCount;
        items = new Item[this.itemCount];
    }

    public string GetResource()
    {
        return resourceID;
    }

    public float GetRatio()
    {
        return resourceRatio;
    }

    public float GetRate()
    {
        return fillRate;
    }

    public int GetItemCount()
    {
        return itemCount;
    }

    public Item [] GetItems()
    {
        return items;
    }

    public override void Run(long duration, Dictionary<string, ShipResource> resources)
    {

        for (int i = 0; i < items.Length; i++)
        {
            //are we filling anything?
            if (items[i] != null)
            {
                //does it need filling
                ResourceStoreInstance resourceStore = GetResourceStore(items[i]);
                if (resourceStore != null && 
                    resourceStore.GetAmount()<resourceStore.GetDef().GetCapacity() &&
                    resourceStore.GetDef().GetRefillDef()!=null &&
                    resourceStore.GetDef().GetRefillDef().isFiller() &&
                    resourceStore.GetDef().GetRefillDef().GetFillRequirement().Equals(resourceID))
                {
                    ChargeItem(resourceStore, duration, resources);
                }
            }
        }

    }

    private void ChargeItem(ResourceStoreInstance resourceStore, long clock, Dictionary<string, ShipResource> resources)
    {
        float replenish = resourceStore.GetDef().GetCapacity() - resourceStore.GetAmount();
        float resource = resources[resourceStore.GetDef().GetRefillDef().GetFillRequirement()].amount;
        float timeToFill=replenish/fillRate;
        float timeToDeplete=resource/(fillRate/resourceRatio);

        if (timeToFill < clock && timeToFill<=timeToDeplete)
        {
            ChargeProcess(timeToFill, resources[resourceStore.GetDef().GetRefillDef().GetFillRequirement()], resourceStore);
        }
        else if (timeToDeplete < clock && timeToDeplete<= timeToFill)
        {
            ChargeProcess(timeToDeplete, resources[resourceStore.GetDef().GetRefillDef().GetFillRequirement()], resourceStore);
        }
        else
        {
            ChargeProcess(clock, resources[resourceStore.GetDef().GetRefillDef().GetFillRequirement()], resourceStore);
        }

    }

    private void ChargeProcess(float clock, ShipResource shipResource, ResourceStoreInstance resourceStore)
    {
        float chargeAmount = clock * fillRate;
        float resourceCost = clock * (fillRate / resourceRatio);
        resourceStore.SetAmount(resourceStore.GetAmount() + chargeAmount);
        shipResource.amount -= resourceCost;

    }

    private ResourceStoreInstance GetResourceStore(Item item)
    {
        if (item.GetDef().GetItemType() == ItemType.EQUIP)
        {
            EquipInstance equipInstance = (EquipInstance)item;
            return equipInstance.GetResourceStore();
        }
        if (item.GetDef().GetItemType() == ItemType.AMMO)
        {
            AmmoInstance ammoInstance = (AmmoInstance)item;
            return ammoInstance.GetResource();
        }
        return null;
    }

    public override bool AddItem(Item item)
    {
        if (item.GetDef().GetItemType() == ItemType.AMMO && item is AmmoInstance)
        {
            AmmoInstance ammoInstance = (AmmoInstance)item;
            if (ammoInstance.GetResource() != null &&
                ammoInstance.GetResource().GetDef().GetRefillDef() != null &&
                ammoInstance.GetResource().GetDef().GetRefillDef().isFiller() &&
                ammoInstance.GetResource().GetDef().GetRefillDef().GetFillRequirement().Equals(resourceID))
            {
                for (int i = 0; i < itemCount; i++)
                {
                    if (items[i] == null)
                    {
                        items[i] = item;
                        return true;
                    }
                }
            }
        }

        if (item.GetDef().GetItemType() == ItemType.EQUIP)
        {
            EquipInstance equipInstance = (EquipInstance)item;

            if (equipInstance.GetResourceStore()!=null &&
                equipInstance.GetResourceStore().GetDef().GetRefillDef()!=null &&
                equipInstance.GetResourceStore().GetDef().GetRefillDef().isFiller() &&
                equipInstance.GetResourceStore().GetDef().GetRefillDef().GetFillRequirement().Equals(resourceID))
            {
                for (int i = 0; i < itemCount; i++)
                {
                    if (items[i] == null)
                    {
                        items[i] = item;
                        return true;
                    }
                }
            }

        }

        return false;
    }
}
