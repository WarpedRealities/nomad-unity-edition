﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
[Serializable]
public class SolarModifiers {
    public float sensorMod=0;
    public float detectionMod = 0;
    public float efficiencyMod = 1;
    public float speedMod = 1;
    public float ftl = 0;

    public SolarModifiers(float efficiency, float detection, float speed, float sensors, float ftl)
    {
        this.efficiencyMod = efficiency;

        this.detectionMod = detection;
        this.speedMod = speed;
        this.sensorMod = sensors;
        this.ftl = ftl;
        
    }
}


[Serializable]
public class ModifierSystem : ShipSystem
{

    SolarModifiers solarModifiers;
    int uniqueModifierId;

    public ModifierSystem(SolarModifiers solarModifiers, int uniqueId = 0)
    {
        this.systemType = SHIPSYSTEMTYPE.MODIFIER;
        this.uniqueModifierId = uniqueId;
        this.solarModifiers = solarModifiers;
    }

    public int GetId()
    {
        return uniqueModifierId;
    }

    public SolarModifiers GetSolarModifiers()
    {
        return solarModifiers;
    }

    public override bool AddItem(Item item)
    {
        return false;
    }

    public override void Run(long duration, Dictionary<string, ShipResource> resources)
    {
        return;
    }
}
