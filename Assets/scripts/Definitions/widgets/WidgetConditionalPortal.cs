﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GlobalFlagConditional
{
    public int value, operand;
    public string flag;

    public GlobalFlagConditional(string flag, int value, int operand)
    {
        this.value = value;
        this.operand = operand;
        this.flag = flag;
    }

    internal static int GetOperand(string v)
    {
        if ("equals".Equals(v))
        {
            return 0;
        }
        if ("greaterthan".Equals(v))
        {
            return 1;
        }
        if ("lessthan".Equals(v))
        {
            return -1;
        }
        return 0;
    }
}

[Serializable]
public class WidgetConditionalPortal : WidgetPortal
{
    string forbidText;
    GlobalFlagConditional conditional;

    public WidgetConditionalPortal(string destination, int id, int facing, string forbidText, GlobalFlagConditional conditional) : base(destination, id, facing)
    {
        this.conditional = conditional;
        this.forbidText = forbidText;
    }

    public override void Step(ViewInterface viewInterface)
    {
        if (ConditionCheck())
        {
            viewInterface.ZoneTransition(destination, identifier);
        }
        else
        {
            viewInterface.DrawText(forbidText);
        }

    }

    private bool ConditionCheck()
    {
        FlagField flagField = GlobalGameState.GetInstance().GetPlayer().GetFlagField();
        int v = flagField.ReadFlag(conditional.flag);
        switch (conditional.operand)
        {
            case -1:
                return v < conditional.value;
            case 0:
                return v == conditional.value;
            case 1:
                return v >= conditional.value;
        }
        return false;
    }
}
