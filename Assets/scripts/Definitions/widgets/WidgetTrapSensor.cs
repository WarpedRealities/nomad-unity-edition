using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class TriggerResults
{
    public int range;
    public bool cardinalDirections;
    public TrapType trapType;
    public TriggerResults(int range, bool cardinal, TrapType trapType)
    {
        this.range = range;
        this.cardinalDirections = cardinal;
        this.trapType = trapType;
    }
}


[Serializable]
public class WidgetTrapSensor : Widget
{
    bool revealed;
    int difficulty, disarmCheck;
    string revealDesc;
    TriggerResults results;

    public WidgetTrapSensor(int difficulty, int disarmCheck, string revealDesc, string description, TriggerResults triggerResults)
    {
        this.revealed = false;
        this.revealDesc = revealDesc;
        this.description = description;
        this.results = triggerResults;
        this.canWalk = true;
        this.blockVision = false;
        this.difficulty = difficulty;
        this.disarmCheck = disarmCheck;
    }

    public override int getSprite()
    {
        return revealed ? 28: 0;
    }

    public override string getDescription()
    {
        return revealed ? description : null;
    }

    public override Widget Clone()
    {
        return new WidgetTrapSensor(difficulty,disarmCheck, revealDesc, description, results);
    }

    public override void Regeneration(long clock)
    {

    }
    public override bool CanInteract()
    {
        return revealed;
    }
    public override void LookAt(ViewInterface viewInterface, Player player, int bonus = 0)
    {
        if (!revealed)
        {
            int diceroll = bonus + DiceRoller.GetInstance().RollDice(20);
            if (diceroll >= difficulty)
            {
                revealed = true;
                viewInterface.DrawText(revealDesc);
            }
        }
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        if (revealed)
        {
            int diceroll = player.GetRPG().getSkill(SK.TECH) + DiceRoller.GetInstance().RollDice(20);
            if (diceroll >= disarmCheck)
            {
                viewInterface.DrawText("Trap disarmed");
                RemoveTrap(viewInterface);
            }
            else
            {

                if (diceroll < disarmCheck - 10)
                {
                    viewInterface.DrawText("Trap triggered");
                    TriggerTrap(viewInterface, GlobalGameState.GetInstance().GetPlayer());
                    RemoveTrap(viewInterface);
                }
                else
                {
                    viewInterface.DrawText("Trap not disarmed");
                }
            }
            return true;
        }
        return false;
    }

    private void TriggerTrap(ViewInterface viewInterface, Player player)
    {
        ZoneTools.TriggerTrap(viewInterface, player, this);
    }

    public TriggerResults GetResults()
    {
        return results;
    }

    public void RemoveTrap(ViewInterface view)
    {
        Vector2Int p = view.GetWidgetPosition(this);

        Tile t = GlobalGameState.GetInstance().getCurrentZone().GetContents().getTiles()[p.x][p.y];      
        t.SetWidget(null);
    }

    public override void Step(ViewInterface viewInterface)
    {
        viewInterface.DrawText("Trap triggered");
        TriggerTrap(viewInterface, GlobalGameState.GetInstance().GetPlayer());
        RemoveTrap(viewInterface);
    }

}
