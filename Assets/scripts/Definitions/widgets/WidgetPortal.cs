﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class WidgetPortal : Widget
{
    protected string destination;
    protected int identifier, facing;
    public WidgetPortal(string destination, int id, int facing)
    {
        this.canWalk = true;
        this.destination = destination;
        this.identifier = id;
        this.facing = facing;
    }


    public override void Step(ViewInterface viewInterface)
    {

        viewInterface.ZoneTransition(destination, identifier);
    }

    public override bool CanInteract()
    {
        return false;
    }

    public int getID()
    {
        return identifier;
    }

    public int getFacing()
    {
        return facing;
    }

    public override Widget Clone()
    {
        throw new NotImplementedException();
    }
    public override void Regeneration(long clock)
    {

    }

    internal void SetDestination(string destination)
    {
        this.destination = destination;
    }
}
