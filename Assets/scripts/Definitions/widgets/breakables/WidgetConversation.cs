﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetConversation : Widget_Breakable
{
    private FlagField flagField;
    private string conversationFile;

    public WidgetConversation(int sprite, string description, string name, BreakableData breakableData) : base(sprite, description, name, breakableData,true)
    {
        this.canWalk = false;
    }

    public WidgetConversation(int sprite, string description, string name, string conversationFile, BreakableData breakableData) : base(sprite, description, name, breakableData,true)
    {
        this.canWalk = false;
        this.conversationFile = conversationFile;
    }

    public override Widget Clone()
    {
        return new WidgetConversation(this.widgetSprite, this.description, this.name, conversationFile, this.breakableData.Clone());
    }

    public override bool CanInteract()
    {
        return true;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        Debug.Log("interact with conversation " + conversationFile);
        if (flagField == null)
        {
            flagField = new FlagField();
        }
        viewInterface.SetScreen(new ScreenDataConversation(conversationFile, flagField, this));

        return true;
    }

    internal void SetConversation(string conversationFile)
    {
        this.conversationFile = conversationFile;
    }
}
