﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetContainer : Widget_Breakable
{
    protected ContainerData containerData;

    public WidgetContainer(int sprite, string description, string name, ContainerData containerData, BreakableData breakableData, bool blocksVision): base(sprite,description,name,breakableData, blocksVision)
    {
        this.canWalk = false;
        this.containerData = containerData;
    }

    public ContainerData GetContainerData()
    {
        return containerData;
    }

    public override bool CanInteract()
    {
        return true;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        if (!containerData.IsOpened())
        {
            containerData.Open();
        }
        widgetSprite = containerData.GetOpenSprite();
        viewInterface.CalculateVision();
        viewInterface.SetScreen(new ScreenDataContainer(containerData));

        return true;
    }


    public override Widget Clone()
    {
        return new WidgetContainer(this.widgetSprite, this.description, this.name, this.containerData.Clone(), this.breakableData.Clone(),this.blockVision);
    }

}
