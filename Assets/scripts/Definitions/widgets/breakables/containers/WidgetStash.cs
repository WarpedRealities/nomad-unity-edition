using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetStash : WidgetContainer
{
    int difficulty;
    bool revealed;
    string revealDesc;
    public WidgetStash(int difficulty, string revealDesc, int sprite, string description, string name, ContainerData containerData, BreakableData breakableData, bool blocksVision) : base(sprite, description, name, containerData, breakableData, blocksVision)
    {
        revealed = false;
        this.difficulty = difficulty;
        this.revealDesc = revealDesc;
    }
    public override Widget Clone()
    {
        return new WidgetStash(this.difficulty,this.revealDesc, this.widgetSprite, this.description, this.name, this.containerData.Clone(), this.breakableData.Clone(), this.blockVision);
    }
    public override int getSprite()
    {
        return revealed ? this.widgetSprite : 0;
    }

    public override bool IsWalkable()
    {
        return !revealed;
    }

    public override void LookAt(ViewInterface viewInterface, Player player, int bonus = 0)
    {
        if (!revealed)
        {
            int diceroll = bonus + DiceRoller.GetInstance().RollDice(20);
            if (diceroll >= difficulty)
            {
                revealed = true;
                viewInterface.DrawText(revealDesc);
            }
        }
    }

    public override bool IsVisionBlocking()
    {
        return revealed ? blockVision: false;
    }

    public override string getDescription()
    {
        return revealed ? description : null;
    }

    public override bool CanInteract()
    {
        return revealed;
    }
}