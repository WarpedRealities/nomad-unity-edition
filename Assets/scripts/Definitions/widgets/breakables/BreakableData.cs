﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BreakableData
{
    private int health, kSoak, tSoak, sSoak, minItems, maxItems;
    private string itemCode;

    public BreakableData(int health, int kSoak, int tSoak, int sSoak, int minItems, int maxItems, string itemCode)
    {
        this.health = health;
        this.kSoak = kSoak;
        this.tSoak = tSoak;
        this.sSoak = sSoak;
        this.minItems = minItems;
        this.maxItems = maxItems;
        this.itemCode = itemCode;
    }


    public int GetHealth()
    {
        return health;
    }

    public int GetMinItems()
    {
        return minItems;
    }

    public int GetMaxItems()
    {
        return maxItems;
    }

    public string GetItemCode()
    {
        return itemCode;
    }

    public float GetDefence(DR dR)
    {
        Debug.Log("dr is " + dR);
        switch (dR)
        {
            case DR.KINETIC:
                return kSoak;
            case DR.SHOCK:
                return sSoak;
            case DR.THERMAL:
                return tSoak;

        }
        return 0;
    }

    internal BreakableData Clone()
    {
        return new BreakableData(this.health, this.kSoak, this.tSoak, this.sSoak, this.minItems, this.maxItems, this.itemCode);
    }

    public void SubtractHealth(int dmg)
    {
        health -= dmg;
    }
}
