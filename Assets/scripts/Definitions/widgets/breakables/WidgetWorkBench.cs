﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetWorkBench : Widget_Breakable
{

    public WidgetWorkBench(int sprite, string description, string name, BreakableData breakableData) : base(sprite, description, name, breakableData,false)
    {

    }

    public override bool CanInteract()
    {
        return true;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {

        viewInterface.SetScreen(ScreenID.CRAFTING);

        return true;
    }
}
