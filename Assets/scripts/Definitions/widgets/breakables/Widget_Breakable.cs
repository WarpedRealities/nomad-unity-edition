﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Widget_Breakable : Widget
{
    protected BreakableData breakableData;


    public Widget_Breakable()
    {

    }
    public Widget_Breakable(int sprite, string description, string name, BreakableData breakableData, bool blockVision)
    {
        this.name = name;
        this.description = description;
        this.widgetSprite = sprite;
        this.breakableData = breakableData;
        this.blockVision = blockVision;
        
    }

    public override Widget Clone()
    {
        return new Widget_Breakable(this.widgetSprite, this.description, this.name, this.breakableData.Clone(), this.blockVision);
    }


    public override void Step(ViewInterface viewInterface)
    {
   
    }
    public override void Regeneration(long clock)
    {

    }

    public int ApplyEffect(Effect effect, AmmoType ammoType, Actor origin, DiceRoller diceRoller, ViewInterface view)
    {
        DiceRoller roller = DiceRoller.GetInstance();
        if (effect.GetEffectType().Equals(EffectType.DAMAGE))
        {
            EffectDamage effectDamage = (EffectDamage)effect;
            if (effectDamage.GetDamagetype() <= DR.SHOCK)
            {
                //apply damage
                int max = (int)(ammoType != null ? effectDamage.GetMax() * ammoType.GetDamageMod() : effectDamage.GetMax());
                //do damage roll
                int dmg = effectDamage.GetMin() == max ? effectDamage.GetMin() : roller.RollDice(max - effectDamage.GetMin()) + effectDamage.GetMin();
                //add bonus
                dmg += (int)(origin.GetRPG().GetAbilityMod(effectDamage.GetAbilityModifier()) * effectDamage.GetAbilityMultiplier());
                //subtract defence

                dmg -= (int)(breakableData.GetDefence(effectDamage.GetDamagetype()) * (1-effectDamage.GetPenetration()));
                dmg = dmg < 0 ? 0 : dmg;
                breakableData.SubtractHealth(dmg);
                return dmg;
            }
        }
        return 0;
    }


    public bool IsDestroyed()
    {
        return breakableData.GetHealth() <= 0;
    }

    public void DestroyWidget(ViewInterface view, bool dumpItems=true)
    {
        Vector2Int p = view.GetWidgetPosition(this);

        Tile t = GlobalGameState.GetInstance().getCurrentZone().GetContents().getTiles()[p.x][p.y];
        if (t.GetWidget() is WidgetSlot)
        {
            WidgetSlot ws = (WidgetSlot)t.GetWidget();
            ws.SetContainedWidget(null);
            ws.SetComponent(null);
            //need to move t to a nearby tile as the slot exists in the space still
            for (int i = 0; i < 8; i++)
            {
                Vector2Int p0=GeometryTools.GetPosition(p.x, p.y, i);
                Tile t0 = GlobalGameState.GetInstance().getCurrentZone().GetContents().getTiles()[p0.x][p0.y];
                if (t0!=null && t0.GetWidget()==null && t0.getDefinition().getMovement() == TileMovement.WALK)
                {
                    t = t0;
                    break;
                }
            }
        }
        else
        {
            t.SetWidget(null);
        }

        if (breakableData.GetMaxItems() > 0 && dumpItems)
        {
            int count = breakableData.GetMaxItems() == breakableData.GetMinItems() ?
                breakableData.GetMinItems() :
                breakableData.GetMinItems() + DiceRoller.GetInstance().RollDice(breakableData.GetMaxItems() - breakableData.GetMinItems());
            if (count == 1)
            {
                t.SetWidget(new WidgetItemPile(GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(breakableData.GetItemCode())));
            }
            else
            {
                ItemStack itemStack = new ItemStack(GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(breakableData.GetItemCode()).GetDef(), count);
                t.SetWidget(new WidgetItemPile(itemStack));
            }
        }

        view.CalculateVision();
    }

    public override bool CanInteract()
    {
        return false;
    }
}
