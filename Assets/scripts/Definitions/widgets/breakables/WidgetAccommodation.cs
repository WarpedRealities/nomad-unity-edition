﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class WidgetAccommodation : Widget_Breakable
{
    private int capacity;

    public WidgetAccommodation(int sprite, string description, string name, BreakableData breakableData, int capacity)
    {
        this.widgetSprite = sprite;
        this.description = description;
        this.name = name;
        this.breakableData = breakableData;
        this.capacity = capacity;
        canWalk = false;
        blockVision = true;
    }

    public override bool CanInteract()
    {
        return true;
    }


    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        //interact nap
        viewInterface.DrawText("You take a nap");
        viewInterface.GetPlayerInZone().GetController().addAction(new ActionRecover(1000, 0.5F));
        return false;
    }

    public override bool IsSafeOnly()
    {
        return true;
    }
    public override Widget Clone()
    {
        return new WidgetAccommodation(widgetSprite, description, name, breakableData.Clone(), capacity);
    }

    internal int GetCapacity()
    {
        return capacity;
    }
}
