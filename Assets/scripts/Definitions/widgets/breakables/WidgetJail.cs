using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetJail : Widget_Breakable
{
    NPC[] captives;

    public WidgetJail(int sprite, string description, string name, BreakableData breakableData) : base(sprite, description, name, breakableData, true)
    {
        this.canWalk = false;
    }

    public void SetCaptives(int length)
    {
        this.captives=new NPC[length]; 
    }

    public override Widget Clone()
    {
       WidgetJail jail= new WidgetJail(this.widgetSprite, this.description, this.name, this.breakableData.Clone());
        jail.SetCaptives(captives.Length);
       return jail;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {

        viewInterface.SetScreen(new ScreenDataJail(this));

        return true;
    }

    internal NPC[] GetCaptives()
    {
        return captives;
    }

    internal void AddCaptive(Actor actor)
    {
        for (int i = 0; i < captives.Length; i++)
        {
            if (captives[i] == null)
            {
                captives[i] = (NPC)actor;
                return;
            }
        }
    }

    internal void RemoveCaptive(Actor actor)
    {
        for (int i = 0; i < captives.Length; i++)
        {
            if (actor.Equals(captives[i]))
            {
                captives[i] = null;
            }
        }
    }
}