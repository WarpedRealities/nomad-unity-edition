using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetResearchComputer : Widget_Breakable
{
    public WidgetResearchComputer(int sprite, string description, string name, BreakableData breakableData) : base(sprite, description, name, breakableData, true)
    {
        this.canWalk = false;
    }

    public override bool CanInteract()
    {
        return true;
    }

    public override Widget Clone()
    {
        return new WidgetResearchComputer(this.widgetSprite, this.description, this.name, this.breakableData.Clone());
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
    
        viewInterface.SetScreen(ScreenID.RESEARCH);

        return true;
    }
}
