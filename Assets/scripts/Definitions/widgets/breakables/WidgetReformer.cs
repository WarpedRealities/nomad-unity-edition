﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class WidgetReformer : Widget_Breakable
{
    bool suppressedReform;
    public WidgetReformer(int sprite, string description, string name, BreakableData breakableData): base(
        sprite, description, name, breakableData, true)
    {
        suppressedReform = false;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        if (suppressedReform)
        {
            viewInterface.DrawText("You have reset the reformation tube, its now functional again and no longer suppressed");
            suppressedReform = false;
        }
        else
        {
            viewInterface.DrawText("You interact with the reformation tube's scanner, synchronizing your quantum signature with the device");
            player.GetReformation().RegisterZone(GlobalGameState.GetInstance().getCurrentZone().getName(),
                GlobalGameState.GetInstance().GetUniverse().GetClock());
        }
        return true;
    }

    public override bool CanInteract()
    {
        return true;
    }

    public void SetSuppression(bool suppressed)
    {
        this.suppressedReform = suppressed;
    }

    public bool IsSuppressed()
    {
        return suppressedReform;
    }
}
