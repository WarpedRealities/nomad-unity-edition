﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetDoor : Widget_Breakable
{

    int lockStrength;
    bool jammed;
    string keyID;

    public WidgetDoor(int strength, string key, int sprite, string description, string name, BreakableData breakableData, bool blocksVision=true): base(sprite,description,name,breakableData,blocksVision)
    {
        this.lockStrength = strength;
        this.keyID = key;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        //key check
        if (KeyCheck(player, viewInterface))
        {
            DestroyWidget(viewInterface, false);
            return true;
        }
        //attempt to pick lock check
        if (PickLock(player, viewInterface))
        {
            DestroyWidget(viewInterface, false);
            return true;
        }
        return false;
    }

    public override bool CanInteract()
    {
        return true;
    }

    private bool KeyCheck(Player player, ViewInterface view)
    {
        if (keyID==null || keyID.Length <= 2)
        {
            Debug.Log("no key");
            view.DrawText("You have opened the door");
            return true;
        }
        for (int i = 0; i < player.GetInventory().GetItemCount(); i++)
        {
            if (player.GetInventory().GetItem(i).GetDef().GetItemType() == ItemType.KEY)
            {
                ItemKeyInstance itemKeyInstance = (ItemKeyInstance)player.GetInventory().GetItem(i);
                if (this.keyID.Equals(itemKeyInstance.GetKeyCode())) {

                    player.GetInventory().RemoveItem(itemKeyInstance);
                    view.DrawText("You have unlocked the door using " + itemKeyInstance.GetKeyName() + itemKeyInstance.GetName());
                    return true;
                }
            }
        }

        return false;
    }

    public string GetKey()
    {
        return keyID;
    }

    private bool PickLock(Player player, ViewInterface view)
    {
        if (jammed)
        {
            view.DrawText("This lock has been jammed and cannot be picked");
            return false;
        }
        int check = DiceRoller.GetInstance().RollDice(20) + player.GetRPG().getSkill(SK.TECH);
        Debug.Log("unlock " + check + " " + lockStrength);
        if (check > lockStrength)
        {
            view.DrawText("You have picked the lock");



            return true;
        }
        if (check < lockStrength / 2)
        {
            jammed = true;
            view.DrawText("You have jammed the lock by attempting to pick it");
        }
        return false;
    }

    internal void SetKey(string key)
    {
        this.keyID=key;
    }

    internal void SetStrength(int strength)
    {
        this.lockStrength = strength;
    }

    public override bool IsSafeOnly()
    {
        return this.lockStrength>0;
    }

    public override bool IsFloored()
    {
        return true;
    }
}
