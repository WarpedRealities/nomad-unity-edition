﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetBarrier : Widget
{

    int spriteNum;
    int globalValue;
    string globalKey;
    int comparison;

    public WidgetBarrier()
    {

    }

    public WidgetBarrier(int sprite, string description)
    {
        this.spriteNum = sprite;
        this.widgetSprite = sprite;
        this.description = description;

    }

    public void SetBarrierValues(string key, int value, string comparison)
    {
        this.globalValue = value;
        this.globalKey = key;
        this.comparison = ConvertComparison(comparison);
    }

    public override bool CanInteract()
    {
        return false;
    }

    private int ConvertComparison(string comparison)
    {
        if ("==".Equals(comparison))
        {
            return 0;
        }

        if (">".Equals(comparison))
        {
            return 1;
        }
        if ("<".Equals(comparison))
        {
            return -1;
        }
        return 0;
    }

    public override Widget Clone()
    {
        WidgetBarrier widgetBarrier = new WidgetBarrier();
        widgetBarrier.description = description;
        widgetBarrier.comparison = comparison;
        widgetBarrier.globalKey = globalKey;
        widgetBarrier.globalValue = globalValue;
        widgetBarrier.widgetSprite = widgetSprite;
        widgetBarrier.spriteNum = spriteNum;
        return widgetBarrier;
    }

    public override void Regeneration(long clock)
    {
        FlagField globalValues = GlobalGameState.GetInstance().GetPlayer().GetFlagField();

        int value = globalValues.ReadFlag(globalKey);
        switch (comparison)
        {
            case -1:
                SetState(globalValue > value);
                break;
            case 0:
                SetState(globalValue == value);
                break;

            case 1:
                SetState(globalValue < value);
                break;
        }
    }

    private void SetState(bool visible)
    {
        this.canWalk = !visible;
        this.blockVision = visible;
        this.spriteNum = visible ? this.widgetSprite : 0;
    }

    public override void Step(ViewInterface viewInterface)
    {

    }
}
