﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class HarvestableData 
{
    private string codeName;
    private int min, max, DC;
    private string harvestComplete, noHarvest, harvestSuccess;
    private long regenerationTime, harvestTimeStamp;
    private bool harvested;

    public HarvestableData(string codeName, int min, int max, int DC, string harvestComplete, string noHarvest, string harvestSuccess, long regenTime)
    {
        harvested = false;
        this.codeName = codeName;
        this.min = min;
        this.max = max;
        this.DC = DC;
        this.harvestComplete = harvestComplete;
        this.noHarvest = noHarvest;
        this.harvestSuccess = harvestSuccess;
        this.regenerationTime = regenTime;
    }

    public bool IsHarvested()
    {
        return harvested;
    }

    public string GetCodeName()
    {
        return codeName;
    }

    public int GetMin()
    {
        return min;
    }

    public int GetMax()
    {
        return max;
    }

    public int GetDC()
    {
        return DC;
    }
    public string GetComplete()
    {
        return harvestComplete;
    }

    public string GetNoHarvest()
    {
        return noHarvest;
    }

    public string GetSuccess()
    {
        return harvestSuccess;
    }

    public long GetRegenTime()
    {
        return regenerationTime;
    }

    public void SetHarvestTimeStamp(long timestamp)
    {
        harvestTimeStamp = timestamp;
    }

    public long GetTimeStamp()
    {
        return harvestTimeStamp;
    }

    public void SetHarvested(bool harvested)
    {
        this.harvested = harvested;
    }
}
