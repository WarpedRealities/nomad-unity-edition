﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class WidgetHarvestable : Widget
{
    private HarvestableData harvestableData;
    private int readySprite, harvestSprite;

    public WidgetHarvestable()
    {
    }

    public WidgetHarvestable(string name, int readySprite, int harvestSprite, string description, bool walkable, HarvestableData harvestableData)
    {
        this.name = name;
        this.readySprite = readySprite;
        this.harvestSprite = harvestSprite;
        this.harvestableData = harvestableData;
        this.description = description;
        this.canWalk = walkable;
        this.widgetSprite = readySprite;
    }

    public override Widget Clone()
    {
        WidgetHarvestable widget = new WidgetHarvestable();
        widget.canWalk = canWalk;
        widget.description = description;
        widget.harvestSprite = harvestSprite;
        widget.readySprite = readySprite;
        widget.widgetSprite = widgetSprite;
        widget.harvestableData = new HarvestableData(harvestableData.GetCodeName(),
            harvestableData.GetMin(),
            harvestableData.GetMax(),
            harvestableData.GetDC(),
            harvestableData.GetComplete(),
            harvestableData.GetNoHarvest(),
            harvestableData.GetSuccess(),
            harvestableData.GetRegenTime());
        return widget;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        if (harvestableData.IsHarvested()) {
            viewInterface.DrawText(harvestableData.GetNoHarvest());
            return false;
        }

        int roll = DiceRoller.GetInstance().RollDice(20)+player.GetRPG().getSkill(SK.SCIENCE);
        bool bonusResources = harvestableData.GetDC() != -1 && roll >= harvestableData.GetDC();
        int count= harvestableData.GetMax()==harvestableData.GetMin() ? 
            harvestableData.GetMax() : 
            harvestableData.GetMin()+ DiceRoller.GetInstance().RollDice(harvestableData.GetMax()-harvestableData.GetMin());
        if (bonusResources)
        {
            count += harvestableData.GetMax() == harvestableData.GetMin() ?
            harvestableData.GetMax() :
            harvestableData.GetMin() + DiceRoller.GetInstance().RollDice(harvestableData.GetMax() - harvestableData.GetMin());
        }
        string amount = count.ToString();
        string send = bonusResources ?
            harvestableData.GetSuccess().Replace("VALUE", amount) :
            harvestableData.GetComplete().Replace("VALUE", amount);
        viewInterface.DrawText(count>1 ? send.Replace(" PLURAL","s") : send.Replace(" PLURAL",""));

        harvestableData.SetHarvestTimeStamp(GlobalGameState.GetInstance().GetUniverse().GetClock());

        Item item= GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(harvestableData.GetCodeName());
        //add these items to the player's inventory
        player.GetInventory().AddItems(item, count);
        harvestableData.SetHarvested(true);
        widgetSprite = harvestSprite;
        viewInterface.CalculateVision();
        return true;
    }

    public override void Step(ViewInterface viewInterface)
    {
      
    }
    public override bool CanInteract()
    {
        return true;
    }

    public override bool IsWalkable()
    {
        return harvestableData.IsHarvested() ? true : canWalk;
    }

    public override void Regeneration(long clock)
    {
        if (harvestableData.IsHarvested())
        {
            if (clock > harvestableData.GetRegenTime() + harvestableData.GetTimeStamp())
            {
                harvestableData.SetHarvested(false);
                widgetSprite = readySprite;
            }
        }
    }
}
