using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetTalkPoint : Widget
{
    private FlagField flagField;
    private string conversationFile;

    public WidgetTalkPoint(int sprite, string description, string name)
    {
        this.description = description;
        this.name = name;
        this.blockVision = false;
        this.widgetSprite = sprite;
        this.canWalk = true;
    }

    public WidgetTalkPoint(int sprite, string description, string name, string conversationFile)
    {
        this.canWalk = true;
        this.description = description;
        this.name = name;
        this.blockVision = false;
        this.widgetSprite = sprite;
        this.conversationFile = conversationFile;
    }

    public override Widget Clone()
    {
        return new WidgetTalkPoint(this.widgetSprite, this.description, this.name, conversationFile);
    }

    public override bool CanInteract()
    {
        return true;
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {

        if (flagField == null)
        {
            flagField = new FlagField();
        }
        viewInterface.SetScreen(new ScreenDataConversation(conversationFile, flagField));

        return true;
    }

    internal void SetConversation(string conversationFile)
    {
        this.conversationFile = conversationFile;
    }

    public override void Step(ViewInterface viewInterface)
    {
      
    }

    public override void Regeneration(long clock)
    {

    }
}
