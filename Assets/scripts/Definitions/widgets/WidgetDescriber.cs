using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetDescriber : Widget
{

    public WidgetDescriber(string description)
    {
        canWalk = true;
        this.description = description;
    }


    public override Widget Clone()
    {
        return null;
    }

    public override void Regeneration(long clock)
    {

    }
    public override bool CanInteract()
    {
        return false;
    }
    public override void LookAt(ViewInterface viewInterface, Player player, int bonus=0)
    {
        TextLog.Log(description);
    }

    public override void Step(ViewInterface viewInterface)
    {

    }
}
