﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[Serializable]
public class WidgetItemPile : Widget
{
    ItemList itemList;

    public WidgetItemPile(Item item)
    {
        widgetSprite = 2;
        canWalk = true;
        List<Item> items = new List<Item>();
        items.Add(item);
        itemList = new ItemList(items);
      
    }

    public WidgetItemPile(ItemList itemList)
    {
        widgetSprite = 2;
        canWalk = true;
        this.itemList = itemList;
    }

    public WidgetItemPile()
    {
        widgetSprite = 2;
        canWalk = true;
    }

    public void SetItemList(ItemList itemList)
    {
        this.itemList = itemList;
    }

    public override bool CanInteract()
    {
        return true;
    }

    public WidgetItemPile(List<Item> items)
    {
        itemList = new ItemList(items);
        widgetSprite = 2;
        canWalk = true;
    }

    public void AddItem(Item item)
    {
        itemList.AddItem(item);
    }

    public override Widget Clone()
    {
        throw new System.NotImplementedException();
    }

    public override void Step(ViewInterface viewInterface)
    {
       
    }

    public override string getDescription()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("An item pile containing ");
        for (int i = 0; i < itemList.GetItemCount(); i++)
        {

            stringBuilder.Append(itemList.GetItems()[i].GetName());
            if (i > 0)
            {
                stringBuilder.Append(",");
            }
            stringBuilder.Append(" ");
        }
        return stringBuilder.ToString();
    }

    public override bool Interact(ViewInterface viewInterface, Player player)
    {
        //take items from the pile
        Item item = itemList.GetItem(0);
        viewInterface.DrawText("You have picked up " + item.GetDef().GetName());

        player.GetInventory().AddItem(itemList.RemoveItem(0));
        //is pile empty?
        if (itemList.GetItemCount() <= 0)
        {
            viewInterface.RemoveWidget(this);
        }
        return true;
    }

    public override void Regeneration(long clock)
    {
       
    }
}
   

