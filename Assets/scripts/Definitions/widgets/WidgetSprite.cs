﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WidgetSprite : Widget
{
    string bigSprite;
    int width, height;

    public WidgetSprite(string sprite, int width, int height)
    {
        this.canWalk = true;
        this.bigSprite = sprite;
        this.width = width;
        this.height = height;
    }
    public override bool CanInteract()
    {
        return false;
    }
    public string GetBigSprite()
    {
        return bigSprite;
    }

    public int getWidth()
    {
        return this.width;
    }

    public int getHeight()
    {
        return this.height;
    }

    public override void Step(ViewInterface viewInterface)
    {

    }

    public override Widget Clone()
    {
        throw new NotImplementedException();
    }
    public override void Regeneration(long clock)
    {

    }
}
