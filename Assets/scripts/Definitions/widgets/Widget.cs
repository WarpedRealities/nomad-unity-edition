﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
abstract public class Widget
{
    protected int widgetSprite;
    protected string description;
    protected bool canWalk;
    protected bool blockVision;
    protected string name;

    public virtual int getSprite()
    {
        return widgetSprite;
    }

    public virtual string getDescription()
    {
        return description;
    }

    public virtual bool IsWalkable()
    {
        return canWalk;
    }

    public virtual bool IsFloored()
    {
        return false;
    }

    public virtual bool IsVisionBlocking()
    {
        return blockVision;
    }

    abstract public bool CanInteract();

    abstract public void Step(ViewInterface viewInterface);

    public virtual void LookAt(ViewInterface viewInterface, Player player, int bonus=0)
    {

    }

    public virtual bool Interact(ViewInterface viewInterface, Player player)
    {
        return false;
    }

    public virtual bool IsSafeOnly()
    {
        return false;
    }

    abstract public Widget Clone();

    public abstract void Regeneration(long clock);

    public virtual bool IsColoured()
    {
        return false;
    }
    public virtual float [] GetColour()
    {
        return null;
    }

    public string GetName()
    {
        return name;
    }
}
