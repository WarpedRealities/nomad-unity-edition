﻿using UnityEngine;
using System.Collections;

public class CraftingRequirement 
{
    string itemCode;
    int count;

    public CraftingRequirement(string item, int count)
    {
        this.itemCode = item;
        this.count = count;
    }

    public int GetCount()
    {
        return count;
    }

    public string GetItemCode()
    {
        return itemCode;
    }
}
