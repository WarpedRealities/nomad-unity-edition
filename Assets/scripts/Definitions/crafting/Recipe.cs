﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recipe
{
    string name, description;
    int techRequirement;
    bool startUnlocked, progressionUnlock;
    int outputCount;
    string outputItemCode;
    List<CraftingRequirement> materialRequirements;
    List<CraftingToken> tokenRequirements;

    public Recipe(string name, string description, string outputItem, int outputCount, int techRequirement, bool startUnlocked)
    {
        this.name = name;
        this.description = description;
        this.outputItemCode = outputItem;
        this.outputCount = outputCount;
        this.startUnlocked = startUnlocked;
        this.techRequirement = techRequirement;
    }

    internal void SetUnlocked()
    {
        progressionUnlock = true;
    }

    public string GetName()
    {
        return name;
    }

    public string GetDescription()
    {
        return description;
    }

    internal void SetTokens(List<CraftingToken> tokens)
    {
        this.tokenRequirements = tokens;
    }

    internal void SetIngredients(List<CraftingRequirement> ingredients)
    {
        this.materialRequirements = ingredients;
    }

    public int GetTechRequirement()
    {
        return techRequirement;
    }

    internal string GetOutput()
    {
        return outputItemCode;
    }

    public bool isUnlocked()
    {
        return progressionUnlock || startUnlocked;
    }

    public List<CraftingToken> GetTokens()
    {
        return tokenRequirements;
    }

    public List<CraftingRequirement> GetIngredients()
    {
        return materialRequirements;
    }

    internal bool isStartUnlocked()
    {
        return startUnlocked;
    }
}
