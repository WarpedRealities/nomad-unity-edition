﻿using UnityEngine;
using System.Collections;

public class CraftingToken 
{

    private string token;
    private int rank;

    public CraftingToken(string token, int rank)
    {
        this.token = token;
        this.rank = rank;
    }

    public string GetToken()
    {
        return token;
    }

    public int GetRank()
    {
        return rank;
    }

}
