﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CraftingLibrary 
{
    List<Recipe> recipeList;



    public CraftingLibrary()
    {
        recipeList = CraftingRecipeLoader.LoadRecipes();
    }

    public void LoadRecipes()
    {
        CraftingRecipeLoader.LoadRecipes(recipeList);
    }

    public int RecipeCount()
    {
        return recipeList.Count;
    }

    public Recipe GetRecipe(int index)
    {
        return recipeList[index];
    }

    public void UnlockRecipe(string name)
    {

        for (int i = 0; i < recipeList.Count; i++)
        {
            if (recipeList[i].GetName().Equals(name)) {
                recipeList[i].SetUnlocked();
                break;
            }
        }
    }

    internal Recipe GetRecipe(string name)
    {
        for (int i = 0; i < recipeList.Count; i++)
        {
            if (recipeList[i].GetName().Equals(name))
            {
                return recipeList[i];
            }
        }
        return null;
    }

    internal List<string> GetUnlocks()
    {
        List<string> unlocks = new List<string>();
        for (int i=0;i< recipeList.Count; i++)
        {
            if (recipeList[i].isUnlocked() &&
                !recipeList[i].isStartUnlocked())
            {
                unlocks.Add(recipeList[i].GetName());
            }
        }
        return unlocks;
    }

    internal void ProcessUnlocks(List<string> list)
    {
        if (list != null)
        {
            for (int i = 0; i < list.Count; i++)
            {
                UnlockRecipe(list[i]);
            }
        }
    }
}
