﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TileDef
{
    private int smartTile;
    private int tileSprite;
    private int tileIndex;
    private int tileOverlay;
    private int smartTileGroup;
    private int smartTileAlias;
    private int tileDecoration;
    private string description;
    private TileMovement tileMovement;
    private TileVision tileVision;

    public TileDef(int smartTile, int tileSprite, int tileOverlay, int smartTileGroup, int smartAlias, int tileDecoration, string description, TileMovement movement, TileVision vision)
    {
        this.smartTile = smartTile;
        this.tileSprite = tileSprite;
        this.tileOverlay = tileOverlay;
        this.tileDecoration = tileDecoration;
        this.smartTileGroup = smartTileGroup;
        this.smartTileAlias = smartAlias;
        this.description = description;
        this.tileMovement = movement;
        this.tileVision = vision;
    }

    public int GetIndex()
    {
        return tileIndex;
    }

    public int getSmartTile()
    {
        return smartTile;
    }
    public int getSprite()
    {
        return tileSprite;
    }

    public int getSmartTileGroup()
    {
        return smartTileGroup;
    }

    public int GetSmartTileAlias()
    {
        return smartTileAlias;
    }

    public int getOverlay()
    {
        return tileOverlay;
    }

    public int getDecoration()
    {
        return tileDecoration;
    }

    public string getDescription()
    {
        return description;
    }

    public TileMovement getMovement()
    {
        return tileMovement;
    }

    public TileVision getVision()
    {
        return tileVision;
    }

    internal void SetIndex(int index)
    {
        this.tileIndex = index;
    }
} 
