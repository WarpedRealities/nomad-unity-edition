﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ZoneTileset
{
    private TileDef[] tiledefs;
    private int width, height;

    public ZoneTileset(TileDef[] tiledefs, int width, int height)
    {
        this.width = width;
        this.height = height;
        this.tiledefs = tiledefs;
    }

    public TileDef getDefinition(int index)
    {
        return tiledefs[index];
    }

    public int getCount()
    {
        return tiledefs.Length;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }
}
