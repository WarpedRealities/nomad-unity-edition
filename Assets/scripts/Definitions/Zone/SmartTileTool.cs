﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartTileTool { 
   

    public static bool likeness(Tile tile, TileDef def)
    {
        if (tile==null)
        {
            return false;
        }
        if (tile.getDefinition().getSmartTileGroup() == def.getSmartTileGroup() ||
            (tile.getDefinition().getSmartTileGroup() == def.GetSmartTileAlias() && def.GetSmartTileAlias()!=0))
        {
            return true;
        }
        return false;
    }

    public static Tile getTile(Tile [][] tiles, int x, int y)
    {
        if (x < 0 || x >= tiles.Length)
        {
            return null;
        }
        if (y < 0 || y >= tiles[0].Length)
        {
            return null;
        }
        return tiles[x][y];
    }
    public static int getAwarenessOffset(TileDef definition, Tile [][] tiles, Vector2S position)
    {
        int offset = 0;
        //north
        if (tiles[0].Length != position.y && likeness(getTile(tiles,position.x,position.y+1),definition))
        {
            offset += 1;
        }
   
        //south
        if (position.y != 0 && likeness(getTile(tiles, position.x,position.y - 1), definition))
        {
            offset += 4;
        }
 
        //east
        if (tiles.Length != position.x && likeness(getTile(tiles, position.x+1,position.y), definition))
        {
            offset += 2;
        }
  
        //west
        if (position.x != 0 && likeness(getTile(tiles, position.x-1,position.y), definition))
        {
            offset += 8;
        }
        return offset;
    }

    public static int getSuperAwarenessOffsetLeft(TileDef definition, Tile[][] tiles, Vector2S position)
    {
        int offset = 0;
        if (position.x != 0 && likeness(tiles[position.x - 1][position.y], definition))
        {
            offset += 1;
        }
        //south
        if (position.y != 0 && likeness(tiles[position.x][position.y - 1], definition))
        {
            offset += 2;
        }
        bool lowerLeft = position.y != 0 &&
        0 != position.x && 
        likeness(tiles[position.x - 1][position.y - 1], definition);
        //we have a tile below, do we have a tile to the lower left?
        if (offset == 2 && lowerLeft)
        {
            return 3;
        }
        //we have a tile below, and to the side, do we have a tile to the lower left
        if (offset == 3 && !lowerLeft)
        {
            return 4;
        }
        if (offset == 3 && lowerLeft)
        {
            return 5;
        }
        return offset;
    }
    public static int getSuperAwarenessOffsetRight(TileDef definition, Tile[][] tiles, Vector2S position)
    {
        int offset = 0;
        //east
        if (tiles.Length != position.x && likeness(getTile(tiles, position.x + 1,position.y), definition))
        {
            offset += 1;
        }
        //south
        if (position.y != 0 && likeness(getTile(tiles,position.x,position.y - 1), definition))
        {
            offset += 2;
        }

        bool lowerright = position.y != 0 &&
            tiles[0].Length != position.y &&
            likeness(getTile(tiles, position.x+1,position.y - 1), definition);
        //we have a tile below, do we have a tile to the lower right?
        if (offset == 2 && lowerright)
        {
            return 3;
        }
        //we have a tile below, and to the side, do we have a tile to the lower right
        if (offset == 3 && !lowerright)
        {
            return 4;
        }
        if (offset == 3 && lowerright)
        {
            return 5;
        }
        return offset;
    }


}
