﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SmartTile : Tile
{
    private int maskSprite;
    protected int maskOverlay;
   public SmartTile(TileDef def, Vector2S position): base(def, position)
    {
        
    }

    public override void AwarenessPass(Tile[][] tiles)
    {
        maskSprite = definition.getSprite() + SmartTileTool.getAwarenessOffset(definition, tiles, position);
        if (maskOverlay > 0)
        {
           // maskOverlay = definition.getOverlay()/* + SmartTileTool.getAwarenessOffset(definition, tiles, position)*/;
        }
    }

    public override int getSprite()
    {
        return maskSprite;
    }

    public override int getOverlay()
    {
        return definition.getOverlay();
    }

}
