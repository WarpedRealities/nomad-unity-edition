﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PregenData
{
    bool [] [] edgeValues;
    List<Vector2S> pointValues;

    public PregenData()
    {
        pointValues = new List<Vector2S>();
        this.edgeValues = new bool [4][];
    }

    public List<Vector2S> GetPointValues()
    {
        return pointValues;
    }

    public bool[] GetEdgeValues(int index)
    {
        if (edgeValues[index] == null)
        {
            edgeValues[index] = new bool[64];
        }
        return edgeValues[index];
    }

    public bool HasEdgeValues(int index)
    {
        return edgeValues[index] != null;
    }

    internal void ReadPointValues()
    {
        for (int i = 0; i < pointValues.Count; i++)
        {
            Debug.Log("p" + i + " x" + pointValues[i].x + " y" + pointValues[i].y);
        }
    }
}