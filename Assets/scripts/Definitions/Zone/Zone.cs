﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

[Serializable]
public class Zone
{
    private ZoneContents zoneContents;
    private PregenData pregenData;
    private string name;
    private string prefix;
    [NonSerialized]
    private Entity entity;

    public Zone(string prefix, string name, Entity entity)
    {

        this.prefix = prefix;
        this.name = name;
        this.entity = entity;
    }

    public Zone(string name)
    {
        this.name = name;
    }

    public PregenData GetPregenData()
    {
        if (pregenData== null)
        {
            pregenData= new PregenData();
        }
        return pregenData;
    }

    public void Generate()
    {
        if (zoneContents != null)
        {
            return;
        }
        XmlDocument document = FileTools.GetXmlDocument(prefix+name);
        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        LandingSite landingSite = entity.GetLandingSite(this);
        zoneContents = zoneContentsGenerator.generateContents(rootXML, landingSite, pregenData, entity);
        if (landingSite != null)
        {
            ShipConnectorTool.Connect(this, landingSite.GetSpaceship().GetZone(landingSite.GetSpaceship().getName()));
        }
        pregenData = null;
    }
    
    public Entity GetEntity()
    {
        return entity;
    }
    
    public void SetEntity(Entity entity)
    {
        this.entity = entity;
    }

    public ZoneContents GetContents()
    {
        return this.zoneContents;
    }

    public bool IsGenerated()
    {
        return zoneContents != null;
    }

    public void ZoneCleanup()
    {
        zoneContents.SetLastVisit(GlobalGameState.GetInstance().GetUniverse().GetClock());
        CleanActorInTile();
        for (int i = this.zoneContents.GetActors().Count - 1; i >= 0; i--)
        {
            if (!zoneContents.GetActors()[i].GetAlive())
            {
                if (zoneContents.GetActors()[i].GetPosition().x >= 0)
                {
                    zoneContents.GetActors()[i].Remove(true);
                    if (zoneContents.GetActors()[i].NoRegeneration())
                    {
                        zoneContents.GetActors().Remove(zoneContents.GetActors()[i]);
                    }
                }
            }
        }
    }

    public void CleanActorInTile()
    {
        if (zoneContents == null)
        {
            Tile[][] tiles = zoneContents.getTiles();
            for (int i = 0; i < zoneContents.GetZoneParameters().GetWidth(); i++)
            {
                for (int j = 0; j < zoneContents.GetZoneParameters().GetHeight(); j++)
                {
                    if (tiles[i][j] != null)
                    {
                        tiles[i][j].SetActor(null);
                        tiles[i][j].SetThreateningActor(null);
                    }
                }
            }
        }
    }

    internal void SetContents(ZoneContents zoneContents)
    {
        this.zoneContents = zoneContents;
    }

    public string getName()
    {
        return name;
    }

    public void Regenerate(long clock)
    {
        if (this.zoneContents == null)
        {
            return;
        }
        for (int i = 0; i < this.zoneContents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < this.zoneContents.GetZoneParameters().GetHeight(); j++)
            {
                if (this.zoneContents.getTiles()[i][j] != null)
                {
                    this.zoneContents.getTiles()[i][j].AwarenessPass(this.zoneContents.getTiles());
                    if (this.zoneContents.getTiles()[i][j].GetWidget() != null)
                    {
                        this.zoneContents.getTiles()[i][j].GetWidget().Regeneration(clock);
                    }
                }
            }
        }
        for (int i = this.zoneContents.GetActors().Count-1; i >=0; i--)
        {
            if (!zoneContents.GetActors()[i].GetAlive() && 
                (zoneContents.GetActors()[i].NoRegeneration() || zoneContents.GetActors()[i].IsVolatile()))
            {
                zoneContents.GetActors().Remove(zoneContents.GetActors()[i]);
            }
            else
            {
                this.zoneContents.GetActors()[i].ZoneEntryHandler(clock);
            }

        }
    }

    internal Actor CompanionCheck(long uid)
    {
        Actor actor = zoneContents.GetActorByID(uid);
        if (actor != null)
        {
            actor.GetRPG().Heal(1);
            zoneContents.GetActors().Remove(actor);
            return actor;
        }

        return null;
    }

    internal void CompanionAdd(Vector2Int position, Actor companion)
    {
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(position.x, position.y, i);
            if (p.x>=0 && p.x < zoneContents.GetZoneParameters().GetWidth() &&
                p.y>=0 && p.y< zoneContents.GetZoneParameters().GetHeight())
            {
                if (zoneContents.GetTile(p.x, p.y) != null && 
                    zoneContents.GetTile(p.x, p.y).canWalk())
                {
                    companion.SetPosition(p);
                    break;
                }
            }
        }
        StatusBind bind = companion.GetRPG().GetStatusHandler().GetStatusBind();
        if (bind != null)
        {
            companion.GetRPG().GetStatusHandler().RemoveStatusEffect(bind.GetUid(), null, companion);
        }
        zoneContents.GetActors().Add(companion);
    }
}
