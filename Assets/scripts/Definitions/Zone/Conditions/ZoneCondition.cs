using System;
using System.Collections;
using System.Xml;
using UnityEngine;

[Serializable]
public class ZoneCondition
{
    Effect effect;
    string conditionType;
    string text;

    public ZoneCondition(Effect effect, string type, string text)
    {
        this.effect = effect;
        this.conditionType = type;
        this.text = text;
    }

    internal static ZoneCondition Build(XmlElement xmlElement)
    {
        string type = xmlElement.GetAttribute("type");
        string text = null;
        Effect effect = null;
        for (int i = 0; i < xmlElement.ChildNodes.Count; i++)
        {
            if (xmlElement.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)xmlElement.ChildNodes[i];
                if ("effectDamage".Equals(e.Name))
                {
                    effect = CombatMoveLoader.BuildEffectDamage(e, false);
                }
                if ("effectStatus".Equals(e.Name))
                {
                    effect = CombatMoveLoader.BuildEffectStatus(e);
                }
                if ("text".Equals(e.Name))
                {
                    text = e.InnerText;
                }
            }
        }
        return new ZoneCondition(effect, type, text);
    }

    public string GetConditionType()
    {
        return this.conditionType;
    }

    internal Effect GetEffect()
    {
        return effect;
    }

    internal String GetText()
    {
        return text;
    }
}