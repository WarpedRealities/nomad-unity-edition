﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ZoneConditions 
{
    ZoneCondition[] conditions; 
    
    public ZoneConditions(ZoneCondition[] conditions) {  
        this.conditions = conditions; 
    }

    public ZoneCondition GetCondition(int index)
    {
        return this.conditions[index];
    }

    public int GetConditionCount()
    {
        return this.conditions.Length;
    }

    internal void RunConditions(Player player, ViewInterface view)
    {
        for (int i = 0; i < conditions.Length; i++)
        {
            if (conditions[i].GetEffect() != null)
            {
                float protection = ((PlayerRPG)player.GetRPG()).GetHazardProtection(conditions[i].GetConditionType());

                if (protection < 1)
                {
                    int value = EffectHandler.ApplyEffect(conditions[i].GetEffect(),
                        false,
                        view.GetPlayerInZone(),
                        null,
                        DiceRoller.GetInstance(),
                        view, 1-protection);
                    if (conditions[i].GetText() != null)
                    {
                        view.DrawText(conditions[i].GetText().Replace("VALUE", value.ToString()));
                    }
                    if (value > 0)
                    {
                        player.SetSuppression(5);
                        if (!player.GetAlive())
                        {
                            view.SetScreen(new ScreenDataGameover("You succumbed to the environment"));
                        }
                    }
                    view.Redraw();
                }
            }
        }
    }

}