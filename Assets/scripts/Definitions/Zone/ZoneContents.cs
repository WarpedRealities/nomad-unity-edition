﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ZoneContents
{
    private Tile[][] tiles;
    private ZoneParameters zoneParameters;
    private ZoneTileset zoneTileset;
    private string description;
    private List<Actor> actors;
    private string [] transitions;
    private Vector2S fixedLandingSite;
    private ZoneConditions zoneConditions;
    private long lastVisit;
    public void setZoneParameters(ZoneParameters zoneParameters)
    {
        this.zoneParameters = zoneParameters;
    }

    public ZoneParameters GetZoneParameters()
    {
        return zoneParameters;
    }

    public Vector2S GetFixedLandingSite()
    {
        return fixedLandingSite;
    }

    internal Tile GetTile(int x, int y)
    {
       if (x>=this.zoneParameters.GetWidth() || x < 0)
        {
          
            return null;
        }
       if (y>=this.zoneParameters.GetHeight() || y < 0)
        {

            return null;
        }
        return tiles[x][y];
    }

    public void SetFixedLandingSite(Vector2S site)
    {
        fixedLandingSite = site;
    }

    public void setTileSet(ZoneTileset zoneTileset)
    {
        this.zoneTileset = zoneTileset;
    }
    public ZoneTileset getTileSet()
    {
        return this.zoneTileset;
    }
    public void setDescription(string description)
    {
        this.description = description;
    }

    public void setTiles(Tile[][] tiles)
    {
        this.tiles = tiles;
    }

    public Tile [][] getTiles()
    {
        return tiles;
    }

    public void SetActors(List<Actor> actors)
    {
        this.actors = actors;
    }

    public List<Actor> GetActors()
    {

        return actors;
    }

    public void setTransitions(string [] transitions)
    {
        this.transitions = transitions;
    }

    public string [] getTransitions()
    {
        return transitions;
    }

    internal void PostLoad()
    {

        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[0].Length; j++)
            {
                if (tiles[i][j]!=null && tiles[i][j].getDefinition() == null)
                {
                    tiles[i][j].SetDefinition(this.zoneTileset.getDefinition(tiles[i][j].GetDefinitionIndex()));
             
                }
            }
        }
    }

    internal Actor GetActorByID(long v)
    {
        for (int i = 0; i < actors.Count; i++)
        {
            if (actors[i]!=null && actors[i].GetUID().Equals(v))
            {
                return actors[i];
            }
        }
        return null;
    }

    public void SetConditions(ZoneConditions conditions)
    {
        this.zoneConditions = conditions;
    }

    internal ZoneConditions GetConditions()
    {
        return this.zoneConditions;
    }

    public long GetLastVisit()
    {
        return lastVisit;
    }

    public void SetLastVisit(long timestamp)
    {
        this.lastVisit = timestamp;
    }
}
