﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public class Tile
{
    [NonSerialized]
    protected TileDef definition;
    protected int tileDefIndex=-1;

    protected Vector2S position;
    private bool explored, visible;
    protected Widget widget;
    [NonSerialized]
    protected ZoneActor actorInTile;
    [NonSerialized]
    protected ZoneActor threateningActor;

    public Tile(TileDef def, Vector2S position)
    {
        this.definition = def;
        this.position = position;
        explored = false;
        visible = false;

    }

    [OnSerializing()]
    internal void OnSerialize(StreamingContext streamingContext)
    {
        this.tileDefIndex = tileDefIndex>=0 ? tileDefIndex : definition.GetIndex();
    }

    public void SetVisible(bool visible, ViewInterface view=null, Player player=null)
    {
        this.visible = visible;
        if (visible)
        {
            if (!explored && widget!=null)
            {
                widget.LookAt(view,player, player.GetRPG().getSkill(SK.PERCEPTION));
            }
            if (player!=null && this.actorInTile != null && this.actorInTile.IsInvisible())
            {
                this.actorInTile.StealthCheck(player, false);
            }
            explored = true;
        }
    }

    public void Scan(int bonus, ViewInterface view=null, Player player=null)
    {
        if (widget != null)
        {
            widget.LookAt(view, player, player.GetRPG().getSkill(SK.SCIENCE)+bonus);
        }
        if (player != null && this.actorInTile != null)
        {
            if (this.actorInTile.IsInvisible())
            {
                this.actorInTile.StealthCheck(player, true, bonus);
            }
        }
    }

    public virtual void AwarenessPass(Tile [][] tiles)
    {

    }

    public virtual int getSprite()
    {
        return definition.getSprite();
    }

    public virtual int getOverlay()
    {
       return definition.getOverlay();
    }

    public TileDef getDefinition()
    {
        return definition;
    }

    public void SetDefinition(TileDef definition)
    {
        this.definition = definition;
    }

    public int GetDefinitionIndex()
    {
        return this.tileDefIndex;
    }

    public bool isExplored()
    {
        return explored;
    }

    public bool isVisible()
    {
        return visible;
    }
    public Widget GetWidget()
    {
        return widget;
    }

    public void SetActor(ZoneActor actor)
    {
        this.actorInTile = actor;
    }

    public void SetThreateningActor(ZoneActor actor)
    {
        this.threateningActor = actor;
    }

    public ZoneActor GetActorInTile()
    {
        return actorInTile;
    }

    public ZoneActor GetThreateningActor()
    {
        return this.threateningActor;
    }

    public void SetWidget(Widget widget)
    {
        this.widget = widget;
    }

    public bool IsVisionBlocking()
    {
        if (widget != null)
        {
            return widget.IsVisionBlocking();
        }
        return definition.getVision() != TileVision.EMPTY;
    }

    public bool canWalk()
    {
        if (this.widget != null && !this.widget.IsWalkable())
        {
            return false;
        }

        return this.definition.getMovement() == TileMovement.WALK;
    }

    internal bool canEnter(TileMovement tileMovement)
    {
        if (this.widget != null && !this.widget.IsWalkable())
        {
            return false;
        }
        return tileMovement>= this.definition.getMovement();
    }

    public virtual int buildGeometry(Vector3[] vertices, Vector2[] uvmap, Vector2 position, Vector2 uvIncrement, int tilesetWidth, int vcount)
    {
       
        int sprite = getSprite()-1;
        int indexOffset = vcount * 4;
        vertices[indexOffset + 0] = new UnityEngine.Vector3(position.x, position.y, 0);
        vertices[indexOffset + 1] = new UnityEngine.Vector3(position.x + 1, position.y, 0);
        vertices[indexOffset + 2] = new UnityEngine.Vector3(position.x, position.y + 1, 0);
        vertices[indexOffset + 3] = new UnityEngine.Vector3(position.x + 1, position.y + 1, 0);
        int yimg = ((sprite) / tilesetWidth) + 1;
        int ximg = (sprite) % tilesetWidth;

        float xcoord = ((ximg) * uvIncrement.x);
        float ycoord = 1 - ((yimg) * uvIncrement.y);
        uvmap[indexOffset + 0] = new UnityEngine.Vector2(xcoord, ycoord);
        uvmap[indexOffset + 1] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord);
        uvmap[indexOffset + 2] = new UnityEngine.Vector2(xcoord, ycoord + uvIncrement.y);
        uvmap[indexOffset + 3] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord + uvIncrement.y);

        vcount++;
        if (getDefinition().getDecoration()>0)
        {
            sprite = getDefinition().getDecoration()-1;
            yimg = ((sprite) / tilesetWidth) + 1;
            ximg = (sprite) % tilesetWidth;

            vertices[indexOffset + 4] = new UnityEngine.Vector3(position.x, position.y, -0.1F);
            vertices[indexOffset + 5] = new UnityEngine.Vector3(position.x + 1, position.y, -0.1F);
            vertices[indexOffset + 6] = new UnityEngine.Vector3(position.x, position.y + 1, -0.1F);
            vertices[indexOffset + 7] = new UnityEngine.Vector3(position.x + 1, position.y + 1, -0.1F);

            xcoord = ((ximg) * uvIncrement.x);
            ycoord = 1 - ((yimg) * uvIncrement.y);

            uvmap[indexOffset + 4] = new UnityEngine.Vector2(xcoord, ycoord);
            uvmap[indexOffset + 5] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord);
            uvmap[indexOffset + 6] = new UnityEngine.Vector2(xcoord, ycoord + uvIncrement.y);
            uvmap[indexOffset + 7] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord + uvIncrement.y);

            vcount++;
        }
        return vcount;
    }

    internal void Restore(ZoneTileset zoneTileset)
    {
        if (definition == null)
        {
            definition = zoneTileset.getDefinition(tileDefIndex);
        }
    }
}
