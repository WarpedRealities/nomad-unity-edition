﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SuperSmartTile : SmartTile
{
    private int leftSprite, rightSprite;

    public SuperSmartTile(TileDef def, Vector2S position) : base(def, position)
    {
        leftSprite = def.getSprite();
        rightSprite = def.getSprite();
    }

    public override void AwarenessPass(Tile[][] tiles)
    {
        leftSprite = definition.getSprite();
        rightSprite = definition.getSprite();
        leftSprite += SmartTileTool.getSuperAwarenessOffsetLeft(definition, tiles, position);
        rightSprite += SmartTileTool.getSuperAwarenessOffsetRight(definition, tiles, position);
        maskOverlay = definition.getOverlay() + SmartTileTool.getAwarenessOffset(definition, tiles, position);
    }
    public override int getSprite()
    {
        return leftSprite;
    }
    public override int getOverlay()
    {
        return maskOverlay;
    }

    public override int buildGeometry(Vector3[] vertices, Vector2[] uvmap, Vector2 position, Vector2 uvIncrement, int tilesetWidth, int vcount)
    {

        int left = leftSprite-1;
        int right = rightSprite-1;
        int indexOffset = vcount * 4;
        vertices[indexOffset + 0] = new UnityEngine.Vector3(position.x, position.y, 0);
        vertices[indexOffset + 1] = new UnityEngine.Vector3(position.x + 0.5F, position.y, 0);
        vertices[indexOffset + 2] = new UnityEngine.Vector3(position.x, position.y + 1, 0);
        vertices[indexOffset + 3] = new UnityEngine.Vector3(position.x + 0.5F, position.y + 1, 0);
       
        vertices[indexOffset + 4] = new UnityEngine.Vector3(position.x + 0.5F, position.y, 0);
        vertices[indexOffset + 5] = new UnityEngine.Vector3(position.x + 1, position.y, 0);
        vertices[indexOffset + 6] = new UnityEngine.Vector3(position.x + 0.5F, position.y + 1, 0);
        vertices[indexOffset + 7] = new UnityEngine.Vector3(position.x + 1, position.y + 1, 0);

        int yimg = ((left) / tilesetWidth) + 1;
        int ximg = (left) % tilesetWidth;

        float xcoord = ((ximg) * uvIncrement.x);
        float ycoord = 1 - ((yimg) * uvIncrement.y);
        uvmap[indexOffset + 0] = new UnityEngine.Vector2(xcoord + (uvIncrement.x / 32), ycoord);
        uvmap[indexOffset + 1] = new UnityEngine.Vector2(xcoord + (uvIncrement.x/2), ycoord);
        uvmap[indexOffset + 2] = new UnityEngine.Vector2(xcoord + (uvIncrement.x / 32), ycoord + uvIncrement.y);
        uvmap[indexOffset + 3] = new UnityEngine.Vector2(xcoord + (uvIncrement.x/2), ycoord + uvIncrement.y);

        yimg = ((right) / tilesetWidth) + 1;
        ximg = (right) % tilesetWidth;
        xcoord = ((ximg) * uvIncrement.x);
        ycoord = 1 - ((yimg) * uvIncrement.y);
        uvmap[indexOffset + 4] = new UnityEngine.Vector2(xcoord + (uvIncrement.x / 2), ycoord);
        uvmap[indexOffset + 5] = new UnityEngine.Vector2(xcoord + (uvIncrement.x), ycoord);
        uvmap[indexOffset + 6] = new UnityEngine.Vector2(xcoord + (uvIncrement.x / 2), ycoord + uvIncrement.y);
        uvmap[indexOffset + 7] = new UnityEngine.Vector2(xcoord + (uvIncrement.x), ycoord + uvIncrement.y);

        vcount += 2;
  
        return vcount;
    }
}
