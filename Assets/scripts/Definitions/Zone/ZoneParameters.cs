﻿
using System;
using UnityEngine;

[Serializable]
public class ZoneParameters
{
    private int width;
    private int height;
    private string spriteSet;

    public ZoneParameters(int width, int height, string spriteSet)
    {
        this.width = width;
        this.height = height;
        this.spriteSet = spriteSet;
    }

    public int GetWidth()
    {
        return width;
    }

    public int GetHeight()
    {
        return height;
    }

    public string getSpriteSet()
    {
        return spriteSet;
    }
}