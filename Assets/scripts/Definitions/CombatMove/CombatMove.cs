﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

[Serializable]
public class CombatMove 
{
    private string name;
    private AttackPattern pattern;
    private MoveType moveType;
    private MoveIndicator moveIndicator;
    private SK skill;
    private int bonus, repeatMove;
    private float rangeDecay;
    private InterruptType interrupt;
    private int icon;

    private int actionCost, ammoCost, ammoPool;


    private bool basicAction, threatening;
    private int timeCost, postActionDelay;
    private List<string> hitTexts, missTexts;
    private List<Effect> effects;

    public CombatMove Clone()
    {
        CombatMove move = new CombatMove();
        move.name = name;
        move.pattern = pattern;
        move.moveType = moveType;
        move.moveIndicator = moveIndicator;
        move.skill = skill;
        move.bonus = bonus;
        move.rangeDecay = rangeDecay;
        move.interrupt = interrupt;
        move.actionCost = actionCost;
        move.ammoCost = ammoCost; move.ammoPool = ammoPool;
        move.basicAction = basicAction; move.threatening = threatening;
        move.timeCost = timeCost; move.postActionDelay = postActionDelay;
        move.hitTexts = hitTexts;
        move.icon = icon;
        move.repeatMove = repeatMove;
        move.missTexts = missTexts;
        move.effects = new List<Effect>();
        for (int i = 0; i < effects.Count; i++)
        {
            move.effects.Add(effects[i].Clone());
        }
        return move;
    }
    public CombatMove()
    {

    }


    public void SetName(string name)
    {
        this.name = name;
    }

    public string GetName()
    {
        return this.name;
    }

    internal void setRangeDecay(float rangedecay)
    {
       this.rangeDecay = rangedecay;
    }

    public void SetPattern(AttackPattern pattern)
    {
        this.pattern = pattern;
    }

    public void SetTimeCost(int timeCost)
    {
        this.timeCost = timeCost;
    }

    public bool IsBasic()
    {
        return this.basicAction;
    }

    public int GetTimeCost()
    {
        return this.timeCost;
    }

    public void SetDelay(int delay)
    {
        this.postActionDelay = delay;
    }

    public void SetSkill(SK skill)
    {
        this.skill = skill;
    }

    public int GetAmmoPool()
    {
        return ammoPool;
    }

    public SK GetSkill()
    {
        return skill;
    }

    public int GetBonus()
    {
        return bonus;
    }

    public int GetAPCost()
    {
        return actionCost;
    }

    public int GetAmmoCost()
    {
        return ammoCost;
    }
    public void SetBonus(int bonus)
    {
        this.bonus = bonus;
    }

    public void SetType(MoveType type)
    {
        this.moveType = type;
    }
    public MoveType GetMoveType()
    {
        return this.moveType;
    }

    public int GetDelay()
    {
        return this.postActionDelay;
    }

    public void SetBasic(bool basic)
    {
        this.basicAction = basic;
    }

    public void SetIcon(int icon)
    {
        this.icon = icon;
    }

    public int GetIcon()
    {
        return this.icon;
    }

    public List<Effect> getEffects()
    {
        return this.effects;
    }

    public void setActionCost(int actionCost)
    {
        this.actionCost = actionCost;
    }

    public void setAmmoCost(int ammoCost)
    {
        this.ammoCost = ammoCost;
    }

    internal float GetRangeDecay()
    {
        return rangeDecay;
    }

    public MoveIndicator GetMoveIndicator()
    {
        return moveIndicator;
    }

    public void setAmmoPool(int ammoPool)
    {
        this.ammoPool = ammoPool;
    }

    public void SetRepeat(int repeat)
    {
        this.repeatMove = repeat;
    }

    public int GetRepeat()
    {
        return repeatMove;
    }

    public List<string> getMissTexts()
    {
        return this.missTexts;
    }

    public List<string> getHitTexts()
    {
        return this.hitTexts;
    }

    public bool IsThreatening()
    {
        return threatening;
    }

    public void SetThreatening(bool threatening)
    {
        this.threatening = threatening;
    }

    public void SetInterruptType(InterruptType interruptType)
    {
        this.interrupt = interruptType;
    }

    public InterruptType GetInterruptType()
    {
        return interrupt;
    }

    public void SetHitTexts(List<string> list)
    {
        this.hitTexts = list;
    }

    public void SetMissTexts(List<string> list)
    {
        this.missTexts = list;
    }

    public void SetEffects(List<Effect> effects)
    {
        for (int i = 0; i < effects.Count; i++)
        {
            if (effects[i].GetEffectType() == EffectType.DAMAGE)
            {
                EffectDamage ed = (EffectDamage)effects[i];
                if ((int)ed.GetDamagetype() >= 3)
                {
                    moveIndicator = MoveIndicator.SEDUCE;
                }
                else
                {
                    moveIndicator = MoveIndicator.HIT;
                }
            }
        }
        this.effects = effects;
    }

    public AttackPattern GetPattern()
    {
        return pattern;
    }
}
