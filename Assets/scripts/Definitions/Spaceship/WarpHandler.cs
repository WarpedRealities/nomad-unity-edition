


using System;

[Serializable]
public class WarpHandler
{
    public int duration = 10000;
    public long jumpStart;
    public int destinationX;
    public int destinationY;
    public int direction;
    public string destinationName;

    public float stress;
    public float charge=0;
    public float chargeIncrement;
    public WarpState state = WarpState.DRAFT;

    internal bool CanAbort()
    {
        return state == WarpState.DRAFT || 
            state == WarpState.FAILED || 
            state == WarpState.CHARGING;
    }
}