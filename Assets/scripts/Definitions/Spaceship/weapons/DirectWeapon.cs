﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class WeaponVisuals
{
    public bool isBeam;
    public string spriteSheet;
    public float speed; //or beam duration?
    public float volleyInterval;

    public WeaponVisuals(bool isBeam, string sprite, float speed, float volley)
    {
        this.isBeam = isBeam;
        this.spriteSheet = sprite;
        this.speed = speed;
        this.volleyInterval = volley;
    }
}

[Serializable]
public class DirectWeapon : CommonWeapon
{
    WeaponVisuals weaponVisuals;
    int accuracyModifier;
    float rangeAccuracyModifier;

    public DirectWeapon(int range, int volley, int accMod, float rangeMod,
        int magazine, int reload,
        WeaponVisuals visuals, WeaponCost[] costs, WeaponEffect[] effects, Dictionary<string,AmmoModifier> modifiers)
    {
        this.range = range;
        this.volley = volley;
        this.magazine = magazine;
        this.reload = reload;
        this.accuracyModifier = accMod;
        this.rangeAccuracyModifier = rangeMod;
        this.weaponVisuals = visuals;
        this.weaponCosts = costs;
        this.weaponEffects = effects;
        this.ammoModifiers = modifiers;
    }

    internal override string GetDescription()
    {
        string r = (range * 100).ToString() + "k";
        string penalty = rangeAccuracyModifier + " per 100k";
        return "range: " + r + " bonus:" + accuracyModifier + " range penalty:" + penalty + " rof:" + volley;  
    }

    public int GetAccuracyModifier()
    {
        return accuracyModifier;
    }

    internal float GetRangeAccuracyModifier(string ammoModifier)
    {
        if (ammoModifier != null && ammoModifiers.ContainsKey(ammoModifier))
        {
            if (ammoModifiers[ammoModifier].modifierChanges.ContainsKey("RANGEMOD"))
            {
                return float.Parse(ammoModifiers[ammoModifier].modifierChanges["RANGEMOD"]);
            }
        }
        return rangeAccuracyModifier;
    }

    public WeaponVisuals GetWeaponVisuals()
    {
        return weaponVisuals;
    }

    internal string GetSpriteSheet(Accept accept)
    {
        if (accept != null && ammoModifiers.ContainsKey(accept.GetCode()))
        {
            if (ammoModifiers[accept.GetCode()].modifierChanges.ContainsKey("SPRITE"))
            {
                return (ammoModifiers[accept.GetCode()].modifierChanges["SPRITE"]);
            }
        }
        return weaponVisuals.spriteSheet;
    }

}
