﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public abstract class WeaponEffect
{
    internal abstract string GetDescription();

    internal abstract WeaponEffect GetModdedClone(Accept accept, Dictionary<string, AmmoModifier> modifiers);
}
