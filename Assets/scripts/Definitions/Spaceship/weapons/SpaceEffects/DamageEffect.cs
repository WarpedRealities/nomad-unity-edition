﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class DamageEffect : WeaponEffect
{
    DT damageType;
    int minDamage, maxDamage;
    float rangeDecay;

    public DamageEffect(DT t, int min, int max, float rangeDecay)
    {
        this.damageType = t;
        this.minDamage = min;
        this.maxDamage = max;
        this.rangeDecay = rangeDecay;
    }

    public DT GetDamageType()
    {
        return damageType;
    }

    public float GetRangeDecay()
    {
        return rangeDecay;
    }

    public int GetMinDamage()
    {
        return minDamage;
    }

    public int GetMaxDamage()
    {
        return maxDamage;
    }

    private string GetDamageString()
    {
        if (minDamage < maxDamage)
        {
            return minDamage + "-" + maxDamage;
        }
        return maxDamage.ToString();
    }

    internal override string GetDescription()
    {
        switch (damageType)
        {
            case DT.NORMAL:
                return "dmg:" + GetDamageString() + BuildDecayString();
        }
        return "";
    }

    private string BuildDecayString()
    {
        if (rangeDecay == 0)
        {
            return "";
        }
        return " -" + rangeDecay + " per 100km";
    }

    internal override WeaponEffect GetModdedClone(Accept accept, Dictionary<string, AmmoModifier> modifiers)
    {
        if (accept != null && modifiers.ContainsKey(accept.GetCode()))
        {
            if (modifiers[accept.GetCode()].modifierChanges.ContainsKey("DMG"))
            {
                return new DamageEffect(damageType, minDamage, int.Parse(modifiers[accept.GetCode()].modifierChanges["DMG"]),rangeDecay);
            }
        }
        return this;
    }
}
