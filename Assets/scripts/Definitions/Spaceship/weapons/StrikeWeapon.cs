using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class StrikerVisuals
{
    string spriteSheet;

    public StrikerVisuals(string spritesheet)
    {
        this.spriteSheet = spritesheet;
    }

    public string GetSpriteSheet()
    {
        return spriteSheet;
    }
}


[Serializable]
public class StrikeWeapon : CommonWeapon
{
    StrikerVisuals strikerVisuals;
    int lifespan, skill, defence, health;
    float speed;

    public StrikeWeapon(float speed, int lifespan, int skill, int defence, int health, int volley, int range,
        int magazine, int reload,
        StrikerVisuals visuals, WeaponCost[] costs, WeaponEffect[] effects, Dictionary<string, AmmoModifier> modifiers)
    {
        this.speed = speed;
        this.lifespan = lifespan;
        this.skill = skill;
        this.defence = defence;
        this.health = health;
        this.volley = volley;
        this.range = range;
        this.magazine = magazine;
        this.reload = reload;
        this.strikerVisuals = visuals;
        this.weaponCosts = costs;
        this.weaponEffects = effects;
        this.ammoModifiers = modifiers;
    }

    internal int GetAttack(Accept accept)
    {
        if (accept != null && ammoModifiers.ContainsKey(accept.GetCode()))
        {
            if (ammoModifiers[accept.GetCode()].modifierChanges.ContainsKey("SKILL"))
            {
                return int.Parse(ammoModifiers[accept.GetCode()].modifierChanges["SKILL"]);
            }
        }
        return this.skill;
    }

    internal int GetDefence(Accept accept)
    {
        if (accept != null && ammoModifiers.ContainsKey(accept.GetCode()))
        {
            if (ammoModifiers[accept.GetCode()].modifierChanges.ContainsKey("DEFENCE"))
            {
                return int.Parse(ammoModifiers[accept.GetCode()].modifierChanges["DEFENCE"]);
            }
        }
        return this.defence;
    }

    internal override string GetDescription()
    {
        string r = (range * 100).ToString() + "k";
        return "range: " + r + " skill:" + skill + " defence:" + defence + " lifespan:"+lifespan+ " health"+ health+ " speed"+speed+ " rof:" + volley;
    }

    internal int GetLifespan(Accept accept)
    {
        if (accept != null && ammoModifiers.ContainsKey(accept.GetCode()))
        {
            if (ammoModifiers[accept.GetCode()].modifierChanges.ContainsKey("LIFE"))
            {
                return int.Parse(ammoModifiers[accept.GetCode()].modifierChanges["LIFE"]);
            }
        }
        return lifespan;
    }

    internal float GetSpeed(Accept accept)
    {

        if (accept != null && ammoModifiers.ContainsKey(accept.GetCode()))
        {
            if (ammoModifiers[accept.GetCode()].modifierChanges.ContainsKey("SPEED"))
            {
                return float.Parse(ammoModifiers[accept.GetCode()].modifierChanges["SPEED"]);
            }
        }
        return speed;
    }

    internal string GetSpriteSheet(Accept accept)
    {
        if (accept != null && ammoModifiers.ContainsKey(accept.GetCode()))
        {
            if (ammoModifiers[accept.GetCode()].modifierChanges.ContainsKey("SPRITE"))
            {
                return (ammoModifiers[accept.GetCode()].modifierChanges["SPRITE"]);
            }
        }
        return strikerVisuals.GetSpriteSheet();
    }
}
