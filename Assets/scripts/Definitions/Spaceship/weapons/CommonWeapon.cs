﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;

[Serializable]
public abstract class WeaponCost {

    protected int amount;

    public int GetAmount()
    {
        return amount;
    }

    public abstract string GetDescription();
}
[Serializable]
public class ResourceCost : WeaponCost
{
    private string resource;
    private string displayName;

    public ResourceCost(string res, string displayName, int amount)
    {
        this.displayName = displayName;
        this.amount = amount;
        this.resource = res;
    }

    public override string GetDescription()
    {
        return amount +" "+ displayName;
    }

    public string GetDisplayName()
    {
        return displayName;
    }

    public string GetResource()
    {
        return resource;
    }
}

[Serializable]
public class Accept
{
    string code;
    string displayName;

    public Accept(string code, string displayName)
    {
        this.code = code;
        this.displayName = displayName;
    }

    public string GetCode()
    {
        return code;
    }

    public string GetDisplayName()
    {
        return displayName;
    }
}

[Serializable]
public class MagazineCost : WeaponCost
{
    private Accept[] accept;
    private string shortSum;
    public MagazineCost(int amount, string shortSum)
    {
        this.shortSum = shortSum;
        this.amount = amount;
    }

    public string GetShortSum()
    {
        return shortSum;
    }

    public void SetAccept(Accept[] accept)
    {
        this.accept = accept;
    }

    public Accept[] GetAccept()
    {
        return accept;
    }

    public override string GetDescription()
    {
        return amount + " " + shortSum;
    }
}

[Serializable]
public class AmmoModifier
{
    public string ammoCode;
    public Dictionary<string, string> modifierChanges;

    public AmmoModifier(string code, Dictionary<string, string> modifiers)
    {
        this.ammoCode = code;
        this.modifierChanges = modifiers;
    }
}

[Serializable]
public abstract class CommonWeapon 
{

    protected int facing;
    protected FIRINGARC arc;
    protected Vector2FS emitter;
    protected int range;
    protected int volley;
    protected int magazine;
    protected int reload;
    protected WeaponCost[] weaponCosts;
    protected WeaponEffect[] weaponEffects;
    protected Dictionary<string, AmmoModifier> ammoModifiers;
    public virtual bool IsReady()
    {
        return true;
    }

    internal FIRINGARC GetArc()
    {
        return arc;
    }

    internal abstract string GetDescription();

    public Dictionary<string, AmmoModifier> GetAmmoModifiers()
    {
        return ammoModifiers;
    }

    public string GetEffect()
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < weaponEffects.Length; i++)
        {
            stringBuilder.Append(weaponEffects[i].GetDescription());
            if (weaponEffects.Length>1 && i < weaponEffects.Length - 1)
            {
                stringBuilder.Append(",");
            }
        }
        return stringBuilder.ToString();
    }

    public float GetRange(string ammoModifier)
    {
        if (ammoModifier!=null && ammoModifiers.ContainsKey(ammoModifier))
        {
            if (ammoModifiers[ammoModifier].modifierChanges.ContainsKey("RANGE"))
            {
                return float.Parse(ammoModifiers[ammoModifier].modifierChanges["RANGE"]);
            }
        }
        return range;
    }

    public int GetVolley()
    {
        return volley;
    }

    public string GetCosts()
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < weaponCosts.Length; i++)
        {
            stringBuilder.Append(weaponCosts[i].GetDescription());
            if (weaponCosts.Length > 1 && i < weaponCosts.Length - 1)
            {
                stringBuilder.Append(",");
            }
        }
        return stringBuilder.ToString();
    }

    public WeaponCost[] GetWeaponCosts()
    {
        return weaponCosts;
    }

    public WeaponEffect [] GetWeaponEffects()
    {
        return weaponEffects;
    }

    public WeaponEffect[] GetWeaponEffects(Accept accept)
    {
        WeaponEffect[] effects = new WeaponEffect[weaponEffects.Length]; 
        for (int i = 0; i < weaponEffects.Length; i++)
        {
            effects[i] = weaponEffects[i].GetModdedClone(accept, ammoModifiers);
        }
        return effects;
    }

    public Vector2FS GetEmitter()
    {
        return emitter;
    }

    internal void Setup(FIRINGARC arc0, FIRINGARC arc1, int facing, Vector2FS emitter)
    {
        int arcValue = ((int)arc0) + ((int)arc1);
        arcValue = arcValue < -1 ? -1 : arcValue;
        arc = (FIRINGARC)arcValue;
        this.facing = facing;
        this.emitter = emitter;
    }

    public void SetEmitter(Vector2FS emitter)
    {
        this.emitter = emitter;
    }

    internal int GetMagazine()
    {
        return magazine;
    }

    internal int GetReload()
    {
        return reload;
    }

    internal bool HasMagazineCosts()
    {
        if (weaponCosts != null)
        {
            for (int i = 0; i < weaponCosts.Length; i++)
            {
                if (weaponCosts[i] is MagazineCost)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public MagazineCost GetMagazineCost()
    {
        for (int i = 0; i < weaponCosts.Length; i++)
        {
            if (weaponCosts[i] is MagazineCost)
            {
                return (MagazineCost)weaponCosts[i];
            }
        }
        return null;
    }
}
