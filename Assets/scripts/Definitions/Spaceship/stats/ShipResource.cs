﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ShipResource
{
    public string resource,displayName;
    public float amount, originalAmount;
    public int capacity;

}
[Serializable]
public class ShipMagazine
{
    public string containedItem;
    public int amount, originalAmount;

    public string GetAmmo()
    {
        if (containedItem != null)
        {
            return GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(containedItem).GetName();
        }
        return "nothing";
    }
}

[Serializable]
public class ShipConverter
{
    private ConversionSystem conversionSystem;
    private bool active;
    private string fromResource, toResource;
    private float intakePerTick, outputPerTick;

    public ShipConverter(ConversionSystem system)
    {
        conversionSystem = system;
        active = system.IsActive();
        fromResource = system.GetFrom();
        toResource = system.GetTo();
        intakePerTick = system.GetRate();
        outputPerTick = system.GetRate() * system.GetRatio();
    }

    public ConversionSystem GetSystem()
    {
        return conversionSystem;
    }

    public string GetFrom()
    {
        return fromResource;
    }
    public string GetTo()
    {
        return toResource;
    }
    public float GetIntake()
    {
        return this.intakePerTick;
    }
    public float GetOutput()
    {
        return this.outputPerTick;
    }

    public bool IsActive()
    {
        return active;
    }

    public void SetActive(bool active)
    {
        this.active = active;
    }

}