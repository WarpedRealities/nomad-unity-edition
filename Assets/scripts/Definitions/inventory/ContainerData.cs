﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ContainerData : ItemList
{

    private int capacity;

    private int openSprite;
    private bool opened = false;

    public ContainerData(int capacity, List<Item> items, int openSprite=-1) : base(items)
    {
        this.openSprite = openSprite;
        this.capacity = capacity;
        CalculateWeight();
        
    }

    public ContainerData(int capacity, int openSprite = -1)
    {
        this.openSprite = openSprite;
        this.capacity = capacity;
    }

    public void SetItems(List<Item> items)
    {
        this.items = items;
        CalculateWeight();
    }

    internal void Open()
    {
        this.opened = true;
    }

    public int GetCapacity()
    {
        return capacity;
    }

    public int GetOpenSprite()
    {
        return openSprite;
    }

    public bool IsOpened()
    {
        return opened;
    }

    internal ContainerData Clone()
    {
        return new ContainerData(this.capacity, new List<Item>(items), openSprite);
    }
}
