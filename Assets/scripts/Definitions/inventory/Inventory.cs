﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static System.Collections.Specialized.BitVector32;

[Serializable]
public class Inventory : ItemList
{
    private Item[] equipSlots = new Item[7];
    private int capacity;
    private SetFloat setEncumbrance;
    private int encumbranceLevel;
    private int gold;
    private int credits;

    public Inventory(int capacity, SetFloat setEncumbrance)
    {
        encumbranceLevel = 1;
        this.setEncumbrance = setEncumbrance;
        this.capacity = capacity;
        this.credits = 0000;
        this.gold = 100;
    }


    public int GetEncumbranceLevel()
    {
        return encumbranceLevel;
    }

    public int GetCapacity()
    {
        return capacity;
    }
    public void SetCapacity(int capacity)
    {
        this.capacity = capacity;
    }

    public void SetSlot(int i, Item item)
    {
        equipSlots[i] = item;
    }

    public Item GetSlot(int i)
    {
        return equipSlots[i];
    }

    public override void CalculateWeight()
    {
        weight = 0;
        for (int i = 0; i < items.Count; i++)
        {
            weight += items[i].GetWeight();
        }
        for (int i = 0; i < equipSlots.Length; i++)
        {
            if (equipSlots[i] != null)
            {
                weight += equipSlots[i].GetWeight();
            }       
        }
        encumbranceLevel = 1 + (int)(weight / capacity);
        setEncumbrance(encumbranceLevel);
    }

    public bool Contains(Item item)
    {
        if (items.Contains(item))
        {
            return true;
        }
        for (int i = 0; i < 7; i++)
        {
            if (equipSlots[i] == item)
            {
                return true;
            }
        }

        return false;
    }

    public void EquipItem(Item item)
    {
        ItemEquippable itemEquippable = (ItemEquippable)item.GetDef();
        equipSlots[(int)itemEquippable.GetSlot()] = item;
        RemoveItem(item,true);
        CalculateWeight();
    }

    public void HandleEquipMods(Item item, PlayerRPG player)
    {
        ItemEquippable itemEquippable = (ItemEquippable)item.GetDef();
        if (itemEquippable.GetDefenceModifiers() != null)
        {
            (player).ApplyDefenceMod(itemEquippable.GetDefenceModifiers());
        }
        if (itemEquippable.GetSkillModifiers() != null)
        {
            (player).ApplySkillMod(itemEquippable.GetSkillModifiers());

        }
        if (itemEquippable.GetMoves() != null)
        {
            (player).UpdateMoves();
        }

        if (itemEquippable.GetHazardProtections() != null)
        {
            (player).UpdateHazardProtection();
        }
        Player pl = GlobalGameState.GetInstance().GetPlayer();
        if (pl != null)
        {
            pl.GetQuickSlotHandler().UpdateQuickSlots(this,
            (player).GetMoves());
        }

    }

    public void HandleUnequipMods(Item item, PlayerRPG player)
    {
        ItemEquippable itemEquippable = (ItemEquippable)item.GetDef();
        if (itemEquippable.GetDefenceModifiers() != null)
        {
            (player).RemoveDefenceMod(itemEquippable.GetDefenceModifiers());
        }
        if (itemEquippable.GetSkillModifiers() != null)
        {
            (player).RemoveSkillMod(itemEquippable.GetSkillModifiers());
        }
        if (itemEquippable.GetMoves() != null)
        {
            (player).UpdateMoves();

        }
        if (itemEquippable.GetHazardProtections() != null)
        {
            (player).UpdateHazardProtection();
        }
        Player pl = GlobalGameState.GetInstance().GetPlayer();
        if (pl != null)
        {
            pl.GetQuickSlotHandler().UpdateQuickSlots(this,
            (player).GetMoves());
        }
    }

    public void UnEquipItem(Item item)
    {
        ItemEquippable itemEquippable = (ItemEquippable)item.GetDef();
        equipSlots[(int)itemEquippable.GetSlot()] = null;
        AddItem(item,true);
        CalculateWeight();
    }

    public override void AddItem(Item item, bool moveStack = false)
    {
        if (item.GetDef().GetItemType() == ItemType.CURRENCY)
        {
            ItemCurrency def = (ItemCurrency) item.GetDef();
            if (item is ItemStack)
            {
                ItemStack itemStack = (ItemStack)item;
                if (def.GetIsCredits())
                {
                    credits += itemStack.GetCount();
                }
                else
                {
                    gold+= itemStack.GetCount();
                }
            }
            else
            {
                if (def.GetIsCredits())
                {
                    credits++;
                }
                else
                {
                    gold++;
                }
            }
            return;
        }
       base.AddItem(item, moveStack);
    }

    public int GetGold()
    {
        return gold;
    }

    public int GetCredits()
    {
        return credits;
    }

    internal void SetGold(int amount)
    {
        gold = amount;
    }

    internal void SetCredits(int amount)
    {
        credits = amount;
    }

    internal int CountItem(string code)
    {

        int count = 0;
        for (int i = 0; i < items.Count; i++)
        {

            if (items[i].GetDef().GetCodeName().Equals(code))
            {

                if (items[i].GetItemClass() == ItemClass.STACK)
                {
                    ItemStack itemStack = (ItemStack)items[i];
                    count += itemStack.GetCount();
                }
                else if (items[i].GetItemClass() == ItemClass.AMMOSTACK)
                {
                    ItemAmmoStack ammoStack = (ItemAmmoStack)items[i];
                    count += ammoStack.GetCount();
                }
                else
                {

                    count++;
                }
            }
        }

        return count;
    }
}
