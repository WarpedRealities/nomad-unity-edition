﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ItemList 
{
    protected List<Item> items;
    protected float weight;

    public ItemList()
    {
        items = new List<Item>();
    }

    public ItemList(List<Item> items)
    {
        this.items = items;
    }

    public float GetWeight()
    {
        return weight;
    }

    public virtual void CalculateWeight()
    {
        weight = 0;
        for (int i = 0; i < items.Count; i++)
        {
            weight += items[i].GetWeight();
        }
    }

    public int GetItemCount()
    {
        return this.items.Count;
    }

    public Item GetItem(int index)
    {
        if (index<0|| index >= items.Count)
        {
            return null;
        }
        return this.items[index];
    }
    public Item GetItem(string code)
    {

        for (int i = 0; i < items.Count; i++)
        {

            if (items[i].GetDef().GetCodeName().Equals(code))
            {
                return items[i];
            }
        }

        return null;
    }
    public List<Item> GetItems()
    {
        return items;
    }

    public Item RemoveItem(int index)
    {
        return RemoveItem(items[index]);
    }

    public Item RemoveItem(Item item, bool takeStack = false)
    {
        if (takeStack || item.GetDef() is ItemCurrency)
        {
            items.Remove(item);
            CalculateWeight();
            return item;
        }

        if (item.GetItemClass() == ItemClass.STACK) {
            ItemStack itemStack = (ItemStack)item;
            itemStack.DecrementCount();
            if (itemStack.GetCount() <= 0)
            {
                items.Remove(itemStack);
            }
            CalculateWeight();
            return itemStack.TakeItem();
        } else if (item.GetItemClass() == ItemClass.AMMOSTACK)
        {
            ItemAmmoStack itemStack = (ItemAmmoStack)item;
            itemStack.SetCount(itemStack.GetCount()-1);
            if (itemStack.GetCount() <= 0)
            {
                items.Remove(itemStack);
            }
            CalculateWeight();
            return itemStack.TakeItem();
        }
        else
        {
            items.Remove(item);
            CalculateWeight();
            return item;
        }

    }


    public virtual void AddItem(Item item, bool moveStack=false)
    {

        if (moveStack)
        {
            int count = item is ItemStack ? ((ItemStack)item).GetCount() : 1;
            count = item is ItemAmmoStack ? ((ItemAmmoStack)item).GetCount() : count;
            if (!StackTool.HandleStacking(item, items, count))
            {
                items.Add(item);
            }
            return;
        }
        if (!StackTool.HandleStacking(item, items, 1))
        {
            if (item.GetItemClass() == ItemClass.STACK)
            {
                ItemStack itemStack = (ItemStack)item;
                items.Add(itemStack.TakeItem());
   
            }
            else if (item.GetItemClass() == ItemClass.AMMOSTACK)
            {
                ItemAmmoStack itemStack = (ItemAmmoStack)item;
                items.Add(itemStack.TakeItem());

            } else
            {
                items.Add(item);
            }

        }
        CalculateWeight();
    }

    public void AddItems(Item item, int count)
    {
        if (!StackTool.HandleStacking(item, items, count))
        {
            if (count == 1)
            {
                items.Add(item);
            }
            else
            {
                if (item.GetDef().GetItemType() == ItemType.AMMO)
                {
                    AmmoInstance itemAmmo = (AmmoInstance)item;
                    items.Add(new ItemAmmoStack((ItemAmmo)item.GetDef(), count,itemAmmo.GetResource().GetAmmoType()));
                }
                else
                {
                    items.Add(new ItemStack(item.GetDef(), count));
                }

            }
        }

        CalculateWeight();
    }
}
