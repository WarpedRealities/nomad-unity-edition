using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectStatusRemove : Effect
{
	int[] statusRemovals;
	string description;

	public EffectStatusRemove(int [] statusRemovals, string description)
	{
		this.description = description;
		this.statusRemovals = statusRemovals;
		this.effectType = EffectType.STATUSREMOVE;
	}

	internal override Effect Clone()
	{
		return new EffectStatusRemove(statusRemovals, description);
	}


	public string GetDescription()
	{
		return description;
	}


	public int[] GetStatusRemovals()
	{
		return statusRemovals;
	}
}
