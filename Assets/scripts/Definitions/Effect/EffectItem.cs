using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EffectItem : Effect
{
    String itemCode;
    
    public EffectItem(String itemCode, bool onMiss)
    {
        this.itemCode = itemCode;
        this.onMiss = onMiss;
        this.effectType = EffectType.ITEM;
    }


    internal override Effect Clone()
    {
        EffectItem effectItem = new EffectItem(this.itemCode, onMiss);
        return effectItem;
    }

    internal string GetItem()
    {
        return itemCode;
    }
}