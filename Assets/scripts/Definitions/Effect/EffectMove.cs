﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EffectMove : Effect
{
    int strength;
    bool affectOrigin, moveAway, bypassActors;
    DR resistValue;
    int failDistance, succeedDistance;
    AB modifierAbility;

    public EffectMove(bool affectOrigin, bool moveAway, bool bypassActors, int strength, int failDistance, int succeedDistance, DR resistValue, AB modifierAbility)
    {
        effectType = EffectType.MOVE;
        this.affectOrigin = affectOrigin;
        this.moveAway = moveAway;
        this.bypassActors = bypassActors;
        this.resistValue = resistValue;
        this.modifierAbility = modifierAbility;
        this.strength = strength;
        this.failDistance = failDistance;
        this.succeedDistance = succeedDistance;
    }

    internal override Effect Clone()
    {
        return new EffectMove(affectOrigin, moveAway,bypassActors, strength, failDistance, succeedDistance, resistValue, modifierAbility);
    }

    public int GetStrength()
    {
        return strength;
    }
    public bool IsAffectOrigin()
    {
        return affectOrigin;
    }
    public bool IsMoveAway()
    {
        return moveAway;
    }
    public bool IsBypassActors()
    {
        return bypassActors;
    }
    public DR GetResist()
    {
        return this.resistValue;
    }
    public AB GetModifierAbility()
    {
        return this.modifierAbility;
    }

    public int GetFailDistance()
    {
        return failDistance;
    }

    public int GetSuccessDistance()
    {
        return succeedDistance;
    }

}
