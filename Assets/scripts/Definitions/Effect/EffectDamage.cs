﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EffectDamage : Effect
{
    int min, max;
    DR damageType;
    bool melee;
    AB modifierAbility;
    float modifierMultiplier, penetration;
    float rangeFalloff;

    public EffectDamage(int min, int max, DR type, AB modifier, bool melee, float modifierMultiplier, float penetration,float rangeFalloff=0)
    {
        this.effectType = EffectType.DAMAGE;
        this.max = max;
        this.min = min;
        this.damageType = type;
        this.modifierAbility = modifier;
        this.melee = melee;
        this.modifierMultiplier = modifierMultiplier;
        this.penetration = penetration;
        this.rangeFalloff = rangeFalloff;
    }

    internal override Effect Clone()
    {
        return new EffectDamage(min, max, damageType, modifierAbility, melee, modifierMultiplier, penetration, rangeFalloff);
    }
    public int GetMin()
    {
        return min;
    }

    public int GetMax()
    {
        return max;
    }

    public void SetMin(int value)
    {
        min = value;
    }

    public void SetMax(int value)
    {
        max = value;
    }

    public AB GetAbilityModifier()
    {
        return modifierAbility;
    }

    public float GetAbilityMultiplier()
    {
        return modifierMultiplier;
    }

    public float GetPenetration()
    {
        return penetration;
    }

    public DR GetDamagetype()
    {
        return damageType;
    }

    internal bool IsMelee()
    {
        return melee;
    }

    public float GetRangeFalloff()
    {
        return rangeFalloff;
    }

    internal void ModDecay(float mod)
    {
        rangeFalloff *= mod;
    }
}
