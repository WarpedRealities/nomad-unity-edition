﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EffectInterrupt : Effect
{
    DR resistType;
    InterruptType interruptType;
    int strength;
    int stunThreshold;
    int stunLength;


    public EffectInterrupt(DR resist, InterruptType interrupt, int strength, int threshold, int stun)
    {
        resistType = resist;
        interruptType = interrupt;
        this.strength = strength;
        this.stunLength = stun;
        this.stunThreshold = threshold;
        effectType = EffectType.INTERRUPT;
    }

    internal override Effect Clone()
    {
        return new EffectInterrupt(resistType, interruptType, strength, stunThreshold, stunLength);
    }

    public DR GetResist()
    {
        return resistType;
    }

    public InterruptType GetInterrupt()
    {
        return interruptType;
    }

    public int GetStrength()
    {
        return strength;
    }

    public int GetStun()
    {
        return stunLength;
    }

    public int GetThreshold()
    {
        return stunThreshold;
    }

}
