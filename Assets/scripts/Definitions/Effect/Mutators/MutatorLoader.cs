﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using System;

public class MutatorLoader 
{


    public static EffectMutator LoadMutator(XmlElement root)
    {
        XmlNodeList children = root.ChildNodes;
        string success = null;
        string failure = null;
        Mutation mutation = null;
        List<MutatorConditional> conditions = new List<MutatorConditional>();
        for (int i = 0; i < children.Count; i++) {

            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];


                if ("success".Equals(xmlElement.Name))
                {
                    success = xmlElement.InnerText;
                }

                if ("failure".Equals(xmlElement.Name))
                {
                    failure = xmlElement.InnerText;
                }
                if ("condition".Equals(xmlElement.Name))
                {
                    conditions.Add(BuildCondition(xmlElement));
                }
                if ("mutation".Equals(xmlElement.Name))
                {
                    mutation = BuildMutation(xmlElement);
                }
            }
        }

        return new EffectMutator(conditions, mutation, success,failure);
    }

    public static Mutation BuildMutation(XmlElement xmlElement)
    {
        if ("partMutation".Equals(xmlElement.GetAttribute("type")))
        {

            return new PartMutation(xmlElement.GetAttribute("part"), "true".Equals(xmlElement.GetAttribute("remove")));
        }
        if ("modifyMutation".Equals(xmlElement.GetAttribute("type")))
        {
            int change = int.Parse(xmlElement.GetAttribute("mod"));
            int sizeAdjustment = 0;
            int.TryParse(xmlElement.GetAttribute("adjust"), out sizeAdjustment);
            int minimum = 0;
            int.TryParse(xmlElement.GetAttribute("min"), out minimum);
            string part = xmlElement.GetAttribute("part");
            string variable = xmlElement.GetAttribute("variable");
            return new ModifyMutation(change, sizeAdjustment, minimum, part, variable);
        }
        if ("setMutation".Equals(xmlElement.GetAttribute("type")))
        {
            int value = int.Parse(xmlElement.GetAttribute("value"));
            string part = xmlElement.GetAttribute("part");
            string variable = xmlElement.GetAttribute("variable");
            return new SetMutation(value, part, variable);
        }
        Debug.Log("no mutation" + xmlElement.GetAttribute("type"));
        return null;
    }

    private static MutatorConditional BuildCondition(XmlElement xmlElement)
    {
        if ("hasPart".Equals(xmlElement.GetAttribute("type")))
        {
            return new HasPartConditional(xmlElement.GetAttribute("part"), "true".Equals(xmlElement.GetAttribute("negative")));
        }
        if ("variable".Equals(xmlElement.GetAttribute("type")))
        {
            return new VariableConditional(xmlElement.GetAttribute("variable"), xmlElement.GetAttribute("part"), 
                int.Parse(xmlElement.GetAttribute("value")), xmlElement.GetAttribute("comparison"));
        }
        return null;
    }
}
