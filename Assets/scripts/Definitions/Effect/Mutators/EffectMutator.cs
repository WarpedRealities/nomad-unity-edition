﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectMutator : Effect
{
    List<MutatorConditional> conditions;
    Mutation mutation;
    string successString, failureString;

    public EffectMutator(List<MutatorConditional> conditions, Mutation mutation, string success, string failure)
    {
        this.effectType = EffectType.MUTATOR;
        this.conditions = conditions;
        this.mutation = mutation;
        this.successString = success;
        this.failureString = failure;
    }

    public List<MutatorConditional> Getconditions()
    {
        return conditions;
    }

    public Mutation GetMutation()
    {
        return mutation;
    }

    public string GetSuccess()
    {
        return successString;
    }

    public string GetFailure()
    {
        return failureString;
    }

    internal override Effect Clone()
    {
        throw new System.NotImplementedException();
    }
}
