﻿using UnityEngine;
using System.Collections;

public interface MutatorConditional 
{

    bool MetCriteria(Appearance appearance);
}

public class HasPartConditional : MutatorConditional
{
    string part;
    bool negative;

    public HasPartConditional(string part, bool negative)
    {
        this.part = part;
        this.negative = negative;
    }

    public bool MetCriteria(Appearance appearance)
    {
        if (!negative && appearance.GetPart(part) != null)
        {
            return true;
        }
        if (negative && appearance.GetPart(part) == null)
        {
            return true;
        }
        return false;
    }
}

public class VariableConditional : MutatorConditional
{
    string variable, part;
    int value;
    int comparator;

    public VariableConditional(string variable, string part, int value, string comparator)
    {
        this.variable = variable;
        this.value = value;
        this.part = part;
        if ("lessthan".Equals(comparator))
        {
            this.comparator = -1;
        }
        else if ("greaterthan".Equals(comparator))
        {
            this.comparator = 1;
        }
        else
        {
            this.comparator = 0;
        }
    }

    public bool MetCriteria(Appearance appearance)
    {
        BodyPart part = appearance.GetPart(this.part);
        int comparison = part.GetValue(variable);
        switch (comparator)
        {
            case 1:
                return comparison > value;
            case 0:
                return comparison == value;
            case -1:
                return comparison < value;
        }
        return false;
    }
}

public interface Mutation
{
    int ApplyMutation(Appearance appearance);
}

public class PartMutation : Mutation
{
    string part;
    bool removePart;

    public PartMutation(string part, bool removePart)
    {
        this.part = part;
        this.removePart = removePart;
    }


    public int ApplyMutation(Appearance appearance)
    {
        Debug.Log("apply mutation " + part + " " + removePart);
        if (this.removePart)
        {
            appearance.RemovePart(part);
        }
        else
        {
            appearance.AddPart(BodyPartLoader.LoadPart(part));
        }

        return 0;
    }
}

public class SetMutation : Mutation
{
    int value;
    string part, variable;

    public SetMutation(int value, string part, string variable)
    {
        this.value = value;
        this.part = part;
        this.variable = variable;
    }

    public int ApplyMutation(Appearance appearance)
    {
        BodyPart bodyPart = appearance.GetPart(part);

        bodyPart.SetValue(variable, value);
        return 0;
    }
}

public class ModifyMutation : Mutation
{
    int change, sizeAdjustment, minimum;
    string part, variable;

    public ModifyMutation(int change, int sizeAdjustment, int minimum, string part, string variable)
    {
        this.change = change;

        this.sizeAdjustment = sizeAdjustment;
        this.minimum = minimum;
        this.part = part;
        this.variable = variable;
    }


    public int ApplyMutation(Appearance appearance)
    {
      
        BodyPart bodyPart = appearance.GetPart(part);
        int originalValue = bodyPart.GetValue(variable);
        int changeAmount = change;

        if (sizeAdjustment > 0)
        {
            changeAmount += change > 0 ? (originalValue / sizeAdjustment) * -1 : (originalValue / sizeAdjustment) * 1;
        }
        bodyPart.SetValue(variable, originalValue + changeAmount);
        return changeAmount;
    }
}


