﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EffectRecover : Effect
{
    private ST stat;
    private float amount;
    private string effectText;


    public EffectRecover(ST stat, float amount, string text)
    {
        this.effectType = EffectType.RECOVER;
        this.stat = stat;
        this.amount = amount;
        this.effectText = text;
    }

    internal override Effect Clone()
    {
        return new EffectRecover(stat, amount, effectText);
    }
    public ST GetStat()
    {
        return stat;
    }

    public float GetModifier()
    {
        return amount;
    }

    public string GetEffectText()
    {
        return effectText;
    }

}
