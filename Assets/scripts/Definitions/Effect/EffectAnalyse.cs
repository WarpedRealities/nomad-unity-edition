using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EffectAnalyse : Effect
{
    int[] healthThresholds;
    int[] resolveThresholds;
    int[] defenceThresholds;
    int[] resistThresholds;

    public EffectAnalyse(int [] healthThresholds, int[] resolveThresholds, int[] defenceThresholds, int[] resistThresholds)
    {
        this.effectType = EffectType.ANALYSE;
        this.healthThresholds = healthThresholds;
        this.resolveThresholds = resolveThresholds;
        this.defenceThresholds = defenceThresholds;
        this.resistThresholds = resistThresholds;
        this.onMiss = true;
    }

    internal override Effect Clone()
    {
        return new EffectAnalyse(healthThresholds, resolveThresholds, defenceThresholds, resistThresholds);
    }

    public int GetHealthThreshold(int index)
    {
        if (healthThresholds == null)
        {
            return -1;
        }
        return healthThresholds[index];
    }

    public int GetResolveThreshold(int index)
    {
        if (resistThresholds == null)
        {
            return -1;
        }
        return resistThresholds[index];
    }

    public int GetDefenceThreshold(int index)
    {
        if (resistThresholds == null)
        {
            return -1;
        }
        return defenceThresholds[index];
    }

    public int GetResistThreshold(int index)
    {
        if (resistThresholds == null)
        {
            return -1;
        }
        return resistThresholds[index];
    }

    internal int [] GetHealthThreshold()
    {
        return healthThresholds;
    }

    internal int [] GetResolveThreshold()
    {
        return resolveThresholds;
    }

    internal int [] GetDefenceThreshold()
    {
        return defenceThresholds;
    }

    internal int [] GetResistThreshold()
    {
        return resistThresholds;
    }
}
