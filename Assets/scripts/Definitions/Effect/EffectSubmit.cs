﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EffectSubmit : Effect
{
    bool resolve;

    public EffectSubmit(bool resolve)
    {
        this.effectType = EffectType.SUBMIT;
        this.resolve = resolve;
    }

    public bool IsResolve()
    {
        return resolve;
    }

    internal override Effect Clone()
    {
        return new EffectSubmit(resolve);
    }
}
