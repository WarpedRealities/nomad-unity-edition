﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EffectSummon : Effect
{
    string file;
    string description;
    int minDistance, maxDistance;
    bool hiddenSpawn, originData;
    int directions;

    public EffectSummon(string file, string desc, int min, int max, bool hidden, bool originData, int directions)
    {
        this.effectType = EffectType.SUMMON;
        this.file = file;
        this.minDistance = min;
        this.maxDistance = max;
        this.hiddenSpawn = hidden;
        this.originData = originData;
        this.directions = directions;
    }

    internal override Effect Clone()
    {
        return new EffectSummon(file, description, minDistance, maxDistance, hiddenSpawn, originData, directions);
    }

    public string GetDescription()
    {
        return description;
    }

    public string GetFile()
    {
        return file;
    }

    public int GetMinDistance()
    {
        return minDistance;
    }

    public int GetMaxDistance()
    {
        return maxDistance;
    }
    
    public bool IsHidden()
    {
        return hiddenSpawn;
    }

    public bool IsOriginData()
    {
        return originData;
    }

    public int GetDirections()
    {
        return directions;
    }
}
