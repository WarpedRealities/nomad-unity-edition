﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Effect 
{
    protected EffectType effectType;
    protected bool onMiss;
    public EffectType GetEffectType()
    {
        return effectType;
    }

    public bool IsOnMiss()
    {
        return onMiss;
    }

    internal abstract Effect Clone();
}
