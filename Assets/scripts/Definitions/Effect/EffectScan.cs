using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EffectScan : Effect
{
    private bool ignoreWalls;
    private float range;
    private float width;
    private int bonus;

    public EffectScan(bool ignoreWalls, float range, float width, int bonus)
    {
        this.effectType = EffectType.SCAN;
        this.onMiss = true;
        this.ignoreWalls = ignoreWalls;
        this.range = range;
        this.width = width;
        this.bonus = bonus;
    }

    public bool isIgnoringWalls()
    {
        return ignoreWalls;
    }


    public float GetRange()
    {
        return range;
    }

    public float GetWidth()
    {
        return range;
    }

    public int GetBonus()
    {
        return bonus;
    }

    internal override Effect Clone()
    {
        return new EffectScan(ignoreWalls, range, width, bonus);
    }
}
