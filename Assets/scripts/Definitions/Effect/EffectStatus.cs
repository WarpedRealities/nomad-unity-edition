﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EffectStatus : Effect
{
    int statusStrength;
    DR defenceResist;
    StatusEffect statusEffect;
    string description;
    string resistDescription;

  public EffectStatus(StatusEffect status, string description, string resistDesc, int strength=-1, DR defence=DR.NONE)
    {
        this.description = description;
        this.resistDescription = resistDesc;
        this.statusStrength = strength;
        this.statusEffect = status;
        this.defenceResist = defence;
        this.effectType = EffectType.STATUS;
    }

    internal override Effect Clone()
    {
        return new EffectStatus(statusEffect, description, resistDescription, statusStrength, defenceResist);
    }

    public StatusEffect GetStatus()
    {
        return statusEffect;
    }

    public string GetDescription()
    {
        return description;
    }

    public string GetResistDescription()
    {
        return resistDescription;
    }

    public int GetStrength()
    {
        return statusStrength;
    }

    public DR GetResistance()
    {
        return defenceResist;
    }

}
