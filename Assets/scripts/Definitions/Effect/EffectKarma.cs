using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EffectKarma : Effect
{
    private float amount;
    private string effectText;


    public EffectKarma(float amount, string text)
    {
        this.effectType = EffectType.KARMA;
        this.amount = amount;
        this.effectText = text;
    }

    internal override Effect Clone()
    {
        return new EffectKarma(amount, effectText);
    }

    public float GetModifier()
    {
        return amount;
    }

    public string GetEffectText()
    {
        return effectText;
    }

}
