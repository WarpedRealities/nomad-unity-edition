﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EffectDismantle : Effect
{
    public EffectDismantle()
    {
        this.effectType = EffectType.DISMANTLE;
    }

    internal override Effect Clone()
    {
        return new EffectDismantle();
    }
}
