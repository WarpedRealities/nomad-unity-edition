﻿using UnityEngine;
using System.Collections;

public class EffectPerk : Effect
{
    string perkCode;
    float experienceReward;

    public EffectPerk(string perk, float experienceReward)
    {
        this.effectType = EffectType.PERK;
        this.perkCode = perk;
        this.experienceReward = experienceReward;
    }

    public string GetPerkCode()
    {
        return perkCode;
    }

    public float GetExperience()
    {
        return experienceReward;
    }

    internal override Effect Clone()
    {
        throw new System.NotImplementedException();
    }
}
