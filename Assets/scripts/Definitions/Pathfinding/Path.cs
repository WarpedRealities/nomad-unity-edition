﻿using UnityEngine;
using System.Collections;

public class Path 
{
    public float h, c;
    public int stepCount;
    public Vector2Int position;
    public Path parent;
    public int direction;

    public Path(Vector2Int p, float h, Path parent, int direction, int cost = 1)
    {
        this.direction = direction;
        this.position = p;
        this.h = h;
        this.parent = parent;
        this.c = parent != null ? parent.c + cost : cost;
        this.stepCount = parent != null ? parent.stepCount + 1 : 0;
    }

    public float value()
    {
        return h+c;
    }

    public override string ToString()
    {
        return "Path d: " + direction + " h: " + h + " s: " + stepCount;
    }
}
