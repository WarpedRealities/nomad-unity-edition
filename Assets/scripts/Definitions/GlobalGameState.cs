﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalGameState
{

    private static GlobalGameState instance;

    public static GlobalGameState GetInstance()
    {
        if (instance == null)
        {
            instance = new GlobalGameState();
        }
        return instance;
    }


    private Universe universe;
    private StarSystem currentSystem;
    private Entity currentEntity;
    private Zone currentZone;
    private Player player;
    private String currentShip;
    private bool playing;
    private GlobalGameState()
    {
        universe = new Universe();
        UniverseGeneratorTools generatorTools = new UniverseGeneratorTools();
        universe.setStarSystems(generatorTools.buildSystems());
    }

    public void NewGame()
    {
        NewGameTool newGameTool = new NewGameTool();
        newGameTool.NewGame(universe);
        universe.ResetClock();
        currentSystem = newGameTool.system;
        currentEntity = newGameTool.entity;
        currentZone = newGameTool.zone;
        player = newGameTool.player;

    }

    internal void SetPlayer(Player player)
    {
        this.player = player;
    }

    internal void SetCurrentEntity(Entity entity)
    {
        this.currentEntity = entity;
    }

    internal void SetCurrentSystem(StarSystem system)
    {
        this.currentSystem = system;
    }

    public void IncrementClock(int amount=1)
    {
        universe.IncrementClock(amount);
    }

    public Universe GetUniverse()
    {
        return universe;
    }

    public StarSystem getCurrentSystem()
    {
        return currentSystem;
    }

    public Entity getCurrentEntity()
    {

        return currentEntity;
    }

    public void setCurrentZone(Zone zone)
    {
        currentZone = zone;
       
    }

    public String GetCurrentShip()
    {
        return currentShip;
    }

    public void SetCurrentShip(string shipName)
    {
        this.currentShip = shipName;
    }

    public Zone getCurrentZone()
    {
        return currentZone;
    }

    public Player GetPlayer()
    {
        return player;
    }

    public bool IsPlaying()
    {
        return playing;
    }

    internal void SetCurrentSystem(string system)
    {
        currentSystem = universe.GetSystem(system);
    }

    internal void setCurrentZone(string zone)
    {
        currentZone = currentEntity.GetZone(zone);
    }

    internal void SetCurrentEntity(string entity)
    {
        currentEntity = currentSystem.getEntity(entity);
    }

    public void SetPlaying(bool value)
    {
        playing = value;
    }
}
