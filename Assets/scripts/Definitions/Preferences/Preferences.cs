﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Preferences 
{
    List<string> forbiddenPreferences;

    public Preferences()
    {
        forbiddenPreferences = PreferenceLoader.Load();
    }

    public void Reload()
    {
        forbiddenPreferences = PreferenceLoader.Load();
    }

    public bool PreferenceBlocked(string fetish)
    {

        for (int i = 0; i < forbiddenPreferences.Count; i++)
        {

            if (forbiddenPreferences[i].Equals(fetish))
            {
                return true;
            }
        }
        return false;
    }
    
}
