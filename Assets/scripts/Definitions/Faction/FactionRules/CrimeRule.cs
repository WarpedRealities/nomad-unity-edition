﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable()]
public class CrimeRule 
{
    int relationshipThreshold;
    int relationshipPenalty;
    int severity;

    public CrimeRule(int relationshipThreshold, int relationshipPenalty, int severity)
    {
        this.relationshipThreshold = relationshipThreshold;
        this.relationshipPenalty = relationshipPenalty;
        this.severity = severity;
    }

    public int GetThreshold()
    {
        return relationshipThreshold;
    }

    public int GetPenalty()
    {
        return relationshipPenalty;
    }

    public int GetSeverity()
    {
        return severity;
    }
}
