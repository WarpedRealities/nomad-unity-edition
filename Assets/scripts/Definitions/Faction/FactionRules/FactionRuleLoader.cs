﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class FactionRuleLoader
{
    internal static FactionRules LoadRules(XmlElement root)
    {
        List<CrimeRules> crimeRules = new List<CrimeRules>();
        List<string> witnessList = new List<string>();
        List<string> authorityList = new List<string>();
        string factionName = root.GetAttribute("factionName");
        bool suppress = "true".Equals(root.GetAttribute("SuppressMessages"));
        XmlNodeList children= root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("witness".Equals(element.Name))
                {
                    witnessList.Add(element.GetAttribute("value"));
                }
                if ("authority".Equals(element.Name))
                {
                    authorityList.Add(element.GetAttribute("value"));
                }
                if ("ruleset".Equals(element.Name))
                {
                    crimeRules.Add(BuildRuleset(element));
                }
            }
        }

        return new FactionRules(crimeRules, authorityList, witnessList,factionName, suppress);
    }

    internal static FactionRules LoadFile(string filename)
    {
        XmlDocument document = FileTools.GetXmlDocument("factionRules/" + filename);
        XmlElement root = (XmlElement)document.FirstChild.NextSibling;
        return LoadRules(root);
    }

    private static CrimeRules BuildRuleset(XmlElement root)
    {
        string name = root.GetAttribute("name");
        List<string> targetNames = new List<string>();
        CrimeRule[] rules = new CrimeRule[8];
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];

                if ("target".Equals(element.Name))
                {
                    targetNames.Add(element.GetAttribute("value"));
                }
                if ("rule".Equals(element.Name)) {
                    rules[(int)EnumTools.strToViolationType(element.GetAttribute("violation"))] = BuildViolationRule(element);
                }
            }
        }

        return new CrimeRules(name, targetNames, rules);
    }

    private static CrimeRule BuildViolationRule(XmlElement root)
    {
        int threshold = 0;
        int penalty = int.Parse(root.GetAttribute("penalty"));
        int.TryParse(root.GetAttribute("threshold"), out threshold);
        int severity = int.Parse(root.GetAttribute("severity"));

        return new CrimeRule(threshold, penalty, severity);
    }
}
