﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.UIElements;

[Serializable()]
public class ViolationDetails {
    public Vector2S violationPosition;
    public ViolationType violationType;
    public int violationSeverity;
    public int violationPenalty;
    public string violationTarget;

    public ViolationDetails(Vector2S violationPosition, ViolationType violationType, int violationSeverity, int violationPenalty, string violationTarget)
    {
        this.violationPosition = violationPosition;
        this.violationType = violationType;
        this.violationSeverity = violationSeverity;
        this.violationPenalty = violationPenalty;
        this.violationTarget = violationTarget;
    }
}

[Serializable()]
public class FactionRules 
{

    private const string PLAYER = "player";
    private int UNFORGIVABLETHRESHOLD = 5;
    ViolationDetails reportedViolation;
    ViolationDetails witnessedViolation;
    List<CrimeRules> crimeRules;

    List<string> authorityNames;
    List<string> witnessNames;
    string factionName;
    private bool suppressMessages = false;
    [NonSerialized]
    Dictionary<string, CrimeRules> ruleMap=new Dictionary<string, CrimeRules>();
    [NonSerialized]
    LineOfSightManager sightCheck;
    public FactionRules(List<CrimeRules> crimeRules, List<string> authorityNames, List<string> witnessNames, string primaryFactionName, bool suppress)
    {
        this.crimeRules = crimeRules;
        this.authorityNames = authorityNames;
        this.witnessNames = witnessNames;
        this.factionName = primaryFactionName;
        this.suppressMessages = suppress;
    }

    public void ReportCrime(ViolationType violationType, Actor victim)
    {
        //check if this is in fact a crime
        CrimeRules crimeRules = GetCrimeRule(victim.GetName());
        CrimeRule crimeRule = crimeRules.GetCrimeRules()[(int)violationType];
        int relationship = victim.GetFaction().getRelationship(PLAYER);

        if (relationship < 50)
        {
            return;
        }
        //if so figure out things like witnesses and authorities
        if (crimeRule != null && (crimeRule.GetThreshold()==0 || relationship<crimeRule.GetThreshold()))
        {
            Zone zone = GlobalGameState.GetInstance().getCurrentZone();
            AlertCrime(crimeRule, violationType, victim.GetName(), victim.GetPosition(), zone, victim);    
        }
    }

    internal void ApplyViolationPenalty()
    {
        GlobalGameState.GetInstance().GetUniverse().getFactionLibrary().GetFaction(factionName).ModifyRelationship(PLAYER, -reportedViolation.violationPenalty);
        this.reportedViolation = null;
    }

    internal void SetReportedViolation(ViolationDetails violation)
    {
        this.reportedViolation=violation;
    }

    public void AlertCrime(CrimeRule crimeRule, ViolationType violationType, string victim, Vector2Int position, Zone zone, Actor victimActor)
    {
        if (sightCheck == null)
        {
            sightCheck = new LineOfSightManager();
        }
        List<Actor> witnesses = new List<Actor>();
        List<Actor> authorities = new List<Actor>();
        bool authoritativeWitness = false;
        for (int i = 0; i < zone.GetContents().GetActors().Count; i++)
        {
            if (zone.GetContents().GetActors()[i].GetAlive() && zone.GetContents().GetActors()[i].GetPosition().x>=0 &&
                zone.GetContents().GetActors()[i]!=victimActor &&
                zone.GetContents().GetActors()[i].GetFaction().GetFactionRuleset() == this)
            {
             
                //are we a witness or authoritsy?
                
                if (authorityNames.Find(item => item.Equals(zone.GetContents().GetActors()[i].GetName())) != null)
                {

                    authorities.Add(zone.GetContents().GetActors()[i]);
                    //line of sight check, if it terminates in an authority then instantly report the crime and stop looking
                    if (sightCheck.CanSee(zone.GetContents().GetActors()[i].GetPosition(), position,zone.GetContents()))
                    {
               
                        authoritativeWitness = true;
                        break;
                    }
                }

                if (witnessNames.Find(item => item.Equals(zone.GetContents().GetActors()[i].GetName())) != null)
                {

                    //line of sight check, add to witnesses if line of sight works
                    if (sightCheck.CanSee(zone.GetContents().GetActors()[i].GetPosition(), position, zone.GetContents()))
                    {

                        witnesses.Add(zone.GetContents().GetActors()[i]);
                    }
                }
            }
        }

        //if there's no authoritative witness, 
        if (!authoritativeWitness)
        {
            if (witnesses.Count > 0)
            {
                //check from witnesses to authorities using a line of sight check
                for (int i = 0; i < witnesses.Count; i++)
                {
                    for (int j = 0; j < authorities.Count; j++)
                    {
                        if (sightCheck.CanSee(witnesses[i].GetPosition(), authorities[j].GetPosition(), zone.GetContents()))
                        {
                            authoritativeWitness = true;
                            break;
                        }
                    }
                }
                //if that check succeeds, consider we have an authoritative witness

                //if we still dont have an authoritative witness mark all witnesses as having witnessed a crime in their crime flags
                if (!authoritativeWitness)
                {
                    for (int i = 0; i < witnesses.Count; i++)
                    {
                        witnesses[i].GetCrimeFlags().SetWitness(true);
                    }
                    AddWitnessedCrime(crimeRule, violationType, victim, position);
                }
                else
                {
                    AddReportedCrime(crimeRule, violationType, victim, position);
                }
            }
        }
        else if (authorities.Count>0)
        {
            AddReportedCrime(crimeRule, violationType, victim, position);
        }
    }

    internal ViolationDetails GetReportedViolation()
    {
        return reportedViolation;
    }

    internal bool WitnessReport(Actor actor)
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();

        for (int i = 0; i < zone.GetContents().GetActors().Count; i++)
        {
            if (zone.GetContents().GetActors()[i].GetAlive() && zone.GetContents().GetActors()[i].GetPosition().x >= 0 &&
                zone.GetContents().GetActors()[i] != actor &&
                zone.GetContents().GetActors()[i].GetFaction().GetFactionRuleset() == this)
            {
                //are we a witness or authority?
                if (authorityNames.Find(item => item.Equals(zone.GetContents().GetActors()[i].GetName())) != null)
                {
                    //line of sight check, if it terminates in an authority then instantly report the crime and stop looking
                    if (sightCheck.CanSee(zone.GetContents().GetActors()[i].GetPosition(), actor.GetPosition(), zone.GetContents()))
                    {
                        CrimeRule crimeRule = new CrimeRule(100, witnessedViolation.violationPenalty, witnessedViolation.violationSeverity);
                        AddReportedCrime(crimeRule, witnessedViolation.violationType, witnessedViolation.violationTarget, 
                            new Vector2Int(witnessedViolation.violationPosition.x, witnessedViolation.violationPosition.y));
                        witnessedViolation = null;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void AddReportedCrime(CrimeRule crimeRule, ViolationType violationType, string victim, Vector2Int position)
    {
        if (crimeRule.GetSeverity()> UNFORGIVABLETHRESHOLD)
        {
            Faction faction = GlobalGameState.GetInstance().GetUniverse().getFactionLibrary().GetFaction(factionName);
            //apply penalty instantly
            faction.ModifyRelationship(PLAYER,-crimeRule.GetPenalty());
            if (!suppressMessages)
            {
                TextLog.Log("Your reputation has suffered due to your reported crime");
            }

            if (faction.getRelationship(PLAYER) < 50)
            {
                return;
            }
        }

        if (reportedViolation == null)
        {
            reportedViolation = new ViolationDetails(new Vector2S(position.x, position.y),
            violationType, crimeRule.GetSeverity(), crimeRule.GetPenalty(), victim);
            if (!suppressMessages)
            {
                TextLog.Log("Your crime has been reported");
            }
            return;
        }
        else if (reportedViolation.violationSeverity < crimeRule.GetSeverity())
        {
            reportedViolation = new ViolationDetails(new Vector2S(position.x, position.y),
            violationType, crimeRule.GetSeverity(), crimeRule.GetPenalty(), victim);
            if (!suppressMessages)
            {
                TextLog.Log("Your crime has been reported");
            }
        }
    }
    private void AddWitnessedCrime(CrimeRule crimeRule, ViolationType violationType, string victim, Vector2Int position)
    {
        if (witnessedViolation == null)
        {
            witnessedViolation = new ViolationDetails(new Vector2S(position.x, position.y), 
                violationType, crimeRule.GetSeverity(), crimeRule.GetPenalty(), victim);
            if (!suppressMessages)
            {
                TextLog.Log("Your crime has been observed");
            }
            return;
        }
        else if (witnessedViolation.violationSeverity < crimeRule.GetSeverity())
        {
            witnessedViolation = new ViolationDetails(new Vector2S(position.x, position.y),
            violationType, crimeRule.GetSeverity(), crimeRule.GetPenalty(), victim);
            if (!suppressMessages)
            {
                TextLog.Log("Your crime has been observed");
            }
        }
    }


    private CrimeRules GetCrimeRule(string name)
    {
        if (ruleMap.ContainsKey(name))
        {
            return ruleMap[name];
        }
        CrimeRules crimeRuleset = this.crimeRules[0];
        for (int i=0;i<crimeRules.Count; i++)
        {
            for (int j = 0; j < crimeRules[i].GetNames().Count; j++)
            {
                if (crimeRules[i].GetNames()[j].Equals(name))
                {
                    crimeRuleset = crimeRules[i];
                    break;
                }
            }
        }
        ruleMap.Add(name, crimeRuleset);
        return crimeRuleset;
    }

    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        ruleMap = new Dictionary<string, CrimeRules>();
    }
}
