﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable()]
public class FactionRuleLibrary 
{
    Dictionary<string, FactionRules> factionRules;
 
    public FactionRuleLibrary()
    {
        factionRules = new Dictionary<string, FactionRules>();
    }

    public FactionRules GetRules(string name)
    {
        if (!factionRules.ContainsKey(name))
        {
            factionRules.Add(name, FactionRuleLoader.LoadFile(name));
        }

        return factionRules[name];
    }
}
