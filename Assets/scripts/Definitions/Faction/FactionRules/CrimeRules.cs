﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable()]
public class CrimeRules 
{
    string name;
    List<string> namesCovered;
    CrimeRule[] crimeRules;

    public CrimeRules(string name, List<string> namesCovered, CrimeRule[] crimeRules)
    {
        this.name = name;
        this.namesCovered = namesCovered;
        this.crimeRules = crimeRules;
    }

    public List<String> GetNames()
    {
        return namesCovered;
    }

    public CrimeRule[] GetCrimeRules()
    {
        return crimeRules;
    }
}
