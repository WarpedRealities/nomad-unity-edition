﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Faction
{
    private string factionName;
    private Dictionary<string, int> relationships;
    private int defaultRelationship;
    private FlagField flagField;
    private string factionRulesetName;
    [NonSerialized]
    private FactionRules factionRules;

    public Faction(string name, Dictionary<string,int> relationship, int defaultRelationship, string factionRules)
    {
        flagField = new FlagField();
        this.factionName = name;
        this.relationships = relationship;
        this.defaultRelationship = defaultRelationship;
        this.factionRulesetName = factionRules;
    }

    public int getRelationship(string factionName)
    {
        int relationship = defaultRelationship;
        if (!relationships.ContainsKey(factionName))
        {
            relationships.Add(factionName, defaultRelationship);   
        }
        else
        {
            return relationships[factionName];
        }
        return relationship;
    }

    public int getRelationship(Faction faction)
    {
        if (this.Equals(faction))
        {
            return 100;
        }
        int otherRelationship = faction.getRelationship(factionName);
        int thisRelationship = getRelationship(faction.factionName);
   
        int relationship = otherRelationship >= thisRelationship ? thisRelationship : otherRelationship;

        if (otherRelationship != thisRelationship)
        {
            faction.relationships[factionName]= relationship;
            relationships[faction.factionName]= relationship;
        }

        return relationship;
    }

    public void ModifyRelationship(string factionName, int amount)
    {

        int relationship = defaultRelationship;
        if (!relationships.TryGetValue(factionName, out relationship))
        {
            relationships.Add(factionName, defaultRelationship);
        }
        relationship += amount;
        if (relationship < 0)
        {
            relationship = 0;
        }
        if (relationship > 100)
        {
            relationship = 100;
        }
        relationships[factionName]= relationship;
        GlobalGameState.GetInstance().GetUniverse().getFactionLibrary().GetFaction(factionName).relationships[this.factionName]=relationship;
    }
    
    public string GetName()
    {
        return factionName;
    }

    public FlagField GetFlagField()
    {
        return flagField;
    }

    public FactionRules GetFactionRuleset()
    {
        if (factionRulesetName != null && factionRules==null)
        {
            factionRules = GlobalGameState.GetInstance().GetUniverse().GetFactionRuleLibrary().GetRules(factionRulesetName);
        }
        return factionRules;
    }

    internal void SetRelationship(string faction, int value)
    {
        int relationship = defaultRelationship;
        if (!relationships.TryGetValue(factionName, out relationship))
        {
            relationships.Add(factionName, defaultRelationship);
        }
        relationship = value;
        if (relationship < 0)
        {
            relationship = 0;
        }
        if (relationship > 100)
        {
            relationship = 100;
        }
        relationships[factionName] = relationship;
        GlobalGameState.GetInstance().GetUniverse().getFactionLibrary().GetFaction(factionName).relationships[this.factionName] = relationship;
    }

    // override object.Equals
    public override bool Equals(object obj)
    {

        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        if (((Faction)obj).GetName().Equals(this.factionName)) {
            return true;
        }
        return base.Equals(obj);
    }

    // override object.GetHashCode
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
