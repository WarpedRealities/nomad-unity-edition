﻿using System;
using System.Collections.Generic;
using System.Xml;

public class FactionLoaderTool
{
    public static Faction loadFaction(string filename)
    {
        string factionRulesName = null;
        Dictionary<string, int> relationships = new Dictionary<string, int>();
        int defaultRelationship = 0;
        XmlDocument document = FileTools.GetXmlDocument("factions/" + filename);
        XmlElement root = (XmlElement)document.FirstChild.NextSibling;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];

                if ("relationship".Equals(element.Name))
                {
                    relationships.Add(element.GetAttribute("faction"), int.Parse(element.GetAttribute("value")));
                }
                if ("defaultRelationship".Equals(element.Name))
                {
                    defaultRelationship = int.Parse(element.GetAttribute("value"));
                }
                if ("factionRules".Equals(element.Name))
                {
                    factionRulesName = element.GetAttribute("value");
                }
            }
        }

        return new Faction(filename, relationships, defaultRelationship, factionRulesName);
    }
}