﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FactionLibrary { 
   Dictionary<string, Faction> factions;

   public FactionLibrary()
   {
        factions = new Dictionary<string, Faction>();
   }

    public Faction GetFaction(string name)
    {
        if (name==null || name.Length < 2)
        {
            return null;
        }
        Faction faction = null;
        if (!factions.TryGetValue(name,out faction))
        {
            faction = FactionLoaderTool.loadFaction(name);
            factions.Add(name, faction);
        }
        return faction;
    }

    internal void Clear()
    {
        factions.Clear();
    }
}
