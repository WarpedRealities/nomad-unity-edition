﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class DefenceModifiers 
{
    float[] defences;

    public DefenceModifiers()
    {
        defences = new float[10];
    }

    public DefenceModifiers(XmlElement root)
    {
        defences = DefenceModifierBuilder.Build(root);
    }
 
    public float[] GetDefences()
    {
        return defences;
    }

    public float GetDefence(DR dR)
    {
        return defences[(int)dR];
    }
}
