﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class SkillModifiers
{
    int[] skills;

    public SkillModifiers()
    {
        skills = new int[14];
    }

    public SkillModifiers(XmlElement root)
    {
        skills = SkillsBuilder.Build(root);
    }

    public int [] GetSkills()
    {
        return skills;
    }

    public int GetSkill(SK sk)
    {
        return skills[(int)sk];
    }

    public int GetSkill(int index)
    {
        return skills[index];
    }
}
