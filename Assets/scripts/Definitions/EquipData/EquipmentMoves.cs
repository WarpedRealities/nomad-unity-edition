﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentMoves 
{
    CombatMove [] combatMoves;


    public EquipmentMoves(CombatMove[] combatMoves)
    {
        this.combatMoves = combatMoves;
    }

    public int GetMoveCount()
    {
        return combatMoves.Length;
    }

    public CombatMove[] GetMoves()
    {
        return combatMoves;
    }
}
