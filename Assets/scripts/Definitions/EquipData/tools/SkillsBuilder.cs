﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

public class SkillsBuilder
{
    public static int[] Build(XmlElement root)
    {
        XmlNodeList children = root.ChildNodes;
        int[] skills = new int[14];
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];

                if ("skill".Equals(element.Name))
                {
                    SK sk = EnumTools.strToSkill(element.GetAttribute("skill"));
                    skills[(int)sk] = int.Parse(element.GetAttribute("value"));
                }
            }
        }
        return skills;
    }
}
