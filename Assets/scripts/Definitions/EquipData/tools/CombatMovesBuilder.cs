﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class CombatMovesBuilder 
{


    internal static CombatMove[] Build(XmlElement root)
    {
        CombatMove[] moves = new CombatMove[int.Parse(root.GetAttribute("count"))];
        XmlNodeList children = root.ChildNodes;
        int index = 0;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                moves[index] = CombatMoveLoader.BuildCombatMove(element);
                index++;
            }
        }
        return moves;
    } 
  
}
