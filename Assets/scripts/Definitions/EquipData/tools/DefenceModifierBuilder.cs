﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

public class DefenceModifierBuilder
{
    internal static float[] Build(XmlElement root)
    {
        XmlNodeList children = root.ChildNodes;
        float[] defences = new float[10];
        for (int i = 0; i < children.Count; i++) {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];

                if ("defence".Equals(element.Name) || "resistance".Equals(element.Name))
                {
                    DR dR = EnumTools.strToDefenceResist(element.GetAttribute("type"));
                    defences[(int)dR] = float.Parse(element.GetAttribute("value"));
                }
            }
        }
        return defences;
    }
}
