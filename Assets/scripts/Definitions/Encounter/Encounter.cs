using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Encounter : MonoBehaviour
{
    private EncounterData encounterData;
    bool encounterStarted = false;
    int state = -2;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        GlobalGameState globalGameState = GlobalGameState.GetInstance();
        if (!globalGameState.IsPlaying())
        {
            globalGameState.NewGame();
            globalGameState.SetPlaying(true);
        }
        if (encounterData == null)
        {
            SetupEncounter();
        }
    }

    public void SetEncounter(EncounterData encounterData)
    {
        this.encounterData = encounterData;
    }

    private void SetupEncounter()
    {
        //build enemy ship

        Creature creature = new CreatureGeneratorTool().GenerateCreature("spaceshark");
        Active_Entity[] hostiles = new Active_Entity[1] { creature };
        creature.SetController(SystemGeneratorTools.BuildController("spaceshark"));

        Spaceship playerShip = (Spaceship)GlobalGameState.GetInstance().getCurrentEntity();
        ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(playerShip, true);
        shipAnalysisTool.ApplyStats();
        shipAnalysisTool.ActivateConverters();
        encounterData = new EncounterData(creature.GetController().GetData(),
            hostiles,
            playerShip,
            GlobalGameState.GetInstance().GetPlayer());
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!encounterStarted)
        {
            //generate an encounter handler
            encounterStarted = true;
            SceneManager.LoadScene("BattleScene");
        }
        if (state != -2 && SceneManager.GetActiveScene().name == "SolarScene")
        {
            HandlePostEncounter();
        }
    }

    private void HandlePostEncounter()
    {
        SolarScriptRunnerTool solarScriptRunnerTool = new SolarScriptRunnerTool(GlobalGameState.GetInstance().getCurrentSystem());
        switch (state)
        {
            case 0:
                //defeat
                solarScriptRunnerTool.RunScript(encounterData.GetData().GetWinScript(), encounterData.GetHostile(0));
                break;
            case 1:
                //victory
                solarScriptRunnerTool.RunScript(encounterData.GetData().GetLossScript(), encounterData.GetHostile(0));
                break;


        }
        Destroy(gameObject);
    }

    public EncounterData GetData()
    {
        return encounterData;
    }

    internal void PlayerEscape()
    {
        for (int i = 0; i < encounterData.GetHostileCount(); i++)
        {
            Active_Entity active_Entity = encounterData.GetHostile(i);
            if (active_Entity.GetController() != null)
            {
                active_Entity.GetController().AddBusyTicks(1000);
            }
        }
        //move the player
        int r = DiceRoller.GetInstance().RollDice(8);
        Vector2 offset = GeometryTools.GetPosition((int)encounterData.GetPlayerShip().GetPosition().x, 
            (int)encounterData.GetPlayerShip().GetPosition().y, r);
        encounterData.GetPlayerShip().setPosition(new Vector2Int((int)offset.x, (int)offset.y));
        encounterData.GetPlayerShip().SetFacing(r);
        encounterStarted = true;
        SceneManager.LoadScene("SolarScene");

    }

    internal void ReconcileEnemies()
    {
        for (int i = 0; i < encounterData.GetHostileCount(); i++)
        {
            if (encounterData.GetHostile(i) is Spaceship)
            {
                Spaceship ship = (Spaceship)encounterData.GetHostile(i);
                ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(ship, false);
                shipAnalysisTool.ReconcileStats();
            }
        }

    }

    internal void SetState(int state)
    {
        this.state = state;
    }
}
