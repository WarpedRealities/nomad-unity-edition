﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EncounterData
{
    ActiveData activeData;
    Active_Entity[] hostiles;
    Spaceship playerShip;
    Player player;

    public EncounterData(ActiveData activeData, Active_Entity[] hostiles, Spaceship playerShip, Player player)
    {
        this.activeData = activeData;
        this.hostiles = hostiles;
        this.player = player;
        this.playerShip = playerShip;
    }

    public int GetHostileCount()
    {
        return hostiles.Length;
    }

    public Active_Entity GetHostile(int index)
    {
        return hostiles[index];
    }

    public Spaceship GetPlayerShip()
    {
        return playerShip;
    }

    public ActiveData GetData()
    {
        return activeData;
    }
}
