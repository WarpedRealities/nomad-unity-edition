﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Store {

    protected string filename;
    protected long lastInteraction;
    protected long refreshInterval;
    protected float profitRatio;
    protected bool useCredits;
    public Store(string name)
    {
        filename = name;
    }

    public string GetFileName()
    {
        return filename;
    }

    public long GetLastInteraction()
    {
        return lastInteraction;
    }

    public long GetRefreshInterval()
    {
        return refreshInterval;
    }

    public void SetLastInteraction(long clock)
    {
        this.lastInteraction = clock;
    }

    public abstract void Refresh();

    public bool IsUsingCredits()
    {
        return useCredits;
    }

    public float GetRatio()
    {
        return profitRatio;
    }
    public abstract SHOPTYPE GetShopType();
}
