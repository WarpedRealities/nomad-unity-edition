﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ShopLibrary 
{
    Dictionary<string, Store> stores;

    public ShopLibrary()
    {
        stores = new Dictionary<string, Store>();
    }

    public Store GetStore(string filename)
    {
        if (stores.ContainsKey(filename))
        {
            return stores[filename];
        }
        stores.Add(filename, StoreLoader.LoadStore(filename));
        return stores[filename];
    }

    internal void Clear()
    {
        stores.Clear();
    }
}
