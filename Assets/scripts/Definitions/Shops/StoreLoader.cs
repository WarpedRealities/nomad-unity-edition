﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class StoreLoader
{
    private static string PREFIX = "shops/";
    private static string SHOPROOT = "ItemStore";


    internal static Store LoadStore(string filename)
    {
        XmlDocument document = FileTools.GetXmlDocument(PREFIX + filename);
        XmlElement root = (XmlElement)document.FirstChild.NextSibling;

        if (SHOPROOT.Equals(root.Name))
        {
            return LoadItemStore(root, filename);
        }


        throw new NotImplementedException();
    }

    private static Store LoadItemStore(XmlElement root, string filename)
    {
        long refreshInterval = long.Parse(root.GetAttribute("refreshInterval"));
        float profitRatio = float.Parse(root.GetAttribute("profitRatio"));
        bool useCredits = root.GetAttribute("useCredits").Equals("true");
        XmlNodeList children = root.ChildNodes;
        ItemList items = new ItemList();
        Dictionary<string, float> specialPrices = new Dictionary<string, float>();
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("item".Equals(element.Name))
                {
                    float probability = float.Parse(element.GetAttribute("probability"));
                    int quantity = int.Parse(element.GetAttribute("quantity"));
                    string itemCode = element.GetAttribute("ID");
                    string addendum = element.GetAttribute("addendum");              
                    if (DiceRoller.GetInstance().RollOdds() < probability)
                    {
                        Item item = LootItemGenerator.BuildItem(itemCode, addendum);

                        if (quantity == 1)
                        {
                            items.AddItem(item);
                        }
                        else
                        {
                            if (item.IsStackable())
                            {
                                if (item.GetDef().GetItemType() == ItemType.AMMO)
                                {
                                    string[] strings = addendum.Split('#');
                                    items.AddItem(new ItemAmmoStack((ItemAmmo)item.GetDef(),
                                        quantity,
                                        GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(strings[0])),true);
                                }
                                else
                                {
                                    items.AddItems(item, quantity);
                                }
                            }
                            else
                            {
                                for (int j = 0; j < quantity; j++)
                                {
                                    items.AddItem(LootItemGenerator.BuildItem(itemCode,addendum));
                                }
                            }
                        }
                    }
                }
                if ("specialPrice".Equals(element.Name))
                {
                    specialPrices.Add(element.GetAttribute("ID"), float.Parse(element.GetAttribute("value")));
                }
            }
        }
        return new ItemStore(items, specialPrices, filename, refreshInterval, profitRatio, useCredits);
    }

    internal static ItemList RefreshItemStore(string filename)
    {
        ItemList items = new ItemList();

        XmlDocument document = FileTools.GetXmlDocument(PREFIX + filename);
        XmlElement root = (XmlElement)document.FirstChild.NextSibling;
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("item".Equals(element.Name))
                {
                    float probability = float.Parse(element.GetAttribute("probability"));
                    int quantity = int.Parse(element.GetAttribute("quantity"));
                    string itemCode = element.GetAttribute("ID");
                    string addendum = element.GetAttribute("addendum");
                    if (DiceRoller.GetInstance().RollOdds() < probability)
                    {
                        Item item = LootItemGenerator.BuildItem(itemCode, addendum);
                        if (quantity == 1)
                        {
                            items.AddItem(item);
                        }
                        else
                        {
                            if (item.IsStackable())
                            {
                                if (item.GetDef().GetItemType() == ItemType.AMMO)
                                {
                                    string[] strings = addendum.Split('#');
                                    items.AddItem(new ItemAmmoStack((ItemAmmo)item.GetDef(),
                                        quantity,
                                        GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(strings[0])));
                                }
                                else
                                {
                                    items.AddItems(item, quantity);
                                }
                            }
                            else
                            {
                                for (int j = 0; j < quantity; j++)
                                {
                                    items.AddItem(LootItemGenerator.BuildItem(itemCode, addendum));
                                }
                            }
                        }
                    }
                }
            }
        }

        return items;
    }
}
