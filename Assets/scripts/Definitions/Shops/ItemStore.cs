﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ItemStore : Store
{
    ItemList shopInventory;
    private Dictionary<string, float> specialPrices;

    public ItemStore(ItemList items, Dictionary<string, float> specialPrices, string name,long refreshInterval,float profitRatio, bool useCredits) : base(name)
    {
        this.profitRatio = profitRatio;
        this.specialPrices = specialPrices;
        this.shopInventory = items;
        this.refreshInterval = refreshInterval;
        this.useCredits = useCredits;
    }

    public override SHOPTYPE GetShopType()
    {
        return SHOPTYPE.ITEM;
    }


    public ItemList GetInventory()
    {
        return shopInventory;
    }

    public override void Refresh()
    {
        shopInventory = StoreLoader.RefreshItemStore(this.filename);
    }

    public Dictionary<string,float> GetSpecialPrices()
    {
        return specialPrices;
    }
}
