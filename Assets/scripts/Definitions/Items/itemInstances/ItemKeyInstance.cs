﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemKeyInstance : Item
{
    string keyCode;
    string keyName;

    public ItemKeyInstance(ItemDef itemDef) : base(itemDef)
    {
    }

    public string GetKeyCode()
    {
        return keyCode;
    }

    public void SetKeyCode(string code)
    {
        this.keyCode = code;
    }

    public string GetKeyName()
    {
        return keyName;
    }

    public override bool IsStackable()
    {
        return false;
    }

    public void SetKeyName(string name)
    {
        this.keyName = name;
    }


    public override string GetItemString()
    {
        return keyName+" "+ this.itemDef.GetName() + GetWeight();
    }
}
