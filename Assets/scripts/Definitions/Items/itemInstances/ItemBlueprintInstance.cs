using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemBlueprintInstance : Item
{
    string recipeCode;
    string recipeName;

    public ItemBlueprintInstance(ItemDef itemDef) : base(itemDef)
    {

    }

    public string GetRecipeCode()
    {
        return recipeCode;
    }

    public String GetRecipeName()
    {
        return recipeName;
    }

    public void SetRecipeCode(string code)
    {
        this.recipeCode = code;
    }

    public void SetRecipeName(string name)
    {
        this.recipeName = name;
    }

    public override bool IsStackable()
    {
        return false;
    }



    public override string GetItemString()
    {
        return recipeName + " " + this.itemDef.GetName() + GetWeight();
    }
}
