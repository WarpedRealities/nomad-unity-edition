﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemQuestInstance : Item
{
    ItemDef uniqueDef;

    public ItemQuestInstance(ItemDef itemDef)
    {
        this.uniqueDef = itemDef;
        this.itemCode = null;
    }

    public override string GetDescription()
    {
        return uniqueDef.GetDescription();
    }

    public override ItemDef GetDef()
    {
        return uniqueDef;
    }
    public override ItemClass GetItemClass()
    {
        return ItemClass.QUEST;
    }

    public override string GetItemString()
    {
        return uniqueDef.GetName() + " " + uniqueDef.GetWeight();
    }

    public override string GetName()
    {
        return uniqueDef.GetName();
    }

    public override int GetValue()
    {
        return uniqueDef.GetValue();
    }

    public override float GetWeight()
    {
        return uniqueDef.GetWeight();
    }

    public override bool IsStackable()
    {
        return false;
    }
}
