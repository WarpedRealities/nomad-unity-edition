﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public class Item
{
    [NonSerialized]
    protected ItemDef itemDef;
    protected string itemCode;

    public Item()
    {

    }

    public Item(ItemDef itemDef)
    {
        this.itemDef = itemDef;
    }
    [OnSerializing()]
    internal void OnSerialize(StreamingContext streamingContext)
    {
        if (itemDef != null) {
            itemCode = itemDef.GetCodeName();
        }

    }

    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        if (itemCode != null)
        {
            itemDef = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(itemCode);
        }      
    }

    public virtual ItemDef GetDef()
    {
        return itemDef;
    }

    public virtual bool IsStackable()
    {
        return itemDef.IsStackable();
    }

    public virtual float GetWeight()
    {
        return itemDef.GetWeight();
    }

    public virtual ItemClass GetItemClass()
    {
        return ItemClass.REGULAR;
    }

    public virtual string GetItemString()
    {
        return itemDef.GetName()+" "+GetWeight();
    }

    public virtual string GetDescription()
    {
        return itemDef.GetDescription();
    }

    public virtual string GetName()
    {
        return itemDef.GetName();
    }

    public virtual int GetValue()
    {
        return itemDef.GetValue();
    }
}
