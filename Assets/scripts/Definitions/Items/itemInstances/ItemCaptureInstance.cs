using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemCaptureInstance : Item
{
    string shipName;


    public ItemCaptureInstance(ItemDef itemDef) : base(itemDef)
    {
    }

    public string GetShipName()
    {
        return shipName;
    }

    public void SetShipName(string shipName)
    {
        this.shipName = shipName;
    }

    public override bool IsStackable()
    {
        return false;
    }

    public override string GetItemString()
    {
        return GetString() + " " + this.itemDef.GetName() + GetWeight();
    }

    private string GetString()
    {
        return shipName != null ? "synchronized" : "unsychronized";
    }
}
