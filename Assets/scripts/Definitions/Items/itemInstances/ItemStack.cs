﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemStack : Item
{
    protected int stackCount;

    public ItemStack(ItemDef item, int count) : base(item)
    {

        this.stackCount = count;
    }

 
    public override float  GetWeight()
    {
        return stackCount*itemDef.GetWeight();
    }

    public override ItemClass GetItemClass()
    {
        return ItemClass.STACK;
    }

    public override bool IsStackable()
    {
        return true;
    }

    public int GetCount()
    {
        return stackCount;
    }

    public void SetCount(int count)
    {

        stackCount = count;
    }

    public override string GetItemString()
    {
        return this.itemDef.GetName() + " x"+ stackCount + " " + GetWeight();
    }

    public override string ToString()
    {
        return "Item stack " + this.itemDef.GetName() + " x " + stackCount;
    }

    public Item TakeItem()
    {
        if (itemDef.GetItemType().Equals(ItemType.EQUIP))
        {
            return new EquipInstance((ItemEquippable)itemDef);
        }
        return new Item(itemDef);
    }

    public void DecrementCount()
    {
        stackCount--;
    }

    public override string GetName()
    {
        return itemDef.GetName() + " x" + stackCount;
    }
}
