﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class AmmoInstance : AmmoBase
{
    ResourceStoreInstance resourceStore;

    public AmmoInstance(ItemAmmo itemDef) : base(itemDef)
    {
        resourceStore = new ResourceStoreInstance(itemDef.GetResourceStore(), itemDef.GetCodeName());
        resourceStore.SetAmount(itemDef.GetResourceStore().GetCapacity());
    }

    public AmmoInstance(ItemAmmo itemDef, AmmoType ammoType) : this(itemDef)
    {
        resourceStore = new ResourceStoreInstance(itemDef.GetResourceStore(), itemDef.GetCodeName());
        resourceStore.SetAmount(itemDef.GetResourceStore().GetCapacity());
        resourceStore.SetAmmoType(ammoType);
    }

    public override string GetItemString()
    {
        ItemAmmo itemAmmo = (ItemAmmo)itemDef;
        if (itemAmmo.GetResourceStore().GetCapacity() > 1)
        {
            return itemDef.GetName() +" "+ resourceStore.GetAmount()+"/" + itemAmmo.GetResourceStore().GetCapacity() +
                " (" + resourceStore.GetAmmoType().GetShortName() + ") " + GetWeight();
        }
        return itemDef.GetName() + " ("+resourceStore.GetAmmoType().GetShortName() + ") " + GetWeight();
    }

    public override string GetDescription()
    {
        ItemAmmo itemAmmo = (ItemAmmo)itemDef;
        if (itemAmmo.GetResourceStore().GetCapacity() > 1)
        {
            String str = base.GetDescription().Replace("AMMOAMOUNT", resourceStore.GetAmount() + "/" + itemAmmo.GetResourceStore().GetCapacity()+
                resourceStore.GetAmmoType().shortName).Replace("AMMODESC",resourceStore.GetAmmoType().GetDescription());
            return str;
        }
        return base.GetDescription().Replace("AMMODESC",resourceStore.GetAmmoType().GetDescription());
    }

    public ResourceStoreInstance GetResource()
    {
        return resourceStore;
    }

    public override bool IsStackable()
    {

        return resourceStore.GetDef().GetCapacity() == 1 || resourceStore.GetAmount()==resourceStore.GetDef().GetCapacity();
    }

    public override AmmoType GetAmmoType()
    {
        return resourceStore.GetAmmoType();
    }

    public override void SetAmmoType(AmmoType ammoType)
    {
        throw new System.NotImplementedException();
    }

    public override int GetValue()
    {
        return (int)(itemDef.GetValue()*resourceStore.GetAmmoType().GetValueMod());
    }

    internal bool isExpendable()
    {
        if (resourceStore.GetAmount() > 0)
        {
            return false;
        }
        ItemAmmo ammmo = ((ItemAmmo)this.GetDef());
        if (ammmo.GetResourceStore().GetRefillDef() != null)
        {
            return false;
        }
        if (ammmo.GetResourceStore().GetCompatabilities().Count > 0)
        {
            return false;
        }

        return true;
    }
}
