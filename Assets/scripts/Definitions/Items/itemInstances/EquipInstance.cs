﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class EquipInstance : Item
{

    ResourceStoreInstance resourceStore;

    public EquipInstance(ItemEquippable itemDef) : base(itemDef)
    {
        if (itemDef.GetResourceStore() != null)
        {
            int ammoPerShot = 1;
            for (int i = 0; i < itemDef.GetMoves().GetMoves().Length; i++)
            {
                if (itemDef.GetMoves().GetMoves()[i].GetAmmoCost() > 0)
                {
                    ammoPerShot = itemDef.GetMoves().GetMoves()[i].GetAmmoCost();
                    break;
                }
            }
            resourceStore = new ResourceStoreInstance(itemDef.GetResourceStore(), itemDef.GetCodeName(), ammoPerShot);
        }
    }

    public ResourceStoreInstance GetResourceStore()
    {
        return resourceStore;
    }

    public override string GetDescription()
    {
        if (resourceStore != null)
        {
            return itemDef.GetDescription() + resourceStore.GetString();
        }
        return itemDef.GetDescription();
    }

    public override string GetItemString()
    {
        return this.resourceStore!=null ? this.itemDef.GetName() + resourceStore.GetString() + " " + GetWeight() : base.GetItemString();
    }

    public override bool IsStackable()
    {
        return this.GetDef().IsStackable() && this.resourceStore==null;
    }
}
