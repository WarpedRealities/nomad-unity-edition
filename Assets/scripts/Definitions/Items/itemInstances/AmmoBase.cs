﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public abstract class AmmoBase : Item
{

    public AmmoBase(ItemAmmo itemAmmo) : base(itemAmmo)
    {

    }

    public abstract AmmoType GetAmmoType();

    public abstract void SetAmmoType(AmmoType ammoType);
}
