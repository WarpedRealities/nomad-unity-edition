﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization;

[Serializable]
public class ItemAmmoStack : AmmoBase
{
    [NonSerialized]
    AmmoType ammoType;
    string ammoTypeString;
    int count;

    public ItemAmmoStack(ItemAmmo itemDef, int count, AmmoType ammoType) : base(itemDef)
    {
        this.count = count;
        this.ammoType = ammoType;
    }

    [OnSerializing()]
    internal new void OnSerialize(StreamingContext streamingContext)
    {
        ammoTypeString = ammoType.GetCodeName();
    }

    [OnDeserialized()]
    internal new void OnDerialize(StreamingContext streamingContext)
    {
        ammoType = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(ammoTypeString);
    }
    public override string GetDescription()
    {
        ItemAmmo itemAmmo = (ItemAmmo)itemDef;
        if (itemAmmo.GetResourceStore().GetCapacity() > 1)
        {
            String str = base.GetDescription().Replace("AMMOAMOUNT", itemAmmo.GetResourceStore().GetCapacity() + "/" + itemAmmo.GetResourceStore().GetCapacity() +
                ammoType.shortName);
            return str;
        }
        return base.GetDescription().Replace("AMMODESC", ammoType.GetDescription());
    }

    public void SetCount(int count)
    {
        this.count = count;
    }

    public override ItemClass GetItemClass()
    {
        return ItemClass.AMMOSTACK;
    }

    public override float GetWeight()
    {
        return count * itemDef.GetWeight();
    }

    public override string GetItemString()
    {
        return itemDef.GetName() + " (" + ammoType.GetName() + ")  x" + count + " " + GetWeight();
    }

    public override string GetName()
    {
        return itemDef.GetName() +" (" + ammoType.GetName() + ") x" + count;
    }

    public int GetCount()
    {
        return count;
    }

    internal Item TakeItem()
    {
        return new AmmoInstance((ItemAmmo)itemDef,ammoType);
    }

    public override AmmoType GetAmmoType()
    {
        return ammoType;
    }

    public override void SetAmmoType(AmmoType ammoType)
    {
        throw new NotImplementedException();
    }

    internal void DecrementCount()
    {
        count--;

    }
}
