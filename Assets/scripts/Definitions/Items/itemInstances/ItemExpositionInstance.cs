﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemExpositionInstance : Item
{
    string expositionName;
    string expositionFile;

    public ItemExpositionInstance(ItemDef itemDef) : base(itemDef)
    {
    }

    public string GetExpositionName()
    {
        return expositionName;
    }

    public void SetExpositionName(string expoName)
    {
        this.expositionName = expoName;
    }

    public string GetExpositionFile()
    {
        return expositionFile;
    }

    public override bool IsStackable()
    {
        return false;
    }

    public void SetExpositionFile(string expoFile)
    {
        this.expositionFile = expoFile;
    }


    public override string GetItemString()
    {
        return this.itemDef.GetName() + ":"+ this.expositionName + " " + GetWeight();
    }
}
