﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ItemLibrary 
{
    Dictionary<string, ItemDef> items;
    Dictionary<string, AmmoType> ammoTypes;
    public ItemLibrary()
    {
        items = ItemLoader.LoadItems();
        ammoTypes = AmmunitionTypeBuilder.LoadAmmoTypes();

    }

    public Item GetItemByCode(string code)
    {
        return ItemInstantiation(items[code]);
    }

    private Item ItemInstantiation(ItemDef itemDef)
    {

        switch (itemDef.GetItemType())
        {
            case ItemType.EQUIP:
    
                return new EquipInstance((ItemEquippable)itemDef);
            case ItemType.AMMO:
     
                return new AmmoInstance((ItemAmmo)itemDef);
            case ItemType.EXPOSITION:

                return new ItemExpositionInstance((ItemExposition)itemDef);
            case ItemType.KEY:
                return new ItemKeyInstance((ItemKey)itemDef);
            case ItemType.BLUEPRINT:
                return new ItemBlueprintInstance((ItemBlueprint)itemDef);
            case ItemType.CAPTURE:
                return new ItemCaptureInstance((ItemCapture)itemDef);
        }
        return new Item(itemDef);
    }

    public ItemDef GetDefByCode(string code)
    {
        if (items.ContainsKey(code))
        {
            return items[code];
        }
        return null;
    }

    internal AmmoType GetAmmoCode(string code)
    {
        return ammoTypes[code];
    }
}
