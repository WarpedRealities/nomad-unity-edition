﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;


public class AmmunitionTypeBuilder
{
    internal static Dictionary<string, AmmoType> LoadAmmoTypes()
    {
        Dictionary<string, AmmoType> ammoTypes = new Dictionary<string, AmmoType>();

        ammoTypes = ScanFolder("gameData/data/ammoTypes", ammoTypes);

        return ammoTypes;
    }

    private static Dictionary<string, AmmoType> ScanFolder(string file, Dictionary<string, AmmoType> ammoTypes)
    {
        string[] strings = Directory.GetFiles(file, "*", SearchOption.AllDirectories);

        for (int i = 0; i < strings.Length; i++)
        {
            //  strings[i]=strings[i].Replace("\\","/");

            if (File.Exists(strings[i]))
            {

                FileStream fileStream = File.Open(strings[i], FileMode.Open);
                XmlDocument document = new XmlDocument();
                document.Load(fileStream);
                AmmoType at = BuildAmmo(document);
                ammoTypes.Add(at.GetCodeName(), at);
            }
        }
        return ammoTypes;
    }

    private static AmmoType BuildAmmo(XmlDocument document)
    {
   
        XmlElement element = (XmlElement)document.FirstChild.NextSibling;

        string codeName = element.GetAttribute("ID");

        string name = element.GetAttribute("name");
        string description = null;
        string shortName = element.GetAttribute("shortName");
        float damageMod = 1, penetrationMod = 0, valueMod=1;
        float impactMod = 1;
        int accuracyMod = 0;
        List<Effect> effects=null;

        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement elem = (XmlElement)element.ChildNodes[i];
                if (elem.Name.Equals("damageMod"))
                {
                    damageMod = float.Parse(elem.GetAttribute("value"));
                }
                if (elem.Name.Equals("penetrationMod"))
                {
                    penetrationMod = float.Parse(elem.GetAttribute("value"));
                }
                if (elem.Name.Equals("accuracyMod"))
                {
                    accuracyMod = int.Parse(elem.GetAttribute("value"));
                }
                if (elem.Name.Equals("valueMod"))
                {
                    valueMod = float.Parse(elem.GetAttribute("value"));
                }
                if (elem.Name.Equals("impactMod"))
                {
                    impactMod = float.Parse(elem.GetAttribute("value"));
                }
                if (elem.Name.Equals("extraEffects"))
                {
                    effects = (BuildEffects(elem));
                }
                if (elem.Name.Equals("description"))
                {
                    description = element.InnerText;

                }
            }
        }
        return new AmmoType(name, codeName, description, shortName, damageMod, penetrationMod, valueMod, accuracyMod , impactMod, effects);
    }

    private static List<Effect> BuildEffects(XmlElement element)
    {
        List<Effect> effects = new List<Effect>();
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement elem = (XmlElement)element.ChildNodes[i];
                if ("effectDamage".Equals(elem.Name))
                {
                    effects.Add(CombatMoveLoader.BuildEffectDamage(elem, false));
                }
            }
        }
        return effects;
    }

}
