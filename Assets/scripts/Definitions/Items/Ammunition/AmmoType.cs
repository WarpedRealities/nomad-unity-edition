﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoType
{
    internal float damageMod;
    internal float penetration;
    internal float valueMod;
    internal float impactMod;
    internal int accuracyMod;
    internal List<Effect> extraEffects;
    internal string codeName, name, shortName, description;

    public AmmoType(string name, string codeName, string description, string shortName, float damageMod, float penetrationMod, float valueMod, int accuracyMod, float impactMod, List<Effect> effects)
    {
        this.shortName = shortName;
        this.name = name;
        this.description = description;
        this.codeName = codeName;
        this.damageMod = damageMod;
        this.valueMod = valueMod;
        this.penetration = penetrationMod;
        this.accuracyMod = accuracyMod;
        this.impactMod = impactMod;
        this.extraEffects = effects;
    }

    public string GetCodeName()
    {
        return codeName;
    }

    public string GetName()
    {
        return name;
    }

    public string GetDescription()
    {
        return description;
    }

    public float GetDamageMod()
    {
        return damageMod;
    }
    public float GetPenetration()
    {
        return penetration;
    }

    public string GetShortName()
    {
        return shortName;
    }

    public int GetAccuracy()
    {
        return accuracyMod;
    }
    public List<Effect> GetExtraEffects()
    {
        return extraEffects;
    }

    public float GetImpactMod()
    {
        return impactMod;
    }

    public float GetValueMod()
    {
        return valueMod;
    }

}
