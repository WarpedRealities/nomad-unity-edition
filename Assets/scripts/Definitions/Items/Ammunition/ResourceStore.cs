﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResourceStore 
{
    string defaultAmmoType;
    int capacity;//if capacity is less than 1 use the item capacity for the resource instance
    List <string> compatabilities;
    int reloadCost;
    RefillDef refillDef;

    public ResourceStore(int capacity, List<string> compatabilities, int reloadCost,RefillDef refillDef, string defaultAmmoType)
    {
        this.defaultAmmoType = defaultAmmoType;
        this.capacity = capacity;
        this.compatabilities = compatabilities;
        this.refillDef = refillDef;
        this.reloadCost = reloadCost;
    }

    public string GetDefaultAmmoType()
    {
        return defaultAmmoType;
    }

    public int GetCapacity()
    {
        return capacity;
    }

    public List<string> GetCompatabilities()
    {
        return compatabilities;
    }
    public int GetReloadCost()
    {
        return reloadCost;
    }

    public RefillDef GetRefillDef()
    {
        return refillDef;
    }
}
