﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization;

[Serializable]
public class ResourceStoreInstance 
{
    string ammoCode;
    [NonSerialized]
    AmmoType ammoType;
    string ammoTypeString;
    int capacity;
    int ammoPerShot;
    float amount;
    [NonSerialized]
    private ResourceStore resourceStore;
    private string itemDef;

    public ResourceStoreInstance(ResourceStore resourceStore, string itemDef, int ammoPerShot=1)
    {
        this.itemDef = itemDef;
        this.ammoType = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(resourceStore.GetDefaultAmmoType());
        this.ammoPerShot = ammoPerShot;
        this.resourceStore = resourceStore;
        this.capacity = resourceStore.GetCapacity() > 0 ? resourceStore.GetCapacity() : 0;
        if (resourceStore.GetCompatabilities().Count > 0)
        {
            this.ammoCode = resourceStore.GetCompatabilities()[0];
        }

    }
    [OnSerializing()]
    internal void OnSerialize(StreamingContext streamingContext)
    {
        ammoTypeString = ammoType.GetCodeName();
    }

    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        ammoType = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(ammoTypeString);
        ItemDef def = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(itemDef);
        if (def.GetItemType() == ItemType.EQUIP)
        {
            resourceStore = ((ItemEquippable)def).GetResourceStore();
        }
        if (def.GetItemType() == ItemType.AMMO)
        {
            resourceStore = ((ItemAmmo)def).GetResourceStore();
        }
    }

    public ResourceStore GetDef()
    {
        return resourceStore;
    }

    internal void SetAmmoType(AmmoType ammoType)
    {
        this.ammoType = ammoType;
    }

    internal void SetAmount(float amount)
    {
        this.amount = amount;
    }

    internal void ExpendAmmo(int ammocost)
    {
        this.amount -= ammocost;
    }

    internal int GetAmount()
    {
        return (int)this.amount;
    }

    internal string GetString()
    {
        return "("+this.ammoType.GetShortName() + ")"+ (this.amount / ammoPerShot) + "/" + (capacity / ammoPerShot);
    }

    public int GetCapacity()
    {
        return capacity;
    }

    public void SetCapacity(int value)
    {
        capacity=value;
    }

    internal AmmoType GetAmmoType()
    {
        return ammoType;
    }

    public string GetAmmoCode()
    {
        return ammoCode;
    }

    public void SetAmmoCode(string ammoCode)
    {
        this.ammoCode = ammoCode;
    }
}
