﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefillDef 
{
    bool usesFiller;
    string fillRequires;

    public RefillDef(bool useFiller, string fillRequires)
    {
        this.usesFiller = useFiller;
        this.fillRequires = fillRequires;
    
    }

    public bool isFiller()
    {
        return usesFiller;
    }

    public string GetFillRequirement()
    {
        return fillRequires;
    }
}
