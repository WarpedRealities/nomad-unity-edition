﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemResource : ItemDef
{

    public ItemResource(string name, string codeName, float weight, int value, string description, int icon)
    {
        this.codeName = codeName;
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.icon = icon;
        this.description = description;
    }


    public override ItemType GetItemType()
    {
        return ItemType.RESOURCE;
    }

    public override ItemUse GetItemUse()
    {
        return ItemUse.NONE;
    }

}
