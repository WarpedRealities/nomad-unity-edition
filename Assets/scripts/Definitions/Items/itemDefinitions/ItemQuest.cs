﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemQuest : ItemDef
{

    public ItemQuest(string code, string name, string description, int value, float weight)
    {
        this.codeName = code;
        this.name = name;
        this.description = description;
        this.value = value;
        this.weight = weight;
    }


    public override ItemType GetItemType()
    {
        return ItemType.QUEST;
    }

    public override ItemUse GetItemUse()
    {
        return ItemUse.NONE;
    }
}
