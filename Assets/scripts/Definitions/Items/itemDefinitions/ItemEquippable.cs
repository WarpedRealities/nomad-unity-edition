﻿using UnityEngine;
using System.Collections;
using System;

public class HazardProtection
{
    public String hazardType;
    public float protectionValue;

    public HazardProtection(string hazardType, float protectionValue)
    {
        this.hazardType = hazardType;
        this.protectionValue = protectionValue;
    }
}
public class ItemEquippable : ItemDef
{

    private readonly int STANDARD = 10;
    private readonly int BODY = 50;
    private readonly int HEAD = 25;
    private readonly int ACCESSORY = 20;

    private EquipSlot equipSlot;
    private SkillModifiers skillModifiers;
    private DefenceModifiers defenceModifiers;
    private EquipmentMoves equipmentMoves;
    private ResourceStore resourceStore;
    private HazardProtection[] hazardProtections;
    private bool canStack;
    private string moveTags;
    private string wornDescription;

    public ItemEquippable(string name, string code, float weight, int value, string description, EquipSlot equipSlot, bool canStack, int icon)
    {
        this.canStack = canStack;
        this.equipSlot = equipSlot;
        this.name = name;
        this.codeName = code;
        this.weight = weight;
        this.value = value;
        this.description = description;
        this.icon = icon;
    }

    public void SetWornDescription(string desc)
    {
        wornDescription = desc;
    }

    public string GetWornDescription()
    {
        return wornDescription;
    }

    public void SetMoveTags(string tags)
    {
        this.moveTags = tags;
    }

    public string GetMoveTags()
    {
        return moveTags;
    }

    public void SetResourceStore(ResourceStore resourceStore)
    {
        this.resourceStore = resourceStore;
    }

    public ResourceStore GetResourceStore()
    {
        return resourceStore;
    }

    public EquipSlot GetSlot()
    {
        return equipSlot;
    }


    public override bool IsStackable()
    {
        return canStack;
    }

    public override ItemType GetItemType()
    {
        return ItemType.EQUIP;
    }

    public override ItemUse GetItemUse()
    {
        return ItemUse.EQUIP;
    }

    public int GetEquipTime()
    {
        switch (equipSlot)
        {
            case EquipSlot.TORSO:
                return BODY;
            case EquipSlot.LEGS:
                return BODY;
            case EquipSlot.ARMS:
                return BODY;
            case EquipSlot.FEET:
                return BODY;
            case EquipSlot.HEAD:
                return HEAD;
            case EquipSlot.ACCESSORY:
                return ACCESSORY;
        }
        return STANDARD;
    }

    public void SetSkill(SkillModifiers skill)
    {
        this.skillModifiers = skill;
    }

    public void SetDefence(DefenceModifiers defence)
    {
        this.defenceModifiers = defence;
    }

    public SkillModifiers GetSkillModifiers()
    {
        return skillModifiers;
    }

    public DefenceModifiers GetDefenceModifiers()
    {
        return defenceModifiers;
    }
    
    public EquipmentMoves GetMoves()
    {
        return equipmentMoves;
    }

    public void SetMoves(EquipmentMoves equipmentMoves)
    {
        this.equipmentMoves = equipmentMoves;
    }

    public void SetHazardProtections(HazardProtection[] hazardProtections)
    {
        this.hazardProtections = hazardProtections;
    }

    public HazardProtection [] GetHazardProtections()
    {
        return this.hazardProtections;
    }
}
