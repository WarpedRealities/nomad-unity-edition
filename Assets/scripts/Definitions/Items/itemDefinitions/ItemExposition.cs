﻿using UnityEngine;
using System.Collections;

public class ItemExposition : ItemDef
{

    public ItemExposition(string code, string name, string description, int value, float weight,int icon)
    {
        this.codeName = code;
        this.name = name;
        this.description = description;
        this.value = value;
        this.weight = weight;
        this.icon = icon;
    }


    public override ItemType GetItemType()
    {
        return ItemType.EXPOSITION;
    }

    public override ItemUse GetItemUse()
    {
        return ItemUse.USE;
    }
}
