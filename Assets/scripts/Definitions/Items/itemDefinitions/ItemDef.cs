﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class ItemDef 
{
    protected string name, codeName;
    protected string description;
    protected float weight;
    protected int value;
    protected int icon;
    public virtual string GetName()
    {
        return name;
    }

    public string GetCodeName()
    {
        return codeName;
    }

    public virtual string GetDescription()
    {
        return description;
    }

    public virtual float GetWeight()
    {
        return weight;
    }

    public int GetValue()
    {
        return value;
    }

    public int GetIcon()
    {
        return icon;
    }
    public abstract ItemType GetItemType();

    public abstract ItemUse GetItemUse();

    public virtual string GetItemString()
    {
        return name+ " "+weight;
    }

    public virtual bool IsStackable()
    {
        return true;
    }
}
