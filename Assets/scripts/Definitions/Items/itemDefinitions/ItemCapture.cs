using UnityEngine;
using System.Collections;

public class ItemCapture : ItemDef
{

    public ItemCapture(string code, string name, string description, int value, float weight,int icon)
    {
        this.codeName = code;
        this.name = name;
        this.description = description;
        this.value = value;
        this.weight = weight;
        this.icon = icon;
    }

    public override ItemType GetItemType()
    {
        return ItemType.CAPTURE;
    }

    public override ItemUse GetItemUse()
    {
        return ItemUse.NONE;
    }
}
