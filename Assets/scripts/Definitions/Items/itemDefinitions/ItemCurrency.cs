using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCurrency : ItemDef
{
    bool isCredit;


    public ItemCurrency(string name, string codeName, float weight, int value, string description, 
        bool credit, int icon)
    {
        this.isCredit = credit;
        this.codeName = codeName;
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.description = description;
        this.icon = icon;
    }

    public override ItemType GetItemType()
    {
        return ItemType.CURRENCY;
    }

    public override ItemUse GetItemUse()
    {
        return ItemUse.NONE;
    }

    public bool GetIsCredits()
    {
        return isCredit;
    }
}
