﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ItemConsumable : ItemDef
{
    protected List<Effect> effects;

    public ItemConsumable(string name, string codeName, float weight, int value, string description, int icon)
    {
       
        this.codeName = codeName;
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.description = description;
        this.icon = icon;
    }

    public void SetEffects(List<Effect> lists)
    {
        this.effects = lists;
    }



    public override ItemType GetItemType()
    {
        return ItemType.CONSUMABLE;
    }


    public override ItemUse GetItemUse()
    {
        return ItemUse.USE;
    }

    public List<Effect> GetEffects()
    {
        return effects;
    }
}
