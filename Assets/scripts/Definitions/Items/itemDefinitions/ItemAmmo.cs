﻿using UnityEngine;
using System.Collections;

public class ItemAmmo : ItemDef
{

    bool loader;
    string compatabilityType;
    ResourceStore resourceStore;

    public ItemAmmo(string name, string code, float weight, int 
        value, string description, string compatability, 
        ResourceStore resourceStore,  bool loader, int icon)
    {
        this.name = name;
        this.codeName = code;
        this.weight = weight;
        this.value = value;
        this.description = description;
        this.compatabilityType = compatability;
        this.resourceStore = resourceStore;
        this.loader = loader;
        this.icon = icon;
    }

    public override ItemType GetItemType()
    {
        return ItemType.AMMO;
    }

    public override ItemUse GetItemUse()
    {
        return ItemUse.NONE;
    }

    public string GetCompatabilityType()
    {
        return compatabilityType;
    }

    public ResourceStore GetResourceStore()
    {
        return resourceStore;
    }
}
