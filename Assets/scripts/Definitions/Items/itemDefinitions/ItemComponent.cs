﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComponent : ItemDef
{
    private string componentFilename;
    private SLOTTYPE slotType;
    private SHIPWEAPONSIZE shipWeaponSize;

    public ItemComponent(string name, string codeName, float weight, int value, string description, string component, SLOTTYPE slotType, int icon)
    {
        this.componentFilename = component;
        this.codeName = codeName;
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.description = description;
        this.slotType = slotType;
        this.icon = icon;
    }
    public ItemComponent(string name, string codeName, float weight, int value, string description, string component, SLOTTYPE slotType,SHIPWEAPONSIZE size, int icon)
    {
        this.componentFilename = component;
        this.codeName = codeName;
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.description = description;
        this.slotType = slotType;
        this.shipWeaponSize = size;
        this.icon = icon;
    }
    public override ItemType GetItemType()
    {
        return ItemType.COMPONENT;
    }

    public override ItemUse GetItemUse()
    {
        return ItemUse.NONE;
    }

    public string GetComponent()
    {
        return componentFilename;
    }

    public SLOTTYPE GetSlotType()
    {
        return slotType;
    }

    internal SHIPWEAPONSIZE GetSize()
    {
        return shipWeaponSize;
    }
}
