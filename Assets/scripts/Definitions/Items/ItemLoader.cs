﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using UnityEngine;

public class ItemLoader
{
    public static Dictionary<string, ItemDef> LoadItems()
    {
        Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
        Dictionary<string, ItemDef> items = new Dictionary<string, ItemDef>();

        items = ScanItems("gameData/data/items", items);

        return items;
    }

    public static Dictionary<string, ItemDef> ScanItems(string file, Dictionary<string, ItemDef> items)
    {
        //get all files in this directory
        string[] strings = Directory.GetFiles(file,"*",SearchOption.AllDirectories);
     
        for (int i = 0; i < strings.Length; i++) {
            //  strings[i]=strings[i].Replace("\\","/");
   
            if (File.Exists(strings[i]))
            {
                try
                {
                    FileStream fileStream = File.Open(strings[i], FileMode.Open);
                    XmlDocument document = new XmlDocument();
                    document.Load(fileStream);
                    ItemDef it = BuildItem(document);
                    items.Add(it.GetCodeName(), it);
                } catch (Exception e)
                {
                    String str = "problem loading item definition " + strings[i];
                    Debug.Log(str);
                    throw new Exception(str, e);
                }

            }
        }
        return items;
    }

    private static ItemDef BuildItem(XmlDocument document)
    {
        XmlElement element = (XmlElement)document.FirstChild.NextSibling;
        if ("ItemResource".Equals(element.Name))
        {
            return BuildResource(element);
        }
        if ("ItemConsumable".Equals(element.Name))
        {
            return BuildConsumable(element);
        }
        if ("ItemWeapon".Equals(element.Name))
        {
            return BuildEquip(element);
        }
        if ("ItemEquip".Equals(element.Name))
        {
            return BuildEquip(element);
        }
        if ("ItemAmmo".Equals(element.Name))
        {
            return BuildAmmo(element);
        }
        if ("ItemComponent".Equals(element.Name))
        {
            return BuildComponent(element);
        }
        if ("ItemExposition".Equals(element.Name))
        {
            return BuildExposition(element);
        }
        if ("ItemKey".Equals(element.Name))
        {
            return BuildKey(element);
        }
        if ("ItemBlueprint".Equals(element.Name))
        {
            return BuildBlueprint(element);
        }
        if ("ItemCurrency".Equals(element.Name))
        {
            return BuildCurrency(element);
        }
        if ("ItemCapture".Equals(element.Name))
        {
            return BuildCapture(element);
        }
        return null;
    }

    private static ItemDef BuildBlueprint(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        string description = element.GetElementsByTagName("description")[0].InnerText;
        return new ItemBlueprint(code, name, description, value, weight, icon);
    }

    private static ItemDef BuildCapture(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        string description = element.GetElementsByTagName("description")[0].InnerText;
        return new ItemCapture(code, name, description, value, weight, icon);
    }

    private static ItemDef BuildCurrency(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        bool isCurrency = "true".Equals(element.GetAttribute("credit"));
        string description = element.GetElementsByTagName("description")[0].InnerText;
        return new ItemCurrency(name,code, weight, value, description, isCurrency,icon);
    }

    private static ItemDef BuildKey(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        string description = element.GetElementsByTagName("description")[0].InnerText;
        return new ItemKey(code, name, description, value, weight, icon);
    }

    private static ItemDef BuildExposition(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        string description = element.GetElementsByTagName("description")[0].InnerText;
        return new ItemExposition(code, name, description, value, weight,icon);
    }

    private static ItemDef BuildComponent(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        XmlElement xmlElement = ((XmlElement)element.GetElementsByTagName("component")[0]);
        string component = xmlElement.GetAttribute("value");
        string description = element.GetElementsByTagName("description")[0].InnerText;
        SLOTTYPE type = EnumTools.strToSlotType(xmlElement.GetAttribute("type"));
        if (type == SLOTTYPE.WEAPON)
        {
            SHIPWEAPONSIZE size = EnumTools.strToWeaponSize(xmlElement.GetAttribute("size"));
            return new ItemComponent(name, code, weight, value, description, component, type,size, icon);
        }
        else
        {
            return new ItemComponent(name, code, weight, value, description, component, type, icon);
        }

    }

    private static ItemDef BuildAmmo(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        string compatability = element.GetAttribute("compatability");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        XmlNodeList children = element.ChildNodes;
        string description = null;
        bool loader=false;

        ResourceStore resourceStore = null;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("resourceStore".Equals(e.Name))
                {
                    resourceStore = BuildResourceStore(e);
                }
                if ("ammoFlags".Equals(e.Name))
                {
                    loader = "true".Equals(e.GetAttribute("loader"));
                }
            }
        }
        return new ItemAmmo(name, code, weight, value, description, compatability, resourceStore, loader, icon);
    }

    private static ItemDef BuildEquip(XmlElement element)
    {
        EquipSlot equipSlot = EnumTools.StrToEquip(element.GetAttribute("slot"));
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        bool stack = "true".Equals(element.GetAttribute("stackable"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        string description = null;
        string wornDescription = null;
        string tagMoves = null;
        XmlNodeList children = element.ChildNodes;
        DefenceModifiers defence = null;
        SkillModifiers skill = null;
        EquipmentMoves moves = null;
        ResourceStore resourceStore = null;
        HazardProtection[] hazardProtections = null;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("tagMoves".Equals(e.Name))
                {
                    tagMoves = e.GetAttribute("value");
                }
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("wornDescription".Equals(e.Name))
                {
                    wornDescription = e.InnerText;
                }
                if ("defenceResistMod".Equals(e.Name))
                {
                    defence = new DefenceModifiers(e);
                }
                if ("skillMod".Equals(e.Name))
                {
                    skill = new SkillModifiers(e);
                }
                if ("moves".Equals(e.Name))
                {
                    moves = new EquipmentMoves(CombatMovesBuilder.Build(e));
                }
                if ("resourceStore".Equals(e.Name))
                {

                    resourceStore = BuildResourceStore(e);
                }
                if ("hazardProtection".Equals(e.Name))
                {
                    hazardProtections = new HazardProtection[int.Parse(e.GetAttribute("count"))];
                    int index = 0;
                    for (int j = 0; j < e.ChildNodes.Count; j++)
                    {
                        XmlElement el = (XmlElement)e.ChildNodes[j];
                        hazardProtections[index] = new HazardProtection(el.GetAttribute("hazard"), float.Parse(el.GetAttribute("value")));
                        index++;
                    }

                }
            }
        }
        ItemEquippable itemEquippable = new ItemEquippable(name, code, weight, value, description, equipSlot, stack, icon);
        if (tagMoves != null)
        {
            itemEquippable.SetMoveTags(tagMoves);
        }
        if (wornDescription != null)
        {
            itemEquippable.SetWornDescription(wornDescription);
        }
        if (defence != null)
        {
            itemEquippable.SetDefence(defence);
        }
        if (skill != null)
        {
            itemEquippable.SetSkill(skill);
        }
        if (moves != null)
        {
            itemEquippable.SetMoves(moves);
        }
        if (hazardProtections != null)
        {
            itemEquippable.SetHazardProtections(hazardProtections);
        }
        if (resourceStore != null)
        {
            itemEquippable.SetResourceStore(resourceStore);
        }
        return itemEquippable;
    }

    private static ResourceStore BuildResourceStore(XmlElement element)
    {
        int capacity = int.Parse(element.GetAttribute("capacity"));
        int reloadCost = 0; int.TryParse(element.GetAttribute("reloadCost"), out reloadCost);
        List<string> compatabilities = new List<string>();
        RefillDef refillDef = null;
        string defaultAmmoType = null; 
        XmlNodeList children = element.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("refillData".Equals(e.Name))
                {

                    refillDef = new RefillDef("true".Equals(e.GetAttribute("fillerRequired")), e.GetAttribute("fillWith"));
                }
                if ("compatability".Equals(e.Name))
                {
                    compatabilities.Add(e.GetAttribute("value"));
                }
                if ("defaultAmmoType".Equals(e.Name))
                {
                    defaultAmmoType = e.GetAttribute("value");

                }
            }
        }
        return new ResourceStore(capacity, compatabilities,reloadCost,refillDef, defaultAmmoType);
    }

    private static ItemDef BuildConsumable(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        string description = null;

        List<Effect> list = new List<Effect>();

        XmlNodeList children = element.ChildNodes;
        for (int i=0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("effectRecover".Equals(e.Name))
                {
     
                    list.Add(CombatMoveLoader.BuildEffectRecover(e));
                }
                if ("effectMutator".Equals(e.Name))
                {
                    list.Add(MutatorLoader.LoadMutator(e));
                }
                if ("effectPerk".Equals(e.Name))
                {
                    list.Add(CombatMoveLoader.BuildEffectPerk(e));
                }
                if ("effectStatus".Equals(e.Name))
                {
                    list.Add(CombatMoveLoader.BuildEffectStatus(e));
                }
                if ("effectStatusRemove".Equals(e.Name))
                {
                    list.Add(CombatMoveLoader.BuildEffectStatusRemove(e));
                }
                if ("effectSummon".Equals(e.Name))
                {
                    list.Add(CombatMoveLoader.BuildEffectSummon(e));
                }
                if ("effectKarma".Equals(e.Name))
                {
                    list.Add(CombatMoveLoader.BuildEffectKarma(e));
                }
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }

            }
        }

        ItemConsumable item = new ItemConsumable(name, code, weight, value, description, icon);
        item.SetEffects(list);
        return item;
    }

    private static ItemDef BuildResource(XmlElement element)
    {
        string name = element.GetAttribute("name");
        string code = element.GetAttribute("ID");
        float weight = float.Parse(element.GetAttribute("weight"));
        int value = int.Parse(element.GetAttribute("value"));
        int icon = int.Parse(element.GetAttribute("icon"));
        string description = element.GetElementsByTagName("description")[0].InnerText;
        return new ItemResource(name, code, weight, value, description, icon);
    }
}