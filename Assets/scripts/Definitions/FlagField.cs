﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class FlagField 
{
    Dictionary<string, int> flags;

    public FlagField()
    {
        flags = new Dictionary<string, int>();
    }

    public int ReadFlag(string flag)
    {
        if (!flags.ContainsKey(flag))
        {
            return 0;
        }
        return flags[flag];
    }

    public void SetFlag(string flag, int value)
    {
  
        flags[flag] = value;
    }

    public void IncrementFlag(string flag, int value)
    {
        if (!flags.ContainsKey(flag))
        {
            flags[flag] = 0;
        }
        flags[flag] = flags[flag] + value;

    }
    public void DecrementFlag(string flag, int value)
    {
        flags[flag] = flags[flag] - value;
    }

    internal void Clear()
    {
        flags.Clear();
    }
}
