﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class PerkLibrary 
{
    Dictionary<string, Perk> perks;
  
    public PerkLibrary()
    {
        perks = PerkLoader.Load();
    }

    public List <Perk> GetPerks()
    {
        return perks.Values.ToList();
    }

    public Perk GetPerk(string perkCode)
    {
        if (perks.ContainsKey(perkCode))
        {
            return perks[perkCode];
        }
        else
        {
            return null;
        }
    }

}
