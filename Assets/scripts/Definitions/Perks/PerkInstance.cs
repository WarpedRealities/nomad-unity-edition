﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;


[Serializable]
public class PerkInstance 
{
    private string perkCode;
    private int perkRank;
    [NonSerialized]
    private Perk perkRef;

    public PerkInstance(Perk perk, int rank)
    {
        this.perkRef = perk;
        this.perkRank = rank;
    }

    [OnSerializing()]
    internal void OnSerialize(StreamingContext streamingContext)
    {
        perkCode = perkRef.GetCode();
    }

    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        perkRef = GlobalGameState.GetInstance().GetUniverse().GetPerkLibrary().GetPerk(perkCode);
    }

    public int GetRank()
    {
        return perkRank;
    }

    public Perk GetRef()
    {
        return perkRef;
    }

    public string GetName()
    {
        return perkRef.GetMaxRank()>1 ? perkRef.GetName() + " " + perkRank: perkRef.GetName();
    }

    internal void SetRank(int rank)
    {
        this.perkRank = rank;
    }
}
