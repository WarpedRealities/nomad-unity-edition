﻿using UnityEngine;
using System.Collections;

public interface PerkEffect {

    PerkEffectType GetEffectType();
}
