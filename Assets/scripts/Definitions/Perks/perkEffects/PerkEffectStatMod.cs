﻿using UnityEngine;
using System.Collections;

public class PerkEffectStatMod : PerkEffect
{
    ST stat;
    int amount;

    public PerkEffectStatMod(ST stat, int amount)
    {
        this.amount = amount;
        this.stat = stat;
    }

    public PerkEffectType GetEffectType()
    {
        return PerkEffectType.STATMOD;
    }

    public int GetAmount()
    {
        return amount;
    }

    public ST GetStat()
    {
        return stat;
    }
}
