
public class PerkEffectMoveAdd : PerkEffect
{
    string moveName;
    Effect effect;


    public PerkEffectMoveAdd(string moveName, Effect effect)
    {
        this.moveName = moveName;  
        this.effect = effect;
    }

   
    public string GetName()
    {
        return moveName;
    }


    public PerkEffectType GetEffectType()
    {
        return PerkEffectType.MOVEADD;
    }

    public Effect GetEffect()
    {
        return effect;
    }

    public string GetMoveName()
    {
        return moveName;
    }
}