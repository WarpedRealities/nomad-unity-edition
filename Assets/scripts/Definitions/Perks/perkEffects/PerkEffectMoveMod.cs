﻿using UnityEngine;
using System.Collections;



public class PerkEffectMoveMod : PerkEffect
{
    string moveName;
    InterruptType interruptType = InterruptType.UNDEFINED;
    int bonusAdjust=0;
    DR damageType=DR.NONE;
    int minMod=0, maxMod=0;
    AttackPattern pattern= AttackPattern.NONE;
    float decayMod=1;

    public PerkEffectMoveMod(string moveName, InterruptType interruptType)
    {
        this.moveName = moveName;
        this.interruptType = interruptType;
    }
    public PerkEffectMoveMod(string moveName, AttackPattern attackPattern)
    {
        this.moveName = moveName;
        this.pattern = attackPattern;
    }
    public PerkEffectMoveMod(string moveName, int min, int max, DR damageType)
    {
        this.moveName = moveName;
        this.damageType = damageType;
        this.minMod = min;
        this.maxMod = max;
    }
    public PerkEffectMoveMod(string moveName, float decayMod)
    {
        this.moveName = moveName;
        this.decayMod = decayMod;
    }
    public string GetName()
    {
        return moveName;
    }

    public int GetMin()
    {
        return minMod;
    }

    public int GetMax()
    {
        return maxMod;
    }

    public InterruptType GetInterruptType()
    {
        return interruptType;
    }

    public AttackPattern GetAttackPattern()
    {
        return pattern;
    }

    public DR GetDamageType()
    {
        return damageType;
    }

    public int Getbonus()
    {
        return bonusAdjust;
    }

    public float GetDecayMod()
    {
        return decayMod;
    }

    public PerkEffectType GetEffectType()
    {
        return PerkEffectType.MOVEMOD;
    }
}
