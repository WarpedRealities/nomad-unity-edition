using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkEffectSecondaryMod : PerkEffect
{
    SV secondaryValue;
    float amount;

    public PerkEffectSecondaryMod(SV secondaryValue, float amount)
    {
        this.amount = amount;
        this.secondaryValue = secondaryValue;
    }


    public PerkEffectType GetEffectType()
    {
        return PerkEffectType.SECONDARYMOD;
    }

    public float GetAmount()
    {
        return amount;
    }

    public SV GetSecondaryValue()
    {
        return secondaryValue;
    }
}
