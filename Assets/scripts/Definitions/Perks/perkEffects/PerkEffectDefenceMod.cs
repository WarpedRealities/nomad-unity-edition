using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkEffectDefenceMod : PerkEffect
{
    DR defenceResist;
    int amount;

    public PerkEffectDefenceMod(DR defenceResist, int amount)
    {
        this.amount = amount;
        this.defenceResist = defenceResist;
    }


    public PerkEffectType GetEffectType()
    {
        return PerkEffectType.DEFENCEMOD;
    }

    public int GetAmount()
    {
        return amount;
    }

    public DR GetDefenceResist()
    {
        return defenceResist;
    }
}
