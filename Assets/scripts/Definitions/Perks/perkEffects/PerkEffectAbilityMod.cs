﻿using UnityEngine;
using System.Collections;
using System;

public class PerkEffectAbilityMod : PerkEffect
{
    int modifierValue;
    AB ability;

    public PerkEffectAbilityMod(AB ability, int value)
    {
        this.ability = ability;
        this.modifierValue = value;
    }


    public PerkEffectType GetEffectType()
    {
        return PerkEffectType.ABILITYMOD;
    }

    internal AB GetAbility()
    {
        return ability;
    }

    internal int GetValue()
    {
        return modifierValue;
    }
}
