﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkEffectSkillMod : PerkEffect
{
    SK skill;
    int modValue;

    public PerkEffectSkillMod(SK skill, int modValue)
    {
        this.skill = skill;
        this.modValue = modValue;
    }

    public PerkEffectType GetEffectType()
    {
        return PerkEffectType.SKILLMOD;
    }

    public int GetModValue()
    {
        return modValue;
    }

    public SK GetSkill()
    {
        return skill;
    }
}
