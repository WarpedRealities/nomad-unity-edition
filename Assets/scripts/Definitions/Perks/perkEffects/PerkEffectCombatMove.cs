﻿using UnityEngine;
using System.Collections;

public class PerkEffectCombatMove : PerkEffect
{

    CombatMove combatMove;

    public PerkEffectCombatMove(CombatMove move)
    {
        this.combatMove = move;
    }

    public PerkEffectType GetEffectType()
    {
        return PerkEffectType.MOVE;
    }

    public CombatMove GetMove()
    {
        return combatMove;
    }
}
