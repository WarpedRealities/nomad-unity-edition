﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

public class PerkLoader
{
    internal static Dictionary<string, Perk> Load()
    {
        Dictionary<string, Perk> perks = new Dictionary<string, Perk>();

        perks = ScanPerks("gameData/data/perks", perks);
        return perks;
    }

    private static Dictionary<string, Perk> ScanPerks(string file, Dictionary<string, Perk> perks)
    {
        //get all files in this directory
        string[] strings = Directory.GetFiles(file, "*", SearchOption.AllDirectories);

        for (int i = 0; i < strings.Length; i++)
        {
            //  strings[i]=strings[i].Replace("\\","/");

            if (File.Exists(strings[i]))
            {

                FileStream fileStream = File.Open(strings[i], FileMode.Open);
                XmlDocument document = new XmlDocument();
                document.Load(fileStream);
                Perk perk = BuildPerk(document);
                //Debug.Log("file name"+ strings[i] + " perk name " + perk.GetName()+" code "+perk.GetCode());
                perks.Add(perk.GetCode(), perk);
            }
        }
        return perks;
    }

    private static Perk BuildPerk(XmlDocument document)
    {

        XmlElement root = (XmlElement)document.FirstChild.NextSibling;
        XmlNodeList children = root.ChildNodes;
        string name = root.GetAttribute("name");
        string codeName = root.GetAttribute("codeName");
        string description = null;
        int maxRank = 0;
        List<PerkRequirement> perkRequirements = null;
        List<PerkEffect> perkEffects = null;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];

                if ("description".Equals(xmlElement.Name))
                {
                    description = xmlElement.InnerText;
                }
                if ("maxRank".Equals(xmlElement.Name))
                {
                    maxRank = int.Parse(xmlElement.GetAttribute("value"));
                }
                if ("effects".Equals(xmlElement.Name))
                {
                    perkEffects = new List<PerkEffect>();
                    for (int j = 0; j < xmlElement.ChildNodes.Count; j++)
                    {
                        if (xmlElement.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement e = (XmlElement)xmlElement.ChildNodes[j];
                            if ("abilityMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectAbilityMod(
                                    EnumTools.StrToAbility(e.GetAttribute("ability")),
                                    int.Parse(e.GetAttribute("value"))));
                            }
                            if ("skillMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectSkillMod(
                                    EnumTools.strToSkill(e.GetAttribute("skill")),
                                    int.Parse(e.GetAttribute("value"))));

                            }
                            if ("statMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectStatMod(
                                              EnumTools.strToStat(e.GetAttribute("stat")),
                                              int.Parse(e.GetAttribute("value"))));
                            }
                            if ("defenceResistMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectDefenceMod(EnumTools.strToDefenceResist(e.GetAttribute("defenceResist")),
                                    int.Parse(e.GetAttribute("value"))));
                            }
                            if ("secondaryValueMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectSecondaryMod(EnumTools.strToSecondaryValue(e.GetAttribute("secondaryValue")),
                                    float.Parse(e.GetAttribute("value"))));
                            }
                            if ("combatMove".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectCombatMove(CombatMoveLoader.BuildCombatMove(e)));
                            }
                            if ("movePatternMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectMoveMod(e.GetAttribute("move"), EnumTools.StrToAttackPattern(e.GetAttribute("pattern"))));
                            }
                            if ("moveDecayMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectMoveMod(e.GetAttribute("move"), float.Parse(e.GetAttribute("decayMod"))));
                            }
                            if ("moveStrengthMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectMoveMod(e.GetAttribute("move"),int.Parse(e.GetAttribute("min")),int.Parse(e.GetAttribute("max")),
                                    EnumTools.strToDefenceResist(e.GetAttribute("type"))));
                            }
                            if ("moveInterruptMod".Equals(e.Name))
                            {
                                perkEffects.Add(new PerkEffectMoveMod(e.GetAttribute("move"), EnumTools.strToInterruptType(e.GetAttribute("interruptType"))));
                            }
                            if ("moveAddEffect".Equals(e.Name))
                            {
                                perkEffects.Add(BuildMoveAdd(e));
                            }
                        }
                    }
                }
                if ("requirements".Equals(xmlElement.Name))
                {
                    perkRequirements = new List<PerkRequirement>();
                    for (int j = 0; j < xmlElement.ChildNodes.Count; j++)
                    {
                        if (xmlElement.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement e = (XmlElement)xmlElement.ChildNodes[j];
                            if ("bodyPart".Equals(e.Name))
                            {
                                perkRequirements.Add(new PartRequirement(e.GetAttribute("value")));
                            }
                            if ("chargen".Equals(e.Name))
                            {
                                perkRequirements.Add(new ChargenRequirement());
                            }
                            if ("skill".Equals(e.Name))
                            {
                                perkRequirements.Add(BuildSkillRequirement(e));
                            }
                            if ("perk".Equals(e.Name))
                            {
                                perkRequirements.Add(new HasPerkRequirement(e.GetAttribute("perkName")));
                            }
                        }
                    }
                }
            }      

        }

        Perk perk = new Perk(name, codeName, description, maxRank);
        if (perkRequirements != null)
        {
            perk.SetRequirements(perkRequirements);
        }
        if (perkEffects != null)
        {
            perk.SetEffects(perkEffects);
        }


        return perk;
    }

    private static PerkEffect BuildMoveAdd(XmlElement root)
    {
        Effect effect = null;
        String moveName = root.GetAttribute("move");
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("effectDamage".Equals(element.Name))
                {
                    effect = CombatMoveLoader.BuildEffectDamage(element, false);
                }
                if ("effectStatus".Equals(element.Name))
                {
                    effect = CombatMoveLoader.BuildEffectStatus(element);
                }
                if ("effectInterrupt".Equals(element.Name))
                {
                    effect = CombatMoveLoader.BuildInterrupt(element);
                }
            }
        }
        return new PerkEffectMoveAdd(moveName, effect);
    }

    private static PerkRequirement BuildSkillRequirement(XmlElement root)
    {
        int[] skills = new int[14];
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("skill".Equals(xmlElement.Name))
                {
                    skills[(int)EnumTools.strToSkill(xmlElement.GetAttribute("skill"))] = int.Parse(xmlElement.GetAttribute("value"));
                }
            }
        }

        return new SkillRequirement(skills);
    }
}
