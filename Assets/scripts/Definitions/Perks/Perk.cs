﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perk
{
    List<PerkRequirement> requirements;
    List<PerkEffect> effects;
    string perkName, perkCode, perkDescription;
    int maxRank;

    public Perk(string name, string code, string description, int maxRank)
    {
        this.perkName = name;
        this.perkCode = code;
        this.perkDescription = description;
        this.maxRank = maxRank;
    }

    public void SetRequirements(List<PerkRequirement> requirements)
    {
        this.requirements = requirements;
    }

    public void SetEffects(List<PerkEffect> effects)
    {
        this.effects = effects;
    }

    public string GetName()
    {
        return perkName;
    }

    public string GetCode()
    {
        return perkCode;
    }

    public string GetDescription()
    {
        return perkDescription;
    }

    public int GetMaxRank()
    {
        return maxRank;
    }

    internal bool CanUse(PlayerRPG playerRPG, Appearance appearance)
    {
        if (requirements == null)
        {
            return true;
        }
        for (int i = 0; i < requirements.Count; i++)
        {
            if (!requirements[i].CanUse(appearance, playerRPG))
            {
                return false;
            }
        }

        return true;
    }

    internal List<PerkEffect> GetEffects()
    {
        return this.effects;
    }

    internal bool EligibleFor(PlayerRPG playerRPG, Appearance appearance)
    {
        if (requirements == null)
        {
            return true;
        }
        for (int i = 0; i < requirements.Count; i++)
        {
            if (!requirements[i].RequirementsMet(appearance, playerRPG))
            {
                return false;
            }
        }

        return true;
    }
}
