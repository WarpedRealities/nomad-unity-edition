﻿using UnityEngine;
using System.Collections;

public class HasPerkRequirement : PerkRequirement
{
    string perkName;

    public HasPerkRequirement(string perkName)
    {
        this.perkName = perkName;
    }

    public bool CanUse(Appearance appearance, PlayerRPG playerRPG)
    {
        return true;
    }

    public bool RequirementsMet(Appearance appearance, PlayerRPG playerRPG)
    {
        foreach (PerkInstance item in playerRPG.GetPerks())
        {
            if (item.GetRef().GetCode().Equals(perkName))
            {
                return true;
            }
        }

        return false;
    }

}
