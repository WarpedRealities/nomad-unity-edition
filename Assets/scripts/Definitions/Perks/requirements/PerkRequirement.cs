﻿using UnityEngine;
using System.Collections;

public interface PerkRequirement
{
    bool RequirementsMet(Appearance appearance, PlayerRPG playerRPG);

    bool CanUse(Appearance appearance, PlayerRPG playerRPG);

}
