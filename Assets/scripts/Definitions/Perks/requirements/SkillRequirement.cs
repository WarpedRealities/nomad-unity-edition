﻿using UnityEngine;
using System.Collections;

public class SkillRequirement : PerkRequirement
{
    int[] skills;

    public SkillRequirement(int[] skills)
    {
        this.skills = skills;
    }

    public bool CanUse(Appearance appearance, PlayerRPG playerRPG)
    {
        return true;
    }

    public bool RequirementsMet(Appearance appearance, PlayerRPG playerRPG)
    {
        for (int i = 0; i < skills.Length; i++)
        {
            if (skills[i]>0 && playerRPG.GetBaseSkill(i)< skills[i])
            {
                return false;
            }
        }
        return true;
    }
}
