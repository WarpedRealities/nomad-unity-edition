﻿using UnityEngine;
using System.Collections;

public class ChargenRequirement : PerkRequirement
{
    public bool CanUse(Appearance appearance, PlayerRPG playerRPG)
    {
        return true;
    }

    public bool RequirementsMet(Appearance appearance, PlayerRPG playerRPG)
    {
        return false;
    }
}
