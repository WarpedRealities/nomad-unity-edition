﻿using UnityEngine;
using System.Collections;

public class PartRequirement : PerkRequirement
{
    private string part;

    public PartRequirement(string part)
    {
        this.part = part;
    }

    public bool CanUse(Appearance appearance, PlayerRPG playerRPG)
    {
        return appearance.GetPart(part) != null;
    }

    public bool RequirementsMet(Appearance appearance, PlayerRPG playerRPG)
    {
        return appearance.GetPart(part) != null;
    }
}
