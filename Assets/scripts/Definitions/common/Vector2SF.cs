﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Vector2SF
{
    public float x, y;

    public Vector2SF(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
}
