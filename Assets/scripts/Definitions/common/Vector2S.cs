﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Vector2S 
{
    public int x, y;

    public Vector2S(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

[Serializable]
public class Vector2FS
{
    public float x, y;

    public Vector2FS(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
}
