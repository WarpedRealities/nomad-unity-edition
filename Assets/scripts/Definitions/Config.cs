﻿using UnityEngine;
using System.Collections;
using System;

public class Config
{

    private static Config instance;

    private PlayerPrefs preferences;
    private static readonly int VERSION = 2;
    public static Config GetInstance()
    {
        if (instance == null)
        {
            instance = new Config();
        }
        return instance;
    }

    private bool showSkillChecks;
    private bool showDamageRolls;
    private bool showClock;
    private bool showPortraits;
    private int skipTicks;
    private float inputWindow;
    private Config()
    {
        showSkillChecks = PlayerPrefs.GetInt("showSkillChecks") == 1;
        showDamageRolls = PlayerPrefs.GetInt("showDamageRolls") == 1;
        showClock = PlayerPrefs.GetInt("showClock") == 1;
        showPortraits = PlayerPrefs.GetInt("showPortraits") == 1;
        skipTicks = PlayerPrefs.GetInt("skipTicks", 5);
        inputWindow = PlayerPrefs.GetFloat("inputWindow", 0.05F);
    }

    public bool IsShowSkillChecks()
    {
        return showSkillChecks;
    }
    public bool IsShowDamageRolls()
    {
        return showDamageRolls;
    }
    public bool IsShowClock()
    {
        return showClock;
    }

    public int GetVersion()
    {
        return VERSION;
    }

    internal bool IsShowPortraits()
    {
       return showPortraits;
    }
    public void SetSkillCheck(bool value)
    {
        this.showSkillChecks = value;
        PlayerPrefs.SetInt("showSkillChecks", value ? 1 : 0);
    }
    public void SetDamageRolls(bool value)
    {
        this.showDamageRolls = value;
        PlayerPrefs.SetInt("showDamageRolls", value ? 1 : 0);
    }
    public void SetClock(bool value)
    {
        this.showClock = value;
        PlayerPrefs.SetInt("showClock", value ? 1 : 0);
    }

    internal void SetPortraits(bool value)
    {
        this.showPortraits = value;
        PlayerPrefs.SetInt("showPortraits", value ? 1 : 0);
    }

    public void SetSkipTicks(int value)
    {
        this.skipTicks = value;
        PlayerPrefs.SetInt("skipTicks", skipTicks);
    }

    internal int GetSkipTicks()
    {
        return skipTicks;
    }

    public void SetInputWindow(float value)
    {
        this.inputWindow = value;
        PlayerPrefs.SetFloat("inputWindow", inputWindow);
    }

    public float GetInputWindow()
    {
        return inputWindow;
    }
}
