﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class StatusEffect 
{
    protected int duration;
    protected string removeDescription;
    protected int uid, icon=-1;
    
    public virtual void Update()
    {
        duration--;
    } 

    public int GetUid()
    {
        return uid;
    }

    public int GetIcon()
    {
        return icon;
    }

    public string GetRemoveDescription()
    {
        return removeDescription;
    }

    public int Getduration()
    {
        return duration;
    }

    public virtual bool sustain(Actor actor, ViewInterface view)
    {

        return (duration > 0);
    }

    public virtual void apply(StatusModifiers statusModifiers)
    {

    }

    public virtual void recalculate(StatusModifiers statusModifiers)
    {

    }

    public virtual void remove(StatusModifiers statusModifiers, ViewInterface view, Actor actor)
    {

    }

    public abstract StatusEffect Clone();

}
