﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SkillModifier
{
    public SK skillType;
    public int amountChanged;
    public SkillModifier(SK type, int amount)
    {
        skillType = type;
        this.amountChanged = amount;
    }
}
[Serializable]
public class DefenceResistModifier
{
    public DR resistType;
    public int amountChanged;

    public DefenceResistModifier(DR resist, int amount)
    {
        this.resistType = resist;
        this.amountChanged = amount;
    }
}

[Serializable]
public class HazardModifier
{
    public string hazardType;
    public float strength;
    public HazardModifier(string hazardType, float strength)
    {
        this.hazardType = hazardType;
        this.strength = strength;
    }
}
[Serializable]
public class StatusModifier : StatusEffect
{
    SkillModifier[] skillModifiers;
    DefenceResistModifier[] resistModifiers;
    HazardModifier[] hazardModifiers;
    public StatusModifier(SkillModifier[] skillmods, DefenceResistModifier[] defenceMods, HazardModifier[] hazardMods, string removeDescription, int duration, int uid, int icon)
    {
        this.icon = icon;
        this.uid = uid;
        this.duration = duration;
        this.skillModifiers = skillmods;
        this.resistModifiers = defenceMods;
        this.hazardModifiers = hazardMods;
        this.removeDescription = removeDescription;
    }

    public override StatusEffect Clone()
    {
        StatusModifier clone = new StatusModifier(skillModifiers, resistModifiers, hazardModifiers, removeDescription,duration,uid,icon);
        return clone;
    }

    public override void apply(StatusModifiers statusModifiers)
    {
        if (skillModifiers != null)
        {
            foreach (SkillModifier skill in skillModifiers)
            {
                statusModifiers.skills[(int)skill.skillType] += skill.amountChanged;
            }
        }
        if (resistModifiers != null)
        {
            foreach (DefenceResistModifier resist in resistModifiers)
            {
                statusModifiers.defencesAndResistances[(int)resist.resistType] += resist.amountChanged;
            }
        }
        if (hazardModifiers != null)
        {
            foreach (HazardModifier hazard in hazardModifiers)
            {
                if (!statusModifiers.hazardProtections.ContainsKey(hazard.hazardType))
                {
                    statusModifiers.hazardProtections.Add(hazard.hazardType, hazard.strength);
                }
                else
                {
                    statusModifiers.hazardProtections[hazard.hazardType] =
                        statusModifiers.hazardProtections[hazard.hazardType] 
                        +hazard.strength;
                }
            }
        }
    }


    public override void recalculate(StatusModifiers statusModifiers)
    {
        if (skillModifiers != null)
        {
            foreach (SkillModifier skill in skillModifiers)
            {
                statusModifiers.skills[(int)skill.skillType] += skill.amountChanged;
            }
        }
        if (resistModifiers != null)
        {
            foreach (DefenceResistModifier resist in resistModifiers)
            {
                statusModifiers.defencesAndResistances[(int)resist.resistType] += resist.amountChanged;
            }
        }
        if (hazardModifiers != null)
        {
            foreach (HazardModifier hazard in hazardModifiers)
            {
                if (statusModifiers.hazardProtections.ContainsKey(hazard.hazardType))
                {
                    statusModifiers.hazardProtections[hazard.hazardType] = hazard.strength;
                }
                else
                {
                    statusModifiers.hazardProtections[hazard.hazardType] =
                        statusModifiers.hazardProtections[hazard.hazardType]
                        + hazard.strength;
                }
            }
        }
    }

    public override void remove(StatusModifiers statusModifiers, ViewInterface view, Actor actor)
    {
        if (view != null)
        {
            view.DrawText(removeDescription.Replace("TARGET", actor.GetName()));
        }
        if (skillModifiers != null)
        {
            foreach (SkillModifier skill in skillModifiers)
            {
                statusModifiers.skills[(int)skill.skillType] -= skill.amountChanged;
            }
        }
        if (resistModifiers != null)
        {
            foreach (DefenceResistModifier resist in resistModifiers)
            {
                statusModifiers.defencesAndResistances[(int)resist.resistType] -= resist.amountChanged;
            }
        }
        if (hazardModifiers != null)
        {
            foreach (HazardModifier hazard in hazardModifiers)
            {
                    statusModifiers.hazardProtections[hazard.hazardType] =
                        statusModifiers.hazardProtections[hazard.hazardType]
                        - hazard.strength;
            
            }
        }
    }
}
