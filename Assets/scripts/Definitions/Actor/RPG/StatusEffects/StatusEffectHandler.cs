﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization;

[Serializable]
public class StatusModifiers
{
    public int[] skills;
    public int[] defencesAndResistances;
    public List<int> statusImmunities;
    public Dictionary<string, float> hazardProtections;
    public StatusModifiers()
    {
        skills = new int[8];
        defencesAndResistances = new int[10];
        statusImmunities = new List<int>();
        hazardProtections = new Dictionary<string, float>();
    }

}

[Serializable]
public class StatusEffectHandler 
{
    bool unStable =true;
    Dictionary<int, StatusEffect> statusEffectsDictionary;
    const int UPDATEINTERVAL = 100;
    int clock;
    StatusModifiers statusModifiers;
    [NonSerialized]
    StatusBind bindState;
    [NonSerialized]
    StatusStealth statusStealth;

    public StatusEffectHandler()
    {
        statusModifiers = new StatusModifiers();
        clock = 0;
        statusEffectsDictionary = new Dictionary<int, StatusEffect>();
    }

    public void Update(int ticks,Actor actor, ViewInterface view)
    {
        if (unStable)
        {
            Stabilize();
        }
        clock += ticks;
        if (clock > UPDATEINTERVAL)
        {
            clock -= UPDATEINTERVAL;
            RunLogic(view,actor);
        }
    }

    private void RunLogic(ViewInterface view, Actor actor)
    {
        foreach(int key in statusEffectsDictionary.Keys)
        {
            statusEffectsDictionary[key].Update();
            if (!statusEffectsDictionary[key].sustain(actor, view))
            {
                RemoveStatusEffect(key,view, actor);
                return;
            }
        }
    }

    public void AddStatusEffect(StatusEffect statusEffect)
    {
        if (statusModifiers.statusImmunities.Contains(statusEffect.GetUid())) {
            return;
        }
        statusEffectsDictionary.Add(statusEffect.GetUid(), statusEffect);
        statusEffect.apply(statusModifiers);

        if (statusEffect is StatusBind)
        {

            bindState = (StatusBind)statusEffect;
        }
        if (statusEffect is StatusStealth)
        {
            statusStealth = (StatusStealth)statusEffect;
        }
    }

    internal void Stabilize()
    {

        foreach (KeyValuePair<int, StatusEffect> item in statusEffectsDictionary)
        {

            if (item.Value is StatusBind)
            {

                bindState = (StatusBind)item.Value;
            }
            if (item.Value is StatusStealth)
            {
                statusStealth = (StatusStealth)item.Value;
            }
        }
        unStable = false;
    }

    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        unStable = true;

    }


    public void RemoveStatusEffect(int id, ViewInterface view, Actor actor)
    {

        statusEffectsDictionary[id].remove(statusModifiers, view, actor);
        statusEffectsDictionary.Remove(id);

        if (bindState!=null && bindState.GetUid() == id)
        {
            bindState = null;
        }
        if (statusStealth!=null && statusStealth.GetUid() == id)
        {
            statusStealth = null;
        }
    }

    internal bool HasStatusEffect(int uid)
    {
        return this.statusEffectsDictionary.ContainsKey(uid);
    }

    public StatusBind GetStatusBind()
    {
        return bindState;
    }

    public bool IsBound()
    {
        return bindState != null;
    }

    public StatusStealth GetStatusStealth()
    {
        return statusStealth;
    }

    public StatusModifiers GetModifiers()
    {
        return statusModifiers;
    }

    public StatusEffect[] GetEffectIcons()
    {
        StatusEffect[] effects = new StatusEffect[4];
        int index = 0;
        foreach (int key in statusEffectsDictionary.Keys)
        {
            if (index < 4)
            {
                effects[index] = statusEffectsDictionary[key];
                index++;
            }
        }
        return effects;
    }
}
