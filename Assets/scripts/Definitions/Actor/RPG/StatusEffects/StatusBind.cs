﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization;

[Serializable]
public class StatusBind : StatusEffect
{
    long originId;
    [NonSerialized]
    NPC origin;
    bool dependent;
    string resistString, applyString;
    int strength;
    SK resistSkill;
    StatusEffect subEffect;

    public StatusBind(int uid, int icon, string resistStr, string breakStr, string applyStr, bool originDependent, int strength, SK resistSkill,StatusEffect subEffect)
    {
        this.uid = uid;
        this.icon = icon;
        this.resistString = resistStr;
        this.removeDescription = breakStr;
        this.applyString = applyStr;
        this.strength = strength;
        this.resistSkill = resistSkill;
        this.dependent = originDependent;
        this.subEffect = subEffect;
    }

    public StatusEffect GetSubEffect()
    {
        return subEffect;
    }

    public SK GetResistSkill()
    {
        return resistSkill;
    }

    public int GetStrength()
    {
        return strength;
    }

    public string GetResistString()
    {
        return resistString;
    }

    public override bool sustain(Actor actor, ViewInterface view)
    {
        if (subEffect != null)
        {
            subEffect.sustain(actor, view);
        }
        if (dependent)
        {
            if (origin == null)
            {
                origin = RetrieveNPC(originId);
                if (origin == null)
                {
                    return false;
                }
            }
            else
            {
                if (!origin.GetAlive())
                {
                    return false;
                }
            }
        }
        return !(duration > 0);
    }
    public override void apply(StatusModifiers statusModifiers)
    {
        if (subEffect != null)
        {
            subEffect.apply(statusModifiers);
        }
    }

    public override void recalculate(StatusModifiers statusModifiers)
    {
        if (subEffect != null)
        {
            subEffect.recalculate(statusModifiers);
        }
    }

    public override void remove(StatusModifiers statusModifiers, ViewInterface view, Actor actor)
    {
        if (view != null)
        {
            view.DrawText(this.removeDescription.Replace("TARGET", actor.GetName()));
        }
        if (subEffect != null)
        {
            subEffect.remove(statusModifiers, view, actor);
        }
    }
    private NPC RetrieveNPC(long originId)
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        for (int i = 0; i < zone.GetContents().GetActors().Count; i++)
        {
            if (zone.GetContents().GetActors()[i]!=null && 
                zone.GetContents().GetActors()[i].GetUID() == originId)
            {
                return (NPC)zone.GetContents().GetActors()[i];
            }
        }
        return null;
    }

    public string GetApplyString()
    {
        return applyString;
    }

    public void SetOrigin(NPC origin)
    {
        this.origin = origin;
        this.originId = origin.GetUID();
    }

    public NPC GetOrigin()
    {
        if (dependent)
        {
            if (origin == null)
            {
                origin = RetrieveNPC(originId);
            }
        }
        return origin;
    }

    public override StatusEffect Clone()
    {
        return new StatusBind(uid, icon, resistString, removeDescription, applyString, dependent, strength, resistSkill,subEffect);
    }

    internal bool IsDependent()
    {
        return dependent;
    }
}
