﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class StatusStealth : StatusEffect
{
    int stealthStrength;
    bool perceptionChecked;

    public StatusStealth()
    {

    }

    public StatusStealth(int uid, int strength)
    {
        this.uid = uid;
        perceptionChecked = false;
        stealthStrength = strength;
    }
    public override bool sustain(Actor actor, ViewInterface view)
    {
        return true;
    }

    public bool StealthCheck(int strength, bool repeatable = true)
    {
        if (!repeatable && perceptionChecked)
        {
            return false;
        }
        if (strength>stealthStrength)
        {

            return true;
        }
        perceptionChecked = true;
        return false;
    }

    public override StatusEffect Clone()
    {
        StatusStealth statusStealth = new StatusStealth();
        statusStealth.uid = uid;
        statusStealth.stealthStrength = stealthStrength;
        statusStealth.perceptionChecked = perceptionChecked;
        return statusStealth;
    }

}
