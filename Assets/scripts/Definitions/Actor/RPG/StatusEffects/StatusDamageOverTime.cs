﻿using System;
using UnityEngine;

[Serializable]
public class StatusDamageOverTime : StatusEffect
{

    ST stat;
    DR damageType;
    int tickChange;
    string tickDescription;
    long originId;
    DR resistance;
    int strength;

    public StatusDamageOverTime(string removeDescription, string tickDescription, int duration, int uid, int icon, 
        ST stat, int tickChange, DR damageType, DR resistance, int strength)
    {
        this.removeDescription = removeDescription;
        this.tickDescription = tickDescription;
        this.duration = duration;
        this.uid = uid;
        this.icon = icon;
        this.stat = stat;
        this.tickChange = tickChange;
        this.damageType = damageType;
        this.resistance = resistance;
        this.strength = strength;
    }

    public void SetOrigin(NPC origin)
    {
        this.originId = origin.GetUID();
    }

    public override bool sustain(Actor actor, ViewInterface view) {
        if (damageType == DR.NONE)
        {
            actor.GetRPG().ReduceStat(stat, tickChange);
        }
        else
        {
            float reduced = tickChange - actor.GetRPG().getDefenceResist(damageType);
            actor.GetRPG().ReduceStat(stat, (int)reduced);
        }
        if (tickChange>0)
        {
            actor.SetSuppression(5);
        }
        Tile t = GlobalGameState.GetInstance().getCurrentZone().GetContents().GetTile(actor.GetPosition().x, actor.GetPosition().y);
        if (t!=null && t.isVisible())
        {
            view.DrawText(tickDescription.Replace("TARGET", actor.GetName()).Replace("VALUE", tickChange.ToString()));
        }
        if (actor.IsPlayer() && (actor.GetRPG().GetStat(ST.HEALTH)<=0 || actor.GetRPG().GetStat(ST.RESOLVE)<=0))
        {
            ZoneActor npc = RetrieveNPC(originId);
            if (npc!=null && npc.GetAlive())
            {
                if (actor.GetRPG().GetStat(ST.HEALTH) <= 0)
                {
                    MonsterVictoryTool.RunVictory(npc, view.GetPlayerInZone(), view);
                }
                else
                {
                    MonsterVictoryTool.RunDominate(npc, view.GetPlayerInZone(), view);
                }
            }
            else
            {
                view.SetScreen(new ScreenDataGameover("You have succumbed"));
            }
        }
        if (resistance != DR.NONE)
        {
            int diceroll = DiceRoller.GetInstance().RollDice(100) + actor.GetRPG().getDefenceResist(this.resistance)-strength;
            if (diceroll > 50)
            {
                duration = 0;
            }
        }
        return (duration > 0);
    }

    public override StatusEffect Clone()
    {
        return new StatusDamageOverTime(this.removeDescription, tickDescription, duration, uid, icon, stat, tickChange, damageType, resistance, strength);
    }

    public override void remove(StatusModifiers statusModifiers, ViewInterface view, Actor actor)
    {
        if (view != null)
        {
            view.DrawText(removeDescription.Replace("TARGET", actor.GetName()));
        }


    }

    private ZoneActor RetrieveNPC(long originId)
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        for (int i = 0; i < zone.GetContents().GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zone.GetContents().GetZoneParameters().GetHeight(); j++)
            {
                Tile t= zone.GetContents().GetTile(i, j);
                if (t !=null && t.GetActorInTile() != null)
                {
                    if (t.GetActorInTile().actor.GetUID().Equals(originId))
                    {
                        return t.GetActorInTile();
                    }
                }
            }
        }
        return null;
    }
}
