﻿using System;
using System.Collections;
using UnityEngine;


[Serializable]
public class StatusImmunity : StatusEffect
{
    int[] immunities;

    public StatusImmunity(string removeDescription, int duration, int uid, int icon, int[] immunities)
    {
        this.immunities= immunities;
        this.duration= duration;
        this.uid= uid;
        this.icon = icon;
        this.removeDescription= removeDescription;
    }


    public override StatusEffect Clone()
    {
        StatusImmunity clone = new StatusImmunity(removeDescription, duration, uid, icon, immunities);
        return clone;
    }

    public override void apply(StatusModifiers statusModifiers)
    {
        for (int i=0;i<this.immunities.Length;i++)
        {
            if (!statusModifiers.statusImmunities.Contains(immunities[i]))
            {
                statusModifiers.statusImmunities.Add(immunities[i]);
            }
        }
    }
    public override void remove(StatusModifiers statusModifiers, ViewInterface view, Actor actor)
    {
        view.DrawText(this.removeDescription.Replace("TARGET", actor.GetName()));
        for (int i = 0; i < this.immunities.Length; i++)
        {
            if (statusModifiers.statusImmunities.Contains(immunities[i]))
            {
                statusModifiers.statusImmunities.Remove(immunities[i]);
            }
        }
    }
}
