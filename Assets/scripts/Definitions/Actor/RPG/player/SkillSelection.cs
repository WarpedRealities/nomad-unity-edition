﻿using UnityEngine;
using System.Collections;
using System;


[Serializable]
public class SkillSelection 
{
    int[] skillSelections = new int [14];

    public SkillSelection()
    {

    }

    public int[] ApplySkills(int[] skills)
    {
        for (int i = 0; i < skills.Length; i++)
        {
            skills[i] += skillSelections[i];
        }
        return skills;
    }

    internal int GetSkill(int i)
    {
        return skillSelections[i];
    }

    internal void SetSkill(int i, int value)
    {
        skillSelections[i] = value;
    }
}
