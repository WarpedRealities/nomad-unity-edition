﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization;
using System.Numerics;

public delegate void SetFloat(float Value);

[Serializable]
public class PlayerRPG : ActorRPG
{
    private StatusEffectHandler statusHandler;
    private SkillSelection skillSelection = new SkillSelection();
    private List<PerkInstance> perks;
    private float[] stats;
    private int[] statsMax;
    private int[] abilities;
    private int[] skills;
    private int[] equipMods;
    private float[] secondaryValues;
    private float[] defencesAndResistances;
    private int level;
    private int experience;
    private float karma;
    private float encumbranceLevel;
    private CombatMove currentMove;
    private Dictionary<String,float> hazardProtection;

    internal SkillSelection GetSkillSelection()
    {
        return skillSelection;
    }
    [NonSerialized]
    private CombatMove[] defaultMoves;
    private List<CombatMove>[] moveList;

    private Inventory inventory;

    private readonly int BASIC_WEIGHT = 10;
    private SetFloat setEncumbrance;

    public PlayerRPG()
    {
        statusHandler = new StatusEffectHandler();
        perks = new List<PerkInstance>();
        moveList = new List<CombatMove>[4];
        for (int i = 0; i < 4; i++)
        {
            moveList[i] = new List<CombatMove>();
        }
        PlayerStatTool playerStatTool = new PlayerStatTool(level,skillSelection,perks);
        float satiation = stats != null ? stats[(int)ST.SATIATION] : -1;
        playerStatTool.BuildRPG(satiation, out stats, out statsMax, out abilities, out skills, out secondaryValues, out defencesAndResistances);
        defaultMoves = PlayerStatTool.BuildDefaultMoves();
        currentMove = defaultMoves[0];
        encumbranceLevel = 1;
        karma = 50;
        experience = 000;
        level = 0;
        equipMods = new int[skills.Length];
        setEncumbrance = SetEncumbrance;
        hazardProtection = new Dictionary<string, float>();
    }

    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        defaultMoves = PlayerStatTool.BuildDefaultMoves();
    }

    internal void CalcStats()
    {
        PlayerStatTool playerStatTool = new PlayerStatTool(level, skillSelection, perks);
        playerStatTool.BuildRPG(stats[(int)ST.SATIATION], out stats, out statsMax, out abilities, out skills, out secondaryValues, out defencesAndResistances);
        
    }

    internal bool HasToken(CraftingToken craftingToken)
    {
        throw new NotImplementedException();
    }

    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;
        UpdateMoves();

    }

    public int GetNextLevel()
    {
        int pl = level + 1;
        return 25*(pl*pl);
    }

    public void UpdateMoves()
    {
        for (int i = 0; i < moveList.Length; i++)
        {
            moveList[i].Clear();
        }

        PlayerMoveListBuilder.Build(defaultMoves, moveList, inventory, currentMove, out currentMove);
        PlayerMoveListBuilder.BuildPerkMoves(moveList, inventory, this);
        currentMove = PlayerMoveListBuilder.FindMove(moveList, currentMove.GetName());
    }

    public List<CombatMove>[] GetMoves()
    {
        return moveList;
    }

    public int GetExperience()
    {
        return experience;
    }
    public bool CanLevel()
    {

        return experience >= GetNextLevel();
    }
    public void ApplyDefenceMod(DefenceModifiers defenceModifiers)
    {
        for (int i = 0; i < defenceModifiers.GetDefences().Length; i++)
        {
            this.defencesAndResistances[i] += defenceModifiers.GetDefences()[i];
        }
    }

    public void ApplySkillMod(SkillModifiers skillModifiers)
    {
        for (int i = 0; i < skillModifiers.GetSkills().Length; i++)
        {
            equipMods[i] += skillModifiers.GetSkills()[i];
        }
    }

    internal List<CombatMove> GetMoveCategory(int moveCat)
    {
        return moveList[moveCat];
    }

    public override StatusEffectHandler GetStatusHandler()
    {
        return statusHandler;
    }

    internal void AddExperience(int exp, ViewInterface view)
    {
        this.experience += exp;

        if (view != null)
        {
            if (CanLevel())
            {
                view.DrawText("You have gained " + exp + " experience and can now level up");
            }
            else
            {
                view.DrawText("You have gained " + exp + " experience");
            }
        }
    }

    public void RemoveDefenceMod(DefenceModifiers defenceModifiers)
    {
        for (int i = 0; i < defenceModifiers.GetDefences().Length; i++)
        {
            this.defencesAndResistances[i] -= defenceModifiers.GetDefences()[i];

        }
    }

    internal void CalcEquipment()
    {
        inventory.SetCapacity(GetCarryWeight());
        equipMods = new int[skills.Length];
        for (int i = 0; i < 7; i++)
        {
            if (inventory.GetSlot(i) != null && inventory.GetSlot(i).GetDef().GetItemType() == ItemType.EQUIP)
            {
                inventory.HandleEquipMods(inventory.GetSlot(i), this);
                /*
                ItemEquippable itemEquippable =(ItemEquippable) inventory.GetSlot(i).GetDef();
                if (itemEquippable.GetSkillModifiers() != null)
                {
                    this.ApplySkillMod(itemEquippable.GetSkillModifiers());
                }
                if (itemEquippable.GetDefenceModifiers() != null)
                {
                    this.ApplyDefenceMod(itemEquippable.GetDefenceModifiers());
                }
                */
            }
        }
    }
    internal void LevelUp()
    {
        experience -= GetNextLevel();
        level++;
        PlayerStatTool playerStatTool = new PlayerStatTool(level, skillSelection, perks);
        float satiation = stats != null ? stats[(int)ST.SATIATION] : -1;
        playerStatTool.BuildRPG(satiation, out stats, out statsMax, out abilities, out skills, out secondaryValues, out defencesAndResistances);
        equipMods = new int[skills.Length];
        for (int i = 0; i < 7; i++)
        {
            if (inventory.GetSlot(i) != null && inventory.GetSlot(i).GetDef().GetItemType()== ItemType.EQUIP)
            {
                inventory.HandleEquipMods(inventory.GetSlot(i), this);
                /*
                ItemEquippable itemEquippable =(ItemEquippable) inventory.GetSlot(i).GetDef();
                if (itemEquippable.GetSkillModifiers() != null)
                {
                    this.ApplySkillMod(itemEquippable.GetSkillModifiers());
                }
                if (itemEquippable.GetDefenceModifiers() != null)
                {
                    this.ApplyDefenceMod(itemEquippable.GetDefenceModifiers());
                }
                */
            }
        }
        inventory.SetCapacity(GetCarryWeight());
        UpdateMoves();
    }

    internal void AddPerk(Perk perk)
    {
        //if we have the perk already, then add a rank
        for (int i = 0; i < perks.Count; i++)
        {
            if (perks[i].GetRef().GetCode().Equals(perk.GetCode()))
            {
                perks[i].SetRank(perks[i].GetRank() + 1);
                return;
            }
        }
        perks.Add(new PerkInstance(perk, 1));
    }

    internal void SetCurrentMove(CombatMove combatMove)
    {
        this.currentMove = combatMove;
    }

    public void RemoveSkillMod(SkillModifiers skillModifiers)
    {
        for (int i = 0; i < skillModifiers.GetSkills().Length; i++)
        {
            equipMods[i] -= skillModifiers.GetSkills()[i];
        }
    }

    public SetFloat GetDelegate()
    {
        return setEncumbrance;
    }

    public void Feed(float amount, bool nonliving)
    {

        float proportionLiving = karma / 100;
        float proportionDead = 1 - (karma / 100);

        float modAmount = nonliving ? proportionDead * amount : proportionLiving * amount;

        if (modAmount <= 0)
        {
            modAmount = 1;
        }
        IncreaseStat(ST.SATIATION, modAmount);
        karma += nonliving ? amount * -0.05F : amount * 0.05F;
        if (karma > 100)
        {
            karma = 100;
        }
        if (karma < 0)
        {
            karma = 0;
        }
    }

    public int GetKarma()
    {
        return (int)karma;
    }

    public void ModKarma(float amount)
    {
        karma += amount;
        if (amount < 0)
            amount = 0;
        if (amount > 100)
            amount = 100;
    }
 

    public override int CombatMoveCount()
    {
        return 1;
    }

    public void SetEncumbrance(float encumbrance)
    {
        this.encumbranceLevel = encumbrance;
    }

    public int getAbility(AB ability)
    {
        return abilities[(int)ability];
    }

    public int getAbility(int ability)
    {
        return abilities[ability];
    }


    public override int GetAbilityMod(AB aB)
    {
        if (AB.NONE == aB)
        {
            return 0;
        }
        return abilities[(int)aB]-5;
    }

    public override CombatMove GetCombatMove(int index)
    {
        return currentMove;
    }

    public List<CombatMove> GetMoveCategory(MoveType moveType)
    {
        return moveList[(int)moveType];
    }

    public CombatMove GetCurrentMove()
    {
        return currentMove;
    }

    public int GetRawDefRes(DR defenceResist)
    {
        return (int)defencesAndResistances[(int)defenceResist];
    }
    public override int getDefenceResist(DR defenceResist)
    {
        if (defenceResist == DR.NONE)
        {
            return 0;
        }
        if (statusHandler.GetModifiers().defencesAndResistances.Length > (int)defenceResist)
        {
            return GetModifiedDefenceResist(defenceResist) + statusHandler.GetModifiers().defencesAndResistances[(int)defenceResist];
        }
        return GetModifiedDefenceResist(defenceResist);
    }

    private int GetModifiedDefenceResist(DR defenceResist)
    {
        if (defenceResist >= DR.POISE)
        {
            int bonus = 0;
            bonus += GetStat(ST.ACTION) > GetStatMax(ST.ACTION) * 0.5F ? GetBonusResist(defenceResist) : 0;
            return (int)defencesAndResistances[(int)defenceResist] + bonus;
        }
        return (int)defencesAndResistances[(int)defenceResist];
    }

    private int GetModifiedDefenceResist(int index)
    {
        if (index >= (int)DR.POISE)
        {
            int bonus = 0;
            bonus += GetStat(ST.ACTION) > GetStatMax(ST.ACTION) * 0.5F ? GetBonusResist(index) : 0;
            return (int)defencesAndResistances[index] + bonus;
        }
        return (int)defencesAndResistances[index];
    }

    public int GetRawDefRes(int index)
    {
        return (int)defencesAndResistances[index];
    }
    public int GetBonusResist(DR defenceResist)
    {

            switch (defenceResist)
            {
                case DR.POISE:
                    return (int)(secondaryValues[(int)SV.POISEBONUS] * defencesAndResistances[(int)defenceResist]);
                case DR.EGO:
                    return (int)(secondaryValues[(int)SV.EGOBONUS] * defencesAndResistances[(int)defenceResist]);
                case DR.METABOLISM:
                    return (int)(secondaryValues[(int)SV.METABOLISMBONUS] * defencesAndResistances[(int)defenceResist]);
            }

        return 0;
    }
    public int GetBonusResist(int index)
    {
        if (index >= (int)DR.POISE)
        {
            switch (index)
            {
                case (int)DR.POISE:
                    return (int)(secondaryValues[(int)SV.POISEBONUS] * defencesAndResistances[index]);
                case (int)DR.EGO:
                    return (int)(secondaryValues[(int)SV.EGOBONUS] * defencesAndResistances[index]);
                case (int)DR.METABOLISM:
                    return (int)(secondaryValues[(int)SV.METABOLISMBONUS] * defencesAndResistances[index]);
            }
        }
        return 0;
    }

    public override int getDefenceResist(int index)
    {
        if (index == (int)DR.NONE)
        {
            return 0;
        }
        if (statusHandler.GetModifiers().defencesAndResistances.Length > index)
        {
            return GetModifiedDefenceResist(index) + statusHandler.GetModifiers().defencesAndResistances[index];
        }
        return GetModifiedDefenceResist(index);
    }

    public int GetBaseSkill(SK skill)
    {
        return skills[(int)skill];
    }
    public int GetBaseSkill(int skill)
    {
        return skills[skill];
    }

    public override int getSkill(SK skill)
    {
        if (statusHandler.GetModifiers().skills.Length > (int)skill)
        {
            return skills[(int)skill] + equipMods[(int)skill]+ statusHandler.GetModifiers().skills[(int)skill];
        }
        return skills[(int)skill]+equipMods[(int)skill];
    }

    public int GetSkill(int index)
    {
        if (statusHandler.GetModifiers().skills.Length > index)
        {
            return skills[index] + equipMods[index]+ statusHandler.GetModifiers().skills[index];
        }
        return skills[index] + equipMods[index];
    }
    public override float getSpeed()
    {

        return secondaryValues[(int)SV.SPEED]/encumbranceLevel;
    }

    public override int GetStat(ST stat)
    {
        return (int)stats[(int)stat];
    }

    public void SetStat(ST st, int amount)
    {
        stats[(int)st] = amount;
    }

    public override int GetStatMax(ST stat)
    {
        return statsMax[(int)stat];
    }

    public override void ReduceStat(ST sT, int value)
    {

        stats[(int)sT] -= value;
        if (stats[(int)sT] < 0)
        {
            stats[(int)sT] = 0;
        }

    }

    public float GetSecondaryValue(SV sV)
    {
        return secondaryValues[(int)sV];
    }

    public List<PerkInstance> GetPerks()
    {
        return perks;
    }

    public void ReduceStat(ST sT, float value)
    {
        stats[(int)sT] -= value;
        if (stats[(int)sT] < 0)
        {
            stats[(int)sT] = 0;
        }
    }

    public override void IncreaseStat(ST sT, float value)
    {
        stats[(int)sT] += value;
        if (stats[(int)sT] > statsMax[(int)sT])
        {
            stats[(int)sT] = statsMax[(int)sT];
        }
    }
    public int GetCarryWeight()
    {
        return BASIC_WEIGHT+(abilities[(int)AB.STRENGTH]*(int)secondaryValues[(int)SV.CARRY]);
    }

    public int GetLevel()
    {
        return level;
    }

    public override void Heal(float proportion)
    {
        float h = proportion * ((float)statsMax[(int)ST.HEALTH]);
        float r = proportion * ((float)statsMax[(int)ST.RESOLVE]);
        if (stats[(int)ST.HEALTH] < h)
        {
            stats[(int)ST.HEALTH] = h;
        }
        if (stats[(int)ST.RESOLVE]< r)
        {
            stats[(int)ST.RESOLVE] = r;
        }
    }

    public bool HasCraftingToken(CraftingToken craftingToken)
    {
        return false;
    }

    public Dictionary<String, float> GetHazardProtection()
    {
        if (hazardProtection == null)
        {
            hazardProtection = new Dictionary<string, float>();
        }
        return hazardProtection;
    }

    public float GetHazardProtection(String hazard)
    {
        if (hazardProtection == null)
        {
            hazardProtection = new Dictionary<string, float>();
        }
        foreach(string key in hazardProtection.Keys)
        {
            Debug.Log("keys" + key);
        }

        float core = hazardProtection.ContainsKey(hazard) ? hazardProtection[hazard] : 0;
        float status = statusHandler.GetModifiers().hazardProtections.ContainsKey(hazard) ?
            statusHandler.GetModifiers().hazardProtections[hazard] :
            0;
        return core + status;
    }

    internal void UpdateHazardProtection()
    {
        if (hazardProtection == null)
        {
            hazardProtection = new Dictionary<string, float>();
        }
        hazardProtection.Clear();
        for (int i = 0; i < 7; i++)
        {
            if (inventory.GetSlot(i) != null)
            {
                if (inventory.GetSlot(i).GetDef() is ItemEquippable) {
                    ItemEquippable itemEquippable = (ItemEquippable)inventory.GetSlot(i).GetDef();

                    if (itemEquippable.GetHazardProtections() != null)
                    {

                        for (int j = 0; j < itemEquippable.GetHazardProtections().Length; j++)
                        {
                            HazardProtection protection = itemEquippable.GetHazardProtections()[j];

                            if (hazardProtection.ContainsKey(protection.hazardType)) {
                                hazardProtection[protection.hazardType] =
                                    hazardProtection[protection.hazardType]
                                    + protection.protectionValue;
                            }
                            else
                            {
                                hazardProtection[protection.hazardType] = protection.protectionValue;
                            }
                        }
                    }
                }
            }
        }
    }

    public override Item GetLoot()
    {
        return null;
    }

}
