﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ReformerEntry : IComparable
{
    public string zoneName;
    public long registryTime;

    public ReformerEntry(string name, long time)
    {
        this.zoneName = name;
        this.registryTime = time;
    }

    public int CompareTo(object obj)
    {
        return (int)(registryTime - ((ReformerEntry)obj).registryTime);
    }
}

[Serializable]
public class ReformationHandler 
{
    List<ReformerEntry> reformerTubes;
    bool noReform;
    public ReformationHandler()
    {
        noReform = false;
        reformerTubes = new List<ReformerEntry>();
    }

    public void RegisterZone(string zoneName, long clock)
    {
        //check for and remove existing entry
        RemoveEntry(zoneName);
        //add an entry at the end
        reformerTubes.Add(new ReformerEntry(zoneName, clock));
        //sort
        reformerTubes.Sort();
    }

    private void RemoveEntry(string zoneName)
    {
        for (int i = 0; i < reformerTubes.Count; i++) {
            if (zoneName.Equals(reformerTubes[i].zoneName))
            {
                reformerTubes.RemoveAt(i);
                break;
            }
        }
    }

    public void Clear()
    {
        reformerTubes.Clear();
    }

    public bool GetNoReform()
    {
        return noReform;
    }

    public void SetNoReform(bool value)
    {
        noReform = value;
    }

    public List<ReformerEntry> GetEntries()
    {
        return reformerTubes;
    }
}
