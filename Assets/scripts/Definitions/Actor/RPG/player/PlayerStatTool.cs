﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using System.Collections.Generic;

public class PlayerStatTool
{
    private SkillSelection skillSelection;
    private List<PerkInstance> perks;
    private int level;
    public PlayerStatTool(int level, SkillSelection skillSelection, List<PerkInstance> perks)
    {
        this.level = level;
        this.skillSelection = skillSelection;
        this.perks = perks;
    }

    internal void BuildRPG(float satiation, out float[] stat, out int[] statMax, out int[] ability, out int[] skill, out float[] secondaryValue, out float[] defencesAndResistance)
    {
        ability = new int[6];
        for (int i = 0; i < ability.Length; i++)
        {
            ability[i] = 5;
        }

        ApplyAbilityPerks(ability);

        statMax = buildStatMax(ability);
        stat = buildStats(satiation, statMax);

        skill = buildSkills(ability);
        secondaryValue = BuildSecondaryValues(ability);
        defencesAndResistance = BuildDefenceAndResistances(ability);//new int[10];

        
     
    }

    private void ApplyAbilityPerks(int[] ability)
    {
        for (int i = 0; i < perks.Count; i++)
        {
            if (perks[i].GetRef().GetEffects() != null)
            {
                for (int j = 0; j < perks[i].GetRef().GetEffects().Count; j++)
                {
                    if (perks[i].GetRef().GetEffects()[j].GetEffectType().Equals(PerkEffectType.ABILITYMOD))
                    {
                        PerkEffectAbilityMod perkEffectAbilityMod = (PerkEffectAbilityMod)perks[i].GetRef().GetEffects()[j];

                        ability[(int)perkEffectAbilityMod.GetAbility()] += perkEffectAbilityMod.GetValue() * perks[i].GetRank();
                    }
                }
            }
        }
    }

    private float[] BuildDefenceAndResistances(int[] ability)
    {
        float[] dr = new float[10];
        dr[(int)DR.KINETIC] = 0;
        dr[(int)DR.THERMAL] = 0;
        dr[(int)DR.SHOCK] = 0;
        dr[(int)DR.TEASE] = 0;
        dr[(int)DR.PHEROMONE] = 0;
        dr[(int)DR.PSI] = 0;
        dr[(int)DR.POISE] = 30+(3*level) + GetAbilityMod(ability[(int)AB.STRENGTH]) * 10; ;
        dr[(int)DR.EGO] = 30 + (3 * level) + GetAbilityMod(ability[(int)AB.INTELLIGENCE]) * 10; ;
        dr[(int)DR.METABOLISM] = 30 + (3 * level)+GetAbilityMod(ability[(int)AB.ENDURANCE])*10;
        dr[(int)DR.SYSTEM] = 999;
        for (int i = 0; i < perks.Count; i++)
        {
            if (perks[i].GetRef().GetEffects() != null)
            {
                for (int j = 0; j < perks[i].GetRef().GetEffects().Count; j++)
                {
                    if (perks[i].GetRef().GetEffects()[j].GetEffectType() == PerkEffectType.DEFENCEMOD)
                    {
                        PerkEffectDefenceMod perkEffect = (PerkEffectDefenceMod)perks[i].GetRef().GetEffects()[j];
                        dr[(int)perkEffect.GetDefenceResist()] += perkEffect.GetAmount() * (perks[i].GetRank());
                    }
                }
            }
        }
        return dr;
    }

    public static CombatMove[] BuildDefaultMoves()
    {
        CombatMove[] moves = new CombatMove[2];
        int index = 0;
        XmlDocument document = FileTools.GetXmlDocument("defaultMoves");
        XmlElement root = (XmlElement)document.FirstChild.NextSibling;
        for (int i = 0; i < root.ChildNodes.Count; i++) {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)root.ChildNodes[i];
                if ("combatMove".Equals(e.Name))
                {
                    moves[index] = CombatMoveLoader.BuildCombatMove(e);
                    index++;
                }
            }
        }

        return moves;
    }

    private float[] BuildSecondaryValues(int[] ability)
    {
        float []secondaryValues = new float[9];

        secondaryValues[(int)SV.SPEED] = 0 + (((float)(ability[(int)AB.AGILITY])) * 0.2F);
        secondaryValues[(int)SV.CARRY] = 8;
        secondaryValues[(int)SV.DECAY] = 0.125F;
        secondaryValues[(int)SV.MOVECOST] = 0;
        secondaryValues[(int)SV.REGENRATE] = 1.0F;
        secondaryValues[(int)SV.REGENTHRESHOLD] = 0.5F;
        secondaryValues[(int)SV.POISEBONUS] = 0.5F;
        secondaryValues[(int)SV.EGOBONUS] = 0.5F;
        secondaryValues[(int)SV.METABOLISMBONUS] = 0.5F;
        for (int i = 0; i < perks.Count; i++)
        {
            if (perks[i].GetRef().GetEffects() != null)
            {
                for (int j = 0; j < perks[i].GetRef().GetEffects().Count; j++)
                {
                    if (perks[i].GetRef().GetEffects()[j].GetEffectType() == PerkEffectType.SECONDARYMOD)
                    {
                        PerkEffectSecondaryMod perkEffect = (PerkEffectSecondaryMod)perks[i].GetRef().GetEffects()[j];
                        secondaryValues[(int)perkEffect.GetSecondaryValue()] += perkEffect.GetAmount() * (perks[i].GetRank());
                    }
                }
            }
        }
        return secondaryValues;
    }
    
    public static int GetSkillCap(int level, ActorRPG rpg, SK skill)
    {
        switch (skill)
        {
            case SK.MELEE:
                return (level / 2) + (rpg.GetAbilityMod(AB.STRENGTH)*1) + 1;
            case SK.RANGED:
                return (level / 2) + (rpg.GetAbilityMod(AB.DEXTERITY) *1) + 1;
            case SK.SEDUCTION:
                return (level / 2) + (rpg.GetAbilityMod(AB.CHARISMA) * 1) + 1;
            case SK.DODGE:
                return (level / 2) + (rpg.GetAbilityMod(AB.AGILITY) * 1) + 1;
            case SK.PARRY:
                return (level / 2) + (rpg.GetAbilityMod(AB.AGILITY) * 1) + 1;
            case SK.WILLPOWER:
                return (level / 2) + (rpg.GetAbilityMod(AB.INTELLIGENCE) * 1) + 1;
            case SK.STRUGGLE:
                return (level / 2) + (rpg.GetAbilityMod(AB.STRENGTH) * 1) + 1;
            case SK.PLEASURE:
                return (level / 2) + (rpg.GetAbilityMod(AB.DEXTERITY) * 1) + 1;
            case SK.PERSUADE:
                return (level / 2) + (rpg.GetAbilityMod(AB.CHARISMA) * 1) + 1;
            case SK.TECH:
                return (level / 2) + (rpg.GetAbilityMod(AB.INTELLIGENCE) * 1) + 1;
            case SK.SCIENCE:
                return (level / 2) + (rpg.GetAbilityMod(AB.INTELLIGENCE) * 1) + 1;
            case SK.PERCEPTION:
                return (level / 2) + (rpg.GetAbilityMod(AB.INTELLIGENCE) * 1) + 1;
        }
        return (level/2)+1;
    }

    private int[] buildSkills(int[] ability)
    {
        int[] skills = new int[14];
        skills[(int)SK.MELEE] = GetAbilityMod(ability[(int)AB.STRENGTH]);
        skills[(int)SK.RANGED] = GetAbilityMod(ability[(int)AB.DEXTERITY]);

        skills[(int)SK.SEDUCTION] = GetAbilityMod(ability[(int)AB.CHARISMA]);
        skills[(int)SK.DODGE] = GetAbilityMod(ability[(int)AB.AGILITY]);
        skills[(int)SK.PARRY] = GetAbilityMod(ability[(int)AB.AGILITY]);
        skills[(int)SK.WILLPOWER] = GetAbilityMod(ability[(int)AB.INTELLIGENCE]);
        skills[(int)SK.STRUGGLE] = GetAbilityMod(ability[(int)AB.STRENGTH]);
        skills[(int)SK.PLEASURE] = GetAbilityMod(ability[(int)AB.DEXTERITY]);
        skills[(int)SK.PERSUADE] = GetAbilityMod(ability[(int)AB.CHARISMA]);
        skills[(int)SK.TECH] = GetAbilityMod(ability[(int)AB.INTELLIGENCE]);
        skills[(int)SK.SCIENCE] = GetAbilityMod(ability[(int)AB.INTELLIGENCE]);
        skills[(int)SK.PERCEPTION] = GetAbilityMod(ability[(int)AB.INTELLIGENCE]);

        skills = skillSelection.ApplySkills(skills);

        for (int i = 0; i < perks.Count; i++)
        {
            if (perks[i].GetRef().GetEffects() != null)
            {
                for (int j = 0; j < perks[i].GetRef().GetEffects().Count; j++)
                {
                    if (perks[i].GetRef().GetEffects()[j].GetEffectType() == PerkEffectType.SKILLMOD)
                    {
                        PerkEffectSkillMod perkEffectSkillMod = (PerkEffectSkillMod)perks[i].GetRef().GetEffects()[j];
                        skills[(int)perkEffectSkillMod.GetSkill()] += perkEffectSkillMod.GetModValue();
                    }
                }
            }
        }

        return skills;
    }

    private int GetAbilityMod(int abilityScore)
    {
        return abilityScore - 5;
    }

    private float[] buildStats(float satiation, int[] statMax)
    {
        float[] stats = new float[4];

        stats[(int)ST.HEALTH] = statMax[(int)ST.HEALTH];
        stats[(int)ST.RESOLVE] = statMax[(int)ST.RESOLVE];
        stats[(int)ST.ACTION] = statMax[(int)ST.ACTION];
        stats[(int)ST.SATIATION] = satiation == -1 ? statMax[(int)ST.SATIATION] : satiation;

        return stats;
    }

    private int[] buildStatMax(int [] abilities)
    {
        int[] statMax = new int[4];
        //calculate health
        statMax[(int)ST.HEALTH] = 5 + (abilities[(int)AB.ENDURANCE] * 5) + (level * (abilities[(int)AB.ENDURANCE] - 3));

        //calculate resolve
        statMax[(int)ST.RESOLVE] = 5 + (abilities[(int)AB.INTELLIGENCE] * 5) + (level * (abilities[(int)AB.INTELLIGENCE] - 3));

        //calculate satiation
        statMax[(int)ST.SATIATION] = 50 + (abilities[(int)AB.ENDURANCE] * 20) + (2*level * (abilities[(int)AB.ENDURANCE] - 3));

        //calculate action
        statMax[(int)ST.ACTION] = 30 + (level * 2);
        for (int i = 0; i < perks.Count; i++)
        {
            if (perks[i].GetRef().GetEffects() != null)
            {
                for (int j = 0; j < perks[i].GetRef().GetEffects().Count; j++)
                {
                    if (perks[i].GetRef().GetEffects()[j].GetEffectType() == PerkEffectType.STATMOD)
                    {
                        PerkEffectStatMod perkEffectStatMod = (PerkEffectStatMod)perks[i].GetRef().GetEffects()[j];
                        statMax[(int)perkEffectStatMod.GetStat()] += perkEffectStatMod.GetAmount() * (perks[i].GetRank());
                    }
                }
            }
        }
        return statMax;
    }
}
