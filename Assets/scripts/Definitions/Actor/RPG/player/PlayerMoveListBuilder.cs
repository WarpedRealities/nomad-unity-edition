﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PlayerMoveListBuilder
{
    public static void Build(CombatMove [] defaultMoves, List<CombatMove>[] moveList, Inventory inventory, CombatMove currentMove, out CombatMove newCurrentMove)
    {
        //establish the basic combat move
        CombatMove basicMove=defaultMoves[0];
        if (inventory.GetSlot(0)!=null && ((ItemEquippable)inventory.GetSlot(0).GetDef()).GetMoves() != null)
        {
            EquipmentMoves moves = ((ItemEquippable)inventory.GetSlot(0).GetDef()).GetMoves();
            basicMove = moves.GetMoves()[0];

        }

        moveList[0].Add(basicMove);
        moveList[1].Add(defaultMoves[1]);
        for (int j = 0; j < 4; j++)
        {
            if (inventory.GetSlot(j) != null && ((ItemEquippable)inventory.GetSlot(j).GetDef()).GetMoves() != null)
            {
                EquipmentMoves moves = ((ItemEquippable)inventory.GetSlot(j).GetDef()).GetMoves();
                for (int i = 0==j? 1: 0; i < moves.GetMoveCount(); i++)
                {
                    moveList[(int)moves.GetMoves()[i].GetMoveType()].Add(moves.GetMoves()[i]);
                }

            }
        }
        newCurrentMove = moveList[0][0];
    }

    internal static void BuildPerkMoves(List<CombatMove>[] moveList, Inventory inventory, PlayerRPG playerRPG)
    {
        List<string> modifiedMoves = new List<string>();
        //add moves
        foreach (PerkInstance item in playerRPG.GetPerks())
        {
            if (item.GetRef().GetEffects() != null)
            {
                foreach (PerkEffect effect in item.GetRef().GetEffects())
                {
                    if (effect.GetEffectType() == PerkEffectType.MOVE)
                    {
                        AddMove((PerkEffectCombatMove)effect, modifiedMoves, moveList, item.GetRank());
                    }
                }
            }
        }
        //add effects
        foreach (PerkInstance item in playerRPG.GetPerks())
        {
            if (item.GetRef().GetEffects() != null)
            {
                foreach (PerkEffect effect in item.GetRef().GetEffects())
                {
                    if (effect.GetEffectType() == PerkEffectType.MOVEADD)
                    {
                        AddEffect((PerkEffectMoveAdd)effect, modifiedMoves, moveList, item.GetRank());
                    }
                }
            }
        }
        //modify moves
        foreach (PerkInstance item in playerRPG.GetPerks())
        {
            if (item.GetRef().GetEffects() != null)
            {
                foreach (PerkEffect effect in item.GetRef().GetEffects())
                {
                    if (effect.GetEffectType() == PerkEffectType.MOVEMOD)
                    {
                        ModifyMoves((PerkEffectMoveMod)effect, modifiedMoves, moveList, item.GetRank());
                    }
                }
            }
        }
    }

    private static void AddEffect(PerkEffectMoveAdd effect, List<string> modifiedMoves, List<CombatMove>[] moveList, int rank)
    {
        CombatMove move = FindMove(effect.GetMoveName(), moveList);
        if (move != null)
        {
            if (!modifiedMoves.Contains(effect.GetName()))
            {
                CombatMove m = move;
                move = move.Clone();
                modifiedMoves.Add(effect.GetName());
                ReplaceMove(m, moveList, move);
            }
            move.getEffects().Add(effect.GetEffect().Clone());
        }
    }

    private static void AddMove(PerkEffectCombatMove effect, List<string> modifiedMoves, List<CombatMove>[] moveList, int rank)
    {
        int category = (int)effect.GetMove().GetMoveType();
        moveList[category].Add(effect.GetMove());
    }

    private static void ModifyMoves(PerkEffectMoveMod effect, List<string> modifiedMoves, List<CombatMove>[] moveList, int rank)
    {
        CombatMove move = FindMove(effect.GetName(), moveList);
        if (move == null)
        {
            return;
        }
        
        if (!modifiedMoves.Contains(effect.GetName()))
        {
            CombatMove m = move;
            move = move.Clone();
            modifiedMoves.Add(effect.GetName());
            ReplaceMove(m, moveList,move);
        }

        if ((int)effect.GetAttackPattern() > (int)move.GetPattern())
        {
            move.SetPattern(effect.GetAttackPattern());
        }
        if ((int)effect.GetInterruptType() > (int)move.GetInterruptType())
        {
            move.SetInterruptType(effect.GetInterruptType());
        }

        for (int i = 0; i < move.getEffects().Count; i++)
        {
            switch (move.getEffects()[i].GetEffectType())
            {
                case EffectType.DAMAGE:
                    EffectDamage d = (EffectDamage)move.getEffects()[i];
                    if (d.GetDamagetype() == effect.GetDamageType() && (effect.GetMin()!=0 || effect.GetMax()!=0))
                    {
                        d.SetMax((effect.GetMax()*rank) + d.GetMax());
                        d.SetMin((effect.GetMin()*rank) + d.GetMin());
                    }
                    if (effect.GetDecayMod() != 1)
                    {
                        d.ModDecay(effect.GetDecayMod());
                    }
                    break;
                case EffectType.STATUS:

                    break;
            }
        }
    }

    private static void ReplaceMove(CombatMove move, List<CombatMove>[] moveList, CombatMove replacement)
    {
        for (int i = 0; i < moveList.Length; i++)
        {
            int index = moveList[i].IndexOf(move);
            if (index != -1)
            {
                moveList[i][index] = replacement;
                return;
            }
        }
    }

    private static CombatMove FindMove(string name, List<CombatMove>[] moveList)
    {
        for (int i = 0; i<moveList.Length; i++)
        {
            for (int j = 0; j < moveList[i].Count; j++)
            {
                if (moveList[i][j].GetName().Equals(name))
                {
                    return moveList[i][j];
                }
            }
        }
        return null;
    }

    internal static CombatMove FindMove(List<CombatMove>[] moveList, string name)
    {
        for (int i = 0; i < moveList.Length; i++)
        {
            for (int j = 0; j < moveList[i].Count; j++)
            {
                if (moveList[i][j].GetName().Equals(name))
                {
                    return moveList[i][j];
                }
            }
        }
        return null;
    }
}
