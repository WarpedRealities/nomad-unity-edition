using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;
using UnityEngine;

public class NPC_Loot
{
    LootSubTableReference lootSubTable;
    int probability;
    String lootList;
    public NPC_Loot( string lootList, int probability)
    {
        this.probability = probability;
        this.lootList = lootList;
    }

    internal Item GetItem()
    {
        if (lootSubTable == null)
        {
            lootSubTable = LootSubTableReferenceLibrary.GetInstance().GetSubtTable(lootList);
        }
        return GenerateItem(lootSubTable);
    }

    public Item GenerateItem(LootSubTableReference tableReference)
    {

        SubTableEntry entry = tableReference.GetEntry();

        int number = entry.GetCount();
        ItemDef item = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(entry.GetItemCode());
        if (number == 1)
        {
            return (LootItemGenerator.BuildItem(entry.GetItemCode(), entry.GetAddendum()));
        }
        else
        {

            if (item.IsStackable())
            {
                if (item.GetItemType() == ItemType.AMMO)
                {
                    return (new ItemAmmoStack((ItemAmmo)item,
                        number,
                        GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(entry.GetAddendum())));
                }
                else
                {
                    return (new ItemStack(item, number));
                }
            }
            else
            {
                for (int i = 0; i < number; i++)
                {
                    return (LootItemGenerator.BuildItem(entry.GetItemCode(), entry.GetAddendum()));
                }
            }
        }
        return null;
    }

    internal int getProbability()
    {
        return probability;
    }
}