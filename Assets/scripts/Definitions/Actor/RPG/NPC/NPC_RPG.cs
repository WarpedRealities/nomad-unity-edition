﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public class NPC_RPG : ActorRPG
{
    [NonSerialized]
    private NPC_StatBlock statBlock;

    private StatusEffectHandler statusHandler;
    private string filename;
    private int [] stats;
    private int[] skillModifiers;
    private int[] defenceResistanceModifiers;
    private bool rewardGiven;
    private int rank;
    public NPC_RPG(string filename, NPC_StatBlock statblock)
    {
        statusHandler = new StatusEffectHandler();
        this.filename = filename;
        statBlock = statblock;
        stats = new int[2];
        stats[0] = statblock.getStatMax(ST.HEALTH);
        stats[1] = statblock.getStatMax(ST.RESOLVE);
        skillModifiers = new int[9];
        defenceResistanceModifiers = new int[10];
    }

    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        statBlock = StatBlockLibrary.GetInstance().getStatBlock(filename);
    }

    public NPC_RPG(NPC_RPG actorRPG)
    {
        statusHandler = new StatusEffectHandler();
        this.statBlock = actorRPG.statBlock;
        this.filename = actorRPG.filename;
        stats = new int[2];
        stats[0] = statBlock.getStatMax(ST.HEALTH);
        stats[1] = statBlock.getStatMax(ST.RESOLVE);
        skillModifiers = new int[6];
        defenceResistanceModifiers = new int[10];
    }

    private void setupStats()
    {
        int []stats = new int[2];
        stats[0] = statBlock.getStatMax(ST.HEALTH);
        stats[1] = statBlock.getStatMax(ST.RESOLVE);


    }

    public int GetRank()
    {
        return rank;
    }

    public void SetRank(int rank)
    {
        this.rank = rank;
    }

    public string GetSprite()
    {

        return statBlock.GetSprite();
    }

    public override int getDefenceResist(DR defenceResist)
    {
        if (statusHandler.GetModifiers().defencesAndResistances.Length > (int)defenceResist)
        {
            return statBlock.getDefenceResist(defenceResist,rank) + defenceResistanceModifiers[(int)defenceResist] +
                statusHandler.GetModifiers().defencesAndResistances[(int)defenceResist];
        }
        return statBlock.getDefenceResist(defenceResist,rank) + defenceResistanceModifiers[(int)defenceResist];
    }

    public override int getDefenceResist(int index)
    {
        if (statusHandler.GetModifiers().defencesAndResistances.Length > index)
        {
            return statBlock.getDefenceResist(index, rank) + defenceResistanceModifiers[index] +
                statusHandler.GetModifiers().defencesAndResistances[index];
        }
        return statBlock.getDefenceResist(index, rank) + defenceResistanceModifiers[index];
    }
    internal string GetRankDescription()
    {
        return statBlock.GetRankDef(rank - 1).GetDescription();
    }

    public override int getSkill(SK skill)
    {
        if (statusHandler.GetModifiers().skills.Length > (int)skill)
        {
            return skillModifiers[(int)skill] + statBlock.getSkill(skill,rank) + 
                statusHandler.GetModifiers().skills[(int)skill];
        }
        return skillModifiers[(int)skill] + statBlock.getSkill(skill, rank);
    }

    public override float getSpeed()
    {
        return statBlock.GetSpeed();
    }

    public override int GetStat(ST stat)
    {

        return stats[(int)stat];
    }

    public override CombatMove GetCombatMove(int index)
    {
        return statBlock.GetCombatMove(index);
    }

    public override int GetAbilityMod(AB aB)
    {
        return 0;
    }

    internal NPC_StatBlock GetStatBlock()
    {
        return statBlock;
    }

    public override void ReduceStat(ST sT, int value)
    {
        if ((int)sT >= stats.Length)
        {
            return;
        }
        stats[(int)sT] -= value;
        if (stats[(int)sT] < 0)
        {
            stats[(int)sT] = 0;
        }
    }

    public override int CombatMoveCount()
    {
        
        return statBlock.CombatMoveCount();
    }

    public override void IncreaseStat(ST sT, float value)
    {
        stats[(int)sT] += (int)value;
        if (stats[(int)sT] > statBlock.getStatMax(sT))
        {
            stats[(int)sT] = statBlock.getStatMax(sT);
        }
    }

    public override int GetStatMax(ST stat)
    {
        return statBlock.getStatMax(stat);
    }

    public override void Heal(float proportion)
    {
        float h = proportion * ((float)statBlock.getStatMax(ST.HEALTH));
        float r = proportion * ((float)statBlock.getStatMax(ST.RESOLVE));
        if (stats[(int)ST.HEALTH] < h)
        {
            stats[(int)ST.HEALTH] = (int)h;
        }
        if (stats[(int)ST.RESOLVE] < r)
        {
            stats[(int)ST.RESOLVE] = (int)r;
        }
    }

    public string GetConversationFile(int index)
    {
        if (statBlock.GetConversationFiles() == null)
        {
            return null;
        }
        return statBlock.GetConversationFiles()[index];
    }

    public void AddExp(Actor actor, ViewInterface view)
    {
        PlayerRPG playerRPG = (PlayerRPG)actor.GetRPG();
        if (!rewardGiven)
        {
            rewardGiven = true;
            int exp = statBlock.GetExperience();
            view.DrawNumber(actor.GetPosition(), exp, 2);
            playerRPG.AddExperience(exp, view);
        }
    }

    public void ResetReward()
    {
        rewardGiven = false;
    }

    public override StatusEffectHandler GetStatusHandler()
    {
        return statusHandler;
    }

    public StatusEffect GetStartStatus()
    {
        return statBlock.GetStartStatus();
    }

    internal bool IsImmobile()
    {
        return statBlock.IsImmobile();
    }

    public override Item GetLoot()
    {
        return statBlock.GetLoot();
    }
}
