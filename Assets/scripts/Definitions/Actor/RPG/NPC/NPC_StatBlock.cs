﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class NPC_StatBlock
{
    private string sprite;

    private int[] statsMax;
    private int[] skills;
    private int[] defenceAndResistance;
    private int experienceValue;
    private CombatMove[] moves;
    private StatusEffect startEffect;
    private float speed;
    private string[] conversationFiles;
    private Rank[] ranks;
    private bool immobile;
    private NPC_Loot loot;
    public NPC_StatBlock()
    {

    }
    public NPC_StatBlock(XmlElement root)
    {
        new StatBlockTool().ReadElement(root, out statsMax, out skills, out defenceAndResistance, out experienceValue, out moves, out speed, out ranks, out startEffect, out immobile, out loot);
    }

    public string [] GetConversationFiles()
    {
        return conversationFiles;
    }

    public void SetConversationFiles(string [] files)
    {
        conversationFiles = files;
    }

    public string GetSprite()
    {
        return sprite;
    }


    public void setSprite(string sprite)
    {
        this.sprite = sprite;
    }

    public int getStatMax(ST stat, int rank = 0)
    {
        return statsMax[(int)stat];
    }

    public int getSkill(SK skill, int rank=0)
    {
        if (rank > 0)
        {
            return skills[(int)skill] + ranks[rank - 1].GetSkill(skill);
        }
        return skills[(int)skill];
    }

    public int getDefenceResist(DR defenceResist, int rank=0)
    {
        if (rank > 0)
        {
            return defenceAndResistance[(int)defenceResist]+ranks[rank-1].GetDefenceResist(defenceResist);
        }
        return defenceAndResistance[(int)defenceResist];
    }

    internal int getDefenceResist(int defenceResist, int rank)
    {
        if (rank > 0)
        {
            return defenceAndResistance[defenceResist] + ranks[rank - 1].GetDefenceResist(defenceResist);
        }
        return defenceAndResistance[defenceResist];
    }
    public float GetSpeed()
    {
        return speed;
    }

    public CombatMove GetCombatMove(int index)
    {
        return moves[index];
    }

    internal int CombatMoveCount()
    {
        if (moves == null)
        {
            return 0;
        }
        return moves.Length;
    }

    internal int GetExperience()
    {
        return experienceValue;
    }

    internal Rank GetRankDef(int index)
    {
        return ranks[index];
    }

    internal StatusEffect GetStartStatus()
    {
        return startEffect;
    }

    internal bool IsImmobile()
    {
        return immobile;
    }

    internal Item GetLoot()
    {
        if (loot != null)
        {
            int diceroll = DiceRoller.GetInstance().RollDice(100);
            if (diceroll < loot.getProbability())
            {
                return loot.GetItem();
            }
        }
        return null;
    }
}
