﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml;
using UnityEngine;

public class StatBlockLibrary
{
    private static StatBlockLibrary instance;

    public static StatBlockLibrary GetInstance()
    {
        if (instance == null)
        {
            instance = new StatBlockLibrary();
        }
        return instance;
    }

    private Dictionary<string, NPC_StatBlock> statBlocks;

    public StatBlockLibrary()
    {
        statBlocks = new Dictionary<string, NPC_StatBlock>();
    }

    public NPC_StatBlock getStatBlock(string filename)
    {
        NPC_StatBlock block=null;
        statBlocks.TryGetValue(filename, out block);
        if (block == null)
        {
            //need to load it
            block = BuildBlock(filename);
            statBlocks.Add(filename, block);
        }
        return block;
    }

    private NPC_StatBlock BuildBlock(string filename)
    {
        string sprite ="";
        NPC_StatBlock statBlock = null;
        XmlDocument doc = FileTools.GetXmlDocument("npcs/" + filename);
        XmlElement root = (XmlElement)doc.FirstChild.NextSibling;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("sprite".Equals(element.Name))
                {
                    sprite = element.GetAttribute("value");
                }
                if ("rpg".Equals(element.Name))
                {
                    statBlock = new NPC_StatBlock(element);
                    statBlock.setSprite(sprite);
                }
                if ("conversations".Equals(element.Name))
                {
                    string[] conversations = new string[6];
                    for (int j = 0; j < element.ChildNodes.Count; j++)
                    {
                        if (element.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement xml = (XmlElement)element.ChildNodes[j];
                            conversations[NPC_Loader.ConversationFromName(xml.Name)] = xml.GetAttribute("value");
                        }
                    }

                    statBlock.SetConversationFiles(conversations);
                }
            }
        }
        return statBlock;
    }

    public void addStatBlock(string filename, NPC_StatBlock statblock)
    {
        statBlocks.Add(filename, statblock);
    }
}
