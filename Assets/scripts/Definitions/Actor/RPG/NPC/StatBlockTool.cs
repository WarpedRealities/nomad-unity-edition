﻿using System.Xml;
using UnityEngine;

public class StatBlockTool
{

    public void ReadElement(XmlElement root, out int[] statsMax, out int[] skills, out int[] defenceAndResistance, 
        out int experience, out CombatMove[] moves, out float speed, out Rank[] ranks, out StatusEffect startStatus,
        out bool immobile, out NPC_Loot loot)
    {
        statsMax = new int[2];
        skills = new int[9];
        defenceAndResistance = new int[10];
        experience = 0;
        moves = null;
        speed = 1;
        ranks = null;
        immobile = false;
        startStatus = null;
        loot = null;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];

                if ("stat".Equals(element.Name))
                {
                    statsMax[(int)EnumTools.strToStat(element.GetAttribute("stat"))] = int.Parse(element.GetAttribute("value"));
                }
                if ("skill".Equals(element.Name))
                {
                    skills[(int)EnumTools.strToSkill(element.GetAttribute("skill"))] = int.Parse(element.GetAttribute("value"));
                }
                if ("defence".Equals(element.Name) || "resist".Equals(element.Name))
                {
                    defenceAndResistance[(int)EnumTools.strToDefenceResist(element.GetAttribute("type"))] = int.Parse(element.GetAttribute("value"));
                }
                if ("experienceValue".Equals(element.Name))
                {
                    experience = int.Parse(element.GetAttribute("value"));
                }
                if ("moves".Equals(element.Name))
                {
                    moves = BuildMoveList(element);
                }
                if ("speed".Equals(element.Name))
                {
                    speed = float.Parse(element.GetAttribute("value"));
                }
                if ("ranks".Equals(element.Name))
                {
                    ranks = BuildRanks(element);                    
                }
                if ("startStatus".Equals(element.Name))
                {
                    startStatus = CombatMoveLoader.BuildStatus((XmlElement)element.ChildNodes[0]);
                }
                if ("immobile".Equals(element.Name))
                {
                    immobile = true;
                }
                if ("loot".Equals(element.Name))
                {
                    loot = new NPC_Loot(element.GetAttribute("lootList"), int.Parse(element.GetAttribute("probability")));
                }
            }
        }

    }

    private Rank[] BuildRanks(XmlElement root)
    {
        Rank[] ranks = new Rank[int.Parse(root.GetAttribute("count"))];
        int index = 0;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("rank".Equals(element.Name))
                {
                    ranks[index] = BuildRank(element);
                    index++;
                }
            }
        }
        return ranks;
    }

    private Rank BuildRank(XmlElement root)
    {
        int[] skills = new int[6];
        int[] defenceAndResistance = new int [10];
        int[] statsMax = new int[2];
        string description = "";
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("stat".Equals(element.Name))
                {
                    statsMax[(int)EnumTools.strToStat(element.GetAttribute("stat"))] = int.Parse(element.GetAttribute("value"));
                }
                if ("skill".Equals(element.Name))
                {
                    skills[(int)EnumTools.strToSkill(element.GetAttribute("skill"))] = int.Parse(element.GetAttribute("value"));
                }
                if ("defence".Equals(element.Name) || "resist".Equals(element.Name))
                {
                    defenceAndResistance[(int)EnumTools.strToDefenceResist(element.GetAttribute("type"))] = int.Parse(element.GetAttribute("value"));
                }
                if ("description".Equals(element.Name))
                {
                    description = element.InnerText;
                }
            }
        }
        return new Rank(skills, defenceAndResistance, statsMax, description);
    }

    private CombatMove[] BuildMoveList(XmlElement root)
    {

        int index = 0;
        int count = int.Parse(root.GetAttribute("count"));
        CombatMove[] moves = new CombatMove[count];
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("combatMove".Equals(element.Name))
                {
                    moves[index] = CombatMoveLoader.BuildCombatMove(element);
                    index++;
                }
            }
        }
        return moves;
    }
}