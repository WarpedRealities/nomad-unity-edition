﻿using UnityEngine;
using System.Collections;
using System;

public class Rank
{
    int[] skills;
    int[] defenceResist;
    int[] statMax;
    string description;

    public Rank(int[] skills, int[] defenceResist, int[] statMax, string description)
    {
        this.skills = skills;
        this.defenceResist = defenceResist;
        this.statMax = statMax;
        this.description = description;
    }

    public int GetSkill(SK skill)
    {
        return this.skills[(int)skill];
    }

    public int GetDefenceResist(DR defenceResist)
    {
        return this.defenceResist[(int)defenceResist];
    }

    internal int GetDefenceResist(int index)
    {
        return this.defenceResist[index];
    }

    public int GetStatMax(ST stat)
    {
        return statMax[(int)stat];
    }

    public string GetDescription()
    {
        return description;
    }

}
