﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
abstract public class ActorRPG 
{

   public abstract int GetStat(ST stat);
    public abstract int GetStatMax(ST stat);
  

    public abstract int getSkill(SK skill);
   public abstract int getDefenceResist(DR defenceResist);

    public abstract int getDefenceResist(int defenceResist);
    public abstract float getSpeed();

    public abstract CombatMove GetCombatMove(int index);

    public abstract int GetAbilityMod(AB aB);
    public abstract void ReduceStat(ST sT, int value);

    public abstract void IncreaseStat(ST sT, float value);
    public abstract int CombatMoveCount();

    public abstract void Heal(float proportion);

    public abstract Item GetLoot();
    public abstract StatusEffectHandler GetStatusHandler();

    //dont need this, only player has it, and the values are mostly internal...except for speed
    //public abstract float getSecondaryValue(SV secondaryValue);
}
