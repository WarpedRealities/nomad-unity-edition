﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class ObserverVoreStage
{

    string [] texts;
    String endScript;

    public ObserverVoreStage(string[] texts, string script)
    {
        this.texts = texts;
        this.endScript = script;
    }

    internal void Progress(int stageProgress, ZoneActor actor, ZoneActor target)
    {
        TextLog.Log(FormatText(texts[stageProgress], actor.actor.GetName(), target.actor.GetName()));

    }

    public string GetEndScript()
    {
        return endScript;
    }

    private string FormatText(string baseText, string origin, string target)
    {
        return baseText.Replace("TARGET", target).Replace("ORIGIN", origin);
    }

    internal bool Complete(int stageProgression)
    {
        return stageProgression >= texts.Length;
    }

}
