﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ObserverVoreOption
{
    String targetName;
    String script;
    int flagField;
    String flagName;
    int value;
    int comparison;
}
