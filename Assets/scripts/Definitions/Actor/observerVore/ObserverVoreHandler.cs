using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ObserverVoreHandler
{
    long targetUID;
    [NonSerialized]
    ZoneActor target;
    ObserverVoreStage[] stages;
    int stageIndex, stageProgression, clock;


    public ObserverVoreHandler(ObserverVoreStage [] stages)
    {
        stageIndex = 0;
        stageProgression = 0;
        this.stages = stages;
    }

    public ZoneActor GetTarget()
    {
        return target;
    }

    public void SetTarget(ZoneActor target)
    {
        this.target = target;
        ((NPC)this.target.actor).SetIsVictim(true);
        this.targetUID = target.actor.GetUID();
    }

    public bool blockAction()
    {
        return stageIndex == 0;
    }

    public bool Update(int time, ZoneActor actor, ViewInterface view)
    {
        if (target == null)
        {
            FindTarget();
        }
        clock += time;
        if (clock > 10)
        {
            clock = 0;
            if (Progress(actor,view))
            {
                return true;
            }
        }
        return false;
    }

    public void Remove()
    {
        if (this.target != null)
        {
            ((NPC)this.target.actor).SetIsVictim(false);
        }
    }

    private void FindTarget()
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        for (int i = 0; i < zone.GetContents().GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zone.GetContents().GetZoneParameters().GetHeight(); j++)
            {
                if (zone.GetContents().GetTile(i,j)!=null &&
                    zone.GetContents().GetTile(i, j).GetActorInTile() != null)
                {
                    if (zone.GetContents().GetTile(i, j).GetActorInTile().actor.GetUID() == targetUID)
                    {
                        target = zone.GetContents().GetTile(i, j).GetActorInTile();
                    }
                }
            }
        }

    }

    private bool Progress(ZoneActor actor,ViewInterface view)
    {
        if (stages[stageIndex]==null ||
            stages[stageIndex].Complete(stageProgression))
        {
            if (stages[stageIndex] != null && 
                stages[stageIndex].GetEndScript()!=null &&
                stages[stageIndex].GetEndScript().Length>2)
            {
                RunScript(stages[stageIndex].GetEndScript(), actor, view);
            }
            if (stageIndex == 0)
            {
                //make the prey vanish
                target.Remove();
            }
            stageIndex++;
            stageProgression = 0;
            if (stageIndex == 3)
            {
                return true;
            }
            return false;
        }
        if (InsideZone(actor.getPosition()) && 
            GlobalGameState.GetInstance().getCurrentZone().GetContents()
            .GetTile(actor.getPosition().x, actor.getPosition().y).isVisible())
        {
            stages[stageIndex].Progress(stageProgression,actor, target);
        }
        stageProgression++;
        return false;
    }

    private bool InsideZone(Vector2Int vector2Int)
    {
        ZoneParameters parameters= GlobalGameState.GetInstance().getCurrentZone().GetContents().GetZoneParameters();
        if (vector2Int.x<0 || vector2Int.x>= parameters.GetWidth())
        {
            return false;
        }
        if (vector2Int.y < 0 || vector2Int.y >= parameters.GetHeight())
        {
            return false;
        }
        return true;

    }

    private void RunScript(string scriptFile, ZoneActor actor, ViewInterface view)
    {
        GenericScriptRunner genericScriptRunner = GenericScriptRunner.GetInstance();
        genericScriptRunner.RunScript(scriptFile, actor, view);
    }

    internal int GetStage()
    {
        return stageIndex;
    }
}
