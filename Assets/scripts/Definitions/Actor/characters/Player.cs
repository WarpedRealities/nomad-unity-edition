﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player : Actor
{
    private ReformationHandler reformation;
    private Appearance appearance;
    private Inventory inventory;
    private ResearchHandler researchHandler;
    private QuickslotHandler quickslotHandler;
    private const string SPRITEFILE = "player";
    private int suppressRegeneration;
    private long companionUID=-1;
    public Player(Faction faction)
    {
        reformation = new ReformationHandler();
        researchHandler = new ResearchHandler();
        quickslotHandler = new QuickslotHandler();
        suppressRegeneration = 0;
        this.name = "player";
        this.faction = faction;
        actorRPG = new PlayerRPG();
        inventory = new Inventory(((PlayerRPG)actorRPG).GetCarryWeight(), ((PlayerRPG)actorRPG).GetDelegate());
        appearance = new Appearance();
        ((PlayerRPG)actorRPG).SetInventory(inventory);
        description = "This, is you";
    }

    public long GetCompanionUID()
    {
        return companionUID;
    }

    public QuickslotHandler GetQuickslots()
    {
        return quickslotHandler;
    }

    internal void SetName(string name)
    {
        this.name = name;
    }

    public override NPC_AI GetAI()
    {
        return null;
    }

    public override string getSprite()
    {
        return SPRITEFILE;
    }

    public override void Harmed()
    {
        suppressRegeneration = 5;
    }

    public int GetSuppression()
    {
        return suppressRegeneration;
    }
    public override void SetSuppression(int suppression)
    {
        suppressRegeneration = suppression;
    }

    public void DecrementSuppression(int time=1)
    {
        suppressRegeneration-=time;
    }

    public override bool IsPlayer()
    {
        return true;
    }

    internal Inventory GetInventory()
    {
        return inventory;
    }

    public ResearchHandler GetResearchHandler()
    {
        if (researchHandler == null)
        {
            researchHandler = new ResearchHandler();
        }
        return researchHandler;
    }

    public override void RunDead(ZoneActor zoneActor)
    {
        
    }

    public ReformationHandler GetReformation()
    {
        return reformation;
    }

    internal Appearance GetAppearance()
    {
        return appearance;
    }

    internal void SetCompanion(long v)
    {
        this.companionUID = v;
    }

    internal QuickslotHandler GetQuickSlotHandler()
    {
        return quickslotHandler;
    }
}
