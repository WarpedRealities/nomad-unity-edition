﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RespawnHandler 
{
    Vector2S spawnPosition;
    bool allowRespawn;
    long deathTimeStamp;
    long timeToRespawn;

    public Vector2S GetSpawnPosition()
    {
        return spawnPosition;
    }

    public void SetSpawnPosition(Vector2S position)
    {
        this.spawnPosition = position;
    }

    public bool IsAllowed()
    {
        return allowRespawn;
    }

    public long GetDeathTime()
    {
        return deathTimeStamp;
    }

    public void SetDeathTime(long time)
    {

        deathTimeStamp = time;
    }

    public long GetRespawnTime()
    {
        return timeToRespawn;
    }

    internal RespawnHandler Clone()
    {
        RespawnHandler respawnHandler = new RespawnHandler();
        respawnHandler.allowRespawn = this.allowRespawn;
        respawnHandler.timeToRespawn = this.timeToRespawn;
        respawnHandler.spawnPosition = this.spawnPosition;
        return respawnHandler;
    }

    internal void SetDuration(int duration)
    {
        this.timeToRespawn = duration;
        this.allowRespawn = true;
    }
}
