﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

[Serializable]
public class NPC : Actor
{
    NPC_AI AI;
    bool peaceful, isVictim;
    ExpirationHandler expirationHandler;
    RespawnHandler respawnHandler;
    NpcScriptHandler scriptHandler;
    VolatilityHandler volatilityHandler;
    ObserverVoreHandler observerVoreHandler;
    bool _companion;

    public NPC()
    {

    }

    public NPC(XmlElement root, string filename)
    {
        NPC_Loader loader = new NPC_Loader();
        loader.LoadNPC(root, filename);
        actorRPG = loader.GetRPG();
        name = loader.GetName();
        description = loader.GetDescription();
        peaceful = loader.GetPeace();
        faction = loader.GetFaction();
        this.movement = loader.GetMovement();
        AI = loader.GetAI();
        uid = GlobalGameState.GetInstance().GetUniverse().GetUIDGenerator().GetNpcID();
        expirationHandler = new ExpirationHandler();
        respawnHandler = loader.GetRespawnHandler();
        volatilityHandler = loader.GetVolatilityHandler();
        crimeFlags = new CrimeFlags();
    }

    internal void SetIsVictim(bool value)
    {
       this.isVictim = value;
    }

    public NPC(NPC npc, Vector2Int p)
    {

        this.name = npc.name;
        actorRPG = new NPC_RPG((NPC_RPG)npc.actorRPG);
        this.position = p;
        faction = npc.GetFaction();
        this.description = npc.GetDescription();
        AI = new NPC_AI(npc.AI.getScriptName());
        expirationHandler = new ExpirationHandler();
        this.respawnHandler = npc.respawnHandler.Clone();
        this.respawnHandler.SetSpawnPosition(new Vector2S(p.x,p.y));
        crimeFlags = new CrimeFlags();
        this.scriptHandler = npc.GetScriptHandler();
        this.peaceful = npc.IsPeaceful();
        this.movement = npc.movement;
        this.volatilityHandler = npc.volatilityHandler != null ? new VolatilityHandler(npc.volatilityHandler.GetLifeSpan()) : null;
        uid = GlobalGameState.GetInstance().GetUniverse().GetUIDGenerator().GetNpcID();
    }

    public NpcScriptHandler GetScriptHandler()
    {
        return scriptHandler;
    }

    public override ObserverVoreHandler GetObserverVoreHandler()
    {
        return observerVoreHandler;
    }

    public void SetObserverVoreHandler(ObserverVoreHandler observerVore)
    {
        this.observerVoreHandler = observerVore;
    }

    public override string GetDescription()
    {
        if (GetRank() > 0)
        {
            return ((NPC_RPG)actorRPG).GetRankDescription();
        }
        return description;
    }

    internal void SetIsCompanion(bool value)
    {
        this._companion = value;

    }

    internal void SetFaction(Faction faction)
    {
        this.faction = faction;
    }

    public int GetRank()
    {
        return ((NPC_RPG)this.actorRPG).GetRank();
    }

    public override bool IsCompanion()
    {
        return _companion;
    }

    public void SetRank(int rank)
    {
        ((NPC_RPG)this.actorRPG).SetRank(rank);
    }

    public override bool NoRegeneration()
    {
        return !respawnHandler.IsAllowed();
    }

    public void SetScriptHandler(NpcScriptHandler scriptHandler)
    {
        this.scriptHandler = scriptHandler;
    }

    public override NPC_AI GetAI()
    {
        return AI;
    }
    public override bool IsVolatile()
    {
        return volatilityHandler!=null;
    }
    public override string getSprite()
    {

        return ((NPC_RPG)actorRPG).GetSprite();
    }

    public override void Harmed()
    {
        peaceful = false;
    }

    public void SetPeaceful(bool peaceful)
    {
        this.peaceful = peaceful;
    }

    public override bool IsPeaceful()
    {
        return peaceful;
    }
    public override bool isHostile(string faction)
    {
        return GetFaction().getRelationship(faction) < 50 && !peaceful;
    }

    public ExpirationHandler GetExpirationHandler()
    {
        return expirationHandler;
    }

    public override void RunDead(ZoneActor zoneActor)
    {
        expirationHandler.Run(zoneActor);
    }

    internal override void Remove(bool dropLoot = false)
    {
        if (dropLoot)
        {
            Item item = actorRPG.GetLoot();
            if (item != null)
            {
                Tile t = GlobalGameState.GetInstance().getCurrentZone().GetContents().GetTile(position.x, position.y);
                if (t != null)
                {
                    if (t.GetWidget() == null)
                    {
                        t.SetWidget(new WidgetItemPile(item));
                    } else if (t.GetWidget() is WidgetItemPile)
                    {
                        WidgetItemPile itemPile = (WidgetItemPile)t.GetWidget();
                        itemPile.AddItem(item);
                    }
                }
            }
        }
        if (scriptHandler != null && scriptHandler.GetDeathScript() != null && scriptHandler.GetDeathScript().Length > 0)
        {

            NpcScriptRunnerTool.GetInstance().RunDeath(this, this.scriptHandler.GetDeathScript());
        }
        respawnHandler.SetDeathTime(GlobalGameState.GetInstance().GetUniverse().GetClock());
        position.x = -256;
        position.y = -256;
    }
    public override bool IsImmobile()
    {
        return ((NPC_RPG)actorRPG).IsImmobile();
    }
    public override bool IsInvolvedInObserverVore()
    {
        return observerVoreHandler!=null|| isVictim;
    }

    public override VolatilityHandler GetVolatilityHandler()
    {
        return volatilityHandler;
    }

    internal override void ZoneEntryHandler(long clock)
    {
        if (this.GetAlive() && this.scriptHandler!=null && 
            this.scriptHandler.GetSpawnScript()!=null && 
            this.scriptHandler.GetSpawnScript().Length > 1)
        {
            if (this.position.x < 0 && NpcScriptRunnerTool.GetInstance().RunSpawn(this,this.scriptHandler.GetSpawnScript()))
            {
                //re-add
                this.position = new Vector2Int(respawnHandler.GetSpawnPosition().x, respawnHandler.GetSpawnPosition().y);
            }
            if (this.position.x>=0 && !NpcScriptRunnerTool.GetInstance().RunSpawn(this, this.scriptHandler.GetSpawnScript()))
            {
                //remove
                this.position = new Vector2Int(this.position.x-256,this.position.y-256);
            }
        }
        if (!this.GetAlive() && position.x >= 0)
        {

            Remove(true);
        }
        if (this.GetAlive())
        {
            this.actorRPG.GetStatusHandler().Update(100, null, null);
        }
        if (!this.GetAlive() && respawnHandler.IsAllowed())
        {

            if (clock > respawnHandler.GetDeathTime() + respawnHandler.GetRespawnTime())
            {
                //place npc 
                this.position = new Vector2Int(respawnHandler.GetSpawnPosition().x, respawnHandler.GetSpawnPosition().y);
                //restore hp and etc
                actorRPG.Heal(1);
                ((NPC_RPG)actorRPG).ResetReward();
            }
        }
        if (this.GetAlive() && ((NPC_RPG)GetRPG()).GetStartStatus()!=null)
        {
            StatusEffect statusEffect = ((NPC_RPG)GetRPG()).GetStartStatus();
            if (!actorRPG.GetStatusHandler().HasStatusEffect(statusEffect.GetUid()))
            {
                actorRPG.GetStatusHandler().AddStatusEffect(statusEffect.Clone());
            }
        }
    }
}
