﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class CrimeFlags
{
    private bool[] flags=new bool[2];
    private bool witness;
    public CrimeFlags()
    {
        flags[0] = false;
        flags[1] = false;
        witness = false;
    }

    public bool ReadFlag(int index)
    {
        return flags[index];
    }

    public void SetFlag(int index)
    {
        flags[index] = true;
    }

    public bool IsWitness()
    {
        return witness;
    }

    public void SetWitness(bool value)
    {
        witness = value;
    }
}
