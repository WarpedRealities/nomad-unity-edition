﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionDelay: Action
{
    private bool prevention;
    public ActionDelay(int duration, bool tiring=true, bool prevention=false)
    {
        this.tiringAction = tiring;
        this.interruptType = InterruptType.NONE;
        this.duration = duration;
        this.actionType = ActionType.NONE;
        this.prevention = prevention;
    }

    public override string GetName()
    {
        return "delay but if you're interrupting this then what the hell";
    }

    internal bool PreventAction()
    {
        return prevention;
    }
}
