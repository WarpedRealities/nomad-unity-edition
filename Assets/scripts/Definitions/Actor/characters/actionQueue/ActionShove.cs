﻿using UnityEngine;
using System.Collections;

public class ActionShove : Action
{
    Vector2Int position;
    public ActionShove(int duration, Vector2Int position)
    {
        this.position = position;
        this.tiringAction = true;
        this.interruptType = InterruptType.NONE;
        this.duration = duration;
        this.actionType = ActionType.SHOVE;
    }

    public override string GetName()
    {
        return "shove";
    }

    public Vector2Int GetPosition()
    {
        return position;
    }
}
