﻿using UnityEngine;
using System.Collections;
using System;

public class ActionDoMove : Action
{
    private CombatMove combatMove;
    private AmmoType ammoType;
    private ZoneActor target;
    private Vector2Int targetPosition;

    public ActionDoMove(CombatMove move, ZoneActor target, AmmoType ammoType)
    {
        this.actionType = ActionType.ATTACK;
        this.interruptType = move.GetInterruptType();
        this.combatMove = move;
        this.ammoType = ammoType;
        this.tiringAction = true;

        this.target = target;
        duration = (int)(move.GetTimeCost());
    }

    public ActionDoMove(CombatMove move, Vector2Int position, AmmoType ammoType)
    {
        this.actionType = ActionType.ATTACK;
        this.interruptType = move.GetInterruptType();
        this.combatMove = move;
        this.ammoType = ammoType;
        this.tiringAction = true;

        this.targetPosition = position;
        duration = (int)(move.GetTimeCost());
    }

    public ActionDoMove(CombatMove move, ZoneActor target, float speedMod, AmmoType ammoType)
    {
        this.actionType = ActionType.ATTACK;
        this.interruptType = move.GetInterruptType();
        this.combatMove = move;
        this.ammoType = ammoType;

        this.tiringAction = true;

        this.target = target;
        duration = (int)(move.GetTimeCost()*speedMod);
    }

    public ActionDoMove(CombatMove move, Vector2Int position, float speedMod, AmmoType ammoType) 
    {
        this.actionType = ActionType.ATTACK;
        this.interruptType = move.GetInterruptType();
        this.combatMove = move;
        this.ammoType = ammoType;
        this.tiringAction = true;

        this.targetPosition = position;
        duration = (int)(move.GetTimeCost() * speedMod);
    }

    public CombatMove GetCombatMove()
    {
        return combatMove;
    }

    public AmmoType GetAmmoType()
    {
        return ammoType;
    }

    public override string GetName()
    {
        return this.combatMove.GetName();
    }

    public ZoneActor GetTarget()
    {
        return this.target;
    }

    public Vector2Int GetTargetPosition()
    {
        return targetPosition;
    }

    public Vector2Int GetPosition()
    {
        if (target != null)
        {
            return target.getPosition();
        }
        else
        {
            return targetPosition;
        }
    }
}
