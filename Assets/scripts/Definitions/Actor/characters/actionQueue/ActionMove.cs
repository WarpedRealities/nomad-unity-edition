﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using UnityEngine;

public class ActionMove : Action
{
    public const int DEFAULTMOVECOST = 10;
    public const float DIAGONALMULTIPLIER = 1.5F;
    
    private int direction;
    public ActionMove(int direction, float speed)
    {
        this.actionType = ActionType.MOVE;
        this.direction = direction;
        duration = direction %2 ==0 ? (int)(speed * DEFAULTMOVECOST): (int)(speed * DEFAULTMOVECOST*DIAGONALMULTIPLIER);
        this.tiringAction = false;
        interruptType = InterruptType.STAGGER;
    }

    public int getDirection()
    {
        return direction;
    }

    public override string GetName()
    {
        return "movement";
    }
}
