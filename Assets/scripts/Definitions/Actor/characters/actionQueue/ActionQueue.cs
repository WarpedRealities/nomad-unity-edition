﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActionQueue 
{
    private Queue<Action> actions;
    private bool canInterrupt, apRegen;
    private string interruptMessage;
    
    public ActionQueue()
    {
        actions = new Queue<Action>();
    }

    public bool IsEmpty()
    {
        
        return actions.Count == 0;
    }

    public bool ProcessQueue()
    {
        canInterrupt = true;
        Action action = actions.Peek();
        apRegen = !action.IsTiring();
        if (action.isActive())
        { 
            actions.Peek().DecrementDuration();
            return false;
        }
        else
        {
            return true;
        }  
    }

    public bool ApRegen()
    {
        return apRegen;
    }

    public Action GetAction()
    {

        return actions.Count > 0 ? actions.Peek(): null;
    }

    public void CompleteAction()
    {
        actions.Dequeue();
    }

    public void AddAction(Action action)
    {
        actions.Enqueue(action);
    }

    public bool CanInterrupt(InterruptType interruptType)
    {
        if (canInterrupt && !IsEmpty())
        {
            int level = (int)interruptType;

            return (level >= (int)actions.Peek().GetInterruptType());
        }
        return false;
    }
    public void Interrupt(InterruptType interruptType)
    {
        if (canInterrupt && !IsEmpty())
        {
            int level = (int)interruptType;
            if (level >= (int)actions.Peek().GetInterruptType())
            {
                interruptMessage = GenerateInterrupt(actions.Peek(), interruptType);
                actions.Clear();
                canInterrupt = false;
            }
        }
    }

    private string GenerateInterrupt(Action action, InterruptType interrupt)
    {
        string interruptString = GenerateInterruptString(interrupt);
        return "action " + action.GetName() + " interrupted " + interruptString;
    }

    private string GenerateInterruptString(InterruptType interrupt)
    {
        switch (interrupt)
        {
            case InterruptType.DAMAGE:
                return "by damage";
            case InterruptType.MELEE:
                return "by melee attack";
            case InterruptType.DISTRACT:
                return "by distraction";
            case InterruptType.STAGGER:
                return "by stagger";
        }

        return "";
    }

    public void HandleDelayedInterruptText(string name, ViewInterface view)
    {
        if (interruptMessage != null)
        {
            string str = interruptMessage;
            interruptMessage = null;
            view.DrawText(name+" "+str);
        }
    }

    public void Clear()
    {
        actions.Clear();
    }

    internal void ReadActions(string name)
    {
        if (actions.Count >0)
        {
            Action action = actions.Peek();
            
        }
    }

    public string ReadAction()
    {
        if (actions.Count > 0)
        {
            Action action = actions.Peek();
            return action.GetName();
        }
        return "";
    }

    internal int GetLength()
    {
        return actions.Count;
    }
}
