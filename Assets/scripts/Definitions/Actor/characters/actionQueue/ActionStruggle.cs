﻿using UnityEngine;
using System.Collections;

public class ActionStruggle : Action
{
    private readonly string NAME = "struggle";

    public ActionStruggle(int duration)
    {
        this.duration = duration;
        this.interruptType = InterruptType.NONE;
        this.tiringAction = true;
        this.actionType = ActionType.STRUGGLE;
    }

    public override string GetName()
    {
        return NAME; 
    }
}
