﻿using UnityEngine;
using System.Collections;

public class ActionReload : Action
{
    Item refill;
    Item ammo;
    public ActionReload(Item ammo, Item refill, int duration, InterruptType interruptType=InterruptType.MELEE)
    {
        Debug.Log("reload " + (ammo != null));
        this.ammo = ammo;
        this.refill = refill;
        this.actionType = ActionType.RELOAD;
        this.interruptType = interruptType;
        this.duration = duration;

    }

    public override string GetName()
    {
        return "reload";
    }

    public Item GetAmmo()
    {
        return ammo;
    }

    public Item GetRefill()
    {
        return refill;
    }
}
