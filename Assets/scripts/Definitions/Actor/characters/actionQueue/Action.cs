﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action 
{
    protected int duration;
    protected InterruptType interruptType;
    protected ActionType actionType;
    protected bool tiringAction;

    public bool isActive()
    {
    
        return duration > 1;
    }

    public void DecrementDuration(int decrement=1)
    {
        duration-=decrement;
    }

    public InterruptType GetInterruptType()
    {
        return interruptType;
    }

    public ActionType GetActionType()
    {
        return actionType;
    }

    public bool IsTiring()
    {
        return tiringAction;
    }

    public int GetDuration()
    {
        return duration;
    }

    public abstract string GetName();
}
