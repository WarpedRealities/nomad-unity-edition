﻿using UnityEngine;
using System.Collections;

public class ActionRecover : Action
{
    float bonus;
    public ActionRecover(int duration,float bonus=0)
    {
        this.bonus = bonus;
        this.tiringAction = false;
        this.interruptType = InterruptType.DAMAGE;
        this.duration = duration;
        this.actionType = ActionType.RECOVER;
    }

    public float GetBonus()
    {
        return bonus;
    }

    public override string GetName()
    {
        return "recover";
    }
}
