﻿using UnityEngine;
using System.Collections;
using System;

public class ActionContainer : Action
{
    private readonly int DURATION = 5;
    private bool putting;
    private ContainerData containerData;
    private Item item;

    public ActionContainer(Item item, ContainerData container, bool put)
    {
        this.containerData = container;
        this.item = item;
        this.putting = put;
        actionType = ActionType.CONTAINER;
        interruptType = InterruptType.DAMAGE;
        duration = DURATION;
    }


    public override string GetName()
    {
        return "container interaction";
    }

    public ContainerData GetContainer()
    {
        return containerData;
    }

    public bool IsPutting()
    {
        return putting;
    }

    public Item GetItem()
    {
        return item;
    }
}
