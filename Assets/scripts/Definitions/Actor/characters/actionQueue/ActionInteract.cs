﻿using System;
using UnityEngine;

internal class ActionInteract : Action
{
    private readonly string ACTIONNAME = "interact";
    private Vector2Int target;
    private Player player;

    public ActionInteract(Vector2Int target, Player player)
    {
        this.actionType = ActionType.INTERACT;
        this.target = target;
        this.player = player;
        this.interruptType = InterruptType.DAMAGE;
        this.duration = 10;
    
    }

    public override string GetName()
    {
        return ACTIONNAME;
    }

    public Vector2Int GetTarget()
    {
        return target;
    }

    public Player GetPlayer()
    {
        return player;
    }
}