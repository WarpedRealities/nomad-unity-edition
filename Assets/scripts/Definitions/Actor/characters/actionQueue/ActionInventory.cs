﻿using System;

public class ActionInventory : Action
{
    private Item item;
    private InventoryAction inventoryAction;

    public ActionInventory(Item item, InventoryAction inventoryAction, int duration)
    {
        this.actionType = ActionType.INVENTORY;
        this.interruptType = InterruptType.STAGGER;
        this.item = item;
        this.duration = duration;
        this.inventoryAction = inventoryAction;
        if (inventoryAction == InventoryAction.EQUIP || inventoryAction == InventoryAction.UNEQUIP)
        {
            this.duration = CalculateEquipDuration(item);
        }
    }

    private int CalculateEquipDuration(Item item)
    {
        ItemEquippable itemEquippable = (ItemEquippable)item.GetDef();

        return itemEquippable.GetEquipTime();
    }

    public override string GetName()
    {
        switch (inventoryAction)
        {
            case InventoryAction.DROP:
                return "drop item";
        }

        return "";
    }

    public Item GetItem()
    {
        return item;
    }

    public InventoryAction GetInventoryActionType()
    {
        return this.inventoryAction;
    }

    public Item TakeItem()
    {
      //  if (item.GetItemType() == ItemType.STACK)
        {
     //       return item.GetItem();
        }
     //   else
        {
      //      return item;
        }
        return null;
    }
}