﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ExpirationHandler
{
    int countdown;

    public ExpirationHandler()
    {
        countdown = -1;
    }

    internal void Run(ZoneActor zoneActor)
    {
        if (countdown == -1)
        {
            countdown = 2000;
        } else
        {
            countdown--;
            if (countdown == -1)
            {
                ExpireNPC(zoneActor);
            }
        }
    }

    private void ExpireNPC(ZoneActor zoneActor)
    {
        if (zoneActor.actor.GetRPG().GetStat(ST.HEALTH) > 0)
        {
            Revive(zoneActor);
        }
        else
        {
            zoneActor.Remove(true);
        }
    }

    private void Revive(ZoneActor zoneActor)
    {
        if (zoneActor.actor is NPC && zoneActor.actor.GetRPG().GetStat(ST.RESOLVE) <= 0)
        {
            ((NPC)zoneActor.actor).SetPeaceful(true);
        }
        zoneActor.actor.GetRPG().Heal(1);
        
        zoneActor.ForceUpdate(true);
    }
}
