﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public abstract class Actor
{
    [NonSerialized]
    protected Vector2Int position;
    protected Vector2S serializePosition;
    protected TileMovement movement;
    protected long uid;
    protected ActorRPG actorRPG;
    protected Faction faction;
    protected string name, description;
    protected FlagField flagField;
    protected CrimeFlags crimeFlags;
    [OnSerializing()]
    internal void OnSerialize(StreamingContext streamingContext)
    {
        serializePosition = new Vector2S(position.x, position.y);
    }

    [OnDeserialized()]
    internal void OnDerialize(StreamingContext streamingContext)
    {
        position = new Vector2Int(serializePosition.x, serializePosition.y);
    }

    public Vector2Int GetPosition()
    {
        return position;
    }

    public long GetUID()
    {
        return uid;
    }

    public virtual bool IsCompanion()
    {
        return false;
    }

    public abstract string getSprite();

    public void SetPosition(Vector2Int position)
    {
        this.position = position;
    }

    public virtual string GetDescription()
    {
        return description;
    }

    public TileMovement GetMovementType()
    {
        return movement;
    }

    public virtual bool IsPlayer()
    {
        return false;
    }

    public virtual bool IsVolatile()
    {
        return false;
    }

    public virtual bool IsPeaceful()
    {
        return false;
    }

    public virtual bool IsInvolvedInObserverVore()
    {
        return false;
    }
    public virtual void SetSuppression(int suppression)
    {

    }
    public ActorRPG GetRPG()
    {
        return actorRPG;
    }

    internal bool GetAlive()
    {
        return !(actorRPG.GetStat(ST.HEALTH)<=0 || actorRPG.GetStat(ST.RESOLVE)<=0);
    }

    public virtual bool isHostile(string faction)
    {

        return GetFaction().getRelationship(faction) < 50;
    }

    internal virtual void Remove(bool dropLoot=false)
    {
        throw new NotImplementedException();
    }

    public virtual bool NoRegeneration()
    {
        return false;
    }

    public Faction GetFaction()
    {
        return faction;
    }

    public FlagField GetFlagField()
    {
        if (flagField == null)
        {
            flagField = new FlagField();
        }
        return flagField;
    }

    abstract public NPC_AI GetAI();

    public string GetName()
    {
        return name;
    }

    public virtual bool IsImmobile()
    {
        return false;
    }
    public abstract void Harmed();

    public abstract void RunDead(ZoneActor zoneActor);

    internal virtual void ZoneEntryHandler(long clock)
    {
        
    }


    public CrimeFlags GetCrimeFlags()
    {
        return crimeFlags;
    }

    public virtual VolatilityHandler GetVolatilityHandler()
    {
        return null;
    }

    public virtual ObserverVoreHandler GetObserverVoreHandler()
    {
        return null;
    }
}
