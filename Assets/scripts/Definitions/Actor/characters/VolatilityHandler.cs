﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class VolatilityHandler
{
    int lifespan = 0;

    public VolatilityHandler(int lifespan)
    {
        this.lifespan = lifespan;
    }
    
    public bool Update(int time)
    {
        lifespan -= time;
        return time <= 0;
    }

    internal int GetLifeSpan()
    {
        return lifespan;
    }
}
