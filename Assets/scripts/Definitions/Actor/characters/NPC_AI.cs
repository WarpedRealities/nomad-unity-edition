﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NPC_AI
{
    private string scriptFile;
    private int[] memoryValues;

    public NPC_AI(string scriptFile)
    {
        memoryValues = new int[16];
        for (int i = 0; i < memoryValues.Length; i++)
        {
            memoryValues[i] = 0;
        }
        this.scriptFile = scriptFile;
    }

    public string getScriptName()
    {
        return scriptFile;
    }

    public int GetValue(int index)
    {
        return memoryValues[index];
    }

    public void SetValue(int index, int value)
    {
        memoryValues[index] = value;
    }
}
