﻿using UnityEngine;
using System.Collections;
using System;

[Serializable()]
public class NpcScriptHandler 
{

    private string spawnScript, deathScript;

    public NpcScriptHandler(string spawn, string death)
    {
        this.spawnScript = spawn;
        this.deathScript = death;
    }

    public string GetSpawnScript()
    {
        return spawnScript;
    }

    public string GetDeathScript()
    {
        return deathScript;
    }
 
}
