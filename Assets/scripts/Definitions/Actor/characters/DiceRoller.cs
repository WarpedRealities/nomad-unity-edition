﻿using System;

public  class DiceRoller
{
    private static DiceRoller instance=null;

    public static DiceRoller GetInstance()
    {
        if (instance == null)
        {
            instance = new DiceRoller();
        }
         return instance;
    }

    private Random random;

    public DiceRoller()
    {
        random = new Random();
    }

    public int RollDice(int range)
    {
        return random.Next(0,range);
    }

    public float RollOdds()
    {
        return (float)random.NextDouble();
    }

}