﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Xml;
using System.Threading;
using System.Linq;

[Serializable]
public class ResearchData {

    protected int diceroll, difficultyCheck;
    protected string dataName;

    public ResearchData(int diceroll, int check, string name)
    {
        this.diceroll = diceroll;
        this.difficultyCheck = check;
        this.dataName = name;
    }

    public virtual bool RunCheck(int science)
    {
        return diceroll + science >= difficultyCheck;
    }

    public virtual bool isConsumed()
    {
        return true;
    }
}
[Serializable]
public class ResearchThresholdData : ResearchData
{
    public ResearchThresholdData(int difficulty, string name) : base(0, difficulty, name)
    {

    }

    public override bool RunCheck(int science)
    {
        return science >= difficultyCheck;
    }

    public override bool isConsumed()
    {
        return false;
    }
}

[Serializable]
public class ResearchHandler 
{
    Dictionary<string, ResearchData> dataDictionary;
    Dictionary<string, int> researchDictionary;
    HashSet<string> unlockedEntries;

    public ResearchHandler()
    {
        dataDictionary = new Dictionary<string, ResearchData>();
        researchDictionary = new Dictionary<string, int>();
        unlockedEntries = new HashSet<string>();

    }

    internal List<string> GetEntryKeys()
    {
        return new List<string>(unlockedEntries);
    }

    public List<String> GetResearchKeys()
    {
        return new List<string>(dataDictionary.Keys);
    }

    internal bool HasResearch(string key)
    {
        return dataDictionary.ContainsKey(key);
    }

    internal bool HasEntry(string key)
    {
        return this.unlockedEntries.Contains(key);
    }

    internal void AddResearch(int dc, string name)
    {
        int roll = DiceRoller.GetInstance().RollDice(20);
        this.dataDictionary.Add(name, new ResearchData(roll, dc, name));
    }

    internal bool ResolveResearch(string studyData, Player player)
    {
        ResearchData researchData = dataDictionary[studyData];

        if (researchData != null)
        {            
            if (researchData.RunCheck(player.GetRPG().getSkill(SK.SCIENCE)))
            {
                if (researchDictionary.ContainsKey(studyData))
                {

                    researchDictionary[studyData] = researchDictionary[studyData] + 1;
                }
                else
                {

                    researchDictionary.Add(studyData, 1);
                }

                dataDictionary.Remove(studyData);
                return true;
            }
            else
            {
                if (researchData.isConsumed())
                {
                    dataDictionary.Remove(studyData);
                }
            }
        }
        return false;
    }

    public string PullEntry(string file)
    {

        XmlDocument document = FileTools.GetXmlDocument("pedia/" + file);
        XmlNodeList children = document.FirstChild.NextSibling.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("description".Equals(element.Name))
                {
                    return element.InnerText;
                }
            }
        }
        return null;
    }

    internal string ResolveUnlocks(Player actor)
    {
        Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

        string[] strings = Directory.GetFiles("gameData\\data\\pedia", "*", SearchOption.AllDirectories);
        for (int i = 0; i < strings.Length; i++)
        {
              strings[i]=strings[i].Replace("\\","/");
            if (File.Exists(strings[i]) && CheckUnlock(strings[i]))
            {
                //open the new file
                FileStream fileStream = File.Open(strings[i], FileMode.Open);
                XmlDocument document = new XmlDocument();
                document.Load(fileStream);
                fileStream.Close();
                if (UnlockEntry(document))
                {
                    string[] splits = strings[i].Split('/');
                    unlockedEntries.Add(splits[splits.Length - 1].Replace(".xml",""));
                    return PullEntry(document);
                }
            }

        }
        return null;
    }

    private string PullEntry(XmlDocument document)
    {
        XmlNodeList children = document.FirstChild.NextSibling.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("description".Equals(element.Name))
                {
                    return element.InnerText;
                }
            }
        }
        return null;
    }

    private bool UnlockEntry(XmlDocument document)
    {
        XmlNodeList children = document.FirstChild.NextSibling.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("requirement".Equals(element.Name))
                {
                    int value = int.Parse(element.GetAttribute("value"));
                    string key = element.GetAttribute("data");


                    if (!researchDictionary.ContainsKey(key))
                    {

                        return false;
                    }

                    if (researchDictionary[key] < value)
                    {

                        return false;
                    }
                }
            }
        }
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("unlocks".Equals(element.Name))
                {
                    GlobalGameState.GetInstance().GetUniverse().GetCraftingLibrary().UnlockRecipe(element.GetAttribute("recipe"));
                }
            }
        }
        return true;
    }

    private bool CheckUnlock(string file)
    {

        string[] splits = file.Split('/');

        if (this.unlockedEntries.Contains(splits[splits.Length - 1].Replace(".xml","")))
        {

            return false;
        }

        return true;
    }
}
