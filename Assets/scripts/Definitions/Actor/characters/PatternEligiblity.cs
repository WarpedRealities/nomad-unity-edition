﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PatternManager
{
    public BlockRay blockRay;
    public VisitDelegate visit;
    public DebugDelegate debug;
    ZoneContents contents;
    ViewInterface viewInterface;
    public PatternManager(ZoneContents zoneContents)
    {
        this.contents = zoneContents;
        blockRay = RayBlock;
        debug = DebugReport;
        visit = VisitTile;
        viewInterface = GameObject.Find("ViewSceneController").GetComponent<ViewScene>();
    }

    public void DebugReport(Vector2 position)
    {
        Debug.Log("debug" + position);
        viewInterface.DrawImpact(position, 4);
    }

    bool RayBlock(Vector2Int position)
    {
        Tile t = contents.GetTile(position.x, position.y);
        if (t != null)
        {

            return t.IsVisionBlocking();
        }
        return true;
    }

    public void VisitTile(Vector2Int position)
    {
     
    }
}
public class PatternEligiblity
{
    LineOfSight2 lineOfSight;
    ZoneActor zoneActor;

    public PatternEligiblity(ZoneActor zoneActor)
    {
        this.zoneActor = zoneActor;
        lineOfSight = new LineOfSight2();
    }

    public bool TargetCheck(CombatMove move, Vector2Int p, bool looseMelee=false)
    {

        switch (move.GetPattern())
        {
            case AttackPattern.MELEE:
                float d = Vector2Int.Distance(p, zoneActor.getPosition());
                return looseMelee ? d < 3: d < 2;
            case AttackPattern.SELF:
                return true;
            case AttackPattern.DIRECT:
                return true;
            case AttackPattern.RANGED:
                return RangedCheck(p);
            case AttackPattern.BLAST:
                return RangedCheck(p);
        }
        return true;
    }

    internal bool IsMelee(AttackPattern attackPattern)
    {
        switch (attackPattern)
        {
            case AttackPattern.MELEE:
                return true;
        }
        return false;
    }

    internal bool IsRanged(AttackPattern attackPattern)
    {
        switch (attackPattern)
        {
            case AttackPattern.MELEE:
                return false;
            case AttackPattern.SELF:
                return false;
            case AttackPattern.NONE:
                return false;
        }
        return true;
    }

    private bool RangedCheck(Vector2Int p)
    {
        //line of sight check
        float distance = Vector2Int.Distance(p, zoneActor.getPosition());
        if (distance<2)
        {
            return true;
        }
        if (distance > 10)
        {

            return false;
        }
        PatternManager patternManager = new PatternManager(GlobalGameState.GetInstance().getCurrentZone().GetContents());
        if (!lineOfSight.CanSee(zoneActor.getPosition(), p, patternManager.visit, patternManager.blockRay, null))
        {
            return false;
        }
        return true;
    }
}
