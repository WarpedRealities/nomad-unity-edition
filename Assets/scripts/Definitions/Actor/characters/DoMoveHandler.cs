﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class DoMoveHandler
{
    private ZoneActor zoneActor;
    private Actor actor;
    private DiceRoller diceRoller;
    private PatternEligiblity patternEligiblity;
    public DoMoveHandler(ZoneActor actor, PatternEligiblity patternEligiblity)
    {
        this.patternEligiblity = patternEligiblity;
        this.diceRoller = DiceRoller.GetInstance();
        this.zoneActor = actor;
        this.actor = actor.actor;
    }

    private bool InRange(Actor actor, ZoneActor target, Vector2Int tPos, AttackPattern pattern)
    {
        Vector2Int t = target!=null ? target.getPosition() : tPos;
        switch (pattern)
        {
            case AttackPattern.MELEE:
                return Vector2Int.Distance(actor.GetPosition(), t) < 2;

            case AttackPattern.RANGED:
                return Vector2Int.Distance(actor.GetPosition(), t) < 12;
        }

        return false;
    }


    private void ExpendAmmo(CombatMove move)
    {
        //expend ammo
        int ammocost = move.GetAmmoCost();
        int ammoPool = move.GetAmmoPool();
        if (ammocost == 0)
        {
            return;
        }
        if (ammoPool >= 0)
        {
            if (((Player)actor).GetInventory().GetSlot(ammoPool) != null)
            {
                if (((ItemEquippable)(((Player)actor).GetInventory().GetSlot(ammoPool)).GetDef()).GetResourceStore() != null)
                {
                    EquipInstance equipInstance = (EquipInstance)((Player)actor).GetInventory().GetSlot(ammoPool);
                    ResourceStoreInstance resourceStoreInstance = equipInstance.GetResourceStore();
                    if (resourceStoreInstance == null)
                    {
                        return;
                    }
                    resourceStoreInstance.ExpendAmmo(ammocost);
                }
                else
                {
                    Inventory inventory = ((Player)actor).GetInventory();
                    //no resource store, item must be used itself as ammo
                    if (inventory.GetSlot(ammoPool) is EquipInstance)
                    {
                        inventory.SetSlot(ammoPool, null);
                        ((PlayerRPG)actor.GetRPG()).UpdateMoves();
                        ((Player)actor).GetQuickSlotHandler().UpdateQuickSlots(inventory,
                        ((PlayerRPG)actor.GetRPG()).GetMoves());
                    }
                    else if (inventory.GetSlot(ammoPool) is ItemStack)
                    {
                        ItemStack itemStack = (ItemStack)inventory.GetSlot(ammoPool);
                        if (itemStack.GetCount() == 1)
                        {
                            inventory.SetSlot(ammoPool, null);
                            //need to recalculate for when the attack is no longer available
                            ((PlayerRPG)actor.GetRPG()).UpdateMoves();
                            ((Player)actor).GetQuickSlotHandler().UpdateQuickSlots(inventory,
                                ((PlayerRPG)actor.GetRPG()).GetMoves());
                        }
                        else
                        {
                            itemStack.SetCount(itemStack.GetCount() - 1);
                        }
                    }
                }
            }
        }
        if (ammoPool == -1)
        {
            actor.GetRPG().ReduceStat(ST.SATIATION, ammocost);
        }
    }

    public bool ProcessAttack(ActionDoMove action, ViewInterface view, ActionQueue queue)
    {

        CombatMove move = action.GetCombatMove();
        if (PreventSelfHarm(move, action))
        {
            return false;
        }

        //check distance is combatible with move
        if (!patternEligiblity.TargetCheck(move, action.GetPosition(),true))
        {
            if (this.actor.IsPlayer())
            {
                this.actor.GetRPG().ReduceStat(ST.ACTION, move.GetAPCost() - 1);
                view.DrawText("target out of range");
            }
            return false;
        }
        if (patternEligiblity.IsRanged(move.GetPattern())) {
            zoneActor.SetActorAnimation(new RangedAnimation(zoneActor));
        }
        else if (patternEligiblity.IsMelee(move.GetPattern()))
        {
            Vector2 t = action.GetTarget() != null ?
                action.GetTarget().getPosition() :
                action.GetTargetPosition();
            zoneActor.SetActorAnimation(new MeleeAnimation(zoneActor, t-actor.GetPosition(), actor.GetPosition()));
        }

        ExpendAmmo(move);
        HandleScan(move, action, action.GetTarget()!=null? action.GetTarget().getPosition() : action.GetTargetPosition(), view);
        if (action.GetTarget() == null)
        {

            if (!ProcessTileAttack(action, action.GetTargetPosition(), view, queue, move))
            {
                HandleAreaAttack(action, view, queue);
                return false;
            }
        }
        HandleAreaAttack(action, view, queue);
        if (action.GetTarget()!=null && !AttackActor(action, actor, action.GetTarget(), view, queue, move))
        {
            return false;
        }


        return true;
    }

    private bool PreventSelfHarm(CombatMove move, ActionDoMove action)
    {
        Vector2Int p = action.GetTarget() != null ?
            action.GetTarget().getPosition() :
            action.GetTargetPosition();
        switch (move.GetPattern())
        {
            case AttackPattern.MELEE:
                if (actor.GetPosition().x == p.x &&
                    actor.GetPosition().y == p.y)
                {
                    return true;
                }
                break;
            case AttackPattern.RANGED:
                if (actor.GetPosition().x == p.x &&
                    actor.GetPosition().y == p.y)
                {
                    return true;
                }
                break;
        }
        return false;

    }

    private void HandleAreaAttack(ActionDoMove action, ViewInterface view, ActionQueue queue)
    {
        CombatMove move = action.GetCombatMove();
        switch (move.GetPattern())
        {
            case AttackPattern.BLAST:
                HandleBlast(action, move, view, queue);
                break;
            default:

                break;
        }
    }

    private void HandleBlast(ActionDoMove action, CombatMove move, ViewInterface view, ActionQueue queue)
    {
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(action.GetPosition().x, action.GetPosition().y, i);
            ProcessTileAttack(action, p, view, queue, move);
            view.DrawImpact(p, (int)move.GetMoveIndicator());
        }
    }

    private bool AttackActor(ActionDoMove action, Actor actor, ZoneActor target, ViewInterface view, ActionQueue queue, CombatMove move)
    {

        AmmoType ammoType = action.GetAmmoType();
        //roll the dice
        int dice = diceRoller.RollDice(20);
        int roll = dice + actor.GetRPG().getSkill(move.GetSkill()) + move.GetBonus();
        if (ammoType != null)
        {
            roll += ammoType.GetAccuracy();
        }
        int difficulty = 8 + target.actor.GetRPG().getSkill(GetDefenceFromSkill(move.GetSkill()));
        bool ranged = move.GetPattern().Equals(AttackPattern.RANGED);
        if (move.GetRangeDecay() > 0)
        {
            difficulty += (int)(move.GetRangeDecay() * Vector2Int.Distance(target.getPosition(), actor.GetPosition()));
        }

        if (roll >= difficulty || move.GetPattern() == AttackPattern.SELF)
        {
            if (Config.GetInstance().IsShowSkillChecks())
            {
                view.DrawText("skill check " + actor.GetName() + " to attack " + target.actor.GetName() + " DC" + difficulty + "roll " + roll + " hit");
            }
            //handle hit
            bool critical = roll >= difficulty + 12 && AttackPattern.SELF != move.GetPattern();

            int value = 0;

            //handle applying effects
            for (int i = 0; i < move.getEffects().Count; i++)
            {
                value += target.ApplyEffect(move.getEffects()[i], critical, zoneActor, ammoType, diceRoller, view);
            }
            if (ammoType!=null &&
                ammoType.GetExtraEffects()!=null &&
                ammoType.GetExtraEffects().Count > 0)
            {
                for (int i=0;i < ammoType.GetExtraEffects().Count; i++)
                {
                    value += target.ApplyEffect(ammoType.GetExtraEffects()[i], critical, zoneActor, ammoType, diceRoller, view);
                }
            }
            if (value > 0)
            {
                target.actor.Harmed();
            }
            if (target.IsVisible())
            {

                view.DrawText(GenerateHitText(move, target.actor.GetName(), this.actor, value, critical));
                target.HandleDelayedText(view);

                view.DrawImpact(target.getPosition(), (int)move.GetMoveIndicator());
                if (ranged)
                {
                    view.DrawRanged(actor.GetPosition(), target.getPosition(), (int)move.GetMoveIndicator());
                }
            }
            target.CheckDefeat(zoneActor, view);
            return true;
        }
        else
        {
            for (int i = 0; i < move.getEffects().Count; i++)
            {
                target.ApplyMissEffect(move.getEffects()[i], false, zoneActor, ammoType, diceRoller, view);
            }
            //handle miss
            if (target.IsVisible())
            {
                if (Config.GetInstance().IsShowSkillChecks())
                {
                    view.DrawText("skill check " + actor.GetName() + " to attack " + target.actor.GetName() + " DC" + difficulty + "roll " + roll + " miss");
                }
                if (ranged)
                {
                    view.DrawRanged(actor.GetPosition(),
                        GeometryTools.GetPosition(target.getPosition().x, target.getPosition().y, (int)(UnityEngine.Random.value * 8)),
                        (int)move.GetMoveIndicator());
                }
                view.DrawText(GenerateMissText(move, target.actor, this.actor));
            }
            return true;
        }
        //apply effects to target

    }

    private bool ProcessTileAttack(ActionDoMove action, Vector2Int position, ViewInterface view, ActionQueue queue, CombatMove move)
    {
        Tile t = GlobalGameState.GetInstance().getCurrentZone().GetContents().getTiles()[position.x][position.y];
        Widget_Breakable widget_Breakable = null;

        AmmoType ammoType = action.GetAmmoType();

        if (t != null && t.GetActorInTile() != null)
        {
            if (!AttackActor(action, actor, t.GetActorInTile(), view, queue, move))
            {
                return false;
            }

            return true;
        }
        if (t != null && t.GetWidget() != null)
        {
            if (t.GetWidget() is WidgetSlot)
            {

                WidgetSlot ws = (WidgetSlot)t.GetWidget();
                if (ws.GetContainedWidget() != null)
                {

                    if (SlotDismantleCheck(ws, actor, move, view))
                    {
                        return true;
                    }
                    widget_Breakable = ws.GetContainedWidget();
                }
            }
            else if (t.GetWidget() is Widget_Breakable)
            {
                widget_Breakable = (Widget_Breakable)t.GetWidget();
            }
        }

        if (widget_Breakable == null)
        {
            return false;
        }
       // Widget_Breakable widget_Breakable = (Widget_Breakable)t.GetWidget();
        int value = 0;
        for (int i = 0; i < move.getEffects().Count; i++)
        {
            value += widget_Breakable.ApplyEffect(move.getEffects()[i],ammoType, actor, diceRoller, view);
        }
        if (value > 0 && widget_Breakable.IsDestroyed())
        {
            widget_Breakable.DestroyWidget(view);
        }
        if (action.GetTarget()==null || action.GetTarget().IsVisible())
        {
            view.DrawText(GenerateHitText(move, widget_Breakable.GetName(), this.actor, value, false));
            if (action.GetTarget() != null)
            {
                action.GetTarget().HandleDelayedText(view);
            }
            switch (move.GetMoveIndicator())
            {
                case MoveIndicator.HIT:
                    view.DrawImpact(action.GetTargetPosition(), 0);
                    break;
                case MoveIndicator.SEDUCE:
                    view.DrawImpact(action.GetTargetPosition(), 1);
                    break;
            }
        }
        return true;
    }

    private void HandleScan(CombatMove move, ActionDoMove action, Vector2Int position, ViewInterface view)
    {
        for (int i = 0; i < move.getEffects().Count; i++)
        {
            if (move.getEffects()[i].GetEffectType() == EffectType.SCAN)
            {
                ScanTool.RunScan((EffectScan)move.getEffects()[i], action, actor, position, view);
            }
        }
    }

    private bool HasScan(CombatMove move)
    {
        for (int i = 0; i < move.getEffects().Count; i++)
        {
            if (move.getEffects()[i].GetEffectType() == EffectType.SCAN)
            {
                return true;
            }
        }
        return false;
    }

    private bool SlotDismantleCheck(WidgetSlot ws, Actor actor, CombatMove move, ViewInterface view)
    {
        if (move.getEffects()[0].GetEffectType().Equals(EffectType.DISMANTLE))
        {
            Debug.Log("slot dismantle check");
            if (ws.GetContainedWidget() is WidgetShipSystem)
            {
                Debug.Log("is ship system");
                WidgetShipSystem wss = (WidgetShipSystem)ws.GetContainedWidget();
                if (wss.IsDamaged())
                {
                    //trash
                    view.DrawText("you have trashed a damaged module trying to extract it");
                    wss.DestroyWidget(view, true);
                    return true;
                }
                else
                {
                    //extract
                    ((Player)actor).GetInventory().AddItem(
                        GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(ws.GetComponent()));
                    ws.SetComponent(null);
                    ws.SetContainedWidget(null);
                    view.DrawText("you have extracted a module");
                    view.CalculateVision();
                    return true;
                }
            }
            else
            {
                //remove normally
                ((Player)actor).GetInventory().AddItem(
                    GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(ws.GetComponent()));
                ws.SetComponent(null);
                ws.SetContainedWidget(null);
                view.CalculateVision();
                view.DrawText("You have extracted a module");
                return true;
            }
        }
        return false;
    }

    private string GenerateMissText(CombatMove move, Actor target, Actor origin)
    {
        int r = move.getMissTexts().Count > 1 ? diceRoller.RollDice(move.getMissTexts().Count) : 0;
        string str = move.getMissTexts()[r];
        return str.Replace("TARGET", target.GetName()).Replace("SOURCE", origin.GetName());
    }
    private string GenerateHitText(CombatMove move, string target, Actor origin, int value, bool critical )
    {
        int r = move.getHitTexts().Count>1 ? diceRoller.RollDice(move.getHitTexts().Count) : 0;
        string str = move.getHitTexts()[r].Replace(
            "TARGET", target).Replace(
            "SOURCE", origin.GetName()).Replace(
            "VALUE", value.ToString());
        return critical ? str+" crit" : str;
    }
    private SK GetDefenceFromSkill(SK sK)
    {
        switch (sK)
        {
            case SK.MELEE:
                return SK.PARRY;
            case SK.RANGED:
                return SK.DODGE;
           


        }
        return SK.WILLPOWER;
    }
}