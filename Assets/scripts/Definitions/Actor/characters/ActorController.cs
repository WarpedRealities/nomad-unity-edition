﻿using UnityEngine;
using System.Collections;
using System;

abstract public class ActorController 
{
    public Actor actor;
    protected ZoneActor zoneActor;
    protected ActionQueue queue;
    protected DoMoveHandler moveHandler;
    protected PatternEligiblity patternEligiblity;
    protected int lastMove=-1;
    public ActorController(Actor actor,ZoneActor zoneActor)
    {
        this.zoneActor = zoneActor;
        queue = new ActionQueue();
        this.actor = actor;
        this.patternEligiblity = new PatternEligiblity(this.zoneActor);
        moveHandler = new DoMoveHandler(zoneActor, patternEligiblity);
    }

    public bool CanAct()
    {
      
        return queue.IsEmpty();
    }

    public virtual bool UpdateLogic(ViewInterface view)
    {

        if (!queue.IsEmpty())
        {
           
            //check if the queue has an action ready to complete
            if (queue.ProcessQueue())
            {
                //handle the action
                Action action = queue.GetAction();
                queue.CompleteAction();
                return ProcessAction(action, view);
            }
        }


        return false;
    }

    public Action GetAction()
    {
        return queue.GetAction();
    }

    public void addAction(Action action)
    {
        queue.AddAction(action);
    }

    protected bool ProcessAction(Action action, ViewInterface view)
    {

        switch (action.GetActionType())
        {
            case ActionType.MOVE:
                return ProcessMove((ActionMove)action, view);
            case ActionType.ATTACK:
                lastMove = -1;
                return moveHandler.ProcessAttack((ActionDoMove)action, view, queue);
            case ActionType.RECOVER:
                lastMove = -1;
                return HandleRecover((ActionRecover)action,view);
            case ActionType.INTERACT:
                return HandleInteract((ActionInteract)action, view);
            case ActionType.INVENTORY:
                return HandleInventory((ActionInventory)action, view);
            case ActionType.CONTAINER:
                return HandleContainer((ActionContainer)action, view);
            case ActionType.RELOAD:
                return HandleReload((ActionReload)action, view);
            case ActionType.SHOVE:
                return HandleShove((ActionShove)action, view);
            case ActionType.STRUGGLE:
                return HandleStruggle((ActionStruggle)action, view);
        }
        return false;
    }

    private bool HandleStruggle(ActionStruggle action, ViewInterface view)
    {
        StatusBind statusBind = actor.GetRPG().GetStatusHandler().GetStatusBind();
        if (statusBind != null)
        {
            int diceroll = DiceRoller.GetInstance().RollDice(20) + actor.GetRPG().getSkill(statusBind.GetResistSkill());
            if (diceroll > statusBind.GetStrength())
            {
                actor.GetRPG().GetStatusHandler().RemoveStatusEffect(statusBind.GetUid(), view, actor);
            }
            else
            {
                view.DrawText(statusBind.GetResistString().Replace("TARGET", actor.GetName()));
            }
        }
        return false;
    }

    private bool HandleShove(ActionShove action, ViewInterface view)
    {

        ZoneActor actor = CollisionTools.GetActorInTile(action.GetPosition(), GlobalGameState.GetInstance().getCurrentZone());
        if (actor!=null)
        {
            //move them to an adjacent tile
            int d = GeometryTools.GetDirection(this.actor.GetPosition().x, this.actor.GetPosition().y, 
                action.GetPosition().x, action.GetPosition().y);

            d = d >= 8 ? d - 8 : d;

            if (CollisionTools.CanEnter(GeometryTools.GetPosition(action.GetPosition().x,
                action.GetPosition().y, d), GlobalGameState.GetInstance().getCurrentZone()))
            {

                actor.actor.SetPosition(GeometryTools.GetPosition(action.GetPosition().x, action.GetPosition().y, d));
                CollisionTools.setActorInTile(action.GetPosition(), null);
                actor.ForceUpdate();
                if (CollisionTools.GetTile(action.GetPosition()).isVisible())
                {
                    view.DrawText(this.actor.GetName() + " has shoved " + actor.actor.GetName());
                }
                return true;
            }
            for (int i = 0; i < 8; i++)
            {
                if (CollisionTools.CanEnter(GeometryTools.GetPosition(action.GetPosition().x,
                    action.GetPosition().y, i),GlobalGameState.GetInstance().getCurrentZone()))
                {
                    actor.actor.SetPosition(GeometryTools.GetPosition(action.GetPosition().x, action.GetPosition().y, i));
                    CollisionTools.setActorInTile(action.GetPosition(), null);
                    actor.ForceUpdate();
                    if (CollisionTools.GetTile(action.GetPosition()).isVisible())
                    {
                        view.DrawText(this.actor.GetName() + " has shoved " + actor.actor.GetName());
                    }
                    return true;
                }
            }
            //swap
            Vector2Int p = actor.actor.GetPosition();
            actor.actor.SetPosition(new Vector2Int(this.actor.GetPosition().x,this.actor.GetPosition().y));
            this.actor.SetPosition(new Vector2Int(p.x, p.y));
            CollisionTools.setActorInTile(actor.actor.GetPosition(), actor);
            CollisionTools.setActorInTile(this.actor.GetPosition(), this.zoneActor);
            this.zoneActor.ForceUpdate();
            actor.ForceUpdate();
            if (CollisionTools.GetTile(action.GetPosition()).isVisible())
            {
                view.DrawText(this.actor.GetName() + " has swapped places with " + actor.actor.GetName());
            }
            return true;
        }
        return true;
    }

    protected virtual bool HandleReload(ActionReload action, ViewInterface view)
    {
        throw new NotImplementedException();
    }

    protected virtual bool HandleContainer(ActionContainer action, ViewInterface view)
    {
        throw new NotImplementedException();
    }

    protected virtual bool HandleInventory(ActionInventory action, ViewInterface view)
    {
        throw new NotImplementedException();
    }

    private bool HandleInteract(ActionInteract action, ViewInterface view)
    {
        float d = Vector2.Distance(this.actor.GetPosition(), action.GetTarget());
        if (d >= 2)
        {
            return false;
        }
        Tile tile = CollisionTools.GetTile(action.GetTarget());
        //find something in that tile to interact with
        if (tile.GetActorInTile() != null)
        {
            if (CollisionTools.SafetyCheck(action.GetTarget())) {
                tile.GetActorInTile().Interact(view, (Player)this.actor);
            }
            else
            {
                view.DrawText("Too dangerous to do that right now");
            }
        }

        //find widget
        if (tile.GetWidget() != null) {

            if (!tile.GetWidget().IsSafeOnly() || (tile.GetWidget().IsSafeOnly() && CollisionTools.SafetyCheck(action.GetTarget()))) {
                tile.GetWidget().Interact(view, action.GetPlayer());
            }
            else
            {
                view.DrawText("Too dangerous to do that right now");
            }
        }

        return false;
    }

    internal void PurgeActions()
    {
        queue.Clear();
    }

    protected virtual bool HandleRecover(ActionRecover actionRecover, ViewInterface view)
    {
        actor.GetRPG().Heal(actionRecover.GetBonus());
        return true;
    }

    private bool ProcessMove(ActionMove action, ViewInterface view)
    {
        //need to move the character
        //check if we can move, if we can't then return false and exit out of this
        
        Vector2Int position = GeometryTools.GetPosition(actor.GetPosition().x, actor.GetPosition().y, action.getDirection());

        if (!CollisionTools.CanEnter(position.x,position.y, GlobalGameState.GetInstance().getCurrentZone(),actor.GetMovementType()))
        {


            return false;
        }
        if (zoneActor.GetController().IsThreatening())
        {
            CollisionTools.RemoveThreat(actor.GetPosition(), zoneActor);
        } 

        CollisionTools.setActorInTile(actor.GetPosition(),null);
        actor.SetPosition(position);
        CollisionTools.setActorInTile(position, zoneActor);
        if (zoneActor.GetController().IsThreatening())
        {
            CollisionTools.SetThreat(position, zoneActor);
        }
        if (actor.IsPlayer())
        {
            view.CalculateVision();
            CollisionTools.Step(position, view);
        }
        lastMove = action.getDirection();
        return true;
    }

    public abstract void AttackOfOpportunity(ZoneActor player);

    public Vector2Int GetEstimatedPosition()
    {
   
        Vector2Int pOffset = lastMove != -1 ? GeometryTools.GetPosition(actor.GetPosition().x, actor.GetPosition().y, lastMove): actor.GetPosition();
        pOffset = GetMoveAction(pOffset);

        return pOffset;
    }

    public int GetMovingTargetEstimate()
    {
        return (lastMove != -1 ? 1 : 0) + (CurrentActionIsMove() ? 1 : 0);
    }

    public bool CanInterrupt(InterruptType interruptType)
    {
        return queue.CanInterrupt(interruptType);
    }

    public bool CurrentActionIsMove()
    {
        return queue.GetAction() != null && queue.GetAction().GetActionType() == ActionType.MOVE;
    }

    public void ReduceQueue(int time)
    {
        queue.GetAction().DecrementDuration(time);
    }

    private Vector2Int GetMoveAction(Vector2Int pOffset)
    {
        if (queue.GetAction() != null && queue.GetAction().GetActionType()==ActionType.MOVE)
        {
            ActionMove actionMove = (ActionMove)queue.GetAction();
            return GeometryTools.GetPosition(pOffset.x, pOffset.y, actionMove.getDirection());
        }
        return pOffset;
    }

    public void HandleDelayedText(ViewInterface view)
    {
        queue.HandleDelayedInterruptText(actor.GetName(), view);
    }

    public void Interrupt(InterruptType interruptType)
    {
        queue.Interrupt(interruptType);
    }

    public virtual bool IsThreatening()
    {
        return false;
    }

    internal void ReadActions()
    {
      //  Debug.Log("action " + queue.ReadAction());
    }

    internal int GetQueueLength()
    {
        return queue.GetLength();
    }

    internal virtual void RunStart(ViewInterface view)
    {

    }
}
