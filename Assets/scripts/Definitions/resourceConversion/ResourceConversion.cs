﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ResourceConversion
{
    private Dictionary<string, float> itemConversions;
    private string resourceID;

    public ResourceConversion(string resourceID)
    {
        this.resourceID = resourceID;
    }

    internal void SetConversions(Dictionary<string, float> conversions)
    {
        itemConversions = conversions;
    }

    public float GetConversion(string item)
    {

        if (!itemConversions.ContainsKey(item))
        {
            return 0;
        }
        return itemConversions[item];
    }
}
