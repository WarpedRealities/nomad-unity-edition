﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;

public class ResourceConversionLibrary 
{
    public Dictionary<string, ResourceConversion> resourceConversions;

    public ResourceConversionLibrary()
    {
        resourceConversions = new Dictionary<string, ResourceConversion>();
    }

    public ResourceConversion GetConversion(string resource)
    {
        if (!resourceConversions.ContainsKey(resource))
        {
            resourceConversions.Add(resource, BuildConversion(resource));
        }
        return resourceConversions[resource];
    }

    private ResourceConversion BuildConversion(string resource)
    {
        ResourceConversion resourceConversion = new ResourceConversion(resource);
        Dictionary<string, float> conversions = new Dictionary<string, float>();

        XmlDocument document = FileTools.GetXmlDocument("resourceConversions/"+resource);
        if (document != null)
        {
            XmlNodeList children = document.FirstChild.NextSibling.ChildNodes;
            for (int i = 0; i < children.Count; i++)
            {
                if (children[i].NodeType == XmlNodeType.Element)
                {
                    XmlElement e = (XmlElement)children[i];
                    if ("conversion".Equals(e.Name))
                    {
                        conversions.Add(e.GetAttribute("item"), float.Parse(e.GetAttribute("value")));
                    }
                }
            }
        }
        resourceConversion.SetConversions(conversions);
        return resourceConversion;
    }
}
