﻿using UnityEngine;
using System.Collections;

public class MacroRange 
{
    int lowerBound, upperBound;
    string output;

    public MacroRange(int lower, int higher, string output)
    {
        this.lowerBound = lower;
        this.upperBound = higher;
        this.output = output;
    }

    public int GetUpperBound()
    {
        return upperBound;
    }

    public int GetLowerBound()
    {
        return lowerBound;
    }

    public string GetOutput()
    {
        return output;
    }
}
