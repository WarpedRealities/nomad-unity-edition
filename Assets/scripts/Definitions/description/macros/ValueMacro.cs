﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ValueMacro : DescriptionMacro
{
    private string macroID, part, value;
    private Dictionary<int, string> translations;
    public ValueMacro(string macroID, string part, string value)
    {
        this.macroID = macroID;
        this.part = part;
        this.value = value;
        translations = new Dictionary<int, string>();
    }

    public string GetMacroValue(Appearance appearance)
    {
        int v = appearance.GetPart(part).GetValue(value);
        return translations[v];
    }

    internal void AddValue(int key, string value)
    {
        translations.Add(key, value);
    }
}
