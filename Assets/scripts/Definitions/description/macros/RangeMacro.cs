﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RangeMacro : DescriptionMacro
{
    List<MacroRange> ranges;
    private string macroID, part, value;

    public RangeMacro(string macroID, string part, string value)
    {
        this.macroID = macroID;
        this.part = part;
        this.value = value;
        ranges = new List<MacroRange>();
    }

    public string GetMacroValue(Appearance appearance)
    {

        int v = appearance.GetPart(part).GetValue(value);
        for (int i = 0; i < ranges.Count; i++)
        {
            if (v >= ranges[i].GetLowerBound() && v <= ranges[i].GetUpperBound())
            {
                return ranges[i].GetOutput();
            }
        }
        return "";
    }

    internal void AddRange(MacroRange macroRange)
    {
        ranges.Add(macroRange);
    }
}
