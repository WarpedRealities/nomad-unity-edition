﻿using UnityEngine;
using System.Collections;

public interface DescriptionMacro 
{

    string GetMacroValue(Appearance appearance);
}
