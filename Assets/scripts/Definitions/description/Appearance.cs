﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Appearance {

    Dictionary<string, BodyPart> bodyParts;
  
    public Appearance()
    {
        bodyParts = new Dictionary<string, BodyPart>();
    }

    public BodyPart GetPart(string name)
    {
        if (bodyParts.ContainsKey(name))
        {
            return bodyParts[name];
        }
        return null;
    }

    internal void AddPart(BodyPart bodyPart)
    {
        bodyParts[bodyPart.GetName()] = bodyPart;
    }

    public void RemovePart(string name)
    {
        bodyParts.Remove(name);
    }
}
