﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BodyPart 
{
    Dictionary<string, int> partValues;
    string name;

    public BodyPart(string name)
    {
        this.name = name;
        partValues = new Dictionary<string, int>();
    }

    public string GetName()
    {
        return name;
    }

    public int GetValue(string valueName)
    {
        return partValues[valueName];
    }

    public void SetValue(string valueName, int value)
    {
        partValues[valueName] = value;
    }

}
