﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class UIDGenerator 
{
    private long ShipID;
    private long NpcID;
    public UIDGenerator()
    {
        NpcID = 0;
        ShipID = 0;
    }

    public long GetShipID()
    {
        long uid = ShipID;
        ShipID++;
        return uid;
    }
    public long GetNpcID()
    {
        long uid = NpcID;
        NpcID++;
        return uid;
    }
    public long ReadShipID()
    {
        return ShipID;
    }
    public long ReadNpcID()
    {
        return NpcID;
    }
    internal void SetShipID(long id)
    {
        ShipID = id;
    }
    internal void SetNpcID(long id)
    {
        NpcID = id;
    }
}
