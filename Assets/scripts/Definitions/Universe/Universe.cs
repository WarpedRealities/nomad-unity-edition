﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Universe 
{
    private List<StarSystem> starsystems;
    private UIDGenerator uIDGenerator;
    private TimeKeeper timeKeeper;
    private FactionLibrary factionLibrary;
    private FactionRuleLibrary factionRuleLibrary;
    private ItemLibrary itemLibrary;
    private PerkLibrary perkLibrary;
    private ShopLibrary shopLibrary;
    private CraftingLibrary craftingLibrary;
    private ResourceConversionLibrary conversionLibrary;
    private Preferences preferences;
    private long universalClock = 1;

    public Universe()
    {
        uIDGenerator = new UIDGenerator();
        timeKeeper = new TimeKeeper();
        factionLibrary = new FactionLibrary();
        factionRuleLibrary = new FactionRuleLibrary();
        itemLibrary = new ItemLibrary();
        perkLibrary = new PerkLibrary();
        shopLibrary = new ShopLibrary();
        craftingLibrary = new CraftingLibrary();
        conversionLibrary = new ResourceConversionLibrary();
        preferences = new Preferences();
    }

    internal TimeKeeper GetTimeKeeper()
    {
        return timeKeeper;
    }

    internal Preferences GetPreferences()
    {
        return preferences;
    }

    public PerkLibrary GetPerkLibrary()
    {
        return perkLibrary;
    }

    internal void SetTimeKeeper(TimeKeeper timeKeeper)
    {
        this.timeKeeper = timeKeeper;
    }

    public CraftingLibrary GetCraftingLibrary()
    {
        return craftingLibrary;
    }

    internal void SetUID(UIDGenerator uIDGenerator)
    {
        this.uIDGenerator = uIDGenerator;
    }

    public ShopLibrary GetShopLibrary()
    {
        return shopLibrary;
    }

    public void SetShopLibrary(ShopLibrary shopLibrary)
    {
        this.shopLibrary = shopLibrary;
    }

    internal void SetFactionLibrary(FactionLibrary factionLibrary)
    {
        this.factionLibrary = factionLibrary;
    }

    internal void SetClock(long clock)
    {
        this.universalClock = clock;
    }

    internal void SetRuleLibrary(FactionRuleLibrary factionRuleLibrary)
    {
        this.factionRuleLibrary = factionRuleLibrary;
    }

    public ResourceConversionLibrary GetResourceConversionLibrary()
    {
        return conversionLibrary;
    }

    public void setStarSystems(List<StarSystem> systems)
    {
        this.starsystems = systems;
    }

    public List<StarSystem> GetSystems()
    {
        return starsystems;
    }
   
    public StarSystem GetSystem(string systemName)
    {
        for (int i = 0; i < this.starsystems.Count; i++)
        {
            if (this.starsystems[i].GetName().Equals(systemName))
            {
                return this.starsystems[i];
            }
        }
        return null; ;
    }

    public void ResetClock()
    {
        universalClock = 0;
    }

    public UIDGenerator GetUIDGenerator()
    {
        return uIDGenerator;
    }

    public FactionLibrary getFactionLibrary()
    {
        return factionLibrary;
    }

    public void IncrementClock(int amount=1)
    {
        universalClock+=amount;
    }

    public ItemLibrary GetItemLibrary()
    {
        return itemLibrary;
    }

    public long GetClock()
    {
        return universalClock;
    }

    public FactionRuleLibrary GetFactionRuleLibrary()
    {
        return factionRuleLibrary;
    }

    public UIDGenerator GetUIDGen()
    {
        return uIDGenerator;
    }

    internal StarSystem GetSystem(Vector2Int target)
    {
        for (int i = 0; i < this.starsystems.Count; i++)
        {
            if (this.starsystems[i].GetPosition().Equals(target))
            {
                return this.starsystems[i];
            }
        }
        return null;

    }

    internal void SetCraftingLibrary(CraftingLibrary craftingLibrary)
    {
        this.craftingLibrary = craftingLibrary;
        if (craftingLibrary == null)
        {
            craftingLibrary = new CraftingLibrary();
        }
    }
}
