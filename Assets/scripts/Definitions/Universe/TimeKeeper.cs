﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class TimeKeeper 
{
    Dictionary<String, long> timeStamp;

    public TimeKeeper()
    {
        timeStamp = new Dictionary<string, long>();
    }

    public void SetTimeStamp(string key, long value)
    {
        timeStamp[key] = value;
    }
    
    public bool HasTimeStamp(string key)
    {
        return timeStamp.ContainsKey(key);
    }

    public long GetTimeStamp(string key)
    {
        return timeStamp[key];
    }
}
