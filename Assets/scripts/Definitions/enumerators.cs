﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ENTITYTYPE {  STAR, PLANET, STATION, SHIP, CREATURE,GROUP, SIGNATURE, HANDLER };
public enum ShipState { SPACE, WARP, LANDED, DOCKED,UNUSABLE, DISTRESS
}
public enum ControlMode { NORMAL =0, INTERACT =2, LOOK =1, ATTACK =3};
public enum TileVision { EMPTY, TRANSPARENT, BLOCK };
public enum TileMovement {  WALK=0, SLOW=-1, SWIM=1, FLY=2, BLOCK=3};
public enum ZoneType { SURFACE, CLOSED, LIMITED };
public enum MoveIndicator { NONE=-1, HIT=0, SEDUCE=1};
public enum ActionType { NONE, MOVE,ATTACK,RECOVER, INTERACT, INVENTORY, CONTAINER, RELOAD, SHOVE, STRUGGLE, IMPRISON };
public enum AttackPattern {NONE=-1, SELF=0,MELEE=1, RANGED=2, BLAST=3, DIRECT=4};
public enum MoveType { FIGHT=0, DOMINATE=1, MOVEMENT=2, OTHER=3 };
public enum TrapType { DOORS, PROJECTILE, DIRECT };
public enum InterruptType { UNDEFINED=-1, DAMAGE=0, MELEE = 1, DISTRACT = 2, STAGGER =3, NONE = 4};
public enum EffectType { DAMAGE, RECOVER, MUTATOR, PERK, STATUS, INTERRUPT,DISMANTLE,SUBMIT, MOVE, SUMMON, STATUSREMOVE, SCAN, ITEM, KARMA, ANALYSE};
public enum ItemType { TRASH, RESOURCE, CONSUMABLE, COMPONENT, EQUIP, WEAPON, AMMO, CURRENCY, EXPOSITION, KEY,QUEST, CAPTURE, BLUEPRINT };
public enum ItemClass { REGULAR, STACK, AMMOSTACK, QUEST, EXPOSITION, KEY};
public enum ViolationType {  ATTACK=0,SEDUCE=1,INTERACT=2,DEFEAT=3,DOMINATE=4,FORCEVORE=5,VORE=6};
public enum AB { STRENGTH=0, DEXTERITY=1, AGILITY=2, ENDURANCE=3, INTELLIGENCE=4, CHARISMA=5, NONE};
public enum ScreenID { NONE, INVENTORY, GAMEOVER,MOVESELECT,APPEARANCE, CRAFTING,SPACESHIP, RESEARCH, HELP };
public enum ScreenDataType { NONE,CONTAINER, CONVERSATION,GAMEOVER,SLOT,SHIPSYSTEM,SHOP, JAIL, QUICKSLOT};
public enum InventoryAction { USE, NONE, EQUIP,UNEQUIP, EXAMINE,DROP,RELOAD,UNLOAD,SPLIT,TAKE,CANCEL, DROPALL};
public enum EquipSlot { NONE =-1, HANDS=0, TORSO=1, LEGS=2, ARMS=3, FEET= 4, HEAD=5, ACCESSORY=6 };
public enum ItemUse { NONE,USE,EQUIP};
public enum PerkEffectType { ABILITYMOD,SKILLMOD,STATMOD,DEFENCEMOD,MOVE,MOVEMOD,MOVEADD, SECONDARYMOD};
public enum SHOPTYPE { ITEM=0,SERVICE=1,MUTATOR=2,SHIP=3,SLAVES=4 };
public enum SLOTTYPE { NORMAL=0, DRIVE=1, SUPPORT = 2, WEAPON =3};
public enum SHIPWEAPONSIZE { LIGHT =0, MEDIUM=1, HEAVY=2};
public enum SHIPSYSTEMTYPE {INVALID=-1, RESOURCE=0, MAGAZINE = 1,FILLER=2,CONVERTER=3, WEAPON=4, MODIFIER=5};
public enum FIRINGARC {FIXED=-2, NARROW=-1,NORMAL=0, WIDE=1, CIRCLE=2};
public enum LootQualifier { NONE, UNIQUE, PLACED};
public enum conversationType { DEFEAT=0,SEDUCED=1,VICTORY=2,DOMINANT=3,TALK=4,CAPTIVE=5};
public enum ST {HEALTH=0, RESOLVE=1, SATIATION=2, ACTION=3 };
public enum MoveSelectState { FIRST=-1,FIGHT=0,DOMINATE=1,MOVEMENT=2,OTHER=3};
public enum LANDINGOPTIONSTATE { ACCESSIBLE, UNEXPLORED, TAKEN};
public enum SK {MELEE=0,RANGED=1,SEDUCTION=2, PARRY=3,DODGE=4,WILLPOWER=5, STRUGGLE=6, PLEASURE=7, PERSUADE=8, PERCEPTION=13, TECH=9, SCIENCE=10, PILOTING=11, GUNNERY=12};

public enum DR {NONE=-1, KINETIC =0, THERMAL = 1, SHOCK = 2,  TEASE =3, PHEROMONE = 4, PSI =5,  POISE =6, EGO =7, METABOLISM =8, SYSTEM = 9};

public enum DT { NORMAL, HULL, SHIELD, ENERGY,HEAT};

public enum SV {NONE = -1, SPEED = 0, MOVECOST = 1, REGENTHRESHOLD = 2, REGENRATE = 3, DECAY = 4,CARRY = 5, POISEBONUS = 6,EGOBONUS = 7,METABOLISMBONUS = 8};

public enum CharacterTab {ABILITY, SKILLS,DEFENCERESIST,PERKS};

public enum ConfirmState { NONE, NEWGAME,EXIT};
public enum WeaponState { ENABLED, DISABLED, CONFIRMED, UNSELECTED, INCAPABLE };

public enum WarpState {DRAFT, CHARGING, JUMPOUT, TRANSIT, JUMPIN, FAILED};

public class EnumTools
{

    public static MoveType strToMoveType(string str)
    {
        if (str == null || "".Equals(str))
        {
            return MoveType.OTHER;
        }
        if ("FIGHT".Equals(str))
        {
            return MoveType.FIGHT;
        }
        if ("DOMINATE".Equals(str))
        {
            return MoveType.DOMINATE;
        }
        if ("MOVEMENT".Equals(str))
        {
            return MoveType.MOVEMENT;
        }
        return MoveType.OTHER;
    }

    public static TrapType strToTrapType(string str)
    {
        if ("DOOR".Equals(str))
        {
            return TrapType.DOORS;
        }
        if ("PROJECTILE".Equals(str))
        {
            return TrapType.PROJECTILE;
        }
        return TrapType.DIRECT;
    }

    public static SHIPSYSTEMTYPE StrToShipSystemType(string str)
    {
        if ("RESOURCE".Equals(str))
        {
            return SHIPSYSTEMTYPE.RESOURCE;
        }
        if ("FILLER".Equals(str))
        {
            return SHIPSYSTEMTYPE.FILLER;
        }
        if ("MAGAZINE".Equals(str))
        {
            return SHIPSYSTEMTYPE.MAGAZINE;
        }
        if ("WEAPON".Equals(str))
        {
            return SHIPSYSTEMTYPE.WEAPON;
        }
        return SHIPSYSTEMTYPE.INVALID;
    }

    public static EquipSlot StrToEquip(string str)
    {
        if ("HANDS".Equals(str))
        {
            return EquipSlot.HANDS;
        }
        if ("TORSO".Equals(str))
        {
            return EquipSlot.TORSO;
        }
        if ("LEGS".Equals(str))
        {
            return EquipSlot.LEGS;
        }
        if ("ARMS".Equals(str))
        {
            return EquipSlot.ARMS;
        }
        if ("FEET".Equals(str))
        {
            return EquipSlot.FEET;
        }
        if ("ACCESSORY".Equals(str))
        {
            return EquipSlot.ACCESSORY;
        }
        if ("HEAD".Equals(str))
        {
            return EquipSlot.HEAD;
        }
        return EquipSlot.NONE;
    }

    public static SHIPWEAPONSIZE strToWeaponSize(string str)
    {
        if ("LIGHT".Equals(str))
        {
            return SHIPWEAPONSIZE.LIGHT;
        }
        if ("MEDIUM".Equals(str))
        {
            return SHIPWEAPONSIZE.MEDIUM;
        }
        if ("HEAVY".Equals(str))
        {
            return SHIPWEAPONSIZE.HEAVY;
        }
        return SHIPWEAPONSIZE.LIGHT;
    }

    public static FIRINGARC strToFiringArc(string str)
    {
        if ("FIXED".Equals(str))
        {
            return FIRINGARC.FIXED;
        }
        if ("NARROW".Equals(str))
        {
            return FIRINGARC.NARROW;
        }
        if ("NORMAL".Equals(str))
        {
            return FIRINGARC.NORMAL;
        }
        if ("WIDE".Equals(str))
        {
            return FIRINGARC.WIDE;
        }
        if ("CIRCLE".Equals(str))
        {
            return FIRINGARC.CIRCLE;
        }
        return FIRINGARC.NORMAL;
    }

    public static AttackPattern StrToAttackPattern(string str)
    {
        if ("SELF".Equals(str))
        {
            return AttackPattern.SELF;
        }
        if ("MELEE".Equals(str))
        {
            return AttackPattern.MELEE;
        }
        if ("RANGED".Equals(str))
        {
            return AttackPattern.RANGED;
        }
        if ("BLAST".Equals(str))
        {
            return AttackPattern.BLAST;
        }
        if ("DIRECT".Equals(str))
        {
            return AttackPattern.DIRECT;
        }
        return AttackPattern.SELF;
    }

    public static InterruptType strToInterruptType(string str)
    {
        if ("MELEE".Equals(str))
        {
            return InterruptType.MELEE;
        }

        if ("STAGGER".Equals(str))
        {
            return InterruptType.STAGGER;
        }
        if ("DISTRACT".Equals(str))
        {
            return InterruptType.DISTRACT;
        }
        if ("DAMAGE".Equals(str))
        {
            return InterruptType.DAMAGE;
        }
        return InterruptType.NONE;
    }

    public static TileVision StrToVision(string str)
    {
        if ("TRANSPARENT".Equals(str))
        {
            return TileVision.TRANSPARENT;
        }
        if ("BLOCKING".Equals(str))
        {
            return TileVision.BLOCK;
        }
        return TileVision.EMPTY;
    }
    public static TileMovement StrToMovement(string str)
    {
        if ("WALK".Equals(str))
        {
            return TileMovement.WALK;
        }
        if ("SLOW".Equals(str))
        {
            return TileMovement.SLOW;
        }
        if ("SWIM".Equals(str))
        {
            return TileMovement.SWIM;
        }
        if ("FLY".Equals(str))
        {
            return TileMovement.FLY;
        }
        return TileMovement.BLOCK;
    }

    public static ZoneType strToZoneType(string str)
    {
        if ("CLOSED".Equals(str))
        {
            return ZoneType.CLOSED;
        }
        if ("LIMITED".Equals(str))
        {
            return ZoneType.LIMITED;
        }

        return ZoneType.SURFACE;
    }

    public static SK strToSkill(string str)
    {

        if ("MELEE".Equals(str))
        {
            return SK.MELEE;
        }
        if ("RANGED".Equals(str))
        {
            return SK.RANGED;
        }
        if ("SEDUCTION".Equals(str))
        {
            return SK.SEDUCTION;
        }
        if ("PARRY".Equals(str))
        {
            return SK.PARRY;
        }
        if ("DODGE".Equals(str))
        {
            return SK.DODGE;
        }
        if ("WILLPOWER".Equals(str))
        {
            return SK.WILLPOWER;
        }
        if ("STRUGGLE".Equals(str))
        {
            return SK.STRUGGLE;
        }
        if ("PLEASURE".Equals(str))
        {
            return SK.PLEASURE;
        }
        if ("PERSUASION".Equals(str))
        {
            return SK.PERSUADE;
        }
        if ("TECH".Equals(str))
        {
            return SK.TECH;
        }
        if ("SCIENCE".Equals(str))
        {
            return SK.SCIENCE;
        }
        if ("GUNNERY".Equals(str))
        {
            return SK.GUNNERY;
        }
        if ("PILOTING".Equals(str))
        {
            return SK.PILOTING;
        }
        if ("PERCEPTION".Equals(str))
        {
            return SK.PERCEPTION;
        }

        return SK.MELEE;
    }

    public static SLOTTYPE strToSlotType(string str)
    {
        if ("DRIVE".Equals(str))
        {
            return SLOTTYPE.DRIVE;
        }
        if ("WEAPON".Equals(str))
        {
            return SLOTTYPE.WEAPON;
        }
        if ("SUPPORT".Equals(str))
        {
            return SLOTTYPE.SUPPORT;
        }

        return SLOTTYPE.NORMAL;
    }

    public static DR strToDefenceResist(string str)
    {
        if ("NONE".Equals(str))
        {
            return DR.NONE;
        }

        if ("KINETIC".Equals(str))
        {
            return DR.KINETIC;
        }
        if ("THERMAL".Equals(str))
        {
            return DR.THERMAL;
        }
        if ("SHOCK".Equals(str))
        {
            return DR.SHOCK;
        }
        if ("TEASE".Equals(str))
        {
            return DR.TEASE;
        }
        if ("PHEROMONE".Equals(str))
        {
            return DR.PHEROMONE;
        }
        if ("PSI".Equals(str))
        {
            return DR.PSI;
        }
        if ("POISE".Equals(str))
        {
            return DR.POISE;
        }
        if ("EGO".Equals(str))
        {
            return DR.EGO;
        }
        if ("METABOLISM".Equals(str))
        {
            return DR.METABOLISM;
        }
        if ("SYSTEM".Equals(str))
        {
            return DR.SYSTEM;
        }

        return DR.NONE;
    }

    public static SV strToSecondaryValue(string str)
    {
        if ("SPEED".Equals(str))
        {
            return SV.SPEED;
        }
        if ("MOVECOST".Equals(str))
        {
            return SV.MOVECOST;
        }
        if ("REGENTHRESHOLD".Equals(str))
        {
            return SV.REGENTHRESHOLD;
        }
        if ("REGENRATE".Equals(str))
        {
            return SV.REGENRATE;
        }
        if ("DECAY".Equals(str))
        {
            return SV.DECAY;
        }
        if ("CARRY".Equals(str))
        {
            return SV.CARRY;
        }
        if ("POISEBONUS".Equals(str))
        {
            return SV.POISEBONUS;
        }
        if ("EGOBONUS".Equals(str))
        {
            return SV.EGOBONUS;
        }
        if ("METABOLISMBONUS".Equals(str))
        {
            return SV.METABOLISMBONUS;
        }
        return SV.NONE;
    }
    public static DT StrToDamageType(string str)
    {
        if ("NORMAL".Equals(str))
        {
            return DT.NORMAL;
        }
        if ("HEAT".Equals(str))
        {
            return DT.HEAT;
        }
        if ("SHIELD".Equals(str))
        {
            return DT.SHIELD;
        }
        if ("ENERGY".Equals(str))
        {
            return DT.ENERGY;
        }
        if ("HULL".Equals(str))
        {
            return DT.HULL;
        }
        return DT.NORMAL;
    }

    public static AB StrToAbility(string str)
    {
        if ("STRENGTH".Equals(str))
        {
            return AB.STRENGTH;
        }
        if ("DEXTERITY".Equals(str))
        {
            return AB.DEXTERITY;
        }
        if ("ENDURANCE".Equals(str))
        {
            return AB.ENDURANCE;
        }
        if ("AGILITY".Equals(str))
        {
            return AB.AGILITY;
        }
        if ("INTELLIGENCE".Equals(str))
        {
            return AB.INTELLIGENCE;
        }
        if ("CHARISMA".Equals(str))
        {
            return AB.CHARISMA;
        }
        return AB.NONE;

    }

    public static ST strToStat(string str)
    {
        if ("HEALTH".Equals(str))
        {
            return ST.HEALTH;
        }
        if ("RESOLVE".Equals(str))
        {
            return ST.RESOLVE;
        }
        if ("ACTION".Equals(str))
        {
            return ST.ACTION;
        }
        if ("SATIATION".Equals(str))
        {
            return ST.SATIATION;
        }
        return ST.HEALTH;
    }

    public static ViolationType strToViolationType(string str)
    {
        if ("ATTACK".Equals(str))
        {
            return ViolationType.ATTACK;
        }
        if ("SEDUCE".Equals(str))
        {
            return ViolationType.SEDUCE;
        }
        if ("INTERACT".Equals(str))
        {
            return ViolationType.INTERACT;
        }
        if ("DEFEAT".Equals(str))
        {
            return ViolationType.DEFEAT;
        }
        if ("DOMINATE".Equals(str))
        {
            return ViolationType.DOMINATE;
        }
        if ("FORCEVORE".Equals(str))
        {
            return ViolationType.FORCEVORE;
        }
        if ("VORE".Equals(str))
        {
            return ViolationType.VORE;
        }

        return ViolationType.ATTACK;
    }

    internal static string FacingToStr(int facing)
    {
        switch (facing)
        {
            case 0:
                return "fore";
            case 1:
                return "fore starboard";
            case 2:
                return "starboard";
            case 3:
                return "aft starboard";
            case 4:
                return "aft";
            case 5:
                return "aft port";
            case 6:
                return "port";

            case 7:
                return "fore port";

        }
        return "????";
    }

    internal static string ArcToStr(FIRINGARC arc)
    {
        switch (arc)
        {
            case FIRINGARC.NARROW:
                return "narrow";
            case FIRINGARC.NORMAL:
                return "normal";
            case FIRINGARC.WIDE:
                return "wide";
            case FIRINGARC.CIRCLE:
                return "circle";

        }
        return "????";

    }

    internal static int strToSide(string value)
    {
        if ("north".Equals(value))
        {
            return 0;
        }
        if ("east".Equals(value))
        {
            return 1;
        }
        if ("south".Equals(value))
        {
            return 2;
        }
        if ("west".Equals(value))
        {
            return 3;
        }
        return -1;

    }
}

public class StringConstants
{
    public static readonly string ICONPREFX = "statusEffectIndicators_";
    public static readonly string ICONS = "sprites/StatusEffectIndicators";
    public static readonly string QUICKICONS = "sprites/quickbar_icons";
    public static readonly string QUICKPREFIX = "quickbar_icons_";
}

public class NumericConstants
{
    public static readonly float BATTLE_SPEED = 2.0F;
    public static readonly float BATTLE_TURN = 0.5F;
    public static readonly float MANOUVER_MULTI = 0.25F;
    public static readonly float CAMERA_SPEED = 2.0F;
}