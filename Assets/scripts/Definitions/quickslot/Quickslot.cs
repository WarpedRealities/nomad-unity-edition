using System;

[Serializable]
public class Quickslot
{
    public string target;
    public string displayName;
    public bool isMove;
    public int icon;
    public bool disabled;
    public Quickslot(string target, string display, bool isMove, int icon)
    {
        this.target = target;
        this.displayName = display;
        this.isMove = isMove;
        this.icon = icon;
    }

};