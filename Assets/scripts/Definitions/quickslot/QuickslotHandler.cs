using System;
using System.Collections.Generic;

[Serializable]
public class QuickslotHandler
{
    Quickslot[] quickslots; 


    public QuickslotHandler()
    {
        quickslots=new Quickslot[6];

        quickslots[0] = new Quickslot("seduce", "seduce", true, 24);
        quickslots[1] = new Quickslot("00MEDKIT","medkit", false, 4);

    }

    public Quickslot GetSlot(int i)
    {
        return quickslots[i];
    }

    public void SetSlot(int index, Quickslot quickslot)
    {
        this.quickslots[index] = quickslot;
    }

    public void UpdateQuickSlots(Inventory inventory, List<CombatMove>[] combatMoves)
    {
        for (int i=0;i<quickslots.Length;i++)
        {
            if (quickslots[i] != null)
            {
                UpdateQuickSlot(quickslots[i],inventory,combatMoves);
            }
        }
    }

    private void UpdateQuickSlot(Quickslot quickslot, Inventory inventory, List<CombatMove>[] combatMoves)
    {
        if (quickslot.isMove)
        {
            UpdateMoveQuickslot(quickslot, combatMoves);
        }
        else
        {
            UpdateItemQuickslot(quickslot, inventory);
        }

    }

    private void UpdateItemQuickslot(Quickslot quickslot, Inventory inventory)
    {
        quickslot.disabled = true;
        //check equip slots
        for (int i = 0; i < 4; i++)
        {
            if (inventory.GetSlot(i)!=null && 
                inventory.GetSlot(i).GetDef().GetCodeName().Equals(quickslot.target)) {
                quickslot.disabled = false;
                return;
            }
        }
        //check inventory
        for (int i = 0; i < inventory.GetItemCount(); i++)
        {
            if (inventory.GetItem(i).GetDef().GetCodeName().Equals(quickslot.target))
            {
                quickslot.disabled = false;
                return;
            }
        }

    }

    private void UpdateMoveQuickslot(Quickslot quickslot, List<CombatMove>[] combatMoves)
    {
        quickslot.disabled = true;
        for (int i = 0; i < combatMoves.Length; i++)
        {
            for (int j = 0; j < combatMoves[i].Count; j++)
            {
                if (combatMoves[i][j].GetName().Equals(quickslot.target))
                {
                    quickslot.disabled = false;
                    return;
                }
            }
        }
    }
};