﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarSpriteRepo 
{
    private static SolarSpriteRepo instance;

    public static SolarSpriteRepo GetInstance()
    {
        if (instance == null)
        {
            instance = new SolarSpriteRepo();
        }
        return instance;
    }

    Dictionary<string, Sprite> sprites;

    public SolarSpriteRepo()
    {
        sprites = new Dictionary<string, Sprite>();
    }

    public Sprite GetSprite(string filename)
    {

        Sprite sprite = null;
        if (sprites.TryGetValue(filename, out sprite))
        {
            return sprite;
        }
        else
        {

            Texture2D texture = TextureManagementTools.loadTexture("gameData/art/space/"+filename+".png");
            sprite = Sprite.Create(texture, 
                new Rect(new Vector2(0, 0), 
                new Vector2(texture.width, texture.height)),
                new Vector2(0.5F,0.5F),32);
            sprites.Add(filename, sprite);
            return sprite;
        }
    }

}
