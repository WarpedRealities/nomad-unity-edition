﻿using UnityEngine;
using System.Collections;
using System;
using MoonSharp.Interpreter;

public class SolarScriptRunnerTool
{
    private StarSystem system;
    private LuaSolarSensor sensor;
    public SolarScriptRunnerTool(StarSystem system)
    {
        this.system = system;
        sensor = new LuaSolarSensor(system);
    }

    internal void RunScript(string scriptName, Active_Entity active_Entity)
    {
        Debug.Log("running script name " + scriptName);
        Script script;
        DynValue mainFunction;
        object[] arguments;
        script = new Script();
        String fileContents = FileTools.GetFileRaw("controllers/scripts/" + scriptName + ".lua");
        script.DoString(fileContents);
        mainFunction = script.Globals.Get("main");
        arguments = new object[2];
        arguments[0] = active_Entity.GetController().GetControllable();
        arguments[1] = sensor;
        DynValue rValue = script.Call(mainFunction, arguments);
    }
}
