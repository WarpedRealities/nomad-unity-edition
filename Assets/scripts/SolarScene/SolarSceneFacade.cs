﻿using UnityEngine;
using System.Collections;

public class SolarSceneFacade : ViewInterface
{
    SolarSceneScript solar;

    public SolarSceneFacade(SolarSceneScript solar)
    {
        this.solar = solar;
    }


    public void AddNPC(Actor actor)
    {
        throw new System.NotImplementedException();
    }

    public void CalculateVision()
    {
        throw new System.NotImplementedException();
    }

    public void DrawImpact(Vector2Int position, int type)
    {
        throw new System.NotImplementedException();
    }

    public void DrawImpact(Vector2 position, int type)
    {
        throw new System.NotImplementedException();
    }

    public void DrawNumber(Vector2Int origin, int number, int type)
    {
        throw new System.NotImplementedException();
    }

    public void DrawRanged(Vector2Int origin, Vector2Int target, int type)
    {
        throw new System.NotImplementedException();
    }

    public void DrawText(string str)
    {
        throw new System.NotImplementedException();
    }

    public ZoneActor GetPlayerInZone()
    {
        throw new System.NotImplementedException();
    }

    public RegenerationTicker GetRegenerationTicker()
    {
        throw new System.NotImplementedException();
    }

    public LuaSensor GetSensor()
    {
        throw new System.NotImplementedException();
    }

    public Vector2Int GetWidgetPosition(Widget widget)
    {
        throw new System.NotImplementedException();
    }

    public void Redraw()
    {
        throw new System.NotImplementedException();
    }

    public void RemoveWidget(Widget widget)
    {
        throw new System.NotImplementedException();
    }

    public void ReplaceWidget(Widget widgetRemove, Widget widgetReplace)
    {
        throw new System.NotImplementedException();
    }

    public void SetScreen(ScreenID screen)
    {
        switch (screen)
        {
            case ScreenID.NONE:
                solar.SetScreen(null);
                break;
        }
       
    }

    public void SetScreen(ScreenData screenData)
    {
        throw new System.NotImplementedException();
    }

    public void UpdateCamera()
    {
        throw new System.NotImplementedException();
    }

    public void ZoneTransition(string destination, int identifier)
    {
        throw new System.NotImplementedException();
    }

    public void ZoneTransition(int side, string destination)
    {
        throw new System.NotImplementedException();
    }
}
