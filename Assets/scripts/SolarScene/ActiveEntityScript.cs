﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void RedrawDelegate();

public class ActiveEntityScript : EntityScript
{
    public ProcessManager processManager;
    public ActiveEntityController entityController;
    private RedrawDelegate redrawDelegate;
    // Start is called before the first frame update
    void Start()
    {

        spriteRenderer = GetComponent<SpriteRenderer>();
        if (this.entity is Spaceship)
        {
            processManager = new SpaceshipProcessManager((Spaceship)this.entity);
        }
    }

    public RedrawDelegate GetDelegate()
    {
        if (redrawDelegate == null)
        {
            redrawDelegate = Redraw;
        }
        return redrawDelegate;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    virtual public void UpdateLogic()
    {
        if (entityController != null)
        {
            if (!entityController.isBusy())
            {
                entityController.TakeAction();
            }
            else
            {
                entityController.Update(10);
                if (processManager != null)
                {
                    processManager.RunProcess(10);
                }
            }
        }

        //need to have some kind of tool to run the project
    }

    public void Redraw()
    {
        transform.position = new Vector3(entity.GetPosition().x, entity.GetPosition().y, 0);
        transform.eulerAngles = new Vector3(0, 0, -entity.getRotation());
    }

}
