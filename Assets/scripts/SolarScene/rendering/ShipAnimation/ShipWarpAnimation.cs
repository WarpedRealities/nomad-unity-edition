using System;
using UnityEngine;

[Serializable]
public class ShipWarpAnimation : ShipAnimation
{
    private WarpHandler warpHandler;
    readonly float VELOCITY = 10;
    readonly float DURATION = 3;
    private Vector2 vector;
    public ShipWarpAnimation(WarpHandler warpHandler)
    {
        clock = DURATION;
        this.warpHandler = warpHandler;
        vector = GeometryTools.GetPosition(0, 0, this.warpHandler.direction);
        vector = vector.normalized;
    }

    public override void Update(ActiveEntityScript entity, SolarSceneScript solarSceneScript)
    {

        if (warpHandler.state == WarpState.JUMPOUT)
        {
            entity.transform.position = new Vector3(entity.transform.position.x + (vector.x * VELOCITY * Time.deltaTime),
                entity.transform.position.y + (vector.y * VELOCITY * Time.deltaTime), 0);
        }
        if (warpHandler.state == WarpState.JUMPIN)
        {
            Vector2 p = entity.GetEntity().GetPosition();
            entity.transform.position = new Vector3(p.x + (-vector.x * VELOCITY * clock), p.y + (-vector.y * VELOCITY * clock),0);

        }

        clock -= Time.deltaTime;
        if (clock <= 0)
        {
            EndHandler();
        }
    }

    private void EndHandler()
    {
        switch (warpHandler.state)
        {
            case WarpState.JUMPOUT:
                warpHandler.state = WarpState.TRANSIT;
                LaunchLandTool launchLandTool = new LaunchLandTool();
                ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(GlobalGameState.GetInstance().getCurrentEntity().
                    GetShip(GlobalGameState.GetInstance().getCurrentZone().getName()));
                ((Spaceship)GlobalGameState.GetInstance().getCurrentEntity()).SetAnimation(null);
                launchLandTool.RelinquishControl(shipAnalysisTool);
                break;
            case WarpState.JUMPIN:
                ((Spaceship)GlobalGameState.GetInstance().getCurrentEntity()).SetWarpHandler(null);
                ((Spaceship)GlobalGameState.GetInstance().getCurrentEntity()).SetAnimation(null);
                break;
        }
    }

    public override bool IsComplete()
    {
        return clock <=0;
    }
}