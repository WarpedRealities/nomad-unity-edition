

using System;

[Serializable]
public abstract class ShipAnimation
{
    protected float clock;

    public abstract void Update(ActiveEntityScript entity, SolarSceneScript solarSceneScript);

    public abstract bool IsComplete();
}