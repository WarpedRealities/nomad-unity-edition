﻿using UnityEngine;
using System.Collections;

public class HazardSprite : MonoBehaviour
{
    float maxLife;
    float lifespan;
    Vector2 velocity, startPos;
    public SpriteRenderer spriteRenderer;
    private Color color;
    // Use this for initialization
    void Start()
    {
        maxLife = 50+(DiceRoller.GetInstance().RollOdds() * 50);
        lifespan = maxLife;
        color = new Color(1, 1, 1, 1);
        startPos = transform.position;
        int ix = (int)transform.position.x;
        int iy = (int)transform.position.y;
        float vx = ix + 0.5F;
        float vy = iy + 0.5F;
        Vector2 n = new Vector2(startPos.x - vx, startPos.y - vy);
        n = n.normalized/maxLife;
        velocity = n;
    }

    // Update is called once per frame
    void Update()
    {
        lifespan -= Time.deltaTime;
        transform.Translate(velocity * Time.deltaTime);
        if (lifespan < 1)
        {
            if (color.a > 0)
            {
                color.a -= Time.deltaTime;
                spriteRenderer.color = color;
            }
        }
        else
        {
            if (color.a < 1)
            {
                color.a += Time.deltaTime;
                spriteRenderer.color = color;
            }
        }
        if (lifespan < 0)
        {
            lifespan = maxLife;
            transform.position = new Vector3(startPos.x, startPos.y);
        }
    }
}
