using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NavigationScreen : MonoBehaviour
{
    public Sprite[] buttonSprites;
    public Image[] buttonImages;
    Spaceship spaceship;
    SolarSceneScript solarScene;
    WarpHandler warpHandler;
    private GameObject stage;
    public TextMeshProUGUI displayText;
    public Spinner spinner;
    int state;
    bool canProgress;
    // Start is called before the first frame update
    void Start()
    {

    }
    public void SetSolarScene(SolarSceneScript solarScene)
    {
        this.solarScene = solarScene;
        this.spaceship = (Spaceship)GlobalGameState.GetInstance().getCurrentEntity();
        if (spaceship.GetWarpHandler() != null)
        {
            warpHandler = spaceship.GetWarpHandler();
            state = 3;
        }
        else
        {
            warpHandler = NavigationTool.GenerateWarp(spaceship);
            state = 0;
        }
        stage = GameObject.Find("NavigationStage");
        SetupState();
    }

    public void SetupState()
    {
        switch (state)
        {
            case 0:
                SetupReadyState();
                break;
            case 1:
                SetupNavState();
                break;
            case 2:
                SetupGravState();
                break;
            case 3:
                SetupWarpState();
                break;
        }

        for (int i = 0; i < buttonImages.Length; i++)
        {
            buttonImages[i].sprite = i <= state ? this.buttonSprites[0]: this.buttonSprites[1];
        }
    }

    private void SetupWarpState()
    {
        if (spaceship.GetWarpHandler() == null)
        {
            warpHandler.state = WarpState.CHARGING;
            spaceship.SetWarpHandler(warpHandler);
            this.solarScene.SetWarpEffect(true);
            spaceship.SetFacing(warpHandler.direction);
            this.solarScene.UpdatePlayer();
        }
        stage.SendMessage("SetWarpState", warpHandler);
        displayText.text = warpHandler.state == WarpState.FAILED ? "JUMP FAILURE \n WARP COLLAPSE": "warp Jump charging";
    }

    private void SetupGravState()
    {
        stage.SendMessage("SetGravState", warpHandler);
        displayText.text = "Grav Stress \n" + GenerateGravString();
        canProgress = true;
    }

    private string GenerateGravString()
    {
        float threshold = 2;

        if (warpHandler.stress < threshold)
        {
            return "okay";
        }
        else
        {
            return "WARNING";
        }
    }

    private void SetupNavState()
    {
        stage.SendMessage("SetNavState", warpHandler);
        if (warpHandler.destinationName != null)
        {
            displayText.text = (GlobalGameState.GetInstance().getCurrentSystem().GetName() + "\n" +
                warpHandler.destinationName).Replace('_',' ');
            canProgress = true;
        }
        else
        {
            displayText.text = "error \n destination not found on vector";
            canProgress = false;
        }
    }

    private void SetupReadyState()
    {
        stage.SendMessage("SetReadyState", warpHandler);
        ShipResource energyResource = spaceship.GetResources().GetResource("RES_ENERGY");
        if (energyResource != null && energyResource.amount>=1)
        {
            canProgress = true;
            displayText.text = "system ready";
        }
        else
        {
            canProgress = false;
            displayText.text = "critical error \n no energy source";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 3 && warpHandler.state == WarpState.FAILED)
        {
            displayText.text = "JUMP FAILURE \n WARP COLLAPSE";
            if (warpHandler.charge > 0)
            {
                warpHandler.charge -= Time.deltaTime / 10;
            }
        }
    }

    public void MoveToState(int value)
    {
        if (state == value - 1 && canProgress)
        {
            state = value;
            SetupState();
            return;
        }
        if (value <= state)
        {
            if (state == 3)
            {
                //abort the warp
                warpHandler = NavigationTool.GenerateWarp(spaceship);
                spaceship.SetWarpHandler(null);
            }
            state = value-1;
            SetupState();
        }
    }

    public void ScrnClick()
    {
        this.solarScene.SetScreen(null);

    }

    public void NavClick()
    {
        MoveToState(1);
    }

    public void LockClick()
    {
        MoveToState(2);
    }

    public void WarpClick()
    {
        MoveToState(3);
    }

    public void TimeClick()
    {
        spinner.AddVelocity(300);
        solarScene.PassTime(100);
    }
}
