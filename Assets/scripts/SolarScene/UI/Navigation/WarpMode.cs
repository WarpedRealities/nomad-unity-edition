using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpMode : MonoBehaviour
{
    public SineWaveMachine waveMachine;
    public WarpHandler warpHandler;
    internal void Setup(WarpHandler handler)
    {
        this.warpHandler = handler;

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.waveMachine.charge = warpHandler.charge;
    }
}
