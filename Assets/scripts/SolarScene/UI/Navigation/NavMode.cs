using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavMode : MonoBehaviour
{
    public LineRenderer line;
    public GameObject[] stars;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Setup(WarpHandler warp)
    {
        if (warp.destinationName != null)
        {
            //get the relative position of the destination
            line.gameObject.SetActive(true);
            stars[1].SetActive(true);
            Vector2 origin = GlobalGameState.GetInstance().getCurrentSystem().GetViewPosition();
            Vector2 target = GlobalGameState.GetInstance().GetUniverse().GetSystem(warp.destinationName).GetViewPosition();
            Vector2 offset = (target - origin) *2;
            stars[1].transform.localPosition = offset;
            line.SetPosition(1, offset);
        }
        else
        {
            line.gameObject.SetActive(false);
            stars[1].SetActive(false);
        }
    }
}
