using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravData
{
    public float stress;
    public float modifier;
    public float[] columnValues;
    public float[] magnitudeValues;
    public float clock;

    public GravData()
    {
        columnValues = new float[8];
        magnitudeValues = new float[8];
        modifier = 0;
        clock = 0;
    }
}

public class GravMode : MonoBehaviour
{
    GravData gravData;
    public GameObject[] columns; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gravData.clock += Time.deltaTime*gravData.stress;
        if (gravData.clock> Mathf.PI * 2)
        {
            gravData.clock = gravData.clock- Mathf.PI * 2;
        }
        UpdateModifier();
        UpdateColumns();
    }

    private void UpdateModifier()
    {
        if (DiceRoller.GetInstance().RollDice(2)==0)
        {
            if (gravData.modifier < 10)
            {
                gravData.modifier += Time.deltaTime;
            }

        }
        else
        {
            if (gravData.modifier > 0)
            {
                gravData.modifier -= Time.deltaTime;
            }
        }
    }

    private void UpdateColumns()
    {
        for (int i = 0; i < 8; i++)
        {
            gravData.columnValues[i] = (ColumnValue(i) * gravData.magnitudeValues[i]) + gravData.modifier;
            gravData.columnValues[i] = gravData.columnValues[i] > 10 ? 10 : gravData.columnValues[i];
            columns[i].transform.localScale= new Vector3(0.25F,gravData.columnValues[i]/5,1);
        }
    }

    private float ColumnValue(int i)
    {
        int r = i % 2;
        switch (r)
        {
            case 0:
                return MathF.Sin(gravData.clock);
            case 1:
                return MathF.Cos(gravData.clock);
        }
        return gravData.clock / (MathF.PI * 2);
    }

    internal void Setup(WarpHandler handler)
    {
        gravData = new GravData();
        gravData.stress = handler.stress > 10 ? 10 : handler.stress;
        for (int i = 0; i < 8; i++)
        {
            gravData.magnitudeValues[i]= DiceRoller.GetInstance().RollOdds()*gravData.stress;
        }
    }
}
