using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationStage : MonoBehaviour
{
    public NavMode navMode;
    public GravMode gravMode;
    public WarpMode warpMode;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetReadyState(WarpHandler handler)
    {
        navMode.gameObject.SetActive(false);
        gravMode.gameObject.SetActive(false);
        warpMode.gameObject.SetActive(false);
    }

    public void SetNavState(WarpHandler handler)
    {
        navMode.Setup(handler);
        navMode.gameObject.SetActive(true);
        gravMode.gameObject.SetActive(false);
        warpMode.gameObject.SetActive(false);
    }

    public void SetGravState(WarpHandler handler)
    {
        gravMode.Setup(handler);
        navMode.gameObject.SetActive(false);
        gravMode.gameObject.SetActive(true);
        warpMode.gameObject.SetActive(false);
    }

    public void SetWarpState(WarpHandler handler)
    {
        warpMode.Setup(handler);
        navMode.gameObject.SetActive(false);
        gravMode.gameObject.SetActive(false);
        warpMode.gameObject.SetActive(true);
    }
}
