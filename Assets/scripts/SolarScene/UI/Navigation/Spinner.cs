using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    float velocity = 0;
    readonly float FRICTION = 0.1F;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (velocity > 0)
        {
            velocity -= FRICTION * velocity * Time.deltaTime;
        }
        transform.Rotate(new Vector3(0,0, -velocity * Time.deltaTime));
    }

    public void AddVelocity(float v)
    {

        velocity += v;
    }

    internal void SetVelocity(float value)
    {
        velocity = 0;
    }
}
