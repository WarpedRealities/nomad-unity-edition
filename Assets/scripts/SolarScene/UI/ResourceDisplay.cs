using System;
using System.Collections;
using System.Collections.Generic;
using System.Resources;
using System.Text;
using UnityEngine;

public class ResourceDisplay : MonoBehaviour
{
    public GameObject prefabResourceDisplay;
    public GameObject prefabSatiationDisplay;

    public ResourceDisplayComponent[] components; 
    public void BuildUI(Spaceship spaceship, Player player)
    {
        components = new ResourceDisplayComponent[spaceship.GetResources().GetResourceList().Count+2];

        components[0] = GenerateComponent("HULL", (spaceship.GetCommon().GetHp() + "/" + spaceship.GetCommon().GetMaxHp()), false);
        int resourceStringCount = spaceship.GetResources().GetResourceList().Count;
        
        for (int i = 0; i < resourceStringCount; i++)
        {
            if (!"RES_COOLANT".Equals(spaceship.GetResources().GetResourceList()[i].resource))
            {
                components[i + 1] = GenerateComponent(spaceship.GetResources().GetResourceList()[i].displayName,
                    (int)spaceship.GetResources().GetResourceList()[i].amount + "/" +
                        spaceship.GetResources().GetResourceList()[i].capacity, false);
            }
        }
        components[resourceStringCount + 1] = GenerateComponent("Satiation:",
        player.GetRPG().GetStat(ST.SATIATION) +
                                "/" + player.GetRPG().GetStatMax(ST.SATIATION), true);
        int index = 0;
        for (int i = 0; i < components.Length; i++)
        {
            if (components[i] != null)
            {
                components[i].transform.SetParent(transform, false);
                components[i].transform.localPosition = new Vector2(0, (-24 * index)-4);
                index++;
            }
        }
       
    }

    private ResourceDisplayComponent GenerateComponent(string name, string value, bool alternate)
    {
        GameObject obj = Instantiate(alternate ? prefabSatiationDisplay : prefabResourceDisplay, null);
        ResourceDisplayComponent displayComponent = obj.GetComponent<ResourceDisplayComponent>();
        displayComponent.SetName(name);
        displayComponent.SetValue(value);
        return displayComponent;
    }

    public void UpdateUI(Spaceship spaceship, Player player)
    {
        components[0].SetValue((spaceship.GetCommon().GetHp() + "/" + spaceship.GetCommon().GetMaxHp()));

        int resourceStringCount = spaceship.GetResources().GetResourceList().Count;
        for (int i = 0; i < resourceStringCount; i++)
        {
            if (components[i + 1] != null)
            {
                components[i + 1].SetValue(((int)spaceship.GetResources().GetResourceList()[i].amount + "/" +
                        spaceship.GetResources().GetResourceList()[i].capacity));
            }
        }
        components[resourceStringCount+1].SetValue(player.GetRPG().GetStat(ST.SATIATION) +
                                "/" + player.GetRPG().GetStatMax(ST.SATIATION));
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
