﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class SolarPanelScript : MonoBehaviour
{
    public ResourceDisplay resourceDisplay;
    public Text coordinates;
    public GameObject ftlFrame;
    Spaceship spaceship;
    Player player;

    // Start is called before the first frame update
    void Start()
    {
        spaceship = (Spaceship)GlobalGameState.GetInstance().getCurrentEntity();
        player = GlobalGameState.GetInstance().GetPlayer();
        resourceDisplay.BuildUI(spaceship,player);
        ftlFrame.SetActive(spaceship.GetSolarStats().GetFTL()>0);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private bool DrawResource(string resource)
    {
        if ("RES_COOLANT".Equals(resource))
        {
            return false;
        }
        return true;
    }

    public void UpdateUI()
    {
        resourceDisplay.UpdateUI(spaceship, player);
        /*
        const string NEWLINE = "\n";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("HULL:" + spaceship.GetCommon().GetHp() + "/" + spaceship.GetCommon().GetMaxHp() + NEWLINE);
        int resourceStringCount = spaceship.GetResources().GetResourceList().Count;
        for (int i = 0; i <= resourceStringCount; i++)
        {
            if (i != resourceStringCount)
            {
                if (DrawResource(spaceship.GetResources().GetResourceList()[i].resource))
                {
                    stringBuilder.Append(spaceship.GetResources().GetResourceList()[i].displayName + ":" +
                    (int)spaceship.GetResources().GetResourceList()[i].amount + "/" +
                    spaceship.GetResources().GetResourceList()[i].capacity + NEWLINE);
                }
            }
            else
            {
                stringBuilder.Append("Satiation:" +
                                player.GetRPG().GetStat(ST.SATIATION) +
                                "/" + player.GetRPG().GetStatMax(ST.SATIATION) +
                                NEWLINE);
            }
        }
        resourceText.text = stringBuilder.ToString();
        */
    }

    public void UpdateCoords()
    {
        coordinates.text = "X:" + spaceship.GetPosition().x + " Y:" + spaceship.GetPosition().y;
    }

    public void ExitButton()
    {
        
        LaunchLandTool launchLandTool = new LaunchLandTool();
        ShipAnalysisTool shipAnalysisTool=new ShipAnalysisTool(GlobalGameState.GetInstance().getCurrentEntity().
            GetShip(GlobalGameState.GetInstance().getCurrentZone().getName()));
        if (shipAnalysisTool.GetShip().GetWarpHandler()!=null &&
            shipAnalysisTool.GetShip().GetWarpHandler().CanAbort())
        {
            shipAnalysisTool.GetShip().SetWarpHandler(null);
        }
        launchLandTool.RelinquishControl(shipAnalysisTool);
    }

    public void UpdateFTL(bool active)
    {
        if (active)
        {
            this.ftlFrame.gameObject.SetActive(this.spaceship.GetSolarStats().GetFTL() > 0 ? true : false);
        }
        else
        {
            this.ftlFrame.gameObject.SetActive(false);
        }

    }

}
