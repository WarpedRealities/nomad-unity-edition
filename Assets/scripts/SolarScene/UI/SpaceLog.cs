using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public delegate void DelegateLogText(string text);

public class SpaceLog : MonoBehaviour
{
    private DelegateLogText logText;
    public TextMeshProUGUI[] logEntries;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public DelegateLogText GetDelegate()
    {
        if (logText == null)
        {
            logText = LogText;
        }
        return logText;
    }

    public void LogText(string text)
    {
        for (int i=logEntries.Length-1;i>0; i--)
        {
            logEntries[i].text = logEntries[i - 1].text;
        }
        logEntries[0].text = text;
    }
}
