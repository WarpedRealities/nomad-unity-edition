using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceDisplayComponent : MonoBehaviour
{
    public TextMeshProUGUI[] textPieces;
    public void SetName(string name)
    {
        textPieces[0].text = name;
    }

    public void SetValue(string value)
    {
        textPieces[1].text = value;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
