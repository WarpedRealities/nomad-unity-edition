using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarSystemPanel : MonoBehaviour
{
    public GameObject prefab;

    ListCallback callback;
    Spaceship spaceship;
    SolarSceneScript solarScene;
    SolarConverterEntry[] converters;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void ToggleConverter(int index)
    {
        spaceship.GetResources().GetConverterList()[index].SetActive(
            !spaceship.GetResources().GetConverterList()[index].IsActive());
        converters[index].SetState(spaceship.GetResources().GetConverterList()[index].IsActive());
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void SetSolarScene(SolarSceneScript solarScene)
    {
        this.solarScene = solarScene;
        this.spaceship = (Spaceship)GlobalGameState.GetInstance().getCurrentEntity();
        BuildUI();
    }

    private void BuildUI()
    {
        callback = ToggleConverter;
        SpaceshipResourceStats spaceshipStats = spaceship.GetResources();
        this.converters = new SolarConverterEntry[spaceshipStats.GetConverterList().Count];
        int index = 0;
        for (int i = 0; i < spaceshipStats.GetConverterList().Count; i++)
        {
            if (!"RES_COOLANT".Equals(spaceshipStats.GetConverterList()[i].GetFrom()) &&
                !"RES_COOLANT".Equals(spaceshipStats.GetConverterList()[i].GetTo()))
            {
                GameObject gameObject = Instantiate(prefab);
                converters[i] = gameObject.GetComponent<SolarConverterEntry>();
                converters[i].transform.SetLocalPositionAndRotation(new Vector3(36, -64 + (index * -32)), Quaternion.identity);
                converters[i].transform.SetParent(transform, false);
                converters[i].SetIndex(i, callback);
                converters[i].labelText.text = spaceshipStats.GetConverterList()[i].GetSystem().GetDescription();
                converters[i].SetState(spaceshipStats.GetConverterList()[i].IsActive());
                index++;
            }
        }
    }

    public void ExitButton()
    {
        this.solarScene.SetScreen(null);
        //Destroy(gameObject);
    }
}