﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SolarConverterEntry : MonoBehaviour
{
    public TMPro.TextMeshProUGUI labelText;
    public TMPro.TextMeshProUGUI buttonText;
    public Image buttonImage;
    public Sprite[] sprites;
    ListCallback listCallback;
    int index;

    public void SetIndex(int index, ListCallback listCallback)
    {
        this.listCallback = listCallback;
        this.index = index;
    }

    public void ClickButton()
    {
        listCallback(index);
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetState(bool activated)
    {
        this.buttonImage.sprite = activated ? sprites[0] : sprites[1];
    }
}
