﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarSceneController : SolarControl
{
    private DelegateLogText logText;
    private SolarScriptRunnerTool scriptTool;
    public List<EntityScript> systemObjects;
    public List<ActiveEntityScript> activeObjects;
    public ActiveEntityScript playerEntity;
    private Spaceship playerShip;
    private StarSystem system;
    public SolarSceneScript parentScript;
    private int sensorClock = 0;
    private readonly static int SENSORINTERVAL = 5;
    public SolarSceneController(StarSystem system, Spaceship ship, SolarSceneScript parentScript)
    {
        activeObjects = new List<ActiveEntityScript>();
        systemObjects = new List<EntityScript>();
        this.parentScript = parentScript;
        this.system = system;
        scriptTool = new SolarScriptRunnerTool(system);
        this.playerShip = ship;
    }

    public void SetLog(DelegateLogText logText)
    {
        this.logText = logText;
    }

    public void Generate(GameObject entityTemplate, GameObject activeTemplate, GameObject signatureTemplate, PlayerEntityController playerController, Transform parent)
    {
        playerShip.SetController(new Player_Captain());
        SolarSystemGenerator generator = new SolarSystemGenerator(entityTemplate, activeTemplate, signatureTemplate);
        generator.Generate(system.GetSystemContents());
        List<GameObject> sprites = generator.GetSprites();
        for (int i = 0; i < sprites.Count; i++)
        {
            if (sprites[i].GetComponent<ActiveEntityScript>() != null)
            {
                activeObjects.Add(sprites[i].GetComponent<ActiveEntityScript>());
                if (sprites[i].GetComponent<ActiveEntityScript>().entity.Equals(playerShip))
                {
                    playerEntity = sprites[i].GetComponent<ActiveEntityScript>();
                    playerEntity.GetEntity().SetVisible(true);
                    playerEntity.entityController = playerController;
                    playerController.SetDelegate(playerEntity.GetDelegate());
                    playerEntity.GetComponent<SpriteRenderer>().enabled = true;
                }
                if (((Active_Entity)sprites[i].GetComponent<ActiveEntityScript>().entity).GetController() is NPC_Captain)
                {
                    ActiveEntityScript activeEntityScript = sprites[i].GetComponent<ActiveEntityScript>();
                    activeEntityScript.entityController = new NpcEntityController((Active_Entity)activeEntityScript.entity, this);
                    activeEntityScript.entityController.SetDelegate(activeEntityScript.GetDelegate());
                }
            }
            else
            {
                systemObjects.Add(sprites[i].GetComponent<EntityScript>());
            }

            sprites[i].transform.parent = parent;
        }
        RunSensors();
    }

    internal SolarScriptRunnerTool GetScriptTool()
    {
        return scriptTool;
    }

    public Entity GetEntityAtPosition(int x, int y, Entity exclude)
    {
        for (int i = 0; i < system.GetSystemContents().GetEntities().Count; i++)
        {
            if (exclude != system.GetSystemContents().GetEntities()[i])
            {
                Entity e = system.GetSystemContents().GetEntities()[i];
                if (e.GetPosition().x == x && e.GetPosition().y == y)
                {
                    return e;
                }
            }
        }

        return null;
    }

    public bool InitiateInteraction(Entity entity)
    {
        if (entity is Planet)
        {
            Planet p = (Planet)entity;
            if (p.GetContents() == null)
            {
                p.generate();
                //auto landing
                if (p.CanLand())
                {
                    Zone z = p.GetZone(0);
                    z.Generate();
                    LaunchLandTool launchLandTool = new LaunchLandTool();
                    ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(playerShip);
                    launchLandTool.Land(shipAnalysisTool, p, z);
                    return true;
                }

            }
            else
            {
                //landing screen
                GameObject screen = (GameObject)GameObject.Instantiate(Resources.Load("LandingPanel2"), GameObject.Find("Canvas").transform);
                screen.SendMessage("SetPlanet", p);
                this.parentScript.SetScreen(screen);
                return true;
            }
        }
        if (entity is Spaceship)
        {
            Spaceship spaceship = (Spaceship)entity;
            spaceship.Interact(playerShip, logText, this);
            return true;
        }
        if (entity is SignatureEntity)
        {
            SignatureEntity signatureEntity = (SignatureEntity)entity;
            signatureEntity.Interact(playerShip, logText, this);
            return false;

        }
        if (entity is Station)
        {
            Station s = (Station)entity;
            if (s.GetContents() == null)
            {
                s.generate();
            }
            if (s.GetContents().GetLandingSites().Count > 1)
            {

            }
            else
            {
                LaunchLandTool launchLandTool = new LaunchLandTool();
                ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(playerShip);
                Debug.Log("zone name " + s.GetContents().GetLandingSites()[0].GetZone());
                Zone z= s.GetZone(s.GetContents().GetLandingSites()[0].GetZone());
                z.Generate();
                launchLandTool.Dock(shipAnalysisTool, s, z);
                return true;
            }
        }
        return false;
    }


    public void UpdateWorld()
    {
        for (int i = 0; i < activeObjects.Count; i++)
        {
            activeObjects[i].UpdateLogic();
        }
        sensorClock++;
        if (sensorClock >= SENSORINTERVAL)
        {
            sensorClock = 0;
            RunSensors();
        }
        UpdateVisibility();
    }

    private void RunSensors()
    {

        ActiveEntityScript player = playerEntity;
        float sensorLevel = (playerShip.GetSolarStats().GetSensors() + 1);

        for (int i=0;i< systemObjects.Count; i++)
        {
            if (playerEntity!= systemObjects[i] && !systemObjects[i].entity.isAlwaysVisible())
            {
                float d = Vector2.Distance(systemObjects[i].entity.GetPosition(), playerEntity.entity.GetPosition()) / 2;
                float detectability = systemObjects[i].entity.GetDetection();

                float obfuscation = system.GetSystemContents().GetSpaceGrid().GetObfuscation((int)systemObjects[i].entity.GetPosition().x,(int)systemObjects[i].entity.GetPosition().y);
                systemObjects[i].entity.SetDetected((d - detectability + obfuscation <= sensorLevel));
            }
        }
        for (int i = 0; i < activeObjects.Count; i++)
        {
            if (playerEntity != activeObjects[i] && !activeObjects[i].entity.isAlwaysVisible())
            {
                float d = Vector2.Distance(activeObjects[i].entity.GetPosition(), playerEntity.entity.GetPosition()) / 2;

                float detectability = activeObjects[i].entity.GetDetection();

                float obfuscation = system.GetSystemContents().GetSpaceGrid().GetObfuscation((int)activeObjects[i].entity.GetPosition().x, (int)activeObjects[i].entity.GetPosition().y);

                activeObjects[i].entity.SetDetected((d - detectability + obfuscation <= sensorLevel));
            }
        }
    }

    private void UpdateVisibility()
    {
        for (int i = 0; i < systemObjects.Count; i++)
        {
            systemObjects[i].UpdateVisibility();
        }
        for (int i = 0; i < activeObjects.Count; i++)
        {
            activeObjects[i].UpdateVisibility();
        }
    }

    public int GetHazard(int x, int y)
    {
        return system.GetSystemContents().GetSpaceGrid().GetHazard(x, y);
    }

    public void DrawText(string text)
    {
        logText(text);
    }

    public void RefreshSprites()
    {
        for (int i = this.systemObjects.Count-1; i >=0 ; i--)
        {
            if (!system.GetSystemContents().GetEntities().Contains(systemObjects[i].entity))
            {
                GameObject.Destroy(systemObjects[i].gameObject);
                systemObjects.Remove(systemObjects[i]);
            }
        }
        for (int i = this.activeObjects.Count-1; i>=0 ; i--)
        {
            if (!system.GetSystemContents().GetEntities().Contains(activeObjects[i].entity))
            {
                GameObject.Destroy(activeObjects[i].gameObject);
                activeObjects.Remove(activeObjects[i]);
            }
        }
    }

    internal ActiveEntityScript GetPlayerEntity()
    {
        return this.playerEntity;
    }
}
