﻿using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;
using System;

public delegate void IntCallback(int value);

public class NpcEntityController : ActiveEntityController
{
    private LuaSolarControllable solarControllable;
    private LuaSolarSensor solarSensor;
    private SolarSceneController solarSceneController;
    Script script;
    DynValue mainFunction;
    object[] arguments;
    IntCallback busyCallback;

    public NpcEntityController(Active_Entity entity, SolarSceneController solarSceneController)
    {
        this.entity = entity;
        solarControllable = new LuaSolarControllable(entity);
        this.solarSensor = new LuaSolarSensor(GlobalGameState.GetInstance().getCurrentSystem());
        this.solarSceneController = solarSceneController;
        script = new Script();

        String fileContents = FileTools.GetFileRaw("controllers/scripts/" + 
            entity.GetController().GetData().GetSolarScript() + ".lua");
        script.DoString(fileContents);
        mainFunction = script.Globals.Get("main");
        arguments = new object[2];
        arguments[0] = solarControllable;
        arguments[1] = solarSensor;
        busyCallback = SetBusy;
        solarControllable.SetBusyCallback(busyCallback);
    }

    public override bool TakeAction()
    {
        DynValue rValue = script.Call(mainFunction, arguments);
        this.redraw();
        return true;
    }

    public override void Update(int duration)
    {
        if (entity.GetController().GetTicks() > 0)
        {
            entity.GetController().RemoveTicks(duration);
        }
    }
}
