﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActiveEntityController 
{

    protected Active_Entity entity;

    protected RedrawDelegate redraw;

    public bool isBusy()
    {
        return entity.GetController().GetTicks() > 0;
    }

    public void SetDelegate(RedrawDelegate redraw)
    {
        this.redraw = redraw;
    }

    public void SetBusy(int ticks)
    {
        this.entity.GetController().AddBusyTicks(ticks);
    }

    public abstract void Update(int duration);

    public abstract bool TakeAction();
}
