﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEntityController : ActiveEntityController
{
    const int DEFAULT_WAIT = 100;
    Spaceship ship;
    SolarControl solarControl;
    private float clock = 0;
    private readonly float INTERVAL = 0.05F;
    private int direction;
    private const int ALIASNORTH = 1;
    private const int ALIASEAST = 2;
    private const int ALIASSOUTH = 4;
    private const int ALIASWEST = 8;
    private const int ALIASNORTHEAST = 3;
    private const int ALIASSOUTHEAST = 6;
    private const int ALIASSOUTHWEST = 12;
    private const int ALIASNORTHWEST = 9;
    private const int ALIASWAIT = -2;

    public PlayerEntityController(Spaceship ship, SolarControl solarControl)
    {
        this.entity = ship;
        this.solarControl = solarControl;
        this.ship = ship;
    }

    public override bool TakeAction()
    {
        if (clock > 0)
        {
            clock += Time.deltaTime;
            if (clock> INTERVAL)
            {
                clock = 0;
                int d = MoveControls();
                return AttemptMove(ConvertAndToDirection(d >= 0 ? d : d));

            }
        }
        else
        {
            int direction = MoveControls();
            if (direction != -1)
            {
                clock += Time.deltaTime;
            }
        }
        return false;
    }

    private int ConvertAndToDirection(int and)
    {
        switch (and)
        {
            case ALIASNORTH:
                return 0;
            case ALIASEAST:
                return 2;
            case ALIASSOUTH:
                return 4;
            case ALIASWEST:
                return 6;
            case ALIASNORTHEAST:
                return 1;
            case ALIASNORTHWEST:
                return 7;
            case ALIASSOUTHEAST:
                return 3;
            case ALIASSOUTHWEST:
                return 5;
            case ALIASWAIT:
                return ALIASWAIT;
        }
        return -1;
    }

    private bool AttemptMove(int direction)
    {
        if (direction >= 0)
        {
            Movement(direction);
            if (ship.GetWarpHandler() != null)
            {
                ship.SetWarpHandler(null);
            }
            return true;
        }
        switch (direction)
        {
            case -2:
                entity.GetController().AddBusyTicks(DEFAULT_WAIT);
                return true;
            default:
                return false;
        }
    }

    private int MoveControls()
    {
        bool shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

        int d = 0;
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.A))
        {
            d = d | ALIASWEST;
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Keypad8) || Input.GetKey(KeyCode.W))
        {
            d = d | ALIASNORTH;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.S))
        {
            d = d | ALIASSOUTH;
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Keypad6) || Input.GetKey(KeyCode.D))
        {
            d = d | ALIASEAST;
        }
        if (Input.GetKey(KeyCode.Keypad7) || (shift && Input.GetKey(KeyCode.Home)))
        {
            d = d | ALIASNORTHWEST;
        }
        if (Input.GetKey(KeyCode.Keypad1) || (shift && Input.GetKey(KeyCode.End)))
        {
            d = d | ALIASSOUTHWEST;
        }
        if (Input.GetKey(KeyCode.Keypad3) || (shift && Input.GetKey(KeyCode.PageDown)))
        {
            d = d | ALIASSOUTHEAST;
        }
        if (Input.GetKey(KeyCode.Keypad9) || (shift && Input.GetKey(KeyCode.PageUp)))
        {
            d = d | ALIASNORTHEAST;
        }
        if (Input.GetKey(KeyCode.Keypad5) || Input.GetKey(KeyCode.Space))
        {
            d = ALIASWAIT;
        }
        return d;
    }

    public override void Update(int duration)
    {
        if (entity.GetController().GetTicks() > 0)
        {
            entity.GetController().RemoveTicks(duration);
        }
    }

    public void Movement(int direction)
    {
        if (ship.GetResources().GetResource("RES_FUEL").amount < ship.GetSolarStats().GetEfficiency())
        {
            return;
        }
        //set facing
        ship.SetFacing(direction);
        float baseCost = direction % 2 > 0 ? 1.5F : 1;
        Vector2Int p = GeometryTools.GetPosition((int)ship.GetPosition().x, (int)ship.GetPosition().y, direction);
        entity.GetController().AddBusyTicks((int)(DEFAULT_WAIT/ship.GetSolarStats().GetCruise()*baseCost));
        int hazard = solarControl.GetHazard(p.x, p.y);
        if (hazard > 0)
        {
            int dc = hazard + 1;
            int dr = DiceRoller.GetInstance().RollDice(10) + GlobalGameState.GetInstance().GetPlayer().GetRPG().getSkill(SK.PILOTING);
            if (dr < dc)
            {
                int dif = dc - dr;
                dif = dif > hazard ? hazard : dif;
                ship.GetCommon().ModHP(-dif);
                solarControl.DrawText("Pilot check failed, damage from hazard " + dif + " points");
            }
        }
        if (!CollisionCheck(p, ship))
        {
            ship.setPosition(p);
        }   
        //need to use up fuel
        ship.GetResources().GetResource("RES_FUEL").amount -= baseCost * ship.GetSolarStats().GetEfficiency();
        redraw();

    }

    private bool CollisionCheck(Vector2Int p,Entity exclude)
    {
        Entity e = solarControl.GetEntityAtPosition(p.x, p.y, exclude);
        return solarControl.InitiateInteraction(e);
    }

    internal void Redraw()
    {
        redraw();
    }
}
