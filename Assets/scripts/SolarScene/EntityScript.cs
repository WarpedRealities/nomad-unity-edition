﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityScript : MonoBehaviour
{
    public Entity entity;
    public SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual bool runDetection(float detection, Vector2Int source)
    {



        return false;
    }

    public void UpdateVisibility()
    {
        if (entity.isAlwaysVisible())
        {
            spriteRenderer.enabled = true;
        }
        else
        {
            spriteRenderer.enabled = entity.GetVisible();
        }
    }

    public Entity GetEntity()
    {
        return entity;
    }


}
