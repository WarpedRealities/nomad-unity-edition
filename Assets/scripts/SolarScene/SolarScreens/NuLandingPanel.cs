using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NuLandingPanel : MonoBehaviour
{
    private SolarSceneScript solarScene;
    private Entity entity;
    public Switch activateSwitch;
    public GameObject panel;
    private ListCallback listCallback;
    private DelegateCall switchCallback;
    public GameObject prefabLandingSite;
    private LandingSiteUI[] landingSites;
    int index = -1;
    private NuLandingOption[] landingOptions;
    private float width, height;
    // Start is called before the first frame update
    void Start()
    {
        switchCallback = this.Activateclick;
        activateSwitch.activate = this.switchCallback;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlanet(Entity entity)
    {
        listCallback = ListCallback;
        this.entity = entity;
        NuLandingOptionsGenerator generator = new NuLandingOptionsGenerator(entity);
        landingOptions = generator.GenerateOptions();
        generator.GenerateHeightWidth(landingOptions, out width, out height);
        BuildUI();
    }

    public void ListCallback(int value)
    {
        index = value;
        UpdateUI();
    }

    private void UpdateUI()
    {
        for (int i=0;i<landingSites.Length;i++)
        {
            landingSites[i].SetSelected(index == i);
        }
    }

    private void BuildUI()
    {
        landingSites = new LandingSiteUI[landingOptions.Length];
        Vector2 p = new Vector2( -0.5F * width, -0.5F*height);
        for (int i = 0; i < landingOptions.Length; i++)
        {
            landingSites[i] = Instantiate(prefabLandingSite, 
                panel.transform).GetComponent<LandingSiteUI>();
            landingSites[i].transform.localPosition = new Vector3(
                (p.x + landingOptions[i].p.x)*64, (p.y + landingOptions[i].p.y)*64);
            landingSites[i].SetState(landingOptions[i].landingOptionState);
            landingSites[i].SetCallback(listCallback,i);
        }
    }

    public void CancelClick()
    {
        this.solarScene.SetScreen(null);
    }

    public void Activateclick()
    {
        if (index!=-1 && landingOptions[index].landingOptionState == LANDINGOPTIONSTATE.ACCESSIBLE)
        {
            if (entity.GetEntityType() == ENTITYTYPE.PLANET)
            {
                //conduct the landing
                Zone z = landingOptions[index].zone;
                LaunchLandTool launchLandTool = new LaunchLandTool();
                ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool((Spaceship)GlobalGameState.GetInstance().getCurrentEntity());
                launchLandTool.Land(shipAnalysisTool, (Planet)this.entity, z);
            }
        }
    }

    public void SetSolarScene(SolarSceneScript solarScene)
    {
        this.solarScene = solarScene;
    }
}
