﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LandingOption
{
    public LandingSite landingSite;
    public Zone zone;
    public LANDINGOPTIONSTATE landingOptionState;

    public LandingOption(LandingSite landingSite, Zone zone, LANDINGOPTIONSTATE state)
    {
        this.landingOptionState = state;
        this.landingSite = landingSite;
        this.zone = zone;
    }
}

public class LandingPanel : MonoBehaviour
{
    private const string PREFIX_AVAILABLE = " available";
    private const string PREFIX_TAKEN = " taken";
    private const string ACTIVATELAND = "land";
    private const string ACTIVATEDOCK = "dock";
    Entity entity;
    LandingOption[] landingOptions;
    public NuList landingSiteList;
    public Button activateButton;
    int index;
    ListCallback callback;
    public SolarSceneScript solarScene;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlanet(Entity entity)
    {
        this.entity = entity;
        LandingOptionsGenerator generator = new LandingOptionsGenerator(entity);
        landingOptions = generator.GenerateOptions();
        this.activateButton.GetComponentInChildren<Text>().text = GetActivateButtonText(entity);
        BuildList();
    }

    private string GetActivateButtonText(Entity entity)
    {
        switch (entity.GetEntityType())
        {
            case ENTITYTYPE.PLANET:
                return ACTIVATELAND;

            case ENTITYTYPE.STATION:
                return ACTIVATEDOCK;
        }
        return ACTIVATEDOCK;
    }

    private void BuildList()
    {
        List<string> strings = new List<string>();
        for (int i = 0; i < landingOptions.Length; i++)
        {
            strings.Add(landingOptions[i].zone.getName() + GetSuffix(landingOptions[i].landingOptionState));
        }
        landingSiteList.SetList(strings);
        callback = ListCallback;
        landingSiteList.SetCallback(this.callback);
    }

    private string GetSuffix(LANDINGOPTIONSTATE landingOptionState)
    {
        switch (landingOptionState)
        {
            case LANDINGOPTIONSTATE.ACCESSIBLE:
                return PREFIX_AVAILABLE;

            case LANDINGOPTIONSTATE.TAKEN:
                return PREFIX_TAKEN;
        }
        return "";
    }

    public void ListCallback(int index)
    {
        this.index = index;
    }

    public void ActivateClick()
    {
        if (landingOptions[index].landingOptionState == LANDINGOPTIONSTATE.ACCESSIBLE)
        {
            if (entity.GetEntityType() == ENTITYTYPE.PLANET)
            {
                //conduct the landing
                Zone z = landingOptions[index].zone;
                LaunchLandTool launchLandTool = new LaunchLandTool();
                ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool((Spaceship)GlobalGameState.GetInstance().getCurrentEntity());
                launchLandTool.Land(shipAnalysisTool, (Planet)this.entity, z);
            }
        }
    }

    public void CancelClick()
    {
        this.solarScene.SetScreen(gameObject);
        Destroy(gameObject);
    }

    public void SetSolarScene(SolarSceneScript solarScene)
    {
        this.solarScene = solarScene;
    }

}
