using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LandingSiteUI : MonoBehaviour
{
    public Image image;
    public Sprite[] sprites;
    private ListCallback callback;
    private int index;
    private bool active = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetState(LANDINGOPTIONSTATE state)
    {
        switch (state)
        {
            case LANDINGOPTIONSTATE.UNEXPLORED:
                image.sprite = sprites[3];
                active = false;
                break;
            case LANDINGOPTIONSTATE.TAKEN:
                image.sprite = sprites[2];
                active = false;
                break;
        }
    }

    public void SetSelected(bool selected)
    {
        if (this.active)
        {
            image.sprite = selected ? sprites[1] : sprites[0];
        }
    }

    internal void SetCallback(ListCallback listCallback, int index)
    {
        this.callback = listCallback;
        this.index = index;
    }

    public void Click()
    {
        if (active)
        {
            this.callback(this.index);
        }
    }
}
