﻿using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

[MoonSharpUserData]
public class LuaSolarSensor 
{
    private static bool isRegistered = false;
    StarSystem starSystem;
    SolarSceneScript sceneScript;
    public LuaSolarSensor(StarSystem starSystem)
    {
        this.starSystem = starSystem;
        if (!isRegistered)
        {
            UserData.RegisterType<LuaSolarSensor>();
            UserData.RegisterType<Vector2>();
            UserData.RegisterType<string>();
            isRegistered = true;
        }
        GameObject solarscene = GameObject.Find("SolarSceneObj");
        if (solarscene != null)
        {
            sceneScript = solarscene.GetComponent<SolarSceneScript>();
        }
    }

    public int ReadGlobalFlag(string flag)
    {
        return GlobalGameState.GetInstance().GetPlayer().GetFlagField().ReadFlag(flag);
    }

    public void StartConversation(string filename)
    {
        if (sceneScript != null)
        {
            ((Spaceship)GlobalGameState.GetInstance().getCurrentEntity()).SetWarpHandler(null);
            GameObject screen = (GameObject)GameObject.Instantiate(
                Resources.Load("SolarDialogueScreen"), GameObject.Find("Canvas").transform);
            this.sceneScript.SetScreen(screen);

            ScreenData screenData = new ScreenDataConversation(filename);
            screen.BroadcastMessage("SetConversation", (ScreenDataConversation)screenData);
        }
        else
        {
            string conversationFile = filename;
            ScreenDataConversation screenDataConversation = new ScreenDataConversation(conversationFile, null, null, false);
            GameObject.Find("ViewSceneController").GetComponent<ViewScene>().SetScreen(screenDataConversation);
        }
    }

    public void StartConversation(string filename, LuaSolarControllable controllable)
    {
        if (sceneScript != null)
        {
            ((Spaceship)GlobalGameState.GetInstance().getCurrentEntity()).SetWarpHandler(null);
            GameObject screen = (GameObject)GameObject.Instantiate(
                Resources.Load("SolarDialogueScreen"), GameObject.Find("Canvas").transform);
            this.sceneScript.SetScreen(screen);
            NPC_Captain captain = (NPC_Captain)controllable.GetEntity().GetController();
            FlagField flagField = captain.GetData().GetFlagField();
            ScreenData screenData = new ScreenDataConversation(filename, flagField, controllable.GetEntity());
            screen.BroadcastMessage("SetConversation", (ScreenDataConversation)screenData);
        }
        else
        {
            string conversationFile = filename;
            ScreenDataConversation screenDataConversation = new ScreenDataConversation(conversationFile, null,null, false);
            GameObject.Find("ViewSceneController").GetComponent<ViewScene>().SetScreen(screenDataConversation);
        }
    }

    public bool HasEncounter()
    {
        return GameObject.Find("Encounter") != null;
    }
    public bool TimerCheck(string timerKey, int timePassed)
    {
        if (!GlobalGameState.GetInstance().GetUniverse().GetTimeKeeper().HasTimeStamp(timerKey))
        {
            return true;
        }
        long stamp = GlobalGameState.GetInstance().GetUniverse().GetTimeKeeper().GetTimeStamp(timerKey);
        long time = GlobalGameState.GetInstance().GetUniverse().GetClock() / 1000;
        return (time - stamp) >= timePassed;
    }

    public LuaActiveEntity DetectPlayer(LuaSolarControllable controllable)
    {
        float sensorLevel = (controllable.GetEntity().GetSolarStats().GetSensors() + 1);

        float detectability = GlobalGameState.GetInstance().getCurrentEntity().GetDetection();

        Vector2 p = GlobalGameState.GetInstance().getCurrentEntity().GetPosition();
        float obfuscation = GlobalGameState.GetInstance().getCurrentSystem()
            .GetSystemContents().GetSpaceGrid().GetObfuscation((int)p.x, (int)p.y);
        float d = Vector2.Distance(p, controllable.GetEntity().GetPosition());        

        return (d-detectability + obfuscation <= sensorLevel) ? 
            ((Active_Entity)GlobalGameState.GetInstance().getCurrentEntity()).GetLuaEntity() 
            : null;
    }

    public void Decompose(bool chanceOfDamage)
    {
        LaunchLandTool launchLandTool = new LaunchLandTool();
        ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(GlobalGameState.GetInstance().getCurrentEntity().
            GetShip(GlobalGameState.GetInstance().getCurrentZone().getName()));
        if (chanceOfDamage)
        {
            shipAnalysisTool.ChanceOfDamage();
        }
        shipAnalysisTool.ReconcileStats();
    }

    public void Board(bool suppression, string[] npcs)
    {

        ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(GlobalGameState.GetInstance().getCurrentEntity().
            GetShip(GlobalGameState.GetInstance().getCurrentZone().getName()));
        BoardingTool.PlaceMonsters(GlobalGameState.GetInstance().getCurrentZone(), npcs);
        shipAnalysisTool.SuppressReformers();
        SceneManager.LoadScene("ViewScene");
    }

    public void LogText(string text)
    {
        this.sceneScript.GetSolarLog().LogText(text);
    }

    public void AddCargo(string lootList)
    {
        List<WidgetContainer> containers = CargoTool.ContainerScan(GlobalGameState.GetInstance().getCurrentEntity().GetZone(0));
        ItemList itemList = new ItemList();
        itemList.AddItem(new LootSubTable(
            1
            ,LootSubTableReferenceLibrary.GetInstance().GetSubtTable(lootList)
            ,LootQualifier.NONE)
            .GenerateItem());
        if (containers.Count == 0)
        {
            this.sceneScript.GetSolarLog().LogText("No cargo capacity for retrieval");
            return;
        }
        float freeAmount = CargoTool.CountSpace(containers);
        if (freeAmount < itemList.GetItem(0).GetDef().GetWeight())
        {
            this.sceneScript.GetSolarLog().LogText("No cargo capacity for retrieval");
            return;
        }
        AddItems(itemList, containers, this.sceneScript.GetSolarLog(), freeAmount);


    }

    private void AddItems(ItemList itemList, List<WidgetContainer> containers, SpaceLog spaceLog, float free)
    {
        int count = itemList.GetItemCount();
        for (int i = 0; i < count; i++)
        {
            Item item = itemList.GetItem(i);
            if (item != null)
            {
                if (item is ItemStack)
                {
                    free -= CargoTool.AddStack((ItemStack)item, containers, spaceLog.GetDelegate());
                }
                else
                {
                    free -= CargoTool.AddItems(item, containers, spaceLog.GetDelegate());
                }
                if (free <= 0)
                {
                    return;
                }
            }
        }
    }

    public void ToViewScene()
    {
        SceneManager.LoadScene("ViewScene");
    }

    public void ToSolarScene()
    {
        SceneManager.LoadScene("SolarScene");
    }
}
