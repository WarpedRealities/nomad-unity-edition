using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;
using System;

[MoonSharpUserData]
public class LuaActiveEntity
{
    Active_Entity entity;
    private static bool isRegistered = false;
    public LuaActiveEntity(Active_Entity entity)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<LuaActiveEntity>();
            UserData.RegisterType<Vector2>();
            isRegistered = true;
        }
        this.entity= entity;
    }

    public Vector2 GetPosition()
    {
        return entity.GetPosition();
    }

}