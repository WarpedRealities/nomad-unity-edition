﻿using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;
using System;

[MoonSharpUserData]
public class LuaSolarControllable
{
    const int DEFAULT_WAIT = 100;
    private static bool isRegistered = false;
    private Active_Entity entity;
    private Spaceship spaceship;
    private IntCallback busyCallback;

    public LuaSolarControllable(Active_Entity entity)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<LuaSolarControllable>();
            isRegistered = true;
        }
        this.spaceship = entity is Spaceship ? (Spaceship)entity: null;
        this.entity = entity;
    }

    public Active_Entity GetEntity()
    {
        return entity;
    }

    public int GetFactionRelationship(string faction)
    {
        return entity.GetController().GetData().GetFaction().getRelationship(faction);
    }

    public bool IsInGame()
    {
        return entity.GetInGame();
    }

    public void SetInGame(bool value)
    {
        entity.SetInGame(value);
    }

    public void Wait(int amount)
    {
        busyCallback(amount);
    }

    public void Orbit(int x, int y, int distance)
    {
       
        int facing = entity.GetFacing();
        Vector2 offset = GeometryTools.GetPosition(0, 0, facing)*4;
        Vector2 nextPosition = entity.GetPosition()+offset;

        float newDistance = Vector2.Distance(nextPosition, new Vector2(x, y));

        float deviation = Math.Abs(newDistance - distance);
        if (deviation > 5)
        {
            int left = facing!=0 ? facing - 1: 7;
            int right = facing != 7 ? facing + 1 : 0;
            Vector2 offsetLeft = GeometryTools.GetPosition(0, 0, left) * 4;
            Vector2 offsetRight = GeometryTools.GetPosition(0, 0, right) * 4;
            float leftDistance = Vector2.Distance(new Vector2(0, 0), offsetLeft+entity.GetPosition());
            float rightDistance = Vector2.Distance(new Vector2(0, 0), offsetRight + entity.GetPosition());
            float leftDeviation = Math.Abs(leftDistance - distance);
            float rightDeviation = Math.Abs(rightDistance - distance);
            facing = rightDeviation < leftDeviation ? right : left;
        }
    
        Move(facing);
    }

    public void MoveTowards(int x, int y)
    {

        int facing = GeometryTools.GetDirection((int)entity.GetPosition().x, (int)entity.GetPosition().y, x, y);
        Move(facing);
    }
    public void startCombat()
    {
        ((Spaceship)GlobalGameState.GetInstance().getCurrentEntity()).SetWarpHandler(null);
        GameObject gameObject = (GameObject)GameObject.Instantiate(Resources.Load("Encounter"));
        gameObject.name = "Encounter";
        Encounter encounter = gameObject.GetComponent<Encounter>();
        Active_Entity[] entities = new Active_Entity[] { this.entity };

        encounter.SetEncounter(new EncounterData(this.entity.GetController().GetData(),
                entities, (Spaceship)GlobalGameState.GetInstance().getCurrentEntity(), GlobalGameState.GetInstance().GetPlayer()));

    }

    public bool IsAlive()
    {
        return entity.GetCommon().GetHp() > 0;
    }

    public void Move(int direction)
    {

        direction = NoEntityCollisions(direction, entity.GetPosition());

        entity.SetFacing(direction);
        float baseCost = direction % 2 > 0 ? 1.5F : 1;

        Vector2Int p = GeometryTools.GetPosition((int)entity.GetPosition().x, (int)entity.GetPosition().y, direction);

        int ticks = (int)(DEFAULT_WAIT / entity.GetSolarStats().GetCruise() * baseCost);
        busyCallback(ticks);
        entity.setPosition(p);
        //check if you've collided with the player and run contact if so
        Vector2 p_position = GlobalGameState.GetInstance().getCurrentEntity().GetPosition();

        if (((int)entity.GetPosition().x) == ((int)p_position.x) && ((int)entity.GetPosition().y) == ((int)p_position.y))
        {
           
            SolarScriptRunnerTool solarScriptRunnerTool = new SolarScriptRunnerTool(
            GlobalGameState.GetInstance().getCurrentSystem());
            solarScriptRunnerTool.RunScript(entity.GetController().GetData().GetContactScript(), entity);
        }
    }

    private int NoEntityCollisions(int direction, Vector2 vector2)
    {
        Vector2Int p = GeometryTools.GetPosition((int)vector2.x, (int)vector2.y, direction);
        StarSystem starSystem = GlobalGameState.GetInstance().getCurrentSystem();
        for (int i = 0; i < starSystem.GetSystemContents().GetEntities().Count; i++)
        {
            if (!(starSystem.GetSystemContents().GetEntities()[i] is Active_Entity))
            {
                Vector2 cf = starSystem.GetSystemContents().GetEntities()[i].GetPosition();
                Vector2Int c = new Vector2Int((int)cf.x, (int)cf.y);
                if (c.x==p.x && c.y == p.y)
                {
                    direction = direction <= 4 ? --direction : ++direction;
                    return direction;
                }
            }

        }

        return direction;
    }

    internal void SetBusyCallback(IntCallback busyCallback)
    {
        this.busyCallback = busyCallback;
    }
}
