using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignatureAnimator : MonoBehaviour
{
    private readonly float INTERVAL = 4;
    public Animator2D animator2D;
    private float clock;
    // Start is called before the first frame update
    void Start()
    {
        clock = 0;
    }

    // Update is called once per frame
    void Update()
    {
        clock += Time.deltaTime;

        if (clock > INTERVAL)
        {
            clock = 0;
            animator2D.setAnimation(0, 20, 0.05f, false);
        }
    }
}
