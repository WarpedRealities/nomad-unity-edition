using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRenderer : MonoBehaviour
{
    SpaceGrid spaceGrid;
    public Sprite[] sprites;
    public Sprite[] hazardSprites;
    public Color[] colours;
    public GameObject prefabObfuscation;
    public GameObject prefabHazard;
    // Start is called before the first frame update
    void Start()
    {
        float[] c = GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetColor();
        float[] h = GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetHighlight();
        colours = new Color[2];
        colours[0] = new Color(c[0], c[1], c[2], c[3]);
        colours[1] = new Color(h[0], h[1], h[2], h[3]);
        spaceGrid = GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetSpaceGrid();
        BuildGrid();
    }

    private void BuildGrid()
    {
        int index = 0;
        for (int i = -128; i < 128; i++)
        {
            for (int j = -128; j < 128; j++)
            {
                int obfuscation = spaceGrid.GetObfuscation(i, j);
                if (obfuscation > 0)
                {
                    index=BuildObfuscation(obfuscation, i, j,index);
                }
                int hazard = spaceGrid.GetHazard(i, j);
                if (hazard > 0)
                {
                    index = BuildHazard(hazard, i, j, index);
                }
            }
        }
    }

    private int BuildHazard(int hazard, int x, int y, int index)
    {
        int max = 2;
        while (hazard > 0)
        {
            float x0 = x + DiceRoller.GetInstance().RollOdds();
            float y0 = y + DiceRoller.GetInstance().RollOdds();
            bool alternate = DiceRoller.GetInstance().RollDice(2) == 0;
            int m = max < hazard ? hazard : max;
            int value = DiceRoller.GetInstance().RollDice(m);
            hazard -= value;
            GameObject g = Instantiate(prefabHazard);
            g.transform.position = new Vector3(x0, y0);
            SpriteRenderer render = g.GetComponent<SpriteRenderer>();
            render.sprite = hazardSprites[(value) + (alternate ? 1 : 0)];
            HazardSprite hazardSprite = g.GetComponent<HazardSprite>();
            index++;
        }

        return index;
    }

    private int BuildObfuscation(int obfuscation, int x, int y, int index)
    {
        float x0 = x+DiceRoller.GetInstance().RollOdds();
        float y0 = y+DiceRoller.GetInstance().RollOdds();
        bool highlight = DiceRoller.GetInstance().RollDice(4) == 0;
        bool alternate = DiceRoller.GetInstance().RollDice(2) == 0;
        GameObject g = Instantiate(prefabObfuscation);
        g.transform.position = new Vector3(x0, y0);
        SpriteRenderer render = g.GetComponent<SpriteRenderer>();
        render.sprite = sprites[(obfuscation / 2) + (alternate ? 1 : 0)];
        render.material.color = highlight ? colours[1] : colours[0];
        return index + 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
