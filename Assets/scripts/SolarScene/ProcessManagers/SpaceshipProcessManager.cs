﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipProcessManager : ProcessManager
{
    private Spaceship ship;

    public SpaceshipProcessManager(Spaceship ship)
    {
        this.ship = ship;
    }

    public override void RunProcess(int duration)
    {
        //run all converters for this amount of time
        SpaceshipResourceStats res= this.ship.GetResources();

        for (int i = 0; i < res.GetConverterList().Count; i++)
        {
            ShipConverter converter = res.GetConverterList()[i];
            if (converter.IsActive())
            {
                ShipResource fromResource = res.GetResource(converter.GetFrom());
                ShipResource toResource = res.GetResource(converter.GetTo());
                if (fromResource!=null && converter.GetTo().Equals("RES_NOTHING") &&
                    fromResource.amount > converter.GetIntake() * duration)
                {
                    fromResource.amount -= converter.GetIntake() * duration;
                }
                else if (toResource!=null && converter.GetFrom().Equals("RES_NOTHING") &&
                   toResource.capacity - toResource.amount > converter.GetOutput() * duration)
                {

                    toResource.amount += converter.GetOutput() * duration;
                }
                else if (toResource!=null && fromResource!=null &&
                    fromResource.amount>converter.GetIntake()*duration &&
                    toResource.capacity - toResource.amount > converter.GetOutput() * duration)
                {
                    fromResource.amount -= converter.GetIntake() * duration;
                    toResource.amount += converter.GetOutput() * duration;
                }

            }       
        }
        if (ship.GetWarpHandler() != null)
        {
            if (ship.GetWarpHandler().state == WarpState.CHARGING)
            {
                //charge up
                float energyReq = 0.03F * duration;

                if (res.GetResource("RES_ENERGY").amount < energyReq)
                {
                    ship.GetWarpHandler().state = WarpState.FAILED;
                }
                else
                {
                    ship.GetWarpHandler().charge += (ship.GetWarpHandler().chargeIncrement / 100) * duration;
                    ship.GetWarpHandler().charge = (ship.GetWarpHandler().charge > 1) ? 1 : ship.GetWarpHandler().charge;

                    res.GetResource("RES_ENERGY").amount -= energyReq;

                }
            }
            if (ship.GetWarpHandler().state == WarpState.FAILED && ship.GetWarpHandler().charge>0)
            {
                ship.GetWarpHandler().charge -= 0.1F;
            }
        }
    }
}
