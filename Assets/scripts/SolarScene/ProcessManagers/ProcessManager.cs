﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProcessManager 
{


    public abstract void RunProcess(int duration);
}
