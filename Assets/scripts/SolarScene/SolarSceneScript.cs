﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarSceneScript : MonoBehaviour
{
    public GameObject entityTemplate, activeTemplate, signatureTemplate;
    public SolarPanelScript panelScript;
    public SpaceLog spaceLog;
    public ParallaxStars parallaxStars;
    public ParticleManager warpParticles;
    StarSystem system;

    private GameObject currentScreen;
    private Spaceship spaceship;
    private SolarSceneController sceneController;
    private PlayerEntityController playerController;
    private RegenerationTicker regenerationTicker;
    private Camera camera;
    private bool busy;
    private float clock;
    private int zoomLevel=1;
    private const float INCREMENT_TIME = 0.02F;

    private void Awake()
    {
        clock = 0;
        if (!GlobalGameState.GetInstance().IsPlaying())
        {
            GlobalGameState.GetInstance().NewGame();
            LaunchLandTool landTool = new LaunchLandTool();
            ShipAnalysisTool tool = new ShipAnalysisTool(GlobalGameState.GetInstance().getCurrentEntity().
            GetShip(GlobalGameState.GetInstance().getCurrentZone().getName()));
            landTool.TakeControl(tool);
            GlobalGameState.GetInstance().SetPlaying(true);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        busy = false;
        regenerationTicker = new RegenerationTicker(GlobalGameState.GetInstance().GetPlayer());
        system = GlobalGameState.GetInstance().getCurrentSystem();

        spaceship = (Spaceship)GlobalGameState.GetInstance().getCurrentEntity();
     
        sceneController = new SolarSceneController(system, spaceship,this);
        sceneController.SetLog(spaceLog.GetDelegate());
        playerController = new PlayerEntityController(spaceship,sceneController);
        system.RunStartScripts(sceneController.GetScriptTool());
        sceneController.Generate(entityTemplate, activeTemplate, signatureTemplate, playerController, transform);
        AlignCamera();
        panelScript.UpdateCoords();
        parallaxStars.transform.localScale=new Vector3(2,2,2);
        warpParticles.transform.SetParent(sceneController.playerEntity.transform,false);
        warpParticles.transform.localPosition = new Vector3(0, 0, 0);
    }

    public SpaceLog GetSolarLog()
    {
        return this.spaceLog;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.spaceship.GetAnimation() != null)
        {
            this.spaceship.GetAnimation().Update(this.sceneController.GetPlayerEntity(), this);
                return;
        }
        if (clock <= 0)
        {
            if (!CriticalCheck() &&
                this.spaceship.GetAnimation() == null)
            {
                if (playerController.isBusy())
                {
                    UpdateLogic();
                    if (!playerController.isBusy())
                    {
                        panelScript.UpdateUI();
                        panelScript.UpdateCoords();
                    }
                }
                else if (currentScreen == null)
                {
                    if (busy)
                    {
                        panelScript.UpdateUI();
                        busy = false;
                    }

                    //receive player input
                    if (playerController.TakeAction())
                    {
                        busy = true;
                        AlignCamera();
                        panelScript.UpdateUI();
                        panelScript.UpdateCoords();
                    }
                }
                if (busy)
                {
                    clock = INCREMENT_TIME;
                }
            }    
        }
        else
        {
            clock -= Time.deltaTime;
        }
       
    }

    private bool CriticalCheck()
    {
        if (GameObject.Find("Encounter") != null)
        {
            return false;
        }
        if (spaceship.GetWarpHandler() != null &&
            spaceship.GetWarpHandler().state == WarpState.CHARGING &&
            spaceship.GetWarpHandler().charge >= 1) {
            spaceship.GetWarpHandler().state = WarpState.JUMPOUT;
            spaceship.GetWarpHandler().jumpStart = GlobalGameState.GetInstance().GetUniverse().GetClock();
            spaceship.SetAnimation(new ShipWarpAnimation(spaceship.GetWarpHandler()));
            this.SetScreen(null);
        }
        int hp = spaceship.GetCommon().GetHp();
        if (hp <= 0)
        {
            TextLog.Log("Critical damage sustained, flight impossible");
            LaunchLandTool launchLandTool = new LaunchLandTool();
            ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(GlobalGameState.GetInstance().getCurrentEntity().
                GetShip(GlobalGameState.GetInstance().getCurrentZone().getName()));
            shipAnalysisTool.ChanceOfDamage();
            launchLandTool.RelinquishControl(shipAnalysisTool);
            return true;
        }
        return false;
    }

    void UpdateLogic() 
    {
        sceneController.UpdateWorld();

        GlobalGameState.GetInstance().IncrementClock(5);

        regenerationTicker.Update(5);

        if (warpParticles.IsActive())
        {
            if (spaceship.GetWarpHandler() != null)
            {
                UpdateParticles();
            }
            else
            {
                warpParticles.SetActive(false);
            }
        }
    }

    public void UpdateParticles()
    {
        warpParticles.spawnRate = spaceship.GetWarpHandler().state == WarpState.FAILED ?
            0:
            (int)(spaceship.GetWarpHandler().charge * 8.0F);
        warpParticles.particleColour =
            new Color(
                1 - (spaceship.GetWarpHandler().charge*0.5F),
                0.5F,
                0.5F + (spaceship.GetWarpHandler().charge/2)
            );
    }

    void AlignCamera()
    {
        
        camera.transform.position = new Vector3(CamOffset(), spaceship.GetPosition().y,-10);
        parallaxStars.SetPosition(-spaceship.GetPosition().x, -spaceship.GetPosition().y);
    }

    float CamOffset()
    {
        float sWidth = Screen.width;
        float baseUIWidth = 230;
        float pixelPerUnit = 32;
        float offsetX = -(baseUIWidth /2 / pixelPerUnit) * GetScaleLookup(zoomLevel);
        float sPosX = spaceship.GetPosition().x;
        return sPosX - offsetX;

    }

    public void SetZoom(bool reduce)
    {
        if (reduce && zoomLevel > 0)
        {
            zoomLevel--;
        }
        if (!reduce && zoomLevel < 4)
        {
            zoomLevel++;
        }

        float s = GetScaleLookup(zoomLevel);
        
        camera.orthographicSize = s*10;
        parallaxStars.transform.localScale = new Vector3(s, s, s);
        AlignCamera();
    }

    private float GetScaleLookup(int zoomLevel)
    {
        switch (zoomLevel)
        {
            case 0:
                return 0.5F;
            case 1:
                return 1.0F;
            case 2:
                return 2.0F;
            case 3:
                return 4.0F;
            case 4:
                return 8.0F;
        }
        return 0;
    }

    public void SetScreen(GameObject screen)
    {
        if (screen != null)
        {
            screen.SendMessage("SetSolarScene", this);
            this.currentScreen = screen;
        }
        else if (currentScreen!=null)
        {
            Destroy(currentScreen);
            currentScreen = null;
        }
        this.panelScript.UpdateFTL(currentScreen == null);
    }


    public void SetSystemScreen()
    {
        if (currentScreen == null)
        {
            GameObject screen = (GameObject)GameObject.Instantiate(Resources.Load("SolarSystemPanel"), GameObject.Find("Canvas").transform);
            SetScreen(screen);
        }
    }

    public void SetFtlScreen()
    {
        if (currentScreen == null)
        {
            GameObject screen = (GameObject)GameObject.Instantiate(Resources.Load("NavigationScreen"), GameObject.Find("Canvas").transform);
            SetScreen(screen);
        }
    }

    internal void PassTime(int duration)
    {
        busy = true;
        playerController.SetBusy(duration);
        AlignCamera();
        panelScript.UpdateUI();
        panelScript.UpdateCoords();
        clock = INCREMENT_TIME;
    }

    public void UpdatePlayer()
    {
        playerController.Redraw();
    }

    public void SetWarpEffect(bool active)
    {
        this.warpParticles.SetActive(active);
        UpdateParticles();
    }

}
