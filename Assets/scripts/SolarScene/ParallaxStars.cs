﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxStars : MonoBehaviour
{
    public Sprite[] sprites;
    public GameObject prefabStar;

    private const int WIDTH = 32;
    private const int HEIGHT = 32;
    private const float DIVIDER = 16;
    // Start is called before the first frame update
    void Start()
    {
        BuildStars();
    }

    public void SetPosition(float x, float y)
    {
        transform.localPosition = new Vector3(x/ DIVIDER, y/ DIVIDER, 12);
    }

    private void BuildStars()
    {
        for (int i = 0; i < WIDTH; i++)
        {
            for (int j = 0; j < HEIGHT; j++)
            {
                float x = (i-0.5F+DiceRoller.GetInstance().RollOdds()-(WIDTH/2))*2;
                float y = (j-0.5F + DiceRoller.GetInstance().RollOdds()-(HEIGHT/2))*2;
                GameObject g= Instantiate(prefabStar, transform);
                g.transform.localPosition = new Vector3(x, y,0);
                g.GetComponent<SpriteRenderer>().sprite = sprites[DiceRoller.GetInstance().RollDice(16)];
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
