﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarSystemGenerator 
{
    List<GameObject> sprites;
    private GameObject templateEntity,activeEntity, signatureEntity;

    public SolarSystemGenerator(GameObject templateEntity, GameObject activeEntity, GameObject signatureEntity)
    {
        sprites = new List<GameObject>();
        this.activeEntity = activeEntity;
        this.templateEntity = templateEntity;
        this.signatureEntity = signatureEntity;
    }


    public List<GameObject> GetSprites()
    {
        return sprites;
    }

    public void Generate(StarSystemContents system)
    {
        for (int i = 0; i < system.GetEntities().Count; i++)
        {
            Entity entity = system.GetEntities()[i];
            if (entity.GetInGame())
            {
                // Debug.Log("sprite from entity " + entity.getSprite());
                if (entity.NeedsGeneration())
                {
                    //      Debug.Log("build for solar");
                    entity.generateForSolar();
                }
                GameObject sprite = CreateObject(entity);
                SpriteRenderer renderer = sprite.GetComponent<SpriteRenderer>();
                if (!(entity is SignatureEntity))
                {
                    renderer.sprite = SolarSpriteRepo.GetInstance().GetSprite(entity.getSprite());
                }
                sprite.transform.position = new Vector3(entity.GetPosition().x, entity.GetPosition().y, 0);
                float smultiplier = 1 / (renderer.sprite.pixelsPerUnit / 16);
                sprite.transform.localScale = new Vector3(entity.getSize() * smultiplier, entity.getSize() * smultiplier);
                sprites.Add(sprite);
                sprite.GetComponent<EntityScript>().entity = entity;
                sprite.transform.eulerAngles = new Vector3(0, 0, -entity.getRotation());
                if (!entity.isAlwaysVisible())
                {
                    renderer.enabled = false;
                }
            }
        }
    }

    private GameObject CreateObject(Entity entity)
    {
        if (entity is Active_Entity)
        {
            return GameObject.Instantiate(activeEntity);
        }
        if (entity is SignatureEntity)
        {
            return GameObject.Instantiate(signatureEntity);
        }
        return GameObject.Instantiate(templateEntity);
    }
}
