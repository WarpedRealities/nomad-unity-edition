﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SolarControl 
{
    Entity GetEntityAtPosition(int x, int y, Entity exclude);

    bool InitiateInteraction(Entity entity);

    int GetHazard(int x, int y);

    void DrawText(string text);

    void RefreshSprites();
}
