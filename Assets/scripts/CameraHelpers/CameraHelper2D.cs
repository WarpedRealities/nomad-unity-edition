﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//forces the gameobject to load a camera if none is present
[RequireComponent(typeof(Camera))]
public class CameraHelper2D : MonoBehaviour
{

    //dimensions of camera in pixels
    public int width = 640;
    public int height = 640;

    //pixel count within a single Unity unit, does not support non square pixels.
    public int pixelsPerUnit = 32;

    //the camera that is attached to this gameobject
    private Camera thisCamera;

    //the rendertexture that the camera will be output to
    private RenderTexture targetTexture;

    // Start is called before the first frame update
    void Start()
    {
        //width and height have to be even numbers
        width -= width % 2;
        height -= height % 2;

        //get the camera on this gameobject
        thisCamera = gameObject.GetComponent<Camera>();

        //force the camera to render in orthographic
        thisCamera.orthographic = true;

        //create a rendertexture for this camera to output to
        targetTexture = new RenderTexture(width, height, 24);
        targetTexture.filterMode = FilterMode.Point;
        targetTexture.Create();


        //set the camera to render to that rendertexture
        thisCamera.targetTexture = targetTexture;

        //set the camera size to match the height (width is implicit)
        thisCamera.orthographicSize = (float)height / (float)(pixelsPerUnit*2);


    }

    // Update is called once per frame
    void Update()
    {
        //RenderTextures can spontaneously delete themselves so it needs to be checked every frame and restored if it no longer exists
        if (!targetTexture.IsCreated())
        {
            targetTexture.Create();
        }
    }

    // LateUpdate is called once per frame and after every Update has been called but before any cameras are rendered
    void LateUpdate()
    {
        //copy the world space position of this objects parent
        Vector3 newPos = this.transform.parent.position;

        //snap that position to the nearest whole pixel value
        newPos.x = Mathf.Floor(newPos.x * pixelsPerUnit + 0.5f) / pixelsPerUnit;
        newPos.y = Mathf.Floor(newPos.y * pixelsPerUnit + 0.5f) / pixelsPerUnit;

        //apply the snapped position to this objects transform, preventing it from causing distorted pixel artifacts
        this.transform.position = newPos;

    }

    /// <summary>
    /// Returns the RenderTexture from this camera for use by UI elements.
    /// </summary>
    /// <returns></returns>
    public RenderTexture GetTexture()
    {
        return targetTexture;
    }

    public void ZoomIn()
    {
        thisCamera.orthographicSize = (float)height / (float)(pixelsPerUnit);
    }

    public void ZoomOut()
    {
        thisCamera.orthographicSize = (float)height / (float)(pixelsPerUnit * 2);
    }
}
