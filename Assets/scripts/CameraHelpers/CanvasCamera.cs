﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//forces the gameobject to load a RawImage if none is present
[RequireComponent(typeof(RawImage))]
public class CanvasCamera : MonoBehaviour
{
    //the RawImage that to display the camera
    private RawImage raw;

    //the UI transform to adjust the size to match the camera
    private RectTransform rect;

    //the camera to grab from
    public GameObject gameCamera;

    
    public RectTransform sideBar;//the sidebar panel that needs to be scaled

    //public RectTransform sideBarCombined;

    //the text scroll panel that needs to be scaled
    public RectTransform textScroll;

    public int textScrollMinHeight;
    public int sideBarMinWidth;

    public int combinedMinWidth;
    public int combinedMinHeight;

    private int combinedBarHeight;

    //The width and height of the screen on the previous frame
    //if either of these changes then CanvasCamera will recalculate its size and reposition the sideBar and textScroll
    private int screenW = 0;
    private int screenH = 0;
    private int width, height;

    private bool zoomed = false;

    private float scaleOut;

    // Start is called before the first frame update
    void Start()
    {
        //get raw and rect
        raw = gameObject.GetComponent<RawImage>();
        rect = gameObject.GetComponent<RectTransform>();

        combinedBarHeight = (int)sideBar.sizeDelta.y;


    }

    // Update is called once per frame
    void Update()
    {
        
        //if there is no texture currently assigned
        if (raw.texture == null)
        {
            //set the texture from the target camera
            raw.texture = gameCamera.GetComponent<CameraHelper2D>().GetTexture();
            //this is in update because it cannot be trusted to work in start
        }

        //if the window is resized
        if (screenH != Screen.height || screenW != Screen.width)
        {
            //update the size of the RawImage
            Resize();
        }
        

    }

    /// <summary>
    /// Resizes the attached RawImage so that it perfectly lines up with the pixels of the screen without overcrowding the other UI windows
    /// </summary>
    void Resize()
    {
        //this is only called when the window is resized so update that size now
        screenH = Screen.height;
        screenW = Screen.width;

        //get the dimensions of the camera output
        int width = gameCamera.GetComponent<CameraHelper2D>().width;
        int height = gameCamera.GetComponent<CameraHelper2D>().height;
        //Debug.Log("width " + width);
        //Debug.Log("height" + height);
        //calculate the largest possible scale that will preserve square pixels and fit within the window
        int scale = Mathf.Min(Mathf.FloorToInt(Screen.width / width), Mathf.FloorToInt(Screen.height / height));

        //do not let the scale be set to zero, a cropped image is better than no image
        scale = Mathf.Max(scale, 1);
        scaleOut = scale;
        bool needScale = true;
        for (int i = scale; i > 1; i--)
        {
            Debug.Log("int i " + i);
            if (Screen.width - width * i >= sideBarMinWidth && Screen.height - height * i >= textScrollMinHeight && needScale)
            {
                LayoutBottom(height, width, i);
                needScale = false;
                i = -1;
            }
            else if (Screen.width - width * i >= combinedMinWidth && Screen.height>= combinedMinHeight && needScale)
            {
                LayoutRight(height, width, i);
                needScale = false;
                i = -1;
            }
            
        }

        if (needScale)
        {
            LayoutBottom(height, width, 1);
        }
    }
    public float GetScaleOut()
    {
        return this.scaleOut;
    }
    public bool IsZoom()
    {
        return zoomed;
    }

    void LayoutBottom(int height, int width, int scale)
    {
        //Debug.Log("layout bottom " + height + " " + width+" "+scale);
        //apply the new scale to the camera
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width * scale);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height * scale);

        //apply the new scale to the text scroll
        textScroll.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Screen.width);
        textScroll.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Mathf.Max(textScrollMinHeight, Screen.height - height * scale));

        //apply the new scale to the side bar
        //sideBarCombined.gameObject.SetActive(false);
        //sideBar.gameObject.SetActive(true);
        sideBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Max(textScrollMinHeight, Screen.width - width * scale));
        sideBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height * scale);

        sideBar.gameObject.SendMessage("PositionStatus");
    }

    void LayoutRight(int height, int width, int scale)
    {
        Debug.Log("layout right " + height + " " + width + " " + scale);
        //apply the new scale to the camera
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width * scale);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height * scale);

        //apply the new scale to the text scroll
        textScroll.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Max(textScrollMinHeight, Screen.width - width * scale));
        textScroll.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Screen.height - combinedBarHeight);

        //apply the new scale to the side bar
        //sideBarCombined.gameObject.SetActive(true);
        //sideBar.gameObject.SetActive(false);
        sideBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Max(textScrollMinHeight, Screen.width - width * scale));
        sideBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, combinedBarHeight);
        sideBar.anchoredPosition = new Vector3(0,0);
        sideBar.gameObject.SendMessage("PositionStatus");
    }


    public void Zoom()
    {
        if (zoomed)
        {
            gameCamera.GetComponent<CameraHelper2D>().ZoomOut();
            zoomed = false;
        }
        else
        {
            gameCamera.GetComponent<CameraHelper2D>().ZoomIn();
            zoomed = true;
        }
    }

}


