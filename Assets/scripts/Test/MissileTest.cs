using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileTest : MonoBehaviour
{
    float facing = 0;
    public GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RunFacing();
        //RunInput();

        RunMoving();
    }

    private void RunInput()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            facing += 1;
            if (facing > 360)
            {
                facing = facing - 360;
            }
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            facing -= 1;
            if (facing < 0)
            {
                facing = 360 + facing;
            }
        }
        transform.rotation = Quaternion.identity;
        transform.Rotate(new Vector3(0,
        0, facing));
    }

    private void RunMoving()
    {
        float radianFacing = facing / 57.3F;
        Vector3 offset = new Vector3(1 * -Mathf.Sin(radianFacing), 1 * Mathf.Cos(radianFacing),0);
        Debug.Log("offset" + offset);
        transform.position = transform.position + (offset* Time.deltaTime);
    }

    public void RunFacing()
    {
        float dt = Time.deltaTime;
        float radianFacing = facing / 57.3F;
        Vector3 offset = new Vector3(1 * -Mathf.Sin(radianFacing), 1 * Mathf.Cos(radianFacing)).normalized;
        float angle = Vector2.SignedAngle(((Vector2)target.transform.position- (Vector2)transform.position).normalized, offset);
        Debug.Log("angle " + angle);
        Debug.Log("facing " + facing);
        if (angle > 5 || angle < -5)
        {
            facing += angle > 0 ? dt * -90 : dt * 90;
            if (facing > 360)
            {
                facing = facing - 360;
            }
            if (facing < 0)
            {
                facing = 360 + facing;
            }
            transform.rotation = Quaternion.identity;
            transform.Rotate(new Vector3(0,
            0, facing));
        }
    }
}
