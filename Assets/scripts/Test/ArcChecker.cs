using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcChecker : MonoBehaviour
{
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = target.transform.position;
      
        if (!ArcCheck(position, 22))
        {
            transform.Rotate(0, 0, 1 * Time.deltaTime);
        }
    }

    private bool ArcCheck(Vector3 position, float threshold)
    {
        //establish vector
        //Debug.Log("euler angles" + transform.eulerAngles);
        Vector2 axis = new Vector2(0, 1);
        Vector2 angle = Quaternion.AngleAxis(45 * 0, Vector3.forward) * axis;

        Vector2 finalAngle = Quaternion.AngleAxis(transform.eulerAngles.z, Vector3.forward) * angle;
        Vector3 target = (position - transform.position).normalized;
        float degrees = Vector2.Angle(finalAngle, target);
      
        return degrees < threshold;
    }
}
