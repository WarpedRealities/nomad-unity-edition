using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageComparisons : MonoBehaviour
{
    int MAXRUNS = 100;
    int knifeVictoryCount = 0, seductionVictoryCount = 0;
    int currentRuns = 0;
    int MAXRESOLVE = 40, MAXHEALTH = 50;
    int health = 0, resolve = 0;
    int knifeDifficulty = 13, seductionDifficulty = 8;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (MAXRUNS > currentRuns)
        {
            DoRun();
            
        }
    }

    private void DoRun()
    {
        SeductionAttack();
        KnifeAttack();
        if (health <= 0)
        {
            knifeVictoryCount++;
            Debug.Log("knife victory, count now at " + knifeVictoryCount);
            NewRun();
        }
        if (resolve <= 0)
        {
            seductionVictoryCount++;
            Debug.Log("seduction victory, count now at " + seductionVictoryCount);
            NewRun();
        }
    }

    private void NewRun()
    {
        currentRuns++;
        health = MAXHEALTH;
        resolve = MAXRESOLVE;
        if (currentRuns == MAXRUNS)
        {
            PostFinalResults();
        }
    }

    private void PostFinalResults()
    {
        Debug.Log("knife victories " + knifeVictoryCount);
        Debug.Log("seduction victories " + seductionVictoryCount);
    }

    private void KnifeAttack()
    {
        int roll = DiceRoller.GetInstance().RollDice(20);
        if (roll >= knifeDifficulty)
        {
            int dmg = DiceRoller.GetInstance().RollDice(5) + 2;
            health -= dmg;
            Debug.Log("knife hit, inflict dmg " + dmg + " target health now " + health);
        }
        else
        {
            Debug.Log("knife attack missed");
        }


    }

    private void SeductionAttack()
    {
        int roll = DiceRoller.GetInstance().RollDice(20);
        if (roll >= seductionDifficulty)
        {
            int dmg = DiceRoller.GetInstance().RollDice(5)+1;
            resolve -= dmg;
            Debug.Log("seduction hit, inflict resolve " + dmg + " target resolve now " + resolve);
        }
        else
        {
            Debug.Log("seduction attack missed");
        }

    }
}
