using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class LineOfSightTest : MonoBehaviour
{
    public GameObject[] markers;
    public GameObject[] dMarkers;
    public GameObject prefabMarker;
    public GameObject prefabDMarker;
    LineOfSight2 lineOfSight;
    VisitDelegate visitDel;
    DebugDelegate debugDelegate;
    float clock = 0;
    readonly float INTERVAL = 1.0F;
    int index = 0, markerIndex=0, dMarkerIndex=0;
    private void Awake()
    {
        markers = new GameObject[32];
        dMarkers = new GameObject[32];
        for (int i = 0; i < 32; i++)
        {
            markers[i] = Instantiate(prefabMarker);
            dMarkers[i] = Instantiate(prefabDMarker);
        }
        lineOfSight = new LineOfSight2();
        debugDelegate = this.DebugVisit;
        visitDel = this.Visit;
    }

    void Start()
    {

    }

    void DebugVisit(Vector2 point)
    {
        
        Debug.Log("visiting " + point+ " index "+markerIndex);
        dMarkers[dMarkerIndex].transform.position = new Vector2(point.x, point.y);
        dMarkerIndex++;
        if (dMarkerIndex >= this.dMarkers.Length)
        {
            dMarkerIndex = 0;
        }
        
    }
    void Visit(Vector2Int point)
    {
        Debug.Log("visiting " + point + " index " + markerIndex);
        markers[markerIndex].transform.position = new Vector2(point.x + 0.5F, point.y + 0.5F);
        markerIndex++;
        if (markerIndex >= this.markers.Length)
        {
            markerIndex = 0;
        }
    }
    // Update is called once per frame
    void Update()
    {
        clock += Time.deltaTime;
        if (clock > INTERVAL) {
            CastRay();
            clock = 0;
        }
    }

    private void CastRay()
    {
        CleanupMarkers();

        Vector2Int position = GetPosition(index);
        lineOfSight.GenerateRay(new Vector2(0, 0), position, visitDel, null , debugDelegate);
        index++;
        if (index >= 80)
        {
            index = 0;
        }
    }

    private void PlaceMarkers(Vector2[] points)
    {
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i] != null)
            {
                markers[i].transform.position = new Vector3(points[i].x+0.5F, points[i].y+0.5F);
            }
        }
    }

    private Vector2Int GetPosition(int index)
    {
        if (index>=0 && index < 20)
        {
            return new Vector2Int(-10 + index, 10);
        }
        if (index >= 20 && index < 40)
        {
            return new Vector2Int(10, 30-(index));
        }
        if (index >= 40 && index < 60)
        {
            return new Vector2Int(50 - index, -10);
        }
        if (index >= 60 && index < 80)
        {
            return new Vector2Int(-10, -70 + (index));
        }
        return new Vector2Int(0, 0);
    }

    private void CleanupMarkers()
    {
        for (int i = 0; i < markers.Length; i++)
        {
            markers[i].transform.position = new Vector3(-32, -32);
            dMarkers[i].transform.position = new Vector3(-32, -32);
        }
    }
}