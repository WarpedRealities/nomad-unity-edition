using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v = new Vector3(0, 0, 0);
        if (Input.GetKey(KeyCode.D))
        {
            v.x += 4;
        }
        if (Input.GetKey(KeyCode.A))
        {
            v.x -= 4;
        }
        if (Input.GetKey(KeyCode.W))
        {
            v.y += 4;
        }
        if (Input.GetKey(KeyCode.S))
        {
            v.y -= 4;
        }
        transform.Translate(v * Time.deltaTime);
    }
}
