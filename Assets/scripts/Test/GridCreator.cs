using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCreator : MonoBehaviour
{
    public GameObject prefab;

    private void Awake()
    {
        int x = -16;
        int y = -16;
        for (int i = 0; i < 32; i++)
        {
            for (int j = 0; j < 32; j++)
            {
                GameObject square = Instantiate(prefab);
                square.transform.position = new Vector3(x + i+0.5F, y + j+0.5F);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
