using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MapGeneratorTest : MonoBehaviour
{
    public Image image;
    private Texture2D drawnTexture;
    Zone zone;

    private void Awake()
    {
        zone = new Zone("stations/purple_derelict/","upper_deck",null);
        //zone = new Zone("worlds/alpha_minoris_IIA/", "omnico", null);
        zone.SetEntity(new Planet(new Vector2Int(0, 0), "alpha_minoris_II"));
        for (int i = 0; i < 32; i++)
        {
            zone.Generate();
            drawnTexture = new Texture2D(128, 64);
            BuildTexture(zone.GetContents().getTiles());
            //image.sprite = Sprite.Create(drawnTexture, new Rect(new Vector2(0, 0), new Vector2(64, 64)), new Vector2(0, 0));
            ExportTexture(drawnTexture,"file "+i);
            zone.SetContents(null);

        }

    }

    private void ExportTexture(Texture2D drawnTexture, string filename)
    {
        byte []bytes= drawnTexture.EncodeToPNG();
        var dirPath = Application.dataPath + "/../SaveImages_hulk/";
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }
        File.WriteAllBytes(dirPath + filename + ".png", bytes);
    }

    private void BuildTexture(Tile[][] tiles)
    {
        
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[0].Length; j++)
            {
                if (tiles[i][j] != null)
                {
                    if (tiles[i][j].canWalk())
                    {

                        drawnTexture.SetPixel(i, j, Color.yellow);
                    }
                    else
                    {

                        drawnTexture.SetPixel(i, j, Color.red);
                    }
                }
            }
        }
        drawnTexture.Apply();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
