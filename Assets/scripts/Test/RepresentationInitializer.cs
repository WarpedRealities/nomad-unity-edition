using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepresentationInitializer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Material material = SpriteLibrary.getInstance().GetMaterial("40mm_solid",
    "representation/");
        RepresentationAnimation representationAnimation = GetComponent<RepresentationAnimation>();
        RepDataEntry repDataEntry = new RepDataEntry(0, 48, 1, 16, 16, true);
        representationAnimation.Generate(repDataEntry, material);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
