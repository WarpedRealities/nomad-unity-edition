﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PerkSelectPanel : MonoBehaviour
{
    Player player;
    PlayerRPG playerRPG;
    List<PerkOption> perkList;
    public NuList listUI;
    public Button button;
    public Text selectText, descriptionText;
    private BooleanDelegate SelectedCallback;
    int index = -1, selectedIndex=-1;

    // Start is called before the first frame update
    void Start()
    {
        ListCallback listCallback = List_Callback;
        listUI.SetCallback(listCallback);
    

        //set the description to that of this one


    }

    public void List_Callback(int index)
    {
        this.index = index;
  
        //set the description to that of this one
      
        descriptionText.text = perkList[index].GetPerk().GetDescription().Replace("\t","");
    }

    public void SelectButton()
    {
        if (index != -1)
        {
            if (selectedIndex != -1)
            {
                //deselect
                button.GetComponent<MonoButton>().SetHighlight(false);
                selectedIndex = -1;
                selectText.text = "Perk:";
                SelectedCallback(false);
            }
            else
            {
                button.GetComponent<MonoButton>().SetHighlight(true);
                selectedIndex = index;
                selectText.text = "Perk:" + perkList[selectedIndex].GetString();
                SelectedCallback(true);
            }
        }
    }

    public void SetCallback(BooleanDelegate bDelegate)
    {
        SelectedCallback = bDelegate;
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
        this.playerRPG = (PlayerRPG)player.GetRPG();
        PerkListGenerator perkListGenerator = new PerkListGenerator(player, playerRPG);
        perkList = perkListGenerator.GetPerks();

        BuildList();
        this.index = 0;
        descriptionText.text = perkList[index].GetPerk().GetDescription().Replace("\t", "");

    }

    private void BuildList()
    {
        List<string> strings = new List<string>();
        for (int i = 0; i < perkList.Count; i++)
        {
            strings.Add(perkList[i].GetString());
        }

        listUI.SetList(strings);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal Perk GetPerk()
    {
        return perkList[selectedIndex].GetPerk();
    }
}
