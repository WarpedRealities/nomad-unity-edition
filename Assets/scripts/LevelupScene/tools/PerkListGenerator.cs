﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkOption
{
    Perk perk;
    int rank;

    public PerkOption(Perk perk, int rank)
    {
        this.perk = perk;
        this.rank = rank;
    }

    public Perk GetPerk()
    {
        return perk;
    }

    public int GetRank()
    {
        return rank;
    }

    public string GetString()
    {
        return perk.GetName() + " " + rank;
    }
}

public class PerkListGenerator
{
    private Player player;
    private PlayerRPG playerRPG;

    public PerkListGenerator(Player player, PlayerRPG playerRPG)
    {
        this.player = player;
        this.playerRPG = playerRPG;
    }

    internal List<PerkOption> GetPerks()
    {
        PerkLibrary perkLibrary = GlobalGameState.GetInstance().GetUniverse().GetPerkLibrary();
        List<Perk> possiblePerks = perkLibrary.GetPerks();
        List <PerkOption> availablePerks=new List<PerkOption>();
        List<PerkInstance> playerPerks = playerRPG.GetPerks();

        for (int i = 0; i < possiblePerks.Count; i++)
        {
            //do we have the perk already?
            PerkInstance playerPerk = HasPerk(playerPerks, possiblePerks[i]);
            if (playerPerk!=null)
            {
                if (possiblePerks[i].GetMaxRank() > playerPerk.GetRank())
                {
                    availablePerks.Add(new PerkOption(possiblePerks[i], playerPerk.GetRank() + 1));
                }
            }
            else
            {
                //do we meet the requirements for the perk?
                if (PerkRequirement(possiblePerks[i]))
                {
                    availablePerks.Add(new PerkOption(possiblePerks[i], 1));
                }
            }
            
        }

        return availablePerks;
    }

    private bool PerkRequirement(Perk perk)
    {
        return perk.EligibleFor((PlayerRPG)player.GetRPG(), player.GetAppearance());
    }

    private PerkInstance HasPerk(List<PerkInstance> playerPerks, Perk perk)
    {
        for (int i = 0; i < playerPerks.Count; i++)
        {
            if (playerPerks[i].GetRef().GetCode().Equals(perk.GetCode()))
            {
                return playerPerks[i];
            }
        }
        return null;
    }
}
