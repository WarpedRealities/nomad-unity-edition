﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public delegate void BooleanDelegate(bool value);


public class LevelUpPanel : MonoBehaviour
{
    GlobalGameState globalGameState;
    Player player;
    PlayerRPG playerRPG;
    public Text[] texts;
    public Button[] buttons;
    public Button levelUpButton;
    public GameObject[] selectionIcons; 
    int selectionCount = 0;
    private int SELECTMAX = 2;
    int [] selections = {-1,-1};
    bool skillSelect = false, perkSelect=false;
    PerkSelectPanel perkSelectPanel;
    public GameObject alternatePanel;
    // Start is called before the first frame update
    void Start()
    {
        perkSelectPanel = GameObject.Find("PerkSelectPanel").GetComponent<PerkSelectPanel>();
        globalGameState = GlobalGameState.GetInstance();
        if (!globalGameState.IsPlaying())
        {
            globalGameState.NewGame();
        }
        
        player = GlobalGameState.GetInstance().GetPlayer();
        playerRPG = (PlayerRPG)player.GetRPG();
        SELECTMAX = ((PlayerRPG)player.GetRPG()).GetLevel() % 2 == 0 ? 2 : 1; 
        BuildTexts();
        ConfigureButtons();
        perkSelect = !((playerRPG.GetLevel() + 1) % 2 == 0);
        perkSelectPanel.gameObject.SetActive(!perkSelect);
        alternatePanel.SetActive(perkSelect);
        if (!perkSelect)
        {
            perkSelectPanel.SetPlayer(player);
            BooleanDelegate bDelegate = PerkSelected;
            perkSelectPanel.SetCallback(bDelegate);
        }
        if (SELECTMAX == 1)
        {
            selectionIcons[1].SetActive(false);
        }
    }

    public void PerkSelected(bool value)
    {
        perkSelect = value;
        LevelUpUpdate();
    }

    private void ConfigureButtons()
    {
        SkillSelection skillSelection = playerRPG.GetSkillSelection();

        for (int i = 0; i < buttons.Length; i++)
        {
            int skillCap = PlayerStatTool.GetSkillCap(playerRPG.GetLevel(),
                playerRPG, (SK)i);
            if (skillSelection.GetSkill(i) >= skillCap)
            {
                buttons[i].interactable = false;
                buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = "skill cap";
            }
        }
    }

    private void BuildTexts()
    {
        for (int i = 0; i < texts.Length; i++)
        {
            if (IsSelection(i))
            {
                texts[i].text = GetSkillString(i) + " " + (this.playerRPG.GetSkill(i)+1);
                
            }
            else
            {
                texts[i].text = GetSkillString(i) + " " + this.playerRPG.GetSkill(i);

            }
        }
        skillSelect = selectionCount >= SELECTMAX;
        LevelUpUpdate();
        
    }

    private bool IsSelection(int i)
    {
        return (selections[0] == i || selections[1] == i);
    }

    public void LevelUpUpdate()
    {
        levelUpButton.interactable = skillSelect && perkSelect;
    }

    private string GetSkillString(int i)
    {
        switch (i)
        {
            case 0:
                return "melee";
            case 1:
                return "ranged";
            case 2:
                return "seduction";
            case 3:
                return "parry";
            case 4:
                return "dodge";
            case 5:
                return "willpower";
            case 6:
                return "struggle";
            case 7:
                return "pleasure";
            case 8:
                return "persuade";
            case 9:
                return "tech";
            case 10:
                return "science";
            case 11:
                return "piloting";
            case 12:
                return "gunnery";
            case 13:
                return "";
        }

        return "null";
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void PushButton(int index)
    {
        if (this.IsSelection(index))
        {
            RemoveSelection(index);
        }
        else if (selectionCount < SELECTMAX)
        {
            AddSelection(index);
        }

        BuildTexts();
        UpdateButtons();
        UpdateSelections();
    }

    private void AddSelection(int index)
    {
        selectionCount++;

        if (-1 == selections[0])
        {
            selections[0] = index;
            return;
        }
        if (-1 == selections[1])
        {
            selections[1] = index;
            return;
        }

    }

    private void RemoveSelection(int index)
    {
        selectionCount--;
        if (index == selections[0])
        {
            selections[0] = - 1;

        }
        if (index == selections[1])
        {
            selections[1] = -1;

        }

    }

    private void UpdateSelections()
    {
        for (int i = 0; i < 2; i++)
        {
            selectionIcons[i].SetActive(i >= selectionCount); 
        }
        if (SELECTMAX == 1)
        {
            selectionIcons[1].SetActive(false);
        }
    }

    public void UpdateButtons()
    {
        for (int i = 0; i < this.buttons.Length; i++)
        {
            this.buttons[i].GetComponent<MonoButton>().SetHighlight(this.IsSelection(i));
        }
    }

    public void CancelClick()
    {
        SceneManager.LoadScene("CharacterScene");
    }
    public void LevelUp()
    {
        playerRPG.GetSkillSelection().SetSkill(selections[0], playerRPG.GetSkillSelection().GetSkill(selections[0]) +1);
        if (selections[1] != -1)
        {
            playerRPG.GetSkillSelection().SetSkill(selections[1], playerRPG.GetSkillSelection().GetSkill(selections[1]) + 1);
        }
        //add perk
        if (((playerRPG.GetLevel() + 1) % 2 == 0))
        {
            playerRPG.AddPerk(perkSelectPanel.GetPerk());
        }

        playerRPG.LevelUp();
        
        SceneManager.LoadScene("CharacterScene");
    }
}
