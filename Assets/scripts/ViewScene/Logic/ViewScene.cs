﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Accessibility;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class ViewScene : MonoBehaviour, ViewInterface
{
    public GameObject UIController;
    public GameObject zoneActorPrefab;
    public GameObject zoneRenderer;

    private GameObject currentScreen;
    private bool screenActive;

    private IndicatorController indicatorController;
    private UIController uiController;
    internal PlayerController GetPlayer()
    {
        return playerController;
    }

    public BrainBank brainBank; 
    private GameObject cameraRig;
    private LineOfSightManager lineOfSight;
    public List<GameObject> zoneActors;
    private ViewSceneController controller;
    private GlobalGameState globalGameState;
    private float clock = 0;
    private int refreshClock = 0;
    private PlayerController playerController;
    private bool controlMode = false, screenLock=false;

    private int width, height;
    private bool visionRecalc;
    private LuaSensor luaSensor;
    private int skipTicks;

    // Start is called before the first frame update
    void Start()
    {
        skipTicks = Config.GetInstance().GetSkipTicks();
        uiController = UIController.GetComponent<UIController>();
        controller.InitialUpdate(this);
    }
    void Awake()
    {
        indicatorController = gameObject.GetComponent<IndicatorController>();
        brainBank = new BrainBank();
        if (cameraRig == null)
        {
            cameraRig = GameObject.Find("CameraRig");
            CameraHelper2D helper = cameraRig.GetComponentsInChildren<CameraHelper2D>()[0];
            width = helper.width / helper.pixelsPerUnit;
            height = helper.height / helper.pixelsPerUnit;
        }

        globalGameState = GlobalGameState.GetInstance();
        if (!globalGameState.IsPlaying())
        {
            globalGameState.NewGame();
            globalGameState.SetPlaying(true);
        }

        controller = new ViewSceneController(globalGameState, brainBank);
  
       lineOfSight = new LineOfSightManager();
    
        zoneActors = new List<GameObject>();
         lineOfSight.RunVision(globalGameState.GetPlayer().GetPosition(), globalGameState.getCurrentZone().GetContents(),this,globalGameState.GetPlayer());
        MoveCamera(globalGameState.GetPlayer().GetPosition());
        GenerateScene();

        luaSensor = new LuaSensor(this.controller, this, lineOfSight);
        UIController.BroadcastMessage("SetPlayer", this.globalGameState.GetPlayer(),SendMessageOptions.DontRequireReceiver);
        controller.RunStartAI(this);
    }

    public void GenerateScene()
    {
        zoneRenderer.BroadcastMessage("initialize", globalGameState.getCurrentZone());
        globalGameState.getCurrentZone().CleanActorInTile();
        List<Actor> actors = globalGameState.getCurrentZone().GetContents().GetActors();
        //build zoneActors
        zoneActors = new List<GameObject>();
        List<ZoneActor> zoneActorScripts = new List<ZoneActor>();
        for (int i=0;i<actors.Count;i++)
        {
            Actor actor = actors[i];
            GameObject zoneActor = Instantiate(zoneActorPrefab);
            ZoneActor script = zoneActor.GetComponent<ZoneActor>();
            zoneActor.BroadcastMessage("SetActor", actors[i]);
            zoneActor.BroadcastMessage("SetController", brainBank.GetBrain(actors[i].GetAI().getScriptName(), script));
            zoneActors.Add(zoneActor);
            zoneActorScripts.Add(script);
            if (actor.GetPosition().x >= 0)
            {
                Tile t = globalGameState.getCurrentZone().GetContents().getTiles()[actor.GetPosition().x][actor.GetPosition().y];
                if (t != null)
                {
                    t.SetActor(script);
                }
            }
        }

        //build a player on the end
        GameObject playerActor = Instantiate(zoneActorPrefab);
        playerActor.BroadcastMessage("SetActor", globalGameState.GetPlayer());

        playerController = new PlayerController(globalGameState.GetPlayer(), playerActor.GetComponent<ZoneActor>(), UIController.GetComponentInChildren<CanvasCamera>());

        zoneActors.Add(playerActor);
        zoneActorScripts.Add(playerActor.GetComponent<ZoneActor>());
        controller.SetActors(zoneActorScripts);
        controller.SetPlayer(playerActor.GetComponent<ZoneActor>(), globalGameState.GetPlayer());
        globalGameState.getCurrentZone().GetContents().getTiles()[globalGameState.GetPlayer().GetPosition().x]
            [globalGameState.GetPlayer().GetPosition().y].SetActor(playerActor.GetComponent<ZoneActor>());
        ZoneActor playerActorScript = playerActor.GetComponent<ZoneActor>();
        playerController.SetZoneActor(playerActorScript);
        playerActorScript.SetController(playerController);
        playerController.SetTargetControl(controller.GetTargetControl());
        controller.GetTargetControl().SetTargeting(indicatorController.GetComponent<IndicatorController>().targetIndicator);
        controller.UpdateVision();
        controller.HandleThreats();
    } 

    public void PlayerDoAction()
    {
        controlMode = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!screenActive && screenLock && !Input.anyKey) 
        {
            screenLock = false;

        }

        if (controlMode && !screenActive && !screenLock)
        {
            if (playerController.DoControls(this))
            {
                playerController.ReadActions();
                controlMode = false;
            }
            if (!controller.PlayerCanAct())
            {
 
                controlMode = false;
            }
        }
        else
        {
            if (controller.PlayerCanAct())
            {
            
                controlMode = true;

            }
            else
            {
                if (clock <= 0 || currentScreen!=null || (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
                {

                    refreshClock++;
                    //controller.ReadActions();
                    for (int i = 0; i < skipTicks; i++)
                    {
                        if (!controller.PlayerCanAct())
                        {
                            controller.Update(this);
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (controller.PlayerCanAct())
                    {
                        //update the targeting data!
                        UIController.BroadcastMessage("Redraw");
                        if (currentScreen != null)
                        {
                            currentScreen.BroadcastMessage("UpdateScreen", SendMessageOptions.DontRequireReceiver);
                        }
                        controller.UpdateTargeting();
                    }
                    if (refreshClock == 5)
                    {
                        refreshClock = 0;
                        UIController.BroadcastMessage("Redraw");
                    }
                    clock = 0.01F;
                }
                else
                {
                    clock -= Time.deltaTime;
                }
            }
        }

        if (visionRecalc)
        {
            ProcessVision();
            controller.UpdateTargeting();
            visionRecalc = false;
        }
        uiController.SetEdgeVisible(!playerController.CanAct());
    }

    private void ProcessVision()
    {
        //also move the camera
        lineOfSight.RunVision(globalGameState.GetPlayer().GetPosition(), globalGameState.getCurrentZone().GetContents(), 
            this, globalGameState.GetPlayer());
        controller.UpdateVision();
       
        zoneRenderer.BroadcastMessage("RebuildChunks");
        MoveCamera(globalGameState.GetPlayer().GetPosition());
    }

    public void CalculateVision()
    {
        visionRecalc = true;
    }

    private void MoveCamera(Vector2Int position)
    {
        int zoneWidth = globalGameState.getCurrentZone().GetContents().GetZoneParameters().GetWidth();
        int zoneHeight = globalGameState.getCurrentZone().GetContents().GetZoneParameters().GetHeight();
        Vector2 camPos = new Vector2(position.x+0.5F, position.y+0.5F);
        if (camPos.x<width/2)
        {
            camPos.x = width / 2;
        }

        if (camPos.y < height / 2)
        {
            camPos.y = height / 2;
        }

        if (camPos.x > zoneWidth-(width / 2))
        {
            camPos.x = zoneWidth- (width / 2);
        }

        if (camPos.y > zoneHeight-(height / 2))
        {
            camPos.y = zoneHeight-(height / 2);
        }
        cameraRig.transform.position = new Vector3(camPos.x, camPos.y, -10);
    }

    public void ZoneTransition(string destination, int identifier)
    {
        this.controller.Transition(destination, identifier);
        
    }

    public void ZoneTransition(int side, string destination)
    {
        this.controller.EdgeTransition(side, destination);

    }
    

    public LuaSensor GetSensor()
    {
        return this.luaSensor;
    }

    public void DrawText(string str)
    {
        TextLog.Log(str);
    }

    public void DrawImpact(Vector2Int position, int type)
    {
        indicatorController.CreateImpact(position, type);
    }
    public void DrawImpact(Vector2 position, int type)
    {
        indicatorController.CreateImpact(position, type);
    }
    public void DrawRanged(Vector2Int origin, Vector2Int end, int type)
    {
        indicatorController.CreateRanged(origin, end, type);
    }

    public void DrawNumber(Vector2Int target, int number, int index)
    {
        indicatorController.CreateNumber(target, number, index);
    }

    public void SetScreen(ScreenID screen)
    {
        this.controller.RefreshActors();
        switch (screen)
        {
            case ScreenID.INVENTORY:
                currentScreen = (GameObject)Instantiate(Resources.Load("InventoryUI"), null);
                SetupScreen(currentScreen);
                UIController.BroadcastMessage("SetScreen", currentScreen);
                screenActive = true;
                break;
            case ScreenID.NONE:
                currentScreen = null;
                UIController.BroadcastMessage("RemoveScreen");
                screenActive = false;
                screenLock = true;
                UIController.BroadcastMessage("Redraw");
                break;
            case ScreenID.MOVESELECT:
                currentScreen = (GameObject)Instantiate(Resources.Load("MoveSelectPanel"), null);
                SetupScreen(currentScreen);
                UIController.BroadcastMessage("SetScreen", currentScreen);
                screenActive = true;

                break;
            case ScreenID.APPEARANCE:
                currentScreen = (GameObject)Instantiate(Resources.Load("AppearancePanel"), null);
                SetupScreen(currentScreen);
                UIController.BroadcastMessage("SetScreen", currentScreen);
                screenActive = true;
                break;
            case ScreenID.SPACESHIP:
                currentScreen = (GameObject)Instantiate(Resources.Load("SpaceshipPanel"), null);
                SetupScreen(currentScreen);
                UIController.BroadcastMessage("SetScreen", currentScreen);
                screenActive = true;
                break;
            case ScreenID.RESEARCH:
                currentScreen = (GameObject)Instantiate(Resources.Load("ResearchPanel"), null);
                SetupScreen(currentScreen);
                UIController.BroadcastMessage("SetScreen", currentScreen);
                screenActive = true;
                break;
            case ScreenID.CRAFTING:
                currentScreen = (GameObject)Instantiate(Resources.Load("CraftingPanel"), null);
             
                SetupScreen(currentScreen);
                UIController.BroadcastMessage("SetScreen", currentScreen);
                screenActive = true;
                break;
            case ScreenID.HELP:
                SceneManager.LoadScene("HelpScene");
                break;
        }
    }

    public void SetScreen(ScreenData screenData)
    {

       switch (screenData.GetDataType())
       {
            case ScreenDataType.CONTAINER:
                currentScreen = (GameObject)Instantiate(Resources.Load("ContainerScreen"), null);
                currentScreen.BroadcastMessage("SetContainer", (ScreenDataContainer)screenData);
                SetupScreen(currentScreen);
                screenActive = true;
                UIController.BroadcastMessage("SetScreen", currentScreen);
                break;
            case ScreenDataType.JAIL:
                currentScreen = (GameObject)Instantiate(Resources.Load("JailScreen"), null);
                currentScreen.BroadcastMessage("SetData", (ScreenDataJail)screenData);
                SetupScreen(currentScreen);
                screenActive = true;
                UIController.BroadcastMessage("SetScreen", currentScreen);
                break;
            case ScreenDataType.CONVERSATION:
                playerController.PurgeActions();
                currentScreen = (GameObject)Instantiate(Resources.Load("DialogueScreen"), null);
                currentScreen.BroadcastMessage("SetConversation", (ScreenDataConversation)screenData);
                SetupScreen(currentScreen);
                screenActive = true;
                UIController.BroadcastMessage("SetScreen", currentScreen);
                break;
            case ScreenDataType.SLOT:
                currentScreen = (GameObject)Instantiate(Resources.Load("SlotScreen"), null);
                currentScreen.BroadcastMessage("SetSlot", (ScreenDataSlot)screenData);
                SetupScreen(currentScreen);
                screenActive = true;
                UIController.BroadcastMessage("SetScreen", currentScreen);

                break;
            case ScreenDataType.SHIPSYSTEM:
                if (!((ScreenDataShipSystem)screenData).GetSystem().IsDamaged())
                {
                    currentScreen = (GameObject)Instantiate(Resources.Load("ShipSystemScreen"), null);
                }
                else
                {
                    currentScreen = (GameObject)Instantiate(Resources.Load("DamagedSystemScreen"), null);
                }
                currentScreen.BroadcastMessage("SetSystem", (ScreenDataShipSystem)screenData);
                SetupScreen(currentScreen);
                screenActive = true;
                UIController.BroadcastMessage("SetScreen", currentScreen);
                break;
            case ScreenDataType.SHOP:
                ScreenDataShop screenDataShop = (ScreenDataShop)screenData;
                switch (screenDataShop.GetStore().GetShopType())
                {
                    case SHOPTYPE.ITEM:
                        currentScreen = (GameObject)Instantiate(Resources.Load("ItemShopScreen"), null);
                        break;
                }                
                currentScreen.BroadcastMessage("SetShop", (ScreenDataShop)screenData);
                SetupScreen(currentScreen);
                screenActive = true;
                UIController.BroadcastMessage("SetScreen", currentScreen);
                break;
            case ScreenDataType.GAMEOVER:
                TextLog.Log(((ScreenDataGameover)screenData).GetGameover());
                SceneManager.LoadScene("GameOverScene");
                break;
            case ScreenDataType.QUICKSLOT:
                currentScreen = (GameObject)Instantiate(Resources.Load("QuickslotScreen"), null);
                currentScreen.BroadcastMessage("SetData", (ScreenDataQuickslot)screenData);
                SetupScreen(currentScreen);
                screenActive = true;
                UIController.BroadcastMessage("SetScreen", currentScreen);
                break;
       }
    }

    public ViewSceneController GetController()
    {
        return controller;
    }
    private void SetupScreen(GameObject screen)
    {
        screen.SendMessage("SetPlayer", controller.GetPlayerInZone());
        screen.SendMessage("SetView", this);
    }

    public void RemoveWidget(Widget widget)
    {
        Debug.Log("remove widget " + widget);
        controller.RemoveWidget(widget);
        CalculateVision();
    }

    public void ReplaceWidget(Widget widget0, Widget widget1)
    {
        controller.ReplaceWidget(widget0,widget1);
        CalculateVision();
    }

    public Vector2Int GetWidgetPosition(Widget widget)
    {
        return controller.GetWidgetPosition(widget);
    }

    public void Redraw()
    {
        UIController.BroadcastMessage("Redraw");
    }

    ZoneActor ViewInterface.GetPlayerInZone()
    {
        return this.controller.GetPlayerInZone();
    }

    public void UpdateCamera()
    {
        visionRecalc = true;
        MoveCamera(playerController.actor.GetPosition());
    }

    public void AddNPC(Actor actor)
    {
        GameObject zoneActor = Instantiate(zoneActorPrefab);
        ZoneActor script = zoneActor.GetComponent<ZoneActor>();
        zoneActor.BroadcastMessage("SetActor", actor);
        zoneActor.BroadcastMessage("SetController", brainBank.GetBrain(actor.GetAI().getScriptName(), script));
        zoneActors.Add(zoneActor);
        controller.GetZoneActors().Add(script);
        if (actor.GetPosition().x >= 0)
        {
            globalGameState.getCurrentZone().GetContents().getTiles()[actor.GetPosition().x][actor.GetPosition().y].SetActor(script);
        }
    }

    public RegenerationTicker GetRegenerationTicker()
    {
        return this.GetController().GetRegenerationTicker();
    }
}
