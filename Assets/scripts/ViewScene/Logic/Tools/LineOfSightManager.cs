using System;
using UnityEngine;
using UnityEngine.XR;
using static UnityEngine.GraphicsBuffer;
using static UnityEngine.UI.Image;

public class VisionHandler
{
    public VisitDelegate VisitDelegate;
    public BlockRay blockDelegate;
    public DebugDelegate debugDelegate;
    ZoneContents contents;
    ViewInterface view;
    Player player;

    public VisionHandler(ViewInterface view, Player player, ZoneContents zoneContents)
    {
        this.contents = zoneContents;
        this.player = player;
        this.view = view;
        blockDelegate = BlockRay;
        VisitDelegate = Visit;
        debugDelegate = debugVisit;
    }
    public bool BlockRay(Vector2Int position)
    {
        Tile t = contents.GetTile(position.x, position.y);
        if (t != null)
        {
            return t.IsVisionBlocking();
        }
        return true;
    }

    public void Visit(Vector2Int position)
    {
        Tile t = contents.GetTile(position.x, position.y);
        if (t != null)
        {
            t.SetVisible(true, view, player);
        }
    }

    public void debugVisit(Vector2 p)
    {
        view.DrawImpact(p, 3);
    }
}

public class LineOfSightManager
{


    LineOfSight2 sight = new LineOfSight2();

    internal bool CanSee(Vector2Int origin, Vector2Int target, ZoneContents zoneContents)
    {
        VisionHandler visionHandler = new VisionHandler(null, null, zoneContents);
        return sight.CanSee(origin, target, null, visionHandler.blockDelegate);
    }

    internal void RunVision(Vector2Int position, ZoneContents zoneContents, ViewScene viewScene, Player player)
    {
        sight.Clear(zoneContents.getTiles(), 
            zoneContents.GetZoneParameters().GetWidth(), 
            zoneContents.GetZoneParameters().GetHeight());

        int xMin = position.x - 10 <= 0 ? 0 : position.x - 10;
        int xMax = position.x + 10 >= zoneContents.GetZoneParameters().GetWidth() ?
            zoneContents.GetZoneParameters().GetWidth() - 1 : position.x + 10;
        int yMin = position.y - 10 <= 0 ? 0 : position.y - 10;
        int yMax = position.y + 10 >= zoneContents.GetZoneParameters().GetHeight() ?
            zoneContents.GetZoneParameters().GetHeight() - 1 : position.y + 10;

        VisionHandler visionHandler = new VisionHandler(viewScene, player, zoneContents);
        Vector2 p = new Vector2(position.x + 0.5F, position.y + 0.5F);
        for (float i = xMin; i < xMax; i+=0.25F)
        {
            sight.GenerateRay(p, new Vector2(i+0.5F, yMin+ 0.5F), visionHandler.Visit, visionHandler.blockDelegate);
            sight.GenerateRay(p, new Vector2(i+0.5F, yMax + 0.5F), visionHandler.Visit, visionHandler.blockDelegate);
        }

        for (float i = yMin; i < yMax; i+=0.25F)
        {
            sight.GenerateRay(p, new Vector2(xMin+ 0.5F, i + 0.5F), visionHandler.Visit, visionHandler.blockDelegate);
            sight.GenerateRay(p, new Vector2(xMax + 0.5F, i + 0.5F), visionHandler.Visit, visionHandler.blockDelegate);
        }

        VisionToNpcs(position, zoneContents, viewScene, player, visionHandler);
    }

    private void VisionToNpcs(Vector2Int position, ZoneContents zoneContents, ViewScene viewScene, Player player, VisionHandler visionHandler)
    {
        for (int i = 0; i < zoneContents.GetActors().Count; i++)
        {
            if (zoneContents.GetActors()[i] != player)
            {
                Vector2Int p = zoneContents.GetActors()[i].GetPosition();
                if (Math.Abs(p.x-position.x)<=10 && Math.Abs(p.y - position.y) <= 10)
                {
                    Tile t = zoneContents.GetTile(p.x, p.y);
                    if (t!=null 
                        && t.GetActorInTile()!=null
                        && t.GetActorInTile().GetAlive()
                        && t.isVisible() == false)
                    {

                        TargetedVision(position, p, zoneContents, viewScene,player, visionHandler);
                    }
                }

            }

        }
    }

    private void TargetedVision(Vector2Int origin, Vector2Int target, ZoneContents zoneContents, ViewScene viewScene, Player player, VisionHandler visionHandler)
    {
        sight.CanSee(origin, target, visionHandler.VisitDelegate, visionHandler.blockDelegate);
    }
}