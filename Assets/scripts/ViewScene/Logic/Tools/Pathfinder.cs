﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder
{
    protected List<Path> openList;
    protected List<Path> closedList;
    protected const int MAXITERATIONS = 256;
    protected bool[][] grid;
    protected int width, height;
    protected Tile[][] tiles;
    protected bool forbidDiagonals;
    public Pathfinder(int width, int height, Tile[][] tiles, bool noDiagonals=false)
    {
        this.forbidDiagonals = noDiagonals;
        this.width = width;
        this.height = height;
        grid = new bool[width][];
        for (int i = 0; i < width; i++)
        {
            grid[i] = new bool[height];
        }
        openList = new List<Path>();
        closedList = new List<Path>();
        this.tiles = tiles;
    }

    private void ClearGrid()
    {
        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid[i].Length; j++)
            {
                grid[i][j] = false;
            }
        }
    }

    public bool FindPath(Vector2Int start, Vector2Int end, int maxSteps, out Path[] path, TileMovement movement)
    {
        openList.Clear();
        closedList.Clear();
        ClearGrid();
        path = new Path[maxSteps+1];

        Path startNode= BuildPathNode(start, end, null, -1);
        Path currentNode = startNode;
        closedList.Add(currentNode);
        grid[start.x][start.y] = true;
        //return true if you reach max steps or the destination
        for (int i = 0; i < MAXITERATIONS; i++) {
            UpdateOpenList(currentNode, end, movement);

            currentNode = FindBestOpenNode();
           
            if (currentNode == null)
            {
           
                return false;
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);
            if ((Vector2Int.Distance(currentNode.position,end)<2)|| currentNode.stepCount==maxSteps)
            {
                //return this path
                while (currentNode != null && currentNode != startNode && currentNode.parent!=null)
                {
                    if (currentNode.stepCount - 1 < 0)
                    {
                        break;
                    }
                    if (currentNode.stepCount - 1 <= path.Length)
                    {
                        path[currentNode.stepCount - 1] = currentNode;
                    }
                    currentNode = currentNode.parent;
                }

                return true;
            }
        }



        return false;
    }

    internal bool PathAway(Vector2Int start, Vector2Int end, int maxSteps, out Path[] path, TileMovement movement)
    {
        openList.Clear();
        closedList.Clear();
        ClearGrid();
        path = new Path[maxSteps];

        Path startNode = BuildPathNode(start, end, null, -1);
        Path currentNode = startNode;
        closedList.Add(currentNode);
        grid[start.x][start.y] = true;
        //return true if you reach max steps or the destination
        for (int i = 0; i < MAXITERATIONS; i++)
        {
            UpdateOpenList(currentNode, end, movement);

            currentNode = FindFurthestNode();

            if (currentNode == null)
            {

                return false;
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);
            if ((Vector2Int.Distance(currentNode.position, end) < 2) || currentNode.stepCount == maxSteps)
            {
                //return this path
                while (currentNode != null && currentNode != startNode)
                {
                    path[currentNode.stepCount - 1] = currentNode;
                    currentNode = currentNode.parent;
                }

                return true;
            }
        }



        return false;
    }

    protected Path FindFurthestNode()
    {
        Path p = null;
        for (int i = 0; i < openList.Count; i++)
        {

            if (p == null)
            {
                p = openList[i];
            }
            else if (openList[i].h > p.h)
            {
                p = openList[i];
            }
        }

        return p;
    }

    protected Path FindBestOpenNode()
    {
        Path p = null;
        for (int i = 0; i < openList.Count; i++)
        {
        
            if (p == null)
            {
                p = openList[i];
            }
            else if (openList[i].value()<p.value())
            {
                p = openList[i];
            }
        }
      
        return p;
    }

    protected virtual void UpdateOpenList(Path currentNode,Vector2Int target, TileMovement tileMovement)
    {
        if (forbidDiagonals)
        {
            for (int i = 0; i < 4; i++)
            {
                Vector2Int p = GeometryTools.GetPosition(currentNode.position.x, currentNode.position.y, i*2);
                if (p.x > 0 && p.x < tiles.Length - 1 && p.y > 0 && p.y < tiles[0].Length - 1)
                {
                    if (GridCheck(p.x, p.y) && tiles[p.x][p.y]!=null && tiles[p.x][p.y].canEnter(tileMovement) &&
                                !(tiles[p.x][p.y].GetActorInTile() != null && tiles[p.x][p.y].GetActorInTile().actor.GetAlive()))
                    {
                        openList.Add(BuildPathNode(p, target, currentNode, i));
                        grid[p.x][p.y] = true;
                    }
                }

            }
        }
        else
        {
            for (int i = 0; i < 8; i++)
            {
                Vector2Int p = GeometryTools.GetPosition(currentNode.position.x, currentNode.position.y, i);
                if (p.x > 0 && p.x < tiles.Length - 1 && p.y > 0 && p.y < tiles[0].Length - 1)
                {
                    if (GridCheck(p.x, p.y) && tiles[p.x][p.y]!=null && tiles[p.x][p.y].canEnter(tileMovement) &&
                                !(tiles[p.x][p.y].GetActorInTile() != null && tiles[p.x][p.y].GetActorInTile().actor.GetAlive()))
                    {
                        openList.Add(BuildPathNode(p, target, currentNode, i));
                        grid[p.x][p.y] = true;
                    }
                }
            }

        }
   
    }

    protected bool GridCheck(int x, int y)
    {
        if (x>=0 && x<width && y>=0 && y < height)
        {
            return !grid[x][y];
        }
        return false;
    }

    protected int Manhattan(int x0, int y0, int x1, int y1)
    {
        int x = Math.Abs(x0 - x1);
        int y = Math.Abs(y0 - y1);
        return x + y;
    }

    protected Path BuildPathNode(Vector2Int p, Vector2Int target, Path parent, int direction, int cost=1)
    {
        return new Path(p, Manhattan(p.x, p.y, target.x, target.y), parent, direction, cost);
    }
}
