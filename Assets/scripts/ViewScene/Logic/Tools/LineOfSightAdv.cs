using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LineOfSightAdv
{

    private void Clear(Tile[][] tiles, int width, int height)
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (tiles[i][j] != null)
                {
                    tiles[i][j].SetVisible(false);
                }
            }
        }
    }

    public void CastRays_Edges(Vector2 start, int width, int height, Tile[][] tiles, ViewInterface view, Player player)
    {

        int innerMarginX = start.x >= 10 ? (int)start.x - 10 : 0;
        int innerMarginY = start.y >= 10 ? (int)start.y - 10 : 0;
        int outerMarginX = start.x + 10 < width ? (int)start.x + 10 : width; //(int)start.x + 10;
        int outerMarginY = start.y + 10 < height ? (int)start.y + 10 : height;

        for (float i = innerMarginX; i < outerMarginX; i += 0.5F)
        {
            double angleAdjustment = 0; // CalcAdjustment(i - innerMarginX);
            //do top and bottom edges
            RayCheckDouble(start, i, (int)(innerMarginY + angleAdjustment), tiles, true, view, player);
            RayCheckDouble(start, i, (int)(outerMarginY - angleAdjustment), tiles, true, view, player);
        }

        for (float j = innerMarginY; j < outerMarginY; j += 0.5F)
        {
            double angleAdjustment = 0;// CalcAdjustment(j - innerMarginY);
            RayCheckDouble(start, (int)(innerMarginX + angleAdjustment), j, tiles, true, view, player);
            RayCheckDouble(start, (int)(outerMarginX - angleAdjustment), j, tiles, true, view, player);
        }

    }

    private double CalcAdjustment(int v)
    {
        double f = (10 - Math.Abs(v - 10)) * 3.14D;
        double r = Math.Cos(f / 5);

        return r * 10;
    }

    public void CastRays(Vector2 start, ZoneContents zone)
    {
        for (int i = (int)start.x - 10; i < (int)start.x + 10; i++)
        {
            if (i > 0 && i < zone.GetZoneParameters().GetWidth())
            {
                for (int j = (int)start.y - 10; j < (int)start.y + 10; j++)
                {
                    if (j > 0 && j < zone.GetZoneParameters().GetHeight())
                    {
                        if (Vector2.Distance(start, new Vector2(i, j)) < 10)
                        {
                            if (zone.getTiles()[i][j] != null)
                            {
                                if (RayCheck(start, i, j, zone.getTiles()))
                                {
                                    //  MakeVisible(i, j, zone.getTiles());
                                }
                            }
                        }
                    }
                }
            }

        }
    }

    private bool RayCheck(Vector2 start, int x, int y, Tile[][] tiles)
    {
        //determine if we're only checking on horizontal or vertically aligned edges through the lousy science of whichever is bigger
        bool verticalEdgeCheck = Math.Abs(start.x - x) > Math.Abs(start.y - y);

        if (Math.Abs(start.x - x) > Math.Abs(start.y - y))
        {
            SlopeIntercept slope = GeometryTools.GetSlopeIntercept(start.x, start.y, x, y);
            return RayCheckHorizontal(start, x, y, tiles, slope);
        }
        else
        {
            SlopeIntercept slope = GeometryTools.GetSlopeIntercept(start.y, start.x, y, x);
            return RayCheckVertical(start, x, y, tiles, slope);
        }
        //return false;
    }
    private bool RayCheckDouble(Vector2 start, float x, float y, Tile[][] tiles, bool makeVisible, ViewInterface view = null, Player player = null)
    {
        //determine if we're only checking on horizontal or vertically aligned edges through the lousy science of whichever is bigger
        bool verticalEdgeCheck = Math.Abs(start.x - x) > Math.Abs(start.y - y);

        if (Math.Abs(start.x - x) > Math.Abs(start.y - y))
        {
            SlopeIntercept slope = GeometryTools.GetSlopeIntercept(start.x, start.y, x, y);
            return RayCheckHorizontalDouble(start, x, y, tiles, slope, makeVisible, view, player);
        }
        else
        {
            SlopeIntercept slope = GeometryTools.GetSlopeIntercept(start.y, start.x, y, x);
            return RayCheckVerticalDouble(start, x, y, tiles, slope, makeVisible, view, player);
        }
        //return false;
    }
    private bool RayCheckVertical(Vector2 start, int x, int y, Tile[][] tiles, SlopeIntercept slope)
    {

        int length = (int)Math.Abs(start.y - y);
        if (length > 10) { length = 10; }
        for (int i = 0; i <= length; i++)
        {
            float xIntersect = start.x + (slope.m * ((float)i));

            float yIntersect = start.y + (i * (start.y > y ? -1 : 1));
            //   Debug.Log("x" + xIntersect + "y" + yIntersect);
            Tile t = tiles[(int)xIntersect][(int)(yIntersect)];
            if (t != null)
            {
                t.SetVisible(true);
            }
            if (t == null || t.getDefinition().getVision() == TileVision.BLOCK || (t.GetWidget() != null && t.GetWidget().IsVisionBlocking()))
            {

                return false;
            }
        }
        return true;
    }

    private bool RayCheckHorizontal(Vector2 start, int x, int y, Tile[][] tiles, SlopeIntercept slope)
    {
        int length = (int)Math.Abs(start.x - x);

        for (int i = 0; i <= length; i++)
        {
            float xIntersect = start.x + (i * (start.x > x ? -1 : 1));
            float yIntersect = start.y + (slope.m * ((float)i));
            Tile t = tiles[(int)xIntersect][(int)yIntersect];
            // Debug.Log("x" + xIntersect + "y" + yIntersect);
            if (t != null)
            {
                t.SetVisible(true);
            }
            if (t == null || t.getDefinition().getVision() == TileVision.BLOCK || (t.GetWidget() != null && t.GetWidget().IsVisionBlocking()))
            {

                return false;
            }
        }
        return true;
    }

    private bool RayCheckVerticalDouble(Vector2 start, float x, float y, Tile[][] tiles, SlopeIntercept slope, bool makeVisible, ViewInterface view, Player player)
    {

        int length = (int)Math.Abs(start.y - y);
        Vector2Int[] points = new Vector2Int[length+1];
        for (int i = 0; i <= length; i++)
        {
            float xIntersect = start.x + (slope.m * ((float)i));

            float yIntersect = start.y + (i * (start.y > y ? -1 : 1));
            //   Debug.Log("x" + xIntersect + "y" + yIntersect);
            Tile t0 = GetTile(tiles, (int)xIntersect, (int)yIntersect);
            yIntersect += start.y > y ? 1 : -1;
            Tile t1 = GetTile(tiles, (int)xIntersect, (int)yIntersect);
            points[i] = new Vector2Int((int)xIntersect, (int)yIntersect);
            if (t0 != null && makeVisible)
            {
                t0.SetVisible(true, view, player);
            }
            if (t0 == null)
            {
                return false;
            }
            if (t0.getDefinition().getVision() == TileVision.BLOCK || (t0 != null && t0.GetWidget() != null && t0.GetWidget().IsVisionBlocking()))
            {
                return false;
            }
            if (i > 0 && (t1 != null && t1.getDefinition().getVision() == TileVision.BLOCK || (t1 != null && t1.GetWidget() != null && t1.GetWidget().IsVisionBlocking())))
            {
                return false;
            }
        }
        if (view != null)
        {
            for (int i = 0; i < length; i++)
            {
                if (points[i] != null)
                {
                    view.DrawImpact(points[i], 3);
                }
            }
        }

        return true;
    }

    private Tile GetTile(Tile[][] tiles, int x, int y)
    {
        if (x < 0 || x >= tiles.Length || y < 0 || y >= tiles[0].Length)
        {
            return null;
        }
        return tiles[x][y];
    }

    private bool RayCheckHorizontalDouble(Vector2 start, float x, float y, Tile[][] tiles, SlopeIntercept slope, bool makeVisible, ViewInterface view, Player player)
    {
        int length = (int)Math.Abs(start.x - x);
        Vector2Int[] points = new Vector2Int[length+1];
        for (int i = 0; i <= length; i++)
        {
            float xIntersect = start.x + (i * (start.x > x ? -1 : 1));
            float yIntersect = start.y + (slope.m * ((float)i));
            Tile t0 = GetTile(tiles, (int)xIntersect, (int)yIntersect);
            xIntersect += start.x > x ? 1 : -1;
            Tile t1 = GetTile(tiles, (int)xIntersect, (int)yIntersect);
            if (i < points.Length)
            {
                points[i] = new Vector2Int((int)xIntersect, (int)yIntersect);
            }

            // Debug.Log("x" + xIntersect + "y" + yIntersect);
            if (t0 != null && makeVisible)
            {
                t0.SetVisible(true, view, player);
            }
            if (t0 == null || t0.getDefinition().getVision() == TileVision.BLOCK || (t0 != null && t0.GetWidget() != null && t0.GetWidget().IsVisionBlocking()))
            {
                return false;
            }
            if (i > 0 && t1 != null && t1.getDefinition().getVision() == TileVision.BLOCK || (t1 != null && t1.GetWidget() != null && t1.GetWidget().IsVisionBlocking()))
            {
                return false;
            }

        }
        if (view != null)
        {
            for (int i = 0; i < length; i++)
            {
                if (points[i] != null)
                {
                    view.DrawImpact(points[i], 3);
                }
            }
            Debug.Log("target " + x + " " + y);
            Debug.Log("last point " + points.Last().x + " " + points.Last().y);
        }

        return true;
    }

    private void MakeVisible(int x, int y, Tile[][] tiles)
    {
        for (int i = x - 1; i <= x; i++)
        {
            for (int j = y - 1; j <= y; j++)
            {
                if (tiles[i][j] != null)
                {
                    tiles[i][j].SetVisible(true);
                }
            }
        }
    }

    public bool CanSee(Vector2Int origin, Vector2Int target, Tile[][] tiles, ViewInterface view=null)
    {
        //check four origin corners to four target corners
        //if any of them work we're good

        for (int i = 0; i < 5; i++)
        {
            Vector2 p0 = GeometryTools.getCornerLos(origin.x, origin.y, i);
            for (int j = 0; j < 5; j++)
            {
                Vector2 p1 = GeometryTools.getCornerLos(target.x, target.y, j);
                if (RayCheckDouble(p0, p1.x, p1.y, tiles, false, view))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void RunVision(Vector2Int position, ZoneContents zone, ViewInterface view, Player player)
    {

        Clear(zone.getTiles(),
            zone.GetZoneParameters().GetWidth(),
            zone.GetZoneParameters().GetHeight());

        for (int i = 0; i < 4; i++)
        {
            Vector2 start = GeometryTools.getCornerLos(position.x, position.y, i);
            CastRays_Edges(start, zone.GetZoneParameters().GetWidth(), zone.GetZoneParameters().GetHeight(), zone.getTiles(), view, player);
        }

        // RayCheck(position, 8, 7, zone.getTiles());

    }

    internal void RunActorDetection(Vector2Int vector2Int, List<ZoneActor> zoneActors, ZoneContents zoneContents, ViewScene viewScene, Player player)
    {
        for (int i = 0; i < zoneActors.Count; i++)
        {
            if (zoneActors[i].actor != player &&
                Math.Abs(zoneActors[i].getPosition().x - vector2Int.x) <= 10 &&
                Math.Abs(zoneActors[i].getPosition().y - vector2Int.y) <= 10)
            {
                if (this.CanSee(vector2Int, zoneActors[i].getPosition(), zoneContents.getTiles()))
                {
                    zoneActors[i].SetVisible(true);
                }
            }
        }
    }

    internal void GenerateRay(Vector2 vector2, Vector2Int position, out Vector2Int[] points)
    {
        RayCheckDoubleDev(vector2, position.x, position.y, out points);
    }

    private bool RayCheckDoubleDev(Vector2 start, float x, float y, out Vector2Int[] points)
    {
        //determine if we're only checking on horizontal or vertically aligned edges through the lousy science of whichever is bigger
        bool verticalEdgeCheck = Math.Abs(start.x - x) > Math.Abs(start.y - y);

        if (Math.Abs(start.x - x) > Math.Abs(start.y - y))
        {
            SlopeIntercept slope = GeometryTools.GetSlopeIntercept(start.x, start.y, x, y);
            return RayCheckHorizontalDoubleDev(start, x, y, slope, out points);
        }
        else
        {
            SlopeIntercept slope = GeometryTools.GetSlopeIntercept(start.y, start.x, y, x);
            return RayCheckVerticalDoubleDev(start, x, y, slope, out points);
        }
        //return false;
    }

    private bool RayCheckVerticalDoubleDev(Vector2 start, float x, float y, SlopeIntercept slope, out Vector2Int[] points)
    {
        int length = (int)Math.Abs(start.y - y);
        points = new Vector2Int[32];
        for (int i = 0; i <= length; i++)
        {
            float xIntersect = start.x + (slope.m * ((float)i));

            float yIntersect = start.y + (i * (start.y > y ? -1 : 1));
            //   Debug.Log("x" + xIntersect + "y" + yIntersect);
            yIntersect += start.y > y ? 1 : -1;
            points[i] = new Vector2Int((int)xIntersect, (int)yIntersect);

        }
        return true;
    }

    private bool RayCheckHorizontalDoubleDev(Vector2 start, float x, float y, SlopeIntercept slope, out Vector2Int[] points)
    {
        int length = (int)Math.Abs(start.x - x);
        points = new Vector2Int[32];
        for (int i = 0; i <= length; i++)
        {
            float xIntersect = start.x + (i * (start.x > x ? -1 : 1));
            float yIntersect = start.y + (slope.m * ((float)i));
            xIntersect += start.x > x ? 1 : -1;
            points[i] = new Vector2Int((int)xIntersect, (int)yIntersect);
        }
        return true;
    }
}
