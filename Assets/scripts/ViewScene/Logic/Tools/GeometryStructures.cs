﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlopeIntercept
{
    public float m, y;
    private float slope;
    private float yIntersect;

    public SlopeIntercept(float slope, float yIntersect)
    {
        m = slope;
        y = yIntersect;
    }
}
