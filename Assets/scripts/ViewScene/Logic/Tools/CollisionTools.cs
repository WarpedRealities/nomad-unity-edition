﻿using UnityEngine;
using System.Collections;
using System;

public class CollisionTools 
{

    public static ZoneActor GetActorInTile(Vector2Int p, Zone zone)
    {
        return GetActorInTile(p.x, p.y, zone);
    }

    public static ZoneActor GetActorInTile(int x, int y, Zone zone)
    {
        if (x < 0 || x >= zone.GetContents().GetZoneParameters().GetWidth())
        {
            return null;
        }
        if (y < 0 || y >= zone.GetContents().GetZoneParameters().GetHeight())
        {
            return null;
        }
        Tile t = zone.GetContents().getTiles()[x][y];
        if (t != null)
        {
            return t.GetActorInTile();
        }
        return null;
    }

    public static bool CanEnter(Vector2Int p, Zone zone, TileMovement movement = TileMovement.WALK)
    {
        return CanEnter(p.x, p.y, zone, movement);
    }
    public static bool CanEnter(int x, int y, Zone zone, TileMovement movement= TileMovement.WALK)
    {
        if (x<0 || x >= zone.GetContents().GetZoneParameters().GetWidth())
        {
            return false;
        }
        if (y<0 || y>= zone.GetContents().GetZoneParameters().GetHeight())
        {
            return false;
        }
        Tile t = zone.GetContents().getTiles()[x][y];
        if (t == null)
        {
            return false;
        }

        if (t.getDefinition().getMovement() > movement)
        {
            return false;
        }

        if (t.GetActorInTile()!=null || (t.GetWidget()!=null && !t.GetWidget().IsWalkable())) {
            return false;
        }

        

        return true;
    }

    internal static void RemoveActor(ZoneActor zoneActor)
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        for (int i = 0; i < zone.GetContents().GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zone.GetContents().GetZoneParameters().GetHeight(); j++)
            {
                if (zone.GetContents().GetTile(i,j)!=null && zoneActor.Equals(zone.GetContents().GetTile(i, j).GetActorInTile()))
                {
                    zone.GetContents().GetTile(i, j).SetActor(null);
                    return;
                }
            }
        }
    }

    internal static Vector2Int MovePath(Vector2Int vector2Int, Vector2 offset, float distance, Zone zone)
    {

        Vector2 p = new Vector2(vector2Int.x, vector2Int.y);
        Vector2Int nuPosition = vector2Int;
        for (int i = 0; i < distance; i++)
        {
            p += offset;

            Tile t = zone.GetContents().GetTile((int)p.x, (int)p.y);
    
            if (t==null || t.IsVisionBlocking())
            {

                return nuPosition;
            }
            if (t!=null && t.canWalk() && t.GetActorInTile()==null)
            {

                nuPosition = new Vector2Int((int)p.x, (int)p.y);
            }
        }

        return nuPosition;

    }

    public static Tile GetTile(Vector2Int p)
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        int x = p.x;
        int y = p.y;
        if (x < 0 || x >= zone.GetContents().GetZoneParameters().GetWidth())
        {
            return null;
        }
        if (y < 0 || y >= zone.GetContents().GetZoneParameters().GetHeight())
        {
            return null;
        }
        return zone.GetContents().getTiles()[x][y];
    }

    public static void setActorInTile(Vector2Int p, ZoneActor actor)
    {
        Tile t = GetTile(p);
        if (t != null)
        {
            t.SetActor(actor);
        }
    }

    public static void SetThreat(Vector2Int pos, ZoneActor actor)
    {
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(pos.x, pos.y, i);
            Tile t = GetTile(p);
            if (t != null)
            {
                t.SetThreateningActor(actor);
            }
        } 
    }
    public static void RemoveThreat(Vector2Int pos, ZoneActor actor)
    {
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(pos.x, pos.y, i);
            Tile t = GetTile(p);
            if (t != null && actor.Equals(t.GetThreateningActor()))
            {
                t.SetThreateningActor(null);
            }
        }
    }

    public static void Step(Vector2Int position, ViewInterface view)
    {

        Tile t = GetTile(position);
        if (t!=null && t.GetWidget() != null)
        {
            t.GetWidget().Step(view);
            return;
        }

        ZoneContents zone = GlobalGameState.GetInstance().getCurrentZone().GetContents();
        if (zone.getTransitions() == null)
        {
            return;
        }
        int width = zone.GetZoneParameters().GetWidth();
        int height = zone.GetZoneParameters().GetHeight();
        if (position.x == 0)
        {
            if (zone.getTransitions()[3] != null)
            {
                view.ZoneTransition(3, zone.getTransitions()[3]);
            }
        }
        if (position.x == width - 1)
        {
            if (zone.getTransitions()[1] != null)
            {
                view.ZoneTransition(1, zone.getTransitions()[1]);
            }
        }
        if (position.y == 0)
        {
            if (zone.getTransitions()[2] != null)
            {
                view.ZoneTransition(2, zone.getTransitions()[2]);
            }
        }
        if (position.y == height - 1)
        {
            if (zone.getTransitions()[0] != null)
            {
                view.ZoneTransition(0, zone.getTransitions()[0]);
            }
        }
    }

    internal static bool SafetyCheck(Vector2Int vector2Int)
    {
        Vector2Int PlayerPos = GlobalGameState.GetInstance().GetPlayer().GetPosition();
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        for (int i = 0; i < zone.GetContents().GetActors().Count; i++)
        {
            Vector2Int p = zone.GetContents().GetActors()[i].GetPosition();
            if (zone.GetContents().GetActors()[i].isHostile("player") &&
                zone.GetContents().GetActors()[i].GetAlive() && 
                p.x >= 0 && p.y>=0 &&
                zone.GetContents().GetTile(p.x,p.y)!=null &&
                zone.GetContents().GetTile(p.x,p.y).isVisible() &&
                Vector2Int.Distance(PlayerPos,p)<10)
            {
                return false;
            }
        }
        return true;
    }

    internal static Vector2Int GetFreeTile(Vector2Int p)
    {
        Zone z = GlobalGameState.GetInstance().getCurrentZone();
        for (int i = 0; i < 8; i++)
        {
            Vector2Int g = GeometryTools.GetPosition(p.x, p.y, i);
            Tile t = z.GetContents().GetTile(g.x, g.y);
            if (t != null)
            {
                if (t.canWalk() && 
                    t.GetActorInTile() == null &&
                    t.GetWidget() == null)
                {
                    return g;
                }
            }
        }
        return new Vector2Int(p.x,p.y);
    }
}
