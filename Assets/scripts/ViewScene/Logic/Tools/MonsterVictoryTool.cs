﻿using UnityEngine;
using System.Collections;
using System;
using System.Numerics;

public class MonsterVictoryTool 
{

  static public void RunVictory(ZoneActor npc, ZoneActor player, ViewInterface view)
    {

        if (Vector2Int.Distance(npc.getPosition(), player.getPosition()) > 2)
        {

            //move npc to player
            if (npc.actor.IsImmobile())
            {
                MoveToPlayer(player, npc, view);
            }
            else
            {
                MoveToPlayer(npc, player, view);
            }
            
        }
        NPC_RPG nPC_RPG = (NPC_RPG)npc.actor.GetRPG();
        string conversation = nPC_RPG.GetStatBlock().GetConversationFiles()[(int)conversationType.VICTORY];
        if (conversation != null)
        {
            view.SetScreen(new ScreenDataConversation(conversation, npc.actor.GetFlagField(), npc, true));
        }
        else
        {
            view.SetScreen(new ScreenDataGameover("You have perished"));
        }
    }

    private static void MoveToPlayer(ZoneActor npc, ZoneActor player, ViewInterface view)
    {
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(player.getPosition().x, player.getPosition().y, i);
            if (CollisionTools.CanEnter(p, GlobalGameState.GetInstance().getCurrentZone()))
            {
                CollisionTools.setActorInTile(npc.getPosition(), null);
                CollisionTools.setActorInTile(p, npc);
                npc.Reposition(p);
                npc.transform.position= new UnityEngine.Vector3(p.x, p.y, 0.5F);
                return;
            }
        }
    }

    static public void RunDominate(ZoneActor npc, ZoneActor player, ViewInterface view)
    {
        if (Vector2Int.Distance(npc.getPosition(), player.getPosition()) > 2)
        {
            //move npc to player
            //move npc to player
            if (npc.actor.IsImmobile())
            {
                MoveToPlayer(player, npc, view);
            }
            else
            {
                MoveToPlayer(npc, player, view);
            }
        }
        NPC_RPG nPC_RPG = (NPC_RPG)npc.actor.GetRPG();
        string conversation = nPC_RPG.GetStatBlock().GetConversationFiles()[(int)conversationType.DOMINANT];
        if (conversation != null)
        {
            view.SetScreen(new ScreenDataConversation(conversation, npc.actor.GetFlagField(), npc, true));
        }
        else
        {
            view.SetScreen(new ScreenDataGameover("You have perished"));
        }
    }
}
