using System;
using UnityEngine;
using UnityEngine.UIElements;

public delegate void VisitDelegate(Vector2Int position);

public delegate bool BlockRay(Vector2Int position);

public delegate void DebugDelegate(Vector2 position);
class LineOfSight2 
{



    internal bool GenerateRay(Vector2 origin, Vector2 position, 
        VisitDelegate visit, BlockRay blockRay, DebugDelegate debugDelegate=null)
    {

        double length = (position - origin).magnitude;
        length = length > 10 ? 10 : length;
        Vector2 delta = (position - origin).normalized;

        int escape = 0;
        double distance = 0;
        int index = 0;
        
        Vector2Int c = new Vector2Int((int)origin.x, (int)origin.y);
        Vector2 p = new Vector2(origin.x, origin.y);
        if (visit != null)
        {
            visit(c);
        }
        if (blockRay != null && blockRay(c))
        {
            return false;
        }
        while (escape<128 && Vector2.Distance(p,origin) < length)
        {
            double w = GetW(delta.x, p.x, c.x);
            double h = GetH(delta.y, p.y, c.y);

            double dt = w < h ? w : h;
            bool v = h > w;
            p.x += delta.x * (float)dt;
            p.y += delta.y * (float)dt;
            if (Vector2.Distance(p, origin) >= length)
            {
                return true;
            }
            c = GetC(c, w, h, delta.x, delta.y);
            //c.x = (int)p.x;
            //c.y = (int)p.y;
            if (visit != null)
            {
                visit(c);
            }
            if (debugDelegate != null)
            {
                debugDelegate(p);
            }
            if (blockRay!=null && blockRay(c))
            {
                if (visit != null)
                {
                    Vector2Int c0= new Vector2Int(c.x, c.y + 1);
                    if (blockRay(c0))
                    {
                        visit(c0);
                    }
                }
                return false;
            }

            index++;
            distance += 1 * dt;
            escape++;
        }
        return true;

    }

    private Vector2Int GetC(Vector2Int c, double w, double h, float x, float y)
    {
        if (w <= h && x!=0)
        {
            //horizontal
            c.x += x > 0 ? 1 : -1;
        }
        if (h <= w & y!=0)
        {
            //vertical
            c.y += y > 0 ? 1 : -1;
        }
        return c;
    }

    private double GetW(double deltaX, double posX, int ctX)
    {
        if (deltaX == 0)
        {
            return 99;
        }
        double localX = posX - ctX;
        double edgeX = deltaX < 0 ? 0 : 1;
        //edgeX += (localX == 1 && deltaX>0) ? 1 : 0;
        //edgeX -= (localX == 0 && deltaX<0) ? 1 : 0;
        double span = Math.Abs(localX - edgeX);
        return span / Math.Abs(deltaX);
    }
    private double GetH(double deltaY, double posY, int ctY)
    {
        if (deltaY == 0)
        {
            return 99;
        }
        double localY = posY - ctY;
        double edgeY = deltaY < 0 ? 0 : 1;
        //edgeY += (localY == 1 && deltaY > 0) ? 1 : 0;
        //edgeY -= (localY == 0 && deltaY < 0) ? 1 : 0;
        double span = Math.Abs(localY - edgeY);
        return span / Math.Abs(deltaY);

    }


    public void Clear(Tile[][] tiles, int width, int height)
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (tiles[i][j] != null)
                {
                    tiles[i][j].SetVisible(false);
                }
            }
        }
    }

    internal bool CanSee(Vector2Int origin, Vector2Int p, VisitDelegate visit, BlockRay blockRay, DebugDelegate debug=null)
    {
            for (int j= 0; j < 5; j++)
            {
                Vector2 o = GeometryTools.getCornerLos(origin.x, origin.y, j);
                for (int i = 0; i < 5; i++)
                {
                    if (this.GenerateRay(o, GeometryTools.getCornerLos(p.x, p.y, i), visit, blockRay, debug))
                    {
                        return true;
                    }
                }

            }
        return false;
    }
}