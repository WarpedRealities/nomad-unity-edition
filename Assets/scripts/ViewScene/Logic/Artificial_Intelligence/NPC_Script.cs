﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;
using System;

public class NPC_Script
{
    private string filename;
    private Script script;
    private DynValue mainFunction, tickFunction, startFunction;
    private object[] arguments;
    public NPC_Script(string filename)
    {
        this.filename = filename;
        this.script = new Script();
        try
        {
            String fileContents = FileTools.GetFileRaw("AI/" + filename + ".lua");

            this.script.DoString(fileContents);
        }
        catch (Exception e)
        {
            throw new Exception("failed to load lua file " + filename, e);
        }
        mainFunction = this.script.Globals.Get("main");
        tickFunction = this.script.Globals.Get("tick");
        startFunction = this.script.Globals.Get("start");
        arguments = new object[2];
    }

    public void RunAI(LuaController controller, LuaSensor sensor)
    {
        arguments[0] = controller;
        arguments[1] = sensor;
        DynValue rValue = this.script.Call(mainFunction, arguments);
    }

    public void RunTick(LuaSensor sensor)
    {
        if (tickFunction!=null && tickFunction.IsNotNil())
        {
            arguments[0] = sensor;
            DynValue rValue = this.script.Call(tickFunction, arguments);
        }
    }

    internal void RunStart(LuaController controller, LuaSensor sensor)
    {
        if (startFunction != null && startFunction.IsNotNil())
        {
            arguments[0] = controller;
            arguments[1] = sensor;
            DynValue rValue = this.script.Call(startFunction, arguments);
        }
    }
}
