﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathHandler 
{
    Path[] paths;
    int index;
    bool active = false;

    public void SetPath(Path [] paths)
    {

        
        if (paths!=null && paths[0] != null)
        {
            for (int i = 0; i < paths.Length; i++)
            {
                if (paths[i] != null)
                {
             //       Debug.Log("record path" + i + " : " + paths[i].ToString());
                }
             
            }
            active = true;
        }    
        this.paths = paths;
        index = 0;
    }

    public bool isActive()
    {
        return active;
    }

    public int GetDirection()
    {
        if (index>=paths.Length || paths[index] == null)
        {
            active = false;
            return -1;
        }
        int d = paths[index].direction;
        index++;
        return d;
    }

    internal void Clear()
    {
        active = false;

    }
}
