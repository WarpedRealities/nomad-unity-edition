﻿using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;
using System;

[MoonSharpUserData]
public class LuaController 
{
    private NPC_AI _AI;
    private static bool isRegistered = false;
    private ZoneActor actor;
    private LuaActor luaActor;
    private ActionQueue actionQueue;
    private PathHandler pathing;
    private PatternEligiblity patternEligiblity;
    public LuaController(ZoneActor actor, ActionQueue actionQueue, NPC_AI _AI)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<int>();
            UserData.RegisterType<LuaController>();
            isRegistered = true;
        }
        this.patternEligiblity = new PatternEligiblity(actor);
        this.actionQueue = actionQueue;
        this.actor = actor;
        this.luaActor = new LuaActor(actor);
        this.pathing = new PathHandler();
        this._AI = _AI;
    }

    public bool Move(int direction)
    {
        if (actor.actor.GetRPG().GetStatusHandler().GetStatusBind() != null)
        {
            actionQueue.AddAction(new ActionStruggle(10));
            return true;
        }
        Vector2Int p = GeometryTools.GetPosition(actor.getPosition().x, actor.getPosition().y, direction);
        if (CollisionTools.CanEnter(p,
            GlobalGameState.GetInstance().getCurrentZone(),actor.actor.GetMovementType()))
        {
            ActionMove actionMove = new ActionMove(direction, (1.0F / actor.actor.GetRPG().getSpeed()));
            actionQueue.AddAction(actionMove);
            return true;
        }
        else
        {
            //check if there's a downed npc in our way, if so, shove them aside
            ZoneActor z = CollisionTools.GetActorInTile(p, GlobalGameState.GetInstance().getCurrentZone());
            if (z!=null && !z.GetAlive())
            {
                actionQueue.AddAction(new ActionShove(5,p));

                actionQueue.AddAction(new ActionMove(direction, (1.0F / actor.actor.GetRPG().getSpeed())));
                return true;
            }
            else
            {
                RemovePath();
            }

        }
        return false;
    }

    public int GetRank()
    {
        return ((NPC_RPG)actor.actor.GetRPG()).GetRank();
    }
    public bool IsCompanion()
    {
        return actor.actor.IsCompanion();
    }

    public bool HasObserverVore()
    {
        return ((NPC)actor.actor).GetObserverVoreHandler()!=null;
    }

    public void StartObserverVore(string filename, LuaActor target)
    {
        ObserverVoreHandler handler = ObserverVoreHandlerBuilder.BuildHandler(filename);
        handler.SetTarget(target.GetZoneActor());
        //build an observer vore handler
        ((NPC)actor.actor).SetObserverVoreHandler(handler);
    }

    public void DoMove(LuaActor actor, int index)
    {
        CombatMove combatMove = this.actor.actor.GetRPG().GetCombatMove(index);
        if (PatternCheck(combatMove, actor))
        {
            actionQueue.AddAction(new ActionDoMove(combatMove, actor.GetZoneActor(), 1, null));

            if (combatMove.GetDelay() > 0)
            {
                actionQueue.AddAction(new ActionDelay(combatMove.GetDelay()));
            }
        }   
    }

    private bool PatternCheck(CombatMove combatMove, LuaActor actor)
    {
        return patternEligiblity.TargetCheck(combatMove, actor.GetPosition());
    }

    public void DoSelfMove(int index)
    {
        DoMove(this.luaActor, index);
    }

    public bool IsVisible()
    {
        return actor.IsVisible();
    }

    public void Wait(int time)
    {
        actionQueue.AddAction(new ActionDelay(time));
    }


    public LuaActor GetActor()
    {
        return luaActor;
    }

    public ZoneActor GetZoneActor()
    {
        return this.actor;
    }

    public long GetUID()
    {
        return actor.actor.GetUID();
    }

    public bool GetPeace()
    {
        return ((NPC)actor.actor).IsPeaceful();
    }

    public void RemovePath()
    {
        pathing.Clear();
    }

    public void FollowPath()
    {

        int d = pathing.GetDirection();
        if (d != -1)
        {

            Move(d);
        }
    }

    public void FastMove()
    {
        actor.GetController().ReduceQueue(10);
    }

    public void Delay(int duration)
    {
        actionQueue.AddAction(new ActionDelay(duration));

    }

    public bool IsHostileToActor(Faction faction)
    {
        return actor.actor.GetFaction().getRelationship(faction) <= 50;
    }

    public bool IsWitness()
    {
        return actor.actor.GetCrimeFlags().IsWitness();
    }

    public void ClearWitness()
    {
        actor.actor.GetCrimeFlags().SetWitness(false);
    }

    public bool ReportCrime()
    {
        return actor.actor.GetFaction().GetFactionRuleset().WitnessReport(actor.actor);
    }

    public void SetPath(Path[] path)
    {

        pathing.SetPath(path);
    }

    public bool HasPath()
    {
        return pathing.isActive();
    }

    public Vector2Int GetPosition()
    {
        return actor.getPosition();
    }

    public bool IsPeaceful()
    {
        return ((NPC)actor.actor).IsPeaceful();
    }

    public int GetValue(int index)
    {
       
        return _AI.GetValue(index);
    }

    public int GetFlag(string flag)
    {
       return ((NPC)actor.actor).GetFlagField().ReadFlag(flag);
    }

    public int GetGlobalFlag(string flag)
    {
        return GlobalGameState.GetInstance().GetPlayer().GetFlagField().ReadFlag(flag);
    }
    public void SetValue(int index, int value)
    {
        _AI.SetValue(index, value);
    }

    public int GetStat(int index)
    {
        return actor.actor.GetRPG().GetStat((ST)index);
    }

    public int GetStatMax(int index)
    {
        return actor.actor.GetRPG().GetStatMax((ST)index);
    }

    public void StartConversation()
    {
        string conversationFile = ((NPC_RPG)actor.actor.GetRPG()).GetConversationFile((int)conversationType.TALK);
       ScreenDataConversation screenDataConversation = new ScreenDataConversation(conversationFile,actor.actor.GetFlagField(), actor, false);
        GameObject.Find("ViewSceneController").GetComponent<ViewScene>().SetScreen(screenDataConversation);
    }
}
