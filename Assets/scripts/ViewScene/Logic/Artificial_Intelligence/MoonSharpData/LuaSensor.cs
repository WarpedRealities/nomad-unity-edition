﻿using MoonSharp.Interpreter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[MoonSharpUserData]
public class LuaSensor
{
    private static bool isRegistered=false;
    private ViewSceneController view;
    private ViewInterface viewInterface;
    private LineOfSightManager lineOfSight;
    private Pathfinder pathfinder;
    private int[] memoryValues = new int[32];

    public LuaSensor(ViewSceneController view, ViewInterface viewInterface, LineOfSightManager lineOfSight)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<LuaSensor>();
            UserData.RegisterType<Vector2Int>();
            UserData.RegisterType<bool>();
            isRegistered = true;
        }

        pathfinder = new Pathfinder(view.GetZoneContents().GetZoneParameters().GetWidth(), 
            view.GetZoneContents().GetZoneParameters().GetHeight(),
            view.GetZoneContents().getTiles());
        this.lineOfSight = lineOfSight;
        this.view = view;
        this.viewInterface = viewInterface;
    }

    public bool PathToPosition(LuaController controller, int x, int y)
    {
        Vector2Int p = new Vector2Int(x, y);
        float distance = Vector2Int.Distance(controller.GetPosition(), p) / 2;
        
        int steps = (int)distance;
        Path[] path;
        bool success = pathfinder.FindPath(controller.GetPosition(), p, steps, out path, controller.GetActor().GetZoneActor().actor.GetMovementType());
        //put the path inside the controller so it can follow the path
        controller.SetPath(path);

        return success;

    }

    public bool PathToActor(LuaController controller, LuaActor target)
    {
        float distance = Vector2Int.Distance(controller.GetPosition(), target.GetPosition())/2;

        float movingTarget = target.GetMovingTarget();
        Vector2Int estimatedPosition = target.GetEstimatedPosition();
        int steps = (distance < 4 ? 4 : (int)distance)-(int)movingTarget;
        Path[] path;
        bool success = pathfinder.FindPath(controller.GetPosition(), estimatedPosition, steps, out path, controller.GetActor().GetZoneActor().actor.GetMovementType());
        //put the path inside the controller so it can follow the path
        controller.SetPath(path);

        return success;

    }

    public bool PathAwayFromActor(LuaController controller, LuaActor target, int steps)
    {
        float distance = Vector2Int.Distance(controller.GetPosition(), target.GetPosition()) / 2;

        float movingTarget = target.GetMovingTarget();
        Vector2Int estimatedPosition = target.GetEstimatedPosition();

        Path[] path;
        bool success = pathfinder.PathAway(controller.GetPosition(), estimatedPosition, steps, out path, controller.GetActor().GetZoneActor().actor.GetMovementType());
        //put the path inside the controller so it can follow the path
        controller.SetPath(path);

        return success;
    }

    public bool PathTo(LuaController controller, int x, int y, int maxSteps)
    {

        Path[] path;
        bool success = pathfinder.FindPath(controller.GetPosition(), new Vector2Int(x, y), maxSteps, out path, controller.GetActor().GetZoneActor().actor.GetMovementType());
        //put the path inside the controller so it can follow the path
        controller.SetPath(path);

        return success;
    }

    public void DrawText(string text)
    {

        viewInterface.DrawText(text);
    }

    public void Log(string text)
    {
        Debug.Log(text);
    }

    public LuaActor GetPlayer(LuaController actor, int distance, bool ignoreWalls)
    {
        ZoneActor player = view.GetPlayerInZone();
        float dCheck = Vector2Int.Distance(player.getPosition(), actor.GetPosition());
        if (dCheck < distance)
        {
            if (ignoreWalls || lineOfSight.CanSee(actor.GetPosition(), player.getPosition(), view.GetZoneContents()))
            {
                return player.GetLuaActor();
            }
        }
        return null;
    }
    public Vector2Int GetWidget(LuaController actor, string name, int distance, bool ignoreWalls)
    {
        int x = actor.GetPosition().x;
        int y = actor.GetPosition().y;
        Vector2Int p = new Vector2Int(-1,-1);
        float comp = 999;
        for (int i = 0; i <(distance * 2); i++)
        {
            for (int j = 0; j<(distance*2); j++)
            {
               
                Tile t = view.GetZoneContents().GetTile(x - distance + i, y - distance + j);
                if (t != null)
                {
                    if (t.GetWidget()!=null && name.Equals(t.GetWidget().GetName()))
                    {
                        Vector2Int p0 = new Vector2Int(x - distance + i, y + distance + j);
                        float d = Vector2Int.Distance(p0, actor.GetPosition());
                        if (d<comp && (ignoreWalls || lineOfSight.CanSee(actor.GetPosition(), p0, view.GetZoneContents())))
                        {
                            comp = d;
                            p = p0;
                        }
                    }
                }
            }
        }
        return p;
    }
    public Vector2Int GetTile(LuaController actor, string description, int distance, bool ignoreWalls)
    {
        int x = actor.GetPosition().x;
        int y = actor.GetPosition().y;
        Vector2Int p = new Vector2Int(-1, -1);
        float comp = 999;
        for (int i = 0; i < (distance * 2); i++)
        {
            for (int j = 0; j < (distance * 2); j++)
            {

                Tile t = view.GetZoneContents().GetTile(x - distance + i, y - distance + j);
                if (t != null)
                {
                    if (description.Equals(t.getDefinition().getDescription()))
                    {
                        Vector2Int p0 = new Vector2Int(x - distance + i, y + distance + j);
                        float d = Vector2Int.Distance(p0, actor.GetPosition());
                        if (d < comp && (ignoreWalls || 
                            lineOfSight.CanSee(actor.GetPosition(), p0, view.GetZoneContents())))
                        {
                            comp = d;
                            p = p0;
                        }
                    }
                }
            }
        }
        return p;
    }

    public bool IsTileVisible(int x, int y)
    {
        Tile t = view.GetZoneContents().GetTile(x, y);
        if (t != null)
        {
            return t.isVisible();
        }
        return false;
    }
    public LuaActor GetNpc(LuaController actor, string name, int distance, bool ignoreWalls)
    {
        List<ZoneActor> actors = view.GetZoneActors();
        float dComp = distance;
        ZoneActor target = null;
        for (int i = 0; i < actors.Count; i++)
        {

            if (!actor.GetActor().Equals(actors[i]) && actors[i].GetAlive() && name.Equals(actors[i].actor.GetName()))
            {
                float dCheck = Vector2Int.Distance(actors[i].getPosition(), actor.GetPosition());

                if (dCheck <= dComp &&
                    (ignoreWalls || lineOfSight.CanSee(actor.GetPosition(), actors[i].getPosition(), view.GetZoneContents())))
                {
                    target = actors[i];
                    dComp = dCheck;
                }
            }
        }
        if (target != null)
        {

            return target.GetLuaActor();
        }
        return null;
    }

    public LuaActor GetActor(LuaController actor, string name, int distance, bool ignoreWalls, bool alive)
    {
        List<ZoneActor> actors = view.GetZoneActors();
        float dComp = distance;
        ZoneActor target = null;
        for (int i = 0; i < actors.Count; i++)
        {

            if (!actor.GetActor().Equals(actors[i]) && 
                actors[i].actor.GetName().Equals(name) &&
                !actors[i].actor.IsInvolvedInObserverVore() &&
                actors[i].GetAlive() == alive)
            {
                float dCheck = Vector2Int.Distance(actors[i].getPosition(), actor.GetPosition());

                if (dCheck <= dComp &&
                    (ignoreWalls || lineOfSight.CanSee(actor.GetPosition(), actors[i].getPosition(), view.GetZoneContents())))
                {
                    target = actors[i];
                    dComp = dCheck;
                }
            }
        }
        if (target != null)
        {
            return target.GetLuaActor();
        }
        return null;
    }

    public LuaActor GetHostile(LuaController actor, int distance, bool ignoreWalls)
    {
        List<ZoneActor> actors = view.GetZoneActors();

        float dComp = distance;
        ZoneActor target = null;
        for (int i = 0; i < actors.Count; i++)
        {
            
            if (!actor.GetActor().Equals(actors[i]) && 
                actors[i].GetAlive() && 
                actor.IsHostileToActor(actors[i].actor.GetFaction()) &&
                !actor.IsPeaceful() &&
                actors[i].actor.GetRPG().GetStatusHandler().GetStatusStealth()==null)
            {
                float dCheck = Vector2Int.Distance(actors[i].getPosition(), actor.GetPosition());

                if (dCheck <= dComp && 
                    (ignoreWalls || 
                    lineOfSight.CanSee(actor.GetPosition(),actors[i].getPosition(),view.GetZoneContents())))
                {
                    target = actors[i];
                    dComp = dCheck;
                }
            }
        }
        if (target != null)
        {
            return target.GetLuaActor();
        }
        return null;
    }

    public bool IsCrimeReported(LuaController actor)
    {
        if (actor.GetZoneActor().actor.GetFaction().GetFactionRuleset().GetReportedViolation() != null)
        {
            return true;
        }

        return false;
    }

    public Vector2Int GetCrimePosition(LuaController actor)
    {
        Vector2S v=actor.GetZoneActor().actor.GetFaction().GetFactionRuleset().GetReportedViolation().violationPosition;
        return new Vector2Int(v.x, v.y);
    }
    public bool CanWalk(int x, int y)
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        Tile t = zone.GetContents().GetTile(x, y);
        if (t!=null && t.canWalk())
        {
            return true;
        }
        return false;
    }
    public int GetKarma()
    {
        return ((PlayerRPG)GlobalGameState.GetInstance().GetPlayer().GetRPG()).GetKarma();
    }

    public void SetGlobal(int index, int value)
    {
        memoryValues[index] = value;
    }

    public int ReadGlobal(int index)
    {
        return memoryValues[index];
    }

    public int ReadGlobalFlag(string name)
    {
        return GlobalGameState.GetInstance().GetPlayer().GetFlagField().ReadFlag(name);
    }
}
