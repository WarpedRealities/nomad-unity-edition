﻿using MoonSharp.Interpreter;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[MoonSharpUserData]
public class LuaActor 
{
    private static bool isRegistered = false;
    private ZoneActor actor;

    public LuaActor(ZoneActor actor)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<LuaActor>();
            isRegistered = true;
        }
        this.actor = actor;
    }

    public float GetHealth()
    {
        float current = actor.actor.GetRPG().GetStat(ST.HEALTH);
        float max = actor.actor.GetRPG().GetStatMax(ST.HEALTH);
        return current / max;
    }

    public float GetDistance(Vector2Int position)
    {
        return Vector2Int.Distance(actor.getPosition(), position);
    }

    public Vector2Int GetPosition()
    {
        return actor.getPosition();
    }

    public ZoneActor GetZoneActor()
    {
        return this.actor;
    }

    public float GetMovingTarget()
    {
        return this.actor.GetController().GetMovingTargetEstimate();
    }

    public Vector2Int GetEstimatedPosition()
    {
        return this.actor.GetController().GetEstimatedPosition();
    }
    public bool HasStatus(int uid)
    {

        return this.actor.actor.GetRPG().GetStatusHandler().HasStatusEffect(uid);
    }

    public void RemoveStatus(int uid)
    {

        this.actor.actor.GetRPG().GetStatusHandler().RemoveStatusEffect(uid,null,this.actor.actor);
    }

    public long GetBindOrigin()
    {
        StatusBind bind= actor.actor.GetRPG().GetStatusHandler().GetStatusBind();
        if (bind != null && bind.IsDependent())
        {
            return bind.GetOrigin().GetUID();
        }
        return 0;
    }
}
