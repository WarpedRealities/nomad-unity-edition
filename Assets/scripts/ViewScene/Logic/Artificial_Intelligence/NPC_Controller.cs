﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_Controller : ActorController
{
    private NPC_Script script;
    private NPC_AI _AI;
    private LuaController luaController;
    private bool threateningNPC;
    private int threatMove=-1;
    public NPC_Controller(ZoneActor actor, NPC_Script script) : base(actor.actor, actor)
    {
        this._AI = actor.actor.GetAI();
        this.script = script;
        this.luaController = new LuaController(actor, queue, this._AI);
        threateningNPC = isThreatening(actor);
    }

    private bool isThreatening(ZoneActor actor)
    {
        if (actor.actor.GetFaction().getRelationship(GlobalGameState.GetInstance().GetPlayer().GetFaction()) > 50)
        {
            return false;
        }
        //do we have a threatening move
      
        for (int i = 0; i < this.actor.GetRPG().CombatMoveCount(); i++)
        {
            if (this.actor.GetRPG().GetCombatMove(i).IsThreatening())
            {
                threatMove = i;
                return true;
            }
        }

        return false;
    }
    public override bool UpdateLogic(ViewInterface view)
    {
        //observer vore overrides things if you're in stage 1, in stage 2-3 it doesn't


        if (!queue.IsEmpty())
        {

            //check if the queue has an action ready to complete
            if (queue.ProcessQueue())
            {
              
                //handle the action
                Action action = queue.GetAction();
                queue.CompleteAction();
                return ProcessAction(action, view);
            }
        }
        else
        {

            //run the AI
            script.RunAI(luaController, view.GetSensor());
        }
        return false;
    }

    public override bool IsThreatening()
    {
        if (actor.GetRPG().GetStatusHandler().GetStatusStealth() != null)
        {
            return false;
        }
        return threateningNPC;
    }

    public override void AttackOfOpportunity(ZoneActor player)
    {
        Action action = queue.GetAction();
        if (IsThreatening() && !((NPC)this.actor).IsPeaceful() && !(action!=null && action.GetActionType() == ActionType.NONE))
        {
            //firstly, queue check
            if (queue.GetAction()!=null)
            {
                if (queue.GetAction().GetActionType() == ActionType.ATTACK)
                {
                    ActionDoMove move = (ActionDoMove)queue.GetAction();
                    if (move.GetName().Equals(actor.GetRPG().GetCombatMove(threatMove).GetName()))
                    {
                        return;
                    }
                }
                if (queue.GetAction().GetActionType() == ActionType.NONE)
                {
                    ActionDelay actionDelay = (ActionDelay)queue.GetAction();
                    if (actionDelay.PreventAction())
                    {
                        return;
                    }
                }
            }
            //clear the queue
            queue.Clear();
            //now add attack action on the player!
            CombatMove combatMove = this.actor.GetRPG().GetCombatMove(threatMove);

            queue.AddAction(new ActionDoMove(combatMove, player,1,null));

            if (combatMove.GetDelay() > 0)
            {
                queue.AddAction(new ActionDelay(combatMove.GetDelay()));
            }
            luaController.RemovePath();
        }
    }

    internal override void RunStart(ViewInterface view)
    {
        script.RunStart(luaController, view.GetSensor());
    }
}
