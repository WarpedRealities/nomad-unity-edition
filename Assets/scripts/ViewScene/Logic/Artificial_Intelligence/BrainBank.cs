﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainBank
{
    Dictionary<string, NPC_Script> scripts;

    public BrainBank()
    {
        scripts = new Dictionary<string, NPC_Script>();
    }


    public NPC_Controller GetBrain(string filename, ZoneActor actor)
    {
        return new NPC_Controller(actor, GetScript(filename));
    }

    private NPC_Script GetScript(string filename)
    {
        NPC_Script script = null;
        if (!scripts.TryGetValue(filename,out script))
        {
            script = new NPC_Script(filename);
            scripts.Add(filename, script);
        }

        return script;
    }

    internal void Update(LuaSensor luaSensor)
    {
        foreach (KeyValuePair<string, NPC_Script> item in scripts)
        {
            item.Value.RunTick(luaSensor);
        }
    }
}
