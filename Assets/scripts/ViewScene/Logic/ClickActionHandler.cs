﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickActionHandler : MonoBehaviour
{
    public ViewScene viewScene;
    public UIController uIController;
    private ViewSceneController sceneController;
    private PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        sceneController = viewScene.GetController();
        player = viewScene.GetPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool LeftClick(Vector2 p)
    {
        Vector2Int pi = new Vector2Int((int)p.x, (int)p.y);
        Tile t = sceneController.GetZoneContents().GetTile(pi.x,pi.y);

        if (t!=null && t.isExplored() && t.isVisible())
        {

            if (t.GetActorInTile() != null)
            {
            
                ZoneActor actor = t.GetActorInTile();
                if (actor.GetAlive() & actor.IsVisible())
                {
                    if (actor.GetAlive() && (
                        (actor.actor.GetFaction().getRelationship(player.actor.GetFaction()) <= 50 && ActorPeacebondCheck(actor))||
                        actor.actor.IsPlayer() && ((PlayerRPG)player.actor.GetRPG()).GetCurrentMove().GetPattern()==AttackPattern.SELF))
                    {
                        if (player.IsQueueEmpty())
                        {
                            return AttackHandler(t, p, viewScene);
                        }

                    }
                    else
                    {
                        if (Vector2.Distance(player.actor.GetPosition(), pi) < 2)
                        {
                            return InteractionHandler(t, p);
                        }
                    }
                }
                else
                {
                    if (Vector2.Distance(player.actor.GetPosition(), pi) < 2)
                    {

                        return InteractionHandler(t, p);
                    }
                }

            }
            if (t.GetWidget() != null)
            {

                if (Vector2.Distance(player.actor.GetPosition(), pi) < 2 && t.GetWidget().CanInteract())
                {

                    return InteractionHandler(t,p);
                }
                else
                {
                    return LookHandler(t);
                }
            }
            //look at the empty tile

            return LookHandler(t);
        }
        return false;
    }

    private bool ActorPeacebondCheck(ZoneActor actor)
    {
        if (actor.actor is NPC)
        {
            NPC npc = (NPC)actor.actor;
            if (npc.IsPeaceful())
            {
                return false;
            }
        }
        return true;
    }

    public bool AttackHandler(Tile t, Vector2 p, ViewInterface view)
    {

        Vector2Int pI = new Vector2Int((int)p.x, (int)p.y);
        //need to check whether the player has the range with their current attack
        CombatMove cMove=((PlayerRPG)player.actor.GetRPG()).GetCurrentMove();
        float distance = Vector2Int.Distance(player.actor.GetPosition(), pI);
        
        //if not, fail this
        if (!AttackInEligible(distance, cMove))
        {

            return false;
        }
        //if so, then launch attack
        if (t.GetActorInTile()!=null)
        {
            if (t.GetActorInTile().GetAlive())
            {

                player.LaunchAttack(t.GetActorInTile(), view);
            }

        }
        else
        {

            player.LaunchAttack(pI,view);
        }
        viewScene.PlayerDoAction();
        return true;

    }

    internal bool ShoveHandler(Tile t, Vector2 p, ViewInterface view)
    {
        Vector2Int pI = new Vector2Int((int)p.x, (int)p.y);
        float distance = Vector2Int.Distance(player.actor.GetPosition(), pI);

        //if so, then launch attack
        if (t.GetActorInTile() != null)
        {
            if (!t.GetActorInTile().GetAlive() || !t.GetActorInTile().GetActor().isHostile("player"))
            {

                player.addAction(new ActionShove(5,pI));
                return true;
            }

        }
        viewScene.PlayerDoAction();
        return false;
    }

    private bool AttackInEligible(float distance, CombatMove cMove)
    {
        //in range
        switch (cMove.GetPattern())
        {
            case AttackPattern.MELEE:
                if (distance < 2)
                {
                    return true;
                } 
                break;
            case AttackPattern.RANGED:
                if (distance< 10)
                {
                    return true;
                }
                break;
            case AttackPattern.SELF:
                if (distance == 0)
                {
                    return true;
                }
                break;
            case AttackPattern.BLAST:
                if (distance < 10)
                {
                    return true;
                }
                break;
        }


        return false;
    }

    public bool InteractionHandler(Tile t, Vector2 p)
    {
        Vector2 p0 = new Vector2((int)p.x, (int)p.y);
        if (Vector2.Distance(p0, new Vector2(player.actor.GetPosition().x, player.actor.GetPosition().y)) < 2) {
            player.addAction(new ActionInteract(new Vector2Int((int)p.x, (int)p.y), (Player)player.actor));
            viewScene.PlayerDoAction();
            return true;
        }
        return false;
    }

    public bool RightClick(Vector2 p, Vector2 sp)
    {
        uIController.GetRightClickUI().ReceiveRightClick(this, p, sp);
        return true;
    }

    public bool LookHandler(Tile t)
    {
   
        if (t.GetActorInTile() != null)
        {

            viewScene.DrawText(t.GetActorInTile().actor.GetDescription());
            return true;
        }
        //widget
        if (t.GetWidget() != null && t.GetWidget().getDescription() != null)
        {
            viewScene.DrawText(t.GetWidget().getDescription());
            return true;
        }
        //tile
        viewScene.DrawText(t.getDefinition().getDescription());
        //viewScene.PlayerDoAction();
        return true;
    }
}
