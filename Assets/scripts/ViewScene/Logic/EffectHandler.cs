﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.XR;
using static System.Collections.Specialized.BitVector32;

public class EffectHandler
{

    public static int ApplyEffect(Effect effect, bool critical, ZoneActor actor, ZoneActor origin, DiceRoller roller, ViewInterface view, float strength = 1)
    {
        
        Actor oActor = origin != null ? origin.actor : null;
        switch (effect.GetEffectType())
        {
            case EffectType.DAMAGE:
                return HandleDamage((EffectDamage)effect, critical, actor, oActor, null, roller, strength);
            case EffectType.RECOVER:
                return HandleRecover((EffectRecover)effect, critical, actor, oActor, roller, view);
            case EffectType.MUTATOR:
                return HandleMutator((EffectMutator)effect, critical, actor, oActor, roller, view);
            case EffectType.STATUS:
                return HandleStatus((EffectStatus)effect, actor, oActor, view, strength);
            case EffectType.STATUSREMOVE:
                return HandleStatusRemove((EffectStatusRemove)effect, actor, oActor, view);
            case EffectType.PERK:
                return HandlePerk((EffectPerk)effect, actor, oActor, view);
            case EffectType.MOVE:
                return HandleMove((EffectMove)effect, actor, origin, view);
            case EffectType.SUMMON:
                return HandleSummon((EffectSummon)effect, actor, origin, view);
            case EffectType.KARMA:
                return HandleKarma((EffectKarma)effect, actor, origin, view);
        }

        return 0;


    }

    private static int HandleKarma(EffectKarma effect, ZoneActor actor, ZoneActor origin, ViewInterface view)
    {
        ((PlayerRPG)actor.actor.GetRPG()).ModKarma(effect.GetModifier());
        return (int)effect.GetModifier();
    }

    private static int HandleStatusRemove(EffectStatusRemove effect, ZoneActor actor, Actor oActor, ViewInterface view)
    {
        for (int i = 0; i < effect.GetStatusRemovals().Length; i++)
        {
            if (actor.actor.GetRPG().GetStatusHandler().HasStatusEffect(effect.GetStatusRemovals()[i]))
            {
                actor.actor.GetRPG().GetStatusHandler().RemoveStatusEffect(effect.GetStatusRemovals()[i], view, actor.actor);
            }
        }
        return 0;
    }

    private static int HandleSummon(EffectSummon effect, ZoneActor actor, ZoneActor origin, ViewInterface view)
    {
        //find nearby spawn point to origin
        int dice = DiceRoller.GetInstance().RollDice(4);
        Vector2 center = actor.getPosition();
        Vector2 position = center;
        for (int i = 0; i < 4; i++)
        {
            int angle = (dice + i) >= 4 ? dice + i - 4 : dice + i;
            if (GeometryTools.isSpawnableAngle(angle, effect.GetDirections()))
            {
                if (GeometryTools.GetSpawnPosition(center, angle, effect.GetMinDistance(), effect.GetMaxDistance(),
                    GlobalGameState.GetInstance().getCurrentZone(), effect.IsHidden(), out position))
                {
                    //spawn the npc at this location
                    NPC npc = new NPC(ActorTools.loadNPC(effect.GetFile()), effect.GetFile());
                    npc.SetPosition(new Vector2Int((int)position.x, (int)position.y));
                    Zone z = GlobalGameState.GetInstance().getCurrentZone();
                    z.GetContents().GetActors().Add(npc);
                    view.AddNPC(npc);
                    return 0;
                }
            }

        }
        return 0;
    }

    private static int HandleMove(EffectMove effect, ZoneActor actor, ZoneActor origin, ViewInterface view)
    {


        Vector2 offset = effect.IsAffectOrigin() ? origin.getPosition() - actor.getPosition() : actor.getPosition() - origin.getPosition();
        offset = !effect.IsMoveAway() ? offset * -1 : offset;
        offset.Normalize();
        float max= (!effect.IsMoveAway() && !effect.IsBypassActors()) ? Vector2.Distance(actor.getPosition(), origin.getPosition())-1 : 99;

        int distanceMod = origin.actor.GetRPG().GetAbilityMod(effect.GetModifierAbility());
        if (effect.IsAffectOrigin())
        {
            if (origin.actor.GetRPG().GetStatusHandler().IsBound())
            {
                return 0;
            }
            //no roll to resist
            float distance = DiceRoller.GetInstance().RollDice(effect.GetSuccessDistance() - effect.GetFailDistance()) + effect.GetFailDistance() + distanceMod;
            distance = distance > max ? max : distance;
            Vector2Int nuposition = CollisionTools.MovePath(origin.actor.GetPosition(), offset, distance, GlobalGameState.GetInstance().getCurrentZone());
            origin.SetActorAnimation(new MoveAnimation(0.25F, origin.getPosition(), nuposition, origin));
            origin.Reposition(nuposition);
            view.UpdateCamera();
        }
        else
        {
            if (actor.actor.GetRPG().GetStatusHandler().IsBound() || actor.actor.IsImmobile())
            {
                return 0;
            }
            int diceRoll = DiceRoller.GetInstance().RollDice(100) + effect.GetStrength() - actor.actor.GetRPG().getDefenceResist(effect.GetResist());
            if (diceRoll > 50)
            {
                //apply success
                float distance = effect.GetSuccessDistance() + distanceMod;
                distance = distance > max ? max : distance;
                Vector2Int nuposition = CollisionTools.MovePath(actor.actor.GetPosition(), offset, distance, GlobalGameState.GetInstance().getCurrentZone());
                actor.SetActorAnimation(new MoveAnimation(0.25F, actor.getPosition(), nuposition, actor));
                actor.Reposition(nuposition);
                view.UpdateCamera();
                return 0;
            }
            else
            {
                //apply fail
                float distance = effect.GetFailDistance() + distanceMod;
                distance = distance > max ? max : distance;
                Vector2Int nuposition = CollisionTools.MovePath(actor.actor.GetPosition(), offset, distance, GlobalGameState.GetInstance().getCurrentZone());
                actor.SetActorAnimation(new MoveAnimation(0.25F, actor.getPosition(), nuposition, actor));
                actor.Reposition(nuposition);
                view.UpdateCamera();
                return 0;
            }
        }

        return 0;
    }

    private static int HandleInterrupt(EffectInterrupt effect, bool critical, ZoneActor zoneActor, Actor origin, AmmoType ammo, DiceRoller roller, ViewInterface view)
    {
        Actor actor = zoneActor.actor;
        ActorRPG rpg = actor.GetRPG();
        if (!zoneActor.GetController().CanInterrupt(effect.GetInterrupt()))
        {
            return 0;
        }
        float str = effect.GetStrength();
        if (critical)
        {
            str += 50;
        }
        if (ammo != null)
        {
            str = str * ammo.GetImpactMod();
        }
        int diceroll = (int)str+ DiceRoller.GetInstance().RollDice(100) - rpg.getDefenceResist(effect.GetResist());
        if (diceroll > 50)
        {
            zoneActor.GetController().Interrupt(effect.GetInterrupt());
            if (effect.GetStun()>0 && diceroll > 50 + effect.GetThreshold())
            {
               
                zoneActor.GetController().addAction(new ActionDelay(effect.GetStun()));
            }          
        }
        return 0;
    }

    private static int HandleStatus(EffectStatus effect, ZoneActor zoneActor, Actor origin, ViewInterface view, float strength = 1)
    {
        Actor actor = zoneActor.actor;
        ActorRPG rpg = actor.GetRPG();
   
        StatusEffectHandler handler=rpg.GetStatusHandler();
        if (handler.HasStatusEffect(effect.GetStatus().GetUid()))
        {
            return 0;
        }
        if (effect.GetResistance() == DR.NONE)
        {
            handler.AddStatusEffect(effect.GetStatus().Clone());
            view.DrawText(effect.GetDescription());
            return 1;
        }
        else
        {
            int mod = rpg.getDefenceResist(effect.GetResistance()) - ((int)(effect.GetStrength()*strength));
            int diceroll =  DiceRoller.GetInstance().RollDice(100);
            if (diceroll+mod <= 50)
            {

                StatusEffect statusEffect = effect.GetStatus().Clone();
                if (statusEffect is StatusBind && origin is NPC)
                {
                    ((StatusBind)statusEffect).SetOrigin((NPC)origin);
                    if (zoneActor.GetController().CurrentActionIsMove())
                    {
                        zoneActor.GetController().PurgeActions();
                    }
                }
                if (statusEffect is StatusDamageOverTime && origin is NPC)
                {
                    ((StatusDamageOverTime)statusEffect).SetOrigin((NPC)origin);
                }
                handler.AddStatusEffect(statusEffect);
                view.DrawText(effect.GetDescription().Replace("TARGET",actor.GetName()));
                return 1;
            }
            else
            {

                view.DrawText(effect.GetResistDescription().Replace("TARGET", actor.GetName()));

            }
            return 0;
        }
        throw new NotImplementedException();
    }

    private static int HandlePerk(EffectPerk effect, ZoneActor actor, Actor origin,ViewInterface view)
    {
        Player player = (Player)actor.actor;
        PlayerRPG rpg = (PlayerRPG)player.GetRPG();
        for (int i = 0; i < rpg.GetPerks().Count; i++)
        {
            if (rpg.GetPerks()[i].GetRef().GetCode().Equals(effect.GetPerkCode()))
            {
                //apply exp instead
                float exp = rpg.GetNextLevel() * effect.GetExperience();
                rpg.AddExperience((int)exp, view);
                return 0;
            }
        }
        Perk perk = GlobalGameState.GetInstance().GetUniverse().GetPerkLibrary().GetPerk(effect.GetPerkCode());
        rpg.AddPerk(perk);
        view.DrawText("You have gained the perk " + perk.GetName());
        return 0;
    }

    private static int HandleMutator(EffectMutator effect, bool critical, ZoneActor actor, Actor origin, DiceRoller roller, ViewInterface view)
    {
        if (actor.actor.IsPlayer())
        {
            bool valid = true;
            Appearance appearance = ((Player)actor.actor).GetAppearance();
            for (int i = 0; i < effect.Getconditions().Count; i++)
            {
                if (!effect.Getconditions()[i].MetCriteria(appearance))
                {
                    valid = false;
                    break;
                }
            }
            if (!valid)
            {
                if (effect.GetFailure() != null)
                {
                    view.DrawText(effect.GetFailure());
                }
            }
            else
            {
                int value = effect.GetMutation().ApplyMutation(appearance);
                string valueString = value.ToString();
                view.DrawText(effect.GetSuccess().Replace("CHANGE", valueString));
            }
        }

        return 0;
    }

    public static int ApplyEffect(Effect effect, bool critical, ZoneActor actor, ZoneActor origin, AmmoType ammoType, DiceRoller roller, ViewInterface view)
    {

        Actor oActor = origin != null ? origin.actor : null;
        switch (effect.GetEffectType())
        {
            case EffectType.DAMAGE:
                return HandleDamage((EffectDamage)effect, critical, actor, oActor, ammoType, roller);
            case EffectType.MUTATOR:
                return HandleMutator((EffectMutator)effect, critical, actor, oActor, roller, view);
            case EffectType.RECOVER:
                return HandleRecover((EffectRecover)effect, critical, actor, oActor, roller, view);
            case EffectType.STATUS:
                return HandleStatus((EffectStatus)effect, actor, oActor, view);
            case EffectType.INTERRUPT:
                return HandleInterrupt((EffectInterrupt)effect, critical, actor, oActor, ammoType, roller,view);
            case EffectType.SUBMIT:
                return HandleSubmit((EffectSubmit)effect, actor, oActor, view);
            case EffectType.MOVE:
                return HandleMove((EffectMove)effect, actor, origin, view);
            case EffectType.SUMMON:
                return HandleSummon((EffectSummon)effect, actor, origin, view);
            case EffectType.ITEM:
                return HandleItem((EffectItem)effect, actor, origin, view);
            case EffectType.ANALYSE:
                return HandleAnalyse((EffectAnalyse)effect, actor, origin, view);
            case EffectType.SCAN:
                return HandleScan((EffectScan)effect, actor, origin, view);
        }

        return 0;
    }

    private static int HandleScan(EffectScan effect, ZoneActor actor, ZoneActor origin, ViewInterface view)
    {
        return 0;
    }

    private static int HandleAnalyse(EffectAnalyse effect, ZoneActor actor, ZoneActor origin, ViewInterface view)
    {
        StringBuilder stringBuilder = new StringBuilder();
        int data = 0;
        data = AnalysisBuilderTool.BuildAnalysis(effect, actor, stringBuilder, origin.GetActor().GetRPG().getSkill(SK.SCIENCE));
        if (data > 0)
        {
            view.DrawText(stringBuilder.ToString());
        }
        return 0;
    }

    private static int HandleItem(EffectItem effect, ZoneActor actor, ZoneActor origin, ViewInterface view)
    {
        Item item = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(effect.GetItem());

        if (DropOnTile(item, actor.getPosition(), view))
        {
            return 0;
        }
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p0 = GeometryTools.GetPosition(actor.getPosition().x, actor.getPosition().y, i);
            if (p0.x >= 0 && p0.x < zone.GetContents().GetZoneParameters().GetWidth() &&
                p0.y >= 0 && p0.y < zone.GetContents().GetZoneParameters().GetHeight())
            {
                if (DropOnTile(item, p0, view))
                {
                    return 0;
                }
            }
        }
        return 0;
    }

    private static bool DropOnTile(Item item, Vector2Int p, ViewInterface view)
    {
        Tile t = GlobalGameState.GetInstance().getCurrentZone().GetContents().getTiles()[p.x][p.y];
        //is the tile i'm standing on empty of widgets?
        if (t.GetWidget() == null)
        {
            //if so, simply place the item in a widget pile where i'm standing
            t.SetWidget(new WidgetItemPile(item));

            view.CalculateVision();
            return true;
        }
        //is there an existing widget item pile where i'm standing?
        if (t.GetWidget() is WidgetItemPile)
        {
            //if so add to that pile
            WidgetItemPile widgetItemPile = (WidgetItemPile)t.GetWidget();
            widgetItemPile.AddItem(item);
            view.CalculateVision();
            return true;
        }
        return false;
    }

    private static int HandleSubmit(EffectSubmit effect, ZoneActor actor, Actor origin, ViewInterface view)
    {
        if (effect.IsResolve())
        {

            if (origin.IsPlayer())
            {
                NPC_RPG nPC_RPG = (NPC_RPG)actor.actor.GetRPG();
                string conversation = nPC_RPG.GetStatBlock().GetConversationFiles()[(int)conversationType.DOMINANT];
                if (conversation != null)
                {
                    origin.GetRPG().ReduceStat(ST.RESOLVE, 999);
                    MonsterVictoryTool.RunDominate(actor, view.GetPlayerInZone(), view);
                }
            }
        }
        else
        {

            if (origin.IsPlayer())
            {
   
                NPC_RPG nPC_RPG = (NPC_RPG)actor.actor.GetRPG();
                string conversation = nPC_RPG.GetStatBlock().GetConversationFiles()[(int)conversationType.VICTORY];
                if (conversation != null)
                {

                    origin.GetRPG().ReduceStat(ST.HEALTH, 999);
                    MonsterVictoryTool.RunVictory(actor, view.GetPlayerInZone(), view);
                }
            }
        }
        return 0;
    }

    private static int HandleRecover(EffectRecover effect, bool critical, ZoneActor actor, Actor origin, DiceRoller roller, ViewInterface view)
    {
        float amount = effect.GetModifier();
        if (effect.GetStat() == ST.SATIATION)
        {
            ((PlayerRPG)actor.actor.GetRPG()).Feed(amount, true);
        }
        else
        {
            actor.actor.GetRPG().IncreaseStat(effect.GetStat(), amount);
        }
        view.DrawText(effect.GetEffectText());
        return 0;
    }

    private static int HandleDamage(EffectDamage effect, bool critical, ZoneActor actor, Actor origin, AmmoType ammoType, DiceRoller roller, float strength = 1)
    {

        float distance = origin!= null ? 
            Vector2.Distance(actor.actor.GetPosition(), origin.GetPosition())-1 
            : 0;
        int max = (int)(ammoType!=null ? effect.GetMax()*ammoType.GetDamageMod() : effect.GetMax());
        //do damage roll
        int dmg = effect.GetMin() == max ?  effect.GetMin() : roller.RollDice(max-effect.GetMin())+effect.GetMin();
        if (critical)
        {
            dmg = HandleCritical(dmg, effect.GetDamagetype(), effect.GetMin(), max);
        }
        dmg = (int)(dmg * strength);
        //add bonus
        dmg += origin!=null ? 
            (int)(origin.GetRPG().GetAbilityMod(effect.GetAbilityModifier()) * effect.GetAbilityMultiplier()) 
            : 0;
        if (effect.GetRangeFalloff() > 0)
        {
            dmg -= (int)(distance * effect.GetRangeFalloff());
            if (dmg < 0)
            {
                dmg = 0;
            }
        }
        //subtract defence

        dmg -= (int)(actor.actor.GetRPG().getDefenceResist(effect.GetDamagetype())*(1-effect.GetPenetration()));

        dmg = dmg < 0 ? 0 : dmg;
        ST statFromDamage = GetStatFromDamage(effect.GetDamagetype());
        if (origin!=null && origin.IsPlayer() && actor.actor.GetCrimeFlags()!=null && !actor.actor.GetCrimeFlags().ReadFlag((int)statFromDamage))
        {

            //need to handle crime reporting here
            if (actor.actor.GetFaction().GetFactionRuleset() != null)
            {
                actor.actor.GetFaction().GetFactionRuleset().ReportCrime((ViolationType)((int)statFromDamage), actor.actor);
            }
            actor.actor.GetCrimeFlags().SetFlag((int)statFromDamage);
        }
        //if damage is above zero then apply interrupt
        if (dmg > 0)
        {
            //apply dmg
            
            actor.actor.GetRPG().ReduceStat(statFromDamage, dmg);
            if ((int)effect.GetDamagetype()<3)
            {
                int str = dmg * max*2;
                int roll = DiceRoller.GetInstance().RollDice(100) - actor.actor.GetRPG().getDefenceResist(DR.POISE);
                if (roll > 50)
                {
                    //apply interrupt
                    actor.Interrupt(effect.IsMelee() ? InterruptType.MELEE : InterruptType.DAMAGE);
                }
            }       
        }

        return dmg >= 0 ? dmg : 0;  
    }

    private static int HandleCritical(int dmg, DR dR, int min, int max)
    {
        switch (dR)
        {
            case DR.KINETIC:
                return dmg * 2;
            case DR.TEASE:
                return dmg * 2;
            case DR.THERMAL:
                return max;
            case DR.PSI:
                return max;
            case DR.PHEROMONE:
                return dmg + min != max ? DiceRoller.GetInstance().RollDice(max - min) + min : max;
            case DR.SHOCK:
                return dmg + min != max ? DiceRoller.GetInstance().RollDice(max - min) + min : max;
        }
        return dmg;
    }

    private static ST GetStatFromDamage(DR dR)
    {
        if (dR == DR.KINETIC || dR == DR.THERMAL || dR == DR.SHOCK || dR == DR.NONE)
        {
            return ST.HEALTH;
        }
        else
        {
            return ST.RESOLVE;
        }

    }
}