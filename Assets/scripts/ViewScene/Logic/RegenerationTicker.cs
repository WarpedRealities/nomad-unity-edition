﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RegenerationTicker
{
    private const string STARVATION = "You have succumbed to starvation";
    private const int TIMECYCLE = 50;
    private const float RESOLVEREGEN = 1.0F;
    private Player player;
    private PlayerRPG playerRPG;
    private int timeClock; 
    public RegenerationTicker(Player player)
    {
        this.player = player;
        this.playerRPG = (PlayerRPG)player.GetRPG();
        this.timeClock = 0;
    }

    public void Update(int time=1, bool suppress = false)
    {

        timeClock+=time;

        while (timeClock >= TIMECYCLE)
        {
            timeClock -= TIMECYCLE;

            if (player.GetSuppression() > 0)
            {
                player.DecrementSuppression();
            }
            else if (!suppress)
            {
                HandleRegen();
            }
            HandleDecay();
        }

    }

    private void HandleDecay(int time=1)
    {
        if (playerRPG.GetStat(ST.SATIATION) > 0)
        {
            //decay satiation
            playerRPG.ReduceStat(ST.SATIATION, SatiationCost()*time);
        } else
        {
            //start decreasing health
            playerRPG.ReduceStat(ST.HEALTH, playerRPG.GetSecondaryValue(SV.DECAY)*time);
            if (playerRPG.GetStat(ST.HEALTH) <= 0)
            {
                TextLog.Log(STARVATION);
                SceneManager.LoadScene("GameOverScene");
            }
        }
    }

    private float SatiationCost()
    {
        return playerRPG.GetSecondaryValue(SV.DECAY) +
                (playerRPG.GetSecondaryValue(SV.DECAY) * (((float)playerRPG.GetKarma()) / 100));
    }

    private void HandleRegen(int time=1)
    {

        if (playerRPG.GetStat(ST.HEALTH) < playerRPG.GetStatMax(ST.HEALTH))
        {
            float sProportion = ((float)playerRPG.GetStat(ST.SATIATION))/((float)playerRPG.GetStatMax(ST.SATIATION));
            //health regen, costs satiation, has threshold
            if (sProportion > playerRPG.GetSecondaryValue(SV.REGENTHRESHOLD))
            {
                //enough satiation to regenerate
                float regen = playerRPG.GetSecondaryValue(SV.REGENRATE)*time;
                playerRPG.IncreaseStat(ST.HEALTH, regen);
                playerRPG.ReduceStat(ST.SATIATION, regen* SatiationCost()*time);
            }
        }
        if (playerRPG.GetStat(ST.RESOLVE) < playerRPG.GetStatMax(ST.RESOLVE))
        {
            //resolve regen, no threshold, no satiation cost
            playerRPG.IncreaseStat(ST.RESOLVE, RESOLVEREGEN*time);
        }

    }
}
