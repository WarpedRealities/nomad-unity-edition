﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;


public class ViewSceneController 
{
    private BrainBank brainBank;
    private GlobalGameState gameState;
    private List<ZoneActor> zoneActors;
    private ZoneActor playerInZone;
    private Player player;
    private RegenerationTicker regenerationTicker;
    private EventTicker eventTicker;
    PlayerTargetControl targetControl;

    public ViewSceneController(GlobalGameState globalGameState, BrainBank brainBank)
    {
        this.brainBank = brainBank;
        this.gameState = globalGameState;
        ZoneRestoreTool.ZoneChecker(gameState.getCurrentZone());
    }

    public bool PlayerCanAct()
    {

        return playerInZone.GetController().CanAct() || !playerInZone.actor.GetAlive();
    }

    public void Update(ViewInterface view)
    {
        regenerationTicker.Update();
        eventTicker.Update();
        for (int i=0;i<zoneActors.Count;i++)
        {
            zoneActors[i].UpdateLogic(view);
        }
        if (gameState.GetUniverse().GetClock() % 10 == 0)
        {
            brainBank.Update(view.GetSensor());
        }
 
        if (gameState.GetUniverse().GetClock() % 50 == 0)
        {

            if (gameState.getCurrentZone().GetContents().GetConditions() != null)
            {

                gameState.getCurrentZone().GetContents().GetConditions().RunConditions(gameState.GetPlayer(),view);
            }
        }
        gameState.IncrementClock();
    }

    internal List<ZoneActor> GetZoneActors()
    {
        return this.zoneActors;
    }

    internal void InitialUpdate(ViewInterface view)
    {
        for (int i = 0; i < zoneActors.Count; i++)
        {
            zoneActors[i].InitialUpdate(view);
        }
    }

    public void SetActors(List<ZoneActor> zoneActorScripts)
    {
        this.zoneActors = zoneActorScripts;
    }

    public void SetPlayer(ZoneActor playerInZone, Player player)
    {
        this.playerInZone = playerInZone;
        this.player = player;
        this.regenerationTicker = new RegenerationTicker(player);
        this.eventTicker = new EventTicker();
        this.targetControl = new PlayerTargetControl(playerInZone, zoneActors);
    }

    public ZoneContents GetZoneContents()
    {
        return this.gameState.getCurrentZone().GetContents();
    }

    public WidgetPortal getPortal(ZoneContents zone, int uid, out Vector2Int p)
    {

        for (int i = 0; i < zone.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zone.GetZoneParameters().GetHeight(); j++)
            {
                Tile t = zone.getTiles()[i][j];
                if (t != null && t.GetWidget() != null)
                {
                    if (t.GetWidget() is WidgetPortal)
                    {
                        WidgetPortal portal = (WidgetPortal)t.GetWidget();
                        if (portal.getID() == uid)
                        {
                            p = new Vector2Int(i, j);
                            return portal;
                        }
                    }
                }
            }
        }
        p = new Vector2Int(0, 0);
        return null;
    }

    public void Transition(string destination, int uid)
    {

        Zone zone = gameState.getCurrentEntity().GetZone(destination);
        if (zone == null)
        {
            return;
        }
        if (!zone.IsGenerated())
        {
            zone.Generate();
        }
        Vector2Int pos;
        WidgetPortal p = getPortal(zone.GetContents(),uid,out pos);
        if (p != null)
        {
            GameObject.Find("CameraOut").SetActive(false);
            Actor companion = gameState.getCurrentZone().CompanionCheck(player.GetCompanionUID());
            gameState.getCurrentZone().ZoneCleanup();
            gameState.setCurrentZone(zone);
            gameState.getCurrentZone().Regenerate(gameState.GetUniverse().GetClock());
            player.SetPosition(GeometryTools.GetPosition(pos.x, pos.y, p.getFacing()));
            if (companion != null)
            {
                gameState.getCurrentZone().CompanionAdd(player.GetPosition(), companion);
            }
            SaveTool.Save("autosave");
            SceneManager.LoadScene("viewScene");
        }
    }

    internal void Transition(string destination, int x, int y)
    {
        Zone zone = gameState.getCurrentEntity().GetZone(destination);
        if (zone == null)
        {
            return;
        }
        if (!zone.IsGenerated())
        {
            zone.Generate();
        }
        GameObject.Find("CameraOut").SetActive(false);
        Vector2Int pos=new Vector2Int(x,y);
        Actor companion = gameState.getCurrentZone().CompanionCheck(player.GetCompanionUID());
        gameState.getCurrentZone().ZoneCleanup();
        gameState.setCurrentZone(zone);
        gameState.getCurrentZone().Regenerate(gameState.GetUniverse().GetClock());
        player.SetPosition(pos);
        if (companion != null)
        {
            gameState.getCurrentZone().CompanionAdd(player.GetPosition(), companion);
        }
        SaveTool.Save("autosave");
        SceneManager.LoadScene("viewScene");
    }

    public void EdgeTransition(int side, string destination)
    {

        Zone zone = gameState.getCurrentEntity().GetZone(destination);
        if (!zone.IsGenerated())
        {
            zone.Generate();
        }
        Vector2Int pos;
        switch (side)
        {
            case 0:
                pos = new Vector2Int(player.GetPosition().x, 1);
                break;
            case 1:
                pos = new Vector2Int(1, player.GetPosition().y);
              
                break;
            case 2:
                pos = new Vector2Int(player.GetPosition().x, zone.GetContents().GetZoneParameters().GetHeight()-2);
                break;
            default:
                pos = new Vector2Int(zone.GetContents().GetZoneParameters().GetWidth() - 2, player.GetPosition().y);
                break;
        }
        GameObject camera = GameObject.Find("CameraOut");
        if (camera != null)
        {
            camera.SetActive(false);
        }
        Actor companion = gameState.getCurrentZone().CompanionCheck(player.GetCompanionUID());
        gameState.getCurrentZone().ZoneCleanup();
        gameState.setCurrentZone(zone);
        gameState.getCurrentZone().Regenerate(gameState.GetUniverse().GetClock());

        if (side==0 || side == 2)
        {
            pos = EdgeTransitionSafetyX(pos, zone.GetContents());
        }
        else
        {
            pos = EdgeTransitionSafetyY(pos, zone.GetContents());
        }
        player.SetPosition(pos);
        if (companion != null)
        {
            gameState.getCurrentZone().CompanionAdd(player.GetPosition(), companion);
        }
        SaveTool.Save("autosave");
        SceneManager.LoadScene("viewScene");
    }

    public PlayerTargetControl GetTargetControl()
    {
        return targetControl;
    }


    private Vector2Int EdgeTransitionSafetyX(Vector2Int pos, ZoneContents zone)
    {
        Tile t = zone.getTiles()[pos.x][pos.y];
        int OffsetY = pos.y > 2 ? 1 : -1;
        Tile t0 = zone.getTiles()[pos.x][pos.y + OffsetY];
        int adjustment = pos.x > zone.GetZoneParameters().GetWidth() / 2 ? -1 : 1;
        while (t==null || !t.canWalk() || t0 == null || !t0.canWalk())
        {
            pos.x += adjustment;
            if (pos.x<0)
            {
                pos.x = zone.GetZoneParameters().GetWidth() - 1;
            }
            if (pos.x>= zone.GetZoneParameters().GetWidth())
            {
                pos.x = 0;
            }
            t = zone.getTiles()[pos.x][pos.y];
            t0 = zone.getTiles()[pos.x + OffsetY][pos.y];
        }
        return pos;
    }
    private Vector2Int EdgeTransitionSafetyY(Vector2Int pos, ZoneContents zone)
    {
        Tile t = zone.getTiles()[pos.x][pos.y];
        int OffsetX = pos.x > 2 ? 1 : -1;
        Tile t0 = zone.getTiles()[pos.x + OffsetX][pos.y];
        int adjustment = pos.y > zone.GetZoneParameters().GetHeight() / 2 ? -1 : 1;
        while (t == null || !t.canWalk() || t0 == null || !t0.canWalk())
        {
            pos.y += adjustment;
            if (pos.y < 0)
            {
                pos.y = zone.GetZoneParameters().GetHeight() - 1;
            }
            if (pos.y >= zone.GetZoneParameters().GetHeight())
            {
                pos.y = 0;
            }
            t = zone.getTiles()[pos.x][pos.y];
            t0 = zone.getTiles()[pos.x + OffsetX][pos.y];
        }

        return pos;
    }


    public void HandleThreats()
    {
        for (int i = 0; i < zoneActors.Count; i++)
        {
            if (zoneActors[i].GetController().IsThreatening())
            {
                CollisionTools.SetThreat(zoneActors[i].getPosition(), zoneActors[i]);
            }
        }
    }

    internal void ReadActions()
    {
        for (int i = 0; i < zoneActors.Count; i++)
        {
            zoneActors[i].GetController().ReadActions();
        }
    }

    public void UpdateVision()
    {
        for (int i=0;i< zoneActors.Count; i++)
        {
            if (zoneActors[i] != null)
            {
                if (CollisionTools.GetTile(zoneActors[i].getPosition()) != null
                    && CollisionTools.GetTile(zoneActors[i].getPosition()).isVisible()
                    && !zoneActors[i].IsInvisible()) {
                    zoneActors[i].SetVisible(true);

                }
                else
                {
                    zoneActors[i].SetVisible(false);
                }
            }
        }
    }

    public void UpdateTargeting()
    {
        targetControl.UpdateTargeting();
    }

    public ZoneActor GetPlayerInZone()
    {
        return playerInZone;
    }

    public void RemoveWidget(Widget widget)
    {
        ZoneContents zoneContents = gameState.getCurrentZone().GetContents();
       for (int i = 0; i < zoneContents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zoneContents.GetZoneParameters().GetHeight(); j++)
            {
                Tile t = zoneContents.getTiles()[i][j];

                if (t!=null && widget.Equals(t.GetWidget()))
                {
                    zoneContents.getTiles()[i][j].SetWidget(null);
                    return;
                }
            }
        }
    }
    internal void ReplaceWidget(Widget widget0, Widget widget1)
    {
        ZoneContents zoneContents = gameState.getCurrentZone().GetContents();
        for (int i = 0; i < zoneContents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zoneContents.GetZoneParameters().GetHeight(); j++)
            {
                Tile t = zoneContents.getTiles()[i][j];

                if (t != null && widget0.Equals(t.GetWidget()))
                {
                    zoneContents.getTiles()[i][j].SetWidget(widget1);
                    return;
                }
            }
        }
    }
    public Vector2Int GetWidgetPosition(Widget widget)
    {
        ZoneContents zoneContents = gameState.getCurrentZone().GetContents();
        for (int i = 0; i < zoneContents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zoneContents.GetZoneParameters().GetHeight(); j++)
            {
                Tile t = zoneContents.getTiles()[i][j];

                if (t != null)
                {
                    if (widget.Equals(t.GetWidget()))
                    {
                        return new Vector2Int(i, j);
                    }
                    if (t.GetWidget() is WidgetSlot)
                    {
                        WidgetSlot ws = (WidgetSlot)t.GetWidget();
                        if (widget.Equals(ws.GetContainedWidget()))
                        {
                            return new Vector2Int(i, j);
                        }
                    }
                }
            }
        }
        return new Vector2Int(0,0);
    }

    internal void RefreshActors()
    {
        for (int i = 0; i < zoneActors.Count; i++)
        {
            if (zoneActors[i].GetAlive())
            {
                zoneActors[i].Regenerate();
            }
        }
    }

    internal RegenerationTicker GetRegenerationTicker()
    {
        return regenerationTicker;
    }

    internal void RunStartAI(ViewInterface view)
    {
        for (int i = 0; i < zoneActors.Count; i++)
        {
            if (zoneActors[i].GetAlive())
            {
                zoneActors[i].GetController().RunStart(view);
            }
        }
    }
}
