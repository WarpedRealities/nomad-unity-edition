﻿using JetBrains.Annotations;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneActor : MonoBehaviour, DialogueActor
{
    public ActorAnimation actorAnimation;
    public Actor actor;
    private LuaActor luaActor;
    private bool defeatState;
    private ActorController controller;

    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    // Start is called before the first frame update
    void Start()
    {
        if (this.actor.GetRPG().GetStat(ST.HEALTH) <= 0)
        {
            ZoneActorRenderTool.regenerate(actor.getSprite(), meshRenderer, meshFilter, 2);
        }
        if (this.actor.GetRPG().GetStat(ST.RESOLVE) <= 0)
        {
            ZoneActorRenderTool.regenerate(actor.getSprite(), meshRenderer, meshFilter, 3);
        }



    }

    public Actor GetActor()
    {
        return actor;
    }

    public void Regenerate()
    {
        if (this.actor.GetRPG().GetStat(ST.HEALTH) <= 0)
        {
            ZoneActorRenderTool.regenerate(actor.getSprite(), meshRenderer, meshFilter, 2);
        }
        else if (this.actor.GetRPG().GetStat(ST.RESOLVE) <= 0)
        {
            ZoneActorRenderTool.regenerate(actor.getSprite(), meshRenderer, meshFilter, 3);
        }
        else
        {
            ZoneActorRenderTool.regenerate(actor.getSprite(), meshRenderer, meshFilter, 0);
        }
    }

    public void SetActor(Actor actor)
    {
        this.actor = actor;
        luaActor = new LuaActor(this);
        meshFilter = gameObject.GetComponent<MeshFilter>();
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        transform.position = new Vector3(actor.GetPosition().x, actor.GetPosition().y, 0.5F);
        ZoneActorRenderTool.generate(actor.getSprite(), meshRenderer, meshFilter, 0);
    }

    internal void InitialUpdate(ViewInterface view)
    {
        this.actor.GetRPG().GetStatusHandler().Update(0, actor, view);
    }

    public void Remove(bool dropLoot = false)
    {
        this.actor.Remove(dropLoot);
        //make invisible
        this.meshRenderer.enabled = false;
        //remove from tile
        Vector2Int p = actor.GetPosition();
        CollisionTools.RemoveActor(this);
        CollisionTools.setActorInTile(p,null);
        p.x = p.x-256;
        p.y = p.y-256;
        actor.SetPosition(p);
        transform.position = new Vector3(p.x, p.y, 0.5F);

    }

    public void Reposition(Vector2Int position)
    {
        Vector2Int p = actor.GetPosition();
        CollisionTools.RemoveActor(this);
        CollisionTools.setActorInTile(p, null);
        actor.SetPosition(position);
        ForceUpdate();
    }

    internal void StealthCheck(Player player, bool repeatable, int bonus=0)
    {
        int roll = DiceRoller.GetInstance().RollDice(20) + bonus +
            (repeatable ? player.GetRPG().getSkill(SK.SCIENCE) : player.GetRPG().getSkill(SK.PERCEPTION));
        if (actor.GetRPG().GetStatusHandler().GetStatusStealth().StealthCheck(roll, repeatable))
        {
            actor.GetRPG().GetStatusHandler().RemoveStatusEffect(
                actor.GetRPG().GetStatusHandler().GetStatusStealth().GetUid(), null, actor);
            UpdateVisibility();
        }
    }

    public bool IsInvisible()
    {
        return this.actor.GetRPG().GetStatusHandler().GetStatusStealth()!=null;
    }

    public bool GetAlive()
    {     
        return actor.GetRPG().GetStat(ST.HEALTH) > 0 && actor.GetRPG().GetStat(ST.RESOLVE) > 0;
    }

    public void SetController(ActorController controller)
    {
        this.controller = controller;
    }

    public void UpdateLogic(ViewInterface view)
    {
        if (actor.GetObserverVoreHandler() != null)
        {
            if (actor.GetObserverVoreHandler().Update(1, this, view))
            {
                ((NPC)actor).GetObserverVoreHandler().Remove();
                ((NPC)actor).SetObserverVoreHandler(null);
            }
            else if (actor.GetObserverVoreHandler().blockAction())
            {
                return;
            }
        }
        if (GetAlive() && actor.GetPosition().x>=0)
        {
            if (controller.UpdateLogic(view))
            {
                UpdateVisibility();
                transform.position = new Vector3(actor.GetPosition().x, actor.GetPosition().y, 0.5F);
                if (defeatState)
                {
                    defeatState = false;
                    ZoneActorRenderTool.regenerate(actor.getSprite(), meshRenderer, meshFilter, 0);
                }
            }
            actor.GetRPG().GetStatusHandler().Update(1, this.actor, view);
        } else if (actor.GetPosition().x >= 0)
        {
            actor.RunDead(this);
        }
    }

    public void ForceUpdate(bool regen=false)
    {
        UpdateVisibility();
        CollisionTools.setActorInTile(getPosition(), this);
        transform.position = new Vector3(actor.GetPosition().x, actor.GetPosition().y, 0.5F);
        if ((regen || defeatState) && GetAlive())
        {
            defeatState = false;
            ZoneActorRenderTool.regenerate(actor.getSprite(), meshRenderer, meshFilter, 0);
        }
    }

    public void Interrupt(InterruptType interruptType)
    {
        controller.Interrupt(interruptType);
    }

    public void UpdateVisibility()
    {
        if (CollisionTools.GetTile(actor.GetPosition())!=null && 
            CollisionTools.GetTile(actor.GetPosition()).isVisible() &&
            !IsInvisible())
        {
            meshRenderer.enabled = true;
        }
        else
        {
            meshRenderer.enabled = false;
        }
    }

    public void HandleDelayedText(ViewInterface view)
    {
        controller.HandleDelayedText(view);
    }

    public int ApplyEffect(Effect effect, bool critical, ZoneActor origin, AmmoType ammoType, DiceRoller roller,ViewInterface view)
    {
        int amount = EffectHandler.ApplyEffect(effect, critical, this, origin, ammoType, roller, view);
        if (effect.GetEffectType() == EffectType.DAMAGE)
        {
            EffectDamage effectDamage = (EffectDamage)effect;
            view.DrawNumber(this.getPosition(), amount, 
                (int)effectDamage.GetDamagetype() <= (int)DR.SHOCK ? 0 : 1 );
        }
        if (amount>0 && actor.GetObserverVoreHandler() != null)
        {
            if (actor.GetObserverVoreHandler().GetStage() == 0)
            {
                ((NPC)actor).GetObserverVoreHandler().Remove();
                ((NPC)actor).SetObserverVoreHandler(null);
            }
        }
        if (!actor.GetAlive())
        {
            controller.PurgeActions();
        }
        return amount;
    }

    public bool IsVisible()
    {
        return meshRenderer.enabled;
    }

    public LuaActor GetLuaActor()
    {
        return luaActor;
    }

    public void CheckDefeat(ZoneActor actor, ViewInterface view)
    {
        if (this.actor.GetRPG().GetStat(ST.HEALTH)<=0)
        {
            controller.PurgeActions();
            ZoneActorRenderTool.regenerate(actor.actor.getSprite(), meshRenderer, meshFilter, 2);
            defeatState = true;
            if (this.actor.IsPlayer())
            {
                if (actor.actor is NPC)
                {
                    MonsterVictoryTool.RunVictory(actor, this, view);
                }
                if (actor.actor.IsPlayer())
                {
                    view.SetScreen(new ScreenDataGameover("You have perished"));
                }

            }
            if (this.actor is NPC && actor.actor.IsPlayer())
            {
                ((NPC_RPG)this.actor.GetRPG()).AddExp(actor.actor, view);
            }
        }
        if (this.actor.GetRPG().GetStat(ST.RESOLVE) <= 0)
        {
            controller.PurgeActions();
            ZoneActorRenderTool.regenerate(actor.actor.getSprite(), meshRenderer, meshFilter, 3);
            defeatState = true;
            if (this.actor.IsPlayer() && actor.actor is NPC)
            {
                MonsterVictoryTool.RunDominate(actor, this, view);      
            }
            if (this.actor is NPC && actor.actor.IsPlayer())
            {
                ((NPC_RPG)this.actor.GetRPG()).AddExp(actor.actor, view);
            }
        }
    }

    internal void Interact(ViewInterface view, Player player)
    {
        if (this.actor is NPC)
        {
            NPC npc = (NPC)this.actor;

            //if seduced use seduced interaction
            if (npc.GetRPG().GetStat(ST.RESOLVE) <= 0)
            {
                string conversation = ((NPC_RPG)npc.GetRPG()).GetConversationFile((int)conversationType.SEDUCED);
                if (conversation != null)
                {
                    view.SetScreen(new ScreenDataConversation(conversation,
                        npc.GetFlagField(), this, false));
                }
                return;
            }
            //if defeated use defeat interaction
            if (npc.GetRPG().GetStat(ST.HEALTH) <= 0)
            {
                string conversation = ((NPC_RPG)npc.GetRPG()).GetConversationFile((int)conversationType.DEFEAT);
                if (conversation != null)
                {
                    view.SetScreen(new ScreenDataConversation(conversation,
                        npc.GetFlagField(), this, false));
                }


                return;
            }
            //if peacebonded or not hostile to player and has a chat then use that interaction
            if (npc.IsPeaceful()|| npc.GetFaction().getRelationship(player.GetFaction()) > 50)
            {
                string conversation = ((NPC_RPG)npc.GetRPG()).GetConversationFile((int)conversationType.TALK);
                if (conversation != null)
                {
                    view.SetScreen(new ScreenDataConversation(conversation,
                        npc.GetFlagField(), this, false));
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (actorAnimation != null)
        {
            if (actorAnimation.Update(transform))
            {
                actorAnimation = null;
                ForceUpdate();
            }
        }
    }

    public void SetActorAnimation(ActorAnimation animation)
    {
        this.actorAnimation = animation;
    }

    public ActorController GetController()
    {
        return controller;
    }

    public void LogicUpdate(ViewInterface view)
    {
        if (this.actor.GetPosition().x >= 0)
        {
            controller.UpdateLogic(view);
        }
        if (this.actor.GetVolatilityHandler() != null)
        {
            if (this.actor.GetVolatilityHandler().Update(1))
            {
                this.Remove();
            }
        }
        
    }

    public Vector2Int getPosition()
    {
        return actor.GetPosition();
    }

    public void SetVisible(bool visible)
    {
        meshRenderer.enabled = visible;
    }

    internal void ApplyMissEffect(Effect effect, bool v, ZoneActor zoneActor, AmmoType ammoType, DiceRoller diceRoller, ViewInterface view)
    {
        if (effect.IsOnMiss())
        {
            ApplyEffect(effect, false, zoneActor, ammoType, diceRoller, view);
        }
    }
}
