﻿using UnityEngine;
using System.Collections;

public class MoveAnimation : ActorAnimation
{
    float duration, speed;
    Vector2 startPosition, endPosition;
    Vector2 offsetPosition;
    Vector2 currentPosition;
    ZoneActor actor;
    public MoveAnimation(float duration, Vector2 start, Vector2 end, ZoneActor actor)
    {
        this.actor = actor;
        this.startPosition = start;
        this.endPosition = end;
        this.duration = duration;
        float d = Vector2.Distance(startPosition, endPosition);
        offsetPosition =  endPosition - startPosition;
        offsetPosition.Normalize();
        currentPosition = new Vector2(startPosition.x, startPosition.y);
        speed = d / duration;
    }

    public override bool Update(Transform transform)
    {
        duration -= Time.deltaTime;
        Vector2 v = speed * Time.deltaTime * offsetPosition;
        currentPosition = currentPosition + v;
        transform.position = currentPosition;
        if (duration <= 0)
        {
            UpdateSprite(GetActorRestState());
        }

        return duration <= 0;
    }
    private void UpdateSprite(int index)
    {
        MeshFilter meshFilter = actor.GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = actor.GetComponent<MeshRenderer>();
        ZoneActorRenderTool.generate(actor.actor.getSprite(), meshRenderer, meshFilter, index);
    }
    protected int GetActorRestState()
    {
        if (this.actor.actor.GetRPG().GetStat(ST.HEALTH) <= 0)
        {
            return 2;
        }
        if (this.actor.actor.GetRPG().GetStat(ST.RESOLVE) <= 0)
        {
            return 3;
        }
        return 0;
    }
}
