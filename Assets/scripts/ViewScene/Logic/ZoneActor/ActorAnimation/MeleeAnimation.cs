using UnityEngine;
using System.Collections;
using System;

public class MeleeAnimation: ActorAnimation {

    float duration;
    Vector2 direction;
    Vector2 currentPosition;
    ZoneActor actor;
    public MeleeAnimation(ZoneActor actor, Vector2 direction, Vector2 start)
    {
        this.actor=actor; ;
        this.duration = 0.2F;
        this.currentPosition = new Vector2(start.x,start.y);
        this.direction = direction;
        UpdateSprite(1);
    }

    private void UpdateSprite(int index)
    {
        MeshFilter meshFilter = actor.GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = actor.GetComponent<MeshRenderer>();
        ZoneActorRenderTool.generate(actor.actor.getSprite(), meshRenderer, meshFilter, index);
    }

    public override bool Update(Transform transform)
    {
        duration -= Time.deltaTime;
        float off = 0.1F-(Mathf.Abs(duration - 0.1F));
        transform.position = currentPosition + (this.direction * off);
        if (duration <= 0)
        {
            UpdateSprite(GetActorRestState());
            return true;
        }
        return false;
    }

    protected int GetActorRestState()
    {
        if (this.actor.actor.GetRPG().GetStat(ST.HEALTH) <= 0)
        {
            return 2;
        }
        if (this.actor.actor.GetRPG().GetStat(ST.RESOLVE) <= 0)
        {
            return 3;
        }
        return 0;
    }
}
