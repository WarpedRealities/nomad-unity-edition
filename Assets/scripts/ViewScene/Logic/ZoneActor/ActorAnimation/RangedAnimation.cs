using UnityEngine;
using System.Collections;
using System;

public class RangedAnimation: ActorAnimation {

    float duration;
    ZoneActor actor;
    public RangedAnimation(ZoneActor actor)
    {
        this.actor=actor; ;
        this.duration = 0.2F;
        UpdateSprite(1);
    }

    private void UpdateSprite(int index)
    {
        MeshFilter meshFilter = actor.GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = actor.GetComponent<MeshRenderer>();
        ZoneActorRenderTool.generate(actor.actor.getSprite(), meshRenderer, meshFilter, index);
    }

    public override bool Update(Transform transform)
    {
        duration -= Time.deltaTime;
        if (duration <= 0)
        {
            UpdateSprite(GetActorRestState());
            return true;
        }
        return false;
    }

    protected int GetActorRestState()
    {
        if (this.actor.actor.GetRPG().GetStat(ST.HEALTH) <= 0)
        {
            return 2;
        }
        if (this.actor.actor.GetRPG().GetStat(ST.RESOLVE) <= 0)
        {
            return 3;
        }
        return 0;
    }
}
