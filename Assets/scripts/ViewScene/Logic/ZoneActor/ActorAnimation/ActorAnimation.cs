﻿using UnityEngine;
using System.Collections;

public abstract class ActorAnimation
{

    public abstract bool Update(Transform transform);
}
