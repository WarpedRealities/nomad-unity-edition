﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorInstruction
{
    public Vector2 position;

    public IndicatorInstruction(Vector2 position)
    {
        this.position = position;
    }
}

public class NumberInstruction : IndicatorInstruction
{
    public int number;
    public int colourIndex;
    public NumberInstruction(Vector2 position, int number, int index ) : base(position)
    {
        this.number = number;
        this.colourIndex = index;
    }
}

public class IndicatorController : MonoBehaviour
{
    public GameObject hitPrefab, seducePrefab, scanPrefab, numberPrefab, pingPrefab;
    private List<Animator2D> hitIndicators, seduceIndicators, scanIndicators, pingIndicators;
    private List<CombatNumber> combatNumbers;

    private Queue<IndicatorInstruction> hitQueue = new Queue<IndicatorInstruction>(),
        seduceQueue = new Queue<IndicatorInstruction>(),
        scanQueue = new Queue<IndicatorInstruction>(),
        pingQueue = new Queue<IndicatorInstruction>();
    private Queue<NumberInstruction> numberQueue = new Queue<NumberInstruction>();
    public List<GameObject> rangeIndicators;
    public GameObject targetIndicator;
    int hitIndex, seduceIndex, scanIndex, rangeIndex, numberIndex, pingIndex;
    private AnimatorInstructionSet instructionSet, quickSet, slowSet;
    // Start is called before the first frame update
    void Start()
    {
        hitIndicators = new List<Animator2D>();
        seduceIndicators = new List<Animator2D>();
        scanIndicators = new List<Animator2D>();
        combatNumbers = new List<CombatNumber>();
        pingIndicators = new List<Animator2D> { };
        BuildIndicators();
        hitIndex = 0;
        seduceIndex = 0;
        scanIndex = 0;
        pingIndex = 0;
        rangeIndex = 0;
        numberIndex = 0;
        instructionSet = new AnimatorInstructionSet(0, 5, 0.05F, false);
        quickSet = new AnimatorInstructionSet(0, 5, 0.025F, false);
        slowSet = new AnimatorInstructionSet(0, 9, 0.1F, false);
    }

    private void BuildIndicators()
    {
        for (int i = 0; i < 16; i++)
        {
            Animator2D hitAnimation = Instantiate(hitPrefab).GetComponent<Animator2D>();
            hitAnimation.transform.position = new Vector3(-9, -9);
            Animator2D seduceAnimation = Instantiate(seducePrefab).GetComponent<Animator2D>();
            seduceAnimation.transform.position = new Vector3(-9, -9);
            Animator2D scanAnimation = Instantiate(scanPrefab).GetComponent<Animator2D>();
            scanAnimation.transform.position = new Vector3(-9, -9);
            CombatNumber combatNumber = Instantiate(numberPrefab).GetComponent<CombatNumber>();
            Animator2D pingAnimation = Instantiate(pingPrefab).GetComponent<Animator2D>();
            pingAnimation.transform.position = new Vector3(-9, -9);
            hitIndicators.Add(hitAnimation);
            seduceIndicators.Add(seduceAnimation);
            scanIndicators.Add(scanAnimation);
            combatNumbers.Add(combatNumber);
            pingIndicators.Add(pingAnimation);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (hitQueue.Count>0)
        {
            TryImpact();
        }
        if (seduceQueue.Count > 0)
        {
            TrySeduce();
        }
        if (numberQueue.Count > 0)
        {
            TryNumber();
        }
        if (scanQueue.Count > 0)
        {
            TryScan();
        }
        if (pingQueue.Count > 0)
        {
            TryPing();
        }
    }

    private void TryPing()
    {
        if (this.pingIndicators[pingIndex].stop)
        {
            IndicatorInstruction instruction = pingQueue.Dequeue();
            pingIndicators[pingIndex].transform.position = new Vector3(instruction.position.x, instruction.position.y + 0.5F);
            pingIndicators[scanIndex].BroadcastMessage("setAnimationI", slowSet);
            pingIndex++;
            if (pingIndex >= pingIndicators.Count)
            {
                pingIndex = 0;
            }
        }
    }

    private void TryScan()
    {
        if (this.scanIndicators[scanIndex].stop)
        {
            IndicatorInstruction instruction = scanQueue.Dequeue();
            scanIndicators[scanIndex].transform.position = new Vector3(instruction.position.x, instruction.position.y + 0.5F);
            scanIndicators[scanIndex].BroadcastMessage("setAnimationI", quickSet);
            scanIndex++;
            if (scanIndex >= scanIndicators.Count) { 
                scanIndex = 0;
            }
        }
    }

    private void TryNumber()
    {
        if (!this.combatNumbers[numberIndex].isAlive())
        {
            NumberInstruction numberInstruction = numberQueue.Dequeue();
            combatNumbers[numberIndex].Initialize(numberInstruction.position,
                numberInstruction.number,
                numberInstruction.colourIndex);

            numberIndex++;
            if (numberIndex >= combatNumbers.Count)
            {
                numberIndex = 0;
            }
        }
    }

    private void TrySeduce()
    {
        if (this.seduceIndicators[seduceIndex].stop)
        {
            IndicatorInstruction instruction = seduceQueue.Dequeue();
            seduceIndicators[seduceIndex].transform.position = new Vector3(instruction.position.x, instruction.position.y + 0.5F);
            seduceIndicators[seduceIndex].BroadcastMessage("setAnimationI", instructionSet);
            seduceIndex++;
            if (seduceIndex >= seduceIndicators.Count)
            {
                seduceIndex = 0;
            }
        }
    }

    private void TryImpact()
    {
        if (this.hitIndicators[hitIndex].stop)
        {
            IndicatorInstruction instruction = hitQueue.Dequeue();
            hitIndicators[hitIndex].transform.position = new Vector3(instruction.position.x, instruction.position.y);
            hitIndicators[hitIndex].BroadcastMessage("setAnimationI", instructionSet);
            hitIndex++;
            if (hitIndex >= hitIndicators.Count)
            {
                hitIndex = 0;
            }
        }
    }

    public void CreateImpact(Vector2Int position, int type)
    {
       switch (type)
        {
            case 0:
                CreateDamageImpact(position);
                break;
            case 1:
                CreateSeduceImpact(position);
                break;
            case 3:
                CreateScanMarker(position);
                break;
            case 4:
                CreatePingMarker(position);
                break;
        }
    }

    private void CreatePingMarker(Vector2Int position)
    {
        pingQueue.Enqueue(new IndicatorInstruction(position));
    }

    private void CreateScanMarker(Vector2Int position)
    {
        scanQueue.Enqueue(new IndicatorInstruction(position));
    }

    public void CreateImpact(Vector2 position, int type)
    {
        switch (type)
        {
            case 3:
                CreateScanMarker(new Vector2Int((int)position.x, (int)position.y));
                break;
            case 4:
                CreatePingMarker(new Vector2Int((int)position.x, (int)position.y));
                break;
        }
    }

    public void CreateNumber(Vector2Int position, int number, int index)
    {
        numberQueue.Enqueue(new NumberInstruction(new Vector2(position.x+0.5F, position.y+0.5F), number, index));
    }

    private void CreateDamageImpact(Vector2Int position)
    {
        hitQueue.Enqueue(new IndicatorInstruction(position));
    }

    private void CreateSeduceImpact(Vector2Int position)
    {
        seduceQueue.Enqueue(new IndicatorInstruction(position));

    }

    internal void CreateRanged(Vector2Int origin, Vector2Int end, int type)
    {
        rangeIndicators[rangeIndex].BroadcastMessage("Initialize", new RangeParams(origin,end,0.25F,type));

        rangeIndex++;
        if (rangeIndex == 8)
        {
            rangeIndex = 0;
        }
    }
}
