﻿using UnityEngine;
using System.Collections;

public interface ViewInterface
{

    void CalculateVision();
    void ZoneTransition(string destination, int identifier);
    void ZoneTransition(int side, string destination);
    LuaSensor GetSensor();
    void DrawText(string str);
    void DrawImpact(Vector2Int position, int type);
    void DrawImpact(Vector2 position, int type);
    void DrawRanged(Vector2Int origin, Vector2Int target, int type);

    void DrawNumber(Vector2Int origin, int number, int type);
    void SetScreen(ScreenID screen);
    void SetScreen(ScreenData screenData);
    void Redraw();
    void RemoveWidget(Widget widget);
    Vector2Int GetWidgetPosition(Widget widget);
    ZoneActor GetPlayerInZone();
    void UpdateCamera();
    void AddNPC(Actor actor);
    RegenerationTicker GetRegenerationTicker();
    void ReplaceWidget(Widget widgetRemove, Widget widgetReplace);
}
