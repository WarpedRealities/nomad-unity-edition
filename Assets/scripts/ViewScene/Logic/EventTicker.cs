﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public class EventTicker
{
    public int clock = 0;
    private readonly int INTERVAL = 200;
    public int state = -1;
    LuaSolarSensor luaSolarSensor;
    FlagDataAdv luaFlagData;
    public EventTicker()
    {
        if (GlobalGameState.GetInstance().getCurrentEntity() is Spaceship)
        {
            Spaceship ship = (Spaceship)GlobalGameState.GetInstance().getCurrentEntity();
            state = 0;
            luaSolarSensor=new LuaSolarSensor(GlobalGameState.GetInstance().getCurrentSystem());
            luaFlagData = new FlagDataAdv(GlobalGameState.GetInstance().GetPlayer().GetFlagField(), GlobalGameState.GetInstance().GetUniverse().getFactionLibrary());
        }
    }
    
    private bool CanRun()
    {
        switch (state)
        {
            case 0:
                return ((Spaceship)GlobalGameState.GetInstance().getCurrentEntity()).GetState() == ShipState.DISTRESS;

        }
        return false;
    }

    internal void Update()
    {
        if (state != -1)
        {
            if (CanRun())
            {
                clock++;
                if (clock >= INTERVAL)
                {
                    clock = 0;
                    RunEvents();
                }
            }
        }
    }

    private void RunEvents()
    {
        switch (state)
        {
            case 0:
                RunDistress();
            break;
        }

    }

    private void RunDistress()
    {

        StarSystem system = GlobalGameState.GetInstance().getCurrentSystem();
        DistressData distressData = system.GetSystemContents().GetDistressData();
        for (int i = 0; i < distressData.GetDistressCalls().Count; i++)
        {
            int diceroll = DiceRoller.GetInstance().RollDice(100);
            if (distressData.GetDistressCalls()[i].GetChance()>=diceroll &&
                distressData.GetDistressCalls()[i].RunCondition(luaSolarSensor, luaFlagData))
            {
                Spaceship ship = (Spaceship)GlobalGameState.GetInstance().getCurrentEntity();
                ship.SetState(ShipState.SPACE);
                distressData.GetDistressCalls()[i].RunEvent(luaSolarSensor, luaFlagData);
                return;
            }
        }

    }
}

