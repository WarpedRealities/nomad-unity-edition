﻿

using System;
using UnityEngine;

public class PlayerAttackControl
{
    private Player player;
    private ZoneActor zoneActor;
    private Zone zone;
    private ControlFacade facade;
    private PlayerMoveControl moveControl;
    private float clock;
    private float INTERVALTIME;
    private const int ALIASNORTH = 1;
    private const int ALIASEAST = 2;
    private const int ALIASSOUTH = 4;
    private const int ALIASWEST = 8;
    private const int ALIASNORTHEAST = 3;
    private const int ALIASSOUTHEAST = 6;
    private const int ALIASSOUTHWEST = 12;
    private const int ALIASNORTHWEST = 9;
    private const int ALIASWAIT = -2;

    private int direction;

    public PlayerAttackControl(Player player, ZoneActor zoneActor, Zone zone, ControlFacade controlFacade, PlayerMoveControl moveControl)
    {
        INTERVALTIME = Config.GetInstance().GetInputWindow();
        this.facade = controlFacade;
        this.player = player;
        this.zoneActor = zoneActor;
        this.zone = zone;
        this.moveControl = moveControl;
    }

    public bool Update(bool shift, ViewInterface view)
    {
        if (clock > 0)
        {
            clock += Time.deltaTime;
            if (clock >= INTERVALTIME)
            {
                //finalize
                int d = HandleInputs(shift);
                clock = 0;
                if (d != 0)
                {
                    int dir = ConvertAndToDirection(d >= 0 ? d : direction);
                    Debug.Log("direction" + dir);
                    Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, dir);
                    return AttackTile(p, view, dir);
                }

            }
        }
        else
        {
            //initialize
            direction = HandleInputs(shift);
            if (direction != -1)
            {

                clock += Time.deltaTime;
            }
        }
        return false;
    }
    public int HandleInputs(bool shift)
    {
        int d = 0;
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.A))
        {
            d = d | ALIASWEST;
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Keypad8) || Input.GetKey(KeyCode.W))
        {
            d = d | ALIASNORTH;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.S))
        {
            d = d | ALIASSOUTH;
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Keypad6) || Input.GetKey(KeyCode.D))
        {
            d = d | ALIASEAST;
        }
        if (Input.GetKey(KeyCode.Keypad7) || (shift && Input.GetKey(KeyCode.Home)))
        {
            d = d | ALIASNORTHWEST;
        }
        if (Input.GetKey(KeyCode.Keypad1) || (shift && Input.GetKey(KeyCode.End)))
        {
            d = d | ALIASSOUTHWEST;
        }
        if (Input.GetKey(KeyCode.Keypad3) || (shift && Input.GetKey(KeyCode.PageDown)))
        {
            d = d | ALIASSOUTHEAST;
        }
        if (Input.GetKey(KeyCode.Keypad9) || (shift && Input.GetKey(KeyCode.PageUp)))
        {
            d = d | ALIASNORTHEAST;
        }
        if (Input.GetKey(KeyCode.Keypad5) || Input.GetKey(KeyCode.Space))
        {
            d = ALIASWAIT;
        }
        return d;
    }
    private int ConvertAndToDirection(int and)
    {
        switch (and)
        {
            case ALIASNORTH:
                return 0;
            case ALIASEAST:
                return 2;
            case ALIASSOUTH:
                return 4;
            case ALIASWEST:
                return 6;
            case ALIASNORTHEAST:
                return 1;
            case ALIASNORTHWEST:
                return 7;
            case ALIASSOUTHEAST:
                return 3;
            case ALIASSOUTHWEST:
                return 5;
            case ALIASWAIT:
                return ALIASWAIT;
        }
        return -1;
    }
    private bool AttackTile(Vector2Int position, ViewInterface view, int direction)
    {

        if (position.x >= 0 && position.y >= 0 &&
                 position.x < zone.GetContents().GetZoneParameters().GetWidth() &&
                 position.y < zone.GetContents().GetZoneParameters().GetHeight())
        {
            Tile t = zone.GetContents().getTiles()[position.x][position.y];
            if (t.GetActorInTile() != null && t.GetActorInTile().GetAlive())
            {
                return facade.LaunchAttack(t.GetActorInTile(),view);
            }
            if (t.GetWidget() != null) {
                if (t.GetWidget() is Widget_Breakable)
                {
                    return facade.LaunchAttack(position, view);
                }
                if (t.GetWidget() is WidgetSlot)
                {
                    WidgetSlot ws = (WidgetSlot)t.GetWidget();
                    Debug.Log("check");
                    if (ws.GetContainedWidget()!=null)
                    {
                        Debug.Log("???");
                        return facade.LaunchAttack(position,view);
                    }
                }
            }
        }
        return moveControl.AttemptMove(direction,view);
    }
}