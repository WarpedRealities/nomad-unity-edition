﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Xml;
using UnityEngine;

public class InventoryActionHandler 
{
    private Player player;
    private ZoneActor playerInZone;
    private Zone zone;

    public InventoryActionHandler(Player player, ZoneActor zoneActor, Zone zone)
    {
        this.playerInZone = zoneActor;
        this.player = player;
        this.zone = zone;
    }

    public bool HandleInventory(ActionInventory action, ViewInterface view)
    {
        switch (action.GetInventoryActionType())
        {
            case InventoryAction.DROP:
                return DropHandler(action, view);
            case InventoryAction.USE:
                return UseHandler(action, view);
            case InventoryAction.EQUIP:
                return EquipHandler(action, view);
            case InventoryAction.UNEQUIP:
                return UnequipHandler(action, view);
        }

        return false;
    }

    private bool UnequipHandler(ActionInventory action, ViewInterface view)
    {
        player.GetInventory().UnEquipItem(action.GetItem());
        player.GetInventory().HandleUnequipMods(action.GetItem(), (PlayerRPG)player.GetRPG());
        /*
        ItemEquippable itemEquippable = (ItemEquippable)action.GetItem().GetDef();
        if (itemEquippable.GetDefenceModifiers() != null)
        {
            ((PlayerRPG)player.GetRPG()).RemoveDefenceMod(itemEquippable.GetDefenceModifiers());
        }
        if (itemEquippable.GetSkillModifiers() != null)
        {
            ((PlayerRPG)player.GetRPG()).RemoveSkillMod(itemEquippable.GetSkillModifiers());
        }
        if (itemEquippable.GetMoves() != null)
        {
            ((PlayerRPG)player.GetRPG()).UpdateMoves();

        }
        if (itemEquippable.GetHazardProtections() != null)
        {
            ((PlayerRPG)player.GetRPG()).UpdateHazardProtection();
        }
        (this.player).GetQuickSlotHandler().UpdateQuickSlots(player.GetInventory(),
        ((PlayerRPG)this.player.GetRPG()).GetMoves());
        */
        return true;
    }

    private bool EquipHandler(ActionInventory action, ViewInterface view)
    {
        player.GetInventory().EquipItem(action.GetItem());
        player.GetInventory().HandleEquipMods(action.GetItem(), (PlayerRPG)player.GetRPG());
        /*
        ItemEquippable itemEquippable = (ItemEquippable)action.GetItem().GetDef();
        if (itemEquippable.GetDefenceModifiers() != null)
        {
            ((PlayerRPG)player.GetRPG()).ApplyDefenceMod(itemEquippable.GetDefenceModifiers());
        }
        if (itemEquippable.GetSkillModifiers() != null)
        {
            ((PlayerRPG)player.GetRPG()).ApplySkillMod(itemEquippable.GetSkillModifiers());

        }
        if (itemEquippable.GetMoves() != null)
        {
            ((PlayerRPG)player.GetRPG()).UpdateMoves();
        }

        if (itemEquippable.GetHazardProtections() != null)
        {
            ((PlayerRPG)player.GetRPG()).UpdateHazardProtection();
        }
            (this.player).GetQuickSlotHandler().UpdateQuickSlots(player.GetInventory(),
            ((PlayerRPG)this.player.GetRPG()).GetMoves());
        */
        return true;
    }

    private bool UseHandler(ActionInventory action, ViewInterface view)
    {
        DiceRoller roller = DiceRoller.GetInstance();
        if (action.GetItem().GetDef().GetItemType().Equals(ItemType.CONSUMABLE))
        {
            ItemConsumable itemConsumable = (ItemConsumable)action.GetItem().GetDef();
            for (int i = 0; i < itemConsumable.GetEffects().Count; i++)
            {
              
                EffectHandler.ApplyEffect(itemConsumable.GetEffects()[i], false, playerInZone, null, roller,view);
            }
            player.GetInventory().RemoveItem(action.GetItem());
        }
        if (action.GetItem().GetDef().GetItemType().Equals(ItemType.EXPOSITION))
        {
            ItemExpositionInstance itemExpositionInstance = (ItemExpositionInstance)action.GetItem();
            XmlDocument document = FileTools.GetXmlDocument("exposition/" + itemExpositionInstance.GetExpositionFile());
            XmlElement element = (XmlElement)document.FirstChild.NextSibling;
            view.DrawText(element.InnerText);
        }
        if (action.GetItem().GetDef().GetItemType().Equals(ItemType.BLUEPRINT))
        {
            ItemBlueprintInstance itemBLueprintInstance = (ItemBlueprintInstance)action.GetItem();
            CraftingLibrary craftingLibrary = GlobalGameState.GetInstance().GetUniverse().GetCraftingLibrary();
            Recipe recipe = craftingLibrary.GetRecipe(itemBLueprintInstance.GetRecipeCode());
            if (!recipe.isUnlocked())
            {
                recipe.SetUnlocked();
                view.DrawText("You've unlocked recipe " + itemBLueprintInstance.GetRecipeName());
                player.GetInventory().RemoveItem(action.GetItem());
            }
        }
        (this.player).GetQuickSlotHandler().UpdateQuickSlots(player.GetInventory(),
        ((PlayerRPG)this.player.GetRPG()).GetMoves());
        view.CalculateVision();
        return true;
    }

    private bool DropHandler(ActionInventory action, ViewInterface view)
    {
        if (DropOnTile(action, player.GetPosition(), view))
        {
            return true;
        }
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p0 = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, i);
            if (p0.x>=0 && p0.x<zone.GetContents().GetZoneParameters().GetWidth() && 
                p0.y>=0 && p0.y < zone.GetContents().GetZoneParameters().GetHeight())
            {
                if (DropOnTile(action, p0, view))
                {
                    (this.player).GetQuickSlotHandler().UpdateQuickSlots(player.GetInventory(),
                    ((PlayerRPG)this.player.GetRPG()).GetMoves());
                    return true;
                }
            }
        }


        return false;
    }

    private bool DropOnTile(ActionInventory action, Vector2Int p, ViewInterface view)
    {
        Tile t = zone.GetContents().getTiles()[p.x][p.y];
        //is the tile i'm standing on empty of widgets?
        if (t.GetWidget() == null)
        {
            //if so, simply place the item in a widget pile where i'm standing
            t.SetWidget(new WidgetItemPile(player.GetInventory().RemoveItem(action.GetItem())));
           
            view.CalculateVision();
            return true;
        }
        //is there an existing widget item pile where i'm standing?
        if (t.GetWidget() is WidgetItemPile)
        {
            //if so add to that pile
            WidgetItemPile widgetItemPile = (WidgetItemPile)t.GetWidget();
            widgetItemPile.AddItem(player.GetInventory().RemoveItem(action.GetItem()));            
            view.CalculateVision();
            return true;
        }
        return false;
    }

    public bool HandleContainer(ActionContainer action, ViewInterface view)
    {
        ContainerData container = action.GetContainer();
        Player player = GlobalGameState.GetInstance().GetPlayer();
        Inventory inventory = player.GetInventory();

        if (action.IsPutting())
        {
            Item item = player.GetInventory().RemoveItem(action.GetItem());
            container.AddItem(item);
        }
        else
        {
            player.GetInventory().AddItem(action.GetItem());
            container.RemoveItem(action.GetItem());
        }
        (this.player).GetQuickSlotHandler().UpdateQuickSlots(player.GetInventory(),
        ((PlayerRPG)this.player.GetRPG()).GetMoves());
        return false;
    }

    internal bool HandleReload(ActionReload action, ViewInterface view)
    {
        if (action.GetRefill().GetDef().GetItemType() == ItemType.EQUIP)
        {
            return HandleEquipReload(action, view);
        }
        if (action.GetRefill().GetDef().GetItemType() == ItemType.AMMO)
        {
            return HandleAmmoReload(action, view);
        }


        throw new NotImplementedException();
    }

    private bool HandleAmmoReload(ActionReload action, ViewInterface view)
    {
        AmmoInstance ammoInstance = (AmmoInstance)action.GetRefill();
        if (action.GetAmmo() == null)
        {
            //unload
            return HandleAmmoUnload(ammoInstance, view);
        }
        else
        {
            //load
            return HandleAmmoLoad(ammoInstance, (AmmoBase)action.GetAmmo(), view);
        }
        throw new NotImplementedException();
    }

    private bool HandleAmmoLoad(AmmoInstance ammoInstance, AmmoBase item, ViewInterface view)
    {
        ResourceStoreInstance resourceStoreInstance = ammoInstance.GetResource();
        resourceStoreInstance.SetAmount(resourceStoreInstance.GetAmount() + 1);
        resourceStoreInstance.SetAmmoType(item.GetAmmoType());
        resourceStoreInstance.SetAmmoCode(item.GetDef().GetCodeName());

        if (item.GetItemClass() == ItemClass.AMMOSTACK)
        {
            ItemAmmoStack itemAmmoStack = (ItemAmmoStack)item;
            itemAmmoStack.SetCount(itemAmmoStack.GetCount() - 1);
            if (itemAmmoStack.GetCount() < 1)
            {
                this.player.GetInventory().RemoveItem(item);
            }
        }
        else
        {
            this.player.GetInventory().RemoveItem(item);
        }
        this.player.GetInventory().CalculateWeight();
        return true;
    }

    private bool HandleAmmoUnload(AmmoInstance ammoInstance, ViewInterface view)
    {
        Inventory inventory = player.GetInventory();
        ResourceStoreInstance resourceStoreInstance = ammoInstance.GetResource();
        ItemDef itemDef = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(resourceStoreInstance.GetAmmoCode());

        for (int i = 0; i < resourceStoreInstance.GetAmount(); i++)
        {
            inventory.AddItem(new AmmoInstance((ItemAmmo)itemDef, resourceStoreInstance.GetAmmoType()));
        }
        this.player.GetInventory().CalculateWeight();
        resourceStoreInstance.SetAmount(0);

        return true;
    }

    private bool HandleEquipReload(ActionReload action, ViewInterface view)
    {
        EquipInstance equipInstance = (EquipInstance)action.GetRefill();

        if (action.GetAmmo() == null)
        {
            return HandleUnload(equipInstance, view);
        }

        if (equipInstance.GetResourceStore().GetDef().GetCapacity() == 0)
        {
            //we're reloading using a magazine
            //need to decompose an item stack
            if (action.GetAmmo() is ItemAmmoStack)
            {
                ItemAmmoStack itemAmmoStack = (ItemAmmoStack)action.GetAmmo();
                AmmoInstance item = (AmmoInstance) itemAmmoStack.TakeItem();
                itemAmmoStack.DecrementCount();
                if (itemAmmoStack.GetCount() == 0)
                {
                    this.player.GetInventory().RemoveItem(itemAmmoStack);
                }
                return HandleReloadMagazine(equipInstance.GetResourceStore(), item, view);
            } else {
                return HandleReloadMagazine(equipInstance.GetResourceStore(), (AmmoInstance)action.GetAmmo(), view);
            }
        }else
        {
            //we're reloading using a round
            return HandleReloadRound(equipInstance.GetResourceStore(), (AmmoBase)action.GetAmmo(), view);
        }

        throw new NotImplementedException();
    }

    private bool HandleUnload(EquipInstance equipInstance, ViewInterface view)
    {
        Inventory inventory = player.GetInventory();
        ResourceStoreInstance resourceStoreInstance = equipInstance.GetResourceStore();
        ItemDef itemDef = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(resourceStoreInstance.GetAmmoCode());
        if (equipInstance.GetResourceStore().GetDef().GetCapacity() == 0)
        {
            if (equipInstance.GetResourceStore().GetCapacity() > 0)
            {
                //unload a clip
                AmmoInstance ammoInstance = new AmmoInstance((ItemAmmo)itemDef, resourceStoreInstance.GetAmmoType());
                ammoInstance.GetResource().SetAmount(resourceStoreInstance.GetAmount());
                if (!ammoInstance.isExpendable())
                {
                    inventory.AddItem(ammoInstance);
                }
                this.player.GetInventory().CalculateWeight();
                resourceStoreInstance.SetAmount(0);
                resourceStoreInstance.SetCapacity(0);
            }
        }
        else
        {
            for (int i = 0; i < resourceStoreInstance.GetAmount(); i++) {
                inventory.AddItem(new AmmoInstance((ItemAmmo)itemDef, resourceStoreInstance.GetAmmoType()));
            }
            this.player.GetInventory().CalculateWeight();
            resourceStoreInstance.SetAmount(0);
        }
        return true;
    }


    private bool HandleReloadMagazine(ResourceStoreInstance resourceStoreInstance, AmmoInstance ammo, ViewInterface view)
    {
        resourceStoreInstance.SetAmount(ammo.GetResource().GetAmount());
        resourceStoreInstance.SetCapacity(ammo.GetResource().GetCapacity()); 
        resourceStoreInstance.SetAmmoType(ammo.GetAmmoType());
        resourceStoreInstance.SetAmmoCode(ammo.GetDef().GetCodeName());

        this.player.GetInventory().RemoveItem(ammo);

        this.player.GetInventory().CalculateWeight();

        return true;
    }


    private bool HandleReloadRound(ResourceStoreInstance resourceStoreInstance, AmmoBase item, ViewInterface view)
    {
        resourceStoreInstance.SetAmount(resourceStoreInstance.GetAmount() + 1);
        resourceStoreInstance.SetAmmoType(item.GetAmmoType());
        resourceStoreInstance.SetAmmoCode(item.GetDef().GetCodeName());
        
        if (item.GetItemClass() == ItemClass.AMMOSTACK)
        {
            ItemAmmoStack itemAmmoStack = (ItemAmmoStack)item;
            itemAmmoStack.SetCount(itemAmmoStack.GetCount() - 1);
            if (itemAmmoStack.GetCount() < 1)
            {
                this.player.GetInventory().RemoveItem(item);
            }
        }
        else
        {
            this.player.GetInventory().RemoveItem(item);
        }
        this.player.GetInventory().CalculateWeight();
        return true;
    }
}
