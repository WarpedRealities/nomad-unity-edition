﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using static System.Collections.Specialized.BitVector32;

public class PlayerController : ActorController, ControlFacade
{
    private Zone zone;
    private Player player;
    private ZoneActor playerInZone;
    private readonly float BASE_REGEN=0.1F;
    private readonly float BASE_AP=30.0F;
    private PlayerInteractionControl playerInteraction;
    private PlayerMoveControl playerMoveControl;
    private PlayerLookControl playerLook;
    private PlayerAttackControl playerAttack;
    private PlayerTargetControl targetControl;
    private InventoryActionHandler inventoryActionHandler;
    private ControlMode controlMode;
    private bool doubleTapPrevention;
    private CanvasCamera canvasCamera;

    public PlayerController(Player player,ZoneActor zoneActor, CanvasCamera canvasCamera) : base(player, zoneActor)
    {
        this.canvasCamera = canvasCamera;
        this.controlMode = ControlMode.NORMAL;
        this.player = player;
        this.zone = GlobalGameState.GetInstance().getCurrentZone();

        this.playerLook = new PlayerLookControl(player, zoneActor, zone);
        this.playerMoveControl = new PlayerMoveControl(player, zoneActor, zone, this);
        this.playerInteraction = new PlayerInteractionControl(player, zoneActor, playerMoveControl);
        this.playerAttack = new PlayerAttackControl(player, zoneActor, zone, this, playerMoveControl);
        this.inventoryActionHandler = new InventoryActionHandler(player, zoneActor, zone);
        doubleTapPrevention = true;
    }

    public ControlMode GetControlMode()
    {
        return controlMode;
    }

    public void SetTargetControl(PlayerTargetControl targetControl)
    {
        this.targetControl = targetControl;
    }
  
    public void SetZoneActor(ZoneActor zoneActor)
    {
        playerInZone = zoneActor;
    }

    public float MoveCost()
    {
        return (1.0F/player.GetRPG().getSpeed());
    }

    public bool LaunchAttack(ZoneActor actor,ViewInterface view)
    {
        CombatMove move = ((PlayerRPG)this.actor.GetRPG()).GetCurrentMove();
        //check AP

        if ((move.GetAPCost() > this.actor.GetRPG().GetStat(ST.ACTION) && !move.IsBasic()))
        {
            view.DrawText("Not enough AP to use " + move.GetName());
            //cant perform non basic actions if you don't have the ap for it            
            return false;
        }
        if (!AmmoCheck(move))
        {
            view.DrawText("Not enough ammo to use " + move.GetName());
            //cant perform attacks if out of ammo
            return false;
        }
        if (!patternEligiblity.TargetCheck(move, actor.getPosition()))
        {
            Debug.Log("can't use this attack?");
            return false;
        }
        int delay = move.GetDelay() * move.GetAPCost() > this.actor.GetRPG().GetStat(ST.ACTION) ? 2 : 1;
        this.actor.GetRPG().ReduceStat(ST.ACTION, move.GetAPCost());
        queue.AddAction(new ActionDoMove(move, actor, move.GetAPCost()> this.actor.GetRPG().GetStat(ST.ACTION)? 2 : 1,GetAmmoType(move)));
        if (move.GetRepeat() > 0)
        {
            for (int i = 0; i < move.GetRepeat(); i++)
            {
                queue.AddAction(new ActionDoMove(move, actor, move.GetAPCost() > this.actor.GetRPG().GetStat(ST.ACTION) ? 2 : 1, GetAmmoType(move)));
            }
        }

        if (delay > 0)
        {
            queue.AddAction(new ActionDelay(delay));
        }

        return true;
    }

    private AmmoType GetAmmoType(CombatMove move)
    {
        if (move.GetAmmoCost() == 0)
        {
            return null;
        }
        if (move.GetAmmoPool() < 0)
        {
            return null;
        }
        if (!(player.GetInventory().GetSlot(move.GetAmmoPool()) is EquipInstance))
        {
            return null;
        }
        EquipInstance equipInstance = (EquipInstance)player.GetInventory().GetSlot(move.GetAmmoPool());
        if (equipInstance.GetResourceStore() == null)
        {
            return null;
        }
        return equipInstance.GetResourceStore().GetAmmoType();
    }

    private bool AmmoCheck(CombatMove move)
    {
        int ammocost = move.GetAmmoCost();
        if (ammocost > 0)
        {
            int ammoPool = move.GetAmmoPool();
            if (ammoPool >= 0)
            {
                if (player.GetInventory().GetSlot(ammoPool) is EquipInstance)
                {
                    EquipInstance equipInstance = (EquipInstance)player.GetInventory().GetSlot(ammoPool);
                    ResourceStoreInstance resourceStoreInstance = equipInstance.GetResourceStore();
                    if (resourceStoreInstance!=null && resourceStoreInstance.GetAmount() < ammocost)
                    {
                        return false;
                    }
                }
            }
            if (ammoPool == -1)
            {
                if (player.GetRPG().GetStat(ST.SATIATION)< ammocost)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public bool LaunchAttack(Vector2Int position, ViewInterface view)
    {
        CombatMove move = ((PlayerRPG)this.actor.GetRPG()).GetCurrentMove();
        //check AP
        if ((move.GetAPCost() > this.actor.GetRPG().GetStat(ST.ACTION) && !move.IsBasic()))
        {
            view.DrawText("Not enough AP to use " + move.GetName());
            //cant perform non basic actions if you don't have the ap for it
            return false;
        }
        if (!AmmoCheck(move))
        {
            view.DrawText("Not enough ammo to use " + move.GetName());
            return false;
        }

        int delay = move.GetDelay() * move.GetAPCost() >= this.actor.GetRPG().GetStat(ST.ACTION) ? 2 : 1;
        this.actor.GetRPG().ReduceStat(ST.ACTION, move.GetAPCost());
        ActionDoMove doMove = new ActionDoMove(move, position, move.GetAPCost() > this.actor.GetRPG().GetStat(ST.ACTION) ? 2 : 1, GetAmmoType(move));
        queue.AddAction(doMove);


        if (delay > 0)
        {
            queue.AddAction(new ActionDelay(delay));
        }
        return true;
    }

    private bool HotKeys(bool shift, ViewInterface view)
    {
        if (Input.GetKey(KeyCode.I) && !doubleTapPrevention && !shift)
        {
            doubleTapPrevention = true;
            controlMode = controlMode==ControlMode.INTERACT ? ControlMode.NORMAL : ControlMode.INTERACT;
            view.Redraw();
           //view.DrawText("mode now " + controlMode);
        }

        if (Input.GetKey(KeyCode.C) && !doubleTapPrevention && shift)
        {
            SceneManager.LoadScene("CharacterScene");
        }
        if (Input.GetKey(KeyCode.Escape) && !doubleTapPrevention)
        {
            SceneManager.LoadScene("MenuScene");
        }

        if (Input.GetKey(KeyCode.Z) && !doubleTapPrevention && !shift)
        {
            doubleTapPrevention = true;
            canvasCamera.Zoom();
        }

        if (Input.GetKey(KeyCode.L) && !doubleTapPrevention && !shift)
        {
            doubleTapPrevention = true;
            controlMode = controlMode == ControlMode.LOOK ? ControlMode.NORMAL : ControlMode.LOOK;
            //view.DrawText("mode now " + controlMode);
            view.Redraw();
        }
        if (Input.GetKey(KeyCode.K) && !doubleTapPrevention && !shift)
        {
            doubleTapPrevention = true;
            controlMode = controlMode == ControlMode.ATTACK ? ControlMode.NORMAL : ControlMode.ATTACK;
            // view.DrawText("mode now " + controlMode);
            view.Redraw();
        }
        if (Input.GetKey(KeyCode.I) && !doubleTapPrevention && shift)
        {
            doubleTapPrevention = true;
            view.SetScreen(ScreenID.INVENTORY);
        }
        if (Input.GetKey(KeyCode.L) && !doubleTapPrevention && shift)
        {
            doubleTapPrevention = true;
            view.SetScreen(ScreenID.APPEARANCE);
        }
        if (Input.GetKey(KeyCode.C) && !doubleTapPrevention && !shift)
        {
            doubleTapPrevention = true;
            targetControl.FindNext();
        }
        if (Input.GetKey(KeyCode.M) && !doubleTapPrevention && !shift)
        {
            doubleTapPrevention = true;
            view.SetScreen(ScreenID.MOVESELECT);
        }
        if (Input.GetKey(KeyCode.R) && !doubleTapPrevention && !shift)
        {
            doubleTapPrevention = true;
            ReloadExecutionTool.TryReload(playerInZone);
        }
        if (Input.GetKey(KeyCode.F1))
        {
            view.SetScreen(ScreenID.HELP);
        }

        return false;
    }

    protected override bool HandleInventory(ActionInventory action, ViewInterface view)
    {
        return inventoryActionHandler.HandleInventory(action, view);
        
    }

    protected override bool HandleReload(ActionReload action, ViewInterface view)
    {
        return inventoryActionHandler.HandleReload(action, view);
    }

    protected override bool HandleContainer(ActionContainer action, ViewInterface view)
    {
        return inventoryActionHandler.HandleContainer(action, view);
    }

    public bool DoControls(ViewInterface view)
    {
        if (doubleTapPrevention && !Input.anyKey)
        {
             doubleTapPrevention = false;
        }

        bool shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

        HotKeys(shift, view);

        if (Input.GetKey(KeyCode.F) && !doubleTapPrevention)
        {
            doubleTapPrevention = true;
            if (targetControl.GetTarget() != null)
            {
                LaunchAttack(targetControl.GetTarget(),view);
                return true;
            }
        }

        if (Input.GetKey(KeyCode.R) && shift)
        {
            for (int i = 0; i < 10; i++)
            {
                playerInZone.GetController().addAction(new ActionRecover(10));
            }
            return true;
        }

        switch (controlMode)
        {
            case ControlMode.NORMAL:
                return playerMoveControl.Update(shift, view);
            case ControlMode.INTERACT:
                return playerInteraction.Update(shift,view);
            case ControlMode.LOOK:
                return playerLook.HandleLook(shift, view);
            case ControlMode.ATTACK:
                return playerAttack.Update(shift, view);
        }
        Debug.Log("player move queue" + playerInZone.GetController().GetQueueLength());
        return false;
    }


    protected override bool HandleRecover(ActionRecover actionRecover, ViewInterface view)
    {
        actor.GetRPG().Heal(actionRecover.GetBonus());
        HandleApRegen(4);
        return true;
    }

    public override bool UpdateLogic(ViewInterface view)
    {
        //handle the player AP regeneration here
        if (queue.ApRegen())
        {
            HandleApRegen(1);
        }
        return base.UpdateLogic(view);
    }

    private void HandleApRegen(float scale)
    {
        float ApFactor = ((float)actor.GetRPG().GetStatMax(ST.ACTION))/ BASE_AP;
        float regen = BASE_REGEN*ApFactor* scale;
        actor.GetRPG().IncreaseStat(ST.ACTION, regen);
    }

    public override void AttackOfOpportunity(ZoneActor player)
    {
       
    }

    internal void PurgeActions()
    {
        queue.Clear();
    }

    internal bool IsQueueEmpty()
    {
        return queue.IsEmpty();
    }
}
