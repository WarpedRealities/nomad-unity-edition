﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveControl
{
    private Player player;
    private ZoneActor playerInZone;
    private Zone zone;
    private ControlFacade facade;
    private float clock;
    private float INTERVALTIME;
    private const int ALIASNORTH = 1;
    private const int ALIASEAST = 2;
    private const int ALIASSOUTH = 4;
    private const int ALIASWEST = 8;
    private const int ALIASNORTHEAST = 3;
    private const int ALIASSOUTHEAST = 6;
    private const int ALIASSOUTHWEST = 12;
    private const int ALIASNORTHWEST = 9;
    private const int ALIASWAIT = -2;

    private int direction;
   
    public PlayerMoveControl(Player player, ZoneActor zoneActor, Zone zone, ControlFacade facade)
    {
        INTERVALTIME = Config.GetInstance().GetInputWindow();
        direction = -1;
        clock = 0;
        this.facade = facade;
        this.player = player;
        this.playerInZone = zoneActor;
        this.zone = zone;
    }


    private bool BlockingActorCheck(Vector2Int p, ViewInterface view)
    {
        ZoneActor actor = CollisionTools.GetActorInTile(p, zone);
        if (actor != null)
        {
            //are they hostile?

            if (actor.GetAlive() && actor.actor.GetFaction().getRelationship(player.GetFaction()) <= 50)
            {
                //if so and we have a melee attack, attack them
                if (player.GetRPG().GetCombatMove(0).GetPattern() == AttackPattern.MELEE)
                {

                    return facade.LaunchAttack(actor,view);
                }
            }
            else
            {
                //if not, then nudge them
                playerInZone.GetController().addAction(new ActionShove(5,p));
                return true;
            }

        }
        return false;
    }

    private void MoveAttackOfOpportunity()
    {
        ZoneActor threat = CollisionTools.GetTile(player.GetPosition()).GetThreateningActor();
        if (threat != null)
        {
            threat.GetController().AttackOfOpportunity(playerInZone);
        }
    }


    public bool AttemptMove(int direction, ViewInterface view)
    {
        if (direction == -1)
        {
            return false;
        }
        if (direction == ALIASWAIT)
        {
            playerInZone.GetController().addAction(new ActionDelay(5,false));
            return true;
        }
        if (player.GetRPG().GetStatusHandler().GetStatusBind() != null)
        {
            playerInZone.GetController().addAction(new ActionStruggle(10));
            return true;
        }
        Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x,
             player.GetPosition().y, direction);
        if (CollisionTools.CanEnter(p, zone) && (p.x!=player.GetPosition().x || p.y!=player.GetPosition().y))
        {
            MoveAttackOfOpportunity();
            ActionMove actionMove = new ActionMove(direction, facade.MoveCost());
            playerInZone.GetController().addAction(actionMove); 
            return true;
        }
        else
        {
            return BlockingActorCheck(p, view);
        }
    }

    public bool Update(bool shift, ViewInterface view)
    {
        if (clock > 0)
        {
            clock += Time.deltaTime;
            if (clock >= INTERVALTIME)
            {
                //finalize
                int d = HandleInputs(shift);
                clock = 0;
                return AttemptMove(ConvertAndToDirection(d>=0 ? d: direction), view);
            }
        } else
        {
            //initialize
            direction = HandleInputs(shift);
            if (direction != -1) { 

                clock += Time.deltaTime;
            }
        }
        return false;
    }

    private int ConvertAndToDirection(int and)
    {
        switch (and)
        {
            case ALIASNORTH:
                return 0;
            case ALIASEAST:
                return 2;
            case ALIASSOUTH:
                return 4;
            case ALIASWEST:
                return 6;
            case ALIASNORTHEAST:
                return 1;
            case ALIASNORTHWEST:
                return 7;
            case ALIASSOUTHEAST:
                return 3;
            case ALIASSOUTHWEST:
                return 5;
            case ALIASWAIT:
                return ALIASWAIT;
        }
        return -1;
    }

    public int HandleInputs(bool shift)
    {
        int d = 0;
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.A))
        {
            d = d | ALIASWEST;
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Keypad8) || Input.GetKey(KeyCode.W))
        {
            d = d | ALIASNORTH;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.S))
        {
            d = d | ALIASSOUTH;
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Keypad6) || Input.GetKey(KeyCode.D))
        {
            d = d | ALIASEAST;
        }
        if (Input.GetKey(KeyCode.Keypad7) || (shift && Input.GetKey(KeyCode.Home)))
        {
            d = d | ALIASNORTHWEST;
        }
        if (Input.GetKey(KeyCode.Keypad1) || (shift && Input.GetKey(KeyCode.End)))
        {
            d = d | ALIASSOUTHWEST;
        }
        if (Input.GetKey(KeyCode.Keypad3) || (shift && Input.GetKey(KeyCode.PageDown)))
        {
            d = d | ALIASSOUTHEAST;
        }
        if (Input.GetKey(KeyCode.Keypad9) || (shift && Input.GetKey(KeyCode.PageUp)))
        {
            d = d | ALIASNORTHEAST;
        }
        if (Input.GetKey(KeyCode.Keypad5) || Input.GetKey(KeyCode.Space))
        {
            d = ALIASWAIT;
        }
        return d;
    }

}
