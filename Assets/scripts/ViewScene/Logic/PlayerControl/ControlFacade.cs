﻿using UnityEngine;

public interface ControlFacade
{
    float MoveCost();
    bool LaunchAttack(ZoneActor actor, ViewInterface view);
    bool LaunchAttack(Vector2Int position, ViewInterface view);
}