﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargetControl 
{
    List<ZoneActor> zoneActors;
    ZoneActor player;
    int index;
    ZoneActor currentTarget;
    GameObject targetingIndicator;

    public PlayerTargetControl(ZoneActor player, List<ZoneActor> zoneActors)
    {
        this.player = player;
        this.zoneActors = zoneActors;
        index = -1;
    }

    public void UpdateTargeting()
    {

        if (currentTarget != null)
        {
            if (!currentTarget.IsVisible()|| !currentTarget.GetAlive())
            {
                FindNext();
            }
        }
        else
        {
            FindNext();
        }
        UpdateTargetIndicator();
    }

    public void UpdateTargetIndicator()
    {
        if (currentTarget != null)
        {
            Vector2 position = currentTarget.getPosition();
            targetingIndicator.SetActive(true);
            targetingIndicator.transform.position= new Vector3(position.x, position.y);
        }
        else
        {
            targetingIndicator.SetActive(false);
        }
    }

    public void FindNext()
    {
        int i = index;
        currentTarget = null;
        for (int loop =0;loop<zoneActors.Count; loop++)
        {            
            i++;
            if (i >= zoneActors.Count)
            {
                i = 0;
            }
            if (EligibleTarget(zoneActors[i]))
            {
                index = i;
                currentTarget = zoneActors[i];
                UpdateTargetIndicator();
                return;
            }
        }
    }

    private bool EligibleTarget(ZoneActor zoneActor)
    {
        if (zoneActor.IsVisible() && 
            zoneActor.GetAlive() && 
            zoneActor.actor.GetFaction().getRelationship(player.actor.GetFaction()) <= 50 && 
            (!((zoneActor.actor) is NPC) || !((NPC)zoneActor.actor).IsPeaceful()))
        {
            return true;
        }
        return false;
    }

    public void FindPrevious()
    {

    }

    public ZoneActor GetTarget()
    {
        return currentTarget;
    }

    public void SetTargeting(GameObject targetIndicator)
    {
        this.targetingIndicator = targetIndicator;
    }
}
