﻿using System;
using UnityEngine;

public class PlayerLookControl
{
    private Player player;
    private ZoneActor zoneActor;
    private Zone zone;
    private bool controlsEnabled;

    public PlayerLookControl(Player player, ZoneActor zoneActor, Zone zone)
    {
        controlsEnabled = true;
        this.player = player;
        this.zoneActor = zoneActor;
        this.zone = zone;
    }

    private void LookAtTile(Vector2Int position, ViewInterface view)
    {
        if (position.x>=0 && position.y>=0 && 
            position.x<zone.GetContents().GetZoneParameters().GetWidth() &&
            position.y < zone.GetContents().GetZoneParameters().GetHeight())
        {
            controlsEnabled = false;
          
            Tile t = zone.GetContents().getTiles()[position.x] [position.y];
            //actor
            if (t == null)
            {
                return;
            }
            if (t.GetActorInTile() != null && t.GetActorInTile().IsVisible())
            {
                Debug.Log(t.GetActorInTile().actor.GetDescription());
                view.DrawText(t.GetActorInTile().actor.GetDescription());
                return;
            }
            //widget
            if (t.GetWidget() != null  && t.GetWidget().getDescription()!=null)
            {
                view.DrawText(t.GetWidget().getDescription());
                return;
            }
            //tile
            view.DrawText(t.getDefinition().getDescription());
            controlsEnabled = false;
        }
    }

    public bool HandleLook(bool shift, ViewInterface view)
    {
       
        if (!controlsEnabled)
        {
            if (!Input.anyKey)
            {
                controlsEnabled = true;
            }
            return false;
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Keypad8) || Input.GetKey(KeyCode.W))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 0);
            LookAtTile(p, view);
            return true;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.S))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 4);
            LookAtTile(p, view);
            return true;
        }
        if (Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 6);
            LookAtTile(p, view);
            return true;
        }
        if (Input.GetKey(KeyCode.Keypad6) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 2);
            LookAtTile(p, view);
            return true;
        }
        if (Input.GetKey(KeyCode.Keypad9))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 1);
            LookAtTile(p, view);
            return true;
        }
        if (Input.GetKey(KeyCode.Keypad3))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 3);
            LookAtTile(p, view);
            return true;
        }
        if (Input.GetKey(KeyCode.Keypad1))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 5);
            LookAtTile(p, view);
            return true;
        }
        if (Input.GetKey(KeyCode.Keypad7))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 7);
            LookAtTile(p, view);
            return true;
        }
        return false;
    }
}