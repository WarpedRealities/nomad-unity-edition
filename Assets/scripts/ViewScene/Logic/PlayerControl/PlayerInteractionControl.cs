﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractionControl
{
    private Player player;
    private ZoneActor zoneActor;
    private PlayerMoveControl moveControl;
    private float clock;
    private float INTERVALTIME;
    private const int ALIASNORTH = 1;
    private const int ALIASEAST = 2;
    private const int ALIASSOUTH = 4;
    private const int ALIASWEST = 8;
    private const int ALIASNORTHEAST = 3;
    private const int ALIASSOUTHEAST = 6;
    private const int ALIASSOUTHWEST = 12;
    private const int ALIASNORTHWEST = 9;
    private const int ALIASWAIT = -2;

    private int direction;

    public PlayerInteractionControl(Player player, ZoneActor zoneActor, PlayerMoveControl moveControl)
    {
        INTERVALTIME = Config.GetInstance().GetInputWindow();
        this.player = player;
        this.zoneActor = zoneActor;
        this.moveControl = moveControl;
    }

    public bool Update(bool shift, ViewInterface view)
    {
        if (clock > 0)
        {
            clock += Time.deltaTime;
            if (clock >= INTERVALTIME)
            {
                //finalize
                int d = HandleInputs(shift);
                clock = 0;
                return AttemptInteract(ConvertAndToDirection(d >= 0 ? d : direction), view);
            }
        }
        else
        {
            //initialize
            direction = HandleInputs(shift);
            if (direction != -1)
            {

                clock += Time.deltaTime;
            }
        }
        return false;
    }

    private bool AttemptInteract(int direction, ViewInterface view)
    {
        Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, direction);
        if (InteractAtPosition(p))
        {
            zoneActor.GetController().addAction(new ActionInteract(p, player));
            return true;
        }
        else
        {
            moveControl.AttemptMove(direction, view);
            return true;
        }

    }

    private int ConvertAndToDirection(int and)
    {
        switch (and)
        {
            case ALIASNORTH:
                return 0;
            case ALIASEAST:
                return 2;
            case ALIASSOUTH:
                return 4;
            case ALIASWEST:
                return 6;
            case ALIASNORTHEAST:
                return 1;
            case ALIASNORTHWEST:
                return 7;
            case ALIASSOUTHEAST:
                return 3;
            case ALIASSOUTHWEST:
                return 5;
            case ALIASWAIT:
                return ALIASWAIT;
        }
        return -1;
    }

    public int HandleInputs(bool shift)
    {
        int d = 0;
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.A))
        {
            d = d | ALIASWEST;
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Keypad8) || Input.GetKey(KeyCode.W))
        {
            d = d | ALIASNORTH;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.S))
        {
            d = d | ALIASSOUTH;
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Keypad6) || Input.GetKey(KeyCode.D))
        {
            d = d | ALIASEAST;
        }
        if (Input.GetKey(KeyCode.Keypad7) || (shift && Input.GetKey(KeyCode.Home)))
        {
            d = d | ALIASNORTHWEST;
        }
        if (Input.GetKey(KeyCode.Keypad1) || (shift && Input.GetKey(KeyCode.End)))
        {
            d = d | ALIASSOUTHWEST;
        }
        if (Input.GetKey(KeyCode.Keypad3) || (shift && Input.GetKey(KeyCode.PageDown)))
        {
            d = d | ALIASSOUTHEAST;
        }
        if (Input.GetKey(KeyCode.Keypad9) || (shift && Input.GetKey(KeyCode.PageUp)))
        {
            d = d | ALIASNORTHEAST;
        }
        if (Input.GetKey(KeyCode.Keypad5) || Input.GetKey(KeyCode.Space))
        {
            d = ALIASWAIT;
        }
        return d;
    }

    public bool InteractAtPosition(Vector2Int p)
    {
        if (CollisionTools.GetTile(p)!=null && ((CollisionTools.GetTile(p).GetWidget() != null && CollisionTools.GetTile(p).GetWidget().CanInteract()) || 
            (CollisionTools.GetTile(p).GetActorInTile()!=null && CollisionTools.GetTile(p).GetActorInTile()!=zoneActor)))
        {
            return true;
        }

        return false;
    }

    public bool HandleInteraction(bool shift, ViewInterface view)
    {

        //initiate an interaction in a direction, 5 for where they're standing
        if (Input.GetKey(KeyCode.Keypad5))
        {
            Vector2Int p = player.GetPosition();
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p, player));

            }
            else
            {
                zoneActor.GetController().addAction(new ActionDelay(5, false));
            }
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Keypad8) || Input.GetKey(KeyCode.W))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 0);
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p , player));

            }
            else
            {
                return moveControl.AttemptMove(0, view);
            }
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.S))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 4);
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p, player));

            }
            else
            {
                return moveControl.AttemptMove(4, view);
            }
        }
        if (Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 6);
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p, player));

            }
            else
            {
                return moveControl.AttemptMove(6, view);
            }
        }
        if (Input.GetKey(KeyCode.Keypad6) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 2);
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p, player));

            }
            else
            {
                return moveControl.AttemptMove(2, view);
            }
        }
        if (Input.GetKey(KeyCode.Keypad9))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 1);
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p, player));

            }
            else
            {
                return moveControl.AttemptMove(1, view);
            }
        }
        if (Input.GetKey(KeyCode.Keypad3))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 3);
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p, player));

            }
            else
            {
                return moveControl.AttemptMove(3, view);
            }
        }
        if (Input.GetKey(KeyCode.Keypad1))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 5);
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p, player));

            }
            else
            {
                return moveControl.AttemptMove(5, view);
            }
        }
        if (Input.GetKey(KeyCode.Keypad7))
        {
            Vector2Int p = GeometryTools.GetPosition(player.GetPosition().x, player.GetPosition().y, 7);
            if (InteractAtPosition(p))
            {
                zoneActor.GetController().addAction(new ActionInteract(p, player));

            }
            else
            {
                return moveControl.AttemptMove(7, view);
            }
        }
        return false;
    }
}
