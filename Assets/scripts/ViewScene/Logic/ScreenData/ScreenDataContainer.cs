﻿using UnityEngine;
using System.Collections;

public class ScreenDataContainer : ScreenData
{
    public ContainerData containerData;

    public ScreenDataContainer(ContainerData containerData)
    {
        this.containerData = containerData;
        this.dataType = ScreenDataType.CONTAINER;

    }

}
