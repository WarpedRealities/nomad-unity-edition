﻿using UnityEngine;
using System.Collections;

public class ScreenDataShipSystem : ScreenData
{
    Player player;
    WidgetShipSystem widgetShipSystem;

    public ScreenDataShipSystem(WidgetShipSystem widgetShipSystem, Player player)
    {
        this.widgetShipSystem = widgetShipSystem;
        this.player = player;
        dataType = ScreenDataType.SHIPSYSTEM;
    }

    public Player GetPlayer()
    {
        return player;
    }

    public WidgetShipSystem GetSystem()
    {
        return widgetShipSystem;
    }
}
