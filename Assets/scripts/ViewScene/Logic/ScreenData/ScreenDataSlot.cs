﻿using UnityEngine;
using System.Collections;

public class ScreenDataSlot : ScreenData
{
    WidgetSlot widgetSlot;
 
    public ScreenDataSlot(WidgetSlot widgetSlot)
    {
        this.widgetSlot = widgetSlot;
        this.dataType = ScreenDataType.SLOT;
    }


    public WidgetSlot GetSlot()
    {
        return widgetSlot;
    }
}
