using UnityEngine;
using System.Collections;

public class ScreenDataJail : ScreenData
{
    public NPC[] captives ;
    public WidgetJail widgetJail;

    public ScreenDataJail(WidgetJail widget)
    {
        this.widgetJail= widget;
        this.captives = widget.GetCaptives();
        this.dataType = ScreenDataType.JAIL;
    }

}
