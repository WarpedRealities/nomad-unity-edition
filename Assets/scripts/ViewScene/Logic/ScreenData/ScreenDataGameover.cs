﻿using UnityEngine;
using System.Collections;

public class ScreenDataGameover : ScreenData
{
    string gameover;

    public ScreenDataGameover(string gameover)
    {
        this.gameover = gameover;
        this.dataType = ScreenDataType.GAMEOVER;
    }

    public string GetGameover()
    {
        return gameover;
    }

}
