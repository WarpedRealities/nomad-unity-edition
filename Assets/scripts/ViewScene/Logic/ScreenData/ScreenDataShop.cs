﻿using UnityEngine;
using System.Collections;

public class ScreenDataShop : ScreenData
{
    Store store;

    public ScreenDataShop(Store store)
    {
        this.store = store;
        this.dataType = ScreenDataType.SHOP;
    }

    public Store GetStore()
    {
        return store;
    }
}
