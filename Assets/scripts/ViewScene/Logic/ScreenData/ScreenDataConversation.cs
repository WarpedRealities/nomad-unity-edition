﻿using UnityEngine;
using System.Collections;
using System;

public class ScreenDataConversation : ScreenData
{

    Widget widget;
    DialogueActor actor;
    Active_Entity entity;
    FlagField flagField;
    private string conversationFile;
    private bool gameOverOnAbort;


    public ScreenDataConversation(string conversationFile, FlagField flagField, WidgetConversation widgetConversation=null)
    {

        this.conversationFile = conversationFile;
        this.flagField = flagField;
        this.widget = widgetConversation;
        this.dataType = ScreenDataType.CONVERSATION;
    }

    public ScreenDataConversation(string conversationFile, FlagField flagField, Active_Entity entity)
    {

        this.conversationFile = conversationFile;
        this.flagField = flagField;
        this.entity = entity;
        this.dataType = ScreenDataType.CONVERSATION;
    }

    public ScreenDataConversation(string conversationFile, FlagField flagField, DialogueActor actor, Widget widget, bool gameOverOnAbort)
    {
        this.gameOverOnAbort = gameOverOnAbort;
        this.conversationFile = conversationFile;
        this.flagField = flagField;
        this.actor = actor;
        this.widget = widget;
        this.dataType = ScreenDataType.CONVERSATION;
    }

    public ScreenDataConversation(string conversationFile, FlagField flagField, DialogueActor actor, bool gameOverOnAbort)
    {
        this.gameOverOnAbort = gameOverOnAbort;
        this.conversationFile = conversationFile;
        this.flagField = flagField;
        this.actor = actor;
        this.dataType = ScreenDataType.CONVERSATION;
    }

    public ScreenDataConversation(string filename)
    {
    }

    public bool IsGameoverOnAbort()
    {
        return gameOverOnAbort;
    }

    public Widget GetWidget()
    {
        return widget;
    }

    public DialogueActor GetActor()
    {
        return actor;
    }

    public FlagField GetFlagField()
    {
        return flagField;
    }

    public string GetConversationFile()
    {
        return conversationFile;
    }

    internal Active_Entity GetEntity()
    {
        return this.entity;
    }
}
