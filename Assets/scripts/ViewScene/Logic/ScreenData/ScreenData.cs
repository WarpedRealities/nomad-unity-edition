﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class ScreenData 
{
    protected ScreenDataType dataType;

    public ScreenDataType GetDataType()
    {
        return dataType;
    }
}
