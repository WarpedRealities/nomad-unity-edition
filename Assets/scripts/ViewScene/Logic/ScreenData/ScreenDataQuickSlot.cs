using UnityEngine;
using System.Collections;

public class ScreenDataQuickslot : ScreenData
{
    int index;

    public ScreenDataQuickslot(int index)
    {
        this.index = index;
        this.dataType = ScreenDataType.QUICKSLOT;
    }

    public int GetIndex()
    {
        return index;
    }
}
