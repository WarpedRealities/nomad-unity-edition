﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkRenderScript : MonoBehaviour
{
    public GameObject prefabWidget, prefabAnimatedTile;
    List<SpecialRender> specialRenders=new List<SpecialRender>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void buildChunk(Zone zone, int x, int y, Material [] materials)
    {
        BroadcastMessage("buildLayer", new BuildLayerParams(x, y, zone));
        BroadcastMessage("setTilesetMaterial", materials);
        BroadcastMessage("buildWidgets", new BuildLayerParams(x, y, zone));
        BroadcastMessage("setWidgetsMaterial", materials);
        BuildSpecialRenders(zone, x, y, materials);
    }

    private void BuildSpecialRenders(Zone zone, int x, int y, Material[] materials)
    {
        int offsetX = x * 16;
        int offsetY = y * 16;
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                Tile t = zone.GetContents().GetTile(offsetX + i, offsetY + j);
                if (t != null)
                {
                    if (t.GetWidget()!=null && t.GetWidget().IsColoured())
                    {
                        ColouredWidget colouredWidget = GameObject.Instantiate(prefabWidget, transform).GetComponent<ColouredWidget>();
                        //position
                        colouredWidget.transform.position = new Vector3(i + offsetX, j + offsetY);
                        //build
                        colouredWidget.SetTile(t);
                        //add material
                        colouredWidget.SetMaterials(materials);
                    }
                    
                    if (t.getDefinition().getSmartTile() == -1)
                    {
                        AnimatedTileRender animatedTileRender = GameObject.Instantiate(prefabAnimatedTile, transform).GetComponent<AnimatedTileRender>();
                        animatedTileRender.transform.position = new Vector3(i + offsetX, j + offsetY);
                        animatedTileRender.SetTile(t, zone.GetContents().getTileSet().getWidth(), zone.GetContents().getTileSet().getHeight());
                        animatedTileRender.SetMaterials(materials);
                    }
                }
            }
        }
        UpdateSpecialRenders();
    }

    internal void rebuildChunk(Zone zone, int x, int y, Material[] materials)
    {
        BroadcastMessage("buildLayer", new BuildLayerParams(x, y, zone));
        BroadcastMessage("buildWidgets", new BuildLayerParams(x, y, zone));
        UpdateSpecialRenders();
    }

    private void UpdateSpecialRenders()
    {
        for (int i = 0; i < specialRenders.Count; i++)
        {
            specialRenders[i].UpdateRender();
        }
    }
}
