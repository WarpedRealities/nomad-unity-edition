﻿using UnityEngine;
using System.Collections;

public class BigSprite : MonoBehaviour
{
    private bool visible = false;
    public Tile[] cornerTiles;
    public SpriteRenderer renderer;
    // Use this for initialization
    void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
        renderer.enabled = false;
        CalculateCorners();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CalculateCorners()
    {
        if (visible)
        {
            return;
        }
        for (int i = 0; i < 4; i++)
        {
            if (cornerTiles[i]!=null && cornerTiles[i].isExplored())
            {
                visible = true;
                renderer.enabled = true;
                return;
            }
        }
    }
}
