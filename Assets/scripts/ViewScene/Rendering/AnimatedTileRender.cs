﻿using UnityEngine;
using System.Collections;
using System;

public class AnimatedTileRender : SpecialRender
{

    int frame = 0;
    float clock = 0;
    float w, h;

    internal void SetTile(Tile t, int textureWidth, int textureHeight)
    {
        tile = t;
        if (meshFilter == null)
        {
            meshFilter = gameObject.GetComponent<MeshFilter>();
        }
        if (meshRenderer == null)
        {
            meshRenderer = gameObject.GetComponent<MeshRenderer>();
        }
        BuildMesh(textureWidth,textureHeight);
    }

    private void BuildMesh(int width, int height)
    {
        int wText = width;
        w = width;
        h = height;
        float w0 = 1 / ((float)width);
        float h0 = 1 / ((float)height);
        Vector2 uvIncrement = new Vector2(w0, h0);

        Mesh mesh = new Mesh();
        UnityEngine.Vector3[] vertices = new UnityEngine.Vector3[4];
        UnityEngine.Vector3[] normals = new UnityEngine.Vector3[4];
        UnityEngine.Vector2[] uv = new UnityEngine.Vector2[4];
        int[] indices = new int[6];
        indices[0] = 0;
        indices[1] = 0 + 1;
        indices[2] = 0 + 3;
        indices[3] = 0;
        indices[4] = 0 + 3;
        indices[5] = 0 + 2;
        vertices[0] = new UnityEngine.Vector3(0, 0, 0);
        vertices[1] = new UnityEngine.Vector3(0 + 1, 0, 0);
        vertices[2] = new UnityEngine.Vector3(0, 0 + 1, 0);
        vertices[3] = new UnityEngine.Vector3(0 + 1, 0 + 1, 0);
        int yimg = ((tile.getSprite()) / (int)w) + 1;
        int ximg = tile.getSprite() % (int)h;

        float xcoord = ((ximg) * uvIncrement.x);
        float ycoord = 1 - ((yimg) * uvIncrement.y);
        uv[0] = new UnityEngine.Vector2(xcoord, ycoord);
        uv[1] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord);
        uv[2] = new UnityEngine.Vector2(xcoord, ycoord + uvIncrement.y);
        uv[3] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord + uvIncrement.y);

        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] = -UnityEngine.Vector3.forward;
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;
    }

    public void SetMaterials(Material[] materials)
    {
        materials = new Material[] { materials[0] };
        this.meshRenderer.material = materials[0];
    }

    internal override void UpdateRender()
    {
        meshRenderer.enabled = false;
        meshRenderer.enabled = tile.isVisible();
    }
    internal override void FrameUpdate()
    {
        clock += Time.deltaTime;
        if (clock > 0.2F)
        {
            clock = 0;
            frame++;
            if (frame > 3)
            {
                frame = 0;
            }
            UpdateFrame();
        }
    }

    private void UpdateFrame()
    {
        float w0 = 1 / ((float)w);
        float h0 = 1 / ((float)h);
        Vector2 uvIncrement = new Vector2(w0, h0);
        UnityEngine.Vector2[] uv= meshFilter.mesh.uv;
        int s = tile.getSprite() + frame-1;
        int yimg = ((s) / (int)w) + 1;
        int ximg = s % (int)h;

        float xcoord = ((ximg) * uvIncrement.x);
        float ycoord = 1 - ((yimg) * uvIncrement.y);
        uv[0] = new UnityEngine.Vector2(xcoord, ycoord);
        uv[1] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord);
        uv[2] = new UnityEngine.Vector2(xcoord, ycoord + uvIncrement.y);
        uv[3] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord + uvIncrement.y);

        meshFilter.mesh.uv = uv;
        UpdateRender();
    }
}
