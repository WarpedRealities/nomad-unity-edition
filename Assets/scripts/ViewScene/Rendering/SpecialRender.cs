﻿using UnityEngine;
using System.Collections;
using System;

public class SpecialRender : MonoBehaviour
{
    protected Tile tile;
    protected Material[] materials;
    protected MeshRenderer meshRenderer;
    protected MeshFilter meshFilter;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.FrameUpdate();
    }

    internal virtual void FrameUpdate()
    {

    }

    internal virtual void UpdateRender()
    {
        throw new NotImplementedException();
    }
}
