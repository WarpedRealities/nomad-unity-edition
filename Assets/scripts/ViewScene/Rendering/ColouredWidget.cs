﻿using UnityEngine;
using System.Collections;
using System;

public class ColouredWidget : SpecialRender
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void SetTile(Tile t)
    {
        tile = t;
        if (meshFilter == null)
        {
            meshFilter = gameObject.GetComponent<MeshFilter>();
        }
        if (meshRenderer == null)
        {
            meshRenderer = gameObject.GetComponent<MeshRenderer>();
        }
        BuildMesh();
    }

    private void BuildMesh()
    {
        int wText = 16;

        float xUV = 1 / 16.0F;
        float yUV = 1 / 16.0F;
        Mesh mesh = new Mesh();
        UnityEngine.Vector3[] vertices = new UnityEngine.Vector3[4];
        UnityEngine.Vector3[] normals = new UnityEngine.Vector3[4];
        UnityEngine.Vector2[] uv = new UnityEngine.Vector2[4];
        int[] indices = new int[6];
        int sprite = tile.GetWidget().getSprite()<0 ? (tile.GetWidget().getSprite() + 1) * -1 : tile.GetWidget().getSprite() - 1;
        Debug.Log("sprite ? " + sprite);
        float offset = tile.GetWidget().IsFloored() ? 0 : 0;
        vertices[0] = new UnityEngine.Vector3(0, 0 + offset, 0);
        vertices[1] = new UnityEngine.Vector3(0 + 1,0 + offset, 0);
        vertices[2] = new UnityEngine.Vector3(0, 0 + 1.0F + offset, 0);
        vertices[3] = new UnityEngine.Vector3(0 + 1, 0 + 1.0F + offset, 0);
        int yimg = ((sprite) / wText) + 1;
        int ximg = (sprite) % wText;
        Vector2 uvIncrement = new Vector2(xUV, yUV);
        float xcoord = ((ximg) * uvIncrement.x);
        float ycoord = 1 - ((yimg) * uvIncrement.y);
        uv[0] = new UnityEngine.Vector2(xcoord, ycoord);
        uv[1] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord);
        uv[2] = new UnityEngine.Vector2(xcoord, ycoord + uvIncrement.y);
        uv[3] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord + uvIncrement.y);


        indices[0] = 0;
        indices[1] = 0 + 1;
        indices[2] = 0 + 3;
        indices[3] = 0;
        indices[4] = 0 + 3;
        indices[5] = 0 + 2;



        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] = -UnityEngine.Vector3.forward;
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;
    }

    public void SetMaterials(Material[] materials) {
        this.materials = new Material[] {new Material(materials[1]), new Material(materials[4])};
        float[] f= tile.GetWidget().GetColour();
        this.materials[0].color = new Color(f[0], f[1], f[2], f[3]);
        this.materials[1].color = new Color(f[0], f[1], f[2], f[3]);
        this.meshRenderer.material = this.tile.GetWidget().getSprite() < 0 ? this.materials[1] : this.materials[0];
    }

    internal override void UpdateRender()
    {
        meshRenderer.enabled = false;
        if (tile.GetWidget()==null || !tile.GetWidget().IsColoured())
        {
            return;
        }
        meshRenderer.enabled = tile.isVisible();
    }
}
