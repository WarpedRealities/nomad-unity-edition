﻿using UnityEngine;
using System.Collections;

public class ZoneActorRenderTool
{

    public static void generate(string filename, MeshRenderer meshRenderer, MeshFilter meshFilter, int sprite)
    {
        meshRenderer.material = SpriteLibrary.getInstance().GetMaterial(filename);
        Mesh mesh = new Mesh();
        float width = ((float)meshRenderer.material.mainTexture.width) / 64;
        float height = ((float)meshRenderer.material.mainTexture.height) / 64;
        float wHalf = width / 2;
        float hHalf = height / 2;
        Vector3[] vertices = new Vector3[4];
        Vector2[] uvmap = new Vector2[4];
        Vector3[] normals = new Vector3[4];
        int[] indices = new int[6];
        float offset = 0;
        vertices[0] = new UnityEngine.Vector3(0.5F-wHalf, offset, 0);
        vertices[1] = new UnityEngine.Vector3(0.5F + wHalf, offset, 0);
        vertices[2] = new UnityEngine.Vector3(0.5F-wHalf, offset + height, 0);
        vertices[3] = new UnityEngine.Vector3(0.5F + wHalf, offset + height, 0);

        float uvW = (sprite % 2) * 0.5F;
        float uvH = sprite >= 2 ? 0.0F : 0.5F;
        uvmap[0] = new UnityEngine.Vector2(uvW, uvH);
        uvmap[1] = new UnityEngine.Vector2(uvW + 0.5F, uvH);
        uvmap[2] = new UnityEngine.Vector2(uvW, uvH + 0.5F);
        uvmap[3] = new UnityEngine.Vector2(uvW + 0.5F, uvH + 0.5F);

        indices[0] = 0;
        indices[1] = 1;
        indices[2] = 3;
        indices[3] = 0;
        indices[4] = 3;
        indices[5] = 2;

        normals[0] = -UnityEngine.Vector3.forward;
        normals[1] = -UnityEngine.Vector3.forward;
        normals[2] = -UnityEngine.Vector3.forward;
        normals[3] = -UnityEngine.Vector3.forward;

        mesh.vertices = vertices;
        mesh.uv = uvmap;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;
    }

    public static void regenerate(string filename, MeshRenderer meshRenderer, MeshFilter meshFilter, int sprite)
    {
        Mesh mesh = meshFilter.mesh;
        float width = ((float)meshRenderer.material.mainTexture.width) / 64;
        float height = ((float)meshRenderer.material.mainTexture.height) / 64;
        float wHalf = width / 2;
        float hHalf = height / 2;
        Vector3[] vertices = new Vector3[4];
        Vector2[] uvmap = new Vector2[4];
        Vector3[] normals = new Vector3[4];
        int[] indices = new int[6];
        float offset = 0;
        vertices[0] = new UnityEngine.Vector3(0.5F - wHalf, offset, 0);
        vertices[1] = new UnityEngine.Vector3(0.5F + wHalf, offset, 0);
        vertices[2] = new UnityEngine.Vector3(0.5F - wHalf, offset + height, 0);
        vertices[3] = new UnityEngine.Vector3(0.5F + wHalf, offset + height, 0);

        float uvW = (sprite % 2)*0.5F;
        float uvH = sprite >= 2 ? 0.0F : 0.5F;
        uvmap[0] = new UnityEngine.Vector2(uvW, uvH);
        uvmap[1] = new UnityEngine.Vector2(uvW+0.5F, uvH);
        uvmap[2] = new UnityEngine.Vector2(uvW, uvH+0.5F);
        uvmap[3] = new UnityEngine.Vector2(uvW+0.5F, uvH + 0.5F);

        indices[0] = 0;
        indices[1] = 1;
        indices[2] = 3;
        indices[3] = 0;
        indices[4] = 3;
        indices[5] = 2;

        normals[0] = -UnityEngine.Vector3.forward;
        normals[1] = -UnityEngine.Vector3.forward;
        normals[2] = -UnityEngine.Vector3.forward;
        normals[3] = -UnityEngine.Vector3.forward;

        mesh.vertices = vertices;
        mesh.uv = uvmap;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;
    }
}
