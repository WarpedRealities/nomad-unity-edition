﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRepo 
{
    private static SpriteRepo instance;

    public static SpriteRepo GetInstance()
    {
        if (instance == null)
        {
            instance = new SpriteRepo();
        }
        return instance;
    }

    Dictionary<string, Sprite> sprites;

    public SpriteRepo()
    {
        sprites = new Dictionary<string, Sprite>();
    }

    public Sprite GetSprite(string filename)
    {
        Sprite sprite = null;
        if (sprites.TryGetValue(filename, out sprite))
        {
            return sprite;
        }
        else
        {
            Texture2D texture = TextureManagementTools.loadTexture("gameData/art/big_sprites/"+filename+".png");
            sprite = Sprite.Create(texture, 
                new Rect(new Vector2(0, 0), 
                new Vector2(texture.width, texture.height)),
                new Vector2(0,0),32);
            sprites.Add(filename, sprite);
            return sprite;
        }
    }

    internal Sprite GetSprite(int v)
    {
        throw new NotImplementedException();
    }
}
