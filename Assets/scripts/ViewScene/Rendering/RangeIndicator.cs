﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeParams
{
    public Vector2Int start, end;
    public float duration;
    public int colourIndex;

    public RangeParams(Vector2Int start, Vector2Int end, float duration, int colourIndex)
    {
        this.start = start;
        this.end = end;
        this.duration = duration;
        this.colourIndex = colourIndex;
    }
}

public class RangeIndicator : MonoBehaviour
{
    LineRenderer lineRenderer;
    public Material parentMaterial;
    Material material;
    float clock;

    // Start is called before the first frame update
    void Start()
    {
        material = new Material(parentMaterial);
        material.color = new Color(1, 0, 0);
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.material = material;
    }

    // Update is called once per frame
    void Update()
    {
        if (clock>0)
        {
            clock -= Time.deltaTime;
            if (clock <= 0)
            {
                lineRenderer.enabled = false;
            }
        }
    }

    public void Initialize(RangeParams rangeParams)
    {
        Initialize(rangeParams.start, rangeParams.end, rangeParams.duration, rangeParams.colourIndex);
    }

    public void Initialize(Vector2 start, Vector2 end, float duration, int colourIndex)
    {
        lineRenderer.enabled = true;
        clock = duration;
        lineRenderer.SetPosition(0, new Vector3(start.x+0.5F, start.y+0.5F, -2));
        lineRenderer.SetPosition(1, new Vector3(end.x+0.5F, end.y+0.5F, -2));
        material.color = GetColourFromIndex(colourIndex);
    }

    private Color GetColourFromIndex(int colourIndex)
    {
        switch (colourIndex)
        {
            case 0:
                return new Color(1, 0, 0);
            case 1:
                return new Color(1, 0.5F, 0.5F);
        }
        return new Color(1,1,1);
    }
}
