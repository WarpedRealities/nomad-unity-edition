﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildLayerParams
{
    public Zone zone;
    public int x, y;

    public BuildLayerParams(int x, int y, Zone zone)
    {
        this.x = x;
        this.y = y;
        this.zone = zone;
    }
}

public class GeometryLayerScript : MonoBehaviour
{
    public bool darkMode;
    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;

    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshFilter = gameObject.GetComponent<MeshFilter>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private int tileCount(int offsetX, int offsetY, Zone zone)
    {
     
        int count = 0;
        Tile[][] tiles = zone.GetContents().getTiles();
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                if (tiles[i + offsetX][j + offsetY] != null)
                {
                    Tile tile = tiles[i + offsetX][j + offsetY];
                    if (tile.isExplored() && 
                        ((!darkMode && tile.isVisible())|| (darkMode && !tile.isVisible())) &&
                        tile.getSprite()>0)
                    {
                        if (tile.getDefinition().getSmartTile() == 2)
                        {
                            count+=2;
                        }
                        else
                        {                        
                            count++;
                            if (tile.getDefinition().getDecoration() > 0)
                            {
                                count++;
                            }
                        }
                      
                    }
                }
            }
        }
        return count;
    }

    public void setTilesetMaterial(Material[] materials)
    {
     
        if (darkMode)
        {
            meshRenderer.material = materials[2];
        }
        else
        {
            meshRenderer.material = materials[0];
        }

    }

    public void buildLayer(BuildLayerParams buildLayerParams)
    {
        if (meshFilter == null)
        {
            meshFilter = gameObject.GetComponent<MeshFilter>();
        }
        if (meshRenderer == null)
        {
            meshRenderer = gameObject.GetComponent<MeshRenderer>();
        }
        buildLayer(buildLayerParams.x, buildLayerParams.y, buildLayerParams.zone);
    }

    public void buildLayer(int x, int y, Zone zone)
    {
        int wText = zone.GetContents().getTileSet().getWidth();
        
        float xUV = 1 / ((float)zone.GetContents().getTileSet().getWidth());
        float yUV = 1 / ((float)zone.GetContents().getTileSet().getHeight());
        Vector2 uvIncrement = new Vector2(xUV, yUV);
        int offsetX = x * 16;
        int offsetY = y * 16;
        int count = tileCount(offsetX, offsetY, zone);
        Mesh mesh = new Mesh();
        UnityEngine.Vector3[] vertices = new UnityEngine.Vector3[count * 4];
        UnityEngine.Vector3[] normals = new UnityEngine.Vector3[count * 4];
        UnityEngine.Vector2[] uv = new UnityEngine.Vector2[count * 4];
        int[] indices = new int[count * 6];
        int index = 0;
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                Tile t = zone.GetContents().getTiles()[i + offsetX][j + offsetY];
               
                if (t!=null && t.isExplored() &&
                   ((!darkMode && t.isVisible()) || (darkMode && !t.isVisible())) && 
                    t.getSprite()>0)
                {
                    
                    index= t.buildGeometry(vertices,uv,new Vector2(i,j),uvIncrement,wText,index);
                }
            }
        }
        
        for (int i = 0; i < count; i++)
        {
            int triangleOffset = i * 6;
            int indexOffset = i * 4;
            indices[triangleOffset + 0] = indexOffset;
            indices[triangleOffset + 1] = indexOffset+1;
            indices[triangleOffset + 2] = indexOffset+3;
            indices[triangleOffset + 3] = indexOffset;
            indices[triangleOffset + 4] = indexOffset+3;
            indices[triangleOffset + 5] = indexOffset+2;
        }


        for (int i=0;i<normals.Length;i++)
        {
            normals[i] = -UnityEngine.Vector3.forward;
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;

    }
}
