﻿using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;


public class ZoneRenderScript : MonoBehaviour
{
    public GameObject prefabChunk, prefabBigSprite;
    private Zone zone;
    private Texture2D tileset, widgets, shipWidgets;
    private GameObject[][] chunks;
    private List<GameObject> bigSprites;
    private ChunkRenderScript chunkRender;
    public Material[] materials;

    // Start is called before the first frame update
    void Start()
    {

       // zone = GlobalGameState.getInstance().getCurrentZone();


    }

    public void initialize(Zone zone)
    {
        this.zone = zone;

        tileset = TextureManagementTools.loadTexture("gameData/art/tilesets/" + zone.GetContents().GetZoneParameters().getSpriteSet() + ".png");
        widgets = TextureManagementTools.loadTexture("gameData/art/misc/widgets.png");
        shipWidgets = TextureManagementTools.loadTexture("gameData/art/misc/ship_Widgets.png");
        materials[0].mainTexture = tileset;
        materials[2].mainTexture = tileset;
        // tileMat.color = new Color(0.5F, 0.5F, 0.5F);
        materials[1].mainTexture = widgets;
        materials[3].mainTexture = widgets;
        materials[4].mainTexture = shipWidgets;
        materials[5].mainTexture = shipWidgets;
        //  chunkRender = gameObject.GetComponentInChildren<ChunkRenderScript>();
        //  chunkRender.buildChunk(zone, 0, 0, materials);
        generateChunks();
        bigSprites = new BigSpriteGenerator().GenerateSprites(zone.GetContents(), prefabBigSprite, gameObject);
    }

    public void generateChunks()
    {
        int width = zone.GetContents().GetZoneParameters().GetWidth() / 16;
        int height = zone.GetContents().GetZoneParameters().GetHeight() / 16;

        chunks = new GameObject[width][];
        for (int i = 0; i < width; i++)
        {
            chunks[i] = new GameObject[height];
            for (int j=0; j< height; j++)
            {

                chunks[i][j] = Instantiate(prefabChunk);
                chunks[i][j].transform.parent = gameObject.transform;
                chunks[i][j].transform.position = new Vector3(i*16, j*16, 0);
                chunks[i][j].GetComponentInChildren<ChunkRenderScript>().buildChunk(zone, i, j, materials);
            }
        }
    }

    public void RebuildChunks()
    {
        for (int i = 0; i < chunks.Length; i++)
        {

            for (int j = 0; j < chunks[0].Length; j++)
            {           
                chunks[i][j].GetComponentInChildren<ChunkRenderScript>().rebuildChunk(zone, i, j, materials);
            }
        }

        for (int i = 0; i < bigSprites.Count; i++)
        {
            bigSprites[i].BroadcastMessage("CalculateCorners");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
