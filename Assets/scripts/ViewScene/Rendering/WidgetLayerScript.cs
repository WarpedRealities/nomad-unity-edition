﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WidgetLayerScript : MonoBehaviour
{
    public bool darkMode, shipWidgets;
    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;

    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshFilter = gameObject.GetComponent<MeshFilter>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private int tileCount(int offsetX, int offsetY, Zone zone)
    {
        int count = 0;
        Tile[][] tiles = zone.GetContents().getTiles();
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                if (tiles[i + offsetX][j + offsetY] != null)
                {
                    Tile tile = tiles[i + offsetX][j + offsetY];
                    if (tile.isExplored() &&
                        ((!darkMode && tile.isVisible()) || (darkMode && !tile.isVisible())) )
                    {
                        if (!shipWidgets && tile.GetWidget() != null && tile.GetWidget().getSprite() > 0)
                        {
                            count++;
                        }
                        if (shipWidgets && tile.GetWidget() != null && tile.GetWidget().getSprite() < 0)
                        {
                            count++;
                        }
                    }
                }
            }
        }
   
        return count;
    }

    public void setWidgetsMaterial(Material[] materials)
    {
        if (darkMode)
        {
            meshRenderer.material = shipWidgets ? materials[5] : materials[3];
           
        //    meshRenderer.material = materials[3];
        }
        else
        {
            meshRenderer.material = shipWidgets ? materials[4] : materials[1];
        //    meshRenderer.material = materials[1];
        }
    }

    public void buildWidgets(BuildLayerParams buildLayerParams)
    {
        if (meshFilter == null)
        {
            meshFilter = gameObject.GetComponent<MeshFilter>();
        }
        if (meshRenderer == null)
        {
            meshRenderer = gameObject.GetComponent<MeshRenderer>();
        }
        buildWidgets(buildLayerParams.x, buildLayerParams.y, buildLayerParams.zone);
    }

    public void buildWidgets(int x, int y, Zone zone)
    {
        int wText = 16;

        float xUV = 1/16.0F;
        float yUV = 1/ 16.0F;
        Vector2 uvIncrement = new Vector2(xUV, yUV);
        int offsetX = x * 16;
        int offsetY = y * 16;
        int count = tileCount(offsetX, offsetY, zone);
        Mesh mesh = new Mesh();
        UnityEngine.Vector3[] vertices = new UnityEngine.Vector3[count * 4];
        UnityEngine.Vector3[] normals = new UnityEngine.Vector3[count * 4];
        UnityEngine.Vector2[] uv = new UnityEngine.Vector2[count * 4];
        int[] indices = new int[count * 6];
        int index = 0;
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                Tile t = zone.GetContents().getTiles()[i + offsetX][j + offsetY];

                if (t != null && t.isExplored() &&
                    ((!darkMode && t.isVisible()) || (darkMode && !t.isVisible())) &&
                    t.GetWidget()!=null && ((t.GetWidget().getSprite() > 0 && !shipWidgets) || (t.GetWidget().getSprite() < 0 && shipWidgets)))
                {

                    int sprite = shipWidgets? (t.GetWidget().getSprite() +1) * -1 : t.GetWidget().getSprite() -1;
                    float offset = t.GetWidget().IsFloored()? 0 : 0;
                    int indexOffset = index * 4;
                    vertices[indexOffset + 0] = new UnityEngine.Vector3(i , j + offset, 0);
                    vertices[indexOffset + 1] = new UnityEngine.Vector3(i  + 1, j + offset, 0);
                    vertices[indexOffset + 2] = new UnityEngine.Vector3(i , j  + 1.0F + offset, 0);
                    vertices[indexOffset + 3] = new UnityEngine.Vector3(i + 1, j + 1.0F + offset, 0);
                    int yimg = ((sprite) / wText) + 1;
                    int ximg = (sprite) % wText;

                    float xcoord = ((ximg) * uvIncrement.x);
                    float ycoord = 1 - ((yimg) * uvIncrement.y);
                    uv[indexOffset + 0] = new UnityEngine.Vector2(xcoord, ycoord);
                    uv[indexOffset + 1] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord);
                    uv[indexOffset + 2] = new UnityEngine.Vector2(xcoord, ycoord + uvIncrement.y);
                    uv[indexOffset + 3] = new UnityEngine.Vector2(xcoord + uvIncrement.x, ycoord + uvIncrement.y);

                    index++;
                }
            }
        }

        for (int i = 0; i < count; i++)
        {
            int triangleOffset = i * 6;
            int indexOffset = i * 4;
            indices[triangleOffset + 0] = indexOffset;
            indices[triangleOffset + 1] = indexOffset + 1;
            indices[triangleOffset + 2] = indexOffset + 3;
            indices[triangleOffset + 3] = indexOffset;
            indices[triangleOffset + 4] = indexOffset + 3;
            indices[triangleOffset + 5] = indexOffset + 2;
        }


        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] = -UnityEngine.Vector3.forward;
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;

    }
}
