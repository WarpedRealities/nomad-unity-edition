using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CombatNumber : MonoBehaviour
{
    public TextMeshPro textMeshPro;
    public Color[] colors;
    private float duration;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (duration >= 0)
        {
            transform.Translate(new Vector3(0, 2 * Time.deltaTime, 0));
            duration -= Time.deltaTime;
            Color color = textMeshPro.color;
            color.a = duration / 2;
            textMeshPro.color = color;
            if (duration <= 0)
            {
                textMeshPro.enabled = false;
            }
        }
    }

    public bool isAlive()
    {
        return duration > 0;
    }

    public void Initialize(Vector2 position, int number, int index)
    {
        duration = 2;
        transform.position = position;
        textMeshPro.enabled = true;
        textMeshPro.text = number.ToString();
        textMeshPro.color = colors[index];
    }
}
