﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShop : MonoBehaviour
{

    ItemStore itemStore;
    Player player;
    ViewScene view;
    int index;
    bool isSelling;
    public SolarSceneScript solarSceneScript;
    public NuList shopInventory;
    public NuList playerInventory;
    public ListCallback shopCallback;
    public ListCallback inventoryCallback;
    public Text moneyText, weightText, itemText, buttonText,descriptionText;
    // Start is called before the first frame update
    void Start()
    {
        long clock = GlobalGameState.GetInstance().GetUniverse().GetClock();
        if (clock > itemStore.GetLastInteraction() + itemStore.GetRefreshInterval())
        {
            //refresh shop
            itemStore.Refresh();
        }

        itemStore.SetLastInteraction(clock);
        isSelling = false;
        index = 0;
        shopCallback = this.ShopCallback;
        inventoryCallback = this.InventoryCallback;
        shopInventory.SetCallback(shopCallback);
        playerInventory.SetCallback(inventoryCallback);
        BuildUI();
        if (itemStore.GetInventory().GetItemCount() > 0)
        {
            ShopCallback(0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSolarScene(SolarSceneScript script)
    {
        this.solarSceneScript = script;
    }
    public void ShopCallback(int index)
    {
        itemText.text = itemStore.GetInventory().GetItem(index).GetItemString() + GetValue(itemStore.GetInventory().GetItem(index), false);
        descriptionText.text = itemStore.GetInventory().GetItem(index).GetDescription();
        buttonText.text = "Buy";
        isSelling = false;
        this.index = index;
    }

    public void InventoryCallback(int index)
    {
        itemText.text = player.GetInventory().GetItem(index).GetItemString() + GetValue(player.GetInventory().GetItem(index), true);
        descriptionText.text = player.GetInventory().GetItem(index).GetDescription();
        buttonText.text = "Sell";
        isSelling = true;
        this.index = index;
    }

    public void InteractButtonClick()
    {
        if (index != -1)
        {
            if (!isSelling)
            {
                //can we afford it
                if (CanAfford(itemStore.GetInventory().GetItem(index)))
                {
                    int value= (int)(itemStore.GetInventory().GetItem(index).GetValue()*itemStore.GetRatio() * (itemStore.IsUsingCredits() ? 10 : 1));
                    Debug.Log("value " + value);
                    //subtract item from inventory of the shop
                    Item item = itemStore.GetInventory().RemoveItem(index);
                    //add item to player inventory
                    player.GetInventory().AddItem(item);
                    //subtract player monies
                    if (itemStore.IsUsingCredits())
                    {
                        player.GetInventory().SetCredits(player.GetInventory().GetCredits() - value);
                    }
                    else
                    {
                        player.GetInventory().SetGold(player.GetInventory().GetGold() - value);
                    }
                    if (index >= itemStore.GetInventory().GetItemCount())
                    {
                        index = -1;
                    }
                    else
                    {
                        ShopCallback(index);
                    }
                    BuildUI();
                }
            }
            else
            {
                int value = GetSellValue(player.GetInventory().GetItem(index));// * (1.0F/itemStore.GetRatio()) * (itemStore.IsUsingCredits() ? 10 : 1));
                Item item = player.GetInventory().RemoveItem(index);
                if (itemStore.IsUsingCredits())
                {
                    player.GetInventory().SetCredits(player.GetInventory().GetCredits() + value);
                }
                else
                {
                    player.GetInventory().SetGold(player.GetInventory().GetGold() + value);
                }
                itemStore.GetInventory().AddItem(item);
                if (index >= player.GetInventory().GetItemCount())
                {
                    index = -1;
                }
                else
                {
                    inventoryCallback(index);
                }
                BuildUI();
            }
        }

    }

    private bool CanAfford(Item item)
    {
        int money = itemStore.IsUsingCredits() ? player.GetInventory().GetCredits() : player.GetInventory().GetGold();
        int amount = (int)( item.GetValue() * (itemStore.IsUsingCredits() ? 10 : 1) * (itemStore.GetRatio()));
        Debug.Log("money" + money + " cost " + amount);
        return (money >= amount);
    }

    public void ExitButtonClick()
    {
        if (view != null)
        {
            view.SetScreen(ScreenID.NONE);
        }
        if (solarSceneScript!= null)
        {
            solarSceneScript.SetScreen(null);
        }
    }

    public void SetShop(ScreenDataShop screenData)
    {
        this.itemStore = (ItemStore)screenData.GetStore();
    }

    public void SetPlayer(ZoneActor player)
    {
        this.player = (Player)player.actor;
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    public void SetView(ViewScene viewScene)
    {
        this.view = viewScene;
    }

    public void BuildUI()
    {
        //build the shop inventory list
        List<string> shopList = new List<string>();
        List<int> shopIcons = new List<int>();
        for (int i = 0; i < itemStore.GetInventory().GetItemCount(); i++)
        {
            shopList.Add(itemStore.GetInventory().GetItem(i).GetItemString()+GetValue(itemStore.GetInventory().GetItem(i),false));
            shopIcons.Add(itemStore.GetInventory().GetItem(i).GetDef().GetIcon());
        }
        this.shopInventory.SetList(shopList);
        this.shopInventory.SetIcons(shopIcons);
        //build the player inventory list
        List<string> inventoryList = new List<string>();
        List<int> inventoryIcons = new List<int>();
        for (int i = 0; i < player.GetInventory().GetItemCount(); i++)
        {
            inventoryList.Add(player.GetInventory().GetItem(i).GetItemString() + GetValue(player.GetInventory().GetItem(i), true));
            inventoryIcons.Add(player.GetInventory().GetItem(i).GetDef().GetIcon());
        }
        this.playerInventory.SetList(inventoryList);
        this.playerInventory.SetIcons(inventoryIcons);
        moneyText.text = (itemStore.IsUsingCredits() ? "C:" : "G:") + 
            (itemStore.IsUsingCredits() ? player.GetInventory().GetCredits() : player.GetInventory().GetGold());
        weightText.text = "weight:" + player.GetInventory().GetWeight();
    }

    private int GetSellValue(Item item)
    {
        float ratio = itemStore.GetRatio();
        int value = itemStore.GetSpecialPrices().ContainsKey(item.GetDef().GetCodeName()) ?
            (int)itemStore.GetSpecialPrices()[item.GetDef().GetCodeName()]
            : (int)(item.GetDef().GetValue() * (1.0F/ratio) * (itemStore.IsUsingCredits() ? 10 : 1));

        return value;
    }

    private string GetValue(Item item, bool buying)
    {
        float ratio = buying ? 1/itemStore.GetRatio()  : itemStore.GetRatio();
        int value = itemStore.GetSpecialPrices().ContainsKey(item.GetDef().GetCodeName()) ?
            (int)itemStore.GetSpecialPrices()[item.GetDef().GetCodeName()]
            : (int)(item.GetDef().GetValue() * ratio * (itemStore.IsUsingCredits() ? 10 : 1));

        return (itemStore.IsUsingCredits() ? " C" : " G") + value.ToString();
    }
}
