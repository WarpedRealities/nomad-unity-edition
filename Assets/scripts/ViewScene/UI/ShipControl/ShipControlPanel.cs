﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Text;

public class ShipControlPanel : MonoBehaviour
{
    public Switch launchSwitch;
    public Text shipDescription;
    public Image readyImage;
    public RectTransform scrollArea;
    public Sprite[] readySprites;
    public Image sosButtonImg;
    public Sprite[] sosSprites;
    public GameObject passTime;
    public GameObject sosMessage;    
    ShipAnalysisTool tool;
    public Spinner spinner;
    private ViewInterface view;
    private const string NEWLINE= "\n";
    private float clock;
    private bool sosDisabled=false;
    // Use this for initialization
    void Start()
    {
        launchSwitch.activate = LaunchClick;
        tool = new ShipAnalysisTool(GlobalGameState.GetInstance().getCurrentEntity().
            GetShip(GlobalGameState.GetInstance().getCurrentZone().getName()));
        tool.ProcessPlayer(GlobalGameState.GetInstance().GetPlayer());
        WriteDescription();
        GlobalGameState.GetInstance().SetCurrentShip(tool.GetShip().getName());
        passTime.SetActive(tool.GetShip().GetWarpHandler() != null || tool.GetShip().GetState() == ShipState.DISTRESS);
    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    public void SetPlayer(ZoneActor zoneActor)
    {

    }

    public void WriteDescription()
    {
        System.Text.StringBuilder stringBuilder=new System.Text.StringBuilder();
        stringBuilder.Append("spaceship hull: "+tool.GetCommon().GetHp()+" / "+tool.GetCommon().GetMaxHp()+ NEWLINE);

        BuildStatDescription(stringBuilder);

        BuildResourceDescription(stringBuilder);

        BuildMagazineDescription(stringBuilder);

        BuildWeaponDescription(stringBuilder);

        if (!tool.isLaunchCapable())
        {
            stringBuilder.Append(tool.GetLaunchRestriction()+NEWLINE);

        }   else
        {
            ShipState state= tool.GetShip().GetState();
            switch (state)
            {
                case ShipState.LANDED:

                    break;
                case ShipState.SPACE:

                    break;
            }
            if (tool.IsCapacityExceeded())
            {
                stringBuilder.Append("capacity exceeded, excess crew will be disembarked on launch" + NEWLINE);
            }
            if (tool.IsTransitEnd())
            {
                stringBuilder.Append("FTL journey complete, ready to return to real space" + NEWLINE);
            }

        }

        shipDescription.text = stringBuilder.ToString();
        shipDescription.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, shipDescription.preferredHeight+32);
        scrollArea.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, shipDescription.preferredHeight+32);
        readyImage.sprite = tool.isLaunchCapable() ? readySprites[1] : readySprites[0];
    }

    private void BuildWeaponDescription(StringBuilder stringBuilder)
    {
        if (this.tool.GetWeapons().Count > 0)
        {
            stringBuilder.Append("WEAPONS");
            stringBuilder.Append(NEWLINE);
        }
        for (int i = 0; i < tool.GetWeapons().Count; i++)
        {
            stringBuilder.Append(tool.GetWeapons()[i].GetName() + " facing:" + EnumTools.FacingToStr(tool.GetWeapons()[i].GetFacing()) + " arc:" + EnumTools.ArcToStr(tool.GetWeapons()[i].GetWeapon().GetArc()) + NEWLINE);
            stringBuilder.Append(tool.GetWeapons()[i].GetWeapon().GetDescription() + NEWLINE);
            stringBuilder.Append(tool.GetWeapons()[i].GetWeapon().GetEffect() + NEWLINE);
            stringBuilder.Append("cost:" + tool.GetWeapons()[i].GetWeapon().GetCosts() + NEWLINE);
        }
    }

    private void BuildMagazineDescription(StringBuilder stringBuilder)
    {
        if (this.tool.GetResources().GetMagazineList().Count > 0)
        {
            stringBuilder.Append("MUNITIONS");
            stringBuilder.Append(NEWLINE);
        }
        for (int i = 0; i < this.tool.GetResources().GetMagazineList().Count; i++)
        {
            stringBuilder.Append(this.tool.GetResources().GetMagazineList()[i].amount.ToString()+" ");
            stringBuilder.Append(GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().
                GetDefByCode(this.tool.GetResources().GetMagazineList()[i].containedItem).GetName());
            stringBuilder.Append(NEWLINE);
        }

    }

    private void BuildStatDescription(StringBuilder stringBuilder)
    {
        stringBuilder.Append("sensor:" + this.tool.GetSolarStats().GetSensors());
        stringBuilder.Append(" detectability:" + this.tool.GetSolarStats().GetDetection());
        stringBuilder.Append(" fuel cost:" + this.tool.GetSolarStats().GetEfficiency());
        stringBuilder.Append(" cruise speed:" + this.tool.GetSolarStats().GetCruise());
        stringBuilder.Append(" capacity:" + this.tool.GetSolarStats().GetCapacity());
        if (this.tool.GetSolarStats().GetFTL() > 0)
        {
            stringBuilder.Append(" ftl:" + this.tool.GetSolarStats().GetFTL());
        }
        stringBuilder.Append(NEWLINE);
        stringBuilder.Append(" combat speed:" + this.tool.GetCombatStats().GetSpeed());
        stringBuilder.Append(" manouverability:" + this.tool.GetCombatStats().GetManouver());
        stringBuilder.Append(" armour:" + this.tool.GetCombatStats().GetArmour());
        stringBuilder.Append(" countermeasures:" + this.tool.GetCombatStats().GetCounterMeasures());
        stringBuilder.Append(NEWLINE);
    }

    private void BuildResourceDescription(StringBuilder stringBuilder)
    {
        for (int i = 0; i < tool.GetResources().GetResourceList().Count; i++)
        {
            ShipResource sr = tool.GetResources().GetResourceList()[i];
            stringBuilder.Append(sr.displayName + ": " + (int)sr.amount + "/" + sr.capacity+NEWLINE);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            view.SetScreen(ScreenID.NONE);
        }
        if (clock > 0)
        {
            clock -= Time.deltaTime;
            if (clock <= 0)
            {
                WriteDescription();
                if (sosDisabled)
                {
                    ActivateSosButton();
                }
            }
        }


    }

    public void LaunchClick()
    {
        if (tool.isLaunchCapable())
        {
            //gonna need to use the analysis tool to feed a launch tool.
            new LaunchLandTool().Launch(tool);
        }
    }

    public void CancelClick()
    {
        view.SetScreen(ScreenID.NONE);
    }

    public void ActivateSosButton()
    {
        sosDisabled = false;
        sosButtonImg.sprite = sosSprites[0];
    }
    public void DistressClick()
    {
        if (!sosDisabled)
        {
            if (tool.GetShip().GetState() == ShipState.DISTRESS)
            {
                sosButtonImg.sprite = sosSprites[0];
                tool.GetShip().SetState(ShipState.SPACE);
                sosMessage.SetActive(false);
                passTime.SetActive(false);
            }
            else if (tool.GetShip().GetState() == ShipState.SPACE)
            {
                sosButtonImg.sprite = sosSprites[1];
                tool.GetShip().SetState(ShipState.DISTRESS);
                sosMessage.SetActive(true);
                passTime.SetActive(true);
                spinner.SetVelocity(0);
                
            }
            else
            {
                sosDisabled = true;
                sosButtonImg.sprite = sosSprites[1];
                clock = 0.2F;
            }
        }
    }

    public void TimeClick()
    {
        if (view.GetPlayerInZone().GetController().CanAct())
        {
            view.GetPlayerInZone().GetController().addAction(new ActionDelay(200*DiceRoller.GetInstance().RollDice(12)));
            spinner.AddVelocity(100);
            clock = 0.5F;
        }
    }
}
