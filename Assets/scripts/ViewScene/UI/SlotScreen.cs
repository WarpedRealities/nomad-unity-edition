﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class SlotScreen : MonoBehaviour
{

    private ScreenDataSlot screenDataSlot;
    private ViewInterface view;
    private ListCallback callback;
    public Player player;
    public ZoneActor playerInZone;
    public NuList listUI;
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        callback = Callback;
        listUI.BroadcastMessage("SetCallback", callback);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            view.SetScreen(ScreenID.NONE);
        }
    }

    public void ExitClick()
    {
        view.SetScreen(ScreenID.NONE);
    }

    public void SetPlayer(ZoneActor zoneActor)
    {
        this.playerInZone = zoneActor;
        this.player = (Player)zoneActor.actor;
        BuildUI();
    }

    public void Callback(int index)
    {
        if (index < 0)
        {
            return;
        }
        if (index < this.player.GetInventory().GetItemCount())
        {
            if (player.GetInventory().GetItem(index).GetDef().GetItemType() == ItemType.COMPONENT)
            {
                AttemptInstallation(player.GetInventory().GetItem(index));
            }
        }
    }

    public void UpdateScreen()
    {

    }

    private void AttemptInstallation(Item item)
    {
        ItemComponent itemDef = (ItemComponent)item.GetDef();
        if (InstallationCheck(itemDef))
        {
            //remove item from player inventory
            player.GetInventory().RemoveItem(item);
            //add widget to slot
            screenDataSlot.GetSlot().AddComponent(itemDef);
            //redraw the widget grid
            view.CalculateVision();
            //add player delay 20
            playerInZone.GetController().addAction(new ActionDelay(20));
            //close
            view.SetScreen(ScreenID.NONE);
        }
        else
        {
            text.text = BuildText();

            switch (itemDef.GetSlotType())
            {
                case SLOTTYPE.DRIVE:
                    text.text = text.text + "\n module is a drive, requires drive slot"; 
                    break;
                case SLOTTYPE.SUPPORT:
                    text.text = text.text + "\n module is a support, requires support slot";
                    break;
                case SLOTTYPE.WEAPON:
                    text.text = text.text + "\n module is a weapon, requires weapon mount";
                    break;
            }
        }
    }

    private bool InstallationCheck(ItemComponent component)
    {
        switch (component.GetSlotType())
        {
            case SLOTTYPE.NORMAL:
                return true;
            case SLOTTYPE.DRIVE:
                return screenDataSlot.GetSlot().GetSlotType() == SLOTTYPE.DRIVE;
            case SLOTTYPE.SUPPORT:
                return screenDataSlot.GetSlot().GetSlotType() == SLOTTYPE.SUPPORT;
            case SLOTTYPE.WEAPON:
                return screenDataSlot.GetSlot().GetSlotType() == SLOTTYPE.WEAPON && 
                    screenDataSlot.GetSlot().GetParams().GetWeaponSize() >= component.GetSize();
        }
        return false;
    }

    private void BuildUI()
    {
        //set the text based on the type of slot
        text.text = BuildText();

        //show inventory on the panel to the right
        List<string> playerList = new List<string>();
        for (int i = 0; i < player.GetInventory().GetItems().Count; i++)
        {
            playerList.Add(player.GetInventory().GetItems()[i].GetItemString());
        }
        listUI.BroadcastMessage("SetList", playerList);
    }

    private string BuildText()
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.Append("Select component to add to this \n");

        switch (screenDataSlot.GetSlot().GetSlotType())
        {
            case SLOTTYPE.NORMAL:
                stringBuilder.Append(" module slot");
                break;
            case SLOTTYPE.DRIVE:
                stringBuilder.Append(" drive slot");
                break;
            case SLOTTYPE.SUPPORT:
                stringBuilder.Append(" support slot");
                break;
            case SLOTTYPE.WEAPON:
                BuildMountParams(stringBuilder);
                break;
        }

        return stringBuilder.ToString();
    }

    private void BuildMountParams(StringBuilder stringBuilder)
    {
        MountParameters mountParams = screenDataSlot.GetSlot().GetParams();
        switch (mountParams.GetWeaponSize())
        {
            case SHIPWEAPONSIZE.LIGHT:
                stringBuilder.Append("light mount facing:"+
                    WidgetSlot.BuildFacing(mountParams.GetFacing())+
                    "\n with arc:"+WidgetSlot.BuildFiringArc(mountParams.GetArc()));
                break;

            case SHIPWEAPONSIZE.MEDIUM:
                stringBuilder.Append("medium mount facing:" +
                    WidgetSlot.BuildFacing(mountParams.GetFacing()) +
                    "\n with arc:" + WidgetSlot.BuildFiringArc(mountParams.GetArc()));
                break;

            case SHIPWEAPONSIZE.HEAVY:
                stringBuilder.Append("heavy mount facing:" +
                    WidgetSlot.BuildFacing(mountParams.GetFacing()) +
                    "\n with arc:" + WidgetSlot.BuildFiringArc(mountParams.GetArc()));
                break;
        }
    }

    public void SetSlot(ScreenDataSlot screenDataSlot)
    {
        this.screenDataSlot = screenDataSlot;
    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }
}
