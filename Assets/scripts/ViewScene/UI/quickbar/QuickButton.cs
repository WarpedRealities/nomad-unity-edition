using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class QuickButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Color[] colours;
    public int index;
    private Image image;
    ListCallback mouseOverCallback;

    public void SetCallback(ListCallback callback)
    {
        this.mouseOverCallback = callback;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseOverCallback(index);
           
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseOverCallback(-1);
    }



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void UpdateUI(Quickslot quickslot)
    {
        if (image == null)
        {
            image = gameObject.transform.GetChild(0).GetComponent<Image>();
        }
        image.enabled = quickslot != null;
        if (quickslot == null)
        {
            return;
        }
        image.sprite = IconLibrary.GetInstance().GetSprite(quickslot.icon);
        image.color = quickslot.disabled ? colours[1] : colours[0];
    }
}
