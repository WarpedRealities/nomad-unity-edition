using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using TMPro;
using UnityEngine;

public class Quickbar : MonoBehaviour
{
    public TextMeshProUGUI descriptionText;
    public QuickButton[] quickButton;
    private QuickslotHandler quickslotHandler;
    private ListCallback mouseOverCallback;
    private ViewInterface view;

    // Start is called before the first frame update
    void Start()
    {
        descriptionText.enabled = false;
    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    public void SetQuickSlotHandler(QuickslotHandler quickslotHandler)
    {
        mouseOverCallback = MouseOver;
        this.quickslotHandler = quickslotHandler;
        for (int i=0; i<quickButton.Length; i++)
        {
            quickButton[i].SetCallback(mouseOverCallback);
        }
    }

    public void MouseOver(int index)
    {
        descriptionText.enabled = TryQuickText(index);
    }

    private bool TryQuickText(int index)
    {
        if (index == -1 || quickslotHandler.GetSlot(index) == null)
        {
            return false;
        }
        
        descriptionText.text = quickslotHandler.GetSlot(index).displayName;
        descriptionText.transform.position = new Vector3(Input.mousePosition.x+2,Input.mousePosition.y+2);
        return true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActivateQuickslot(int index)
    {
        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            view.SetScreen(new ScreenDataQuickslot(index));
        }
        else if (quickslotHandler.GetSlot(index)!=null && !quickslotHandler.GetSlot(index).disabled)
        {
            if (quickslotHandler.GetSlot(index).isMove)
            {
                ActivateMove(quickslotHandler.GetSlot(index));
            }
            else
            {
                ActivateItem(quickslotHandler.GetSlot(index));
            }
        }
    }

    private void ActivateItem(Quickslot quickslot)
    {
        ItemDef itemDef = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(quickslot.target);
        switch (itemDef.GetItemUse()) {
            case ItemUse.USE:
                ActivateUsable(itemDef);
                break;
            case ItemUse.EQUIP:
                ActivateEquip(itemDef);
                break;
        }
    }

    private void ActivateEquip(ItemDef itemDef)
    {
        Inventory inventory = ((Player)view.GetPlayerInZone().actor).GetInventory();
        //check if the item is equipped
        for (int i = 0; i < 4; i++)
        {
            if (inventory.GetSlot(i)!=null && inventory.GetSlot(i).GetDef().GetCodeName().Equals(itemDef.GetCodeName()))
            {
                view.GetPlayerInZone()
                    .GetController().addAction(
                    new ActionInventory(inventory.GetSlot(i), InventoryAction.UNEQUIP, 10));
                return;
            }
        }
        Item item = inventory.GetItem(itemDef.GetCodeName());
        if (item != null)
        {
            SwapItemCheck((ItemEquippable)item.GetDef());
            view.GetPlayerInZone()
            .GetController().addAction(
            new ActionInventory(item, InventoryAction.EQUIP, 10));
        }
    }

    private void SwapItemCheck(ItemEquippable itemEquippable)
    {
        EquipSlot slot = itemEquippable.GetSlot();
        Inventory inventory = ((Player)view.GetPlayerInZone().actor).GetInventory();
        if (inventory.GetSlot((int)slot) != null)
        {
            view.GetPlayerInZone().GetController().addAction(new ActionInventory(inventory.GetSlot((int)slot), InventoryAction.UNEQUIP, 10));
        }
    }

    private void ActivateUsable(ItemDef itemDef)
    {
        Item item = ((Player)view.GetPlayerInZone().actor).GetInventory().GetItem(itemDef.GetCodeName());
        if (item != null)
        {
            view.GetPlayerInZone().GetController().addAction(
                new ActionInventory(item, InventoryAction.USE, 10));
        }
    }

    private void ActivateMove(Quickslot quickslot)
    {
        CombatMove move = GetMove((PlayerRPG)view.GetPlayerInZone().actor.GetRPG(), quickslot.target);
        if (move == ((PlayerRPG)view.GetPlayerInZone().actor.GetRPG()).GetCurrentMove())
        {
            move = ((PlayerRPG)view.GetPlayerInZone().actor.GetRPG()).GetMoveCategory(0)[0];
            ((PlayerRPG)view.GetPlayerInZone().actor.GetRPG()).SetCurrentMove(move);
        }
        else
        {
            ((PlayerRPG)view.GetPlayerInZone().actor.GetRPG()).SetCurrentMove(move);
        }
        view.Redraw();
    }

    private CombatMove GetMove(PlayerRPG playerRPG, string target)
    {
        for (int i=0;i<4; i++)
        {
            List<CombatMove> moves = playerRPG.GetMoveCategory(i);
            for (int j = 0; j < moves.Count; j++)
            {
                if (moves[j].GetName().Equals(target))
                {
                    return moves[j];
                }
            }
        }
        return null;
    }

    internal void UpdateUI()
    {
        for (int i = 0; i < 6; i++)
        {
            quickButton[i].UpdateUI(quickslotHandler.GetSlot(i));
        }
    }
}
