using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusIconBox : MonoBehaviour
{
    Player player;
    public Image[] icons;
    private Dictionary<string, Sprite> spriteLibrary=new Dictionary<string, Sprite>();
    // Start is called before the first frame update
    void Start()
    {
        Sprite[] all = Resources.LoadAll<Sprite>(StringConstants.ICONS);
        for (int i = 0; i < all.Length; i++)
        {
            spriteLibrary.Add(all[i].name, all[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void SetPlayer(Player player)
    {
        this.player = player;
    }

    public void Rebuild()
    {
        StatusEffectHandler handler = player.GetRPG().GetStatusHandler();
        StatusEffect[] effects = handler.GetEffectIcons();
        for (int i = 0; i < effects.Length; i++)
        {           
            icons[i].gameObject.SetActive(effects[i] != null);
            if (effects[i] != null)
            {
                icons[i].gameObject.SetActive(effects[i].GetIcon()!=-1);
                if (effects[i].GetIcon() != -1)
                {
                    icons[i].sprite = LoadImage(effects[i].GetIcon());
                }
            }
        }
    }

    private Sprite LoadImage(int index)
    {
        string buildString = StringConstants.ICONPREFX + index.ToString();    
        if (spriteLibrary.ContainsKey(buildString))
        {
            return spriteLibrary[buildString];
        }
        return null;
    }
}
