using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ClockDisplay : MonoBehaviour
{

    TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        if (!Config.GetInstance().IsShowClock())
        {
            Destroy(gameObject);
        }
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = GlobalGameState.GetInstance().GetUniverse().GetClock().ToString();
    }
}
