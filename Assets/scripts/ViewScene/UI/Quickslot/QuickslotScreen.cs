using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickslotScreen : MonoBehaviour
{
    public NuList list;
    private ViewInterface view;
    private Player player;
    private ScreenDataQuickslot screenData;
    private ListCallback listCallback;
    private List<CombatMove> moves;
    private List<Item> items;
    private int split;

    // Start is called before the first frame update
    void Start()
    {
        listCallback = ListCallback;
        list.SetCallback(listCallback);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ExitClick()
    {
        view.SetScreen(ScreenID.NONE);
    }

    private void ListCallback(int index)
    {
        if (index < split)
        {
            player.GetQuickSlotHandler().SetSlot(this.screenData.GetIndex(), BuildQuickSlotMove(moves[index]));
        }
        else
        {
            player.GetQuickSlotHandler().SetSlot(this.screenData.GetIndex(), BuildQuickSlotItem(items[index-split]));
        }
        view.Redraw();
        ExitClick();
    }

    private Quickslot BuildQuickSlotItem(Item item)
    {
        return new Quickslot(item.GetDef().GetCodeName(), item.GetDef().GetName(), false, item.GetDef().GetIcon());
    }

    private Quickslot BuildQuickSlotMove(CombatMove combatMove)
    {
        return new Quickslot(combatMove.GetName(), combatMove.GetName(), true, combatMove.GetIcon());
    }
    public void SetData(ScreenDataQuickslot data)
    {
        this.screenData = data;
    }
    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    public void SetPlayer(ZoneActor player)
    {

        this.player = (Player)player.actor;
        BuildUI();
    }

    private void BuildUI()
    {
        moves = BuildCombatMoves();
        items = BuildItems();
        List<String> strings = BuildStrings(moves, items);
        this.list.SetList(strings);

    }

    private List<string> BuildStrings(List<CombatMove> moves, List<Item> items)
    {
        List<String> strings = new List<string>();
        for (int i = 0; i < moves.Count; i++)
        {
            strings.Add(moves[i].GetName());
        }
        for (int i = 0; i < items.Count; i++)
        {
            strings.Add(items[i].GetDef().GetName());
        }

        return strings;
    }

    private List<CombatMove> BuildCombatMoves()
    {
        List<CombatMove> moves = new List<CombatMove>();
        for (int i = 0; i < 4; i++)
        {
            List<CombatMove> moveCategory = ((PlayerRPG)player.GetRPG()).GetMoveCategory(i);
            for (int j = 0; j < moveCategory.Count; j++)
            {
                moves.Add(moveCategory[j]);
            }
        }
        this.split = moves.Count;
        return moves;
    }

    private List<Item> BuildItems()
    {
        List<Item> items = new List<Item>();
        for (int i = 0; i < 4; i++)
        {
            if (player.GetInventory().GetSlot(i) != null)
            {
                items.Add(player.GetInventory().GetSlot(i));
            }
        }
        for (int i = 0; i < player.GetInventory().GetItemCount(); i++)
        {
            items.Add(player.GetInventory().GetItem(i));
        }

        return items;
    }
}
