﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MoveSelectPanel : MonoBehaviour
{
    private Text[] textStrings;
    public Button[] buttons;
    private MoveSelectState selectState;
    private Player player;
    private PlayerRPG playerRPG;
    private ZoneActor playerInZone;
    private ViewInterface view;
    private bool doubleTapPrevention=true;

    private void Awake()
    {
        textStrings = new Text[buttons.Length];
        for (int i = 0; i < buttons.Length; i++)
        {
            textStrings[i] = buttons[i].GetComponentInChildren<Text>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        selectState = MoveSelectState.FIRST;

    }

    // Update is called once per frame
    void Update()
    {
        if (doubleTapPrevention)
        {
            if (!Input.anyKey)
                doubleTapPrevention = false;
        }else
        {
            ControlUpdate();
        }
    }

    private void ControlUpdate()
    {
        if (Input.GetKey(KeyCode.Alpha1) || Input.GetKey(KeyCode.Keypad1))
        {
            ControlInput(0);
        }
        if (Input.GetKey(KeyCode.Alpha2) || Input.GetKey(KeyCode.Keypad2))
        {
            ControlInput(1);
        }
        if (Input.GetKey(KeyCode.Alpha3) || Input.GetKey(KeyCode.Keypad3))
        {
            ControlInput(2);
        }
        if (Input.GetKey(KeyCode.Alpha4) || Input.GetKey(KeyCode.Keypad4))
        {
            ControlInput(3);
        }
        if (Input.GetKey(KeyCode.Alpha5) || Input.GetKey(KeyCode.Keypad5))
        {
            ControlInput(4);
        }
        if (Input.GetKey(KeyCode.Alpha6) || Input.GetKey(KeyCode.Keypad6))
        {
            ControlInput(5);
        }
        if (Input.GetKey(KeyCode.Alpha7) || Input.GetKey(KeyCode.Keypad7))
        {
            ControlInput(6);
        }
        if (Input.GetKey(KeyCode.Alpha8) || Input.GetKey(KeyCode.Keypad8))
        {
            ControlInput(7);
        }
        if (Input.GetKey(KeyCode.Alpha9) || Input.GetKey(KeyCode.Keypad9))
        {
            ControlInput(8);
        }
        if (Input.GetKey(KeyCode.Alpha0) || Input.GetKey(KeyCode.Keypad0))
        {
            ControlInput(9);
        }
        if (Input.GetKey(KeyCode.M) || Input.GetKey(KeyCode.Escape))
        {
            //back out
            view.SetScreen(ScreenID.NONE);
        }
    }

    public void ControlInput(int index)
    {
        doubleTapPrevention = true;
        if (selectState==MoveSelectState.FIRST)
        {
            InputMain(index);
        }
        else
        {
            InputSelect(index);
        }
    }

    private void InputSelect(int index)
    {
        List<CombatMove> combatMoves = playerRPG.GetMoveCategory((int)selectState);


        if (index == combatMoves.Count)
        {
            selectState = MoveSelectState.FIRST;
            BuildUI();
        }
        else
        {
            if (index<combatMoves.Count)
            {
                playerRPG.SetCurrentMove(combatMoves[index]);
                view.SetScreen(ScreenID.NONE);
            }
        }
    }

    private void InputMain(int index)
    {
        switch (index)
        {
            case 0:
                if (playerRPG.GetMoveCategory(MoveType.FIGHT).Count > 0)
                {
                    selectState = MoveSelectState.FIGHT;
                    BuildUI();
                }
                break;

            case 1:
                if (playerRPG.GetMoveCategory(MoveType.DOMINATE).Count > 0)
                {
                    selectState = MoveSelectState.DOMINATE;
                    BuildUI();
                }
                break;

            case 2:
                if (playerRPG.GetMoveCategory(MoveType.MOVEMENT).Count > 0)
                {
                    selectState = MoveSelectState.MOVEMENT;
                    BuildUI();
                }
                break;

            case 3:
                if (playerRPG.GetMoveCategory(MoveType.OTHER).Count > 0)
                {
                    selectState = MoveSelectState.OTHER;
                    BuildUI();
                }
                break;

            case 4:
                view.SetScreen(ScreenID.NONE);
                break;


        }
    }

    public void SetPlayer(ZoneActor zoneActor)
    {
        this.playerInZone = zoneActor;
        this.player = (Player)zoneActor.actor;
        this.playerRPG = (PlayerRPG)player.GetRPG();
        selectState = MoveSelectState.FIRST;
        BuildUI();
    }

    private void BuildUI()
    {
        CleanList();
        switch (selectState)
        {
            case MoveSelectState.FIRST:
                BuildFirst();
                break;
            case MoveSelectState.FIGHT:
                BuildList(MoveType.FIGHT);
                break;
            case MoveSelectState.DOMINATE:
                BuildList(MoveType.DOMINATE);
                break;
            case MoveSelectState.MOVEMENT:
                BuildList(MoveType.MOVEMENT);
                break;
            case MoveSelectState.OTHER:
                BuildList(MoveType.OTHER);
                break;
        }
    }

    private void BuildFirst()
    {
        textStrings[0].text = "1. FIGHT";
        textStrings[1].text = "2. DOMINATE";
        textStrings[2].text = "3. MOVEMENT";
        textStrings[3].text = "4. OTHER";
        textStrings[4].text = "5. CANCEL";

    }

    private void CleanList()
    {
        for (int i = 0; i < 10; i++)
        {
            textStrings[i].text = "";
        }
    }

    private void BuildList(MoveType moveType)
    {
        List<CombatMove> moves = playerRPG.GetMoveCategory(moveType);
        int count = moves.Count;
        if (count > 9)
        {
            count = 9;
        }
        for (int i = 0; i < count; i++)
        {
            textStrings[i].text = (i+1)+". "+moves[i].GetName();
        }
        textStrings[count].text = (count + 1) + ". back";

    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

}
