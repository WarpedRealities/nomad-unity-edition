using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarBehaviour
{
    public float growth, deviation, frequency, level;
    private float noise;
    internal void Reset()
    {
        level = 0;
        noise = 0;
        growth = DiceRoller.GetInstance().RollOdds();
        frequency = DiceRoller.GetInstance().RollOdds() * 2;
        deviation = frequency * DiceRoller.GetInstance().RollOdds() / 2;
    }

    internal void Update(float power, float deltaTime)
    {
        level = power * growth;
        noise += deltaTime/frequency;
        noise = noise > 6 ? noise - 6: noise;
        float sin = MathF.Sin(noise);
        level += deviation * sin;
        level = level > 1 ? 1 : level;
        level = level < 0 ? 0 : level;
    }
}

public class BusyIndicator : MonoBehaviour
{
    public Image[] bars;
    private BarBehaviour[] behaviours = new BarBehaviour[8];
    ZoneActor player;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void Reset(ZoneActor player)
    {
        this.player = player;
        for (int i = 0; i < 8; i++)
        {
            behaviours[i] = new BarBehaviour();
            behaviours[i].Reset();
        }
    }

    // Update is called once per frame
    void Update()
    {
        float power = (500-player.GetController().GetAction().GetDuration())/500;
        for (int i = 0; i < 8; i++)
        {
            behaviours[i].Update(power, Time.deltaTime);
            bars[i].fillAmount = behaviours[i].level;
        }
    }
}
