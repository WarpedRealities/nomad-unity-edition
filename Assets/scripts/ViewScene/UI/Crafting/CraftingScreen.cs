﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RecipeEntry
{
    public Recipe recipe;
    public bool active;

    public RecipeEntry(Recipe recipe, bool active)
    {
        this.recipe = recipe;
        this.active = active;
    }
}

public class CraftingScreen : MonoBehaviour
{
    ViewInterface view;
    ZoneActor player;
    List<RecipeEntry> recipes;
    Recipe currentRecipe;
    public NuList craftablesList;
    public Text descriptionText, amountText;
    public Text[] ingredientTexts;
    public TextMeshProUGUI techText;
    public Button craftButton;
    public BusyIndicator busyIndicator;
    ListCallback childCallback;
    bool canCraft = false, crafting = false;

    // Start is called before the first frame update
    void Start()
    {
        //descriptionText = transform.Find("DescriptionText").GetComponent<Text>();
        //amountText = transform.Find("ItemAmountText").GetComponent<Text>();
        //craftablesList = (NuList)transform.Find("CraftablesList").GetComponent<NuList>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            CloseWindow();
        }
    }

    void Callback(int index)
    {
        if (!crafting)
        {
            if (recipes[index].active)
            {
                currentRecipe = recipes[index].recipe;
                //set the description block
                descriptionText.text = currentRecipe.GetDescription();
                //set the amount count
                BuildAmountCount(currentRecipe.GetOutput());
                //create the ingredient details
                BuildIngredients(currentRecipe);
            }
            else
            {
                Debug.Log("cant make");
                descriptionText.text = "Raise your technology skills to be able to craft this item";
                for (int i = 0; i < ingredientTexts.Length; i++)
                {
                    ingredientTexts[i].text = "";
                }
            }

        }
    }

    private void BuildIngredients(Recipe currentRecipe)
    {
        canCraft = true;
        for (int i = 0; i < ingredientTexts.Length; i++)
        {
            if (i < currentRecipe.GetIngredients().Count)
            {
                BuildIngredientText(i, currentRecipe.GetIngredients()[i]);
            }
            else
            {
                ingredientTexts[i].text = "";
            }
        }
        Debug.Log("can craft " + canCraft);
        craftButton.interactable = canCraft;
    }

    private void BuildIngredientText(int index, CraftingRequirement craftingRequirement)
    {
        string itemName = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(craftingRequirement.GetItemCode()).GetName();
        
        ingredientTexts[index].text = itemName + " x" + craftingRequirement.GetCount();
        if (CraftableListGenerator.IngredientCheck(((Player)player.actor).GetInventory(), craftingRequirement))
        {
            ingredientTexts[index].color = new Color(1, 1, 1);

        }
        else
        {
            ingredientTexts[index].color = new Color(1, 0, 0);

            canCraft = false;
        }

    }

    private void BuildAmountCount(string outputCode)
    {
        Inventory inventory = ((Player)player.actor).GetInventory();
        int count = 0;
        for (int i = 0; i < inventory.GetItemCount(); i++)
        {
            Item item = inventory.GetItems()[i];
            if (item.GetDef().GetCodeName().Equals(outputCode))
            {
                if (item.GetItemClass() == ItemClass.STACK)
                {
                    ItemStack itemStack = (ItemStack)item;
                    count += itemStack.GetCount();
                }
                else if (item.GetItemClass() == ItemClass.AMMOSTACK)
                {
                    ItemAmmoStack itemStack = (ItemAmmoStack)item;
                    count += itemStack.GetCount();
                }
                else
                {
                    count++;
                }
            }

        }
        amountText.text = "you already have " + count;
    }

    public void SetPlayer(ZoneActor player)
    {
        this.player = player;
        recipes = CraftableListGenerator.BuildList((Player)player.actor, GlobalGameState.GetInstance().GetUniverse().GetCraftingLibrary());

        BuildUI();
        Callback(0);
    }

    public void SetView(ViewInterface viewInterface)
    {
        this.view = viewInterface;
    }

    private void BuildUI()
    {

        List<string> strings = new List<string>();
        bool[] actives = new bool[recipes.Count];
        for (int i = 0; i < recipes.Count; i++)
        {
            strings.Add(recipes[i].recipe.GetName());
            actives[i] = recipes[i].active;
        }
        childCallback = Callback;
        craftablesList.SetCallback(childCallback);
        craftablesList.SetList(strings);
        craftablesList.SetActiveArray(actives);
        techText.text = "Tech Skill:" + player.actor.GetRPG().getSkill(SK.TECH);
    }

    public void CloseWindow()
    {
        view.SetScreen(ScreenID.NONE);
    }

    public void CraftItem()
    {
        if (!crafting)
        {
            player.GetController().addAction(new ActionDelay(500));
            crafting = true;
            busyIndicator.Reset(player);
            busyIndicator.gameObject.SetActive(true);
        } 
    }

    public void UpdateScreen()
    {
        if (this.crafting)
        {
            busyIndicator.gameObject.SetActive(false);
            //subtract raw materials

            //add new item

            CraftingTool.CraftItem(((Player)player.actor).GetInventory(), this.currentRecipe);
            crafting = false;
            BuildUI();
            descriptionText.text = currentRecipe.GetDescription();
            //set the amount count
            BuildAmountCount(currentRecipe.GetOutput());
            //create the ingredient details
            BuildIngredients(currentRecipe);
        }
    }
}