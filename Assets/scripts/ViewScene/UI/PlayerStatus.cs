﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerStatus : MonoBehaviour
{
    public StatusIconBox statusIconBox;
    public ModeIndicator modeIndicator;
    public PowerBar[] powerBars;
    public Quickbar quickbar;
    private Player player;
    public Text[] texts;
    public GameObject box;
    public ViewScene viewScene;


    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        modeIndicator.SetPlayerController(viewScene.GetPlayer());
        quickbar.SetView(viewScene);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Redraw()
    {
        Rebuild();
    }

    private void Rebuild()
    {
        statusIconBox.Rebuild();
        powerBars[0].Set(player.GetRPG().GetStat(ST.HEALTH), player.GetRPG().GetStatMax(ST.HEALTH));
        powerBars[1].Set(player.GetRPG().GetStat(ST.RESOLVE), player.GetRPG().GetStatMax(ST.RESOLVE));
        powerBars[2].Set(player.GetRPG().GetStat(ST.SATIATION), player.GetRPG().GetStatMax(ST.SATIATION));
        powerBars[3].Set(player.GetRPG().GetStat(ST.ACTION), player.GetRPG().GetStatMax(ST.ACTION));
        texts[0].text = "HP: " + player.GetRPG().GetStat(ST.HEALTH) + "/" + player.GetRPG().GetStatMax(ST.HEALTH);
        texts[0].color = BuildColour(ST.HEALTH);
        texts[1].text = "RP: " + player.GetRPG().GetStat(ST.RESOLVE) + "/" + player.GetRPG().GetStatMax(ST.RESOLVE);
        texts[1].color = BuildColour(ST.RESOLVE);
        texts[2].text = "SP: " + player.GetRPG().GetStat(ST.SATIATION) + "/" + player.GetRPG().GetStatMax(ST.SATIATION);
        texts[2].color = BuildColour(ST.SATIATION);
        texts[3].text = "AP: " + player.GetRPG().GetStat(ST.ACTION) + "/" + player.GetRPG().GetStatMax(ST.ACTION);
        texts[3].color = BuildColour(ST.ACTION);
        texts[4].text = BuildHandIndicator();
        texts[5].text = ((PlayerRPG)player.GetRPG()).GetCurrentMove().GetName();
        modeIndicator.Redraw();
        quickbar.UpdateUI();
    }

    public void MoveSelectButton()
    {
        viewScene.SetScreen(ScreenID.MOVESELECT);
    }

    private string BuildHandIndicator()
    {
        if (player.GetInventory().GetSlot(0) != null)
        {
            string str = player.GetInventory().GetSlot(0).GetName();
            if (player.GetInventory().GetSlot(0) is EquipInstance)
            {
                EquipInstance equipInstance = (EquipInstance)player.GetInventory().GetSlot(0);
                if (equipInstance.GetResourceStore() != null)
                {
                    ResourceStoreInstance resource = equipInstance.GetResourceStore();
                    str = str + " " + resource.GetString();
                }
            }
            if (player.GetInventory().GetSlot(0) is ItemStack)
            {
                ItemStack stack = (ItemStack)player.GetInventory().GetSlot(0);
                str = str + " x" + stack.GetCount();
            }
            return str;
        }
        return "none";
    }

    private Color BuildColour(ST stat)
    {
        float ratio = ((float)player.GetRPG().GetStat(stat)) / ((float)player.GetRPG().GetStatMax(stat));
        if (ratio > 0.5F)
        {
            return new Color(1, 1, 1);
        }
        if (ratio <= 0.25F)
        {
            return new Color(1, 0, 0);
        }
        return new Color(1, 1, 0);
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
        statusIconBox.SetPlayer(player);
        quickbar.SetQuickSlotHandler(player.GetQuickSlotHandler());
        Rebuild();

    }

    public void PositionStatus()
    {
        int left = (int)GetComponent<RectTransform>().sizeDelta.x;
        for (int i = 0; i < texts.Length; i++)
        {
          //  RectTransform rectT = texts[i].GetComponent<RectTransform>();
          //  rectT.position = new Vector3(-left+160, -32+(32 *-i), 0)+transform.position;

        }
      //  RectTransform rect = box.GetComponent<RectTransform>();
      //  rect.transform.position = transform.position+ new Vector3(-left+8, -240, 0);
    }

    public void ActivateInventory()
    {
        viewScene.SetScreen(ScreenID.INVENTORY);
    }
    public void ActivateCharacter()
    {
        SceneManager.LoadScene("CharacterScene");
    }
    public void ActivateAppearance()
    {
        viewScene.SetScreen(ScreenID.APPEARANCE);
    }
    public void ActivateJournal()
    {

    }
    public void ActivateMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
