﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class UIController : MonoBehaviour
{
    public GameObject playerStatus;
    public Quickbar quickbar;
    private GameObject currentScreen;
    public RightClickUI clickUI;
    private Player player;
    public GameObject panelEdge;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (currentScreen == null) {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                quickbar.ActivateQuickslot(0);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                quickbar.ActivateQuickslot(1);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                quickbar.ActivateQuickslot(2);
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                quickbar.ActivateQuickslot(3);
            }
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                quickbar.ActivateQuickslot(4);
            }
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                quickbar.ActivateQuickslot(5);
            }
        }
    }

    public void RedrawStatus()
    {
        playerStatus.BroadcastMessage("Redraw");
    }

    public void SetScreen(GameObject screen)
    {
        if (currentScreen != null)
        {
            currentScreen.transform.parent = null;
            Destroy(currentScreen);
        }
        if (screen != null)
        {
            currentScreen = screen;
            currentScreen.transform.SetParent(transform,false);

        }
    }

    public void RemoveScreen()
    {
        if (currentScreen != null)
        {
      
            Destroy(currentScreen);
            currentScreen = null;
        }
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
        playerStatus.BroadcastMessage("SetPlayer", player);
    }

    public RightClickUI GetRightClickUI()
    {

        return clickUI;
    }

    public bool EdgeVisible()
    {
        return panelEdge.activeSelf;
    }

    public void SetEdgeVisible(bool value)
    {
        panelEdge.SetActive(value);
    }
}
