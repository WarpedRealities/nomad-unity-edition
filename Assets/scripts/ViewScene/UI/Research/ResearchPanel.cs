using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResearchPanel : MonoBehaviour
{
    bool isPediaMode, studying;
    string studyData;

    public TMPro.TextMeshProUGUI toggleButtonText;
    public Button studyButton;
    public NuList researchList;
    public GameObject descriptionScroll;
    public TMPro.TextMeshProUGUI descriptionText;
    public BusyIndicator busyIndicator;
    ViewInterface view;
    ZoneActor player;
    ListCallback list_delegate;
    ResearchHandler researchHandler;

    // Start is called before the first frame update
    void Start()
    {
        list_delegate = ListCallback;
        this.researchList.SetCallback(list_delegate);
    }

    // Update is called once per frame
    void Update()
    {
        if (!this.studying && Input.GetKeyDown(KeyCode.Escape))
        {
            view.SetScreen(ScreenID.NONE);
        }
    }

    public void SetPlayer(ZoneActor player)
    {
        this.player = player;
        researchHandler = ((Player)player.actor).GetResearchHandler();
        if (researchHandler.GetResearchKeys().Count == 0)
        {
            isPediaMode = true;
            if (researchHandler.GetEntryKeys().Count > 0)
            {
                SetText(researchHandler.PullEntry(researchHandler.GetEntryKeys()[0]));
            }
        }
        BuildUI();
    }

    private void BuildUI()
    {

        if (this.isPediaMode)
        {
            List<string> keys = researchHandler.GetEntryKeys();
            for (int i = 0; i < keys.Count; i++)
            {
                keys[i] = keys[i].Replace("pedia\\", "").Replace(".xml", "");
            }

            researchList.SetList(keys);
            
        }
        else
        {
            List<string> keys = researchHandler.GetResearchKeys();
            Debug.Log("keys " + keys.Count);
            researchList.SetList(keys);
        }
        descriptionScroll.SetActive(this.isPediaMode);
        studyButton.gameObject.SetActive(!this.isPediaMode);
    }
    public void UpdateScreen()
    {
        if (this.studying)
        {
            this.studying = false;
            busyIndicator.gameObject.SetActive(false);
            ResolveResearch();
            BuildUI();
        }
    }

    private void SetText(string str)
    {
        str = str.Replace("\n", " ");
        str = str.Replace("\r", "");
        str = str.Replace("\t", "");
        str = str.Replace("LBREAK", "\n");
        descriptionText.text = str;
        float height = 1000;
        descriptionText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }

    private void ResolveResearch()
    {
        if (researchHandler.ResolveResearch(studyData, (Player)player.actor))
        {
            //handle whether we are unlocking a new entry
            string PediaText = researchHandler.ResolveUnlocks((Player)player.actor);
            if (PediaText != null)
            {
                isPediaMode = true;
                SetText(PediaText);
            }
            view.DrawText("Research has made progress");
        }
        else
        {
            view.DrawText("Research results are inconclusive");
        }
        studyData = null;
    }

    public void SetView(ViewInterface viewInterface)
    {
        this.view = viewInterface;
    }

    public void ListCallback(int index)
    {
        if (this.isPediaMode)
        {
            List<string> entries = researchHandler.GetEntryKeys();
            if (index < entries.Count)
            {
                SetText(researchHandler.PullEntry(entries[index]));
            }
        }
    }

    public void ExitClick()
    {
        if (!this.studying)
        {
            view.SetScreen(ScreenID.NONE);
        }

    }

    public void Toggleclick()
    {

        if (!this.studying)
        {
            this.isPediaMode = !isPediaMode;
            BuildUI();
        }
    }

    public void StudyClick()
    {
        if (!this.studying)
        {
            int index = this.researchList.GetIndex();
            List<string> keys = new List<string>(researchHandler.GetResearchKeys());
            if (index != -1 && index<keys.Count)
            {
                this.studying = true;
                this.studyData = keys[index];
                player.GetController().addAction(new ActionDelay(1000));
                busyIndicator.gameObject.SetActive(true);
                busyIndicator.Reset(player);
            } 
        }
    }
}
