using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JailScreen : MonoBehaviour
{
    private ScreenDataJail jail;
    private ViewInterface view;
    private ZoneActor playerInZone;
    private Player player;
    private int index = 0;

    private ListCallback listCallback;
    public JailIndicators jailIndicators;
    public NuList list;
    public Switch[] switches;
    public DelegateCall[] switchDelegates;

    // Start is called before the first frame update
    void Start()
    {
        listCallback = ListCallback;
        list.SetCallback(listCallback);
        switchDelegates = new DelegateCall[2];
        switchDelegates[0] = ClickOpen;
        switchDelegates[1] = ClickFlush;
        for (int i = 0; i < switches.Length; i++)
        {
            switches[i].activate = switchDelegates[i];
        }
        BuildUI();
    }

    private void ListCallback(int index)
    {
        this.index = index;
        BuildIndicators();
    }

   

    private void BuildUI()
    {
        List<string> strings = new List<string>();
        for (int i = 0; i < jail.captives.Length; i++)
        {
            if (jail.captives[i] != null)
            {
                strings.Add(jail.captives[i].GetName());
            }
            else
            {
                strings.Add("unoccupied");
            }
        }
        list.SetList(strings);
        BuildIndicators();
    }

    private void BuildIndicators()
    {
        int indicators = 0;
        
        //pod occupied check
        indicators += (index >= 0 && index < jail.captives.Length && jail.captives[index] != null) ?
            1 : 0;
        //flush enabled check
        Spaceship ship = GlobalGameState.GetInstance().getCurrentEntity() is Spaceship ? 
            (Spaceship)GlobalGameState.GetInstance().getCurrentEntity() : null;
        if (ship == null)
        {
            ship = CaptureTool.GetDockedShip();
        }
        indicators += (ship != null && ship.GetState().Equals(ShipState.SPACE)) ? 2 : 0;
        //capture device check
        Item item = player.GetInventory().GetItem("00CAPTURE");
        ItemCaptureInstance captureDevice = item is ItemCaptureInstance ? 
            (ItemCaptureInstance)item: null;
        indicators += captureDevice != null ? 4 : 0;
        if (captureDevice != null)
        {
            //sync check
            indicators += captureDevice.GetShipName() != null ? 8 : 0;
            if (ship != null)
            {
                //with this check
                indicators += ship.getName().Equals(captureDevice.GetShipName()) ? 16 : 0;
            }

        }
        jailIndicators.SetValues(indicators);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetData(ScreenDataJail jail)
    {
        this.jail = jail;
    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    public void SetPlayer(ZoneActor zoneActor)
    {
        this.playerInZone = zoneActor;
        this.player = (Player)zoneActor.actor;
    }

    public void ClickOpen()
    {
        if (this.index != -1 && this.index < this.jail.captives.Length)
        {
            String conversation = ((NPC_RPG)this.jail.captives[index].
                GetRPG()).
                GetConversationFile((int)conversationType.CAPTIVE);
            this.view.SetScreen(
                new ScreenDataConversation(
                    conversation,
                    this.jail.captives[index].GetFlagField(),
                    new DummyActor(this.jail.captives[index]),
                    this.jail.widgetJail,
                    false
                    ));
        }
    }

    public void ClickFlush()
    {
        if (CanFlush() && this.index!=-1 && this.index<this.jail.captives.Length)
        {
            this.jail.captives[index] = null;
            BuildUI();
        }
    }

    public bool CanFlush()
    {
        Spaceship ship = GlobalGameState.GetInstance().getCurrentEntity() is Spaceship ?
           (Spaceship)GlobalGameState.GetInstance().getCurrentEntity() : null;
        if (ship != null)
        {
            return (ship != null && ship.GetState().Equals(ShipState.SPACE));
        }
        return false;
    }

    public void ClickExit()
    {
        view.SetScreen(ScreenID.NONE);
    }

    public void ClickSync()
    {
        Item item = player.GetInventory().GetItem("00CAPTURE");
        ItemCaptureInstance captureDevice = item is ItemCaptureInstance ?
            (ItemCaptureInstance)item : null;
        Spaceship ship = GlobalGameState.GetInstance().getCurrentEntity() is Spaceship ?
            (Spaceship)GlobalGameState.GetInstance().getCurrentEntity() : null;
        if (ship == null)
        {
            ship = CaptureTool.GetDockedShip();
        }
        if (captureDevice!=null && ship != null)
        {
            captureDevice.SetShipName(ship.getName());
        }

        BuildIndicators();
    }
}
