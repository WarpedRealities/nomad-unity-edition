using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JailIndicators : MonoBehaviour
{
    public Image[] images;
    public Sprite[] sprites;
    internal void SetValues(int indicators)
    {
        images[0].sprite = (indicators & 1) == 0 ? sprites[2] : sprites[0];
        images[1].sprite = (indicators & 1) == 1 ? sprites[1] : sprites[0];
        images[2].sprite = (indicators & 2) == 0 ? sprites[2] : sprites[0];
        images[3].sprite = (indicators & 2) == 2 ? sprites[1] : sprites[0];
        images[4].sprite = (indicators & 4) == 0 ? sprites[2] : sprites[0];
        images[5].sprite = (indicators & 4) == 4 ? sprites[1] : sprites[0];
        images[6].sprite = (indicators & 8) == 0 ? sprites[2] : sprites[0];
        images[7].sprite = (indicators & 8) == 8 ? sprites[1] : sprites[0];
        images[8].sprite = (indicators & 16) == 0 ? sprites[2] : sprites[0];
        images[9].sprite = (indicators & 16) == 16 ? sprites[1] : sprites[0];
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
