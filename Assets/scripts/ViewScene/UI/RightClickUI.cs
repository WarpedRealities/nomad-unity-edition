﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class RightClickUI : MonoBehaviour
{
    float clock;
    Vector2 target;
    public ViewScene viewScene;
    private ClickActionHandler clickActionHandler;
    private RectTransform rectTransform;
    private ViewSceneController sceneController;
    public Button shoveButton;
    public Text shoveSelectText;
    private bool shove;
    // Use this for initialization
    void Start()
    {

        sceneController = viewScene.GetController();
        clock = 0;
        ControlShoveButton();
    }

    // Update is called once per frame
    void Update()
    {


    }

    private void OnGUI()
    {
        Vector2 p = Event.current.mousePosition;
        p.x -= rectTransform.position.x;
        p.y = ((Screen.height * 1.0F) - p.y);
        p.y -= rectTransform.position.y;
        if (!rectTransform.rect.Contains(p))
        {
            gameObject.SetActive(false);
        }
    }

    public void ReceiveRightClick(ClickActionHandler handler, Vector2 position, Vector2 screenPos)
    {
        SetPosition(screenPos);
        this.clickActionHandler = handler;
        this.target = position;
        this.gameObject.SetActive(true);

    }

    private void ControlShoveButton()
    {
        Vector2Int pi = new Vector2Int((int)target.x, (int)target.y);
        float d = Vector2Int.Distance(pi, GlobalGameState.GetInstance().GetPlayer().GetPosition());
        if (d <= 2)
        {
            Tile t = sceneController.GetZoneContents().GetTile(pi.x, pi.y);
            if (t != null && t.GetActorInTile() != null && pi!= GlobalGameState.GetInstance().GetPlayer().GetPosition())
            {
                ZoneActor actor = t.GetActorInTile();
                if (!actor.GetAlive() || actor.GetActor().GetFaction().GetName().Equals("player"))
                {
                    this.shove = true;
                    this.shoveSelectText.text = "shove";
                }
            }
        }
    }

    private void SetPosition(Vector2 screenPos)
    {
        rectTransform = rectTransform==null ? GetComponent<RectTransform>(): rectTransform;
        screenPos.x -= 5;
        screenPos.y += 5;
        rectTransform.anchoredPosition = screenPos;
    }

    public void ButtonCallback(int index)
    {
        Vector2Int pi = new Vector2Int((int)target.x, (int)target.y);
        Tile t = sceneController.GetZoneContents().GetTile(pi.x, pi.y);
        switch (index)
        {
            case 0:
                clickActionHandler.AttackHandler(t, target, viewScene);
                break;
            case 1:
                clickActionHandler.LookHandler(t);
                break;
                
            case 2:
                clickActionHandler.InteractionHandler(t, target);
                break;
            case 3:
                if (shove)
                {
                    Debug.Log("attempt shove");
                    clickActionHandler.ShoveHandler(t, target, viewScene);
                }
                else
                {
                    viewScene.SetScreen(ScreenID.MOVESELECT);
                }
                break;
        }
        gameObject.SetActive(false);
    }
}
