﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PowerBar : MonoBehaviour
{
    public Image bar;



    // Use this for initialization
    void Awake()
    {
        bar = transform.GetChild(0).GetComponent<Image>();
    }

    void Start()
    {

    }

    public void Set(float current, float max)
    {
        if (bar == null)
        {
            bar = transform.GetChild(0).GetComponent<Image>();
        }
        float amount = current / max;
        amount = amount > 1 ? 1 : amount;
        amount = amount < 0 ? 0 : amount;
        bar.fillAmount = amount;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
