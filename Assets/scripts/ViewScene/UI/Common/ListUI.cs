﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public delegate void ListCallback(int index);

public class ListUI : MonoBehaviour
{
    public GameObject leftSibling, rightSibling;
    public int listSize;
    public Vector2Int position;
    private int index,offset,size;
    public List<string> textStrings;
    private Color selected = new Color(0, 1, 0);
    private Color deactivatedHighlight = new Color(0, 0.5F, 0);
    private Color normal = new Color(1, 1, 1);
    private GameObject[] textEntries;
    private float controlClock = 0.25F;
    public bool active, requireRebuild;
    public ListCallback listCallback;
    private readonly float TIME_INTERVAL = 0.1F;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        offset = 0;
        size = textStrings.Count;
      
        Vector2 p = new Vector2(-80, 400);
        textEntries = new GameObject[listSize];
        for (int i = 0; i < listSize; i++)
        {
            textEntries[i] = BuildTextEntry(i, i<textStrings.Count ? textStrings[i]: "", new Vector3(position.x, position.y -(i * 48)));
        }
        HighLightSelect();
    }

    public void SetActive(bool active)
    {
        Debug.Log("set active is " + active);
        this.active = active;
        this.controlClock = 0.5F;
        if (active) {
            HighLightSelect();
        }
    }

    public void SetList(List<string> list)
    {
        controlClock = 0.25F;
        textStrings = list;
        size = textStrings.Count;
        if (index >= textStrings.Count)
        {
            Debug.Log("index is over text strings");
            index = textStrings.Count - 1;
        }
        if (offset >= textStrings.Count)
        {
            offset = textStrings.Count - 1;
        }
        if (offset < 0)
        {
            offset = 0;
        }
        requireRebuild = true;
    }

    public void SetIndex(int index)
    {
        this.index = index;
        HighLightSelect();
    }

    public void SetCallback(ListCallback callback) {
        this.listCallback = callback;
    }

    private void HighLightSelect()
    {
        for (int i = 0; i < listSize; i++)
        {
            if (active)
            {
                textEntries[i].GetComponent<Text>().color = i == index - offset ? selected : normal;
            }
            else
            {
                textEntries[i].GetComponent<Text>().color = i == index - offset ? deactivatedHighlight : normal;
            }
           
        }
    }

    private GameObject BuildTextEntry(int index, string text, Vector3 position)
    {
        GameObject textEntry = new GameObject();
        textEntry.name = "LineEntry" + index;

        textEntry.transform.SetParent(this.transform);
        textEntry.transform.position = position;
        textEntry.AddComponent<Text>().text = text;
        textEntry.GetComponent<Text>().fontSize = 32;
        textEntry.GetComponent<Text>().font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        textEntry.GetComponent<Text>().color = normal;
        textEntry.GetComponent<RectTransform>().sizeDelta = new Vector2(512, 48);
        textEntry.GetComponent<RectTransform>().pivot = new Vector2(0, 0.5F);
        return textEntry;
    }

    // Update is called once per frame
    void Update()
    {
        if (requireRebuild)
        {
            requireRebuild = false;
            Rebuild();
        }

        if (controlClock > 0)
        {
            controlClock -= Time.deltaTime;
        } else if (active)
        {
           
            Controls();
        }
    }


    private void Controls()
    {

        if (Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey(KeyCode.Return))
        {
            listCallback(index);
            controlClock = TIME_INTERVAL;
        }

        if ((Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Keypad8)) && index>0)
        {
            index--;
            if (index < offset)
            {
                offset--;
                Rebuild();
            }
            HighLightSelect();
            controlClock = TIME_INTERVAL;
        }
        if ((Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Keypad2)) && index<size-1) 
        {
            index++;
            if (index >= offset + listSize)
            {
                offset++;
                Rebuild();
            }
            HighLightSelect();
            controlClock = TIME_INTERVAL;
        }
        if ((Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Keypad6)) && rightSibling !=null)
        {
            active = false;
            rightSibling.BroadcastMessage("SetActive", true);
            HighLightSelect();
            controlClock = TIME_INTERVAL;
        }
        if ((Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Keypad4)) && leftSibling != null)
        {
            active = false;
            leftSibling.BroadcastMessage("SetActive", true);
            HighLightSelect();
            controlClock = TIME_INTERVAL;
        }
    }

    private void Rebuild()
    {
      
        for (int i = 0; i < listSize; i++)
        {

            textEntries[i].GetComponent<Text>().text = i+offset < textStrings.Count ? textStrings[i+offset] : "";
        }
        HighLightSelect();

    }
}
