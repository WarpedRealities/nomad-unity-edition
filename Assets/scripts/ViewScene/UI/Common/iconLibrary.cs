
using System.Collections.Generic;
using UnityEngine;

public class IconLibrary {

    private static IconLibrary _instance;
    private Dictionary<string, Sprite> library = new Dictionary<string, Sprite>();
    public static IconLibrary GetInstance()
    {
        if (_instance == null)
        {
            _instance = new IconLibrary();
        }
        return _instance;
    }

    public IconLibrary()
    {
        library = new Dictionary<string, Sprite>();
        Sprite[] all = Resources.LoadAll<Sprite>(StringConstants.QUICKICONS);
        for (int i = 0; i < all.Length; i++)
        {
            library.Add(all[i].name, all[i]);
        }
    }

    public Sprite GetSprite(int index)
    {
        return library[StringConstants.QUICKPREFIX + index.ToString()];
    }
}


