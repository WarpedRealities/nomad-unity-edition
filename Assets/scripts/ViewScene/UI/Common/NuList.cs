﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NuList : Selectable
{
    public GameObject prefabNuList;
    public Selectable left, right, up, down;
    public int xModifier;
    private List<string> strings;
    private List<int> icons;
    private List<NuListEntry> nuListEntries;
    bool[] activeArray;
    private ListCallback childCallback;
    private ListCallback parentCallback;
    private int index = 0;
    private bool updateSelect = false, updateList=false;

    protected override void Start()
    {
        childCallback=ListCallback;

    }

    public void ListCallback(int index)
    {

        this.index = index;
        if (this.parentCallback != null)
        {
            parentCallback(index);
        }

        for (int i = 0; i < nuListEntries.Count; i++)
        {
            if (index == i)
            {
                nuListEntries[i].SetIllumination(true);
            }
            else
            {
                nuListEntries[i].SetIllumination(false);
            }
        }
    }

    public void SetActive(bool active)
    {

    }

    public int GetIndex()
    {
        return index;
    }

    public void SetCallback(ListCallback callback)
    {
        this.parentCallback = callback;

    }

    public void Update()
    {
   
        if (updateSelect && nuListEntries!=null && nuListEntries.Count>0)
        {
            if (index >= nuListEntries.Count)
            {
                index = nuListEntries.Count-1;
            }
            EventSystem.current.SetSelectedGameObject(nuListEntries[index].gameObject,null);
            updateSelect = false;
        }
        if (updateList)
        {
            updateList = false;
            if (Application.isPlaying)
            {
                // updateSelect = AnySelected();
                updateSelect = true;
                SetSize(strings.Count);

                GenerateTextEntries(strings);

                if (nuListEntries.Count > 0)
                {
                    BuildTextLinks();
                }
                
            }
        }
    }

    private bool AnySelected()
    {
        if (nuListEntries == null)
        {
            return false;
        }
        GameObject g = EventSystem.current.currentSelectedGameObject;
        for (int i = 0; i < nuListEntries.Count; i++)
        {
            if (nuListEntries[i].gameObject == g)
            {
                Debug.Log("any selected is true");
                return true;
            }
        }
        return false;
    }

    public void SetList(List<string> list)
    {
        strings = list;
        if (list != null)
        {
            updateList = true;
        }
    }
    public void SetIcons(List<int> list)
    {

        icons = list;
        if (list != null)
        {
            updateList = true;
        }
    }

    public void SetActiveArray(bool [] array)
    {
        this.activeArray = array;
    }

    private void BuildTextLinks()
    {
        if (nuListEntries.Count == 1)
        {
            nuListEntries[0].navigation = BuildNavigation(-2, 0);
            return;
        }
        for (int i = 0; i < nuListEntries.Count; i++)
        {
            if (i == 0)
            {
                nuListEntries[i].navigation = BuildNavigation(-1,i);
            } else if (i == nuListEntries.Count - 1)
            {
                nuListEntries[i].navigation = BuildNavigation(1,i);
            }
            else
            {
                nuListEntries[i].navigation = BuildNavigation(0,i);
            }
        }
    }

    private Navigation BuildNavigation(int position, int index)
    {
        Navigation navigation = new Navigation();
       
        navigation.mode = Navigation.Mode.Explicit;
        navigation.selectOnLeft = left;
        navigation.selectOnRight = right;
        navigation.selectOnUp = (position == -1 || position == -2)? up : nuListEntries[index - 1];
        navigation.selectOnDown = (position == 1 || position == -2)? up : nuListEntries[index + 1];

        return navigation;
    }

    private void GenerateTextEntries(List<string> list)
    {
        Transform contentWindow = transform.Find("Viewport").Find("Content");
        foreach (Transform child in contentWindow)
        {
            GameObject.Destroy(child.gameObject);
        }
        nuListEntries = new List<NuListEntry>();
        if (childCallback == null)
        {
            childCallback = ListCallback;
        }
        RectTransform contents = contentWindow.GetComponent<RectTransform>();
        Transform parent = transform.Find("Viewport").Find("Content");
        float xEdge = this.transform.position.x+ xModifier;
        float topY = -24+parent.position.y;
        //now we have the height of the window to try and offset the y of the list entries properly

        for (int i = 0; i < list.Count; i++)
        {
            NuListEntry listEntry = ((GameObject)Instantiate(
                prefabNuList != null ? prefabNuList : Resources.Load("NuListEntry"),parent)).GetComponent<NuListEntry>();

            listEntry.transform.localPosition = new Vector3( 8, (-16) - (i * 28));
            //listEntry.transform.SetParent(parent, true);
            listEntry.SetText(list[i]);
            nuListEntries.Add(listEntry);
            if (activeArray!=null && i < activeArray.Length)
            {
                if (!activeArray[i])
                {
                    listEntry.SetDisabled(true);
                }
            }
            listEntry.SetCallback(childCallback, i);
            if (i==this.index)
            {
                listEntry.SetIllumination(true);
            }

            if (icons != null)
            {

                Image image = listEntry.transform.Find("Icon").GetComponent<Image>();
                Sprite sprite = IconLibrary.GetInstance().GetSprite(icons[i]);


                image.sprite = sprite;

            }
        }
    }

    private void SetSize(int entryCount)
    {
        RectTransform window = GetComponent<RectTransform>();
        RectTransform contents = transform.Find("Viewport").Find("Content").GetComponent<RectTransform>();
        float hw = window.sizeDelta.y;
        float hc = contents.sizeDelta.y;
        float hsize = entryCount * 30;
        float hnew = hw > hsize ? hw : hsize;
        contents.sizeDelta = new Vector2(contents.sizeDelta.x, hnew);
    }

    public override void OnSelect(BaseEventData baseEventData)
    {
        updateSelect = true;
    }

}
