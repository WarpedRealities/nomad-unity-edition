﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NuListEntry : Selectable, IPointerClickHandler, ISubmitHandler
{
    ListCallback listCallback;
    int index;
    bool illuminated, disabled;
    public ColorBlock illuminatedColours;
    private ColorBlock normalColours;
    public TextMeshProUGUI text;
    public Text oldText;

    protected override void Start()
    {
        normalColours = this.colors;

    }

    public void SetCallback(ListCallback listCallback,int index)
    {
        this.listCallback = listCallback;
        this.index = index;
    }

    public bool GetHighlight()
    {
        return this.IsHighlighted();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Submit();
    }

    public void OnSubmit(BaseEventData eventData)
    {
        Submit();
    }

    private void Submit()
    {
        listCallback(index);

    }

    public void SetIllumination(bool illumination)
    {
        if (this.text != null)
        {
            this.GetComponent<NuListMono>().SetHighlight(illumination, disabled);
        }
        else
        {
            this.GetComponent<Text>().color = illumination ? new Color(1, 1, 0) : GetColor();
        }
        this.colors = illumination ? illuminatedColours : normalColours;
    }

    private Color GetColor()
    {
        return disabled ? new Color(0.5F, 0.5F, 0.5F) : new Color(1, 1, 1);
    }

    internal void SetText(string text)
    {
        if (this.text != null)
        {
            this.text.text = text;
        }
        else
        {
            GetComponent<Text>().text = text;
        }
    }

    internal void SetDisabled(bool disabled)
    {
        this.disabled = disabled;

        if (this.text != null)
        {
            this.text.color = new Color(0.5F, 0.5F, 0.5F);
        }
        else
        {
            GetComponent<Text>().color = new Color(0.5F, 0.5F, 0.5F);
        }
    }
}
