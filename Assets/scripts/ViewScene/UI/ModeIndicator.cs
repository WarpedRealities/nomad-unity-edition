using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeIndicator : MonoBehaviour
{

    public Sprite[] sprites;
    private Image image;
    private PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Redraw()
    {
        if (playerController != null)
        {
            image.sprite = sprites[(int)playerController.GetControlMode()];
        }
    }

    internal void SetPlayerController(PlayerController playerController)
    {
        this.playerController = playerController;
    }
}
