﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CanvasClickManager : MonoBehaviour, IPointerClickHandler
{
    public ClickActionHandler actionHandler;
    private CanvasCamera canvasCamera;
    private GameObject cameraRig;
    private RectTransform rectTransform;
    private const float pixelWidth = 32;

    // Use this for initialization
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasCamera = GetComponent<CanvasCamera>();
        cameraRig = GameObject.Find("CameraRig");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
       
        Vector2 p0 = new Vector2(eventData.position.x-transform.position.x- (rectTransform.rect.width/2), 
            eventData.position.y-transform.position.y+(rectTransform.rect.height/2));
        Vector2 p1 = GetClickPosition(p0);
        Vector2 p2 = new Vector2(eventData.position.x,
            eventData.position.y-transform.position.y);
        switch (eventData.button)
        {
            case PointerEventData.InputButton.Left:
                actionHandler.LeftClick(p1);
                break;
            case PointerEventData.InputButton.Right:
                actionHandler.RightClick(p1,p2);
                break;
        }
    }
    public float GetPixelWidth()
    {
        return pixelWidth * canvasCamera.GetScaleOut();
    }
    public Vector2 GetClickPosition(Vector2 position)
    {
        float x = position.x/(canvasCamera.IsZoom() ? GetPixelWidth() / 2 : GetPixelWidth());
        float y = position.y/(canvasCamera.IsZoom() ? GetPixelWidth() / 2 : GetPixelWidth());

        x = x + cameraRig.transform.position.x;
        y = y + cameraRig.transform.position.y;

        return new Vector2(x, y);
    }
}
