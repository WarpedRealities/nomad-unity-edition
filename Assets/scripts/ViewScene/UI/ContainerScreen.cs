﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ContainerScreen : MonoBehaviour
{

    public ScreenDataContainer screenDataContainer;
    public Player player;
    public ZoneActor playerInZone;
    public NuList leftList, rightList;
    public Text leftWeight, rightWeight, oneManyText;
    bool many = false;
    private ListCallback leftCallback, rightCallback;
    private ViewInterface view;
  
    // Start is called before the first frame update
    void Start()
    {

        BuildUI();
        leftCallback = CallbackLeft;
        rightCallback = CallbackRight;
        leftList.SetCallback(leftCallback);
        rightList.SetCallback(rightCallback);
        EventSystem.current.SetSelectedGameObject(leftList.gameObject);
    }

    public void SetPlayer(ZoneActor zoneActor)
    {
        this.playerInZone = zoneActor;
        this.player = (Player)zoneActor.actor;

    }

    public void SetContainer(ScreenDataContainer screenDataContainer)
    {
        
        this.screenDataContainer = screenDataContainer;
    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    public void CallbackLeft(int index)
    {
        if (index < 0)
        {
            return;
        }

        if (screenDataContainer.containerData.GetItem(index) != null)
        {
            Item it = screenDataContainer.containerData.GetItem(index);
            if ((it.GetItemClass().Equals(ItemClass.STACK) || it.GetItemClass().Equals(ItemClass.AMMOSTACK)) && GetMany())
            {
                int m = MaxCount(it);
                int c = m < 10 ? m : 10;
                for (int i = 0; i < c; i++)
                {
                    this.playerInZone.GetController().addAction(new ActionContainer(this.screenDataContainer.containerData.GetItem(index), this.screenDataContainer.containerData, false));
                }
            }
            else
            {
                this.playerInZone.GetController().addAction(new ActionContainer(this.screenDataContainer.containerData.GetItem(index), this.screenDataContainer.containerData, false));
            }
        }
    }

    public void CallbackRight(int index)
    {
        if (index < 0)
        {
            return;
        }
        if (player.GetInventory().GetItem(index) != null)
        {
            Item it = player.GetInventory().GetItem(index);
            if (CanAdd(this.player.GetInventory().GetItem(index)) && (it.GetItemClass().Equals(ItemClass.STACK) || it.GetItemClass().Equals(ItemClass.AMMOSTACK)) && GetMany())
            {
                int m = MaxCount(it);
                int c = m < 10 ? m : 10;
                for (int i = 0; i < c; i++)
                {
                    this.playerInZone.GetController().addAction(new ActionContainer(this.player.GetInventory().GetItem(index), this.screenDataContainer.containerData, true));
                }
            }
            else if (CanAdd(this.player.GetInventory().GetItem(index)))
            {
                this.playerInZone.GetController().addAction(new ActionContainer(this.player.GetInventory().GetItem(index), this.screenDataContainer.containerData, true));
            }
        }
    }

    public void Put()
    {
        int index = rightList.GetIndex();
        if (player.GetInventory().GetItem(index) != null)
        {
            Item it = player.GetInventory().GetItem(index);
            if (CanAdd(this.player.GetInventory().GetItem(index)) && (it.GetItemClass().Equals(ItemClass.STACK)|| it.GetItemClass().Equals(ItemClass.AMMOSTACK)) && GetMany())
            {
                int m = MaxCount(it);
                int c = m < 10 ? m : 10;
                for (int i = 0; i < c; i++)
                {
                    this.playerInZone.GetController().addAction(new ActionContainer(this.player.GetInventory().GetItem(index), this.screenDataContainer.containerData, true));
                }
            }
            else if (CanAdd(this.player.GetInventory().GetItem(index)))
            {
                this.playerInZone.GetController().addAction(new ActionContainer(this.player.GetInventory().GetItem(index), this.screenDataContainer.containerData, true));
            }
        }
    }

    private bool GetMany()
    {
        bool shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        return many || shift;
    }

    public void Take()
    {
        int index = leftList.GetIndex();
        if (screenDataContainer.containerData.GetItem(index) != null)
        {
            Item it = screenDataContainer.containerData.GetItem(index);
            if ((it.GetItemClass().Equals(ItemClass.STACK) || it.GetItemClass().Equals(ItemClass.AMMOSTACK)) && GetMany())
            {
                int m = MaxCount(it);
                int c = m < 10 ? m : 10;
                for (int i = 0; i < c; i++)
                {
                    this.playerInZone.GetController().addAction(new ActionContainer(this.screenDataContainer.containerData.GetItem(index), this.screenDataContainer.containerData, false));
                }
            }
            else
            {
                this.playerInZone.GetController().addAction(new ActionContainer(this.screenDataContainer.containerData.GetItem(index), this.screenDataContainer.containerData, false));
            }
        }
    }

    private int MaxCount(Item item)
    {
        if (item.GetItemClass().Equals(ItemClass.STACK))
        {
            return ((ItemStack)item).GetCount();
        }
        if (item.GetItemClass().Equals(ItemClass.AMMOSTACK)) {
            return ((ItemAmmoStack)item).GetCount();
        }

        return 1;
    }

    private bool CanAdd(Item item)
    {
        float w = item.GetDef().GetWeight();

        return screenDataContainer.containerData.GetWeight()+w<screenDataContainer.containerData.GetCapacity();
    }

    public void UpdateScreen()
    {
        BuildUI();
    }

    public void BuildUI()
    {

        leftWeight.text = String.Format("{0:0.#}", screenDataContainer.containerData.GetWeight()) + "/" + screenDataContainer.containerData.GetCapacity();
        rightWeight.text = String.Format("{0:0.#}", player.GetInventory().GetWeight()) + "/" + player.GetInventory().GetCapacity();

        List<string> containerList = new List<string>();
        List<int> containerIcons = new List<int>();
        for (int i = 0; i < screenDataContainer.containerData.GetItems().Count; i++)
        {
            containerList.Add(screenDataContainer.containerData.GetItems()[i].GetItemString());
            containerIcons.Add(screenDataContainer.containerData.GetItems()[i].GetDef().GetIcon());
        }
        leftList.SetList(containerList);
        leftList.SetIcons(containerIcons);
        List<string> playerList = new List<string>();
        List<int> playerIcons = new List<int>();
        for (int i=0;i < player.GetInventory().GetItems().Count; i++)
        {
            playerList.Add(player.GetInventory().GetItems()[i].GetItemString());
            playerIcons.Add(player.GetInventory().GetItems()[i].GetDef().GetIcon());
        }
        rightList.SetList(playerList);
        rightList.SetIcons(playerIcons);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            view.SetScreen(ScreenID.NONE);
        }  
      
    }

    public void ToggleManyClick()
    {
        many = !many;
        oneManyText.text = many ? "many" : "one";
    }

    public void ClickExit()
    {
        view.SetScreen(ScreenID.NONE);
    }
}
