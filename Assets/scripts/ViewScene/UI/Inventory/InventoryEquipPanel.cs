﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryEquipPanel : MonoBehaviour
{

    public Text[] EquipSlots;
    private int index;
    public bool active;
    public ListCallback listCallback;
    public GameObject leftSibling, rightSibling;
    private float controlClock;
    private Color selected = new Color(0, 1, 0);
    private Color deactivatedHighlight = new Color(0, 0.5F, 0);
    private Color normal = new Color(1, 1, 1);
    private readonly float TIME_INTERVAL = 0.1F;
    private readonly int PANELSLOTS = 7;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;

    }

    public void SetStrings(string [] strings)
    {
        for (int i = 0; i < PANELSLOTS; i++)
        {
            EquipSlots[i].text = strings[i];
        }
    }

    public void SetActive(bool active)
    {
        this.active = active;
        controlClock = 0.5F;
        if (active)
        {
            HighLightSelect();
        }
    }

    private void HighLightSelect()
    {
        for (int i=0;i<PANELSLOTS;i++)
        {
            if (active)
            {
                EquipSlots[i].GetComponent<Text>().color = i == index  ? selected : normal;
            }
            else
            {
                EquipSlots[i].GetComponent<Text>().color = i == index ? deactivatedHighlight : normal;
            }

        }
    }

    public void SetCallback(ListCallback callback)
    {
        this.listCallback = callback;
    }

    public void SelectOption(int index)
    {
        this.listCallback(index);
    }

    // Update is called once per frame
    void Update()
    {
        if (controlClock > 0)
        {
            controlClock -= Time.deltaTime;
        }
        else if (active)
        {

          //  Controls();
        }

     
    }

    private void Controls()
    {
        if (Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey(KeyCode.Return))
        {
            listCallback(index);
            controlClock = TIME_INTERVAL;
        }

        if ((Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Keypad8)) && index > 0)
        {
            index--;

            HighLightSelect();
            controlClock = TIME_INTERVAL;
        }
        if ((Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.Keypad2)) && index < PANELSLOTS - 1)
        {
            index++;

            HighLightSelect();
            controlClock = TIME_INTERVAL;
        }
        if ((Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.Keypad6)) && rightSibling != null)
        {
            active = false;
            rightSibling.BroadcastMessage("SetActive", true);
            HighLightSelect();
            controlClock = TIME_INTERVAL;
        }
        if ((Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Keypad4)) && leftSibling != null)
        {
            active = false;
            leftSibling.BroadcastMessage("SetActive", true);
            HighLightSelect();
            controlClock = TIME_INTERVAL;
        }
    }
}
