﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void SubmenuCallback (InventoryAction action);

public class UIInventorySubmenu : MonoBehaviour
{
    public Text text;
    public Button[] buttons;
    public InventoryAction [] buttonAction = new InventoryAction[6];
    private int buttonCount, index;
    private bool updateSelected, clickPrevention, isStack, dropAll;
    private EventSystem eventSystem;
    private float controlClock;
    private readonly float TIME_INTERVAL = 0.1F;
    private Color selected = new Color(0, 1, 0);
    private Color normal = new Color(1, 1, 1);
    public Item item;
    private bool isEquipped;
    public SubmenuCallback callback;
    // Start is called before the first frame update
    void Start()
    {
        updateSelected = false;
        eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        
    }

    public void SetItem(Item item)
    {
        this.item = item;
        this.isStack = (item is ItemStack || item is ItemAmmoStack);
        BuildTexts();
    }

    public void SetIsEquipped(bool isEquipped)
    {
        this.isEquipped = isEquipped;
    }

    private void BuildTexts()
    {
        text.text = item.GetDef().GetName();
        SubmenuTool.Build(item, text, buttons, buttonAction,isEquipped);

    }

    public void UpdateSelection()
    {
        eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        updateSelected = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!updateSelected)
        {
            eventSystem.SetSelectedGameObject(buttons[0].gameObject);
            buttons[0].OnSelect(null);
            updateSelected = true;
        }
        if (isStack)
        {
            if (!dropAll && (Input.GetKeyDown(KeyCode.LeftShift) | Input.GetKeyDown(KeyCode.RightShift)))
            {
                UpdateDrop(true);
                dropAll = true;
            }
            if (dropAll && Input.GetKeyDown(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.RightShift))
            {
                UpdateDrop(false);
                dropAll = false;
            }
        }
        if (clickPrevention)
        {
            if (!Input.anyKey)
            {
                clickPrevention = false;
            }
        }
        else
        {
            KeyInput();
        }
      
    }

    private void UpdateDrop(bool enabled)
    {
        for (int i=0;i< buttonAction.Length; i++)
        {
            if (buttonAction[i] == InventoryAction.DROP)
            {
                buttons[i].GetComponentInChildren<Text>().text = enabled ? "drop all" : "drop";
                break;
            }
        }
    }

    private void KeyInput()
    {
       if (Input.GetKey(KeyCode.Alpha1))
        {
            ButtonClick(0);
            clickPrevention = true;
        }
        if (Input.GetKey(KeyCode.Alpha2) && buttonCount>=2)
        {
            ButtonClick(1);
            clickPrevention = true;
        }
        if (Input.GetKey(KeyCode.Alpha3) && buttonCount >= 3)
        {
            ButtonClick(2);
            clickPrevention = true;
        }
        if (Input.GetKey(KeyCode.Alpha4) && buttonCount >= 4)
        {
            ButtonClick(3);
            clickPrevention = true;
        }
        if (Input.GetKey(KeyCode.Alpha5) && buttonCount >= 5)
        {
            ButtonClick(4);
            clickPrevention = true;
        }
        if (Input.GetKey(KeyCode.Alpha6) && buttonCount >= 6)
        {
            ButtonClick(5);
            clickPrevention = true;
        }
    }

    public void ButtonClick(int index)
    {
        if (isStack && dropAll && buttonAction[index] == InventoryAction.DROP)
        {
            callback(InventoryAction.DROPALL);
            return;
        }
        callback(buttonAction[index]);
    }

    public void SetCallback(SubmenuCallback callback)
    {

        this.callback = callback;
    }
}
