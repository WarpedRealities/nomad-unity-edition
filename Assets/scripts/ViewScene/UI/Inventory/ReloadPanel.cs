﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void SublistCallback(Item item);

public class ReloadPanel : MonoBehaviour
{
    private NuList listUI;
    public Item [] items;
    public SublistCallback callback;

    // Start is called before the first frame update
    void Start()
    {
        listUI = GetComponentInChildren<NuList>();
        listUI.SetCallback(ListCallback);

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ListCallback(int index)
    {
        if (index < items.Length)
        {
            callback(items[index]);
        }
        else
        {
            callback(null);
        }
    }

    public void SetCallback(SublistCallback callback)
    {
        this.callback = callback;

    }

    public void PopulateList(Item [] items)
    {
        if (listUI == null)
        {
            listUI = GetComponentInChildren<NuList>();
        }
        this.items = items;
        List<string> listStrings = new List<string>();
        for (int i = 0; i < items.Length; i++)
        {
            listStrings.Add(items[i].GetItemString());
        }
        listStrings.Add("cancel");
        listUI.SetList(listStrings);
    }
}