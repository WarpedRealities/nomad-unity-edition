﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryScreen : MonoBehaviour
{
    private ViewInterface view;
    public GameObject UIList,UIWeight, UISubmenu, UIEquipPanel, description,subList;
    public Text goldText, creditsText;
    private UIInventorySubmenu inventorySubmenu;
    private bool controlsEnabled = false, active=true;
    private int index;
    private Player player;
    private ZoneActor playerInZone;
    private SubmenuCallback submenuCallback;
    private SublistCallback sublistCallback;
    // Start is called before the first frame update
    void Start()
    {
        inventorySubmenu = UISubmenu.GetComponent<UIInventorySubmenu>();
        ListCallback listCallback = Callback;
        submenuCallback = SubmenuCallback;
        UIList.BroadcastMessage("SetCallback", listCallback);
        UISubmenu.BroadcastMessage("SetCallback", submenuCallback);
        UISubmenu.SetActive(false);
        ListCallback equipCallback = EquipCallback;
        UIEquipPanel.BroadcastMessage("SetCallback", equipCallback);
        UIEquipPanel.BroadcastMessage("SetActive", false);
        BuildEquipPanel();
        sublistCallback = SublistCallback;
    }

    private void BuildEquipPanel()
    {
        string[] strings = new string[7];
        for (int i = 0; i < 7; i++)
        {
            if (player.GetInventory().GetSlot(i) != null)
            {
                strings[i] = player.GetInventory().GetSlot(i).GetItemString();
            }
            else
            {
                strings[i] = "none";
            }

        }
        UIEquipPanel.BroadcastMessage("SetStrings", strings);
    }

    // Update is called once per frame
    void Update()
    {
        if (!controlsEnabled && !Input.anyKey)
        {
            controlsEnabled = true;
        }
        if (controlsEnabled && Input.GetKey(KeyCode.I) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            view.SetScreen(ScreenID.NONE);
        }
        if (controlsEnabled && Input.GetKey(KeyCode.Escape))
        {
            view.SetScreen(ScreenID.NONE);
        }
    }

    public void ExitClick()
    {
        view.SetScreen(ScreenID.NONE);
    }

    public void SubmenuCallback(InventoryAction inventoryAction)
    {
        if (!player.GetInventory().Contains(UISubmenu.GetComponent<UIInventorySubmenu>().item))
        {
            UISubmenu.SetActive(false);
            UIList.BroadcastMessage("SetActive", true);
            return;
        }
        switch (inventoryAction)
        {
            case InventoryAction.DROP:

                playerInZone.GetController().addAction(new ActionInventory(player.GetInventory().GetItem(index), inventoryAction,10));
                active = false;
                break;

            case InventoryAction.DROPALL:
                HandleDropAll();
                active = false;
                break;
            case InventoryAction.USE:
                playerInZone.GetController().addAction(new ActionInventory(player.GetInventory().GetItem(index), inventoryAction, 10));
                active = false;
                break;

            case InventoryAction.EQUIP:
                SwapItemCheck(player.GetInventory().GetItem(index));
                playerInZone.GetController().addAction(new ActionInventory(player.GetInventory().GetItem(index), inventoryAction, 10));
                active = false;
                break;

            case InventoryAction.UNEQUIP:
                UISubmenu.SetActive(false);
                playerInZone.GetController().addAction(new ActionInventory(player.GetInventory().GetSlot((index+1)*-1), inventoryAction,10));
                active = false;
                break;

            case InventoryAction.TAKE:

                HandleTakeCommand();
                UpdateScreen();
                active = false;
                break;

            case InventoryAction.CANCEL:
                UISubmenu.SetActive(false);
               // UIList.BroadcastMessage("SetActive", true);
                break;


            case InventoryAction.EXAMINE:
                if (index >= 0)
                {
                    description.GetComponent<Text>().text = player.GetInventory().GetItem(index).GetDescription();
                }
                else
                {
                    description.GetComponent<Text>().text = player.GetInventory().GetSlot((index*-1)-1).GetDescription();
                }

                break;
            case InventoryAction.UNLOAD:
                UISubmenu.SetActive(false);
                HandleUnloadCommand();
                UpdateScreen();
                active = false;
                break;
            case InventoryAction.RELOAD:
                UISubmenu.SetActive(false);
                subList.SetActive(true);
                Debug.Log("index is " + index);
                //populate list
                if (index >= 0)
                {
                    subList.BroadcastMessage("PopulateList", ReloadListTool.BuildList(player.GetInventory().GetItem(index),player.GetInventory().GetItems()));
                }
                else
                {
                    subList.BroadcastMessage("PopulateList", ReloadListTool.BuildList(player.GetInventory().GetSlot((index + 1) * -1), player.GetInventory().GetItems()));
                }
                subList.SendMessage("SetCallback", sublistCallback);

                break;
        }
        if (index>=0 && !player.GetInventory().GetItems().Contains(UISubmenu.GetComponent<UIInventorySubmenu>().item))
        {
            UISubmenu.SetActive(false);
            UIList.BroadcastMessage("SetActive", true);
        }
    }

    private void HandleDropAll()
    {
        Item item = player.GetInventory().GetItem(index);
        if (item is ItemStack)
        {
            ItemStack itemStack = (ItemStack)item;
            for (int i = 0; i < itemStack.GetCount(); i++)
            {
                playerInZone.GetController().addAction(new ActionInventory(item, InventoryAction.DROP, 10));
            }

        }
        if (item is ItemAmmoStack)
        {
            ItemAmmoStack itemAmmoStack = (ItemAmmoStack)item;
            for (int i = 0; i < itemAmmoStack.GetCount(); i++)
            {
                playerInZone.GetController().addAction(new ActionInventory(item, InventoryAction.DROP, 10));
            }
        }

    }

    private void HandleUnloadCommand()
    {
        Item reloadThis = index < 0 ? player.GetInventory().GetSlot((index + 1) * -1) : player.GetInventory().GetItems()[index];
        ResourceStoreInstance resourceStore = GetResourceStore(reloadThis);// reloadThis.GetDef().GetItemType() == ItemType.EQUIP ? ((EquipInstance)reloadThis).GetResourceStore(): null;

        playerInZone.GetController().addAction(new ActionReload(null, reloadThis, resourceStore.GetDef().GetReloadCost()));
    }

    private ResourceStoreInstance GetResourceStore(Item reloadThis)
    {
        if (reloadThis.GetDef().GetItemType() == ItemType.EQUIP)
        {
            return ((EquipInstance)reloadThis).GetResourceStore();
        }
        if (reloadThis.GetDef().GetItemType() == ItemType.AMMO)
        {
            return ((AmmoInstance)reloadThis).GetResource();
        }
        throw new NotImplementedException();
    }

    private void HandleTakeCommand()
    {
        if (index >= 0)
        {
            Item item = player.GetInventory().GetItem(index);
            if (item.GetItemClass() == ItemClass.AMMOSTACK)
            {
                ItemAmmoStack itemAmmo = (ItemAmmoStack)item;
                if (itemAmmo.GetCount() > 1)
                {
                    Item takeItem = itemAmmo.TakeItem();
                    itemAmmo.SetCount(itemAmmo.GetCount() - 1);
                    player.GetInventory().GetItems().Add(takeItem);
                    player.GetInventory().CalculateWeight();
                }
            }
            if (item.GetItemClass() == ItemClass.STACK)
            {
                ItemStack itemStack = (ItemStack)item;
                if (itemStack.GetCount() > 1)
                {
                    Item takeItem=itemStack.TakeItem();
                    itemStack.SetCount(itemStack.GetCount() - 1);
                    player.GetInventory().GetItems().Add(takeItem);
                    player.GetInventory().CalculateWeight();
                }
            }
        }
    }

    private void GetDescription(int index)
    {
        if (index >= 0)
        {
            description.GetComponent<Text>().text = player.GetInventory().GetItem(index).GetDescription();
        }
        else
        {
            description.GetComponent<Text>().text = player.GetInventory().GetSlot((index+1)*-1).GetDescription();
        }
    }

    private void SwapItemCheck(Item item)
    {
        ItemEquippable itemEquippable = (ItemEquippable)item.GetDef();
        if (player.GetInventory().GetSlot((int)itemEquippable.GetSlot())!=null)
        {
            playerInZone.GetController().addAction(new ActionInventory(player.GetInventory().GetSlot((int)itemEquippable.GetSlot()), InventoryAction.UNEQUIP, 10));
        }
    }

    public void Callback(int index)
    {

        if (player.GetInventory().GetItemCount() > index)
        {
            this.index = index;
       //     UIList.BroadcastMessage("SetActive", false);
            UISubmenu.SetActive(true);
            UISubmenu.BroadcastMessage("UpdateSelection");
            UISubmenu.BroadcastMessage("SetIsEquipped", false);
            UISubmenu.BroadcastMessage("SetItem", player.GetInventory().GetItem(index));

        }
   //     Debug.Log("callback called with " + index);
    }

    public void SublistCallback(Item item)
    {

        if (item != null)
        {
            //trigger a reload sequence, let the madness commence!
            ReloadAction(item);
        }
        subList.SetActive(false);
        UIList.SetActive(true);
        //UIList.BroadcastMessage("SetActive", true);
    }

    private void ReloadAction(Item ammoItem)
    {
        //need to find the item we're reloading
        Item reloadThis = index < 0 ? player.GetInventory().GetSlot((index + 1) * -1) : player.GetInventory().GetItems()[index];
        Debug.Log("reload action from inventory screen");
        ReloadExecutionTool.Execute(ammoItem, reloadThis, playerInZone);

    }

    public void EquipCallback(int index)
    {

        if (player.GetInventory().GetSlot(index) != null)
        {
            this.index = (index*-1)-1;
            Debug.Log("this index set to " + this.index);
            UIEquipPanel.BroadcastMessage("SetActive", false);
            UISubmenu.SetActive(true);
            UISubmenu.BroadcastMessage("UpdateSelection");
            UISubmenu.BroadcastMessage("SetIsEquipped", true);
            UISubmenu.BroadcastMessage("SetItem", player.GetInventory().GetSlot(index));

        }
    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    public void UpdateScreen()
    {
        active = true;
        BuildUI();
        BuildEquipPanel();
    }

    public void BuildUI()
    {
        List<string> list = new List<string>();
        List<int> iconList = new List<int>();
        for (int i = 0; i < player.GetInventory().GetItemCount(); i++)
        {
            list.Add(player.GetInventory().GetItem(i).GetItemString());
            iconList.Add(player.GetInventory().GetItem(i).GetDef().GetIcon());
        }
        UIList.BroadcastMessage("SetList", list);
        UIList.BroadcastMessage("SetIcons", iconList);
        Text text = UIWeight.GetComponent<Text>();
        text.text="weight:"+player.GetInventory().GetWeight()+"/"+player.GetInventory().GetCapacity();
        text.color = BuildColor();
        if (inventorySubmenu!=null && inventorySubmenu.item!=null && !player.GetInventory().GetItems().Contains(inventorySubmenu.item))
        {
            UISubmenu.SetActive(false);
            if (index >= 0)
            {
      //          UIList.BroadcastMessage("SetActive", true);
            }
            else
            {
                UIEquipPanel.BroadcastMessage("SetActive", true);
            }

        }
        goldText.text = "gold:" + player.GetInventory().GetGold();
        creditsText.text = "credits:" + player.GetInventory().GetCredits();
    }

    private Color BuildColor()
    {
        switch (player.GetInventory().GetEncumbranceLevel())
        {

            case 2:
                return new Color(1, 1, 0);
            case 3:
                return new Color(1, 0.5F, 0);
            case 4:
                return new Color(1, 0, 0);
            case 5:
                return new Color(0.5F, 0, 0);
        }
        return new Color(1, 1, 1);
    }

    public void SetPlayer(ZoneActor player)
    {
        this.playerInZone = player;
        this.player = (Player)player.actor;
        BuildUI();
    }
}
