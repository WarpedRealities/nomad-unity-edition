﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppearanceScreen : MonoBehaviour
{

    Player player;
    ViewInterface view;
    public Text text;
    bool controlsEnabled = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!controlsEnabled && !Input.anyKey)
        {
            controlsEnabled = true;
        }
        if (controlsEnabled && Input.GetKey(KeyCode.L) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            view.SetScreen(ScreenID.NONE);
        }
    }

    public void SetPlayer(ZoneActor player)
    {
        this.player = (Player)player.actor;
        BuildText();
    }

    private void BuildText()
    {
        this.text.text = DescriptionBuilder.BuildText(player);
    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    public void ClickExit()
    {
        view.SetScreen(ScreenID.NONE);
    }
}
