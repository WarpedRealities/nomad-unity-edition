﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class DialogueScreen : MonoBehaviour
{
    public Text[] texts;
    public RectTransform mainPanel;
    DialogueEngine dialogueEngine;
    private bool doubleTapPrevention;
    ViewInterface viewInterface;    
    SolarSceneScript solarScene;
    public DialoguePortrait portrait;
    // Start is called before the first frame update
    void Start()
    {
        AlignPanels();
        doubleTapPrevention = true;
      //  SetConversation(new ScreenDataConversation("interactions/escapepod", new FlagField(), new WidgetConversation(0, "", "", null)));
    }

    private void AlignPanels()
    {
        GameObject statusUI = GameObject.Find("PlayerStatusUI");
        if (statusUI != null)
        {
            RectTransform rect = statusUI.GetComponent<RectTransform>();
            mainPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, rect.sizeDelta.x);
            mainPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, rect.sizeDelta.y);

        }
    }

    public void SetPlayer(ZoneActor player)
    {

    }

    public void SetView(ViewInterface viewInterface)
    {

        this.viewInterface = viewInterface;
        if (dialogueEngine != null)
        {
            dialogueEngine.SetView(viewInterface);
        }
    }

    public void SetSolarScene(SolarSceneScript solarScene)
    {
        SolarSceneFacade facade = new SolarSceneFacade(solarScene);
        this.viewInterface = facade;
        if (dialogueEngine != null)
        {
            dialogueEngine.SetView(facade);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (doubleTapPrevention && !Input.anyKey)
        {
            doubleTapPrevention = false;
        }
        if (!doubleTapPrevention)
        {
            if (Input.GetKey(KeyCode.Alpha1) || Input.GetKey(KeyCode.Keypad1))
            {
                doubleTapPrevention = true;
                MakeChoice(0);
            }
            if (Input.GetKey(KeyCode.Alpha2) || Input.GetKey(KeyCode.Keypad2))
            {
                doubleTapPrevention = true;
                MakeChoice(1);
            }
            if (Input.GetKey(KeyCode.Alpha3) || Input.GetKey(KeyCode.Keypad3))
            {
                doubleTapPrevention = true;
                MakeChoice(2);
            }
            if (Input.GetKey(KeyCode.Alpha4) || Input.GetKey(KeyCode.Keypad4))
            {
                doubleTapPrevention = true;
                MakeChoice(3);
            }
            if (Input.GetKey(KeyCode.Alpha5) || Input.GetKey(KeyCode.Keypad5))
            {
                doubleTapPrevention = true;
                MakeChoice(4);
            }
            if (Input.GetKey(KeyCode.Alpha6) || Input.GetKey(KeyCode.Keypad6))
            {
                doubleTapPrevention = true;
                MakeChoice(5);
            }
            if (Input.GetKey(KeyCode.Alpha7) || Input.GetKey(KeyCode.Keypad7))
            {
                doubleTapPrevention = true;
                MakeChoice(6);
            }
            if (Input.GetKey(KeyCode.Alpha8) || Input.GetKey(KeyCode.Keypad8))
            {
                doubleTapPrevention = true;
                MakeChoice(7);
            }
            if (Input.GetKey(KeyCode.Alpha9) || Input.GetKey(KeyCode.Keypad9))
            {
                doubleTapPrevention = true;
                MakeChoice(8);
            }
            if (Input.GetKey(KeyCode.Alpha0) || Input.GetKey(KeyCode.Keypad0))
            {
                doubleTapPrevention = true;
                MakeChoice(9);
            }
        }
    }

    public void MakeChoice(int index)
    {
        if (dialogueEngine.MakeChoice(index))
        {
            UpdateState();
        }
    }

    void UpdateScreen()
    {

    }

    void SetConversation(ScreenDataConversation screenDataConversation)
    {
        dialogueEngine = new DialogueEngine(screenDataConversation.GetConversationFile(), 
            screenDataConversation.GetFlagField(),
            GlobalGameState.GetInstance().GetPlayer(),
            portrait);
        if (screenDataConversation.GetActor() != null)
        {
            dialogueEngine.SetActor(screenDataConversation.GetActor());
        }
        if (screenDataConversation.GetWidget() != null)
        {
            dialogueEngine.SetWidget(screenDataConversation.GetWidget());
        }
        if (screenDataConversation.GetEntity() != null)
        {
            dialogueEngine.SetEntity(screenDataConversation.GetEntity());
        }
        if (viewInterface != null)
        {
            dialogueEngine.SetView(viewInterface);
        }
        dialogueEngine.InitialState();
        if (screenDataConversation.IsGameoverOnAbort() && !dialogueEngine.HasPage())
        {
            TextLog.Log("You have perished");
            SceneManager.LoadScene("GameOverScene");
            return;
        }
        UpdateState();
    }

    void UpdateState()
    {
        dialogueEngine.ProcessNode();
        if (dialogueEngine.GetPageContents() != null){
            TextLog.Log(dialogueEngine.GetPageContents());
        }
        CleanChoices();
        BuildChoices();
    }

    private void BuildChoices()
    {
        for (int i = 0; i < 10; i++)
        {
            if (i < dialogueEngine.GetChoices().Count)
            {
                texts[i].text = (i+1)+ ". " + FormatChoice(dialogueEngine.GetChoices()[i].text);
            }
            else
            {
                texts[i].text = "";
            }

        }
    }

    public string FormatChoice(string text)
    {
        return text.Replace("$", "");
    }

    void CleanChoices()
    {
        for (int i = 0; i < 10; i++)
        {
            texts[i].text = "";
        }
    }
}
