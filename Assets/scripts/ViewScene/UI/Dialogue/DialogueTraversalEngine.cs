﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;

public class DialogueTraversalEngine 
{
    XmlDocument xmlDocument;
    XmlNodeList pages;
    XmlElement currentPage;
    DialogueEvaluator dialogueEvaluator;

    public DialogueTraversalEngine(string filename, DialogueEvaluator dialogueEvaluator)
    {
        this.dialogueEvaluator = dialogueEvaluator;
        xmlDocument = FileTools.GetXmlDocument("conversations/"+filename);

        pages=xmlDocument.FirstChild.NextSibling.ChildNodes;
    }

    public XmlElement FindNode(string id)
    {
        if ("end".Equals(id))
        {
            currentPage = null;
            return null;
        }


        for (int i = 0; i < pages.Count; i++)
        {
            if (XmlNodeType.Element == pages[i].NodeType)
            {
                XmlElement xmlElement = (XmlElement)pages[i];
                if (id.Equals(xmlElement.GetAttribute("ID")) && dialogueEvaluator.CheckNode(xmlElement)) {
                    currentPage = xmlElement;
                    return xmlElement;
                }
            }
         
        }
        return currentPage;
    }

    public XmlElement GetCurrentNode()
    {
        return currentPage;
    }
}
