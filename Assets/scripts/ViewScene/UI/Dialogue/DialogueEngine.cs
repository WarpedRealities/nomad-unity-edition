﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogueChoice
{
    public int index;
    public string destination;
    public string text;
    public DialogueChoice(int index, string destination, string text)
    {
        this.index = index;
        this.destination = destination;
        this.text = text.Replace("PNAME",GlobalGameState.GetInstance().GetPlayer().GetName());
    }
}

public class DialogueEngine
{

    private FlagField localFlags;
    private FlagField globalFlags;
    private Player player;
    private Widget widget;
    private DialogueActor actor;
    private Active_Entity entity;
    private string pageContents;
    private List<DialogueChoice> dialogueChoices;
    DialogueTraversalEngine dialogueTraversalEngine;
    DialogueEvaluator dialogueEvaluator;
    DialogueEffectProcessor dialogueEffectProcessor;
    private ViewInterface view;

    public DialogueEngine(string conversationFile, FlagField flagField, Player player,DialoguePortrait portrait)
    {
        this.player = player;
        dialogueChoices = new List<DialogueChoice>();
        globalFlags = GlobalGameState.GetInstance().GetPlayer().GetFlagField();
        dialogueEvaluator = new DialogueEvaluator(flagField, globalFlags,player);
        dialogueEffectProcessor = new DialogueEffectProcessor(flagField, globalFlags, player, portrait);
        dialogueTraversalEngine = new DialogueTraversalEngine(conversationFile, dialogueEvaluator);
        this.localFlags = flagField;
    }

    internal void SetWidget(Widget widget)
    {
        this.widget = widget;
        this.dialogueEffectProcessor.SetWidget(widget);
    }

    internal void SetActor(DialogueActor actor)
    {
        this.actor = actor;
        this.dialogueEvaluator.SetActor(actor);
        this.dialogueEffectProcessor.SetActor(actor);
    }
    internal void SetEntity(Active_Entity entity)
    {
        this.entity = entity;
        this.dialogueEffectProcessor.SetEntity(entity);
        this.dialogueEvaluator.SetEntity(entity);

    }
    internal void InitialState()
    {
        this.dialogueTraversalEngine.FindNode("start");
        ProcessNode();
    }

    internal void ProcessNode()
    {
        XmlElement currentPage = dialogueTraversalEngine.GetCurrentNode();
        if (currentPage != null)
        {
            if ("page".Equals(currentPage.Name))
            {
                ProcessPage(currentPage);
            }
            if ("check".Equals(currentPage.Name))
            {
                ProcessCheck(currentPage);
            }
            if ("gameover".Equals(currentPage.Name))
            {
                ProcessGameover(currentPage);
            }
        }
        else
        {
            pageContents = null;
        }
    }

    private void ProcessGameover(XmlElement currentPage)
    {
        XmlNodeList children = currentPage.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("text".Equals(element.Name))
                {
               
                    TextLog.Log(element.InnerText);
                    pageContents = null;
                }
            }
        }
        if ("true".Equals(currentPage.GetAttribute("noReform")))
        {
            player.GetReformation().SetNoReform(true);
        }
        SceneManager.LoadScene("GameOverScene");
    }

    private void ProcessCheck(XmlElement currentPage)
    {
        string[] outcomes = new string[2];
        int DC = 0;
        int index = 0;
        SK skill = 0;
        XmlNodeList children = currentPage.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("test".Equals(element.Name))
                {
                    skill = EnumTools.strToSkill(element.GetAttribute("what"));
                    DC = int.Parse(element.GetAttribute("DC"));
                }
                if ("outcome".Equals(element.Name))
                {
                    outcomes[index] = element.GetAttribute("destination");
                    index++;
                }
            }
        }

        //resolve the check       
        int roll = DiceRoller.GetInstance().RollDice(20)+player.GetRPG().getSkill(skill);
        if (roll >= DC)
        {
            dialogueTraversalEngine.FindNode(outcomes[0]);
            ProcessNode();
        }
        else
        {
            dialogueTraversalEngine.FindNode(outcomes[1]);
            ProcessNode();
        }
    }

    private void ProcessPage(XmlElement currentPage)
    {
        dialogueChoices.Clear();
        XmlNodeList children = currentPage.ChildNodes;
        dialogueEffectProcessor.ProcessEffects(currentPage);
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("text".Equals(element.Name))
                {
                    ProcessPageContents(element);
                }
                if ("choice".Equals(element.Name))
                {
                    ProcessChoice(element);
                }
            }
        }

    }

    private void ProcessChoice(XmlElement element)
    {
        if (dialogueEvaluator.CheckNode(element))
        {
            dialogueChoices.Add(new DialogueChoice(dialogueChoices.Count, element.GetAttribute("destination"), element.GetAttribute("text")));
        }
    }

    internal bool MakeChoice(int index)
    {
        if (index >= dialogueChoices.Count)
        {
            return false;
        }
        ProcessChoiceText(dialogueChoices[index]);
        XmlElement element=dialogueTraversalEngine.FindNode(dialogueChoices[index].destination);
        if (element==null)
        {
            if (view != null)
            {
                view.SetScreen(ScreenID.NONE);
            } else
            {
                GameObject sceneController = GameObject.Find("SolarSceneObj");
                if (sceneController != null)
                {
                    SolarSceneScript solarSceneScript =
                        sceneController.GetComponent<SolarSceneScript>();
                    solarSceneScript.SetScreen(null);
                }

            }

        }

        return true;
    }

    private void ProcessChoiceText(DialogueChoice dialogueChoice)
    {
        if (dialogueChoice.text.StartsWith('$'))
        {
            TextLog.Log(player.GetName() + ":" + dialogueChoice.text.Substring(1));
        }
        if (dialogueChoice.text.StartsWith('#'))
        {
            TextLog.Log(this.actor.GetActor().GetName() + ":" + dialogueChoice.text.Substring(1));
        }

    }

    private void ProcessPageContents(XmlElement element)
    {
        StringBuilder stringBuilder = new StringBuilder();
        XmlNodeList children = element.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Text)
            {

                stringBuilder.Append(children[i].Value.Replace("\t", "").Replace("PNAME",player.GetName()));


            }
            if (children[i].NodeType== XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("conditional".Equals(xmlElement.Name) && dialogueEvaluator.CheckNode(xmlElement))
                {
                    stringBuilder.Append(ProcessConditionalBlock(xmlElement));
                }
                if ("macro".Equals(xmlElement.Name))
                {
                    stringBuilder.Append(MacroGetter(xmlElement));
                }
                if ("measurement".Equals(xmlElement.Name))
                {
                    stringBuilder.Append(MeasurementGetter(xmlElement));
                }
                if ("readequipment".Equals(xmlElement.Name))
                {
                    int slot = int.Parse(xmlElement.GetAttribute("slot"));
                    stringBuilder.Append(player.GetInventory().GetSlot(slot).GetName());
                }
                if ("random".Equals(xmlElement.Name))
                {
                    DialogueRandomizer randomizer = DialogueRandomizer.GetInstance();
                    stringBuilder.Append(
                        randomizer.GetRandomizer(xmlElement.GetAttribute("id")).GetString());
                }
            }
        }
        pageContents = stringBuilder.ToString();
    }

    private string MeasurementGetter(XmlElement xmlElement)
    {
        return player.GetAppearance().GetPart(xmlElement.GetAttribute("part")).GetValue(xmlElement.GetAttribute("variable")).ToString();
    }

    private string MacroGetter(XmlElement element)
    {
        string id = element.GetAttribute("id");
        if (id==null || id.Length < 2)
        {
            id= element.GetAttribute("ID");
        }
        return DescriptionMacroTool.GetMacro(id, player.GetAppearance());
    }

    private string ProcessConditionalBlock(XmlElement element)
    {
        StringBuilder stringBuilder = new StringBuilder();
        XmlNodeList children = element.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Text)
            {
                stringBuilder.Append(children[i].Value);
            }
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("conditional".Equals(xmlElement.Name))
                {
                    if (dialogueEvaluator.CheckNode(xmlElement))
                    {
                        stringBuilder.Append(ProcessConditionalBlock(xmlElement));
                    }
                }
                if ("macro".Equals(xmlElement.Name))
                {
                    stringBuilder.Append(DescriptionMacroTool.GetMacro(xmlElement.GetAttribute("id"), player.GetAppearance()));
                }
                if ("readequipment".Equals(xmlElement.Name))
                {
                    int slot = int.Parse(xmlElement.GetAttribute("slot"));
                    stringBuilder.Append(player.GetInventory().GetSlot(slot).GetName());
                }
                if ("measurement".Equals(xmlElement.Name))
                {
                    stringBuilder.Append(ReadValue(xmlElement));
                }
                if ("random".Equals(xmlElement.Name))
                {
                    DialogueRandomizer randomizer = DialogueRandomizer.GetInstance();
                    stringBuilder.Append(
                        randomizer.GetRandomizer(xmlElement.GetAttribute("id")).GetString());
                }
            }
        }
        return stringBuilder.ToString();
    }

    private string ReadValue(XmlElement e)
    {
        BodyPart part = player.GetAppearance().GetPart(e.GetAttribute("part"));
        int remain = 0;
        int divide = 1;
        int.TryParse(e.GetAttribute("rem"), out remain);
        int.TryParse(e.GetAttribute("div"), out divide);

        if (remain > 0)
        {
            return (part.GetValue(e.GetAttribute("variable")) % remain).ToString();
        }
        if (divide > 0)
        {
            return (part.GetValue(e.GetAttribute("variable")) / divide).ToString();
        }
        return (part.GetValue(e.GetAttribute("variable"))).ToString();
    }

    internal bool HasPage()
    {
        return dialogueTraversalEngine.GetCurrentNode() != null;
    }

    internal void SetView(ViewInterface viewInterface)
    {
        this.view = viewInterface;
        this.dialogueEffectProcessor.SetView(viewInterface);
    }

    internal string GetPageContents()
    {
        return pageContents;
    }

    public List<DialogueChoice> GetChoices()
    {
        return dialogueChoices;
    }


}
