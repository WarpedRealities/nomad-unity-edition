using UnityEngine;

public interface DialogueActor
{
    void ForceUpdate(bool regen=false);
    public Actor GetActor();
    ActorController GetController();
    Vector2Int getPosition();
    public void Remove(bool dropLoot=false);
}