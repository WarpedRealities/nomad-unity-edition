using UnityEngine;

public class DummyActor : DialogueActor
{
    Actor actor;

    public DummyActor(Actor actor)
    {
        this.actor = actor;
    }

    public void ForceUpdate(bool regen=false)
    {
      
    }

    public Actor GetActor()
    {
        return actor;
    }

    public ActorController GetController()
    {
        throw new System.NotImplementedException();
    }

    public Vector2Int getPosition()
    {
        return actor.GetPosition();
    }

    public void Remove(bool dropLoot = false)
    {
       
    }
}