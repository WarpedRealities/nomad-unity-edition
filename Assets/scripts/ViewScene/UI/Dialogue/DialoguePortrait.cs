using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class DialoguePortrait : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Image image;
    public RectTransform rect;
    private Sprite[] sprites = new Sprite[2];
    private Vector2[] sizes = new Vector2[2];
    private string filename;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPortrait(string file)
    {

        if (filename == file)
        {
            return;
        }
        filename = file;
        Texture2D texture2D= TextureManagementTools.loadTexture("gameData/art/portraits/" + file + ".png", true);

        if (texture2D != null)
        {
            gameObject.SetActive(true);
            
            BuildSprites(texture2D);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void BuildSprites(Texture2D texture2D)
    {
        Rect rect0 = new Rect(0, texture2D.height-texture2D.width, texture2D.width, texture2D.width);
        Rect rect1 = new Rect(0, 0, texture2D.width, texture2D.height);
        Debug.Log("rectangle" + rect);
        Debug.Log("texture2d" + texture2D);
        sprites[0] = Sprite.Create(texture2D, rect0, new Vector2(0, 0));
        sprites[1] = Sprite.Create(texture2D, rect1, new Vector2(0, 0));
        float y = ((float)texture2D.height)/ ((float)texture2D.width);
        Debug.Log("y is " + y);
        float x = rect.sizeDelta.x;
        this.sizes[0] = new Vector2(x, x);
        this.sizes[1] = new Vector2(x, x * y);
        image.sprite = sprites[0];
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        image.sprite = sprites[0];
        rect.sizeDelta = sizes[0];
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        image.sprite = sprites[1];
        rect.sizeDelta = sizes[1];
    }
}
