﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class DialogueEvaluator
{
    private FlagField localFlags;
    private FlagField globalFlags;
    private Player player;
    private DialogueActor actor;
    private Faction faction;
    private GlobalGameState gameState;
    private Active_Entity entity;

    public DialogueEvaluator(FlagField localFlags, FlagField globalFlags, Player player)
    {
        this.gameState = GlobalGameState.GetInstance();
        this.player = player;
        this.localFlags = localFlags;
        this.globalFlags = globalFlags;
    }

    internal void SetActor(DialogueActor actor)
    {
        this.actor = actor;
        faction = actor.GetActor().GetFaction();
    }

    internal bool CheckNode(XmlElement root)
    {
        XmlNodeList xmlNodeList = root.ChildNodes;

        for (int i = 0; i < xmlNodeList.Count; i++)
        {
            if (XmlNodeType.Element == xmlNodeList[i].NodeType)
            {
                XmlElement element = (XmlElement)xmlNodeList[i];

                if ("condition".Equals(element.Name) && !ConditionalEvaluator(element))
                {
                    return false;
                }
                if ("assertion".Equals(element.Name) && !AssertionEvaluator(element))
                {
                    return false;
                }
                if ("assertnot".Equals(element.Name) && AssertionEvaluator(element))
                {
                    return false;
                }
                if ("preference".Equals(element.Name) && !PreferenceEvaluator(element))
                {
                    return false;
                }
            }
        }

        return true;
    }

    private bool PreferenceEvaluator(XmlElement element)
    {
        string fetish = element.GetAttribute("fetish");
        if (gameState.GetUniverse().GetPreferences().PreferenceBlocked(fetish))
        {
            return false;
        }
        return true;
    }

    private bool AssertionEvaluator(XmlElement element)
    {
        string evaluate = element.GetAttribute("evaluate");

        if ("operatorOr".Equals(evaluate))
        {
            return OperatorOr(element);
        }
        if ("operatorAnd".Equals(evaluate))
        {
            return OperatorAnd(element);
        }
        if ("isCompanion".Equals(evaluate))
        {

            return actor.GetActor().IsCompanion();
        }
        if ("inZone".Equals(evaluate))
        {
            string name = element.GetAttribute("zone");
            if (GlobalGameState.GetInstance().getCurrentZone().getName().Contains(name))
            {
                return true;
            }
        }
        if ("inEntity".Equals(evaluate))
        {
            string name = element.GetAttribute("entity");
            if (GlobalGameState.GetInstance().getCurrentEntity().getName().Contains(name))
            {
                return true;
            }
        }
        if ("inSystem".Equals(evaluate))
        {
            string name = element.GetAttribute("system");
            if (GlobalGameState.GetInstance().getCurrentSystem().GetName().Contains(name))
            {
                return true;
            }
        }
        if ("companionName".Equals(evaluate))
        {
            string name = element.GetAttribute("name");

            if (player.GetCompanionUID() != -1)
            {

                Actor actor = GlobalGameState.GetInstance().getCurrentZone().GetContents().GetActorByID(player.GetCompanionUID());

                if (actor != null)
                {

                    return name.Equals(actor.GetName());
                }
            }
        }
        if ("isFaction".Equals(evaluate))
        {
            string faction = element.GetAttribute("faction");
            return actor.GetActor().GetFaction().GetName().Equals(faction);
        }
        if ("companionSlotFree".Equals(evaluate))
        {

            return player.GetCompanionUID() == -1;
        }
        if ("hasStatus".Equals(evaluate))
        {
            int uid = int.Parse(element.GetAttribute("uid"));
            return player.GetRPG().GetStatusHandler().HasStatusEffect(uid);
        }
        if ("hasperk".Equals(evaluate))
        {
            string perk = element.GetAttribute("perk");
            List<PerkInstance> perks = ((PlayerRPG)player.GetRPG()).GetPerks();
            for (int i = 0; i < perks.Count; i++)
            {
                if (perks[i].GetRef().GetName().Equals(perk) ||
                    perks[i].GetRef().GetCode().Equals(perk))
                {
                   
                    return perks[i].GetRef().CanUse((PlayerRPG)player.GetRPG(),player.GetAppearance());
                }
            }
        }
        if ("haspart".Equals(evaluate))
        {
            string part = element.GetAttribute("part");
            if (player.GetAppearance().GetPart(part)!=null)
            {
                return true;
            }
        }
        if ("slothasitem".Equals(evaluate))
        {
            int slot = int.Parse(element.GetAttribute("slot"));
            if (player.GetInventory().GetSlot(slot) != null)
            {
                return true;
            }
        }
        if ("hasCompanion".Equals(evaluate))
        {
            if (player.GetCompanionUID() != -1)
            {
                Actor actor = GlobalGameState.GetInstance().getCurrentZone().GetContents().GetActorByID(player.GetCompanionUID());
                if (actor != null)
                {
                    if (actor.GetName().Equals(element.GetAttribute("name")))
                    {
                        return true;
                    }
                }
            }
        }
        if ("hasResearch".Equals(evaluate))
        {
            if (player.GetResearchHandler().HasResearch(element.GetAttribute("research")))
            {
                return true;
            }
        }
        if ("hasEntry".Equals(evaluate))
        {
            if (player.GetResearchHandler().HasEntry(element.GetAttribute("entry")))
            {
                return true;
            }
        }
        if ("crimeReported".Equals(evaluate))
        {
            if (this.actor.GetActor().GetFaction().GetFactionRuleset().GetReportedViolation() != null)
            {
                return true;
            }
        }
        if ("canCapture".Equals(evaluate))
        {
            Item item = this.player.GetInventory().GetItem("00CAPTURE");
            if (item is ItemCaptureInstance)
            {
                ItemCaptureInstance captureDevice = (ItemCaptureInstance)item;
                if (captureDevice.GetShipName() != null)
                {
                    return CaptureTool.CanCapture(captureDevice);
                }
            }
        }
        return false;
    }

    private bool OperatorAnd(XmlElement element)
    {
        XmlNodeList children = element.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if (!AssertionEvaluator(e))
                {
                    return false;
                }
            }
        }
        return true;
    }

    private bool OperatorOr(XmlElement element)
    {
        XmlNodeList children = element.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if (AssertionEvaluator(e))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private bool ConditionalEvaluator(XmlElement element)
    {
        string evaluate = element.GetAttribute("evaluate");
        string operand = element.GetAttribute("operator");
        int value = int.Parse(element.GetAttribute("value"));
        int comparison = 0;
        if ("SCRIPT".Equals(evaluate)) 
        {
            string filename = element.GetAttribute("src");
            DialogueScriptRunnerTool scriptRunner = DialogueScriptRunnerTool.GetInstance();
            comparison = scriptRunner.RunScript(filename, this.actor, this.player);
        }
        if ("LIKENESS".Equals(evaluate))
        {
            string likeness = element.GetAttribute("likeness");
            comparison = LikenessTool.ProcessLikeness(likeness, player);
        }
        if ("BODYVALUE".Equals(evaluate))
        {
            BodyPart part = player.GetAppearance().GetPart(element.GetAttribute("bodypart"));
            if (part != null)
            {
                comparison = part.GetValue(element.GetAttribute("partvalue"));
            }
        }

        if ("GLOBALFLAG".Equals(evaluate))
        {
            string flag = element.GetAttribute("flag");
            comparison = globalFlags.ReadFlag(flag);
        }
        if ("LOCALFLAG".Equals(evaluate))
        {
            string flag = element.GetAttribute("flag");
            comparison = localFlags.ReadFlag(flag);
        }
        if ("FACTIONFLAG".Equals(evaluate))
        {
            string factionStr = element.GetAttribute("faction");
            string flag = element.GetAttribute("flag");
            Faction fac = GlobalGameState.GetInstance().GetUniverse().getFactionLibrary().GetFaction(factionStr);
            comparison = fac!=null ? fac.GetFlagField().ReadFlag(flag) : faction.GetFlagField().ReadFlag(flag);
        }
        if ("DISPOSITION".Equals(evaluate))
        {
            string factionStr = element.GetAttribute("faction");
            Faction fac = GlobalGameState.GetInstance().GetUniverse().getFactionLibrary().GetFaction(factionStr);
            comparison = fac!=null ? fac.getRelationship("player") : faction.getRelationship("player");

        }
        if ("SKILL".Equals(evaluate))
        {
            comparison = player.GetRPG().getSkill(EnumTools.strToSkill(element.GetAttribute("skill")));
        }
        if ("GOLD".Equals(evaluate))
        {
            comparison = player.GetInventory().GetGold();
        }
        
        if ("RANK".Equals(evaluate))
        {
            comparison = ((NPC)actor.GetActor()).GetRank();
        }
        if ("COMPANIONRANK".Equals(evaluate))
        {
            Actor ac = GlobalGameState.GetInstance().getCurrentZone().GetContents().GetActorByID(player.GetCompanionUID());
            comparison = actor != null ? ((NPC)ac).GetRank() : 0;
        }
        if ("CREDITS".Equals(evaluate))
        {
            comparison = player.GetInventory().GetCredits();
        }
        if ("VIOLATIONTYPE".Equals(evaluate))
        {
            comparison = (int)actor.GetActor().GetFaction().GetFactionRuleset().GetReportedViolation().violationType;
        }
        if ("VIOLATIONSEVERITY".Equals(evaluate))
        {
            comparison = actor.GetActor().GetFaction().GetFactionRuleset().GetReportedViolation().violationSeverity;
        }
        if ("KARMA".Equals(evaluate))
        {
            comparison = ((PlayerRPG)player.GetRPG()).GetKarma();
        }
        if ("HASITEM".Equals(evaluate))
        {
            comparison = player.GetInventory().CountItem(element.GetAttribute("item"));
        }
        if ("TIMEPASSED".Equals(evaluate))
        {
            string key = element.GetAttribute("key").Length > 1 ? element.GetAttribute("key") : actor.GetActor().GetUID().ToString();
            long stamp = GlobalGameState.GetInstance().GetUniverse().GetTimeKeeper().HasTimeStamp(key) ? 
                GlobalGameState.GetInstance().GetUniverse().GetTimeKeeper().GetTimeStamp(key) : 0;
            long time = GlobalGameState.GetInstance().GetUniverse().GetClock() / 1000;
            comparison = (int)(time - stamp);
        }
        return CalculateComparison(value, comparison, operand);
    }

    private bool CalculateComparison(int value, int comparison, string operand)
    {
        if (operand.Equals("equals") && value==comparison)
        {

            return true;
        }
        if (operand.Equals("greaterthan") && comparison >= value)
        {

            return true;
        }
        if (operand.Equals("lessthan") && comparison < value)
        {

            return true;
        }
        return false;
    }

    internal void SetEntity(Active_Entity entity)
    {
        this.entity = entity;
        this.faction = entity.GetController().GetData().GetFaction();
    }
}
