
using System;
using System.Collections.Generic;
using System.Xml;

public class TextRandomizer
{
    public string[] strings;

    public TextRandomizer(string[] strings)
    {
        this.strings = strings;
    }

    public string GetString()
    {
        return strings[DiceRoller.GetInstance().RollDice(strings.Length)];
    }
}

public class DialogueRandomizer
{
    private static DialogueRandomizer _instance;

    Dictionary<string, TextRandomizer> randomizers;

    public static DialogueRandomizer GetInstance()
    {
        if (_instance == null)
        {
            _instance = new DialogueRandomizer();
        }
        return _instance;
    }

    private DialogueRandomizer()
    {
        randomizers = new Dictionary<string, TextRandomizer>();
    }

    public TextRandomizer GetRandomizer(string filename)
    {
        if (!randomizers.ContainsKey(filename))
        {
            randomizers.Add(filename, BuildRandomizer(filename));
        }
        return randomizers[filename];
    }

    private TextRandomizer BuildRandomizer(string filename)
    {
        XmlDocument doc = FileTools.GetXmlDocument("textRandomizers/" + filename);
        XmlElement root = (XmlElement)doc.FirstChild.NextSibling;
        string[] strings = new string[int.Parse(root.GetAttribute("count"))];
        int index = 0;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("element".Equals(element.Name))
                {
                    strings[index] = element.InnerText;
                    index++;
                }
            }
        }
        return new TextRandomizer(strings);
    }
}