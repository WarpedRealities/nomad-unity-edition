﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using UnityEngine.SceneManagement;
using System.Runtime.CompilerServices;

public class DialogueEffectProcessor
{
    private const string PLAYER = "player";
    private FlagField localflags;
    private FlagField globalFlags;
    private Faction faction;
    private Player player;
    private Widget widget;
    private DialogueActor actor;
    private Active_Entity activeEntity;
    private DialoguePortrait portrait;
    private InventoryActionHandler inventoryActionHandler;
    private ViewInterface view;

    public DialogueEffectProcessor(FlagField flagField, FlagField globalFlags, Player player, DialoguePortrait portrait)
    {
        this.portrait = portrait;
        this.localflags = flagField;
        this.globalFlags = globalFlags;
        this.player = player;
        this.inventoryActionHandler = new InventoryActionHandler(player, null, null);
    }

    internal void SetFaction(Faction faction)
    {
        this.faction = faction;
    }

    internal void SetWidget(Widget widget)
    {
        this.widget = widget;
    }

    internal void SetActor(DialogueActor actor)
    {
        this.actor = actor;
        faction = actor.GetActor().GetFaction();
    }

    internal void ProcessEffects(XmlElement currentPage)
    {
        XmlNodeList children = currentPage.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];

                if ("effect".Equals(xmlElement.Name))
                {
                    ProcessEffect(xmlElement);
                }
                if ("special".Equals(xmlElement.Name))
                {
                    ProcessSpecialEffect(xmlElement);
                }


            }
        }
    }

    private void ProcessSpecialEffect(XmlElement xmlElement)
    {
        string type = xmlElement.GetAttribute("effect");
        if ("nonhostile".Equals(type))
        {
            ((NPC)actor.GetActor()).SetPeaceful(true);
        }
        if ("makehostile".Equals(type))
        {
            ((NPC)actor.GetActor()).SetPeaceful(false);
        }
        if ("makecompanion".Equals(type))
        {
            ((NPC)actor.GetActor()).SetIsCompanion(true);
            ((NPC)actor.GetActor()).SetFaction(player.GetFaction());
            player.SetCompanion(actor.GetActor().GetUID());
        }
        if ("moveShip".Equals(type))
        {
            Vector2Int pos = GeometryTools.GetPosition(
                (int)activeEntity.GetPosition().x,
                (int)activeEntity.GetPosition().y, 
                DiceRoller.GetInstance().RollDice(8));
            activeEntity.setPosition(pos);
        }
        if ("opendoor".Equals(type.ToLower()))
        {
            WidgetDoor widget = ZoneTools.GetDoor(xmlElement.GetAttribute("lock"));
            if (widget != null)
            {
                widget.DestroyWidget(this.view, false);
            }
        }
        if ("removeStatus".Equals(type))
        {
            int uid = int.Parse(xmlElement.GetAttribute("id"));
            player.GetRPG().GetStatusHandler().RemoveStatusEffect(uid, view, player);
        }
        if ("addStatus".Equals(type))
        {
            StatusEffect statusEffect = BuildStatus(xmlElement);
            if (statusEffect != null)
            {
                player.GetRPG().GetStatusHandler().AddStatusEffect(statusEffect);
            }
        }
        if ("removecompanion".Equals(type))
        {
            ((NPC)actor.GetActor()).SetIsCompanion(false);
            player.SetCompanion(-1);
        }

        if ("healnpc".Equals(type))
        {
            ((NPC_RPG)actor.GetActor().GetRPG()).Heal(1);
            actor.ForceUpdate();
        }
        if ("removenpc".Equals(type) && actor.getPosition().x>=0)
        {
            actor.Remove();
        }
        if ("removeWidget".Equals(type))
        {
            view.RemoveWidget(widget);
        }
        if ("seduce".Equals(type))
        {
            actor.GetActor().GetRPG().ReduceStat(ST.RESOLVE, 999);
            actor.GetActor().GetRPG().IncreaseStat(ST.HEALTH, 1);
        }
        if ("research".Equals(type))
        {
            int dc = int.Parse(xmlElement.GetAttribute("DC"));
            player.GetResearchHandler().AddResearch(dc, xmlElement.GetAttribute("data"));
        }
        if ("giveperk".Equals(type))
        {
            string code = xmlElement.GetAttribute("code");
            if (code.Length > 4)
            {
                ((PlayerRPG)player.GetRPG()).AddPerk(GlobalGameState.GetInstance().GetUniverse().GetPerkLibrary().GetPerk(code));
                ((PlayerRPG)player.GetRPG()).CalcStats();
                ((PlayerRPG)GlobalGameState.GetInstance().GetPlayer().GetRPG()).CalcEquipment();
            }
        }
        if ("marktime".Equals(type)|| "markTime".Equals(type))
        {
            string key= xmlElement.GetAttribute("key").Length > 1 ? xmlElement.GetAttribute("key"): actor.GetActor().GetUID().ToString();
            GlobalGameState.GetInstance().GetUniverse().GetTimeKeeper().SetTimeStamp(key,
                GlobalGameState.GetInstance().GetUniverse().GetClock() / 1000);
        }
        if ("destroywidget".Equals(type))
        {
            view.RemoveWidget(this.widget);
        }
        if ("replaceWidget".Equals(type))
        {
            view.ReplaceWidget(this.widget, WidgetBuilder.BuildWidget(xmlElement.GetAttribute("name")));
        }
        if ("forgiveCrime".Equals(type))
        {
            actor.GetActor().GetFaction().GetFactionRuleset().SetReportedViolation(null);
        }
        if ("applyCrimePenalty".Equals(type))
        {
            actor.GetActor().GetFaction().GetFactionRuleset().ApplyViolationPenalty();
        }
        if ("strip".Equals(type))
        {
            RunStrip(player, "true".Equals(xmlElement.GetAttribute("dropall")));
        }
        if ("shop".Equals(type))
        {
            Store store = GlobalGameState.GetInstance().GetUniverse().GetShopLibrary().GetStore(xmlElement.GetAttribute("ID"));
            GameObject sceneController = GameObject.Find("ViewSceneController");
            if (sceneController != null)
            {
                ViewScene viewScene = sceneController.GetComponent<ViewScene>();
                viewScene.SetScreen(ScreenID.NONE);
                viewScene.SetScreen(new ScreenDataShop(store));
            }
            else
            {
                sceneController = GameObject.Find("SolarSceneObj");
                SolarSceneScript solarSceneScript = 
                    sceneController.GetComponent<SolarSceneScript>();
                solarSceneScript.SetScreen(null);
                GameObject shop = (GameObject)GameObject.Instantiate(Resources.Load("ItemShopScreen"), null);
                shop.SendMessage("SetShop", new ScreenDataShop(store));
                shop.SendMessage("SetPlayer", GlobalGameState.GetInstance().GetPlayer());
                GameObject canvas = GameObject.Find("Canvas");
                shop.transform.SetParent(canvas.transform, false);
                solarSceneScript.SetScreen(shop);
            }
        }
        if ("transition".Equals(type))
        {
            ViewScene viewScene = GameObject.Find("ViewSceneController").GetComponent<ViewScene>();
            viewScene.GetController().Transition(xmlElement.GetAttribute("destination"), int.Parse(xmlElement.GetAttribute("x")), int.Parse(xmlElement.GetAttribute("y")));
        }
        if ("portrait".Equals(type))
        {
            if (Config.GetInstance().IsShowPortraits())
            {
                portrait.SetPortrait(xmlElement.GetAttribute("image"));
            }
        }
        if ("unlockRecipe".Equals(type))
        {
            GlobalGameState.GetInstance().GetUniverse().GetCraftingLibrary().UnlockRecipe(xmlElement.GetAttribute("recipe"));
        }
        if ("spaceCombat".Equals(type)) {

            GameObject gameObject = (GameObject)GameObject.Instantiate(Resources.Load("Encounter"));
            gameObject.name = "Encounter";
            Encounter encounter = gameObject.GetComponent<Encounter>();
            Active_Entity[] entities = new Active_Entity[] { activeEntity };  
            encounter.SetEncounter(new EncounterData(activeEntity.GetController().GetData(),
                entities, (Spaceship)GlobalGameState.GetInstance().getCurrentEntity(), GlobalGameState.GetInstance().GetPlayer()));
        }
        if ("dock".Equals(type))
        {
            ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool((Spaceship)GlobalGameState.GetInstance().getCurrentEntity(), true);
            LaunchLandTool launchLandTool = new LaunchLandTool();
            launchLandTool.Dock(shipAnalysisTool, (Spaceship)this.activeEntity);
        }
        if ("removeShip".Equals(type))
        {
            this.activeEntity.SetInGame(false);
        }
        if ("viewScene".Equals(type))
        {
            ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool((Spaceship)GlobalGameState.GetInstance().getCurrentEntity(), true);
            shipAnalysisTool.ReconcileStats();
            SceneManager.LoadScene("ViewScene");
        }
        if ("worldScript".Equals(type))
        {
            WorldScriptTool worldScriptTool = new WorldScriptTool();
            worldScriptTool.Run(xmlElement.GetAttribute("script"));
        }
        if ("captureNPC".Equals(type))
        {
            CaptureTool.CaptureNPC(this.actor, this.player);
            actor.Remove();
        }
        if ("removeCaptive".Equals(type)) {
            CaptureTool.RemoveNPC(this.actor.GetActor(), this.widget);
        }
        if ("mutation".Equals(type))
        {
            HandleMutations(xmlElement);

        }
        if ("createNPC".Equals(type))
        {
            string filename = xmlElement.GetAttribute("file");
            NPC npc = new NPC(ActorTools.loadNPC(filename), filename);
            Zone z = GlobalGameState.GetInstance().getCurrentZone();
            Vector2Int p = player.GetPosition();
            Vector2Int position = CollisionTools.GetFreeTile(p);
            npc.SetPosition(position);
            z.GetContents().GetActors().Add(npc);
            view.AddNPC(npc);
        }
    }

    private StatusEffect BuildStatus(XmlElement root)
    {
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("buff".Equals(element.Name) || "debuff".Equals(element.Name))
                {
                    return StatusEffectLoader.buildModifier(element);
                }
                if ("bind".Equals(element.Name))
                {
                    return StatusEffectLoader.buildBind(element);
                }
                if ("stealth".Equals(element.Name))
                {
                    return new StatusStealth(int.Parse(element.GetAttribute("uid")), int.Parse(element.GetAttribute("strength")));
                }
                if ("damageOverTime".Equals(element.Name) || "regeneration".Equals(element.Name))
                {
                    return StatusEffectLoader.buildDOT(element);
                }
                if ("statusImmune".Equals(element.Name))
                {
                    return StatusEffectLoader.buildImmunity(element);
                }
            }
        }
        return null;
    }
    private void HandleMutations(XmlElement root)
    {
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("mutation".Equals(element.Name))
                {
                    Mutation mutation = MutatorLoader.BuildMutation(element);
                    mutation.ApplyMutation(player.GetAppearance());
                }
                if ("criteria".Equals(element.Name))
                {
                    string eval = element.GetAttribute("evaluate");
                    if ("HASPART".Equals(eval))
                    {
                        bool negative = "true".Equals(element.GetAttribute("negative"));
                        if (!negative && player.GetAppearance().GetPart(element.GetAttribute("part"))==null)
                        {
                            return;
                        }
                        if (negative && player.GetAppearance().GetPart(element.GetAttribute("part")) != null)
                        {
                            return;
                        }
                    }
                }
            }

        }
    }

    private void RunStrip(Player player, bool drop)
    {
        for (int i = 0; i < 4; i++)
        {
            if (player.GetInventory().GetSlot(i) != null)
            {
                inventoryActionHandler.HandleInventory(new ActionInventory(player.GetInventory().GetSlot(i), InventoryAction.UNEQUIP, 0),view);
            }
        }
        if (drop)
        {
            while (player.GetInventory().GetItemCount() > 0)
            {
                inventoryActionHandler.HandleInventory(new ActionInventory(player.GetInventory().GetItem(0), InventoryAction.DROP, 0), view);
            }
        }
    }

    private void ProcessEffect(XmlElement xmlElement)
    {
        string type = xmlElement.GetAttribute("type");

        if ("setglobalflag".Equals(type))
        {
            globalFlags.SetFlag(xmlElement.GetAttribute("flag"), int.Parse(xmlElement.GetAttribute("value")));
        }
        if ("setlocalflag".Equals(type))
        {
            localflags.SetFlag(xmlElement.GetAttribute("flag"), int.Parse(xmlElement.GetAttribute("value")));
        }
        if ("setfactionflag".Equals(type))
        {
            faction.GetFlagField().SetFlag(xmlElement.GetAttribute("flag"), int.Parse(xmlElement.GetAttribute("value")));
        }
        if ("setflagforfaction".Equals(type))
        {
            GlobalGameState.GetInstance().GetUniverse()
                .getFactionLibrary().GetFaction(xmlElement.GetAttribute("faction")).GetFlagField()
                .SetFlag(xmlElement.GetAttribute("flag"), int.Parse(xmlElement.GetAttribute("value")));
        }
        if ("incrementflagforfaction".Equals(type))
        {
            GlobalGameState.GetInstance().GetUniverse()
                .getFactionLibrary().GetFaction(xmlElement.GetAttribute("faction")).GetFlagField()
                .IncrementFlag(xmlElement.GetAttribute("flag"), int.Parse(xmlElement.GetAttribute("value")));
        }
        if ("companionpromote".Equals(type))
        {
            int value = int.Parse(xmlElement.GetAttribute("value"));
            Actor ac = GlobalGameState.GetInstance().getCurrentZone().GetContents().GetActorByID(player.GetCompanionUID());
            if (ac != null)
            {
                NPC npc = (NPC)ac;
                if (npc.GetRank() == value - 1)
                {
                    npc.SetRank(value);
                }
            }
        }
        if ("incrementlocalflag".Equals(type))
        {
            localflags.IncrementFlag(xmlElement.GetAttribute("flag"), int.Parse(xmlElement.GetAttribute("value")));
        }
        if ("giveitem".Equals(type))
        {
            Item item = LootItemGenerator.BuildItem(xmlElement.GetAttribute("code"), xmlElement.GetAttribute("addendum"));
            player.GetInventory().AddItems(item,
                int.Parse(xmlElement.GetAttribute("value")));
        }
        if ("stunnpc".Equals(type.ToLower()))
        {
            this.actor.GetController().addAction(new ActionDelay(10*int.Parse(xmlElement.GetAttribute("value")),true,true));
        }
        if ("removeItem".Equals(type) || "removeitem".Equals(type))
        {
            Item item = player.GetInventory().GetItem(xmlElement.GetAttribute("item"));
            int value = int.Parse(xmlElement.GetAttribute("value"));
            for (int i = 0; i < value; i++)
            {
                player.GetInventory().RemoveItem(item);
            }
        }
        if ("giveperk".Equals(type))
        {
            string code = xmlElement.GetAttribute("code");
            Debug.Log("give perk" + code);
            if (code.Length > 4)
            {
                ((PlayerRPG)player.GetRPG()).AddPerk(GlobalGameState.GetInstance().GetUniverse().GetPerkLibrary().GetPerk(code));
                ((PlayerRPG)player.GetRPG()).CalcStats();
                ((PlayerRPG)GlobalGameState.GetInstance().GetPlayer().GetRPG()).CalcEquipment();
            }
        }
        if ("experience".Equals(type))
        {
            ((PlayerRPG)player.GetRPG()).AddExperience(int.Parse(xmlElement.GetAttribute("value")),null);
        }
        if ("heal".Equals(type))
        {
            float v = float.Parse(xmlElement.GetAttribute("value"));
            player.GetRPG().Heal(v);
            actor.ForceUpdate();
        }
        if ("feed".Equals(type))
        {
            ((PlayerRPG)player.GetRPG()).Feed(int.Parse(xmlElement.GetAttribute("value")),false);
        }
        if ("nourish".Equals(type))
        {
            ((PlayerRPG)player.GetRPG()).Feed(int.Parse(xmlElement.GetAttribute("value")), true);
        }
        if ("stunnpc".Equals(type))
        {
            int duration = int.Parse(xmlElement.GetAttribute("value")) * 10;
            actor.GetController().addAction(new ActionDelay(duration,true,true));
        }
        if ("modifyGold".Equals(type) || "givegold".Equals(type))
        {
            int amount = int.Parse(xmlElement.GetAttribute("value"));
            player.GetInventory().SetGold(player.GetInventory().GetGold() + amount);
        }

        if ("modifyCredits".Equals(type))
        {
            int amount = int.Parse(xmlElement.GetAttribute("value"));
            player.GetInventory().SetCredits(player.GetInventory().GetCredits() + amount);
        }
        if ("modFactionDisposition".Equals(type))
        {
            Debug.Log("mod disposition " + xmlElement.GetAttribute("value"));
            faction.ModifyRelationship(DialogueEffectProcessor.PLAYER, int.Parse(xmlElement.GetAttribute("value")));
        }
        if ("shop".Equals(type))
        {

            Store store= GlobalGameState.GetInstance().GetUniverse().GetShopLibrary().GetStore(xmlElement.GetAttribute("ID"));
            ViewScene viewScene = GameObject.Find("ViewSceneController").GetComponent<ViewScene>();
            viewScene.SetScreen(ScreenID.NONE);
            viewScene.SetScreen(new ScreenDataShop(store));
        }
        if ("imprison".Equals(type))
        {
            bool suppress = "true".Equals(xmlElement.GetAttribute("suppress"));
            int amount = int.Parse(xmlElement.GetAttribute("value"));
            GlobalGameState.GetInstance().GetUniverse().IncrementClock(amount * 10);
            view.GetRegenerationTicker().Update(amount*10, suppress);
            actor.GetController().addAction(new ActionDelay(10, true, true));
            //view.GetPlayerInZone().GetController().addAction(new ActionDelay(10 * amount, true, true));
            //if (suppress)
            //{
            //    player.SetSuppression(amount*100);
            //}
            //actor.GetController().addAction(new ActionDelay(amount*11, true, true));
        }

    }

    internal void SetView(ViewInterface viewInterface)
    {
        this.view = viewInterface;
    }

    internal void SetEntity(Active_Entity entity)
    {
        this.activeEntity = entity;
        this.faction = entity.GetController().GetData().GetFaction();
    }
}
