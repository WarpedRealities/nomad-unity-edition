using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class SystemWeaponPanel : MonoBehaviour
{
    WeaponSystem weaponSystem;
    public TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void SetWeapon(WeaponSystem weaponSystem)
    {
        this.weaponSystem = weaponSystem;
        BuildUI();
    }

    public void UpdateUI()
    {

    }

    private void BuildUI()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(weaponSystem.GetName() + " facing:"+ EnumTools.FacingToStr(weaponSystem.GetFacing())+ " arc:"+EnumTools.ArcToStr(weaponSystem.GetWeapon().GetArc())+"\n");
        stringBuilder.Append(weaponSystem.GetWeapon().GetDescription() + "\n");
        stringBuilder.Append(weaponSystem.GetWeapon().GetEffect() + "\n");
        stringBuilder.Append("cost:" +weaponSystem.GetWeapon().GetCosts() + "\n");
        text.text = stringBuilder.ToString();
    }
}
