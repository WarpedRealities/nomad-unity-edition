using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DamagedSystemScreen : MonoBehaviour
{
    ScreenDataShipSystem screenDataShipSystem;
    private ZoneActor playerInZone;
    private Player player;
    private ViewInterface view;
    public TextMeshProUGUI description, requires, amount;
    public Button repairButton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            view.SetScreen(ScreenID.NONE);
        }
    }


    public void SetSystem(ScreenDataShipSystem screenDataShipSystem)
    {
        this.screenDataShipSystem = screenDataShipSystem;
    }
    public void SetPlayer(ZoneActor zoneActor)
    {
        this.playerInZone = zoneActor;
        this.player = (Player)zoneActor.actor;
        BuildUI();
    }


    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    private void BuildUI()
    {
        WidgetShipSystem widgetShipSystem = (WidgetShipSystem)this.screenDataShipSystem.GetSystem();
        description.text = widgetShipSystem.GetName() + " is damaged and inoperable";
        string itemName = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary()
            .GetDefByCode(widgetShipSystem.GetRequirement().GetCode()).GetName();
        requires.text = "You need " + widgetShipSystem.GetRequirement().GetCount().ToString() + " " +
            itemName+" to repair it";
        int count = player.GetInventory().CountItem(widgetShipSystem.GetRequirement().GetCode());
        amount.text = "you have " + count.ToString();
        if (count < widgetShipSystem.GetRequirement().GetCount())
        {
            repairButton.enabled = false;
        }
    }

    public void RepairClick()
    {
        WidgetShipSystem widgetShipSystem = (WidgetShipSystem)this.screenDataShipSystem.GetSystem();
        //subtract items
        for (int i = 0; i < widgetShipSystem.GetRequirement().GetCount(); i++)
        {
            player.GetInventory().RemoveItem(player.GetInventory().GetItem(widgetShipSystem.GetRequirement().GetCode()));
        }
        widgetShipSystem.SetLastInteraction(GlobalGameState.GetInstance().GetUniverse().GetClock());
        widgetShipSystem.SetDamaged(false);
        view.CalculateVision();
        view.SetScreen(ScreenID.NONE);
    }

    public void ExitClick()
    {
        view.SetScreen(ScreenID.NONE);
    }
}
