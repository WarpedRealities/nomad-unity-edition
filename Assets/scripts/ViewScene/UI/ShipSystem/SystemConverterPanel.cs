﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class SystemConverterPanel : MonoBehaviour
{

    public Text description;
    public Button button;
    private Text buttonText;
    ConversionSystem conversionSystem;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void SetConversionSystem(ConversionSystem conversionSystem)
    {
        this.conversionSystem = conversionSystem;
        BuildUI();
    }

    private void BuildUI()
    {
        this.description.text = conversionSystem.GetDescription();
        buttonText = this.button.GetComponentInChildren<Text>();
        SetupButton();
    }

    public void UpdateUI()
    {
        BuildUI();
    }

    private void SetupButton()
    {
        buttonText.text = this.conversionSystem.IsActive() ? "Turn off" : "Turn on";
    }

    public void ClickButton()
    {
        this.conversionSystem.SetActive(!conversionSystem.IsActive());
        SetupButton();
    }
}
