﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemChargerPanel : MonoBehaviour
{

    FillerSystem fillerSystem;
    public Text title;
    public Text[] itemTexts;
    public Button[] buttons;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKey(KeyCode.Keypad1) || Input.GetKey(KeyCode.Alpha1)) && this.buttons[0].enabled)
        {
            RemoveItem(0);
        }
        if ((Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.Alpha2)) && this.buttons[1].enabled)
        {
            RemoveItem(1);
        }
        if ((Input.GetKey(KeyCode.Keypad3) || Input.GetKey(KeyCode.Alpha3)) && this.buttons[2].enabled)
        {
            RemoveItem(2);
        }
        if ((Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.Alpha4)) && this.buttons[3].enabled)
        {
            RemoveItem(3);
        }
    }

    public void SetFillerSystem(FillerSystem fillerSystem)
    {
        this.fillerSystem = fillerSystem;
        BuildUI();
    }

    public void RemoveItem(int index)
    {
        GlobalGameState.GetInstance().GetPlayer().GetInventory().AddItem(fillerSystem.GetItems()[index]);
        fillerSystem.GetItems()[index] = null;
        transform.parent.SendMessage("UpdateUI");
        UpdateUI();
    }

    public void UpdateUI()
    {
        BuildUI();
    }

    private void BuildUI()
    {
        title.text = fillerSystem.GetResource() + " charger";

        for (int i = 0; i < 4; i++)
        {
            if (i < fillerSystem.GetItemCount())
            {
                if (fillerSystem.GetItems()[i] != null)
                {
                    itemTexts[i].text = fillerSystem.GetItems()[i].GetItemString();
                    buttons[i].gameObject.SetActive(true);
                    buttons[i].enabled = true;
                }
                else
                {
                    itemTexts[i].text = "Empty";
                    buttons[i].gameObject.SetActive(false);
                    buttons[i].enabled = false;
                }
            }
            else
            {
                buttons[i].enabled = false;
                buttons[i].gameObject.SetActive(false);
                itemTexts[i].enabled = false;
            }

        }
    }
}
