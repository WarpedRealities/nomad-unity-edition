﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ShipSystemScreen : MonoBehaviour
{
    ZoneActor playerInZone;
    Player player;
    ViewInterface view;
    ScreenDataShipSystem screenDataShipSystem;
    private GameObject[] systemTabs;
    public NuList list;
    private ListCallback callback;
    // Use this for initialization
    void Start()
    {
        callback = ListCallback;
        list.SetCallback(callback);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            view.SetScreen(ScreenID.NONE);
        }
    }

    public void ListCallback(int index)
    {
        if (index < 0)
        {
            return;
        }
        if (index < this.player.GetInventory().GetItemCount())
        {
            Item item = player.GetInventory().GetItems()[index];

            for (int i = 0; i < screenDataShipSystem.GetSystem().GetSystems().Length; i++)
            {
                if (screenDataShipSystem.GetSystem().GetSystems()[i].AddItem(item))
                {
                    player.GetInventory().RemoveItem(player.GetInventory().GetItems()[index]);
                    UpdateUI();
                    break;
                }
            }
        }
    }

    public void SetSystem(ScreenDataShipSystem screenDataShipSystem)
    {
        this.screenDataShipSystem = screenDataShipSystem;
    }

    public void SetPlayer(ZoneActor zoneActor)
    {
        this.playerInZone = zoneActor;
        this.player = (Player)zoneActor.actor;
        BuildUI();
    }

    private void BuildUI()
    {
        systemTabs = new GameObject[3];
        List<string> playerList = new List<string>();
        for (int i = 0; i < player.GetInventory().GetItems().Count; i++)
        {
            playerList.Add(player.GetInventory().GetItems()[i].GetItemString());
        }
        list.BroadcastMessage("SetList", playerList);
 
        for (int i = 0; i < screenDataShipSystem.GetSystem().GetSystems().Length; i++)
        {

            systemTabs[i] = BuildPanel(screenDataShipSystem.GetSystem().GetSystems()[i], i);
        }
    }

    private void UpdateUI()
    {
        List<string> playerList = new List<string>();
        for (int i = 0; i < player.GetInventory().GetItems().Count; i++)
        {
            playerList.Add(player.GetInventory().GetItems()[i].GetItemString());
        }
        list.BroadcastMessage("SetList", playerList);
        for (int i = 0; i < 3; i++)
        {
            if (systemTabs[i] != null)
            {
                systemTabs[i].SendMessage("UpdateUI");
            }
        }
    }

    private GameObject BuildPanel(ShipSystem shipSystem, int index)
    {
        RectTransform tr = GetComponent<RectTransform>();
        GameObject p = null;
        if (shipSystem.GetSystemType() == SHIPSYSTEMTYPE.RESOURCE)
        {
            ResourceSystem resourceSystem = (ResourceSystem)shipSystem;
            p = (GameObject)Instantiate(Resources.Load("SystemResourcePanel"));
            //   r.sizeDelta = new Vector2(-0.5F, -0.5F);
            p.GetComponent<SystemResourcePanel>().SetResourceSystem((ResourceSystem)shipSystem);
        }
        if (shipSystem.GetSystemType() == SHIPSYSTEMTYPE.FILLER)
        {
            FillerSystem fillerSystem = (FillerSystem)shipSystem;
            p = (GameObject)Instantiate(Resources.Load("SystemChargerPanel"));
            //   r.sizeDelta = new Vector2(-0.5F, -0.5F);
            p.GetComponent<SystemChargerPanel>().SetFillerSystem((FillerSystem)shipSystem);
        }
        if (shipSystem.GetSystemType() == SHIPSYSTEMTYPE.CONVERTER)
        {
            ConversionSystem conversionSystem = (ConversionSystem)shipSystem;
            p = (GameObject)Instantiate(Resources.Load("SystemConverterPanel"));
            p.GetComponent<SystemConverterPanel>().SetConversionSystem(conversionSystem);
        }
        if (shipSystem.GetSystemType() == SHIPSYSTEMTYPE.MAGAZINE)
        {
            MagazineSystem magazineSystem = (MagazineSystem)shipSystem;
            p = (GameObject)Instantiate(Resources.Load("SystemMagazinePanel"));
            p.GetComponent<SystemMagazinePanel>().SetMagazine(magazineSystem);
        }
        if (shipSystem.GetSystemType() == SHIPSYSTEMTYPE.WEAPON)
        {
            WeaponSystem weaponSystem = (WeaponSystem)shipSystem;
            p = (GameObject)Instantiate(Resources.Load("SystemWeaponPanel"));
            p.GetComponent<SystemWeaponPanel>().SetWeapon(weaponSystem);
        }
        if (p != null)
        {
            p.transform.SetParent(gameObject.transform, false);
            RectTransform r = p.GetComponent<RectTransform>();
            //need to set the position properly
            r.anchoredPosition = new Vector2(-516, -200 - (200 * index));
            r.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 580);
            r.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 200);
        }

        return p;
    }

    public void SetView(ViewInterface view)
    {
        this.view = view;
    }

    public void ClickExit()
    {
        view.SetScreen(ScreenID.NONE);
    }
}
