﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemResourcePanel : MonoBehaviour
{
    ResourceSystem resourceSystem;
    public Text text, title;
    public Slider slider;
    public PowerBar powerBar;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetResourceSystem(ResourceSystem resourceSystem)
    {
        this.resourceSystem = resourceSystem;
        BuildUI();
    }

    public void UpdateUI()
    {
        BuildUI();
    }

    private void BuildUI()
    {
        title.text = "Resource " + this.resourceSystem.GetDisplay();
        powerBar.Set(resourceSystem.GetAmount(), resourceSystem.GetCapacity());
        text.text = resourceSystem.GetAmount().ToString("F1") + "/" + resourceSystem.GetCapacity();
    }
}
