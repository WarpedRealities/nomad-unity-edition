﻿using UnityEngine;
using System.Collections;
using System;
using TMPro;

public class SystemMagazinePanel : MonoBehaviour
{
    public TextMeshProUGUI text, title;
    MagazineSystem magazineSystem;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void SetMagazine(MagazineSystem magazineSystem)
    {
        this.magazineSystem = magazineSystem;
        BuildUI();
    }

    private void BuildUI()
    {
        title.text = "Ammunition magazine";
        text.text = magazineSystem.GetAmmo() + " " + magazineSystem.GetAmount() + "/" + magazineSystem.GetCapacity();
    }

    public void UpdateUI()
    {
        BuildUI();
    }

    public void TakeButton()
    {
        if (magazineSystem.GetAmount() > 0)
        {
            Item item = magazineSystem.TakeItem();
            GlobalGameState.GetInstance().GetPlayer().GetInventory().AddItem(item);
        }
        if (magazineSystem.GetAmount() == 0)
        {
            magazineSystem.ClearCode();
        }
        transform.parent.SendMessage("UpdateUI");
    }

    public void EmptyButton()
    {
        if (magazineSystem.GetAmount() > 0)
        {
            Item item = magazineSystem.TakeAll();
            GlobalGameState.GetInstance().GetPlayer().GetInventory().AddItem(item,true);

            magazineSystem.ClearCode();
        }
        transform.parent.SendMessage("UpdateUI");
    }
}
