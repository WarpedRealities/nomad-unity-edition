﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NewSavePanel : MonoBehaviour
{
    public InputField inputField;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveClick()
    {
        if (SaveTool.Save(inputField.text))
        {
            SceneManager.LoadScene("ViewScene");
        }
    }

    public void BackClick()
    {
        GameObject obj = transform.parent.Find("SavePanel").gameObject;
        obj.SetActive(true);
        gameObject.SetActive(false);
    }
}
