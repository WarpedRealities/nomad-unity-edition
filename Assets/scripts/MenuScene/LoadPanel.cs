﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadPanel : MonoBehaviour
{
    public GameObject extraPanel;
    public NuList saveList;
    public Text saveText;
    private List<string> filenames;
    int index = -1;
    private bool showExtra;
    // Start is called before the first frame update
    void Start()
    {
        ListCallback listCallback = SelectSave;
        saveList.SetCallback(listCallback);
    }

    void Initialize()
    {
        filenames = new List<string>();
        string[] fstrings = Directory.GetDirectories("saves");
     
        for (int i = 0; i < fstrings.Length; i++)
        {
            filenames.Add(fstrings[i].Replace("saves\\", ""));
        }
        saveList.SetList(filenames);
        if (filenames.Count >= 1)
        {
            SelectSave(0);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SelectSave(int index)
    {
        this.index = index;
        this.saveText.text = filenames[index];
    }

    public void ExitClick()
    {
        transform.parent.Find("MenuPanel").gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void LoadClick()
    {
        if (index != -1)
        {
            if (LoadTool.Load(filenames[index]))
            {
                GlobalGameState.GetInstance().SetPlaying(true);
                SceneManager.LoadScene("ViewScene");
            }
        }

    }

    public void ToggleExtra()
    {
        this.showExtra = !this.showExtra;
        this.extraPanel.SetActive(showExtra);
    }

    public void ResetClick()
    {
        if (index != -1)
        {
            if (LoadTool.Load(filenames[index],true))
            {
                GlobalGameState.GetInstance().SetPlaying(true);
                SceneManager.LoadScene("ViewScene");
            }
        }
    }
}
