﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPanel : MonoBehaviour
{
    public Button[] buttons;
    private static int[] inPlayButtons = { 0, 2 };
    private GameObject confirmPanel;
    float clock = 0;

    // Start is called before the first frame update
    void Start()
    {
        confirmPanel = transform.parent.Find("ConfirmationPanel").gameObject;
        if (!GlobalGameState.GetInstance().IsPlaying())
        {
            //disable continue and save game buttons
            buttons[inPlayButtons[0]].interactable = false;
            buttons[inPlayButtons[1]].interactable = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        clock += Time.deltaTime;
        if (clock > 0.5F)
        {
            if (Input.GetKey(KeyCode.Escape) && GlobalGameState.GetInstance().IsPlaying())
            {
                SceneManager.LoadScene("ViewScene");
            }
        }
    }

    public void ContinueClick()
    {
        SceneManager.LoadScene("ViewScene");
    }

    public void SaveClick()
    {
        GameObject obj = transform.parent.Find("SavePanel").gameObject;
        obj.SetActive(true);
        obj.SendMessage("Initialize");
        gameObject.SetActive(false);
    }
    public void LoadClick()
    {
        GameObject obj = transform.parent.Find("LoadPanel").gameObject;
        obj.SetActive(true);
        obj.SendMessage("Initialize");
        gameObject.SetActive(false);
    }
    public void ExitClick()
    {
        if (GlobalGameState.GetInstance().IsPlaying())
        {
            confirmPanel.SetActive(true);
            confirmPanel.SendMessage("SetState", ConfirmState.EXIT);
            gameObject.SetActive(false);
        }
        else
        {
            Application.Quit();
        }

    }

    public void NewGameClick()
    {
        if (GlobalGameState.GetInstance().IsPlaying())
        {
            confirmPanel.SetActive(true);
            confirmPanel.SendMessage("SetState", ConfirmState.NEWGAME);
            gameObject.SetActive(false);
        }else
        {
            SceneManager.LoadScene("NewGameScene");
        }
    }

    public void PreferenceClick()
    {
        transform.parent.Find("PreferencePanel").gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
    public void OptionClick()
    {
        transform.parent.Find("OptionsPanel").gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
