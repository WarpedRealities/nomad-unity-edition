﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SavePanel : MonoBehaviour
{
    public NuList saveList;
    public Text saveText;
    private List<string> filenames;
    int index = -1;

    // Start is called before the first frame update
    void Start()
    {
        ListCallback listCallback= SelectSave;
        saveList.SetCallback(listCallback);
    }

    void Initialize()
    {
        filenames = new List<string>();
        string [] fstrings = Directory.GetDirectories("saves");
        for (int i = 0; i < fstrings.Length; i++)
        {
            filenames.Add(fstrings[i].Replace("saves\\",""));
        }
        saveList.SetList(filenames);



    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectSave(int index)
    {
        this.index = index;
        this.saveText.text = filenames[index];
    }

    public void ExitClick()
    {
        transform.parent.Find("MenuPanel").gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void SaveClick()
    {
        if (index != -1)
        {
            if (SaveTool.Save(filenames[index]))
            {
                SceneManager.LoadScene("ViewScene");
            }
        }
    }

    public void NewSaveClick()
    {
        transform.parent.Find("NewSavePanel").gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
