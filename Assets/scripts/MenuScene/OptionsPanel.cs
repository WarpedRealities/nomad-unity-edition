using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class OptionsPanel : MonoBehaviour
{
    public TextMeshProUGUI gameSpeed, inputWindow;
    public TextMeshProUGUI[] buttonTexts;
    // Start is called before the first frame update
    void Start()
    {
        BuildUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BuildUI()
    {
        this.buttonTexts[0].text = Config.GetInstance().IsShowSkillChecks() ? "1" : "0";
        this.buttonTexts[1].text = Config.GetInstance().IsShowDamageRolls() ? "1" : "0";
        this.buttonTexts[2].text = Config.GetInstance().IsShowClock() ? "1" : "0";
        this.buttonTexts[3].text = Config.GetInstance().IsShowPortraits() ? "1" : "0";
        gameSpeed.text = Config.GetInstance().GetSkipTicks().ToString();
        inputWindow.text = string.Format("{0:0.##}", Config.GetInstance().GetInputWindow());
    }

    public void ExitClick()
    {
        transform.parent.Find("MenuPanel").gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OptionButton(int value)
    {
        switch (value)
        {
            case 0:
                Config.GetInstance().SetSkillCheck(!Config.GetInstance().IsShowSkillChecks());
                break;
            case 1:
                Config.GetInstance().SetDamageRolls(!Config.GetInstance().IsShowDamageRolls());
                break;
            case 2:
                Config.GetInstance().SetClock(!Config.GetInstance().IsShowClock());
                break;
            case 3:
                Config.GetInstance().SetPortraits(!Config.GetInstance().IsShowPortraits());
                break;
        }

        BuildUI();
    }

    public void AddSpeed()
    {
        if (Config.GetInstance().GetSkipTicks() < 10)
        {
            Config.GetInstance().SetSkipTicks(Config.GetInstance().GetSkipTicks() + 1);
            BuildUI();
        }
    }

    public void ReduceSpeed()
    {
        if (Config.GetInstance().GetSkipTicks() > 1)
        {
            Config.GetInstance().SetSkipTicks(Config.GetInstance().GetSkipTicks() - 1);
            BuildUI();
        }

    }

    public void IncreaseWindow()
    {
        if (Config.GetInstance().GetInputWindow() < 0.99F)
        {
            Config.GetInstance().SetInputWindow(Config.GetInstance().GetInputWindow() + 0.01F);
            BuildUI();
        }
    }

    public void ReduceWindow()
    {
        if (Config.GetInstance().GetInputWindow() > 0.01F)
        {
            Config.GetInstance().SetInputWindow(Config.GetInstance().GetInputWindow() - 0.01F);
            BuildUI();
        }
    }
}
