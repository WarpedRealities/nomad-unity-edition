using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SubTitle : MonoBehaviour
{
    static string[] subTitles = new string[]
    {
        "the vore game","II: The Voyage Home", "the remake", "not really a sequel", "new and improved","*may contain nuts",
        "unity edition","In space no one can hear you churn","captain's edition","strange adventures in hungry space",
        "infinite voyages in voracious space","prepare to digest edition","not endorsed by pepsi max","subtitled for her pleasure",
        "III: the quest for Earth", "future imperfect","millenium edition","praise armok","powered by Unity"
    };
    public TextMeshProUGUI textMesh;
    // Start is called before the first frame update
    void Start()
    {
        int r = DiceRoller.GetInstance().RollDice(subTitles.Length);
        textMesh.text = subTitles[r];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
