﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreferencePanel : MonoBehaviour
{
    public NuList leftList, rightList;

    private PreferenceManager preferenceManager;

    // Start is called before the first frame update
    void Start()
    {
        preferenceManager = new PreferenceManager();
        leftList.SetList(preferenceManager.GetForbidden());
        rightList.SetList(preferenceManager.GetPreferences());
        ListCallback leftCallback = RemovePref;
        ListCallback rightCallback = AddPref;
        leftList.SetCallback(leftCallback);
        rightList.SetCallback(rightCallback);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RemovePref(int index)
    {
        preferenceManager.RemovePref(index);
        leftList.SetList(preferenceManager.GetForbidden());
    }

    public void AddPref(int index)
    {
        preferenceManager.AddPref(index);
        leftList.SetList(preferenceManager.GetForbidden());
    }

    public void ScanButton()
    {

        preferenceManager.Scan();
        rightList.SetList(preferenceManager.GetPreferences());
    }

    public void ExitButton()
    {
        Exit();
    }

    public void SaveAndExitButton()
    {
        //save the files
        preferenceManager.Save();

        Exit();
    }

    private void Exit()
    {
        transform.parent.Find("MenuPanel").gameObject.SetActive(true);
       // GameObject.Find("MenuPanel").SetActive(true);
        gameObject.SetActive(false);
    }
}
