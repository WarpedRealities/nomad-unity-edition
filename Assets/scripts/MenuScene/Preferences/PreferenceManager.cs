﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PreferenceManager 
{

    List<string> forbiddenPrefs;
    PreferenceScanner preferenceScanner;

    public PreferenceManager()
    {
        preferenceScanner = new PreferenceScanner();
        forbiddenPrefs = PreferenceLoader.Load();
        preferenceScanner.ReadMasterList();
    }

    public void Scan()
    {
        preferenceScanner.ScanDialogueFiles();
    }

    public List<string> GetForbidden()
    {
        return forbiddenPrefs;
    }

    public List<string> GetPreferences()
    {
        return preferenceScanner.GetPreferences();
    }

    internal void Save()
    {
        preferenceScanner.Save();
        PreferenceLoader.Save(forbiddenPrefs);
        if (GlobalGameState.GetInstance().IsPlaying())
        {
            GlobalGameState.GetInstance().GetUniverse().GetPreferences().Reload();
        }

    }

    internal void RemovePref(int index)
    {
        forbiddenPrefs.RemoveAt(index);
    }

    internal void AddPref(int index)
    {
        if (!forbiddenPrefs.Contains(preferenceScanner.GetPreference(index))) {
            forbiddenPrefs.Add(preferenceScanner.GetPreference(index));
        }
    }
}
