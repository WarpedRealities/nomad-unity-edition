﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System;
using System.Text.RegularExpressions;

public class PreferenceScanner 
{
    Dictionary<string, int> scannedPreferences;
    List<string> orderedPreferences;
    private static string FOLDERNAME = "gameData/data/conversations";
    private static string FILENAME = "gameData/data/masterlist.txt";
    public void ReadMasterList()
    {
        scannedPreferences = new Dictionary<string, int>();

        string masterlist= FileTools.GetFileRaw(FILENAME);
        if (masterlist == null)
        {
            return;
        }
        string[] segmentedMasterlist = masterlist.Split(',');

        for (int i = 0; i < segmentedMasterlist.Length; i += 2)
        {
            string pref = segmentedMasterlist[i];
            int count = int.Parse(segmentedMasterlist[i + 1]);
            scannedPreferences.Add(pref, count);
        }
        BuildOrderedPreferences();
    }

    internal void Save()
    {
        //write master list

        //if (File.Exists(FILENAME))
        //   FileStream fileStream = File.Open("masterlist.txt",FileMode.OpenOrCreate);
        using (StreamWriter streamWriter = new StreamWriter(FILENAME, false))
        {
            string [] keyCollection=new string[scannedPreferences.Keys.Count];
            scannedPreferences.Keys.CopyTo(keyCollection,0);
            for (int i = 0; i < keyCollection.Length; i++)
            {
                streamWriter.Write(keyCollection[i] + "," + scannedPreferences[keyCollection[i]]);
                if (i < keyCollection.Length - 1)
                {
                    streamWriter.Write(",");
                }
            }

        }
    }

    public List<string> GetPreferences()
    {
        return orderedPreferences;
    }

    public string GetPreference(int index)
    {
        string str = orderedPreferences[index];
        
        return Regex.Replace(str, @"[\d-\s]", string.Empty);
    }

    private void BuildOrderedPreferences()
    {
        orderedPreferences = new List<string>();
        foreach (KeyValuePair<string, int> entry in scannedPreferences)
        {
            orderedPreferences.Add(entry.Key + " " + entry.Value);
        }
    }

    public void ScanDialogueFiles()
    {
        scannedPreferences = new Dictionary<string, int>();

        string[] strings = Directory.GetFiles(FOLDERNAME, "*", SearchOption.AllDirectories);

        for (int i = 0; i < strings.Length; i++)
        {
            //  strings[i]=strings[i].Replace("\\","/");

            if (File.Exists(strings[i]))
            {
                XmlTextReader textReader = new XmlTextReader(strings[i]);
   
                while (textReader.Read())
                {
                    if (textReader.NodeType.Equals(XmlNodeType.Element) &&
                        textReader.Name.Equals("preference"))
                    {
                        string fetish = textReader.GetAttribute("fetish");
                        AddPreference(fetish);
                    }
                }
            }
        }
        BuildOrderedPreferences();
    }

    private void AddPreference(string fetish)
    {
        if (scannedPreferences.ContainsKey(fetish))
        {
            scannedPreferences[fetish] = scannedPreferences[fetish] + 1;
            return;
        }
        scannedPreferences.Add(fetish, 1);
    }
}
