﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;

public class PreferenceLoader
{
    static string FILENAME = "preferences.txt";

    internal static List<string> Load()
    {

        List<string> preferences = new List<string>();
        string prefFile = FileTools.GetFileRaw(FILENAME);

        if (prefFile != null)
        {
            string[] prefStrings = prefFile.Split(',');

            for (int i = 0; i < prefStrings.Length; i++)
            {
                preferences.Add(prefStrings[i]);
            }
        }

        return preferences;
    }

    internal static void Save(List<string> forbiddenPrefs)
    {

        using (StreamWriter streamWriter = new StreamWriter("gameData/data/"+FILENAME, false))
        {
            
            for (int i = 0; i < forbiddenPrefs.Count; i++)
            {
                streamWriter.Write(forbiddenPrefs[i]);
                if (i < forbiddenPrefs.Count - 1)
                {
                    streamWriter.Write(",");
                }
            }

        }
    }
}
