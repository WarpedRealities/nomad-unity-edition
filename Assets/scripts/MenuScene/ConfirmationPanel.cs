﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConfirmationPanel : MonoBehaviour
{
    ConfirmState confirmState;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetState(ConfirmState confirmState)
    {
        this.confirmState = confirmState;
    }

    public void ClickYes()
    {
        switch (confirmState)
        {
            case ConfirmState.NEWGAME:
                SceneManager.LoadScene("NewGameScene");
                break;
            case ConfirmState.EXIT:
                Application.Quit();
                break;
        }
    }

    public void ClickNo()
    {
        transform.parent.Find("MenuPanel").gameObject.SetActive(true);
        // GameObject.Find("MenuPanel").SetActive(true);
        gameObject.SetActive(false);
    }
}
