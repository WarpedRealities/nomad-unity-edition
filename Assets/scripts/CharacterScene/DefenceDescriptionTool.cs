﻿using System;

internal class DefenceDescriptionTool
{

    private static string[] texts = {
        "Kinetic soak is the number of points of damage subtracted from kinetic attacks such as claws, swords and bullets",
        "Thermal soak is the number of points of damage subtracted from thermal attacks such as fire and lasers",
        "Shock soak is the number of points of damage subtracted from electrical attacks such as thunderbolts and tasers.",
        "Tease soak is the number of points of resolve damage subtracted from seduction based attacks such as flashing and posing",
        "Pheromone soak is the number of points of resolve damage subtracted from biochemistry based attacks such as alluring scents",
        "Psi soak is the number of points of resolve damage subtracted from psionic domination attacks",
        "Poise is your resistance to status effects such as staggers and other martial techniques",
        "Ego is your resistance to mental status effects such as distraction and fascination",
        "Metaboblism is your resistance to poison or chemical based status effects"
    };

    internal static string GetDescription(int index)
    {
        return texts[index];
    }
}