﻿using UnityEngine;
using System.Collections;
using System;

public class SafetyTool
{
    internal static bool PlayerSafety(Player player, Zone zone)
    {
        LineOfSightManager lineOfSight = new LineOfSightManager();

        for (int i = 0; i < zone.GetContents().GetActors().Count; i++)
        {
            if (zone.GetContents().GetActors()[i] is NPC)
            {
                NPC actor = (NPC)zone.GetContents().GetActors()[i];
                if (actor.GetAlive() &&
                    actor.GetFaction().getRelationship(player.GetFaction()) < 55 &&
                    !actor.IsPeaceful() &&
                    Vector2Int.Distance(actor.GetPosition(),player.GetPosition())<10 &&
                    lineOfSight.CanSee(player.GetPosition(), actor.GetPosition(), zone.GetContents()))
                {
         
                    return false;
                }
            }

        }

        return true;
    }
}
