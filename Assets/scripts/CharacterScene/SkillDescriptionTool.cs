﻿using UnityEngine;
using System.Collections;
using System;

public class SkillDescriptionTool
{
    private static string[] texts = {
        "Melee is the skill for attacking with hand to hand weapons, it increases your chance of hitting with swords,"+
        " knives and other weapons and your chance of inflicting critical hits",
        "Ranged is the skill of marksmanship and increases your chance of hitting with guns or other ranged weapons and your chance of"+
        " inflicting critical hits",
        "seduction is the skill of inflicting tease damage on your opponents to make them preyish and horny, this increases your chance "+
        "to hit with seduction moves",
        "Parry is the skill of protecting yourself against close combat moves. It decreases your chance of being hit by things like claws,"+
        "tentacles and blades",
        "Dodge is the skill of evading ranged attacks. It decreases your chance of being hit by projectiles, beams and rays of all kinds.",
        "Willpower is the skill that represents your resistance to teasing, pheromones, psychic influence. It'll decrease your chance to be hit"+
            "by these sorts of attacks",
        "Struggle is your skill at escaping confinement, restraint, bindings or most importantly sticky,tight and digestive situations",
        "Pleasure is the skill of sex and stimulation. It has obvious uses, but it also can be used to coax a predator into letting you go",
        "Persuade is the art of convincing someone to do what you want with words. Diplomacy has its place.",
        "Technology is your skill at crafting, repairing and hacking. Use it to fix spacecraft, make new items and break security",
        "Science is the skill of research and analysis used for operating sensors and understanding the world around you",
        "Piloting is your shiphandling skill, allowing you to more efficiently traverse space and evade attacks in space combat",
        "Gunnery is your skill at targeting with ship scale weapons in space combat"
    };

    internal static object GetDescription(int index)
    {
        return texts[index];
    }
}
