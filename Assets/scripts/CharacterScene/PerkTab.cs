﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkTab : MonoBehaviour
{
    public NuList listUI;
    private Player player;
    private PlayerRPG playerRPG;

    // Start is called before the first frame update
    void Start()
    {
        ListCallback listCallback = Callback;
        listUI.SetCallback(listCallback);
    }

    public void Callback(int index)
    {
        if (index < playerRPG.GetPerks().Count)
        {
            transform.parent.SendMessage("SetDescription", playerRPG.GetPerks()[index].GetRef().GetDescription());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
        this.playerRPG = (PlayerRPG)player.GetRPG();

        BuildUI();
    }

    private void BuildUI()
    {

        List<string> strings = new List<string>();
        for (int i = 0; i < playerRPG.GetPerks().Count; i++)
        {
            strings.Add(playerRPG.GetPerks()[i].GetName());
        }
        listUI.SetList(strings);
    }
}
