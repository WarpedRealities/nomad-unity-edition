﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterScreen : MonoBehaviour
{

    private bool doubleTapPrevention;
    private float controlClock;
    private GlobalGameState globalGameState;
    private Player player;
    public GameObject topBar, abilityTab, skillTab, defenceResistTab, perkTab, descriptionBox;
    private SkillTab skillTabScript;
    private DefenceResistTab defenceResistTabScript;
    public Text descriptionText;
    private void Awake()
    {

        if (!GlobalGameState.GetInstance().IsPlaying())
        {
            GlobalGameState.GetInstance().NewGame();
            
            GlobalGameState.GetInstance().SetPlaying(true);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        globalGameState = GlobalGameState.GetInstance();
        player = globalGameState.GetPlayer();
        gameObject.BroadcastMessage("SetPlayer", player);
        doubleTapPrevention = true;
        skillTabScript=skillTab.GetComponent<SkillTab>();
        defenceResistTabScript = defenceResistTab.GetComponent<DefenceResistTab>();
        
        //Selectable button = transform.Find("TopBar").Find("SkillsButton").GetComponent<Selectable>();
        //AttachNavigation(skillTabScript.AttachNavigation(button), button);
    }

    // Update is called once per frame
    void Update()
    {
        if (doubleTapPrevention && !Input.anyKey)
        {
            doubleTapPrevention = false;
        }
        if (Input.GetKey(KeyCode.C) && !doubleTapPrevention && ( Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SceneManager.LoadScene("ViewScene");
        }
    }

    void SetTab(CharacterTab tab)
    {
        Selectable button;
        switch (tab)
        {
            case CharacterTab.ABILITY:
                abilityTab.SetActive(true);
                abilityTab.BroadcastMessage("SetPlayer", player);
                defenceResistTab.SetActive(false);
                perkTab.SetActive(false);
                skillTab.SetActive(false);
                button = transform.Find("TopBar").Find("AbilityButton").GetComponent<Selectable>();
                //AttachNavigation(skillTabScript.AttachNavigation(button), button);
                break;
            case CharacterTab.SKILLS:
                skillTab.SetActive(true);
                skillTab.BroadcastMessage("SetPlayer", player);
                defenceResistTab.SetActive(false);
                perkTab.SetActive(false);
                abilityTab.SetActive(false);
                button = transform.Find("TopBar").Find("SkillButton").GetComponent<Selectable>();
                //AttachNavigation(skillTabScript.AttachNavigation(button),button);
                break;
            case CharacterTab.DEFENCERESIST:
                skillTab.SetActive(false);
                defenceResistTab.SetActive(true);
                defenceResistTab.BroadcastMessage("SetPlayer", player);
                perkTab.SetActive(false);
                abilityTab.SetActive(false);
                button = transform.Find("TopBar").Find("DefenceButton").GetComponent<Selectable>();
                //AttachNavigation(defenceResistTabScript.AttachNavigation(button), button);
                break;

            case CharacterTab.PERKS:
                skillTab.SetActive(false);
                defenceResistTab.SetActive(false);
                perkTab.SetActive(true);
                abilityTab.SetActive(false);
                perkTab.BroadcastMessage("SetPlayer", player);
                break;
        }
    }

    private void AttachNavigation(Selectable obj, Selectable button)
    {
        Navigation navigation = button.navigation;

        navigation.selectOnDown = obj;

        navigation.mode = Navigation.Mode.Explicit;
        button.navigation = navigation;
    }

    public void SetDescription(string description)
    {
        descriptionText.text = description;
    }
}
