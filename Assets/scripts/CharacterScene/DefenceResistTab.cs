﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DefenceResistTab : MonoBehaviour
{

    public TextMeshProUGUI[] texts;
    public Player player;
    public PlayerRPG playerRPG;
    // Start is called before the first frame update
    void Start()
    {
        ListCallback listCallback = Callback;
        //BroadcastMessage("SetCallback", listCallback);
    }
    public void Callback(int index)
    {

        transform.parent.SendMessage("SetDescription", DefenceDescriptionTool.GetDescription(index));
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
        this.playerRPG = (PlayerRPG)player.GetRPG();
        UpdateText();
    }


    public void UpdateText()
    {
        for (int i = 0; i < texts.Length; i++)
        {
            string str = GetDefenceResistString(i) + " " + this.playerRPG.GetRawDefRes(i);
            int bonus = this.playerRPG.GetBonusResist(i);
            if (bonus > 0)
            {
                str += (" (+" + bonus + ")");
            }
            texts[i].text = str;

        }
    }

    private string GetDefenceResistString(int index)
    {
        switch (index)
        {
            case 0:
                return "kinetic";
            case 1:
                return "thermal";
            case 2:
                return "shock";
            case 3:
                return "tease";
            case 4:
                return "pheromone";
            case 5:
                return "psi";
            case 6:
                return "poise";
            case 7:
                return "ego";
            case 8:
                return "metabolism";
        }
        return "null";
    }

    internal Selectable AttachNavigation(Selectable button)
    {
        Selectable[] selectable = new Selectable[texts.Length];
        for (int i = 0; i < texts.Length; i++)
        {
            selectable[i] = texts[i].GetComponent<Selectable>();
        }
        for (int i = 0; i < texts.Length; i++)
        {
            Navigation navigation = new Navigation();
            navigation.mode = Navigation.Mode.Explicit;
            if (i == 0)
            {
                navigation.selectOnUp = button;
            }
            else
            {
                navigation.selectOnUp = selectable[i - 1];
            }

            if (i == texts.Length - 1)
            {
                navigation.selectOnDown = null;
            }
            else
            {
                navigation.selectOnDown = selectable[i + 1];
            }
            selectable[i].navigation = navigation;
        }
        return selectable[0];
    }
}
