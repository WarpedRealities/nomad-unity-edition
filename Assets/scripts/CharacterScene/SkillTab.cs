﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillTab : MonoBehaviour
{

    public TextMeshProUGUI[] texts;
    public Player player;
    public PlayerRPG playerRPG;
    // Start is called before the first frame update
    void Start()
    {
        ListCallback listCallback = Callback;
        //BroadcastMessage("SetCallback", listCallback);
    }
    public void Callback(int index)
    {

        transform.parent.SendMessage("SetDescription", SkillDescriptionTool.GetDescription(index));
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SetPlayer(Player player)
    {
        this.player = player;
        this.playerRPG = (PlayerRPG)player.GetRPG();
        UpdateText();
    }

    public void UpdateText()
    {
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].text = GetSkillString(i) + " " + this.playerRPG.GetSkill(i);
        }
    }

    private string GetSkillString(int i)
    {
        switch (i)
        {
            case 0:
                return "melee";
            case 1:
                return "ranged";
            case 2:
                return "seduction";
            case 3:
                return "parry";
            case 4:
                return "dodge";
            case 5:
                return "willpower";
            case 6:
                return "struggle";
            case 7:
                return "pleasure";
            case 8:
                return "persuade";
            case 9:
                return "tech";
            case 10:
                return "science";
            case 11:
                return "piloting";
            case 12:
                return "gunnery";
            case 13:
                return "";
        }

        return "null";
    }

    internal Selectable AttachNavigation(Selectable obj)
    {
        Selectable[] selectable = new Selectable[texts.Length];
        for (int i = 0; i < texts.Length; i++)
        {
            selectable[i] = texts[i].GetComponent<Selectable>();
        }
        for (int i = 0; i < texts.Length; i++)
        {
            Navigation navigation = new Navigation();
            navigation.mode = Navigation.Mode.Explicit;
           if (i == 0)
            {
                navigation.selectOnUp=obj;
            }
           else
            {
                navigation.selectOnUp = selectable[i - 1];
            }

           if (i == texts.Length - 1)
            {
                navigation.selectOnDown = null;
            }
           else
            {
                navigation.selectOnDown = selectable[i + 1];
            }
            selectable[i].navigation = navigation;
        }
        return selectable[0];
    }
}
