﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillText : Selectable, IPointerClickHandler, ISubmitHandler
{

    public int callbackInt;
    public ListCallback callback;

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        callback(callbackInt);
    }
    public void OnSubmit(BaseEventData eventData)
    {
        callback(callbackInt);
    }

    public void SetCallback(ListCallback callback)
    {
        
        this.callback = callback;
    }


}
