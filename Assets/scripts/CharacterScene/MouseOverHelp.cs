using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseOverHelp : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    HelpText helpText;
    public int index;
    public int subIndex;
    void Awake()
    {
        helpText = GameObject.Find("HelpText").GetComponent<HelpText>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        this.helpText.SetHelp(eventData.position, index, subIndex);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        this.helpText.ExitHelp(index, subIndex);
    }
}
