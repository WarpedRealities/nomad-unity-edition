using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class HelpWrapper
{
    [SerializeField]
    public List<string> strings;
}

public class HelpText : MonoBehaviour
{
    public TextMeshProUGUI text;
    public Image image;
    public HelpWrapper[] helpStrings;
    int index=-1, subIndex=-1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetHelp(Vector2 position, int index, int subIndex)
    {
        this.index = index;
        this.subIndex = subIndex;
        this.transform.position = position + new Vector2(264,-32);
        this.text.enabled = true;
        this.image.enabled = true;
        this.text.text = helpStrings[index].strings[subIndex];
    }

    public void ExitHelp(int index, int subIndex)
    {
        if (this.index == index && this.subIndex == subIndex)
        {
            text.enabled = false;
            image.enabled = false;
        }
    }
}
