﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class TopBarScript : MonoBehaviour
{

    public Player player;
    public PlayerRPG playerRPG;
    public TextMeshProUGUI nameText, levelText, expText;
    public MonoButton[] monoButtons; 
    public Button levelButton;
    int index = 0;
    public bool safety;
    // Start is called before the first frame update
    void Start()
    {
        UpdateButtons();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
        this.playerRPG = (PlayerRPG)player.GetRPG();
        safety = SafetyTool.PlayerSafety(player, GlobalGameState.GetInstance().getCurrentZone());
        if (!this.playerRPG.CanLevel() || !safety)
        {
            levelButton.interactable = false;
            //levelButton.enabled = false;
        }
        UpdateText();
    }

    public void UpdateText()
    {
        nameText.text = player.GetName();

        levelText.text = "level " + playerRPG.GetLevel();
        expText.text = "exp " + playerRPG.GetExperience() + "/" + playerRPG.GetNextLevel();
    }

    public void UpdateButtons()
    {
        for (int i = 0; i < monoButtons.Length; i++)
        {
            monoButtons[i].SetHighlight(i == index ? true : false);
        }
    }

    public void ButtonCallback(int index)
    {
        this.index = index - 1;
        UpdateButtons();
        switch (index)
        {
            case 0:
                SceneManager.LoadScene("ViewScene");
                break;
            case 1:
                gameObject.transform.parent.SendMessage("SetTab", CharacterTab.ABILITY);
                break;
            case 2:
                gameObject.transform.parent.SendMessage("SetTab", CharacterTab.SKILLS);
                break;

            case 3:
                gameObject.transform.parent.SendMessage("SetTab", CharacterTab.DEFENCERESIST);
                break;

            case 4:
                gameObject.transform.parent.SendMessage("SetTab", CharacterTab.PERKS);
                break;

            case 5:
                SceneManager.LoadScene("LevelUpScene");
                break;
        }
    }
}
