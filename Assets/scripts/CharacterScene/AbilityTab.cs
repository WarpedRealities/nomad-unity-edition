﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class AbilityTab : MonoBehaviour
{
    public TextMeshProUGUI[] texts;
    public Player player;
    public PlayerRPG playerRPG;
    // Start is called before the first frame update
    void Start()
    {
        ListCallback listCallback = Callback;
        //BroadcastMessage("SetCallback", listCallback);
    }
    public void Callback(int index)
    {

       // transform.parent.SendMessage("SetDescription", SkillDescriptionTool.GetDescription(index));
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void SetPlayer(Player player)
    {
        this.player = player;
        this.playerRPG = (PlayerRPG)player.GetRPG();
        UpdateText();
    }

    public void UpdateText()
    {
        for (int i = 0; i < texts.Length-1; i++)
        {
            texts[i].text = GetAbilityName(i) + " " + this.playerRPG.getAbility(i);
        }
        texts[texts.Length - 1].text = GetAbilityName(6) + " " + this.playerRPG.GetKarma();
    }

    private string GetAbilityName(int i)
    {
        switch (i)
        {
            case 0:
                return "strength";
            case 1:
                return "dexterity";
            case 2:
                return "agility";
            case 3:
                return "endurance";
            case 4:
                return "intelligence";
            case 5:
                return "charisma";
            case 6:
                return "karma";
        }

        return "null";
    }

    internal Selectable AttachNavigation(Selectable obj)
    {
        Selectable[] selectable = new Selectable[texts.Length];
        for (int i = 0; i < texts.Length; i++)
        {
            selectable[i] = texts[i].GetComponent<Selectable>();
        }
        for (int i = 0; i < texts.Length; i++)
        {
            Navigation navigation = new Navigation();
            navigation.mode = Navigation.Mode.Explicit;
            if (i == 0)
            {
                navigation.selectOnUp = obj;
            }
            else
            {
                navigation.selectOnUp = selectable[i - 1];
            }

            if (i == texts.Length - 1)
            {
                navigation.selectOnDown = null;
            }
            else
            {
                navigation.selectOnDown = selectable[i + 1];
            }
            selectable[i].navigation = navigation;
        }
        return selectable[0];
    }
}
