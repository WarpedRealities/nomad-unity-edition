﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    string reformZone;
    ReformationTool reformationTool;
    public Button reformButton;

    // Start is called before the first frame update
    void Start()
    {
        GlobalGameState.GetInstance().SetPlaying(false);
        reformationTool = GlobalGameState.GetInstance().GetPlayer()!= null ?
            new ReformationTool(GlobalGameState.GetInstance().GetPlayer().GetReformation()): null;
        if (reformationTool!=null && reformationTool.CanReform())
        {
            reformButton.enabled = true;
        } else
        {
            reformButton.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadClick()
    {
        GameObject obj = transform.parent.Find("LoadPanel").gameObject;
        obj.SetActive(true);
        obj.SendMessage("Initialize");
        gameObject.SetActive(false);
    }

    public void ReformClick()
    {
        GlobalGameState.GetInstance().SetPlaying(true);
        reformationTool.TriggerReformation();
    }

    public void MenuClick()
    {

        SceneManager.LoadScene("MenuScene");
    }

    public void ExitClick()
    {
        Application.Quit();
    }
}
