using Radishmouse;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate float PullOffset();
public class SineWave
{
    public float amplitude;
    public float frequency;
    public float offset;
    public float velocity;
    float mult = 1;
    public PullOffset offsetDelegate;
    public SineWave(float amplitude, float frequency, float offset)
    {
        this.amplitude = amplitude;
        this.frequency = frequency;
        this.velocity = DiceRoller.GetInstance().RollOdds();
        this.offset = offset;
    }

    public float GetValueSin(float p)
    {
        float inFreq = ((p + offset) * frequency);
        return amplitude * Mathf.Sin(inFreq);
    }

    public void Update(float charge)
    {
        offset += velocity * Time.deltaTime;
        if (offset > MathF.PI*2)
        {
            offset = offset - (MathF.PI*2);
            if (charge > 0.5F)
            {
                float otherOffset = this.offsetDelegate();
                if (otherOffset > Mathf.PI)
                {
                    offset -= 0.01F;
                }
                else
                {
                    offset += 0.01F;
                }
            }
        }
    }

    internal void CorrectAmplitude(float charge)
    {
        amplitude = charge;
    }

    internal void CorrectFrequency(float charge)
    {
        frequency = 3 +(7- (charge*7));
    }

    internal void CorrectOffset(float other)
    {
        if (other < Mathf.PI)
        {
            offset = offset < other ? offset + Time.deltaTime : offset;
        }
    }

    internal bool VelocityNormalized()
    {
        return velocity == 0.1F;
    }

    internal void CorrectVelocity()
    {
        velocity = velocity > 0.1F ? velocity - Time.deltaTime : velocity;
        velocity = velocity < 0.1F ? velocity + Time.deltaTime : velocity;
        if (MathF.Abs(velocity - 0.1F) < 0.05F)
        {
            velocity = 0.1F;
        }
    }

    internal void Accelerate(float charge)
    {
        if (velocity < charge)
        {
            velocity += Time.deltaTime;
        }
        else
        {
            velocity = charge;
        }
    }

    internal void MatchOffset(float other)
    {
        if (offset < other)
        {
            offset += Time.deltaTime/10;
        }
        if (offset > other+0.01F)
        {
            offset -= Time.deltaTime / 8;
        }
    }

    public float GetOffset()
    {
        return offset;
    }
} 

public class SineWaveMachine : MonoBehaviour
{
    public SineWave[] waves;
    public LineRenderer[] lines;
    public Vector3[][] points;
    public float charge;
    // Start is called before the first frame update
    void Start()
    {
        waves = new SineWave[2];
        waves[0] = new SineWave(0.1F, 3, 0);
        waves[1] = new SineWave(-1, 10, 0);
        PullOffset offsetDelegate = waves[0].GetOffset;
        waves[1].offsetDelegate = offsetDelegate;
        PullOffset offsetDelegate1 = waves[1].GetOffset;
        waves[0].offsetDelegate = offsetDelegate1;
        BuildPoints();
    }

    public void BuildPoints()
    {
        points = new Vector3[2][];
        for (int i = 0; i < 2; i++)
        {
            points[i] = new Vector3[64];
            for (int j = 0; j < 64; j++)
            {
                points[i][j] = new Vector3(((float)j)/16, 0, 1);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {


        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 64; j++)
            {
                float v = ((waves[i].GetValueSin(((float)j) / 32)-1));
                points[i][j].y = v;
            }
            lines[i].SetPositions(points[i]);
            waves[i].Update(charge);
        }
        waves[0].CorrectAmplitude(charge);
        waves[1].CorrectFrequency(charge);
        if (charge> 0.5F)
        {
            waves[0].Accelerate(charge);
            waves[1].Accelerate(charge);
        }
    }
}
