using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    public SpriteRenderer Renderer;
    Vector2 velocity;
    Color colour;
    float lifespan;
    float life;
    bool active;
    internal bool IsActive()
    {
        return active;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            float proportion = 1-(life / lifespan);
            float a = life > 0.1F ? 1 : life * 10;
            Renderer.color = new Color(colour.r * proportion, colour.g * proportion, colour.b * proportion, 1);
            life -= Time.deltaTime;
            this.transform.Translate(velocity * Time.deltaTime);
            this.transform.localScale = new Vector3((1 + (proportion*1))*a, (1 + (proportion*1))*a, 1);
            if (life <= 0)
            {
                active = false;
                Renderer.enabled = false;
            }
        }
    }

    public void Setup(Vector2 velocity, float lifespan, Color colour)
    {
        active = true;
        this.transform.Translate(velocity * 0.1F);
        this.lifespan = lifespan;
        this.life = lifespan;
        this.colour = colour;
        this.velocity = velocity;
        Renderer.enabled = true;
    }
}
