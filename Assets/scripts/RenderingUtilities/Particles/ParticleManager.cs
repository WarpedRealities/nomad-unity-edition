using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public GameObject prefabParticle;
    public int maximumParticles;
    private Particle[] particles;
    public int spawnRate;
    private float clock = 0;
    private bool active = false;
    private readonly float INTERVAL = 0.1F;
    public Color particleColour;
    // Start is called before the first frame update
    void Start()
    {
        particles=new Particle[maximumParticles];
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            clock += Time.deltaTime;
            if (clock > INTERVAL)
            {
                clock = 0;
                RunParticles();
            }
        }
    }

    private void RunParticles()
    {
        int count = this.spawnRate;
        for (int i = 0; i < particles.Length; i++)
        {
            if (particles[i] == null)
            {
                GameObject obj = Instantiate(prefabParticle, null);
                obj.transform.position = new Vector3(transform.position.x, transform.position.y);
                particles[i] = obj.GetComponent<Particle>();
                particles[i].Setup(RandomVector(), 1, particleColour);
                count--;
            }
            else if (!particles[i].IsActive())
            {
                particles[i].transform.position = new Vector3(transform.position.x, transform.position.y);
                particles[i].Setup(RandomVector(), 1, particleColour);
                count--;
            }
            if (count <= 0)
            {
                return;
            }
        }

    }

    private Vector2 RandomVector()
    {
        return new Vector2(-1 + (DiceRoller.GetInstance().RollOdds() * 2), -1 + (DiceRoller.GetInstance().RollOdds() * 2));
    }

    public void SetActive(bool active)
    {
        this.active = active;
    }

    public bool IsActive()
    {
        return this.active;
    }
}
