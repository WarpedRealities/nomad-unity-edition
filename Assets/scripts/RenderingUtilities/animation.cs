﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class animation : MonoBehaviour
{
    private Image image;
    public Sprite[] sprites;
    public float duration;
    private float clock;
    private int index;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        clock += Time.deltaTime;
        if (clock > duration)
        {
            index++;
            if (index >= sprites.Length)
            {
                index = 0;
            }
            clock = 0;
            image.sprite = sprites[index];
        }
    }
}
