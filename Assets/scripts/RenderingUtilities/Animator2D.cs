﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AnimatorInstructionSet
{
    public int index=0, length=1;
    public float speed = 0.1F;
    public bool repeat = true;

    public AnimatorInstructionSet(int index, int length, float speed, bool repeat)
    {
        this.index = index;
        this.length = length;
        this.speed = speed;
        this.repeat = repeat;
    }
};

public class Animator2D : MonoBehaviour
{
    public Sprite[] sprites;
    public int[] startIndices;
    public float speed = 0.1F;
    private float clock = 0;
    public int length = 2;
    private int currentAnim=0, index=0;
    public bool repeating=true;
    public bool stop = false;
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!stop && length>1)
        {
            clock += Time.deltaTime;
            if (clock > speed)
            {
                clock = 0;
                index++;
                if (index >= length)
                {
                    if (repeating)
                    {
                        index = 0;
                    }
                    else
                    {
                        stop = true;
                        index -= 1;
                    }
                }
                spriteRenderer.sprite = sprites[index + startIndices[currentAnim]];
            }
        }     
    }

    public void setAnimation(int index, int length, float speed, bool repeat)
    {
        this.stop = false;
        this.speed = speed;
        this.length = length;
        this.currentAnim = index;
        this.repeating = repeat;
        this.index = 0;
        if (spriteRenderer==null)
        {
            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        }
    
        if (length < 2)
        {
            spriteRenderer.sprite = sprites[startIndices[currentAnim]];
        }
 
    }
    public void setAnimationI(AnimatorInstructionSet animatorInstructionSet)
    {
        setAnimation(animatorInstructionSet.index, animatorInstructionSet.length, animatorInstructionSet.speed, animatorInstructionSet.repeat);
    }
    public void setStop(bool stop)
    {
        this.stop = stop;
    }

    public void setFlip(bool flip)
    {
        spriteRenderer.flipX = flip;
    }

}
