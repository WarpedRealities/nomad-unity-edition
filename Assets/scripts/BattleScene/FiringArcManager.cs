using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiringArcManager : MonoBehaviour
{
    public GameObject arcPrefab;
    WeaponData weaponData;
    CombatStats combatStats;
    public Arc[] arcs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BuildArcs(GameObject playerShip, WeaponData weapons, CombatStats combatStats)
    {
        weaponData = weapons;
        this.combatStats = combatStats;
        arcs = new Arc[combatStats.GetWeaponSystems().Count];
        for (int i = 0; i < arcs.Length; i++)
        {
            GameObject gameObject = Instantiate(arcPrefab, playerShip.transform);
            gameObject.transform.localPosition = new Vector3(0, 0, 0);
            arcs[i] = gameObject.GetComponent<Arc>();
            arcs[i].BuildArc(GetArc(combatStats.GetWeaponSystems()[i].GetArc()),
                1);

        }
    }

    public static float GetArc(FIRINGARC fIRINGARC)
    {
        switch (fIRINGARC)
        {
            case FIRINGARC.FIXED:
                return 11;
            case FIRINGARC.NARROW:
                return 22;
            case FIRINGARC.NORMAL:
                return 45;
            case FIRINGARC.WIDE:
                return 90;
            case FIRINGARC.CIRCLE:
                return 180;
        }
        return 90;
    }

    internal void UpdateArcs(bool show)
    {
        for (int i = 0; i < arcs.Length; i++)
        {
            arcs[i].gameObject.SetActive(show && weaponData.GetShowArc(i));
        }
    }
}
