﻿using UnityEngine;
using System.Collections;

public interface BattleSim 
{
    void AddSpawnable(Spawnable spawn);

    void RemoveSpawnable(Spawnable spawn);
}
