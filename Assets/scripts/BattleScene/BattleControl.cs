using System;
using System.Collections.Generic;
using UnityEngine;


public class WeaponData {
    public WeaponState [] weaponStates;
    public List<BattleAttack> attacks = new List<BattleAttack>();
    public WeaponData(WeaponState [] states)
    {
        this.weaponStates = states;
        for (int i = 0; i < weaponStates.Length; i++)
        {
            weaponStates[i] = WeaponState.ENABLED;
        }

    }

    internal bool GetShowArc(int index)
    {
        switch (weaponStates[index])
        {
            case WeaponState.DISABLED:
                return false;
            case WeaponState.INCAPABLE:
                return false;
            case WeaponState.UNSELECTED:
                return false;
            case WeaponState.ENABLED:
                return true;
            case WeaponState.CONFIRMED:
                return true;
        }
        return false;
    }

}

public class BattleControl : MonoBehaviour
{
    BattleCamera battleCamera;
    public BattlerExpectation expectation;
    public BattleSimulation battleSimulation;
    public BattleHud battleHud;
    public BattleTargeting targeting;
    public FiringArcManager arcManager;
    private Battler playerBattler;
    private Battler[] enemyBattlers;
    private List <Targetable> targetablesList = new List<Targetable>();
    private NpcBattleController[] enemyControllers;
    private LuaBattleInfo battleInfo;
    private WeaponData weaponData;
    private bool buttonSafety = false;
    private bool showArcs = true;
    private bool active = true;
    // Start is called before the first frame update
    void Start()
    {
        battleSimulation = GetComponent<BattleSimulation>();
        battleCamera = GameObject.Find("Main Camera").GetComponent<BattleCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            CameraControl();
            MovementControls();
            WeaponControls();
        }
        if (buttonSafety && 
            !Input.anyKey)
        {
            buttonSafety = false;
        }
    }

    internal void UpdateWeaponStates()
    {
        weaponData.attacks.Clear();

        for (int i = 0; i < this.weaponData.weaponStates.Length; i++)
        {
            if (weaponData.weaponStates[i]==WeaponState.CONFIRMED|| 
                weaponData.weaponStates[i] == WeaponState.UNSELECTED)
            {
                weaponData.weaponStates[i] = WeaponState.ENABLED;
            }
            bool canFire = this.playerBattler.GetData().GetCombatStats().CheckWeaponUsable(i,
                ((Spaceship)this.playerBattler.GetData().GetActive_Entity()).GetResources());
            if (canFire && weaponData.weaponStates[i].Equals(WeaponState.INCAPABLE))
            {
                weaponData.weaponStates[i] = WeaponState.DISABLED;
            }
            if (!canFire && !weaponData.weaponStates[i].Equals(WeaponState.INCAPABLE))
            {
                weaponData.weaponStates[i] = WeaponState.INCAPABLE;
            }
        }
        battleHud.UpdateUI();
    }

    private void WeaponControls()
    {
        if (!buttonSafety)
        {
            if (Input.GetKey(KeyCode.Alpha1))
            {
                buttonSafety = true;
                ToggleWeapon(0);
            }
            if (Input.GetKey(KeyCode.Alpha2))
            {
                buttonSafety = true;
                ToggleWeapon(1);
            }
            if (Input.GetKey(KeyCode.Alpha3))
            {
                buttonSafety = true;
                ToggleWeapon(2);
            }
            if (Input.GetKey(KeyCode.Alpha4))
            {
                buttonSafety = true;
                ToggleWeapon(3);
            }
            if (Input.GetKey(KeyCode.Alpha5))
            {
                buttonSafety = true;
                ToggleWeapon(4);
            }
            if (Input.GetKey(KeyCode.Alpha6))
            {
                buttonSafety = true;
                ToggleWeapon(5);
            }
            if (Input.GetKey(KeyCode.Alpha7))
            {
                buttonSafety = true;
                ToggleWeapon(6);
            }
            if (Input.GetKey(KeyCode.Alpha8))
            {
                buttonSafety = true;
                ToggleWeapon(7);
            }
            if (Input.GetKey(KeyCode.Alpha9))
            {
                buttonSafety = true;
                ToggleWeapon(8);
            }
            if (Input.GetKey(KeyCode.Alpha0))
            {
                buttonSafety = true;
                ToggleWeapon(9);
            }
            if (Input.GetKey(KeyCode.H))
            {
                buttonSafety = true;
                showArcs = !showArcs;
                arcManager.UpdateArcs(showArcs);
            }
            if (Input.GetKey(KeyCode.F))
            {
                buttonSafety = true;
                targeting.Increment();
            }
            if (Input.GetKey(KeyCode.C))
            {
                buttonSafety = true;
                targeting.Decrement();
            }
            if (Input.GetKey(KeyCode.R))
            {
                buttonSafety = true;
                HandleMagazineChange();
            }
            if (Input.GetKey(KeyCode.Space))
            {
                buttonSafety = true;
                ScheduleAttacks();
                battleHud.UpdateUI();
            }
        }
    }

    private void HandleMagazineChange()
    {

        for (int i = 0; i < weaponData.weaponStates.Length; i++)
        {
            if (weaponData.weaponStates[i] == WeaponState.ENABLED &&
                this.playerBattler.GetData().GetCombatStats().GetWeaponSystems()[i].GetWeapon().HasMagazineCosts())
            {
                MagazineCost magazineCost = this.playerBattler.GetData().
                    GetCombatStats().
                    GetWeaponSystems()[i].
                    GetWeapon().
                    GetMagazineCost();
                int index = 0;
                for (int j = 0; j < magazineCost.GetAccept().Length; j++)
                {

                    if (this.playerBattler.GetData().GetCombatStats().GetWeaponSystems()[i].GetAmmoType() == 
                        magazineCost.GetAccept()[j])
                    {

                        index = j;
                    }
                }
                while (true)
                {
                    index++;
                    if (index >= magazineCost.GetAccept().Length)
                    {
                        index = 0;
                    }

                    ShipMagazine shipMagazine = ((Spaceship)playerBattler.GetData().GetActive_Entity()).GetResources()
                        .GetMagazine(magazineCost.GetAccept()[index].GetCode());
 
                    if (shipMagazine != null)
                    {
                        this.playerBattler.GetData().GetCombatStats().GetWeaponSystems()[i].SetammoType(
                            magazineCost.GetAccept()[index]);
                        battleHud.UpdateUI();
                        break;
                    }
                }
            }
        }
    }

    private void ScheduleAttacks()
    {
        for (int i = 0; i < weaponData.weaponStates.Length; i++)
        {
            switch (weaponData.weaponStates[i])
            {
                case WeaponState.ENABLED:
                    ScheduleAttack(i);
                    break;
                case WeaponState.CONFIRMED:
                    UnScheduleAttack(i);
                    break;
            }
        }
    }

    private void UnScheduleAttack(int index)
    {
        weaponData.weaponStates[index] = WeaponState.ENABLED;
        //remove scheduled attack

        for (int i = 0; i < weaponData.attacks.Count; i++)
        {
            if (playerBattler.GetData().GetCombatStats().GetWeaponSystems()[index] ==
                weaponData.attacks[i].chosenWeapon)
            {
                weaponData.attacks.Remove(weaponData.attacks[i]);
                break;
            }
        }

    }

    private void ScheduleAttack(int index)
    {
        weaponData.weaponStates[index] = WeaponState.CONFIRMED;
        weaponData.attacks.Add(new BattleAttack(targeting.GetTarget(), playerBattler.GetData().GetCombatStats().GetWeaponSystems()[index]));
    }

    private void ToggleWeapon(int index)
    {
        if (index< GetWeaponStates().Length)
        {
            WeaponState state = GetWeaponStates()[index];
            switch (GetWeaponStates()[index])
            {
                case WeaponState.CONFIRMED:
                    state = WeaponState.UNSELECTED;
                    break;

                case WeaponState.UNSELECTED:
                    state = WeaponState.CONFIRMED;
                    break;
                case WeaponState.ENABLED:
                    state = WeaponState.DISABLED;
                    break;

                case WeaponState.DISABLED:
                    state = WeaponState.ENABLED;
                    break;
            }
            GetWeaponStates()[index] = state;
            battleHud.UpdateUI();
            arcManager.UpdateArcs(showArcs);
        }
    }

    private void MovementControls()
    {
        bool expectationUpdate = false;
        if (Input.GetKey(KeyCode.D))
        {

            if (playerBattler.GetData().GetMovePosition().turn > -1)
            {
                playerBattler.GetData().GetMovePosition().turn -= Time.deltaTime;
            }
            expectationUpdate = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            if (playerBattler.GetData().GetMovePosition().turn < 1)
            {
                playerBattler.GetData().GetMovePosition().turn += Time.deltaTime;
            }
            expectationUpdate = true;
        }
        if (Input.GetKey(KeyCode.W))
        {
            if (playerBattler.GetData().GetMovePosition().thrust < 1)
            {
                playerBattler.GetData().GetMovePosition().thrust += Time.deltaTime;
            }
            expectationUpdate = true;
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (playerBattler.GetData().GetMovePosition().thrust >0)
            {
                playerBattler.GetData().GetMovePosition().thrust -= Time.deltaTime;
            }
            expectationUpdate = true;
        }


        if (expectationUpdate)
        {
            expectation.UpdateExpectation();
        }
    }

    internal WeaponState [] GetWeaponStates()
    {
        return weaponData.weaponStates;
    }

    public WeaponData GetWeaponData()
    {
        return weaponData;
    }

    public void Setup(Battler player, Battler[] enemies)
    {
        this.battleInfo = new LuaBattleInfo(this.battleSimulation);
        this.playerBattler = player;
        this.enemyBattlers = enemies;
        enemyControllers = new NpcBattleController[enemies.Length];
        for (int i = 0; i < enemyBattlers.Length; i++)
        {
            enemyControllers[i] = new NpcBattleController(battleInfo, enemyBattlers[i]);
        }
        WeaponState[] weaponStates = new WeaponState[player.GetData().GetCombatStats().GetWeaponSystems().Count];
        weaponData = new WeaponData(weaponStates);
        arcManager.BuildArcs(player.gameObject, weaponData, player.GetData().GetCombatStats());
    }

    private void CameraControl()
    {
        int mouseResult = MouseEdgeCheck();
        if (Input.GetKey(KeyCode.UpArrow) || mouseResult==1)
        {
            battleCamera.AdjustPosition(0, Time.deltaTime * NumericConstants.CAMERA_SPEED);
        }
        if (Input.GetKey(KeyCode.RightArrow) || mouseResult == 2)
        {
            battleCamera.AdjustPosition(Time.deltaTime * NumericConstants.CAMERA_SPEED, 0);
        }
        if (Input.GetKey(KeyCode.DownArrow) || mouseResult == 3)
        {
            battleCamera.AdjustPosition(0, -Time.deltaTime * NumericConstants.CAMERA_SPEED);
        }
        if (Input.GetKey(KeyCode.LeftArrow) || mouseResult == 4)
        {
            battleCamera.AdjustPosition(-Time.deltaTime * NumericConstants.CAMERA_SPEED, 0);
        }
    }

    private int MouseEdgeCheck()
    {
        Vector3 mouse = Input.mousePosition;
        float width = Screen.width;
        float height = Screen.height;
        if (mouse.y > height - 16)
        {
            return 1;
        }
        if (mouse.x > width - 16)
        {
            return 2;
        }
        if (mouse.y < 16)
        {
            return 3;
        }
        if (mouse.x < 16)
        {
            return 4;
        }
        return -1;
    }

    internal void RunAI()
    {
        for (int i = 0; i < enemyBattlers.Length; i++)
        {
            if (enemyBattlers[i].GetAlive())
            {
                enemyControllers[i].Run();
            }
        }
    }

    internal void SetActive(bool active)
    {
        this.active = active;
    }

    internal bool GetActive()
    {
        return active;
    }
}
