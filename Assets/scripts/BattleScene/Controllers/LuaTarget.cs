﻿using MoonSharp.Interpreter;
using System;
using System.Collections;
using UnityEngine;

[MoonSharpUserData]
public class LuaTarget {
    private static bool isRegistered = false;
    Targetable targetable;
    public LuaTarget(Targetable targetable)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<LuaTarget>();
            isRegistered = true;
        }
        this.targetable = targetable;
    }

    public Vector2 GetPosition()
    {
        return targetable.GetPosition();
    }

    internal Targetable GetTarget()
    {
        return targetable;
    }

    public float GetDistance(Vector2 position)
    {
        return Vector2.Distance(position, targetable.GetPosition());
    }
}
