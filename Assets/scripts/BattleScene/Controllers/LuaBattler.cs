using MoonSharp.Interpreter;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
[MoonSharpUserData]
public class LuaBattler
{
    private static bool isRegistered = false;
    private Battler battler;
    private List<BattleAttack> attacks;
    public LuaBattler(Battler battler)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<LuaBattler>();
            isRegistered = true;
        }
        attacks = new List<BattleAttack>();
        this.battler = battler;
    }

    public Vector2 GetPosition()
    {
        return battler.transform.position;
    }

    public int GetTeam()
    {
        return battler.GetTeam();
    }

    public float GetFacing()
    {
        return battler.GetData().GetMovePosition().facing;
    }

    public float GetRelativeFacing(Vector2 target)
    {
        Vector2 offset = target - battler.GetPosition();
        Vector2 offsetN = offset.normalized;
        Vector2 facing = new Vector2(1 * -Mathf.Sin(battler.GetData().GetMovePosition().facing), 1 * Mathf.Cos(battler.GetData().GetMovePosition().facing)).normalized;

        float angle= Vector2.SignedAngle(facing, offsetN);

        return angle;
    }

    public void Attack(int index, LuaTarget target)
    {
        if (battler.GetData().GetCombatStats().GetWeaponSystems()[index].GetMagazine() != 0)
        {

            attacks.Add(new BattleAttack(target.GetTarget(), battler.GetData().GetCombatStats().GetWeaponSystems()[index]));
        }
    }
    public void SetTurn(float turn)
    {
        this.battler.GetData().GetMovePosition().SetTurn(turn);
    }

    public void ModTurn(float turn)
    {
        this.battler.GetData().GetMovePosition().SetTurn(turn +
           this.battler.GetData().GetMovePosition().turn);
    }

    public void SetThrust(float thrust)
    {
        this.battler.GetData().GetMovePosition().thrust = thrust;
    }

    public float GetDistance(Vector2 position)
    {
        return Vector2.Distance(position, this.battler.GetPosition());
    }
    public void Reset()
    {
        this.attacks.Clear();
    }

    internal List<BattleAttack> GetAttacks()
    {
        return this.attacks;
    }
}
