using MoonSharp.Interpreter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[MoonSharpUserData]
public class LuaBattleInfo
{
    private static bool isRegistered = false;
    private BattleSimulation battleSimulation;
    public LuaBattleInfo(BattleSimulation battleSimulation)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<Vector2>();
            UserData.RegisterType<LuaBattleInfo>();
            isRegistered = true;
        }
        this.battleSimulation = battleSimulation;
    }

    public LuaTarget GetNearestHostile(LuaBattler battler)
    {
        float comparison = 999;
        LuaTarget target = null;
        for (int i = 0; i < battleSimulation.GetBattlers().Length; i++)
        {
            if (battleSimulation.GetBattlers()[i].GetAlive() && 
                battler.GetTeam() != battleSimulation.GetBattlers()[i].GetTeam())
            {
                float distance = Vector2.Distance(battleSimulation.GetBattlers()[i].GetPosition(), battler.GetPosition());
                if (distance < comparison)
                {
                    comparison = distance;
                    target = new LuaTarget(battleSimulation.GetBattlers()[i]);
                }
            }
        }
        for (int i = 0; i < battleSimulation.GetSpawnables().Count; i++)
        {
            if (battleSimulation.GetSpawnables()[i].GetAlive() && 
                battler.GetTeam() != battleSimulation.GetSpawnables()[i].GetTeam())
            {
                float distance = Vector2.Distance(battleSimulation.GetSpawnables()[i].GetPosition(), battler.GetPosition());
                if (distance < comparison)
                {
                    comparison = distance;
                    target = new LuaTarget(battleSimulation.GetSpawnables()[i]);
                }
            }
        }
        return target;
    }
    public LuaTarget GetNearestSpawn(LuaBattler battler)
    {
        float comparison = 999;
        LuaTarget target = null;

        for (int i = 0; i < battleSimulation.GetSpawnables().Count; i++)
        {
            if (battleSimulation.GetSpawnables()[i].GetAlive() && 
                battler.GetTeam() != battleSimulation.GetSpawnables()[i].GetTeam())
            {
                float distance = Vector2.Distance(battleSimulation.GetSpawnables()[i].GetPosition(), battler.GetPosition());
                if (distance < comparison)
                {
                    comparison = distance;
                    target = new LuaTarget(battleSimulation.GetSpawnables()[i]);
                }
            }
        }
        return target;
    }
    public LuaTarget GetNearestShip(LuaBattler battler)
    {
        float comparison = 999;
        LuaTarget target = null;
        for (int i = 0; i < battleSimulation.GetBattlers().Length; i++)
        {
            if (battleSimulation.GetBattlers()[i].GetAlive() && 
                battler.GetTeam() != battleSimulation.GetBattlers()[i].GetTeam())
            {
                float distance = Vector2.Distance(battleSimulation.GetBattlers()[i].GetPosition(), battler.GetPosition());
                if (distance < comparison)
                {
                    comparison = distance;
                    target = new LuaTarget(battleSimulation.GetBattlers()[i]);
                }
            }
        }
        return target;
    }
}
