using MoonSharp.Interpreter;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcBattleController
{
    private LuaBattleInfo battleInfo;
    private Battler battler;
    private LuaBattler controlled;
    private string filename;
    private Script script;
    private DynValue mainFunction;
    private object[] arguments;

    public NpcBattleController(LuaBattleInfo battleInfo, Battler battler)
    {
        this.battleInfo = battleInfo;
        this.controlled = new LuaBattler(battler);
        this.filename = battler.GetData().GetActive_Entity().GetController().GetData().GetCombatScript();
        this.script = new Script();
        String fileContents = FileTools.GetFileRaw("controllers/scripts/" + filename + ".lua");

        this.script.DoString(fileContents);
        mainFunction = this.script.Globals.Get("main");
        arguments = new object[2];
        arguments[0] = controlled;
        arguments[1] = battleInfo;
        this.battler = battler;
    }

    internal void Run()
    {
        this.controlled.Reset();
        DynValue rValue = this.script.Call(mainFunction, arguments);
        this.battler.GetData().SetAttacks(controlled.GetAttacks());
    }
}
