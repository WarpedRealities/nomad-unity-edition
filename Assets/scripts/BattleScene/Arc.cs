using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arc : MonoBehaviour
{
    public ArcDisplay[] rods;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BuildArc(float width, float length)
    {
        Debug.Log("width" + width);
        for (int i = 0; i < rods.Length; i++)
        {
            rods[i].BuildDisplay(i == 1, length);
        }
        rods[0].transform.Rotate(new Vector3(0, 0,-width));
        rods[2].transform.Rotate(new Vector3(0, 0, width));
    }
}
