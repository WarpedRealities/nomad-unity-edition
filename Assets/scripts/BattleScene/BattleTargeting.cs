using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTargeting : MonoBehaviour
{
    List<Targetable> targetables = new List<Targetable>();
    Targetable currentTarget = null;
    public GameObject targetIcon;
    public int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateTargeting(Battler [] battlers, List<Spawnable> spawnables)
    {
        targetables.Clear();
        for (int i = 0; i < battlers.Length; i++)
        {
            if (battlers[i].GetAlive() && battlers[i].GetTeam() == 1)
            {
                targetables.Add(battlers[i]);
            }
        }
        foreach (Spawnable spawn in spawnables)
        {
            if (spawn.GetTeam() == 1)
            {
                targetables.Add(spawn);
            }
        }
        SetCurrentTarget();
    }

    public void RepositionTargeter()
    {
        if (currentTarget != null)
        {
            targetIcon.transform.position = currentTarget.GetPosition();
        }
    }

    public void Increment()
    {
        index++;
        if (index >= targetables.Count)
        {
            index = 0;
        }
        currentTarget = targetables[index];
        RepositionTargeter();
    }

    public void Decrement()
    {
        index--;
        if (index < 0)
        {
            index = targetables.Count - 1;
        }
        currentTarget = targetables[index];
        RepositionTargeter();
    }

    private void SetCurrentTarget()
    {
        if (currentTarget == null)
        {
            currentTarget = targetables[0];
            index = 0;
        }
        else
        {
            if ((!currentTarget.GetAlive() ||
                !targetables.Contains(currentTarget)) &&
                targetables.Count>0)
            {
                currentTarget = targetables[0];
                index = 0;
            }
        }
        RepositionTargeter();
    }

    public void SetActive(bool active)
    {
        targetIcon.SetActive(active);

    }

    internal Targetable GetTarget()
    {
        return currentTarget;
    }
}
