using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    Battler playerShip;
    GameObject[] backgroundStars;
    GameObject[] backgroundObfuscation;
    GameObject[] backgroundDetritus;
    public Color[] colours;
    public Sprite[] starSprites;
    public Sprite[] obfuscationSprites;
    public Sprite[] hazardSprites;
    public GameObject prefabObfuscation;
    public GameObject prefabHazard;
    public GameObject prefabStar;

    public void SetPlayerShip(Battler playerShip)
    {
        this.playerShip = playerShip;
    }

    private void Awake()
    {
        float[] c = GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetColor();
        float[] h = GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetHighlight();
        colours = new Color[2];
        colours[0] = new Color(c[0], c[1], c[2], c[3]);
        colours[1] = new Color(h[0], h[1], h[2], h[3]);
        BuildBackground();
    }

    void BuildBackground()
    {
        backgroundStars = new GameObject[256];
        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                Vector2 s = new Vector2(-17.0F + (i*2), -17.0F + (j*2));
                s.x += DiceRoller.GetInstance().RollOdds()*2;
                s.y += DiceRoller.GetInstance().RollOdds()*2;
                s *= 4;
                GameObject gameObject = Instantiate(prefabStar);
                SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
                spriteRenderer.sprite = starSprites[DiceRoller.GetInstance().RollDice(16)];
                gameObject.transform.position = s;
                backgroundStars[(i * 16) + j] = gameObject;
            }
        }
        backgroundObfuscation = new GameObject[256];
        for (int i = 0; i< 256; i++)
        {
            Vector2 s = new Vector2(-16+DiceRoller.GetInstance().RollDice(32),-16+DiceRoller.GetInstance().RollDice(32));
            s.x += DiceRoller.GetInstance().RollOdds();
            s.y += DiceRoller.GetInstance().RollOdds();
            s *= 4;
            GameObject gameObject = Instantiate(prefabObfuscation);
            SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = obfuscationSprites[DiceRoller.GetInstance().RollDice(4)];
            gameObject.transform.position = s;
            bool highlight = DiceRoller.GetInstance().RollDice(2) == 0;
            spriteRenderer.material.color = highlight ? colours[1] : colours[0];
            backgroundObfuscation[i] = gameObject;
        }
        backgroundDetritus = new GameObject[256];
        for (int i = 0; i < 256; i++)
        {
            Vector2 s = new Vector2(-16 + DiceRoller.GetInstance().RollDice(32), -16 + DiceRoller.GetInstance().RollDice(32));
            s.x += DiceRoller.GetInstance().RollOdds();
            s.y += DiceRoller.GetInstance().RollOdds();
            s *= 4;
            GameObject gameObject = Instantiate(prefabHazard);
            SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = hazardSprites[DiceRoller.GetInstance().RollDice(4)];
            gameObject.transform.position = s;
            backgroundDetritus[i] = gameObject;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
