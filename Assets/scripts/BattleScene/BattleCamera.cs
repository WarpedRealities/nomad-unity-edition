using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleCamera : MonoBehaviour
{
    Vector3 cameraPosition;
    readonly float CameraConstraint = 5;
    Battler player;
    // Start is called before the first frame update
    void Start()
    {
        cameraPosition = new Vector3(0, 0);
    }

    public void SetupCamera(Battler player)
    {
        this.player = player;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position= new Vector3(player.transform.position.x + cameraPosition.x,
            player.transform.position.y+cameraPosition.y, -10);
    }

    internal void AdjustPosition(float x, float y)
    {
        this.cameraPosition.x += x;
        this.cameraPosition.y += y;
        cameraPosition.x = cameraPosition.x > CameraConstraint ? CameraConstraint : cameraPosition.x;
        cameraPosition.x = cameraPosition.x < -CameraConstraint ? -CameraConstraint : cameraPosition.x;
        cameraPosition.y = cameraPosition.y > CameraConstraint ? CameraConstraint : cameraPosition.y;
        cameraPosition.y = cameraPosition.y < -CameraConstraint ? -CameraConstraint : cameraPosition.y;
    }
}
