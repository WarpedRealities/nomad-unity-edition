using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlerExpectation : MonoBehaviour
{
    public BattlerSprite sprite;
    Battler origin;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Setup(Battler origin, float alpha=0.5F)
    {
        this.origin = origin;
        sprite.Setup(origin.GetData().GetSpriteData());
        sprite.SetAlpha(alpha);
    }

    public void UpdateExpectation(int increments=10)
    {
        Vector2 position = origin.transform.position;
        float velocity = origin.GetData().GetCombatStats().GetSpeed() * 
            origin.GetData().GetMovePosition().thrust * 0.1F * 
            NumericConstants.BATTLE_SPEED;
        float turn = (1+(origin.GetData().GetCombatStats().GetManouver()* NumericConstants.MANOUVER_MULTI)) *
            origin.GetData().GetMovePosition().GetDynamicTurn() * 0.1F * 
            NumericConstants.BATTLE_TURN;
        float facing = origin.GetData().GetMovePosition().facing;
        for (int i = 0; i < increments; i++) {
            Vector2 offset = new Vector2(velocity * -Mathf.Sin(facing), velocity * Mathf.Cos(facing));
            position += offset;
            facing += turn;
        }
        transform.eulerAngles = new Vector3(0, 0, facing* 57.3F);
        transform.position = position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
