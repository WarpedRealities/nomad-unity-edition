using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battler : MonoBehaviour, Targetable
{
    BattlerData data;
    ProcessManager processManager;
    int team;
    float size = 0;
    public BattlerSprite spriteScript;
    public void Setup(Active_Entity active_Entity)
    {
        team = GlobalGameState.GetInstance().getCurrentEntity() == active_Entity ? 0 : 1;
        if (active_Entity is Spaceship)
        {
            Spaceship ship = (Spaceship)active_Entity;
            processManager = new SpaceshipProcessManager(ship);
            BattleSpriteData spriteData = new BattleSpriteData(ship.getSprite(), 0, ship.GetSpaceshipStats().GetWidth()/5);
            size = ship.GetSpaceshipStats().GetWidth() / 5;
            data = new BattlerData(ship, spriteData, ship.GetCommon(), ship.GetCombatStats(), ship.GetResources());
        }
        if (active_Entity is Creature)
        {
            Creature creature = (Creature)active_Entity;
            size = creature.getSize();
            BattleSpriteData spriteData = new BattleSpriteData(creature.getSprite(), 0, size);
            data = new BattlerData(active_Entity, spriteData, active_Entity.GetCommon(), creature.GetCombatStats(), null);
        }
        spriteScript.Setup(data.GetSpriteData());
        //transform.position = new Vector3(0, 0, 0);
       // this.data = data;
    }

    public void StartSimulation()
    {
        spriteScript.SetRunning(true);
        spriteScript.SetSpeed(data.GetMovePosition().thrust);
    }

    internal void SimulationUpdate(float deltaTime)
    {
        
        float velocity = data.GetCombatStats().GetSpeed() * data.GetMovePosition().thrust* deltaTime* NumericConstants.BATTLE_SPEED;
        float turn = (1 + (data.GetCombatStats().GetManouver()* 
            NumericConstants.MANOUVER_MULTI)) * 
            data.GetMovePosition().GetDynamicTurn() * deltaTime* NumericConstants.BATTLE_TURN;
        data.GetMovePosition().facing += turn;
        float facing = data.GetMovePosition().facing;
        Vector3 offset = new Vector3(velocity* -Mathf.Sin(facing), velocity * Mathf.Cos(facing));
        transform.eulerAngles = new Vector3(0, 0, facing*57.3F);
        transform.position = transform.position + offset;

    }

    internal void FinishSimulation()
    {
        if (processManager != null)
        {
            processManager.RunProcess(10);
        }
        if (data.GetActive_Entity() is Spaceship)
        {
            List<WeaponSystem> weapons = this.GetData().GetCombatStats().GetWeaponSystems();
            for (int j = 0; j < weapons.Count; j++)
            {
                weapons[j].InitializeAmmoType(this.GetData());
            }
        }
        this.spriteScript.SetRunning(false);
        this.HandleReloads();
    }

    private void HandleReloads()
    {       
        for (int i = 0; i < data.GetCombatStats().GetWeaponSystems().Count; i++)
        {
            if (data.GetCombatStats().GetWeaponSystems()[i].GetMagazine() == 0)
            {
                data.GetCombatStats().GetWeaponSystems()[i].HandleReload();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public BattlerData GetData()
    {
        return data;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GetAlive())
        {
            transform.Rotate(new Vector3(0,0,10 * Time.deltaTime));
        }
    }

    public void SetMovePosition(BattlerMovePosition battlerMovePosition)
    {
        this.data.SetMovePosition(battlerMovePosition);
        transform.position = new Vector3(data.GetMovePosition().position.x, data.GetMovePosition().position.y, 0);
        transform.eulerAngles = new Vector3(0, 0, -data.GetMovePosition().facing);
    }

    public Vector2 GetPosition()
    {
        return transform.position;
    }

    public bool GetAlive()
    {
        return data.GetCommonStats().GetHp()>0;
    }

    public int GetTeam()
    {
       return team;
    }

    public int GetDefence()
    {
        return (int)data.GetCombatStats().GetDynamicDefence(data.GetMovePosition().thrust);
    }

    public Vector2 GetEstimatedPosition(float deltaTime)
    {
        float velocity = data.GetCombatStats().GetSpeed() * data.GetMovePosition().thrust * deltaTime * NumericConstants.BATTLE_SPEED;
        float facing = data.GetMovePosition().facing;
        Vector3 offset = new Vector3(velocity * -Mathf.Sin(facing), velocity * Mathf.Cos(facing));
        return transform.position + offset;
    }

    public int ApplyDamage(DT damageType, int damage)
    {
        switch (damageType)
        {
            case DT.NORMAL:
                damage -= (int)data.GetCombatStats().GetArmour();
                data.GetCommonStats().ModHP(-damage);
                return damage;

        }
        return 0;
    }

    internal void RemoveCoolant()
    {
        if (this.data.GetActive_Entity() is Spaceship)
        {
            Spaceship ship = (Spaceship)this.data.GetActive_Entity();
            ship.GetResources().GetResource("RES_COOLANT").amount = 0;
        }
    }

    public float GetSize()
    {
        return size;
    }
}
