using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlerSprite : MonoBehaviour
{
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    public float speed;
    public bool running, flip;
    private float clock;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    internal void SetAlpha(float alpha)
    {
        meshRenderer.material.color = new Color(1, 1, 1, alpha);
    }

    // Update is called once per frame
    void Update()
    {
        if (running)
        {
            clock += Time.deltaTime;
            if (clock > 0.1F)
            {
                clock = 0;
                int frame = GetFrame()+ (flip ? 1: 0);
                Regenerate(frame);
                flip = !flip;
            }
        }
    }

    private int GetFrame()
    {
        if (speed < 0.33F)
        {
            return 0;
        }
        if (speed > 0.66F)
        {
            return 2;
        }
        return 1;
    }

    public void Regenerate(int frame)
    {
        float uvW = (frame % 2) * 0.5F;
        float uvH = frame >= 2 ? 0.0F : 0.5F;
        Vector2[] uvmap = new Vector2[4];
        uvmap[0] = new UnityEngine.Vector2(uvW, uvH);
        uvmap[1] = new UnityEngine.Vector2(uvW + 0.5F, uvH);
        uvmap[2] = new UnityEngine.Vector2(uvW, uvH + 0.5F);
        uvmap[3] = new UnityEngine.Vector2(uvW + 0.5F, uvH + 0.5F);
        Mesh mesh = meshFilter.mesh;
        mesh.uv = uvmap;
    }

    public void SetRunning(bool running)
    {
        this.running = running;
    }

    internal void SetSpeed(float thrust)
    {
        this.speed = thrust;
    }

    internal void Setup(BattleSpriteData battleSpriteData)
    {
        meshRenderer.material = SpriteLibrary.getInstance().GetMaterial(battleSpriteData.GetSpriteSheet(),"battle/");
        Mesh mesh = new Mesh();
        int sprite = 0;
        Vector3[] vertices = new Vector3[4];
        Vector2[] uvmap = new Vector2[4];
        Vector3[] normals = new Vector3[4];
        int[] indices = new int[6];
        float offset = -0.5F;
        vertices[0] = new UnityEngine.Vector3(-0.5F, offset, 0);
        vertices[1] = new UnityEngine.Vector3(-0.5F + 1, offset, 0);
        vertices[2] = new UnityEngine.Vector3(-0.5F, offset + 1, 0);
        vertices[3] = new UnityEngine.Vector3(-0.5F + 1, offset + 1, 0);

        float uvW = (sprite % 2) * 0.5F;
        float uvH = sprite >= 2 ? 0.0F : 0.5F;
        uvmap[0] = new UnityEngine.Vector2(uvW, uvH);
        uvmap[1] = new UnityEngine.Vector2(uvW + 0.5F, uvH);
        uvmap[2] = new UnityEngine.Vector2(uvW, uvH + 0.5F);
        uvmap[3] = new UnityEngine.Vector2(uvW + 0.5F, uvH + 0.5F);

        indices[0] = 0;
        indices[1] = 1;
        indices[2] = 3;
        indices[3] = 0;
        indices[4] = 3;
        indices[5] = 2;

        normals[0] = -UnityEngine.Vector3.forward;
        normals[1] = -UnityEngine.Vector3.forward;
        normals[2] = -UnityEngine.Vector3.forward;
        normals[3] = -UnityEngine.Vector3.forward;

        mesh.vertices = vertices;
        mesh.uv = uvmap;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;
        transform.localScale = new Vector3(battleSpriteData.GetSize(), battleSpriteData.GetSize());
    }
}
