﻿using UnityEngine;
using System.Collections;

public interface Targetable 
{

    Vector2 GetPosition();
    Vector2 GetEstimatedPosition(float time);

    bool GetAlive();

    int GetTeam();
    int GetDefence();
    int ApplyDamage(DT damageType, int damage);
    float GetSize();
}
