﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BattleSpriteData
{
    string spritesheet;
    int animationMode;
    float size;
    public BattleSpriteData(string sheet, int mode, float size)
    {
        this.size = size;
        this.spritesheet = sheet;
        this.animationMode = mode;
    }

    public float GetSize()
    {
        return size;
    }

    public string GetSpriteSheet()
    {
        return spritesheet;
    }
    public int GetAnimationMode()
    {
        return animationMode;
    }
}

public class BattlerMovePosition
{
    public Vector2 position;
    public float facing;
    public float turn=0, thrust=0.5F;

    public BattlerMovePosition(Vector2 position, float facing)
    {
        this.facing = facing;
        this.position = position;
    }

    internal void SetTurn(float turn)
    {
        this.turn = turn;
        this.turn = turn > 1 ? 1 : turn;
        this.turn = turn < -1 ? -1 : turn;
    }

    public float GetDynamicTurn()
    {
        return (1.0F - (thrust/2)) * turn;
    }
}

public class BattleAttack
{
    public WeaponSystem chosenWeapon;
    public Targetable target;
    public float clock;
    public int shotsLeft;
    public bool executed;

    public BattleAttack(Targetable target, WeaponSystem weapon)
    {
        this.target = target;
        this.chosenWeapon = weapon;
        shotsLeft = weapon.GetWeapon().GetVolley();
    }
}

public class BattlerData 
{
    BattlerMovePosition movePosition;
    BattleSpriteData spriteData;
    CommonStats commonStats;
    CombatStats combatStats;
    SpaceshipResourceStats resourceStats;
    Active_Entity active_Entity;
    List<BattleAttack> attacks;

    internal BattleSpriteData GetSpriteData()
    {
        return spriteData;
    }

    public BattlerData(Active_Entity active_Entity, BattleSpriteData spriteData, CommonStats commonStats,CombatStats combatStats, SpaceshipResourceStats resourceStats)
    {
        this.active_Entity = active_Entity;
        this.spriteData = spriteData;
        this.commonStats = commonStats;
        this.combatStats = combatStats;
        this.resourceStats = resourceStats;
    }

    public CombatStats GetCombatStats()
    {
        return this.combatStats;
    }

    public CommonStats GetCommonStats()
    {
        return this.commonStats;
    }

    public void SetMovePosition(BattlerMovePosition movePosition)
    {
        this.movePosition = movePosition;
    }

    public BattlerMovePosition GetMovePosition()
    {
        return movePosition;
    }

    public void SetAttacks(List<BattleAttack> attacks)
    {
        this.attacks = attacks;
    }
    public List<BattleAttack> GetAttacks()
    {
        return attacks;
    }

    public Active_Entity GetActive_Entity()
    {
        return active_Entity;
    }

    internal BattleAttack GetReadyAttack()
    {
        if (attacks == null)
        {
            return null;
        }
        for (int i = 0; i < attacks.Count; i++)
        {
            if (!attacks[i].executed)
            {
                return attacks[i];
            }
        }
        return null;
    }
}
