using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSimulation : MonoBehaviour, BattleSim
{

    Battler[] battlers;
    List<Spawnable> spawnables = new List<Spawnable>();

    const float INTERVAL = 0.1F;
    float clock, activeClock;
    bool running;
    public BattleHud battleHud;
    public BattlerExpectation expectation;
    public BattleControl battleControl;
    public BattleTargeting battleTargeting;
    public AttackResolver attackResolver;
    public void SetupBattlers(Battler[] battlers)
    {
        this.battlers = battlers;
        battleTargeting.UpdateTargeting(battlers, spawnables);
        for (int i=0;i< battlers.Length; i++)
        {
            List<WeaponSystem> weapons= this.battlers[i].GetData().GetCombatStats().GetWeaponSystems();
            for (int j = 0; j < weapons.Count; j++)
            {
                weapons[j].InitializeMagazine();
                weapons[j].InitializeAmmoType(battlers[i].GetData());
            }
        }
        attackResolver = new AttackResolver(GetComponent<RepresentationManager>());
        
    }

    public Battler[] GetBattlers()
    {
        return battlers;
    }

    public List<Spawnable> GetSpawnables()
    {
        return spawnables;
    }

    public void AddSpawnable(Spawnable spawn)
    {
        spawnables.Add(spawn);
    }

    public void RemoveSpawnable(Spawnable spawn)
    {
        spawnables.Remove(spawn);
    }

    // Start is called before the first frame update
    void Start()
    {
        battleControl = GetComponent<BattleControl>();
    }

    public void RunSimulation()
    {
        clock = 0;
        running = true;
        
        for (int i = 0; i < battlers.Length; i++)
        {
            battlers[i].StartSimulation();
        }
        for (int i = 0; i < spawnables.Count; i++)
        {
            spawnables[i].StartSimulation();
        }
        battleTargeting.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (running)
        {
            clock += Time.deltaTime;
            activeClock += Time.deltaTime;
            for (int i = 0; i < battlers.Length; i++)
            {
                battlers[i].SimulationUpdate(Time.deltaTime);
            }
            for (int i = 0; i < spawnables.Count; i++)
            {
                spawnables[i].UpdateLogic(Time.deltaTime);
            }
            if (activeClock > INTERVAL)
            {
                activeClock = 0;
                for (int i = 0; i < battlers.Length; i++)
                {
                    if (battlers[i].GetAlive())
                    {
                        attackResolver.ResolveAttacks(battlers[i], this);
                    }
                }
            }
            if (clock > 1)
            {
                clock = 0;
                running = false;
                for (int i = 0; i < battlers.Length; i++)
                {
                    battlers[i].GetData().SetAttacks(null);
                    battlers[i].FinishSimulation();
                }
                for (int i = spawnables.Count-1; i >= 0; i--)
                {
                    spawnables[i].FinishSimulation();
                }
                GlobalGameState.GetInstance().IncrementClock(10);
                battleHud.UpdateUI();
                CheckEndState();
                battleControl.UpdateWeaponStates();
                expectation.UpdateExpectation();
                battleTargeting.SetActive(true);
                battleTargeting.UpdateTargeting(battlers, spawnables);
            }
        }
    }

    internal bool IsRunning()
    {
        return running;
    }

    public void CheckEndState()
    {
        //check player health
        if (!battlers[0].GetAlive())
        {
            EndHandler(0);
            return;
        }
        //check all enemies defeated
        if (EnemiesDefeated())
        {
            EndHandler(1);
            return;
        }
        //check player more than 20 units away from the enemy
        float distance = 99;
        for (int i = 1; i < battlers.Length; i++)
        {
            if (battlers[i].GetAlive())
            {
                float d = Vector2.Distance(battlers[0].GetPosition(), battlers[i].GetPosition());
                if (d < distance)
                {
                    distance = d;
                }
            }
        }
        if (distance > 20)
        {
            EndHandler(-1);
        }
    }

    private void EndHandler(int state)
    {
        for (int i = 0; i < battlers.Length; i++)
        {
            battlers[i].RemoveCoolant();
        }
        battleControl.SetActive(false);
        battleHud.battleRecap.Initialize(state);
    }

    private bool EnemiesDefeated()
    {
        for (int i = 1; i < battlers.Length; i++)
        {
            if (battlers[i].GetAlive())
            {
                return false;
            }
        }
        return true;
    }
}
