
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleHud : MonoBehaviour
{
    public GameObject resourcePrefab;
    public GameObject weaponPrefab;
    private Spaceship spaceship;
    private Battler battler;
    public BattleControl battleControl;
    public BattleRecap battleRecap;
    private HudResource[] resourceTexts;
    private HudWeapon[] weaponTexts;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetPlayer(Battler battler, Spaceship spaceship)
    {
        this.spaceship = spaceship;
        this.battler = battler;
        BuildUI();
    }

    public void UpdateUI()
    {
        resourceTexts[0].text.text = "Hull " + spaceship.GetCommon().GetHp() + "/" + spaceship.GetCommon().GetMaxHp();
        for (int i = 0; i < spaceship.GetResources().GetResourceList().Count; i++)
        {

            resourceTexts[i + 1].text.text = spaceship.GetResources().GetResourceList()[i].displayName + 
                " " +
                spaceship.GetResources().GetResourceList()[i].amount.ToString("F1") +
                "/" + spaceship.GetResources().GetResourceList()[i].capacity;
        }
        int offset = spaceship.GetResources().GetResourceList().Count + 1;
        for (int i = 0; i < spaceship.GetResources().GetMagazineList().Count; i++)
        {

            resourceTexts[i + offset].text.text = 
                spaceship.GetResources().GetMagazineList()[i].GetAmmo() +
                " " +
                spaceship.GetResources().GetMagazineList()[i].amount;


        }
        for (int i = 0; i < spaceship.GetCombatStats().GetWeaponSystems().Count; i++)
        {
            weaponTexts[i].SetState(this.battleControl.GetWeaponStates()[i]);
            weaponTexts[i].text.text = 
                GetWeaponString(i) + " " + spaceship.GetCombatStats().GetWeaponSystems()[i].GetString();
        }
    }

    private void BuildUI()
    {
        int count = spaceship.GetResources().GetMagazineList().Count + spaceship.GetResources().GetResourceList().Count;
        resourceTexts = new HudResource[count+1];
        //setup hull
        HudResource res = Instantiate(resourcePrefab, transform).GetComponent<HudResource>();
        RectTransform t = res.GetComponent<RectTransform>();
        t.anchoredPosition = new Vector3(0, 0);
        res.text.text = "Hull " + spaceship.GetCommon().GetHp() + "/" + spaceship.GetCommon().GetMaxHp();
        resourceTexts[0] = res;
        for (int i = 0; i < spaceship.GetResources().GetResourceList().Count; i++)
        {
            res = Instantiate(resourcePrefab,transform).GetComponent<HudResource>();
            t = res.GetComponent<RectTransform>();
            t.anchoredPosition= new Vector3(0, (20 * i)+20);
            res.text.text = spaceship.GetResources().GetResourceList()[i].displayName + " " + spaceship.GetResources().GetResourceList()[i].amount.ToString("F1") + "/" + spaceship.GetResources().GetResourceList()[i].capacity;
            resourceTexts[i+1] = res;
        }
        int offset = spaceship.GetResources().GetResourceList().Count+1;
        for (int i = 0; i < spaceship.GetResources().GetMagazineList().Count; i++)
        {
            res = Instantiate(resourcePrefab, transform).GetComponent<HudResource>();
            t = res.GetComponent<RectTransform>();
            t.anchoredPosition = new Vector3(0, (20 * (i + offset)));

            res.text.text = spaceship.GetResources().GetMagazineList()[i].GetAmmo() + " " + spaceship.GetResources().GetMagazineList()[i].amount;
            resourceTexts[i+offset] = res;

        }
        count = spaceship.GetCombatStats().GetWeaponSystems().Count;
        weaponTexts = new HudWeapon[count];
        for (int i = 0; i < count; i++)
        {
            HudWeapon weapon = Instantiate(weaponPrefab, transform).GetComponent<HudWeapon>();
            t = weapon.GetComponent<RectTransform>();
            t.anchoredPosition = new Vector3(0, (20 * (count-i))-20);
            weapon.text.text = GetWeaponString(i)+" "+spaceship.GetCombatStats().GetWeaponSystems()[i].GetString();
            weaponTexts[i] = weapon;
        }
    }

    private string GetWeaponString(int index)
    {
        return ((index + 1) % 10).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
