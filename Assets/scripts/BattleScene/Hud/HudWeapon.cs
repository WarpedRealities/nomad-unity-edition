using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HudWeapon : MonoBehaviour
{
    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void SetState(WeaponState weaponState)
    {
        switch (weaponState)
        {
            case WeaponState.ENABLED:
                text.color = Color.white;
                break;
            case WeaponState.DISABLED:
                text.color = Color.grey;
                break;
            case WeaponState.CONFIRMED:
                text.color = Color.green;
                break;
            case WeaponState.UNSELECTED:
                text.color = Color.yellow;
                break;
            case WeaponState.INCAPABLE:
                text.color = Color.red;
                break;
        }
    }
}
