﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


    public class BattleRecap : MonoBehaviour
    {
        public TextMeshProUGUI title;
        int state;

        internal void Initialize(int state)
        {
            this.state = state;
            this.gameObject.SetActive(true);
            switch (state)
            {
                case 1:
                    title.text = "VICTORY";
                    break;
                case 0:
                    title.text = "DEFEAT";
                    break;
                case -1:
                    title.text = "ESCAPE";
                    break;
            }
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ProgressButton()
        {
            Encounter encounter = GameObject.Find("Encounter").GetComponent<Encounter>();
            EncounterData encounterData = encounter.GetData();
            SceneManager.LoadScene("SolarScene");

            encounter.ReconcileEnemies();
            encounter.SetState(state);
            if (state == -1)
            {
                encounter.PlayerEscape();
            }
        }
    }
