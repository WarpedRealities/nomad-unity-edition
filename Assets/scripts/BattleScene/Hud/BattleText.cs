using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BattleText : MonoBehaviour
{
    public TextMeshPro text;
    float lifespan;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        lifespan -= Time.deltaTime;
        transform.Translate(new Vector3(0, 1 * Time.deltaTime, 0));
        if (lifespan < 0)
        {
            gameObject.SetActive(false);
        }
    }

    public bool IsAlive()
    {
        return lifespan > 0;
    }

    public void Initialize(Vector2 position, string text, Color color)
    {
        gameObject.SetActive(true);
        this.transform.position = position;
        this.text.text = text;
        this.text.color = color;
        this.lifespan = 1;

    }
}
