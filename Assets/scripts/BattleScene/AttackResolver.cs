﻿using UnityEngine;
using System.Collections;
using System;

public class AttackResolver
{
    RepresentationManager representationManager;

    public AttackResolver( RepresentationManager representationManager)
    {
        this.representationManager = representationManager;
    }


    internal void ResolveAttacks(Battler battler, BattleSimulation battleSimulation)
    {
        BattleAttack battleAttack = battler.GetData().GetReadyAttack();
        if (battleAttack == null)
        {
            return;
        }
     
        String ammoCode = battleAttack.chosenWeapon.GetAmmoType() != null ? 
            battleAttack.chosenWeapon.GetAmmoType().GetCode() :
            null;
        //range check
        float distance = Vector2.Distance(battleAttack.target.GetPosition(), battler.GetPosition());

        if (distance > battleAttack.chosenWeapon.GetWeapon().GetRange(ammoCode))
        {

            return;
        }
        //arc check
        if (!ArcCheck(battleAttack, battler))
        {

            return;
        }
        //resource check
        if (battler.GetData().GetActive_Entity() is Spaceship)
        {
            if (!battler.GetData().GetCombatStats().CheckWeaponUseable(battleAttack.chosenWeapon, 
                ((Spaceship)battler.GetData().GetActive_Entity()).GetResources()))
            {

                battleAttack.executed = true;
                return;
            }
        }
        else
        {
            if (!battler.GetData().GetCombatStats().CheckWeaponUseable(battleAttack.chosenWeapon,null))
            {

                battleAttack.executed = true;
                return;
            }
        }

        //subtract resources
        if (battler.GetData().GetActive_Entity() is Spaceship)
        {
            SubtractResources((Spaceship)battler.GetData().GetActive_Entity(), battleAttack);
        }

        battleAttack.shotsLeft--;
        if (battleAttack.shotsLeft <= 0)
        {
            battleAttack.executed = true;
        }
        battleAttack.chosenWeapon.UseMagazine(1);

        //roll attack
        bool result = battleAttack.chosenWeapon.GetWeapon() is DirectWeapon ? 
            RollDirect(battleAttack, battler, ammoCode) : 
            RollStriker(battleAttack, battler);
        //generate representation of attack
        GenerateRepresentation(result, battleAttack, battler);
    }

    private void GenerateRepresentation(bool result, BattleAttack battleAttack, Battler battler)
    {
        if (battleAttack.chosenWeapon.GetWeapon() is DirectWeapon)
        {
            Representation representation = representationManager.ProvideRepresentation();
            representation.Initialize(battleAttack, battler, result);
        }
        if (battleAttack.chosenWeapon.GetWeapon() is StrikeWeapon)
        {
            Striker striker = representationManager.SpawnStriker();
            striker.Initialize(battleAttack, battler);
        }
    }

    private bool RollStriker(BattleAttack battleAttack, Battler battler)
    {
        return true;
    }

    private bool RollDirect(BattleAttack battleAttack, Battler battler, String ammoCode)
    {
        DirectWeapon directWeapon = (DirectWeapon)battleAttack.chosenWeapon.GetWeapon();
        int bonus = battler.GetData().GetCombatStats().GetGunnery()+directWeapon.GetAccuracyModifier();
        float range = Vector2.Distance(battleAttack.target.GetPosition(), battler.GetPosition());
        int penalty = battleAttack.target.GetDefence()+ (int) (directWeapon.GetRangeAccuracyModifier(ammoCode) *range);
        int roll = DiceRoller.GetInstance().RollDice(20) + bonus;
        Debug.Log("penalty " + penalty + " roll " + roll + " bonus "+ bonus);

        return roll >= 4+penalty;
    }

    private void SubtractResources(Spaceship spaceship, BattleAttack battleAttack)
    {
        WeaponCost[] costs = battleAttack.chosenWeapon.GetWeapon().GetWeaponCosts();
        for (int i = 0; i < costs.Length; i++)
        {
            if (costs[i] is ResourceCost)
            {
                ResourceCost resourceCost = (ResourceCost)costs[i];
                spaceship.GetResources().GetResource(resourceCost.GetResource()).amount -= resourceCost.GetAmount();
            }
            if (costs[i] is MagazineCost)
            {
                MagazineCost magazineCost = (MagazineCost)costs[i];
                ShipMagazine magazine = spaceship.GetResources().GetMagazine(battleAttack.chosenWeapon.GetAmmoType().GetCode());
                magazine.amount -= magazineCost.GetAmount();
            }
        }
    }

    private bool ArcCheck(BattleAttack battleAttack, Battler battler)
    {
        //establish vector
        Vector2 axis = new Vector2(0, 1);
        Vector2 angle = Quaternion.AngleAxis(45 * battleAttack.chosenWeapon.GetFacing(), Vector3.forward) * axis;
        Vector2 finalAngle = Quaternion.AngleAxis(battler.GetData().GetMovePosition().facing* 57.3F, Vector3.forward) * angle;
        Vector2 target = (battleAttack.target.GetPosition()-battler.GetPosition()).normalized;
        float degrees = Vector2.Angle(finalAngle, target);
        float threshold = FiringArcManager.GetArc(battleAttack.chosenWeapon.GetArc());
        return degrees < threshold;
    }
}
