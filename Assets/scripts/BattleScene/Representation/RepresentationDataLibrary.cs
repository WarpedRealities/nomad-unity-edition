﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RepresentationDataLibrary 
{
    private Dictionary<string, RepresentationData> representatData;

    private static RepresentationDataLibrary instance;

    public static RepresentationDataLibrary GetInstance()
    {
        if (instance == null)
        {
            instance = new RepresentationDataLibrary();
        }
        return instance;
    }

    private RepresentationDataLibrary()
    {
        representatData = new Dictionary<string, RepresentationData>();
    }
  
    public RepresentationData GetData(string filename)
    {
        if (representatData.ContainsKey(filename))
        {
            return representatData[filename];
        }
        RepresentationData rep = RepresentationData.BuildData(filename);
        representatData.Add(filename, rep);
        return rep;
    }
}
