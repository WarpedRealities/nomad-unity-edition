﻿using UnityEngine;
using System.Collections;
using System;

public class RepresentationAnimation : MonoBehaviour
{
    public bool deactivateOnEnd=true;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    public RepDataEntry dataEntry;
    private float clock = 0, distance =0;
    private int index = 0;
    private readonly float FRAME = 0.05F;
    private bool active = true;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (dataEntry != null && dataEntry.GetLength()>1)
        {
            clock += Time.deltaTime;
            if (clock >= FRAME)
            {
                clock = 0;
                index++;
                if (index>= dataEntry.GetLength())
                {
                    if (dataEntry.IsRepeat())
                    {
                        index = 0;
                        UpdateFrame();
                    }
                    else
                    {
                        active = false;
                        if (deactivateOnEnd)
                        {
                            gameObject.SetActive(false);
                        }
                    }
                }
                else
                {
                    UpdateFrame();
                }
            }
        }
    }

    public bool IsActive()
    {
        return active;
    }

    private void UpdateFrame()
    {
        float width = meshRenderer.material.mainTexture.width;
        float height = meshRenderer.material.mainTexture.height;
        float rWidth = dataEntry.GetWidth() / 32;
        float rHeight = dataEntry.GetHeight() / 32;
        Vector2[] uvmap = GenerateFrame(dataEntry, index, width, height);
        meshFilter.mesh.uv = uvmap;
    }

    public void BeamAdjust(float distance)
    {
        this.distance = distance;
        Mesh mesh = meshFilter.mesh != null ? meshFilter.mesh : new Mesh();
        float width = meshRenderer.material.mainTexture.width;
        float height = meshRenderer.material.mainTexture.height;
        float rWidth = dataEntry.GetWidth() / 32;
        float rHeight = dataEntry.GetHeight() / 32;
        Vector3[] vertices = new Vector3[4];
        vertices[0] = new UnityEngine.Vector3(-0.5F * rWidth, distance, 0);
        vertices[1] = new UnityEngine.Vector3(0.5F * rWidth, distance, 0);
        vertices[2] = new UnityEngine.Vector3(-0.5F * rWidth, 0, 0);
        vertices[3] = new UnityEngine.Vector3(0.5F * rWidth, 0, 0);
        mesh.vertices = vertices;
    }

    public void Generate(RepDataEntry repDataEntry, Material material)
    {
        active = true;
        distance = 0;
        clock = 0;
        index = 0;
        dataEntry = repDataEntry;
        material = material == null ? meshRenderer.material : material;
        float width = material.mainTexture.width;
        float height = material.mainTexture.height;
        float rWidth = repDataEntry.GetWidth() / 32;
        float rHeight = repDataEntry.GetHeight() / 32;
        Mesh mesh = meshFilter.mesh != null ? meshFilter.mesh : new Mesh();

        Vector3[] vertices = new Vector3[4];
        Vector2[] uvmap = GenerateFrame(repDataEntry,0, width, height);
        Vector3[] normals = new Vector3[4];
        int[] indices = new int[6];

        normals[0] = -UnityEngine.Vector3.forward;
        normals[1] = -UnityEngine.Vector3.forward;
        normals[2] = -UnityEngine.Vector3.forward;
        normals[3] = -UnityEngine.Vector3.forward;

        vertices[0] = new UnityEngine.Vector3(-0.5F*rWidth, -0.5F*rHeight, 0);
        vertices[1] = new UnityEngine.Vector3(0.5F*rWidth, -0.5F * rHeight, 0);
        vertices[2] = new UnityEngine.Vector3(-0.5F * rWidth, 0.5F * rHeight, 0);
        vertices[3] = new UnityEngine.Vector3(0.5F*rWidth, 0.5F * rHeight, 0);



        indices[0] = 3;
        indices[1] = 1;
        indices[2] = 0;
        indices[3] = 2;
        indices[4] = 3;
        indices[5] = 0;

        normals[0] = -UnityEngine.Vector3.forward;
        normals[1] = -UnityEngine.Vector3.forward;
        normals[2] = -UnityEngine.Vector3.forward;
        normals[3] = -UnityEngine.Vector3.forward;

        mesh.vertices = vertices;
        mesh.uv = uvmap;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;
        meshRenderer.material = material;
    }

    private Vector2[] GenerateFrame(RepDataEntry repDataEntry, int index, float width, float height)
    {
        float w = repDataEntry.GetWidth() / width;
        float h = distance == 0 ? repDataEntry.GetHeight() / height : (repDataEntry.GetHeight() / height)*distance;
        float x = (repDataEntry.GetX() / width)+(w*((float)index));
        float y = repDataEntry.GetY() / height;
        Vector2[] uv = new Vector2[4];

        uv[0] = new Vector2(x, y);
        uv[1] = new Vector2(x+w, y);
        uv[2] = new Vector2(x, y + h);
        uv[3] = new Vector2(x + w, y + h);

        return uv;

    }

    internal void GenerateBeam(RepDataEntry repDataEntry, Material material, float distance)
    {
        active = true;
        this.distance = distance;
        clock = 0;
        index = 0;
        dataEntry = repDataEntry;
        float width = material.mainTexture.width;
        float height = material.mainTexture.height;
        float rWidth = repDataEntry.GetWidth() / 32;
        float rHeight = repDataEntry.GetHeight() / 32;
        Mesh mesh = meshFilter.mesh != null ? meshFilter.mesh : new Mesh();

        Vector3[] vertices = new Vector3[4];
        Vector2[] uvmap = GenerateFrame(repDataEntry, 0, width, height);
        Vector3[] normals = new Vector3[4];
        int[] indices = new int[6];

        normals[0] = -UnityEngine.Vector3.forward;
        normals[1] = -UnityEngine.Vector3.forward;
        normals[2] = -UnityEngine.Vector3.forward;
        normals[3] = -UnityEngine.Vector3.forward;

        vertices[0] = new UnityEngine.Vector3(-0.5F * rWidth, distance, 0);
        vertices[1] = new UnityEngine.Vector3(0.5F * rWidth, distance, 0);
        vertices[2] = new UnityEngine.Vector3(-0.5F * rWidth, 0, 0);
        vertices[3] = new UnityEngine.Vector3(0.5F * rWidth, 0, 0);



        indices[0] = 3;
        indices[1] = 1;
        indices[2] = 0;
        indices[3] = 2;
        indices[4] = 3;
        indices[5] = 0;

        normals[0] = -UnityEngine.Vector3.forward;
        normals[1] = -UnityEngine.Vector3.forward;
        normals[2] = -UnityEngine.Vector3.forward;
        normals[3] = -UnityEngine.Vector3.forward;

        mesh.vertices = vertices;
        mesh.uv = uvmap;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;
        meshRenderer.material = material;
    }
}
