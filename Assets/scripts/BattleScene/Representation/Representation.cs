﻿using UnityEngine;
using System.Collections;
using System;



public class Representation : MonoBehaviour
{
    class RuntimeData
    {
        public float lifespan, facing, speed, distance;
        public bool hit, beam;
    };

    protected bool alive;
    protected RepresentationData representationData;
    protected BattleAttack attack;
    protected Material material;
    public RepresentationAnimation[] elements;
    public RepresentationManager representationManager;
    private RuntimeData runtimeData;
    public bool IsAlive()
    {
        return alive;
    }
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (alive && runtimeData.lifespan>=0)
        {
            runtimeData.lifespan -= Time.deltaTime;
            RunRayAnimation();
            if (!runtimeData.beam)
            {
                HandleProjectileCollision();
            }
            if (runtimeData.lifespan <= 0)
            {

                if (elements[1].gameObject.activeSelf && runtimeData.beam)
                {
                    elements[0].gameObject.SetActive(false);
                    elements[1].gameObject.SetActive(false);
                    elements[2].gameObject.SetActive(false);
                    if (runtimeData.hit)
                    {
                        BattleEffectResolver.HandleEffects(attack.chosenWeapon.GetWeapon().GetWeaponEffects(),
                         attack, runtimeData.distance, representationManager);
                    }
                    else
                    {
                        BattleText battleText = representationManager.ProvideText();
                        battleText.Initialize(attack.target.GetPosition(), "miss", Color.white);
                    }
                }
                if (elements[1].gameObject.activeSelf)
                {
                    elements[1].gameObject.SetActive(false);
                    if (!runtimeData.beam)
                    {
                        if (runtimeData.hit)
                        {
                            elements[2].gameObject.SetActive(true);

                            elements[2].transform.position = elements[1].transform.position;
                            BattleEffectResolver.HandleEffects(attack.chosenWeapon.GetWeapon().GetWeaponEffects(),
                                attack, runtimeData.distance, representationManager);
                        }
                        else
                        {
                            BattleText battleText= representationManager.ProvideText();
                            battleText.Initialize(attack.target.GetPosition(), "miss", Color.white);
                        }
                    }
                }
            }
        }
        if (alive && NoneActive())
        {
            alive = false;
        }
    }

    private void HandleProjectileCollision()
    {
        float distance = Vector2.Distance(elements[1].transform.position, attack.target.GetPosition());
        if (distance < attack.target.GetSize()*0.5F)
        {
            runtimeData.lifespan = 0;
        }
    }

    private void RunRayAnimation()
    {
        if (runtimeData.beam)
        {

            elements[1].transform.position = elements[0].transform.position;

            if (runtimeData.hit)
            {
                elements[1].transform.rotation = Quaternion.identity;
                Vector2 target = (attack.target.GetPosition() - (Vector2)elements[0].transform.position).normalized;

                float angle = Vector2.SignedAngle(target, new Vector2(0, 1));
                elements[1].transform.Rotate(0, 0, -angle);
                float d = Vector2.Distance(elements[0].transform.position, attack.target.GetPosition());
                elements[1].BeamAdjust(d);
            }
        }
        else
        {
            //move projectile
            float velocity = runtimeData.speed * Time.deltaTime;
            float facing = runtimeData.facing/ -57.3F;
            Vector3 offset = new Vector3(velocity * -Mathf.Sin(facing), velocity * Mathf.Cos(facing));
            elements[1].transform.position = elements[1].transform.position + offset;
        }
    }

    private bool NoneActive()
    {
        for (int i = 0; i < elements.Length; i++)
        {
            if (elements[i].gameObject.activeSelf)
            {
                return false;
            }
        }
        return true;
    }

    internal void Initialize(BattleAttack attack, Battler battler, bool isHit)
    {
        for (int i = 0; i < elements.Length; i++)
        {
            elements[i].gameObject.SetActive(true);
        }
        this.attack = attack;
        WeaponSystem chosenWeapon = attack.chosenWeapon;
        material = SpriteLibrary.getInstance().GetMaterial(
            ((DirectWeapon)chosenWeapon.GetWeapon()).GetSpriteSheet(chosenWeapon.GetAmmoType()),
            "representation/");
        representationData = RepresentationDataLibrary.GetInstance().GetData(
            ((DirectWeapon)chosenWeapon.GetWeapon()).GetSpriteSheet(chosenWeapon.GetAmmoType()));

        BuildOrigin(attack, battler);
        bool isBeam = ((DirectWeapon)chosenWeapon.GetWeapon()).GetWeaponVisuals().isBeam;
        if (isBeam)
        {
            string ammotype = chosenWeapon.GetAmmoType() != null ? chosenWeapon.GetAmmoType().GetCode() : null;
            runtimeData = new RuntimeData();
            runtimeData.hit = isHit;
            runtimeData.distance = Vector2.Distance(battler.GetPosition(), attack.target.GetPosition());
            runtimeData.beam = isBeam;
            runtimeData.lifespan = ((DirectWeapon)chosenWeapon.GetWeapon()).GetWeaponVisuals().speed;
            runtimeData.facing = Vector2.SignedAngle(attack.target.GetPosition(), new Vector2(0, 1));
            
            if (isHit)
            {
                BuildImpact(attack, battler);
                float facing = runtimeData.facing* 57.3F;
                Vector3 offset = new Vector3(
                    runtimeData.distance * -Mathf.Sin(facing), 
                    runtimeData.distance * Mathf.Cos(facing));
                elements[2].transform.position = elements[1].transform.position + offset;
                elements[2].transform.SetParent(battler.transform);

            }
            else
            {
                runtimeData.distance = ((DirectWeapon)chosenWeapon.GetWeapon()).GetRange(ammotype);
            }
            BuildRayBeam(attack, battler);
        }
        else
        {
            BuildRayProjectile(attack, battler);
            runtimeData = new RuntimeData();
            runtimeData.hit = isHit;
            float speed = ((DirectWeapon)chosenWeapon.GetWeapon()).GetWeaponVisuals().speed;
            float distance = Vector2.Distance(battler.GetPosition(), attack.target.GetPosition());
            float time = distance / speed;
            Vector2 targetPosition = attack.target.GetEstimatedPosition(time);
            distance = Vector2.Distance(battler.GetPosition(), targetPosition);
            Vector2 target = (attack.target.GetPosition() - battler.GetPosition()).normalized;
            runtimeData.facing = Vector2.SignedAngle(target, new Vector2(0, 1));
            string ammotype = chosenWeapon.GetAmmoType()!=null ? chosenWeapon.GetAmmoType().GetCode() : null;
            runtimeData.speed = speed;
            runtimeData.beam = isBeam;
            runtimeData.distance = Vector2.Distance(battler.GetPosition(), attack.target.GetPosition());
            runtimeData.lifespan = isHit ? distance / speed: 
                ((DirectWeapon)chosenWeapon.GetWeapon()).GetRange(ammotype);
            BuildImpact(attack, battler);
        }
        alive = true;
    }

    private void BuildRayBeam(BattleAttack attack, Battler battler)
    {
        elements[1].transform.rotation = Quaternion.identity;
        elements[1].transform.SetParent(null);
        elements[1].transform.position = elements[0].transform.position;
        Vector2 target = (attack.target.GetPosition() - battler.GetPosition()).normalized;
        float angle = Vector2.SignedAngle(target, new Vector2(0, 1));
        elements[1].transform.Rotate(0, 0, -angle);
        elements[1].GenerateBeam(representationData.GetData(1), material, runtimeData.distance);
    }

    private void BuildRayProjectile(BattleAttack attack, Battler battler)
    {
        
        elements[1].transform.rotation = Quaternion.identity;
        elements[1].transform.SetParent(null);
        elements[1].transform.position = elements[0].transform.position;
        Vector2 target = (attack.target.GetPosition() - battler.GetPosition()).normalized;

        float angle = Vector2.SignedAngle(target, new Vector2(0, 1));

        elements[1].transform.Rotate(0, 0, -angle);
        elements[1].Generate(representationData.GetData(1), material);
    }

    private void BuildOrigin(BattleAttack attack, Battler battler)
    {
        elements[0].transform.rotation = Quaternion.identity;
        WeaponSystem chosenWeapon = attack.chosenWeapon;
        //parent origin to battler
        elements[0].transform.SetParent(battler.transform);
        //align?
        Vector2FS emitter = chosenWeapon.GetWeapon().GetEmitter();
        elements[0].transform.localPosition = new Vector2(emitter.x,emitter.y);
        Vector2 target =  (attack.target.GetPosition()- battler.GetPosition()).normalized;
        Vector2 local = Quaternion.Euler(0, 0, battler.GetData().GetMovePosition().facing) * target;
        float angle = Vector2.SignedAngle(local, new Vector2(0, 1));
        elements[0].transform.Rotate(0,0,-angle);
        //generate
        elements[0].Generate(representationData.GetData(0), material);
        elements[0].deactivateOnEnd = ((DirectWeapon)attack.chosenWeapon.GetWeapon()).GetWeaponVisuals().isBeam ? false : true;
    }

    private void BuildImpact(BattleAttack attack, Battler battler)
    {
        elements[2].transform.rotation = Quaternion.identity;
        elements[2].transform.SetParent(null);
        Vector2 target = (attack.target.GetPosition() - battler.GetPosition()).normalized;
        float angle = Vector2.SignedAngle(target, new Vector2(0, 1));
        elements[2].transform.Rotate(0, 0, -angle);
        elements[2].Generate(representationData.GetData(2), material);
        elements[2].deactivateOnEnd = ((DirectWeapon)attack.chosenWeapon.GetWeapon()).GetWeaponVisuals().isBeam ? false : true;
    }
}
