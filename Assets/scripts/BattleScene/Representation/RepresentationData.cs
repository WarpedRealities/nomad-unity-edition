﻿using UnityEngine;
using System.Collections;

public class RepDataEntry
{
    float x, y;
    int length;
    float width, height;
    bool repeat;

    public RepDataEntry(float x, float y, int length, float width, float height, bool repeat)
    {
        this.x = x;
        this.y = y;
        this.length = length;
        this.width = width;
        this.height = height;
        this.repeat = repeat;
    }

    public bool IsRepeat()
    {
        return repeat;
    }

    public float GetX()
    {
        return x;
    }

    public float GetY()
    {
        return y;
    }

    public int GetLength()
    {
        return length;
    }

    public float GetWidth()
    {
        return width;
    }

    public float GetHeight()
    {
        return height;
    }
}

public class RepresentationData
{
    RepDataEntry[] entries;
    readonly static string PREFIX = "representation/";

    private RepresentationData(RepDataEntry[] entries)
    {
        this.entries = entries;
    }

    public RepDataEntry GetData(int index)
    {
        return entries[index];
    }

    public static RepresentationData BuildData(string filename)
    {
        string file = FileTools.GetFileRaw((PREFIX+filename+".txt"));
        file = file.Replace('\n', ' ');
        string[] split = file.Split(',');
        int length = int.Parse(split[0]);
       
        RepDataEntry[] repDataEntries = new RepDataEntry[length];
        for (int i = 0; i < length; i++)
        {
            int offset = (i * 6)+1;
            repDataEntries[i] = new RepDataEntry(
                (float)int.Parse(split[offset + 0]),
                (float)int.Parse(split[offset + 1]),
                int.Parse(split[offset+2]),
                (float)int.Parse(split[offset+3]),
                (float)int.Parse(split[offset+4]),
                int.Parse(split[offset+5]) == 1
                );
        }
        return new RepresentationData(repDataEntries);
    }
}
