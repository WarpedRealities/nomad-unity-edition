using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepresentationManager : MonoBehaviour
{
    public GameObject prefabRepresentation;
    public GameObject prefabStriker;
    public GameObject prefabText;
    BattleSim battleSim;
    List<BattleText> battleTexts = new List<BattleText>();
    List<Representation> representations=new List<Representation>();
    // Start is called before the first frame update
    void Start()
    {
        battleSim = GetComponent<BattleSimulation>();    
    }

    internal Representation ProvideRepresentation()
    {
        for (int i = 0; i < representations.Count; i++)
        {
            if (!representations[i].IsAlive())
            {
                return representations[i];
            }
        }
        Representation representation = Instantiate(prefabRepresentation, null).GetComponent<Representation>();
        representations.Add(representation);
        representation.representationManager = this;
        return representation;
    }

    internal BattleText ProvideText()
    {
        for (int i = 0; i < battleTexts.Count; i++)
        {
            if (!battleTexts[i].IsAlive())
            {
                return battleTexts[i];
            }
        }
        BattleText battleText = Instantiate(prefabText, null).GetComponent<BattleText>();
        battleTexts.Add(battleText);
        return battleText;
    }

    internal Striker SpawnStriker()
    {
        GameObject gameobject = Instantiate(prefabStriker, null);
        Striker striker = gameobject.GetComponent<Striker>();
        striker.representationManager = this;
        battleSim.AddSpawnable(striker);
        return striker;
    }

    internal void RemoveSpawnable(Spawnable spawnable)
    {
        battleSim.RemoveSpawnable(spawnable);
    }
}
