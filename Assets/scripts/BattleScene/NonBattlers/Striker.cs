﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class StrikerData
{
    public int health, lifespan, defence, attack;
    public float speed, facing, life;
    public int state, team;
    public RepresentationData representationData;
    public string ammoCode;
    public WeaponEffect[] weaponEffects;
    public BattleAttack battleAttack;
}

public class Striker : MonoBehaviour, Spawnable
{
    StrikerData data;
    public RepresentationAnimation representationAnimation;
    public RepresentationManager representationManager;
    public int ApplyDamage(DT damageType, int damage)
    {
        data.health -= damage;
        if (data.health <= 0)
        {
            RemoveStriker();
        }
        return damage;
    }

    private void RemoveStriker()
    {
        representationManager.RemoveSpawnable(this);
        BattleEffectResolver.HandleEffects(data.weaponEffects, data.battleAttack, 0, representationManager, true);
        representationAnimation.Generate(data.representationData.GetData(1), null);
        data.state = 2;
    }

    public bool GetAlive()
    {
        return data.health > 0;
    }

    public int GetDefence()
    {
        return data.defence;
    }

    public Vector2 GetEstimatedPosition(float time)
    {
        float offset = time * data.speed;
        Vector2 between = ((Vector2)transform.position-data.battleAttack.target.GetPosition()).normalized;
        return (Vector2)transform.position + (between * offset);    
    }

    public Vector2 GetPosition()
    {
        return transform.position;
    }

    public float GetSize()
    {
        return 1;
    }

    public int GetTeam()
    {
        return data.team;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (data.state == 2)
        {
            if (!representationAnimation.IsActive())
            {
                representationManager.RemoveSpawnable(this);
                Destroy(gameObject);
            }
        }
    }

    public void UpdateLogic(float dt)
    {
        switch (data.state)
        {
            case 0:
                RunLaunch(dt);
                break;
            case 1:
                RunSeek(dt);
                break;

            case 3:
                RunBlind(dt);
                break;
        }
    }


    private void RunBlind(float dt)
    {
        Move(dt, data.speed);
    }

    private void RunSeek(float dt)
    {
        float facing = data.facing / 57.3F;
        Vector3 offset = new Vector3(1 * -Mathf.Sin(facing), 1 * Mathf.Cos(facing)).normalized;
        float angle = Vector2.SignedAngle((data.battleAttack.target.GetPosition()- (Vector2)transform.position).normalized, offset);
        if (angle>5 || angle < -5)
        {
            data.facing += angle > 0 ? dt * -180 : dt * 180;
            if (data.facing > 360)
            {
                data.facing = data.facing - 360;
            }
            if (data.facing < 0)
            {
                data.facing = 360 + data.facing;
            }
            transform.rotation = Quaternion.identity;
            transform.Rotate(new Vector3(0,
            0, data.facing));
        }
        data.life -= dt;
        Move(dt, data.speed);
        float d = Vector2.Distance(transform.position, data.battleAttack.target.GetPosition());
        if (d < data.battleAttack.target.GetSize()*0.5F)
        {
            RunAttack();
        }

    }

    private void RunAttack()
    {
        if (Roll(data.battleAttack))
        {
            RepresentationManager representationManager = GameObject.Find("BattleManager").GetComponent<RepresentationManager>();
            BattleEffectResolver.HandleEffects(data.weaponEffects, data.battleAttack, 0, representationManager, true);
            representationAnimation.Generate(data.representationData.GetData(1), null);
            data.state = 2;
        }
        else
        {
            BattleText battleText = representationManager.ProvideText();
            battleText.Initialize(data.battleAttack.target.GetPosition(), "miss", Color.white);
            data.state = 3;
        }
    }
    private bool Roll(BattleAttack battleAttack)
    {
        int bonus = data.attack;
        int penalty = battleAttack.target.GetDefence();
        int roll = DiceRoller.GetInstance().RollDice(20) + bonus;
        return roll >= 4 + penalty;
    }
    public void StartSimulation()
    {
        switch (data.state)
        {
            case 0:
                data.state = 1;
                break;

        }

    }

    public void RunLaunch(float dt)
    {
        Move(dt, 1);
    }

    public void Move(float dt, float speed)
    {
        float facing = data.facing/57.3F;
        Vector3 offset = new Vector3(speed * -Mathf.Sin(facing), speed * Mathf.Cos(facing));
        transform.position = transform.position + (offset * dt);
    }

    internal void Initialize(BattleAttack battleAttack, Battler battler)
    {
        transform.rotation = Quaternion.identity;
        data = new StrikerData();
        WeaponSystem chosenWeapon = battleAttack.chosenWeapon;
        StrikeWeapon strikeWeapon = (StrikeWeapon)chosenWeapon.GetWeapon();
        Material material = SpriteLibrary.getInstance().GetMaterial(
        (strikeWeapon).GetSpriteSheet(chosenWeapon.GetAmmoType()),
        "representation/");
        RepresentationData representationData = RepresentationDataLibrary.GetInstance().GetData(
            (strikeWeapon).GetSpriteSheet(chosenWeapon.GetAmmoType()));
        data.battleAttack = battleAttack;
        data.ammoCode = chosenWeapon.GetAmmoType().GetCode();
        data.state = 0;
        data.team = battler.GetTeam();
        data.defence = strikeWeapon.GetDefence(chosenWeapon.GetAmmoType());
        data.attack = strikeWeapon.GetAttack(chosenWeapon.GetAmmoType());
        data.lifespan = strikeWeapon.GetLifespan(chosenWeapon.GetAmmoType());
        data.life = data.lifespan;
        data.weaponEffects = strikeWeapon.GetWeaponEffects(chosenWeapon.GetAmmoType());
        data.speed = strikeWeapon.GetSpeed(chosenWeapon.GetAmmoType());
        data.representationData = representationData;
        representationAnimation.Generate(representationData.GetData(0), material);
        Vector2FS emitter = chosenWeapon.GetWeapon().GetEmitter();
        Vector2 p = Quaternion.Euler(
            0, 0, battler.GetData().GetMovePosition().facing) *
            new Vector2(emitter.x, emitter.y);
        transform.position = p+(Vector2)battler.transform.position;
        data.facing = (battler.GetData().GetMovePosition().facing * 57.3F) +
            (45 * battleAttack.chosenWeapon.GetFacing());
        transform.Rotate(new Vector3(0,
            0, data.facing));
    }

    public void FinishSimulation()
    {
       if (data.state == 3)
        {
            RemoveStriker();       
        }
    }
}
