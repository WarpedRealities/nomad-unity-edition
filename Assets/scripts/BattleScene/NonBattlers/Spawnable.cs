﻿using UnityEngine;
using System.Collections;

public interface Spawnable : Targetable
{

    void UpdateLogic(float dt);

    void StartSimulation();
    void FinishSimulation();
}
