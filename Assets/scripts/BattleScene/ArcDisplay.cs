using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcDisplay : MonoBehaviour
{
    public MeshRenderer meshRenderer;
    public MeshFilter meshFilter;

    private void Awake()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshFilter = gameObject.GetComponent<MeshFilter>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BuildDisplay(bool alternate, float length=0)
    {
        UnityEngine.Vector3[] vertices = new UnityEngine.Vector3[4];
        UnityEngine.Vector3[] normals = new UnityEngine.Vector3[4];
        UnityEngine.Vector2[] uv = new UnityEngine.Vector2[4];
        int[] indices = new int[6];

        float offsetUV = alternate ? 0.5F : 0.0F;
        uv[0] = new Vector2(offsetUV+0, length);
        uv[1] = new Vector2(offsetUV+ 0.5F, length);
        uv[2] = new Vector2(offsetUV+0.5F, 0);
        uv[3] = new Vector2(offsetUV+0, 0);

        vertices[0] = new Vector3(-0.25F, length);
        vertices[1] = new Vector3(0.25F, length);
        vertices[2] = new Vector3(0.25F, 0);
        vertices[3] = new Vector3(-0.25F, 0);
        indices[0] = 0;
        indices[1] = 0 + 1;
        indices[2] = 0 + 2;
        indices[3] = 0;
        indices[4] = 0 + 2;
        indices[5] = 0 + 3;

        Mesh mesh = new Mesh();
        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] = -UnityEngine.Vector3.forward;
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.normals = normals;
        mesh.triangles = indices;

        meshFilter.mesh = mesh;
    }
}
