using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    public GameObject prefabBattler;
    Encounter encounter;
    Battler playerBattler;
    Battler[] enemyBattlers;
    public BattlerExpectation expectation;
    BattleSimulation simulation;
    BattleControl battleControl;
    BattleCamera battleCamera;
    // Start is called before the first frame update
    void Start()
    {
        battleControl = GetComponent<BattleControl>();
        battleCamera = GameObject.Find("Main Camera").GetComponent<BattleCamera>();
        simulation = GetComponent<BattleSimulation>();
        encounter = GameObject.Find("Encounter").GetComponent<Encounter>();
        SetupBattlers();
        battleCamera.SetupCamera(playerBattler);
        battleControl.Setup(playerBattler, enemyBattlers);
        GameObject.Find("BackGroundManager").GetComponent<BackgroundManager>().SetPlayerShip(playerBattler);
        GameObject.Find("BattleHud").GetComponent<BattleHud>().SetPlayer(playerBattler, encounter.GetData().GetPlayerShip());
    }

    private void SetupBattlers()
    {
        Battler[] battlers = new Battler[encounter.GetData().GetHostileCount() + 1];
        playerBattler = Instantiate(prefabBattler).GetComponent<Battler>();
        playerBattler.Setup(encounter.GetData().GetPlayerShip());
        playerBattler.SetMovePosition(new BattlerMovePosition(new Vector2(0, 0), 0));
        expectation.Setup(playerBattler);
        expectation.UpdateExpectation();
        battlers[0] = playerBattler;
        //setup enemy battlers
        enemyBattlers = new Battler[encounter.GetData().GetHostileCount()];
        for (int i = 0; i < encounter.GetData().GetHostileCount(); i++)
        {
            enemyBattlers[i]= Instantiate(prefabBattler).GetComponent<Battler>();
            enemyBattlers[i].Setup(encounter.GetData().GetHostile(i));
            float r = DiceRoller.GetInstance().RollDice(12)*0.5F;
            int d = DiceRoller.GetInstance().RollDice(6);
            Vector2 p = new Vector2(0 - d * Mathf.Sin(r), d * Mathf.Cos(r));
            p.y += 10;
            battlers[i + 1] = enemyBattlers[i];
            enemyBattlers[i].SetMovePosition(new BattlerMovePosition(p, 180));
        }
        simulation.SetupBattlers(battlers);
    }

    // Update is called once per frame
    void Update()
    {
        if (!simulation.IsRunning() && battleControl.GetActive())
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                RunSimulation();
            }
        }

    }

    private void RunSimulation()
    {
        //remember later we need to load ships up with their attack choices here
        battleControl.RunAI();
        playerBattler.GetData().SetAttacks(battleControl.GetWeaponData().attacks);
        simulation.RunSimulation();
    }

}
