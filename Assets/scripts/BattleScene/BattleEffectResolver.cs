﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BattleEffectResolver
{
    internal static void HandleEffects(WeaponEffect[] weaponEffect, BattleAttack attack, float distance, RepresentationManager representationManager, bool noMods=false)
    {
        int value = 0;
        for (int i = 0; i < weaponEffect.Length; i++)
        {
            if (weaponEffect[i] is DamageEffect)
            {
                value+=HandleDamage((DamageEffect)weaponEffect[i], attack, distance,noMods);
            }
        }
        BattleText battleText = representationManager.ProvideText();
        battleText.Initialize(attack.target.GetPosition(), value.ToString(), Color.red);
    }

    private static int GetAmmoModifier(int value, String id, String ammoCode, Dictionary<string, AmmoModifier> modifiers)
    {
        if (modifiers.ContainsKey(ammoCode))
        {
            if (modifiers[ammoCode].modifierChanges.ContainsKey(id))
            {
                return int.Parse(modifiers[ammoCode].modifierChanges[id]);
            }
        }
        return value;
    }

    private static int HandleDamage(DamageEffect damageEffect, BattleAttack attack, float distance, bool noMods)
    {
        
        int min = damageEffect.GetMinDamage();
        int max = damageEffect.GetMaxDamage();
        if (!noMods && attack.chosenWeapon.GetAmmoType() != null)
        {
            max = GetAmmoModifier(max, "DMG", 
                attack.chosenWeapon.GetAmmoType().GetCode(), 
                attack.chosenWeapon.GetWeapon().GetAmmoModifiers());
        }
        int roll = max == min ? max : 
            DiceRoller.GetInstance().RollDice(max - 
            min)+min;
        float decay = damageEffect.GetRangeDecay() * distance;
        roll = roll - (int)decay;
        roll = roll < 1 ? 1 : roll;
        return attack.target.ApplyDamage(damageEffect.GetDamageType(), roll);
    }
}
