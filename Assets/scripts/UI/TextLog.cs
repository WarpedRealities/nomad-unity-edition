﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLog : MonoBehaviour
{

    private ScrollRect scrollRect;
    private RectTransform rectTransform;
    //private static int staticMaxEntries;
    private int updatePosition;
    public TextContent textContent;
    public static Queue<string> stringQueue =new Queue<string>();
    private int compareCounter = 0;
    public Button toBottomButton;
    //[SerializeField]
    //int maxEntries;

    // Start is called before the first frame update
    void Start()
    {
        updatePosition = 1;
        scrollRect = transform.parent.parent.GetComponent<ScrollRect>();
        rectTransform = transform.parent.parent.GetComponent<RectTransform>();
    }

    private void Update()
    {


        if (stringQueue.Count > 0)
        {

            this.textContent.AddText(stringQueue.Dequeue());
            updatePosition = 2;

        }
        if (toBottomButton != null)
        {
            toBottomButton.gameObject.SetActive(scrollRect.verticalNormalizedPosition != 0 ? true : false);
        }
    }

    public void ToBottomClick()
    {
        scrollRect.verticalNormalizedPosition = 0;
    }

    public void LateUpdate()
    {
        if (updatePosition> 0 )
        {
            scrollRect.verticalNormalizedPosition = Offset();
            updatePosition--;
        }
    }

    public float Offset()
    {
        float contentHeight = textContent.GetHeight();
        float portionHeight = textContent.GetOffset();
        float windowHeight = rectTransform.rect.height;
        if (portionHeight > windowHeight)
        {
            float off = portionHeight - windowHeight;
            return off / contentHeight;
        }
        return 0;
    }

    public static void Log(string str)
    {

        str = str.Replace("\n", " ");
        str = str.Replace("\r", "");
        str = str.Replace("\t", "");
        str = str.Replace("LBREAK", "\n");
        stringQueue.Enqueue(str);
    }


}
