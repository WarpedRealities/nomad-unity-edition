using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PulsingText : MonoBehaviour
{
    public TextMeshProUGUI text;
    public Color color;
    private float clock;
    // Start is called before the first frame update
    void Start()
    {
        clock = 0;
    }

    // Update is called once per frame
    void Update()
    {
        clock += Time.deltaTime/2;
        if (clock > 1)
        {
            clock = 0;
        }
        float v = Mathf.Abs(0.5F - clock);
        color.b = v;
        color.r = v;
        color.g = 0.5F + v;
        text.color = color;
    }
}
