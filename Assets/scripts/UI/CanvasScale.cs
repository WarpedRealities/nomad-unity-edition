﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasScale : MonoBehaviour
{
    public Canvas canvas1;
    public int canvas1MinSize;
    public Canvas canvas2;
    public int canvas2MinSize;

    public bool horizontal;

    private int thisHeight = 0;
    private int thisWidth = 0;
    private CanvasScale parentCanvas;
    private bool hasParent = false;

    // Start is called before the first frame update
    void Start()
    {
        parentCanvas = transform.parent.gameObject.GetComponent<CanvasScale>();
        if (parentCanvas != null)//there is a parent canvas scale
        {
            hasParent = true;
        }
        else//there is no parent canvas, so this canvas scale is the top of the chain and should take its size directly from the 
        {
            hasParent = false;

            

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
