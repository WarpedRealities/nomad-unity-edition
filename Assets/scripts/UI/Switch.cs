using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UIElements;
public delegate void DelegateCall();

public class Switch : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, ISubmitHandler
{
    public UnityEngine.UI.Image image;
    public Sprite[] sprites;
    private float position = 0;
    private bool held;
    public bool reverse;
    private float mPos = 0;
    public DelegateCall activate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (held)
        {
            if (this.reverse)
            {
                position -= (Input.mousePosition.y - mPos) / 128;
            }
            else
            {
                position += (Input.mousePosition.y - mPos) / 128;
            }

                mPos = Input.mousePosition.y;

            UpdateImage();
            if (position >= 1)
            {
                Trigger();
            }
        }
        else
        {
            if (position > 0)
            {
                position -= Time.deltaTime;
                UpdateImage();
            }

        }
        if (held && !Input.GetMouseButton(0))
        {
            held = false;
        }
    }

    private void Trigger()
    {
        activate();
    }

    private void UpdateImage()
    {
        int v = (int)(position*14);

        v = v < 0 ? 0 : v;
        v = v > 14 ? 14 : v;
        image.sprite = sprites[v];
    }

    public void OnPointerClick(PointerEventData eventData)
    {

    }

    public void OnSubmit(BaseEventData eventData)
    {
        held = true;
        position += Time.deltaTime;
        UpdateImage();
        if (position >= 1)
        {
            Trigger();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {

        if (!held)
        {
            held = true;
            mPos = Input.mousePosition.y;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }
}
