using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NuListMono : MonoBehaviour
{
    public NuListEntry entry;
    public bool highlight;
    public Image imageComponent;
    public TextMeshProUGUI textComponent;
    public Color[] colours; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetHighlight(bool value, bool disabled)
    {
        highlight = value;
        textComponent.color = value ? colours[1]: (disabled ? colours[2]: colours[0]);
        imageComponent.enabled = value ? true : false;
    }
}
