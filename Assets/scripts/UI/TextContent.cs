using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextContent : MonoBehaviour
{
    private RectTransform rect;
    public GameObject prefabText;

    public static string[] stringBank = new string[32];
    public TextMeshProUGUI[] textStrings = new TextMeshProUGUI[32];
    private int takenSlots = 0;
    private int highlight = -1;
    private float height = 0;
    private int reposition = 0;
    private float offset = 0;
    // Start is called before the first frame update
    void Start()
    {
        rect = GetComponent<RectTransform>();
        HighlightLast();
        Rebuild();
        reposition = 2;

    }

    private void HighlightLast()
    {
        for (int j= stringBank.Length-1; j>=0; j--)
        {
            if (stringBank[j] != null)
            {
                highlight = j;
                break;
            }
        }
    }

    public int GetHighlight()
    {
        return this.highlight;
    }

    public float GetOffset()
    {
        return offset;
    }

    public float GetHeight()
    {
        return height;
    }

    private void Rebuild()
    {
        for (int i =0; i < stringBank.Length ; i++)
        {
            if (stringBank[i] != null)
            {
                GameObject gameObject = Instantiate(prefabText, transform);
                textStrings[takenSlots] = gameObject.GetComponent<TextMeshProUGUI>();
                textStrings[takenSlots].text = stringBank[i];
                takenSlots++;
            }
        }
        reposition = 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (reposition > 0)
        {
            PositionTexts();
        }
    }

    private void LateUpdate()
    {

    }

    public void AddText(string text)
    {

        if (takenSlots < textStrings.Length)
        {

            GameObject gameObject = Instantiate(prefabText,transform);
            textStrings[takenSlots] = gameObject.GetComponent<TextMeshProUGUI>();
            textStrings[takenSlots].text = text;
            stringBank[takenSlots] = text;
            highlight = takenSlots;
            takenSlots++;
        }
        else
        {
            TextMeshProUGUI textMeshProUGUI = textStrings[0];
            TextMeshProUGUI[] temp = new TextMeshProUGUI[textStrings.Length];
            string[] tempStr = new string[stringBank.Length];
            Array.Copy(textStrings, 0, temp, 0, textStrings.Length);
            Array.Copy(temp, 1, textStrings, 0, textStrings.Length - 1);
            Array.Copy(stringBank, 0, tempStr, 0, textStrings.Length);
            Array.Copy(tempStr, 1, stringBank, 0, textStrings.Length - 1);

            textStrings[textStrings.Length - 1] = textMeshProUGUI;
            textStrings[textStrings.Length - 1].text = text;
            stringBank[textStrings.Length - 1] = text;
            highlight = textStrings.Length - 1;
        }
        reposition = 2;
    }

    public void PositionTexts()
    {
        height = 0;
        for (int i = 0; i < textStrings.Length; i++)
        {
            if (textStrings[i] != null)
            {
                textStrings[i].transform.localPosition = new Vector3(0, -height);

                height += textStrings[i].preferredHeight+4;
                textStrings[i].color = i == highlight ? Color.yellow : Color.white;
                if (i == highlight)
                {
                    offset = (textStrings[i].preferredHeight +4) * 1.3F;
                }
            }
        }
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height+8);
        reposition--;
    }
}
