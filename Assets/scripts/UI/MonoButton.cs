using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MonoButton : MonoBehaviour
{
    public Image image;
    public TextMeshProUGUI text;
    public Sprite[] backgrounds;
    public Color[] colours;
    private bool highlight;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetHighlight(bool value)
    {
        image.sprite = value ? backgrounds[1] : backgrounds[0];
        text.color = value ? colours[1] : colours[0];
    }
}
