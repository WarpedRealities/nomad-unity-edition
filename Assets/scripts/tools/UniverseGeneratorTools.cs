﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;

public class UniverseGeneratorTools
{
   
    public UniverseGeneratorTools()
    {

    }

    public List<StarSystem> buildSystems()
    {
        List<StarSystem> systems = new List<StarSystem>();
        XmlDocument document = FileTools.GetXmlDocument("universe");
      
        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;

        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
               if ("system".Equals(element.Name))
               {
                    Vector2Int p = new Vector2Int(Int32.Parse(element.GetAttribute("x")),
                        Int32.Parse(element.GetAttribute("y")));
                    String filename = element.GetAttribute("name");
                    Vector2 v = new Vector2(float.Parse(element.GetAttribute("xPos")),
                        float.Parse(element.GetAttribute("yPos")));
                    systems.Add(new StarSystem(p, v,filename));
               }
            }
        }
        return systems;
    }


}
