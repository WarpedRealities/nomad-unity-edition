﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubmenuTool
{


    public static void Build(Item item, Text text,  Button[] buttons, InventoryAction[] buttonAction, bool isEquipped)
    {
        int index = 0;
        string[] strings = new string[6];
        text.text = item.GetDef().GetName();
        if (item.GetDef().GetItemUse().Equals(ItemUse.USE))
        {
            strings[index] = "use";
            buttonAction[index] = InventoryAction.USE;
            index++;
            //buttons[0].enabled = true;
           // buttons[0].GetComponentInChildren<Text>().text = "use";
            //buttonAction[0] = InventoryAction.USE;
        }
        if (item.GetDef().GetItemUse().Equals(ItemUse.EQUIP))
        {
            if (isEquipped)
            {
                strings[index] = "unequip";
                buttonAction[index] = InventoryAction.UNEQUIP;
            }
            else
            {
                strings[index] = "equip";
                buttonAction[index] = InventoryAction.EQUIP;
            }
            index++;

            //check if the item is one you can reload
            if (item is EquipInstance)
            {
                EquipInstance equipInstance = (EquipInstance)item;
                if (equipInstance.GetResourceStore() != null && equipInstance.GetResourceStore().GetDef().GetCompatabilities().Count > 0)
                {
                    strings[index] = "reload";
                    buttonAction[index] = InventoryAction.RELOAD;
                    index++;
                    if (equipInstance.GetResourceStore().GetAmount() > 0 || equipInstance.GetResourceStore().GetDef().GetCapacity() == 0)
                    {
                        strings[index] = "unload";
                        buttonAction[index] = InventoryAction.UNLOAD;
                        index++;
                    }
                }
            }
           
        }
        if (item is AmmoInstance)
        {
            AmmoInstance ammoInstance = (AmmoInstance)item;
            if (ammoInstance.GetResource().GetDef().GetCompatabilities().Count > 0)
            {

                strings[index] = "reload";
                buttonAction[index] = InventoryAction.RELOAD;
                index++;
                if (ammoInstance.GetResource().GetAmount() > 0)
                {
                    strings[index] = "unload";
                    buttonAction[index] = InventoryAction.UNLOAD;
                    index++;
                }
            }
        }
        if (!(item.GetDef().GetItemUse()==ItemUse.EQUIP || item.GetDef().GetItemUse() == ItemUse.USE))
        {
         //   buttons[0].gameObject.SetActive(false);
        }

        strings[index] = "examine";
        buttonAction[index] = InventoryAction.EXAMINE;
        index++;
        //buttons[1].GetComponentInChildren<Text>().text = "examine";
        //buttonAction[1] = InventoryAction.EXAMINE;
        if (item.GetItemClass() == ItemClass.STACK || item.GetItemClass() == ItemClass.AMMOSTACK)
        {
            strings[index] = "take";
            buttonAction[index] = InventoryAction.TAKE;
            index++;
            //buttons[2].GetComponentInChildren<Text>().text = "take";
            //buttonAction[2] = InventoryAction.TAKE;
            //buttons[2].gameObject.SetActive(true);
        }
        else
        {
           // buttons[2].gameObject.SetActive(false);
        }
        
        strings[index] = "drop";
        buttonAction[index] = InventoryAction.DROP;
        index++;
        //  buttons[3].GetComponentInChildren<Text>().text = "drop";
        //  buttonAction[3] = InventoryAction.DROP;
        strings[index] = "cancel";
        buttonAction[index] = InventoryAction.CANCEL;
        index++;
        //buttons[4].GetComponentInChildren<Text>().text = "cancel";
        //buttonAction[4] = InventoryAction.CANCEL;

        for (int i = 0; i < 6; i++)
        {
       
            if (strings[i] == null)
            {
                buttons[i].gameObject.SetActive(false);
            } else
            {             
                buttons[i].GetComponentInChildren<Text>().text = strings[i];
                buttons[i].gameObject.SetActive(true);
            }
        }

    }
}
