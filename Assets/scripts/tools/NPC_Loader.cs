﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;

public class NPC_Loader
{
    private string sprite, name, description;
    private bool peaceBond = false;
    private Faction faction;
    private TileMovement movement= TileMovement.WALK;
    private NPC_RPG actorRPG;
    private NPC_AI aI;
    private RespawnHandler respawnHandler;
    private VolatilityHandler volatilityHandler;

    public void LoadNPC(XmlElement root, string filename)
    {
        respawnHandler = new RespawnHandler();
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];

                if ("name".Equals(element.Name))
                {
                    this.name = element.InnerText;
                }
                if ("movement".Equals(element.Name))
                {
                    movement = EnumTools.StrToMovement(element.GetAttribute("value"));

                }
                if ("sprite".Equals(element.Name))
                {
                    sprite = element.GetAttribute("value");
                }

                if ("description".Equals(element.Name))
                {
                    description = element.InnerText;
                }

                if ("rpg".Equals(element.Name))
                {

                    NPC_StatBlock statblock= StatBlockLibrary.GetInstance().getStatBlock(filename);
                    if (statblock == null)
                    {
                        statblock = new NPC_StatBlock(element);
                        StatBlockLibrary.GetInstance().addStatBlock(filename, statblock);
                    }
                    actorRPG = new NPC_RPG(filename,statblock);
                    statblock.setSprite(sprite);
                }
                if ("controller".Equals(element.Name))
                {
                    aI = new NPC_AI(element.GetAttribute("value"));
                }
                if ("conversations".Equals(element.Name))
                {
                    string[] conversations = new string[6];
                    for (int j = 0; j < element.ChildNodes.Count; j++)
                    {
                        if (element.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement xml = (XmlElement)element.ChildNodes[j];
                            conversations[ConversationFromName(xml.Name)] = xml.GetAttribute("value");
                        }
                    }

                    actorRPG.GetStatBlock().SetConversationFiles(conversations);
                }
                if ("peaceBond".Equals(element.Name))
                {
                    peaceBond = true;
                }
                if ("volatility".Equals(element.Name) || "temporary".Equals(element.Name))
                {
                    volatilityHandler = new VolatilityHandler(int.Parse(element.GetAttribute("lifespan")));
                }
                if ("faction".Equals(element.Name))
                {
                    faction = GlobalGameState.GetInstance().GetUniverse().getFactionLibrary().GetFaction(element.GetAttribute("value"));
                }
                if ("respawn".Equals(element.Name))
                {

                    int duration = int.Parse(element.GetAttribute("value"));
                    respawnHandler.SetDuration(duration);
                }
            }
        }
    }

    internal VolatilityHandler GetVolatilityHandler()
    {
        return volatilityHandler;
    }

    internal TileMovement GetMovement()
    {
        return movement;
    }

    internal bool GetPeace()
    {
        return peaceBond;
    }

    internal RespawnHandler GetRespawnHandler()
    {
        return respawnHandler;
    }

    public static int ConversationFromName(string name)
    {
        if ("defeat".Equals(name))
        {
            return (int)conversationType.DEFEAT;
        }
        if ("victory".Equals(name))
        {
            return (int)conversationType.VICTORY;
        }
        if ("seduced".Equals(name))
        {
            return (int)conversationType.SEDUCED;
        }
        if ("dominant".Equals(name))
        {
            return (int)conversationType.DOMINANT;
        }
        if ("talk".Equals(name))
        {
            return (int)conversationType.TALK;
        }
        if ("captive".Equals(name))
        {
            return (int)conversationType.CAPTIVE;
        }
        return 0;
    }

    public string GetDescription()
    {
        return description;
    }

    public string GetName()
    {
        return name;
    }

    public NPC_AI GetAI()
    {
        return new NPC_AI(this.aI.getScriptName());
    }

    public Faction GetFaction()
    {
        return faction;
    }

    public ActorRPG GetRPG()
    {
        return actorRPG;
    }
}
