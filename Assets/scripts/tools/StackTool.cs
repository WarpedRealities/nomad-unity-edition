﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackTool
{
    public static bool HandleStacking(Item item, List<Item> items, int count)
    {

        if (!item.IsStackable())
        {
            return false;
        }
        if (item.GetDef().GetItemType().Equals(ItemType.AMMO))
        {

            return HandleAmmoStacking(item, items, count);
        }
        
        //can we add this to an existing stack?
        for (int i = 0; i < items.Count; i++)
        {

            if (items[i].GetItemClass() == ItemClass.STACK && items[i].GetDef().GetCodeName().Equals(item.GetDef().GetCodeName()))
            {
                ItemStack itemStack = (ItemStack)items[i];
                itemStack.SetCount(itemStack.GetCount() + count);

                return true;
            }
        }
        //can we find an identical item that's also stackable and make a stack of 2?
        for (int i=0;i<items.Count; i++)
        {
 
            if (items[i].GetItemClass() != ItemClass.STACK &&
                items[i].IsStackable() && 
                items[i].GetDef().GetCodeName().Equals(item.GetDef().GetCodeName()))
            {
                //a match, take this item out, stack them together
                items.Remove(items[i]);
                items.Add(new ItemStack(item.GetDef(), 1+count));
            
                return true;
            }
        }
        if (count > 1)
        {
            ItemStack stack = new ItemStack(item.GetDef(), count);
            items.Add(stack);

            return true;
        }

        return false;
    }

    private static bool HandleAmmoStacking(Item item, List<Item> items, int count)
    {

        AmmoBase ammoBase = (AmmoBase)item;
        //can we add this to an existing stack?
        for (int i = 0; i < items.Count; i++)
        {

            if (items[i].GetItemClass() == ItemClass.AMMOSTACK && 
                items[i].GetDef().GetCodeName().Equals(item.GetDef().GetCodeName()) &&
                ((AmmoBase)items[i]).GetAmmoType().GetCodeName().Equals(ammoBase.GetAmmoType().GetCodeName()))
            {
                ItemAmmoStack itemStack = (ItemAmmoStack)items[i];
                itemStack.SetCount(itemStack.GetCount() + count);
            
                return true;
            }
        }
        //can we find an identical item that's also stackable and make a stack of 2?
        for (int i = 0; i < items.Count; i++)
        {
        
            if (items[i].GetItemClass() != ItemClass.AMMOSTACK && 
                items[i].GetDef().GetItemType() == ItemType.AMMO && 
                items[i].IsStackable() && 
                items[i].GetDef().GetCodeName().Equals(item.GetDef().GetCodeName()) &&
                ((AmmoBase)items[i]).GetAmmoType().GetCodeName().Equals(ammoBase.GetAmmoType().GetCodeName()))
            {
                //a match, take this item out, stack them together
                items.Remove(items[i]);
                items.Add(new ItemAmmoStack((ItemAmmo)item.GetDef(), 2, ammoBase.GetAmmoType()));
    
                return true;
            }
        }
        if (count > 1)
        {
            if (item.GetDef().GetItemType() == ItemType.AMMO)
            {
                ItemAmmoStack stack = new ItemAmmoStack((ItemAmmo)item.GetDef(), count,((AmmoBase)item).GetAmmoType());
                items.Add(stack);

            }
            else
            {
                ItemStack stack = new ItemStack(item.GetDef(), count);
                items.Add(stack);

            }

            return true;
        }
        if (item.GetItemClass() == ItemClass.AMMOSTACK)
        {
            ItemAmmoStack itemAmmoStack = (ItemAmmoStack)item;
            items.Add(itemAmmoStack.TakeItem());
            return true;
        }
        return false;
    }
}
