﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PointsOfInterest 
{
    List<Vector2Int> points;
    Vector2Int currentPoint;
    public PointsOfInterest()
    {
        points = new List<Vector2Int>();
    }

    public void AddPointOfInterest(Vector2Int p)
    {
        points.Add(p);
    }

    public List<Vector2Int> GetPoints()
    {
        return points;
    }

    public Vector2Int NextPoint()
    {
        currentPoint = points.Count > 1 ? points[DiceRoller.GetInstance().RollDice(points.Count)] : points[0];

        points.Remove(currentPoint);

        return currentPoint;
    }

    public Vector2Int GetPoint()
    {
        if (currentPoint == null)
        {
            NextPoint();
        }
        return currentPoint;
    }
}
