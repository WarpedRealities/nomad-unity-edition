﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigSpriteGenerator 
{
    public List<GameObject> GenerateSprites(ZoneContents zoneContents, GameObject prefab, GameObject hostObject)
    {
        List<GameObject> sprites = new List<GameObject>();
        for (int i = 0; i < zoneContents.getTiles().Length; i++)
        {
            for (int j = 0; j < zoneContents.getTiles()[0].Length; j++)
            {
                if (zoneContents.getTiles()[i][j]!=null && zoneContents.getTiles()[i][j].GetWidget() != null)
                {
                    Widget widget = zoneContents.getTiles()[i][j].GetWidget();

                    if (widget is WidgetSprite)
                    {
                        WidgetSprite widgetSprite = (WidgetSprite)widget;
                        GameObject sprite = GameObject.Instantiate(prefab);
                        sprite.transform.position = new Vector3(i, j);
                        sprite.GetComponent<SpriteRenderer>().sprite = SpriteRepo.GetInstance().GetSprite(widgetSprite.GetBigSprite());
                        sprites.Add(sprite);
                        Tile[] cornerTiles = new Tile[4];
                        cornerTiles[0] = zoneContents.getTiles()[i][j];
                        cornerTiles[1] = zoneContents.getTiles()[i+ widgetSprite.getWidth()][j];
                        cornerTiles[2] = zoneContents.getTiles()[i + widgetSprite.getWidth()][j+widgetSprite.getHeight()];
                        cornerTiles[3] = zoneContents.getTiles()[i][j + widgetSprite.getHeight()];
                        sprite.GetComponent<BigSprite>().cornerTiles=cornerTiles;
                        sprite.transform.parent = hostObject.transform;
                    }
                }
            }
        }

        return sprites;
    }
}
