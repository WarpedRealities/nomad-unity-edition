﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class ReformTarget
{
    public bool canReform;
    public string targetZone;
    public ResourceSystem energySource;
    public Vector2 position;

}

public class ReformationTool 
{
    ReformTarget reformTarget;
    ReformationHandler reformationHandler;

    public ReformationTool(ReformationHandler handler)
    {
        reformationHandler = handler;
        Run();
    }

    public bool CanReform()
    {
        if (reformationHandler.GetNoReform())
        {
            return false;
        }
        if (!reformTarget.canReform)
        {
            return false;
        }
        return true;
    }

    public void Run()
    {
        for (int i = 0; i < reformationHandler.GetEntries().Count; i++)
        {
           if (CheckReformZone(reformationHandler.GetEntries()[i]))
            {
                break;
            }
        }
        if (reformTarget == null)
        {
            reformTarget = new ReformTarget();
            reformTarget.canReform = false;
        }
    }

    private bool CheckReformZone(ReformerEntry entry)
    {
        Vector2 position=new Vector2(-1,-1);
        ResourceSystem resourceSystem = null;

        //can we find the zone?
        Zone zone = GlobalGameState.GetInstance().getCurrentEntity().GetZone(entry.zoneName);
        if (zone == null || !zone.IsGenerated())
        {
            return false;
        }
        //does the zone contain a reformer tube?
        ZoneContents contents = zone.GetContents();
        contents.PostLoad();
        for (int i = 0; i < contents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < contents.GetZoneParameters().GetHeight(); j++)
            {
                if (contents.GetTile(i, j) != null && contents.GetTile(i,j).GetWidget()!=null)
                {
                    if (contents.GetTile(i, j).GetWidget() is WidgetReformer){
                        WidgetReformer reformer = (WidgetReformer)contents.GetTile(i, j).GetWidget();
                        if (!reformer.IsSuppressed())
                        {
                            position = FindOpenSpace(i, j, zone);
                        }
                    }
                    if (contents.GetTile(i, j).GetWidget() is WidgetShipSystem)
                    {
                        WidgetShipSystem system = (WidgetShipSystem)contents.GetTile(i, j).GetWidget();
                        for (int k = 0; k < system.GetSystems().Length; k++)
                        {
                            if (system.GetSystems()[k].GetSystemType() == SHIPSYSTEMTYPE.RESOURCE)
                            {
                                ResourceSystem resource = (ResourceSystem)system.GetSystems()[k];
                                if ("RES_ENERGY".Equals(resource.GetContentId()) && resource.GetAmount()>=20){
                                    resourceSystem = resource;
                                }
                            }
                        }
                    }
                    if (contents.GetTile(i,j).GetWidget() is WidgetSlot)
                    {
                        WidgetSlot slot = (WidgetSlot)contents.GetTile(i, j).GetWidget();
                        if (slot.GetContainedWidget() != null)
                        {
                            if (slot.GetContainedWidget() is WidgetShipSystem)
                            {
                                WidgetShipSystem system = (WidgetShipSystem)slot.GetContainedWidget();
                                for (int k = 0; k < system.GetSystems().Length; k++)
                                {
                                    if (system.GetSystems()[k].GetSystemType() == SHIPSYSTEMTYPE.RESOURCE)
                                    {
                                        ResourceSystem resource = (ResourceSystem)system.GetSystems()[k];
                                        if ("RES_ENERGY".Equals(resource.GetContentId()) && resource.GetAmount() >= 20)
                                        {
                                            resourceSystem = resource;
                                        }
                                    }
                                }
                            }
                            if (slot.GetContainedWidget() is WidgetReformer)
                            {
                                WidgetReformer reformer = (WidgetReformer)slot.GetContainedWidget();
                                if (!reformer.IsSuppressed())
                                {
                                    position = FindOpenSpace(i, j, zone);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (position.x != -1)
        {
            reformTarget = new ReformTarget();
            reformTarget.energySource = resourceSystem;
            reformTarget.canReform = true;
            reformTarget.position = position;
            reformTarget.targetZone = zone.getName();
            return true;
        }
        return false;
    }

    internal void TriggerReformation()
    {
        //check energy source
        bool powered = this.reformTarget.energySource != null;
        Player player = GlobalGameState.GetInstance().GetPlayer();
        PlayerRPG rpg = (PlayerRPG)player.GetRPG();
        if (powered)
        {
            this.reformTarget.energySource.SetAmount(reformTarget.energySource.GetAmount() - 20);
            rpg.SetStat(ST.HEALTH, rpg.GetStatMax(ST.HEALTH));
            rpg.SetStat(ST.RESOLVE, rpg.GetStatMax(ST.RESOLVE));
            rpg.SetStat(ST.ACTION, rpg.GetStatMax(ST.ACTION));
            DropItems(GlobalGameState.GetInstance().getCurrentZone(), player, false);

        }
        else
        {
            rpg.SetStat(ST.HEALTH, rpg.GetStatMax(ST.HEALTH)/2);
            rpg.SetStat(ST.RESOLVE, rpg.GetStatMax(ST.RESOLVE)/2);
            rpg.SetStat(ST.ACTION, rpg.GetStatMax(ST.ACTION)/2);
            rpg.SetStat(ST.SATIATION, rpg.GetStat(ST.SATIATION) / 2);
            DropItems(GlobalGameState.GetInstance().getCurrentZone(), player, true);
            TextLog.Log("Insufficient power has led to a sub par reformation");
        }
        long companion = GlobalGameState.GetInstance().GetPlayer().GetCompanionUID();
        Actor comp = null;
        if (companion != -1)
        {
            comp = GlobalGameState.GetInstance().getCurrentZone().CompanionCheck(companion);
        }
        GlobalGameState.GetInstance().GetPlayer().SetPosition(new Vector2Int((int)reformTarget.position.x, (int)reformTarget.position.y));
        GlobalGameState.GetInstance().setCurrentZone(GlobalGameState.GetInstance().getCurrentEntity().GetZone(reformTarget.targetZone));
        if (comp != null)
        {
            GlobalGameState.GetInstance().getCurrentZone().CompanionAdd(player.GetPosition(), comp);
        }
        GlobalGameState.GetInstance().SetPlaying(true);
        SceneManager.LoadScene("ViewScene");
    }

    private void DropItems(Zone zone, Player player, bool bigDrop)
    {
        Vector2 p = FindOpenSpace(player.GetPosition().x, player.GetPosition().y, zone);
        List<Item> items = new List<Item>();
        if (bigDrop)
        {
            items = new List<Item>(player.GetInventory().GetItems());
            player.GetInventory().GetItems().Clear();
            player.GetInventory().CalculateWeight();
            if (items.Count > 0)
            {
                zone.GetContents().GetTile((int)p.x, (int)p.y).SetWidget(new WidgetItemPile(items));
            }
        }
        else
        {
            int numberToDrop = player.GetInventory().GetItemCount()>4 ? 4 + DiceRoller.GetInstance().RollDice(player.GetInventory().GetItemCount()/4) : 4;
            ItemList itemList = new ItemList();
            while(numberToDrop>0 && player.GetInventory().GetItems().Count > 0)
            {
                int r = DiceRoller.GetInstance().RollDice(player.GetInventory().GetItemCount());
                itemList.AddItem(player.GetInventory().RemoveItem(r));
                numberToDrop--;
            }
            if (itemList.GetItemCount() > 0)
            {
                zone.GetContents().GetTile((int)p.x, (int)p.y).SetWidget(new WidgetItemPile(itemList));
            }

        }
   
    }

    private Vector2 FindOpenSpace(int x, int y, Zone zone)
    {
        for (int i = 0; i < 8; i++)
        {
           Vector2Int p= GeometryTools.GetPosition(x, y, i);
               if (zone.GetContents().GetTile(p.x,p.y)!=null && 
                zone.GetContents().GetTile(p.x,p.y).GetWidget()==null &&
                zone.GetContents().GetTile(p.x,p.y).canWalk())
            {
                return p;
            }

        }
        return new Vector2(-1, -1);
    }
}
