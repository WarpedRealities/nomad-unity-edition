﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TextureManagementTools
{
   


    public static Texture2D loadTexture(string FilePath, bool graceful=false)
    {
        Texture2D Tex2D;
        byte[] FileData;
      
        if (File.Exists(FilePath))
        {

            FileData = File.ReadAllBytes(FilePath);
            Tex2D = new Texture2D(2, 2, TextureFormat.RGBA32,false);           // Create new "empty" texture
            if (Tex2D.LoadImage(FileData))
            {
                Tex2D.Apply();
                Tex2D.filterMode = FilterMode.Point;
                
                // Load the imagedata into the texture (size is set automatically)
                return Tex2D; // If data = readable -> return texture
            }               
        }
        else
        {
            if (!graceful)
            {
                Debug.Log("file does not exist " + FilePath);
            }

        }
        return null;                     // Return null if load failed
    }
}
