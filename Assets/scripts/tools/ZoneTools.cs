﻿using UnityEngine;
using System.Collections;
using System;

public class ZoneTools
{
    internal static void TriggerTrap(ViewInterface viewInterface, Player player, WidgetTrapSensor widgetTrapSensor)
    {
        TriggerResults results = widgetTrapSensor.GetResults();
        Vector2Int p = viewInterface.GetWidgetPosition(widgetTrapSensor);
        Widget[] widgets = null;
        if (results.cardinalDirections)
        {
            widgets = ZoneTools.CardinalCheck(p, results.range, results.trapType == TrapType.DOORS ? "WidgetDoor" : "WidgetTrap");
        }
        else
        {
            widgets = ZoneTools.AreaCheck(p, results.range, results.trapType == TrapType.DOORS ? "WidgetDoor" : "WidgetTrap");
        }
        for (int i = 0; i < 4; i++)
        {
            if (widgets != null)
            {
                ActivateWidget(widgets[i], viewInterface, player);
            }
        }
    }

    private static void ActivateWidget(Widget widget, ViewInterface viewInterface, Player player)
    {
        if (widget is WidgetDoor)
        {
            WidgetDoor door = (WidgetDoor)widget;
            if (door.GetKey() == "TRAP")
            {
                door.DestroyWidget(viewInterface, false);
            }
        }

    }

    private static Widget[] AreaCheck(Vector2Int p1, int range, string type)
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        Widget[] widgets = new Widget[4];
        int index = 0;
        for (int i = 0; i < range * 2; i++)
        {
            for (int j = 0; j < range * 2; j++)
            {
                if (index >= 4)
                {
                    break;
                }
                int x = i - range+p1.x;
                int y = j - range+p1.y;
                Tile t=zone.GetContents().GetTile(x, y);
                if (t != null)
                {
                    Widget w = GetWidget(t, type);
                    if (w != null)
                    {
                        widgets[index] = w;
                        index++;
                    }
                }
            }
        }
        return widgets;
    }

    private static Widget GetWidget(Tile t, string type)
    {
        if (t.GetWidget() == null)
        {
            return null;
        }
        if ("WidgetDoor".Equals(type) && t.GetWidget() is WidgetDoor)
        {
            Debug.Log("door found");
            return t.GetWidget();
        }
        if ("WidgetTrap".Equals(type) && t.GetWidget() is WidgetTrap)
        {
            return t.GetWidget();
        }
        return null;
    }

    private static Widget[] CardinalCheck(Vector2Int p1, int range, string type)
    {
        throw new NotImplementedException();
    }

    internal static WidgetDoor GetDoor(string lockKey)
    {
        Zone zone = GlobalGameState.GetInstance().getCurrentZone();
        for (int i = 0; i < zone.GetContents().GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zone.GetContents().GetZoneParameters().GetHeight(); j++)
            {
                Tile t = zone.GetContents().GetTile(i, j);
                if (t != null && t.GetWidget()!=null && t.GetWidget() is WidgetDoor)
                {
                    WidgetDoor widget = (WidgetDoor)t.GetWidget();
                    if (widget.GetKey().Equals(lockKey))
                    {
                        return widget;
                    }
                }
            }
        }
        return null;
    }
}
