
using MoonSharp.Interpreter;
using System;
using UnityEngine;

public class WorldScriptTool
{
    private Script script;
    private DynValue mainFunction;
    private object[] arguments;
    internal void Run(string filename)
    {
        script = new Script();
        String fileContents = FileTools.GetFileRaw("scripts/" + filename + ".lua");
        this.script.DoString(fileContents);
        mainFunction = this.script.Globals.Get("main");

        arguments = new object[1];
        arguments[0] = new WorldLua(GlobalGameState.GetInstance().getCurrentEntity());
        DynValue rValue = this.script.Call(mainFunction, arguments);
    }
} 