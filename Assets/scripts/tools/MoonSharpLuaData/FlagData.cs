﻿using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;

[MoonSharpUserData]
public class FlagData 
{
    private static bool isRegistered=false;
    private FlagField globalFlags;
    private FlagField factionFlags;
    private FlagField npcFlags;

    public FlagData(FlagField globalFlags, FlagField factionFlags, FlagField npcFlags)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<string>();
            UserData.RegisterType<int>();
            UserData.RegisterType<FlagData>();
            isRegistered = true;
        }
        this.globalFlags = globalFlags;
        this.factionFlags = factionFlags;
        this.npcFlags = npcFlags;
    }

    public int ReadGlobalFlag(string name)
    {
        return globalFlags.ReadFlag(name);
    }

    public void SetGlobalFlag(string name, int value)
    {
        globalFlags.SetFlag(name, value);
    }

    public void IncrementGlobalFlag(string name, int value)
    {
        
        globalFlags.IncrementFlag(name, value);
        Debug.Log("flag is now" + globalFlags.ReadFlag(name));
    }

}
