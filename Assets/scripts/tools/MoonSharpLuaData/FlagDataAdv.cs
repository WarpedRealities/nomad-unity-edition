﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MoonSharp.Interpreter;

[MoonSharpUserData]
public class FlagDataAdv
{
    private static bool isRegistered = false;
    private FlagField globalFlags;
    private FactionLibrary factions;
    public FlagDataAdv(FlagField globalFlags, FactionLibrary factionLibrary)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<string>();
            UserData.RegisterType<int>();
            UserData.RegisterType<FlagDataAdv>();
            isRegistered = true;
        }
        this.globalFlags = globalFlags;
        this.factions = factionLibrary;
    }

    public int ReadGlobalFlag(string flag)
    {
        return globalFlags.ReadFlag(flag);
    }

    public int ReadFactionFlag(string faction, string flag)
    {
        return factions.GetFaction(faction).GetFlagField().ReadFlag(flag);
    }

    public int ReadFactionRelationship(string faction, string comparison)
    {
        return factions.GetFaction(faction).getRelationship(comparison);
    }
}
