using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;

[MoonSharpUserData]
public class ToolsLua
{
    private static bool isRegistered = false;

    public ToolsLua()
    {
        if (!isRegistered)
        {
            UserData.RegisterType<string>();
            UserData.RegisterType<int>();
            UserData.RegisterType<ToolsLua>();
            isRegistered = true;
        }
    }

    public void LogText(string message)
    {
        TextLog.Log(message);
    }

}
