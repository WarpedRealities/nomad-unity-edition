﻿using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;

[MoonSharpUserData]
public class ViewLua
{
    private ViewInterface view;
    private static bool isRegistered = false;
    public ViewLua(ViewInterface view)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<string>();
            UserData.RegisterType<int>();
            UserData.RegisterType<ViewLua>();
            isRegistered = true;
        }
        this.view = view;
    }

    public void DrawText(string text)
    {
        view.DrawText(text);
    }

    public void RemoveNPC(LuaActor actor)
    {
        actor.GetZoneActor().Remove();
    }
    public void AddNPC(string filename, Vector2Int pos)
    {

        int x = pos.x;
        int y = pos.y;
        FindFreeTile(x, y, out x, out y);
        NPC npc = new NPC(ActorTools.loadNPC(filename), filename);
        npc.SetPosition(new Vector2Int((int)x, (int)y));
        Zone z = GlobalGameState.GetInstance().getCurrentZone();
        z.GetContents().GetActors().Add(npc);
        view.AddNPC(npc);
    }

    public void AddNPC(string filename, int x, int y)
    {
        FindFreeTile(x, y, out x, out y);
        NPC npc = new NPC(ActorTools.loadNPC(filename), filename);
        npc.SetPosition(new Vector2Int((int)x, (int)y));
        Zone z = GlobalGameState.GetInstance().getCurrentZone();
        z.GetContents().GetActors().Add(npc);
        view.AddNPC(npc);
    }

    private void FindFreeTile(int x0, int y0, out int x1, out int y1)
    {
        ZoneContents zoneContents = GlobalGameState.GetInstance().getCurrentZone().GetContents();
        if (zoneContents.GetTile(x0, y0)!=null &&
            zoneContents.GetTile(x0,y0).canEnter(TileMovement.WALK) && 
            zoneContents.GetTile(x0,y0).GetActorInTile() == null)
        {
            x1 = x0;
            y1 = y0;
            return;
        }
        for(int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(x0, y0, i);
            if (zoneContents.GetTile(p.x, p.y)!=null &&
                zoneContents.GetTile(p.x, p.y).canEnter(TileMovement.WALK) &&
                       zoneContents.GetTile(p.x, p.y).GetActorInTile() == null)
            {
                x1 = p.x;
                y1 = p.y;
                return;
            }
        }


        x1 = x0;
        y1 = y0;
    }
}
