using MoonSharp.Interpreter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[MoonSharpUserData]
public class WorldLua 
{
    private static bool isRegistered = false;
    private Entity entity;
    private Zone zone;
    public WorldLua(Entity entity)
    {
        this.entity = entity;
        if (!isRegistered)
        {
            UserData.RegisterType<string>();
            UserData.RegisterType<int>();
            UserData.RegisterType<WorldLua>();
            isRegistered = true;
        }
    }

    public void SetZone(string zoneName)
    {
        this.zone = entity.GetZone(zoneName);
    }
    public void ClearTile(int x, int y)
    {
        this.zone.GetContents().GetTile(x, y).SetWidget(null);
    }

    public void RestoreShip(string zone)
    {
        this.entity.GetLandingSite(this.entity.GetZone(zone)).GetSpaceship().SetState(ShipState.LANDED);
    }
}
