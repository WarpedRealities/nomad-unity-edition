﻿using MoonSharp.Interpreter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[MoonSharpUserData]
public class PlayerLua
{
    private static bool isRegistered = false;
    Player player;
    public PlayerLua(Player player)
    {
        if (!isRegistered)
        {
            UserData.RegisterType<string>();
            UserData.RegisterType<int>();
            UserData.RegisterType<PlayerLua>();
            isRegistered = true;
        }
        this.player = player;
    }

    public bool HasPart(string bodypart)
    {
        return player.GetAppearance().GetPart(bodypart) != null;
    }

    public int PartValue(string bodypart, string variable)
    {
        return player.GetAppearance().GetPart(bodypart).GetValue(variable);
    }
}

