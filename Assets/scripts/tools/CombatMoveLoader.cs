﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;

public class CombatMoveLoader 
{
   public static CombatMove BuildCombatMove(XmlElement root)
   {
        CombatMove move = new CombatMove();
        move.SetName(root.GetAttribute("name"));
        int repeat = 0;
        int.TryParse(root.GetAttribute("repeat"), out repeat);
        move.SetRepeat(repeat);
        move.SetPattern(EnumTools.StrToAttackPattern(root.GetAttribute("pattern")));
        move.SetType(EnumTools.strToMoveType(root.GetAttribute("type")));
        move.SetBasic("true".Equals(root.GetAttribute("basicAction")));
        int bonus = 0;
        int.TryParse(root.GetAttribute("bonus"), out bonus);
        move.SetBonus(bonus);
        int icon = 0;
        int.TryParse(root.GetAttribute("icon"), out icon);
        move.SetIcon(icon);
        float rangedecay = 0;
        float.TryParse(root.GetAttribute("rangeDecay"), out rangedecay);
        move.setRangeDecay(rangedecay);
        SK skill = EnumTools.strToSkill(root.GetAttribute("skill"));

        move.SetSkill(skill);
        int delayTime=0;
        move.SetThreatening("true".Equals(root.GetAttribute("threatening")));
        int.TryParse(root.GetAttribute("delay"), out delayTime);
        move.SetTimeCost(int.Parse(root.GetAttribute("timeCost")));
        move.SetDelay(delayTime);

        List<Effect> effects = new List<Effect>();

        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];

                if ("costs".Equals(element.Name))
                {
                    int actionCost = 0, ammoCost = 0, ammoPool = 0;

                    int.TryParse(element.GetAttribute("actionCost"), out actionCost);
                    int.TryParse(element.GetAttribute("ammoCost"), out ammoCost);
                    int.TryParse(element.GetAttribute("ammoPool"), out ammoPool);
                    move.setActionCost(actionCost);
                    move.setAmmoCost(ammoCost);
                    move.setAmmoPool(ammoPool);
                }
                if ("interrupt".Equals(element.Name))
                {
                    move.SetInterruptType(EnumTools.strToInterruptType(element.GetAttribute("type")));
                }
                if ("hit".Equals(element.Name) || "hitTexts".Equals(element.Name))
                {
                    move.SetHitTexts(BuildTexts(element));
                }
                if ("miss".Equals(element.Name) || "missTexts".Equals(element.Name))
                {
                    move.SetMissTexts(BuildTexts(element));
                }
                if ("effectDamage".Equals(element.Name))
                {
                    effects.Add(BuildEffectDamage(element, move.GetPattern()==AttackPattern.MELEE));
                }
                if ("effectStatus".Equals(element.Name))
                {
                    effects.Add(BuildEffectStatus(element));
                }
                if ("effectInterrupt".Equals(element.Name))
                {
                    effects.Add(BuildInterrupt(element));
                }
                if ("effectDismantle".Equals(element.Name))
                {
                    effects.Add(new EffectDismantle());
                }
                if ("effectSubmit".Equals(element.Name))
                {
                    effects.Add(new EffectSubmit("true".Equals(element.GetAttribute("resolve"))));
                }
                if ("effectSummon".Equals(element.Name))
                {
                    effects.Add(BuildEffectSummon(element));
                }
                if ("effectMove".Equals(element.Name))
                {
                    effects.Add(BuildEffectMove(element));
                }
                if ("effectItem".Equals(element.Name))
                {
                    effects.Add(BuildEffectItem(element));
                }
                if ("effectRecover".Equals(element.Name))
                {
                    effects.Add(CombatMoveLoader.BuildEffectRecover(element));
                }
                if ("effectAnalyse".Equals(element.Name))
                {
                    effects.Add(CombatMoveLoader.BuildEffectAnalyse(element));
                }
                if ("effectScan".Equals(element.Name))
                {
                    effects.Add(CombatMoveLoader.BuildEffectScan(element));
                }
            }
        }

        if (effects.Count > 0)
        {
            move.SetEffects(effects);
        }
        return move;
   }

    private static Effect BuildEffectScan(XmlElement element)
    {
        return new EffectScan("true".Equals(element.GetAttribute("ignoreWalls")),
            float.Parse(element.GetAttribute("range")),
            float.Parse(element.GetAttribute("width")),
            int.Parse(element.GetAttribute("bonus")));
    }

    private static Effect BuildEffectAnalyse(XmlElement root)
    {
        int[] hpThresholds = null;
        int[] rpThresholds = null;
        int[] defThresholds = null;
        int[] resThresholds = null;
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType.Equals(XmlNodeType.Element))
            {
                XmlElement element = (XmlElement)children[i];
                if ("healthScan".Equals(element.Name))
                {
                    hpThresholds = new int[3];
                    hpThresholds[0] = -1;
                    int.TryParse(element.GetAttribute("low"), out hpThresholds[0]);
                    hpThresholds[1] = -1;
                    int.TryParse(element.GetAttribute("mid"), out hpThresholds[1]);
                    hpThresholds[2] = -1;
                    int.TryParse(element.GetAttribute("hi"), out hpThresholds[2]);
                }
                if ("resolveScan".Equals(element.Name))
                {
                    rpThresholds = new int[3];
                    rpThresholds[0] = -1;
                    int.TryParse(element.GetAttribute("low"), out rpThresholds[0]);
                    rpThresholds[1] = -1;
                    int.TryParse(element.GetAttribute("mid"), out rpThresholds[1]);
                    rpThresholds[2] = -1;
                    int.TryParse(element.GetAttribute("hi"), out rpThresholds[2]);
                }
                if ("defenceScan".Equals(element.Name))
                {
                    defThresholds = new int[3];
                    defThresholds[0] = -1;
                    int.TryParse(element.GetAttribute("low"), out defThresholds[0]);
                    defThresholds[1] = -1;
                    int.TryParse(element.GetAttribute("mid"), out defThresholds[1]);
                    defThresholds[2] = -1;
                    int.TryParse(element.GetAttribute("hi"), out defThresholds[2]);
                }
                if ("resistScan".Equals(element.Name))
                {
                    resThresholds = new int[3];
                    resThresholds[0] = -1;
                    int.TryParse(element.GetAttribute("low"), out resThresholds[0]);
                    resThresholds[1] = -1;
                    int.TryParse(element.GetAttribute("mid"), out resThresholds[1]);
                    resThresholds[2] = -1;
                    int.TryParse(element.GetAttribute("hi"), out resThresholds[2]);
                }
            }

        }
        return new EffectAnalyse(hpThresholds, rpThresholds, defThresholds, resThresholds);
    }

    private static Effect BuildEffectItem(XmlElement element)
    {
        string code = element.GetAttribute("item");
        bool onMiss = "true".Equals(element.GetAttribute("onMiss"));
        return new EffectItem(code, onMiss);
    }

    private static EffectMove BuildEffectMove(XmlElement root)
    {
        bool away = "true".Equals(root.GetAttribute("away"));
        bool origin = "true".Equals(root.GetAttribute("affectOrigin"));
        bool bypass = "true".Equals(root.GetAttribute("bypassActors"));
        DR resistValue = DR.NONE;
        AB modValue = AB.NONE;
        int fail = 0;
        int succeed = 0;
        int strength = 0;
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType.Equals(XmlNodeType.Element))
            {
                XmlElement element = (XmlElement)children[i];
                if ("resist".Equals(element.Name))
                {
                    resistValue = EnumTools.strToDefenceResist(element.GetAttribute("resist"));
                    strength = int.Parse(element.GetAttribute("strength"));
                }
                if ("movement".Equals(element.Name))
                {
                    fail = int.Parse(element.GetAttribute("fail"));
                    succeed = int.Parse(element.GetAttribute("succeed"));
                    if (element.GetAttribute("modifier").Length > 2)
                    {
                        modValue = EnumTools.StrToAbility(element.GetAttribute("modifier"));
                    }
                }
            }
        }

        return new EffectMove(origin, away, bypass, strength, fail, succeed, resistValue, modValue);
    }

    public static Effect BuildInterrupt(XmlElement element)
    {
        int strength = int.Parse(element.GetAttribute("strength"));
        InterruptType interrupt=EnumTools.strToInterruptType(element.GetAttribute("type"));
        DR resist=EnumTools.strToDefenceResist(element.GetAttribute("resist"));
        int threshold = int.Parse(element.GetAttribute("threshold"));
        int stun = int.Parse(element.GetAttribute("stun"));
        return new EffectInterrupt(resist, interrupt, strength, threshold, stun);
    }

    public static EffectRecover BuildEffectRecover(XmlElement e)
    {
        float modifier = float.Parse(e.GetAttribute("amount"));
        ST stat = EnumTools.strToStat(e.GetAttribute("stat"));
        string desc = e.InnerText;
        return new EffectRecover(stat, modifier, desc);
    }

    public static EffectDamage BuildEffectDamage(XmlElement element, bool melee)
    {
        int min, max;
        DR type;
        AB modifier;
        min = int.Parse(element.GetAttribute("min"));
        max = int.Parse(element.GetAttribute("max"));
        type = EnumTools.strToDefenceResist(element.GetAttribute("type"));
        modifier = EnumTools.StrToAbility(element.GetAttribute("modifier"));
        float multiplier = 1;
        float penetration = 1;
        float falloff = 0;
        float.TryParse(element.GetAttribute("modifierMultiplier"), out multiplier);
        float.TryParse(element.GetAttribute("penetration"), out penetration);
        float.TryParse(element.GetAttribute("falloff"), out falloff);
        return new EffectDamage(min, max, type, modifier, melee, multiplier, penetration, falloff);
    }
    internal static Effect BuildEffectPerk(XmlElement element)
    {
        float exp = float.Parse(element.GetAttribute("experience"));
        string perk = element.GetAttribute("perk");
        return new EffectPerk(perk, exp);
    }
    private static List<string> BuildTexts(XmlElement element)
    {
        List<string> list = new List<string>();
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                list.Add(e.InnerText);
            }
        }
        return list; 
    }

    internal static EffectStatus BuildEffectStatus(XmlElement root)
    {
        int strength = -1;
        DR damageResist = DR.NONE;
        int.TryParse(root.GetAttribute("strength"), out strength);
        if (strength != -1)
        {
            damageResist = EnumTools.strToDefenceResist(root.GetAttribute("resistance"));
        }
        string description = "";
        string resistDescription = "";
        StatusEffect statusEffect = null;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("buff".Equals(element.Name)|| "debuff".Equals(element.Name))
                {
                    statusEffect = StatusEffectLoader.buildModifier(element);
                }
                if ("bind".Equals(element.Name))
                {
                    statusEffect = StatusEffectLoader.buildBind(element);
                }
                if ("stealth".Equals(element.Name))
                {
                    statusEffect = new StatusStealth(int.Parse(element.GetAttribute("uid")), int.Parse(element.GetAttribute("strength")));
                }
                if ("damageOverTime".Equals(element.Name) || "regeneration".Equals(element.Name))
                {
                    statusEffect = StatusEffectLoader.buildDOT(element);
                }
                if ("statusImmune".Equals(element.Name))
                {
                    statusEffect = StatusEffectLoader.buildImmunity(element);
                }
                
                if ("description".Equals(element.Name))
                {
                    description = element.InnerText;
                }
                if ("resistDescription".Equals(element.Name))
                {
                    resistDescription = element.InnerText;
                }
            }
        }
        return new EffectStatus(statusEffect, description, resistDescription, strength, damageResist);
    }

    internal static Effect BuildEffectSummon(XmlElement root)
    {
        string file = root.GetAttribute("file");
        string desc = "";
        int min = int.Parse(root.GetAttribute("minDistance"));
        int max = int.Parse(root.GetAttribute("maxDistance"));
        int direction = 0;
        bool hidden = false, originData = false; 
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("hiddenTilesOnly".Equals(element.Name))
                {
                    hidden = true;
                }
                if ("addOriginData".Equals(element.Name))
                {
                    originData = true;
                }
                if ("description".Equals(element.Name))
                {
                    desc = element.InnerText;
                }
                if ("directions".Equals(element.Name))
                {
                    direction += element.InnerText.Contains("NORTH") ? 1 : 0;
                    direction += element.InnerText.Contains("EAST") ? 2 : 0;
                    direction += element.InnerText.Contains("SOUTH") ? 4 : 0;
                    direction += element.InnerText.Contains("WEST") ? 8 : 0;
                }
            }
        }
        return new EffectSummon(file, desc, min, max, hidden, originData, direction);
    }

    internal static StatusEffect BuildStatus(XmlElement xmlElement)
    {
        if ("stealth".Equals(xmlElement.Name))
        {
            return new StatusStealth(int.Parse(xmlElement.GetAttribute("uid")), int.Parse(xmlElement.GetAttribute("strength")));
        }
        return null;
    }

    internal static Effect BuildEffectStatusRemove(XmlElement root)
    {
        int[] uids = null;
        string description = "";
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            XmlElement element = (XmlElement)root.ChildNodes[i];
            if ("description".Equals(element.Name))
            {
                description = element.InnerText;
            }
            if ("remove".Equals(element.Name))
            {
                uids = new int[int.Parse(element.GetAttribute("count"))];
                int index = 0;
                for (int j = 0; j < element.ChildNodes.Count; j++)
                {
                    XmlElement e = (XmlElement)element.ChildNodes[j];
                    uids[index] = int.Parse(e.GetAttribute("value"));
                    index++;
                }
            }
        }
        return new EffectStatusRemove(uids, description);
    }

    internal static Effect BuildEffectKarma(XmlElement e)
    {
        return new EffectKarma(int.Parse(e.GetAttribute("value")), e.InnerText);
    }
}
