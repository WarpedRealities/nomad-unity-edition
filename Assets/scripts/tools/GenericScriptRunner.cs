﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenericScriptRunner
{
    static GenericScriptRunner instance;

    public static GenericScriptRunner GetInstance()
    {
        if (instance == null)
        {
            instance = new GenericScriptRunner();
        }
        return instance;
    }

    Dictionary<string, RunnableScript> scripts; 

    private GenericScriptRunner()
    {
        scripts = new Dictionary<string, RunnableScript>();
    }

    public RunnableScript GetScript(string scriptName)
    {
        if (!scripts.ContainsKey(scriptName))
        {
            scripts.Add(scriptName, new RunnableScript(scriptName));
        }
        return scripts[scriptName];
    }

    public void RunScript(string scriptName, ViewInterface view)
    {
        FlagField globalFlags = GlobalGameState.GetInstance().GetPlayer().GetFlagField();
        FlagData flagData = new FlagData(globalFlags, null,null);
        ViewLua viewLua = new ViewLua(view);
        RunnableScript script = GetScript(scriptName);
        script.RunScript(flagData, viewLua);
    }
    public void RunScript(string scriptName, ZoneActor actor, ViewInterface view)
    {
        FlagField globalFlags = GlobalGameState.GetInstance().GetPlayer().GetFlagField();
        FlagData flagData = new FlagData(globalFlags, null, null);
        ViewLua viewLua = new ViewLua(view);
        LuaActor luaActor = actor.GetLuaActor();
        RunnableScript script = GetScript(scriptName);
        script.RunScript(flagData, viewLua, luaActor);
    }
}
