﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using System.Collections.Generic;

public class StationTool
{
    internal string generateSolar(string name, out float size, out int detection)
    {

        XmlDocument document = FileTools.GetXmlDocument("stations/" + name);

        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        XmlElement xmlElement = (XmlElement)rootXML.GetElementsByTagName("entity").Item(0);
        detection = int.Parse(xmlElement.GetAttribute("detection"));
        size = float.Parse(xmlElement.GetAttribute("size"));
        String sprite = xmlElement.GetAttribute("sprite");

        return sprite;
    }

    public StationContents GenerateContents(string fname, Entity entity)
    {

        XmlDocument document = FileTools.GetXmlDocument("stations/" + fname);
        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        List<Zone> zones = new List<Zone>();
        List<LandingSite> landingSites = new List<LandingSite>();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("zone".Equals(element.Name))
                {
                    string name = element.GetAttribute("name");
                    zones.Add(new Zone("stations/" + fname + "/",name,entity));
                }
                if ("landingZone".Equals(element.Name))
                {
                    LandingSite site = new LandingSite(element.GetAttribute("zone"), null);
                    site.SetPosition(new Vector2S(int.Parse(element.GetAttribute("x")),
                        int.Parse(element.GetAttribute("y"))));
                    landingSites.Add(site);
                }
            }

        }
        StationContents stationContents = new StationContents();
        stationContents.SetZones(zones);
        stationContents.SetLandingSites(landingSites);
        return stationContents;
    }
}
