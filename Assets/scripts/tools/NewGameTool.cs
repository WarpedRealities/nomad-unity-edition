﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;

public class NewGameTool 
{

    public StarSystem system;
    public Entity entity;
    public Zone zone;
    public Player player;
    private InventoryActionHandler InventoryActionHandler;

    public NewGameTool()
    {

    }

    public void NewGame(Universe universe)
    {

        XmlDocument document = FileTools.GetXmlDocument("start");

        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;

        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];

                if ("system".Equals(element.Name))
                {
                    string name = element.GetAttribute("value");
                    system = universe.GetSystem(name);
                    SystemGeneratorTools systemGenerator = new SystemGeneratorTools();
                    system.setContents(systemGenerator.generateStarSystem(system.GetName(),universe.GetUIDGenerator()));
                }
                if ("entity".Equals(element.Name))
                {
                    string name = element.GetAttribute("value");
                    Debug.Log("entity" + name);
                    entity = system.getEntity(name);
                    entity.generate();
                }
                if ("zone".Equals(element.Name))
                {
                    zone = entity.GetZone(element.GetAttribute("value"));
                    if (!zone.IsGenerated())
                    {
                        zone.Generate();
                    }
                }
                if ("position".Equals(element.Name))
                {
                    player = new Player(universe.getFactionLibrary().GetFaction("player"));
                    player.SetPosition(new Vector2Int(int.Parse(element.GetAttribute("x")), 
                        int.Parse(element.GetAttribute("y"))));
                    InventoryActionHandler = new InventoryActionHandler(player, null, null);
                }
                if ("item".Equals(element.Name))
                {
                    Item item = universe.GetItemLibrary().GetItemByCode(element.GetAttribute("ID"));
                    if (item.GetDef().GetItemType() == ItemType.EQUIP)
                    {
                        int ammo = 0;
                        EquipInstance equipInstance = (EquipInstance)item;
                        int.TryParse(element.GetAttribute("ammo"), out ammo);
                        string itemAmmo = element.GetAttribute("ammoItem");
                        
                        if (itemAmmo.Length > 4)
                        {
                            ItemAmmo ammoItem = (ItemAmmo)universe.GetItemLibrary().GetDefByCode(itemAmmo);
                            equipInstance.GetResourceStore().SetCapacity(ammoItem.GetResourceStore().GetCapacity());
                            equipInstance.GetResourceStore().SetAmmoCode(itemAmmo);
                            AmmoType ammoType = universe.GetItemLibrary().GetAmmoCode(ammoItem.GetResourceStore().GetDefaultAmmoType());
                            equipInstance.GetResourceStore().SetAmmoType(ammoType);


                        }
                        if (ammo > 0)
                        {
                            equipInstance.GetResourceStore().SetAmount(ammo);
                        }
                    }
                    if (item.GetDef().GetItemType() == ItemType.AMMO)
                    {
                        int ammo = -1;
                        AmmoInstance ammoInstance = (AmmoInstance)item;
                        int.TryParse(element.GetAttribute("ammo"), out ammo);
                        if (ammo > -1)
                        {

                            ammoInstance.GetResource().SetAmount(ammo);
                        }


                    }
                    player.GetInventory().AddItems(item, int.Parse(element.GetAttribute("count")));
                }
                if ("equip".Equals(element.Name))
                {
                    EquipItem(element);
                }
                if ("appearance".Equals(element.Name))
                {
                  
                    BuildAppearance(element, player);
                }
            }
        }
    }

    private void EquipItem(XmlElement element)
    {
        string item = element.GetAttribute("item");
        Item itemInstance = GetItem(item);
        InventoryActionHandler.HandleInventory(new ActionInventory(itemInstance, InventoryAction.EQUIP, 0),null);
    }

    private Item GetItem(string item)
    {
        for (int i = 0; i < player.GetInventory().GetItemCount(); i++)
        {
            if (item.Equals(player.GetInventory().GetItem(i).GetDef().GetCodeName()))
            {
                return player.GetInventory().GetItem(i);
            }
        }
        return null;
    }

    private void BuildAppearance(XmlElement element, Player player)
    {
        XmlNodeList children = element.ChildNodes;
        Appearance appearance = player.GetAppearance();
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];

                if ("bodyPart".Equals(e.Name))
                {
                    BodyPart bodyPart = BodyPartLoader.LoadPart(e.GetAttribute("partName"));
                    for (int j = 0; j < e.ChildNodes.Count; j++)
                    {
                        if (e.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement es = (XmlElement)e.ChildNodes[j];
                            bodyPart.SetValue(e.GetAttribute("partName"), int.Parse(e.GetAttribute("value")));
                        }
                    }
                    appearance.AddPart(bodyPart);
                }
            }
        }

    }

    internal bool ShiftStart(SaveState state)
    {
        XmlDocument document = FileTools.GetXmlDocument("start");

        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        Universe universe = GlobalGameState.GetInstance().GetUniverse();
        player = GlobalGameState.GetInstance().GetPlayer();
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];

                if ("system".Equals(element.Name))
                {
                    string name = element.GetAttribute("value");
                    system = universe.GetSystem(name);
                    SystemGeneratorTools systemGenerator = new SystemGeneratorTools();
                    system.setContents(systemGenerator.generateStarSystem(system.GetName(), universe.GetUIDGenerator()));
                }
                if ("entity".Equals(element.Name))
                {
                    string name = element.GetAttribute("value");

                    entity = system.getEntity(name);
                    entity.generate();
                }
                if ("zone".Equals(element.Name))
                {
                    zone = entity.GetZone(element.GetAttribute("value"));
                    if (!zone.IsGenerated())
                    {
                        zone.Generate();
                    }
                }
                if ("position".Equals(element.Name))
                {
                    player.SetPosition(new Vector2Int(int.Parse(element.GetAttribute("x")),
                        int.Parse(element.GetAttribute("y"))));
                }
 
            }
        }
        return true;
    }

    internal bool SpaceStart(SaveState state)
    {
        XmlDocument document = FileTools.GetXmlDocument("start");

        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        Universe universe = GlobalGameState.GetInstance().GetUniverse();
        player = GlobalGameState.GetInstance().GetPlayer();
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];

                if ("system".Equals(element.Name))
                {
                    string name = element.GetAttribute("value");
                    system = universe.GetSystem(name);
                    SystemGeneratorTools systemGenerator = new SystemGeneratorTools();
                    system.setContents(systemGenerator.generateStarSystem(system.GetName(), universe.GetUIDGenerator()));
                }
            }
        }
        entity = state.GetShipEntity();
        zone = state.GetShipEntity().GetZone(0);
        system.GetSystemContents().GetEntities().Add(entity);
        state.GetShipEntity().setPosition(new Vector2Int(-16, 16));
        return true;
    }
}
