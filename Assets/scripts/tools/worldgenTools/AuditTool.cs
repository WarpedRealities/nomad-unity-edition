﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class AuditTool
{

    AuditPathfinder auditPathfinder;
    Pathfinder pathfinder;
    List<Vector2Int> points;
    public AuditTool(int width, int height, Tile[][] tiles)
    {
        points = new List<Vector2Int>();
        auditPathfinder = new AuditPathfinder(width, height, tiles,true);
        pathfinder = new Pathfinder(width, height, tiles,true);
    }

    public void Setup(List<Actor> actors, Tile[][] tiles, PointsOfInterest pointsOfInterest)
    {
        for (int i = 0; i < actors.Count; i++)
        {
            points.Add(GetPoint(actors[i].GetPosition(),tiles));
        }
        for (int i = 0; i < pointsOfInterest.GetPoints().Count; i++)
        {
            points.Add(pointsOfInterest.GetPoints()[i]);
        }
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[0].Length; j++)
            {
                if (tiles[i][j]!=null && tiles[i][j].GetWidget() != null)
                {

                    points.Add(GetPoint(new Vector2Int(i,j),tiles));
                }
            }
        }
    }
    internal void Setup(List<Actor> actors, Tile[][] tiles, bool[] bools, int side, PointsOfInterest pointsOfInterest)
    {
        for (int i = 0; i < actors.Count; i++)
        {
            points.Add(GetPoint(actors[i].GetPosition(), tiles));
        }
        for (int i = 0; i < pointsOfInterest.GetPoints().Count; i++)
        {
            points.Add(pointsOfInterest.GetPoints()[i]);
        }
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[0].Length; j++)
            {
                if (tiles[i][j] != null && tiles[i][j].GetWidget() != null)
                {
                    points.Add(GetPoint(new Vector2Int(i, j), tiles));
                }
            }
        }
        if (bools != null)
        {
            int successive = 0;
            for (int i=0;i<bools.Length;i++)
            {
                if (bools[i])
                {
                    successive++;
                    if (successive > 1)
                    {
                        switch (side)
                        {
                            case 0:
                                points.Add(new Vector2Int(i, tiles[0].Length - 1));
                                break;
                            case 1:
                                points.Add(new Vector2Int(tiles.Length-1, i));
                                break;
                            case 2:
                                points.Add(new Vector2Int(i, 0));
                                break;
                            case 3:
                                points.Add(new Vector2Int(0, i));
                                break;
                        }
                        successive = 0;
                    }
                }
                else
                {
                    successive = 0;
                }
            }
        }
        points = points.OrderBy(_ => DiceRoller.GetInstance().RollOdds()).ToList();
    }

    private Vector2Int GetPoint(Vector2Int p, Tile[][] tiles)
    {
        for (int i = 0; i < 4; i++)
        {
            Vector2Int p0 = GeometryTools.GetPosition(p.x, p.y, i * 2);
            //is this a walkable tile?
            if (p0.x>=0 && p0.x < tiles.Length - 1 &&
                p0.y>=0 && p0.y < tiles[0].Length-1)
            {
                if (tiles[p0.x][p0.y] != null && tiles[p0.x][p0.y].canWalk())
                {
                    return p0;
                }
            }
        }
        return p;
    }
    public void AuditPaths(TileTools tileTools, Tile[][] tiles,int carve, int replace)
    {
        auditPathfinder.SetupAudit(true, replace, carve);

        for (int i = 0; i < points.Count-1; i++)
        {
            Path[] path = null;
            if (!pathfinder.FindPath(points[i],points[i+1],256,out path, TileMovement.WALK))
            {
 
               if (auditPathfinder.FindPath(points[i],points[i+1],256,out path, TileMovement.WALK))
                { 
                    for (int j = 0; j < path.Length; j++)
                    {
                        if (path[j] != null)
                        {
                            tileTools.PaintTile(tiles, path[j].position.x, path[j].position.y, replace);
                        }
                    }
                }
               else
                {

                }
            }
        }
        for (int i = 0; i < points.Count; i++)
        {
            for (int j=points.Count-1; j>=0; j--)
            {
                if (i != j)
                {
                    Path[] path = null;
                    if (!pathfinder.FindPath(points[i], points[j], 256, out path, TileMovement.WALK))
                    {

                        if (auditPathfinder.FindPath(points[i], points[j], 256, out path, TileMovement.WALK))
                        {
                            for (int k = 0; k < path.Length; k++)
                            {
                                if (path[k] != null)
                                {
                                    tileTools.PaintTile(tiles, path[k].position.x, path[k].position.y, replace);
                                }
                            }
                        }
                        else
                        {

                        }
                    }
                }
            }

        }
    }

}
