﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using System.Collections.Generic;

public class TownBuilder
{
    private Tile[][] tiles;
    private bool[][] grid;
    private TileTools tileTools;
    private PointsOfInterest pointsOfInterest;
    
    public TownBuilder(Tile[][] tiles, bool[][] grid, TileTools tileTools, PointsOfInterest pointsOfInterest)
    {
        this.tiles = tiles;
        this.grid = GridCopier.Copy(grid);
        this.tileTools = tileTools;
        this.pointsOfInterest = pointsOfInterest;
    }

    internal bool [][] BuildTown(XmlElement element)
    {
        int minSize = int.Parse(element.GetAttribute("minSize"))+1;
        int maxSize = int.Parse(element.GetAttribute("maxSize"))+1;
        int minHouses = int.Parse(element.GetAttribute("minHouses"));
        int maxHouses = int.Parse(element.GetAttribute("maxHouses"));
        int floor = int.Parse(element.GetAttribute("floor"));
        int wall = int.Parse(element.GetAttribute("wall"));
        int spacing = int.Parse(element.GetAttribute("spacing"));
        bool registerPoints = "true".Equals(element.GetAttribute("registerPoints"));
        int houseCount = minHouses == maxHouses ? maxHouses : minHouses + (DiceRoller.GetInstance().RollDice(maxHouses - minHouses));

        List<Rect> houseRectangles = BuildRectangles(houseCount, minSize, maxSize,spacing);
        //now build the houses
         return BuildHouses(houseRectangles, floor, wall, registerPoints);
        //return grid;
    }

    private bool [][] BuildHouses(List<Rect> houseRectangles, int floor, int wall, bool register)
    {
        bool [][]nugrid = new bool[tiles.Length][];
        for (int i = 0; i < grid.Length; i++)
        {
            nugrid[i] = new bool[grid[0].Length];
            for (int j = 0; j < grid[0].Length; j++)
            {
                if (tiles[i][j] != null)
                {
                    nugrid[i][j] = false;
                }
            }
        }
        for (int i = 0; i < houseRectangles.Count; i++)
        {
          //  houseRectangles[i].position.Set(houseRectangles[i].position.x+1,houseRectangles[i].position.y+1);
          //  houseRectangles[i].size.Set(houseRectangles[i].size.x - 1, houseRectangles[i].size.y - 1);
            BuildHouse(houseRectangles[i], floor, wall, nugrid);
            if (register)
            {
                pointsOfInterest.AddPointOfInterest(
                    new Vector2Int((int)(houseRectangles[i].position.x+(houseRectangles[i].size.x/2)), 
                    (int)(houseRectangles[i].position.y+(houseRectangles[i].size.y/2))));
            }
        }
        return nugrid;
    }

    private void BuildHouse(Rect rect, int floor, int wall, bool [][] nugrid)
    {
        //establish entrance position
            int rEntrance = DiceRoller.GetInstance().RollDice(4);
        //build walls

        //horizontal walls
        for (int i = 0; i < rect.size.x; i++)
        {
            tileTools.PaintTile(tiles, i + (int)rect.position.x, (int)rect.position.y, wall);
            tileTools.PaintTile(tiles, i + (int)rect.position.x, (int)(rect.position.y+rect.size.y-1), wall);
        }
        //vertical walls
        for (int i = 1; i < rect.size.y-1; i++)
        {
            tileTools.PaintTile(tiles, (int)rect.position.x, (int)rect.position.y+i, wall);
            tileTools.PaintTile(tiles, (int)(rect.position.x+rect.size.x-1), (int)rect.position.y+i, wall);
        }
        //build floors


        for (int i = 1; i < rect.size.x-1; i++)
        {
            for (int j = 1; j < rect.size.y-1; j++)
            {
                nugrid[i + (int)rect.position.x][j + (int)rect.position.y] = true;
           //     tileTools.PaintTile(tiles, i + (int)rect.position.x, j+(int)rect.position.y, floor);
            }
        }

        //carve entrance
        BuildDoor(rect, rEntrance, floor);
    }

    private void BuildDoor(Rect rect, int rEntrance, int floor)
    {
        switch (rEntrance)
        {
            case 0:
                tileTools.PaintTile(tiles, (int)rect.position.x,(int)( rect.position.y+(rect.size.y/2)), floor);
                tileTools.PaintTile(tiles, (int)rect.position.x-1, (int)(rect.position.y + (rect.size.y / 2)), floor);
                break;
            case 1:
                tileTools.PaintTile(tiles, (int)(rect.position.x + (rect.size.x / 2)), (int)rect.position.y, floor);
                tileTools.PaintTile(tiles, (int)(rect.position.x + (rect.size.x / 2)), (int)rect.position.y-1, floor);
                break;
            case 2:
                tileTools.PaintTile(tiles, (int)(rect.position.x+rect.size.x-1), (int)(rect.position.y + (rect.size.y / 2)), floor);
                tileTools.PaintTile(tiles, (int)(rect.position.x + rect.size.x), (int)(rect.position.y + (rect.size.y / 2)), floor);
                break;
            case 3:
                tileTools.PaintTile(tiles, (int)(rect.position.x + (rect.size.x / 2)), (int)(rect.position.y+rect.size.y-1), floor);
                tileTools.PaintTile(tiles, (int)(rect.position.x + (rect.size.x / 2)), (int)(rect.position.y + rect.size.y), floor);
                break;
        }
    }

    private List<Rect> BuildRectangles(int houseCount, int minSize, int maxSize, int spacing)
    {
        List<Rect> rectangles = new List<Rect>();
    
        for (int i = 0; i < houseCount; i++)
        {
            int width = minSize + DiceRoller.GetInstance().RollDice(maxSize - minSize);
            int height = minSize + DiceRoller.GetInstance().RollDice(maxSize - minSize);

            rectangles.Add(BuildRect(width, height, minSize, spacing));
        }
        return rectangles;
    }

    private Rect BuildRect(int width, int height, int minSize, int spacing)
    {
        int correct = 0;
        int x = 1+DiceRoller.GetInstance().RollDice(tiles.Length - 2-width);
        int y = 1 + DiceRoller.GetInstance().RollDice(tiles[0].Length - 2-height);
        while (!ValidCheck(x, y,width, height))
        {
            correct++; 
            x = 1 + DiceRoller.GetInstance().RollDice(tiles.Length - 2-width);
            y = 1 + DiceRoller.GetInstance().RollDice(tiles[0].Length - 2-height);
            if (correct > 10)
            {
                if (width > minSize)
                {
                    width--;
                }
                if (height > minSize)
                {
                    height--;
                }
            }
        }

        for (int i = x-spacing; i < x+width+ spacing; i++)
        {
            for (int j = y- spacing; j < y+height+ spacing; j++)
            {
                grid[i][j] = false;
            }
        }

        return new Rect(new Vector2(x, y), new Vector2(width, height));
    }

    private bool ValidCheck(int x, int y,int width, int height)
    {

        for (int i = x; i < x + width; i++)
        {
            for (int j=y;j< y+ height; j++)
            {
                if (!grid[i][j])
                {

                    return false;
                }
                if (tiles[i][j]!=null)
                {
                    return false;
                }
            }
        }
   
        return true;
    }
}
