using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellularAutomataTool 
{
    bool[][] cells;
    int width, height;
    readonly int OVERCROWDING = 5;
    readonly int STARVATION = 2;
    readonly int RESURRECT = 4;
    public CellularAutomataTool(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    public void InitialState(float liveCellProportion, bool [][] grid)
    {
        cells = BuildCells();
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (grid[i][j])
                {
                    float odds = DiceRoller.GetInstance().RollOdds();
                    cells[i][j] = odds < liveCellProportion;
                }
                else
                {
                    cells[i][j] = false;
                }
            }
        }
    }

    public void Iterate(bool [][] grid, bool noDeath)
    {
        bool[][] nuCells = BuildCells();
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (grid[i][j])
                {
                    int neighbours = countNeighbours(i, j);
                    nuCells[i][j] = GetCellState(cells[i][j],neighbours, noDeath);
                }
                else
                {
                    nuCells[i][j] = false;
                }
            }
        }
        cells = nuCells;
    }

    public void BuildWalls(bool[][] grid, Tile[][] tiles, TileTools tileTools, int walls)
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (grid[i][j] && !cells[i][j] && tiles[i][j]==null)
                {
                    tileTools.PaintTile(tiles, i, j, walls);
                }
            }
        }
    }

    public bool[][] GenerateGrid()
    {
        bool[][] grid = BuildCells();
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                grid[i][j] = cells[i][j];
            }
        }
        return grid;
    }

    private bool GetCellState(bool currentState, int neighbours, bool noDeath)
    {
        if (currentState)
        {
            if (neighbours > OVERCROWDING && !noDeath)
            {
                return false;
            }
            if (neighbours < STARVATION && !noDeath)
            {
                return false;
            }
        }
        else
        {
            if (neighbours >= RESURRECT)
            {
                return true;
            }
        }
        return currentState;
    }

    private int countNeighbours(int x, int y)
    {
        int count = 0;
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(x, y, i);
            if (p.x<0||p.x>=width || p.y < 0 || p.y >= height)
            {
                //Debug.Log("shouldn't happen");
            }
            else
            {
                if (cells[p.x][p.y])
                {
                    count++;
                }
            }
        }
        return count;
    }

    private bool[][] BuildCells()
    {
        bool[][] cells = new bool[width][];
        for (int i = 0; i < width; i++)
        {
            cells[i] = new bool[height];

        }
        return cells;
    }

    internal bool FloodFillCheck(float minimum)
    {
        bool[][] floodGrid = new bool[width][];
        for (int j = 0; j < width; j++)
        {
            floodGrid[j] = new bool[height];
        }
        Vector2Int p = GetFloodFillStart();
        floodGrid[p.x][p.y] = true;
        int max = width * height;
        int threshold = (int)(max * minimum);
        int filled = 0;
        Stack<Vector2Int> stack = new Stack<Vector2Int>();
        stack.Push(p);
        while (stack.Count > 0)
        {
            //check around p
            if (FloodCheck(p, out p, floodGrid))
            {
                stack.Push(p);
                floodGrid[p.x][p.y] = true;
                filled++;
            }
            else
            {
                //start backtracking
                p = stack.Pop();
            }
        }

        if (filled >= threshold)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (cells[i][j] && !floodGrid[i][j])
                    {
                        cells[i][j] = false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    private bool FloodCheck(Vector2Int p1, out Vector2Int p2, bool [][] grid)
    {
        for (int i = 0; i < 4; i++)
        {
            Vector2Int p3 = GeometryTools.GetPosition(p1.x, p1.y, i*2);
            if (p3.x < 0 || p3.x >= width || p3.y < 0 || p3.y >= height)
            {
                //Debug.Log("shouldn't happen");
            }
            else if (cells[p3.x][p3.y] && !grid[p3.x][p3.y])
            {
                p2 = p3;
                return true;
            }
        }
        p2 = p1;
        return false;
    }

    private Vector2Int GetFloodFillStart()
    {
        Vector2Int p = new Vector2Int(DiceRoller.GetInstance().RollDice(width), 
            DiceRoller.GetInstance().RollDice(height));
        while (!cells[p.x][p.y])
        {
            p = new Vector2Int(DiceRoller.GetInstance().RollDice(width),
            DiceRoller.GetInstance().RollDice(height));
        }
        return p;
    }
}
