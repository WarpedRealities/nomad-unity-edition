﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class WallbreakerTool
{
    private TileTools tileTools;

    public WallbreakerTool(TileTools tileTools)
    {
        this.tileTools = tileTools;
    }

    internal void BreakWallsHorizontal(TileDef tileDef, int middle, int left, int right, int chance, bool[][] grid, Tile[][] tiles)
    {
        List<Vector2Int> sections = GetHorizontalSections(tileDef, tiles, grid);

        int count = 0;
        for (int i = 0; i < sections.Count; i++)
        {
            int diceRoll = DiceRoller.GetInstance().RollDice(chance);
            if (diceRoll == 0)
            {
                //break this wall
                count++;
                BreakWallsHorizontal(tileDef, sections[i], left, right, middle, tiles);
            }
        }

        CleanupHorizontal(tiles, 
            tileTools.GetTileset().getDefinition(left-1),
            tileTools.GetTileset().getDefinition(middle-1),
            tileTools.GetTileset().getDefinition(right-1),middle);
        //throw new NotImplementedException();
    }

    private void CleanupHorizontal(Tile[][] tiles, TileDef left, TileDef middle, TileDef right, int middleInt)
    {
        for (int i = 1; i < tiles.Length - 1; i++)
        {
            for (int j = 1; j < tiles.Length - 1; j++)
            {
                //check if the tile is a left or right, if it is check if it has a middle tile on both sides, if so, replace with middle tile
                if (tiles[i][j]!=null && (tiles[i][j].getDefinition().Equals(left)|| tiles[i][j].getDefinition().Equals(right)))
                {
                    if (tiles[i-1][j].getDefinition().Equals(middle) && tiles[i + 1][j].getDefinition().Equals(middle))
                    {
                        tileTools.PaintTile(tiles,i, j, middleInt);
                    }
                }
            }
        }
    }

    private void BreakWallsHorizontal(TileDef tileDef, Vector2Int vector2Int, int left, int right, int middle, Tile[][] tiles)
    {
        tileTools.PaintTile(tiles, vector2Int.x, vector2Int.y, middle);
        if (tiles[vector2Int.x - 1][vector2Int.y].getDefinition().Equals(tileDef))
        {
            tileTools.PaintTile(tiles, vector2Int.x-1, vector2Int.y, left);
        }
        if (tiles[vector2Int.x +1][vector2Int.y].getDefinition().Equals(tileDef))
        {
            tileTools.PaintTile(tiles, vector2Int.x+1, vector2Int.y, right);
        }
      
    }

  

    internal List<Vector2Int> GetHorizontalSections(TileDef tileDef, Tile[][] tiles, bool[][] grid)
    {
        List<Vector2Int> sections = new List<Vector2Int>();
        for (int i = 1; i < tiles.Length - 1; i++)
        {
            for (int j = 1; j < tiles.Length - 1; j++)
            {
                if (grid[i][j] && tiles[i][j]!=null && tiles[i][j].getDefinition() == tileDef)
                {
                    if (AdjacentTileChecker.AdjacencyCheck(i,j, tileDef, tiles)==10)
                    {
                        sections.Add(new Vector2Int(i, j));
                    }
                }
            }
        }
        return sections;
    }

    private bool CheckTileHorizontal(int x, int y, Tile[][] tiles, TileDef tileDef)
    {
   
        int[] presence = { 2, 6 };
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(x, y, i);
            Tile t = tiles[p.x][p.y];

            if (presence[0]==i || presence[1] == i)
            {
                if (t == null || !t.getDefinition().Equals(tileDef))
                {
                    return false;
                }
            }
            else
            {
                if (t!=null && t.getDefinition().Equals(tileDef))
                {
                    return false;
                }
            }
        }
        return true;     
    }

    internal void BreakWallsVertical(TileDef tileDef, int middle, int left, int right, int chance, bool[][] grid, Tile[][] tiles)
    {
        List<Vector2Int> sections = GetVerticalSections(tileDef, tiles, grid);

        int count = 0;
        for (int i = 0; i < sections.Count; i++)
        {
            
            int diceRoll = DiceRoller.GetInstance().RollDice(chance);
            if (diceRoll == 0)
            {
                //break this wall
                count++;
                BreakWallsVertical(tileDef, sections[i], left, right, middle, tiles);
            }
            
            //tileTools.PaintTile(tiles, sections[i].x, sections[i].y, middle);
        }

        CleanupVertical(tiles,
            tileTools.GetTileset().getDefinition(left - 1),
            tileTools.GetTileset().getDefinition(middle - 1),
            tileTools.GetTileset().getDefinition(right - 1), middle);
    }

    private void CleanupVertical(Tile[][] tiles, TileDef left, TileDef middle, TileDef right, int middleInt)
    {
        for (int i = 1; i < tiles.Length - 1; i++)
        {
            for (int j = 1; j < tiles.Length - 1; j++)
            {
                //check if the tile is a left or right, if it is check if it has a middle tile on both sides, if so, replace with middle tile
                if (tiles[i][j] != null && (tiles[i][j].getDefinition().Equals(left) || tiles[i][j].getDefinition().Equals(right)))
                {
                    if (tiles[i][j-1].getDefinition().Equals(middle) && tiles[i][j+1].getDefinition().Equals(middle))
                    {
                        tileTools.PaintTile(tiles, i, j, middleInt);
                    }
                }
            }
        }
    }

    private void BreakWallsVertical(TileDef tileDef, Vector2Int vector2Int, int left, int right, int middle, Tile[][] tiles)
    {
        tileTools.PaintTile(tiles, vector2Int.x, vector2Int.y, middle);
        if (tiles[vector2Int.x][vector2Int.y-1].getDefinition().Equals(tileDef))
        {
            tileTools.PaintTile(tiles, vector2Int.x, vector2Int.y-1, left);
        }
        if (tiles[vector2Int.x][vector2Int.y+1].getDefinition().Equals(tileDef))
        {
            tileTools.PaintTile(tiles, vector2Int.x, vector2Int.y+1, right);
        }
    }

    private List<Vector2Int> GetVerticalSections(TileDef tileDef, Tile[][] tiles, bool[][] grid)
    {
        List<Vector2Int> sections = new List<Vector2Int>();
        for (int i = 1; i < tiles.Length - 1; i++)
        {
            for (int j = 1; j < tiles.Length - 1; j++)
            {
                if (grid[i][j] && tiles[i][j] != null && tiles[i][j].getDefinition() == tileDef)
                {
                    if (AdjacentTileChecker.AdjacencyCheck(i, j, tileDef, tiles)==5 &&
                        AdjacentTileChecker.AdjacencyCheck(i, j-1, tileDef, tiles) == 5)
                    {
                        sections.Add(new Vector2Int(i, j));
                    }
                }
            }
        }
        return sections;
    }

    private bool CheckTileVertical(int x, int y, Tile[][] tiles, TileDef tileDef)
    {
     
        int[] presence = { 0, 4 };
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(x, y, i);
            Tile t = tiles[p.x][p.y];

            if (presence[0] == i || presence[1] == i)
            {
                if (t == null || !t.getDefinition().Equals(tileDef))
                {
                    return false;
                }
            }
            else
            {
                if (t != null && t.getDefinition().Equals(tileDef))
                { 
                    return false;
                }
            }
        }
        return true;
    }
}
