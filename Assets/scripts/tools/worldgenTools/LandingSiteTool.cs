﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

public class LandingSiteTool
{
    private const string FILEPREFIX = "/ships/";
    private Tile[][] tiles;
    private Vector2S fixedLandingSite;
    private ZoneParameters zoneParameters;
    private TileTools tileTools;

    public LandingSiteTool(Tile[][] tiles, Vector2S fixedLandingSite, ZoneParameters zoneParameters, TileTools tileTools)
    {
        this.zoneParameters = zoneParameters;
        this.tileTools = tileTools;
        this.tiles = tiles;
        this.fixedLandingSite = fixedLandingSite;
    }

    public void RemoveLanding(LandingSite landingSite)
    {
        Vector2S p = landingSite.GetPosition();
        int width = landingSite.GetSpaceship().GetSpaceshipStats().GetWidth();
        int height = landingSite.GetSpaceship().GetSpaceshipStats().GetHeight();
        if (p != null)
        {
            tiles[p.x][p.y].SetWidget(null);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Tile t = tiles[p.x + i][p.y + j];
                    if (t.getDefinition().GetIndex() == 0)
                    {
                        tileTools.PaintTile(tiles, p.x + i, p.y + j, 2);
                    }
                    if (t.GetWidget() is WidgetPortal)
                    {
                        t.SetWidget(null);
                    }
                }
            }
        }
    }

    internal bool Land(LandingSite landingSite, ref bool[][] grid, bool pregen)
    {

        if (pregen)
        {
            grid= PregenLanding(landingSite, grid);
            return true;
        }
        else
        {
            return PostGenLanding(landingSite, grid);
        }
        throw new NotImplementedException();
    }

    private bool PostGenLanding(LandingSite landingSite, bool[][] grid)
    {
        //find an open area wide enough for the ship
        Vector2S p = GetLandingPosition(landingSite.GetSpaceship().GetSpaceshipStats().GetWidth(),
            landingSite.GetSpaceship().GetSpaceshipStats().GetHeight(),fixedLandingSite);
        if (p == null) { return false; }
        //print the ship into the zone
        landingSite.SetPosition(p);
        PrintPrefab(new Vector2Int(p.x, p.y), landingSite, grid);
        return true;
    }

    private Vector2S GetLandingPosition(int width, int height, Vector2S fixedLanding)
    {
        if (fixedLanding != null)
        {
            //offset by half width and half height
            return new Vector2S(fixedLanding.x-(width/2), fixedLanding.y-(height/2));
        }
        bool inverse = DiceRoller.GetInstance().RollDice(6) % 2 > 0;
        if (!inverse)
        {
            for (int i = 1; i < zoneParameters.GetWidth() - width-1; i++)
            {
                for (int j = 1; j < zoneParameters.GetHeight() - height-1; j++)
                {
                    if (CanPlace(i, j, width, height))
                    {
                        return new Vector2S(i, j);
                    }
                }
            }
        }
        else
        {
            for (int i = zoneParameters.GetWidth() - width-1; i > 0 ; i--)
            {
                for (int j = zoneParameters.GetHeight() - height-1; j > 0 ; j--)
                {
                    if (CanPlace(i, j, width, height))
                    {
                        return new Vector2S(i, j);
                    }
                }
            }
        }

        return null;
    }

    private bool CanPlace(int x, int y, int width, int height)
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (tiles[i + x][j + y] == null)
                {
                    return false;
                }
                if (!tiles[i + x][j + y].canWalk())
                {
                    return false;
                }
            }
        }
        return true;

    }

    private bool[][] PregenLanding(LandingSite landingSite, bool[][] grid)
    {
        if (fixedLandingSite != null)
        {
            Vector2Int p = new Vector2Int(fixedLandingSite.x - (landingSite.GetSpaceship().GetSpaceshipStats().GetWidth() / 2),
                fixedLandingSite.y - (landingSite.GetSpaceship().GetSpaceshipStats().GetHeight() / 2));
            landingSite.SetPosition(new Vector2S(p.x, p.y));
            //print prefab
            Debug.Log("p?" + p);
            return PrintPrefab(p, landingSite, grid);
        }
        else
        {
            //find a random spot
            Vector2Int p = GetPosition(landingSite);
            landingSite.SetPosition(new Vector2S(p.x,p.y));
            //print prefab
            return PrintPrefab(p, landingSite, grid);
        }
        throw new NotImplementedException();
    }

    private bool[][] PrintPrefab(Vector2Int p, LandingSite landingSite, bool [][] grid)
    {

        XmlDocument document = FileTools.GetXmlDocument(FILEPREFIX + landingSite.GetSpaceship().GetFile());
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;

        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("exterior".Equals(element.Name))
                {
                    return Prefab(element, p, grid,landingSite);
                }
            }
        }
        return grid;
    }

    private bool[][] Prefab(XmlElement root, Vector2Int offset, bool[][] grid, LandingSite landingSite)
    {
        int width = landingSite.GetSpaceship().GetSpaceshipStats().GetWidth();
        int yIndex = 0;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("row".Equals(element.Name))
                {

                    string textContents = element.InnerText.Replace(" ", "");

                    char[] charArray = textContents.ToCharArray();
                    for (int j = 0; j < width; j++)
                    {
                        tiles[j + offset.x][offset.y + yIndex] = tileTools.GetTile(System.Uri.FromHex(charArray[j]), new Vector2S(j + offset.x, offset.y + yIndex));
                    }
                    yIndex++;
                }
                if ("portal".Equals(element.Name))
                {
                    int x = int.Parse(element.GetAttribute("x"));
                    int y = int.Parse(element.GetAttribute("y"));
                    int id = int.Parse(element.GetAttribute("uid"));
                    int facing = int.Parse(element.GetAttribute("facing"));
                    tiles[x + offset.x][y + offset.y].SetWidget(new WidgetPortal(null, id, facing));
                }

            }
        }
        tiles[offset.x][offset.y].SetWidget(new WidgetSprite(landingSite.GetSpaceship().getSprite(), width,
            landingSite.GetSpaceship().GetSpaceshipStats().GetHeight()));
        
        return grid;
    }

    private Vector2Int GetPosition(LandingSite landingSite)
    {
        int width = landingSite.GetSpaceship().GetSpaceshipStats().GetWidth();
        int height = landingSite.GetSpaceship().GetSpaceshipStats().GetHeight();
        int x = DiceRoller.GetInstance().RollDice(zoneParameters.GetWidth() - 8 - width);
        int y = DiceRoller.GetInstance().RollDice(zoneParameters.GetHeight() - 8 - height);
        x = x + 4 - (width / 2);
        y = y + 4 - (height / 2);
        return new Vector2Int(x, y);
    }
}
