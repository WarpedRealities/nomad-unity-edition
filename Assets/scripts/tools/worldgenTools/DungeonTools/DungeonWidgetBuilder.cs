﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class DungeonWidgetBuilder
{
    private Dictionary<string, LootTableTool> lootGenerators;

    public DungeonWidgetBuilder(Dictionary<string, LootTableTool> lootGenerators)
    {
        this.lootGenerators = lootGenerators;
    }

    internal Widget BuildWidget(string file, string info)
    {
        string[] strings = info.Split('#');
        if ("PORTAL".Equals(file))
        {
            return BuildPortal(strings);
        }
        else
        {
            return BuildRegular(file, info);
        }
        throw new NotImplementedException();
    }

    private Widget BuildRegular(string file, string info)
    {
        Widget widget= WidgetBuilder.BuildWidget(file);
        if (widget is WidgetContainer)
        {
            WidgetContainer widgetContainer = (WidgetContainer)widget;
            widgetContainer.GetContainerData().SetItems(lootGenerators[info].GetLoot());
        }
        if (widget is WidgetItemPile)
        {
            WidgetItemPile itemPile = (WidgetItemPile)widget;
            List<Item> itemList = lootGenerators[info].GetLoot();
            if (itemList.Count > 0)
            {
                itemPile.SetItemList(new ItemList(itemList));
            }
            else
            {
                return null;
            }
        }
        if (widget is WidgetConversation)
        {
            WidgetConversation widgetConversation = (WidgetConversation)widget;
            widgetConversation.SetConversation(info);
        }
        if (widget is WidgetDoor)
        {
            WidgetDoor widgetDoor = (WidgetDoor)widget;
            string[] str = info.Split('#');
            widgetDoor.SetStrength(int.Parse(str[0]));
            widgetDoor.SetKey(str[1]);
        }
        if (widget is WidgetBarrier)
        {
            WidgetBarrier widgetBarrier = (WidgetBarrier)widget;
            string[] split = info.Split('#');
            string key = split[0];
            string comparison = split[1];
            int value = int.Parse(split[2]);

            widgetBarrier.SetBarrierValues(key, value, comparison);
        }

        return widget;
    }

    private Widget BuildPortal(string[] strings)
    {
        return new WidgetPortal(strings[0], int.Parse(strings[1]), int.Parse(strings[2]));
    }
}
