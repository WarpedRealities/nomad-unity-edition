﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DungeonNewBlockTool
{
    private List<DungeonBlock> blocks;
    private List<KeyBlock> keyBlocks;
    int keyblocksPlaced;
    public DungeonNewBlockTool(List<DungeonBlock> blocks, List<KeyBlock> keyBlocks)
    {
        this.blocks = blocks;
        this.keyBlocks = keyBlocks;
        keyblocksPlaced = CountFixedKeyBlocks(keyBlocks);
    }

    private int CountFixedKeyBlocks(List<KeyBlock> keyBlocks)
    {
        int count = 0;
        for (int i = 0; i < keyBlocks.Count; i++)
        {
            if (keyBlocks[i].isFixed())
            {
                count++;
            }
        }
        return count;
    }

    internal int GetBlock(short[] edgeMap)
    {
        List<int> indices = new List<int>();
        for (int i = 0; i < blocks.Count; i++)
        {
            if (CanPlace(edgeMap, blocks[i]))
            {
                indices.Add(i);
            }
        }
        if (indices.Count == 0)
        {
            return 0;
        }
        int r = indices.Count == 1 ? 0 : DiceRoller.GetInstance().RollDice(indices.Count);
     //   Debug.Log("get block " + indices[0]);
        return indices[r]+1;
    }

    internal int GetNewBlock(short[] edgeMap, short heat)
    {
        if (keyblocksPlaced < keyBlocks.Count)
        {
            int index = DiceRoller.GetInstance().RollDice(3)== 1 ? GetKeyBlock(edgeMap, heat) : 0;
            if (index != 0)
            {
                return index;
            }
            else
            {
                return GetBlock(edgeMap);
            }
        }
        else
        {
            return GetBlock(edgeMap);
        }
    }

    private bool CanPlace(short[] edgeMap, DungeonBlock block)
    {
        for (int i = 0; i < 4; i++)
        {
            if (edgeMap[i] != -1)
            {
                if (block.GetEdgeValues(i) != edgeMap[i])
                {
                    return false;
                }
            }
        }
        return true;
    }

    internal bool AllKeyBlocksPlaced()
    {
        return keyblocksPlaced >= keyBlocks.Count;
    }

    public void Reset()
    {
        keyblocksPlaced = CountFixedKeyBlocks(keyBlocks);
        foreach (KeyBlock item in keyBlocks)
        {
            item.SetPlaced(false);
        }
    }

    private int GetKeyBlock(short[] edgeMap, short heat)
    {

        for (int i = 0; i < this.keyBlocks.Count; i++)
        {
            if (!keyBlocks[i].isPlaced() && keyBlocks[i].GetHeat()<=heat)
            {
                if (CanPlace(edgeMap, keyBlocks[i]))
                {
              
                    keyBlocks[i].SetPlaced();
                    keyblocksPlaced++;
                    return (i*-1)-1;
                }
            }
        }
        return 0;
    }

    internal int GetKeyBlockCount()
    {
        return keyblocksPlaced;
    }

    public int GetKeyCount()
    {
        return keyBlocks.Count;
    }
}
