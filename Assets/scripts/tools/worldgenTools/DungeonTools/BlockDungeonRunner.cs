﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using System.Collections.Generic;

public class BlockDungeonRunner
{
    internal static void Load(BlockDungeonTool blockDungeonTool, XmlElement root)
    {
        int extent = int.Parse(root.GetAttribute("extent"));
        List<KeyBlock> keyBlocks = new List<KeyBlock>();
        List<DungeonBlock> blocks = new List<DungeonBlock>();
        Dictionary<string, LootTableTool> lootTables = new Dictionary<string, LootTableTool>();
        Dictionary<int, string> spawnTable = new Dictionary<int, string>();
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType.Equals(XmlNodeType.Element))
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("keyblock".Equals(element.Name))
                {
                    keyBlocks.Add(LoadKeyBlock(element));
                }

                if ("blockset".Equals(element.Name))
                {
                    LoadBlocks(element.GetAttribute("file"), blocks);
                }
                if ("lootTable".Equals(element.Name))
                {
                    lootTables.Add(element.GetAttribute("id"), new LootTableTool(LootTableBuilder.Build(element)));
                }
                if ("spawn".Equals(element.Name))
                {
                    spawnTable.Add(int.Parse(element.GetAttribute("id")), element.GetAttribute("file"));
                }
            }
        }

        blockDungeonTool.LoadElements(extent, keyBlocks, blocks, lootTables,spawnTable);
        //throw new NotImplementedException();
    }

    private static void LoadBlocks(string filename, List<DungeonBlock> blocks)
    {
        XmlDocument xmlDocument = FileTools.GetXmlDocument("dungeonBlocks/"+filename);
        XmlElement rootXML = (XmlElement)xmlDocument.FirstChild.NextSibling;
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (XmlNodeType.Element.Equals(rootXML.ChildNodes[i].NodeType))
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("block".Equals(element.Name))
                {
                    blocks.Add(LoadBlock(element));
                }
            }
        }
    }
    private static DungeonBlock LoadBlock(XmlElement root)
    {

        short[] edges = new short[4];// = new short[4];
        List<DungeonWidget> widgets = new List<DungeonWidget>();
        List<DungeonSpawn> spawns = new List<DungeonSpawn>();
        int[][] grid = new int[8][];
        for (int i = 0; i < 8; i++)
        {
            grid[i] = new int[8];
        }
        int index = 0;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (XmlNodeType.Element.Equals(root.ChildNodes[i].NodeType))
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("row".Equals(element.Name))
                {
                    string textContents = element.InnerText.Replace(" ", "");
                    char[] charArray = textContents.ToCharArray();
                    for (int j = 0; j < 8; j++)
                    {
                        grid[index][j] = System.Uri.FromHex(charArray[j]);
                    }
                    index++;
                }
                if ("widget".Equals(element.Name))
                {
                    int x0 = int.Parse(element.GetAttribute("x"));
                    int y0 = int.Parse(element.GetAttribute("y"));

                    widgets.Add(new DungeonWidget(new Vector2Int(x0, y0),
                        element.GetAttribute("file"),
                        element.GetAttribute("info")));

                }
                if ("npc".Equals(element.Name))
                {
                    int x0 = int.Parse(element.GetAttribute("x"));
                    int y0 = int.Parse(element.GetAttribute("y"));
                    spawns.Add(new DungeonSpawn(new Vector2Int(x0, y0), int.Parse(element.GetAttribute("npc"))));
                }
                if ("edge".Equals(element.Name))
                {
                    ProcessEdge(element, edges);
                }
            }

        }
        return new DungeonBlock(grid, edges, widgets, spawns);
    }

    private static short[] ProcessEdge(XmlElement element, short[] edges)
    {
        string side = element.GetAttribute("side");
        if ("NORTH".Equals(side))
        {
            edges[0] = short.Parse(element.GetAttribute("value"));
        }
        if ("EAST".Equals(side))
        {
            edges[1] = short.Parse(element.GetAttribute("value"));
        }
        if ("SOUTH".Equals(side))
        {
            edges[2] = short.Parse(element.GetAttribute("value"));
        }
        if ("WEST".Equals(side))
        {
            edges[3] = short.Parse(element.GetAttribute("value"));
        }
        return edges;
    }

    private static KeyBlock LoadKeyBlock(XmlElement root)
    {
        int heat = 0;
        int.TryParse(root.GetAttribute("heat"), out heat);
        Vector2Int p=new Vector2Int(-1,-1);
        int x = -1, y = -1;
        int.TryParse(root.GetAttribute("x"), out x);
        if (int.TryParse(root.GetAttribute("y"), out y))
        {
            p = new Vector2Int(x, y);
        }
        short [] edges=new short[4];// = new short[4];
        List<DungeonWidget> widgets = new List<DungeonWidget>();
        List<DungeonSpawn> spawns = new List<DungeonSpawn>();
        int[][] grid = new int[8][];
        for (int i = 0; i < 8; i++)
        {
            grid[i] = new int[8];
        }
        int index = 0;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (XmlNodeType.Element.Equals(root.ChildNodes[i].NodeType))
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("row".Equals(element.Name))
                {
                    string textContents = element.InnerText.Replace(" ", "");
                    char[] charArray = textContents.ToCharArray();
                    for (int j = 0; j < 8; j++)
                    {
                        grid[index][j] = System.Uri.FromHex(charArray[j]);
                    }
                    index++;
                }
                if ("widget".Equals(element.Name))
                {
                    int x0 = int.Parse(element.GetAttribute("x"));
                    int y0 = int.Parse(element.GetAttribute("y"));

                    widgets.Add(new DungeonWidget(new Vector2Int(x0, y0),
                        element.GetAttribute("file"),
                        element.GetAttribute("info")));

                }
                if ("npc".Equals(element.Name))
                {
                    int x0 = int.Parse(element.GetAttribute("x"));
                    int y0 = int.Parse(element.GetAttribute("y"));
                    spawns.Add(new DungeonSpawn(new Vector2Int(x0, y0), int.Parse(element.GetAttribute("npc"))));
                }
                if ("edge".Equals(element.Name))
                {
                    ProcessEdge(element, edges);
                }
            }
            
        }
        return new KeyBlock(p, heat, grid, edges, widgets,spawns);
    }
}
