﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBlock : DungeonBlock
{
    Vector2Int position;
    int heat;
    bool placed;

    public KeyBlock(Vector2Int position, int heat, int [][] grid, short [] edges, List<DungeonWidget> widgets, List<DungeonSpawn> npcs) : base(grid,edges,widgets,npcs)
    {
        placed = false;
        this.position = position;
        this.heat = heat;
    }

    public bool isFixed()
    {
        return position.x >= 0;
    }

    public bool isPlaced()
    {
        return placed;
    }

    public void SetPlaced(bool value=true)
    {
        placed = value;
    }

    public Vector2Int GetPosition()
    {
        return position;
    }

    public int GetHeat()
    {
        return heat;
    }
}
