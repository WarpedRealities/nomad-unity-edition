﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class BlockDungeonBlockData
{
    public int tileIndex;
    public int heat;
}

public class BlockDungeonTool
{
    private Tile[][] tiles;
    private bool[][] grid;
    private TileTools tileTools;
    private ActorTools actorTools;
    private PointsOfInterest pointsOfInterest;
    private List<KeyBlock> keyBlocks;
    private List<DungeonBlock> blocks;
    private int extent;
    private DungeonWidgetBuilder widgetBuilder;
    private List<BlockEntry> placedBlocks;
    private DungeonNewBlockTool dungeonNewBlockTool;
    private Dictionary<string, LootTableTool> lootGenerators;
    private Dictionary<int, string> spawns;
    public BlockDungeonTool(Tile[][] tiles, bool[][] grid, TileTools tileTools,ActorTools actorTools,  PointsOfInterest pointsOfInterest)
    {
        this.tiles = tiles;
        this.grid = grid;
        this.tileTools = tileTools;
        this.actorTools = actorTools;
        this.pointsOfInterest = pointsOfInterest;

        placedBlocks = new List<BlockEntry>();
    }

    public void LoadElements(int extent, List<KeyBlock> keyBlocks, List<DungeonBlock> blocks, Dictionary<string,LootTableTool> lootGenerators, Dictionary<int,string> spawn)
    {
        this.extent = extent;
        this.keyBlocks = keyBlocks;
        this.blocks = blocks;
        this.lootGenerators = lootGenerators;
        this.dungeonNewBlockTool = new DungeonNewBlockTool(blocks, keyBlocks);
        this.widgetBuilder = new DungeonWidgetBuilder(lootGenerators);
        this.spawns = spawn;
    }

    internal bool[][] Build()
    {
        BlockEntry[][] blockGrid = null;
        int e = 0;
        do
        {
            e++;
            placedBlocks.Clear();
            dungeonNewBlockTool.Reset();
            blockGrid = BuildEntryGrid(tiles.Length, tiles[0].Length);

            KeyBlocksFirstPass(blockGrid);

            int escape = 0;
            int placed = placedBlocks.Count;

            while (escape < extent * 16)
            {
                if (PlaceBlock(blockGrid))
                {
                    placed++;
                }

                escape++;
            }
        }
        while (e < 256 && !(placedBlocks.Count>= extent && dungeonNewBlockTool.AllKeyBlocksPlaced()));
        if (!dungeonNewBlockTool.AllKeyBlocksPlaced())
        {
            Debug.Log("not all keys placed "+dungeonNewBlockTool.GetKeyBlockCount());
            Debug.Log("key blocks required " + dungeonNewBlockTool.GetKeyCount());

        }
        ApplyBlocks(blockGrid);

        return grid;
    }

    private bool PlaceBlock(BlockEntry[][] blockGrid)
    {
        //find a placed block
        int r0 = placedBlocks.Count > 1 ? DiceRoller.GetInstance().RollDice(placedBlocks.Count) : 0;
        BlockEntry blockEntry = placedBlocks[r0];
        //can we build something around this block?
        int r1 = GetOpening(blockEntry);
        if (!BorderCheck(blockEntry.GetPosition(), r1, blockGrid.Length,blockGrid[0].Length))
        {
       //     Debug.Log("border check failed");
            return false;
        }

        //check that tile
        Vector2Int p = GeometryTools.GetPosition(blockEntry.GetPosition().x, blockEntry.GetPosition().y, r1 * 2);
        if (blockGrid[p.x][p.y] != null)
        {
     //       Debug.Log("block is already placed");
            return false;
        }
        //build an edge map so it matches its surroundings.
        short[] edgeMap = BuildEdgeMap(p, blockGrid);
        short calculatedHeat = GetHeat(p, blockGrid);
        //get a block that matches the edge map, a -1 is an any
        int block = dungeonNewBlockTool.GetNewBlock(edgeMap, calculatedHeat);
        if (block != 0)
        {
            short heat = block > 0 ? calculatedHeat : (short)0;
            BlockEntry newBlock = new BlockEntry(p, block > 0 ? blocks[block - 1] : keyBlocks[(block + 1) * -1], heat);
            blockGrid[p.x][p.y] = newBlock;
            placedBlocks.Add(newBlock);

            return true;
        }
        return false;
    }

    private short GetHeat(Vector2Int p, BlockEntry[][] blockGrid)
    {
        short heat = 99;
        for (int i = 0; i < 4; i++)
        {
            Vector2Int p0 = GeometryTools.GetPosition(p.x, p.y, i * 2);
            if (p0.x >= 0 && p.y >= 0 &&
                p0.x < grid.Length && p0.y < grid[0].Length)
            {
                if (this.ForbidEdges(p0.x,p0.y,blockGrid) && blockGrid[p0.x][p0.y] != null)
                {
                    short cHeat = blockGrid[p0.x][p0.y].GetHeat();
                    if (cHeat < heat)
                    {
                        heat = cHeat;
                    }
                }
               
            }
        }
        return ++heat;
    }

    private short[] BuildEdgeMap(Vector2Int p, BlockEntry[][] grid)
    {
        short[] edgeMap = new short[4];
        for (int i = 0; i < 4; i++)
        {
            Vector2Int p0 = GeometryTools.GetPosition(p.x, p.y, i * 2);
            if (p0.x >= 0 && p.y >= 0 &&
                p0.x<grid.Length && p0.y<grid[0].Length)
            {
                if (!ForbidEdges(p0.x,p0.y,grid)|| grid[p0.x][p0.y] == null)
                {
                    edgeMap[i] = -1;
                }
                else
                {
                    DungeonBlock block = grid[p0.x][p0.y].GetBlock();
                    int index = i < 2 ? i + 2 : i - 2;
                    edgeMap[i] = block.GetEdgeValues(index);
                }
            }
            else
            {
                edgeMap[i] = -1;
            }
        }
        return edgeMap;
    }

    private bool ForbidEdges(int x, int y, BlockEntry[][] grid)
    {
        if (x<0 || y < 0)
        {
            return false;
        }
        if (x >= grid.Length || y >= grid[0].Length)
        {
            return false;
        }
        return true;
    }

    private DungeonBlock GetBlock(int v)
    {
        if (v > 0)
        {
            return blocks[v - 1];
        }
        if (v < 0)
        {
            return keyBlocks[(v + 1) * -1];
        }
        return null;
    }

    private bool BorderCheck(Vector2Int p, int r1, int width, int height)
    {
        //check if we're off the edges
        if (r1==2 && p.y == 0)
        {
            return false;
        }
        if (r1 == 3 && p.x == 0)
        {
            return false;
        }
        if (r1 == 0 && p.y == height-1)
        {
            return false;
        }
        if (r1 == 1 && p.x == width-1)
        {
            return false;
        }
        //check if its on the grid
        if (!GridCheck(GeometryTools.GetPosition(p.x, p.y, r1 * 2)))
        {
            return false;
        }

        return true;
    }

    private bool GridCheck(Vector2Int vector2Int)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (OutOfGrid(i + (vector2Int.x * 8), j + (vector2Int.y * 8)) ||
                    !this.grid[i + (vector2Int.x * 8)][j + (vector2Int.y * 8)])
                {
                    return false;
                }
            }
        }
        return true;
    }

    private bool OutOfGrid(int x, int y)
    {
        if (x < 0 || y < 0)
        {
            return true;
        }
        if (x>=grid.Length || y >= grid[0].Length)
        {
            return true;
        }
        return false;
    }
    private int GetOpening(BlockEntry blockEntry)
    {
        int r = DiceRoller.GetInstance().RollDice(4);
        if (blockEntry.GetEdges()[r] > 0)
        {
            return r;
        }
        else
        {
            while (blockEntry.GetEdges()[r] == 0)
            {
                r++;
                if (r >= 4)
                {
                    r = 0;
                }
            }
            return r;

        }
    }

    void KeyBlocksFirstPass(BlockEntry [][] blockGrid)
    {
        for (int i = 0; i < keyBlocks.Count; i++)
        {
            if (keyBlocks[i].isFixed())
            {
                BlockEntry blockEntry = new BlockEntry(keyBlocks[i].GetPosition(), keyBlocks[i], 0);
                blockGrid[keyBlocks[i].GetPosition().x][keyBlocks[i].GetPosition().y] = blockEntry;
                placedBlocks.Add(blockEntry);
                keyBlocks[i].SetPlaced();
            }
        }
    }

    void ApplyBlocks(BlockEntry[][] blockGrid)
    {
        for (int i = 0; i < blockGrid.Length; i++)
        {
            for (int j = 0; j < blockGrid[0].Length; j++)
            {
                if (blockGrid[i][j] != null)
                {
                    blockGrid[i][j].GetBlock().Apply(tileTools,actorTools, tiles, widgetBuilder,spawns, i * 8, j * 8);
                }
            }
        }
    }

    internal BlockEntry[][] BuildEntryGrid(int width, int height)
    {
        BlockEntry[][] blockGrid = new BlockEntry[width][];
        for (int i = 0; i < width; i++)
        {
            blockGrid[i] = new BlockEntry[height];
            for (int j = 0; j < height; j++)
            {
                blockGrid[i][j]=null;
            }
        }
        return blockGrid;
    }
}
