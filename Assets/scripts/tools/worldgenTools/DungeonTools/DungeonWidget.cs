﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonWidget
{
    Vector2Int position;
    string widgetFile, info;


    public DungeonWidget(Vector2Int p, string file, string info)
    {
        this.position = p;
        this.widgetFile = file;
        this.info = info;

    }

    public Vector2Int GetPosition()
    {
        return position;
    }

    public string GetFile()
    {
        return widgetFile;
    }


    public string GetInfo()
    {
        return info;
    }
}
