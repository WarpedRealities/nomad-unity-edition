﻿using UnityEngine;
using System.Collections;

public class BlockEntry 
{
    Vector2Int position;
    short[] edges;
    short edgeCount;
    short heat;
    private Vector2Int p;
    private DungeonBlock dungeonBlock;

    private void CopyEdges(DungeonBlock block)
    {
        edgeCount = 0;
        this.edges = new short[4];
        for (int i = 0; i < 4; i++)
        {
            this.edges[i] = block.GetEdgeValues(i);
            if (this.edges[i] > 0)
            {
                edgeCount++;
            }
        }
    }

    public BlockEntry(int x, int y, DungeonBlock block, short heat)
    {
        position = new Vector2Int(x, y);
        CopyEdges(block);
        this.heat = heat;
        this.dungeonBlock = block;

    }
    public BlockEntry(Vector2Int p, DungeonBlock block, short heat)
    {
        position = new Vector2Int(p.x, p.y);
        CopyEdges(block);
        this.heat = heat;
        this.dungeonBlock = block;
    }


    public Vector2Int GetPosition()
    {
        return position;
    }

    public short[] GetEdges()
    {
        return edges;
    }

    public short GetEdgeCount()
    {
        return edgeCount;
    }

    public short GetHeat()
    {
        return heat;
    }

    public DungeonBlock GetBlock()
    {
        return dungeonBlock;
    }
   
}
