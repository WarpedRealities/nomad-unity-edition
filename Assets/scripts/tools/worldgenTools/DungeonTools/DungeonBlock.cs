﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonSpawn
{
    public int index;
    public Vector2Int position;
    public DungeonSpawn(Vector2Int p, int index)
    {
        this.position = p;
        this.index = index;
    }
}

public class DungeonBlock 
{
    private short[] edgeValues;

    private int[][] blockGrid;


    private List<DungeonWidget> dungeonWidgets;
    private List<DungeonSpawn> dungeonNPCs;

    public DungeonBlock(int[][] grid, short[] edges, List<DungeonWidget> widgets, List<DungeonSpawn> npcs)
    {
        edgeValues = edges;
        blockGrid = grid;
        dungeonWidgets = widgets;
        dungeonNPCs = npcs;

    }

    public short GetEdgeValues(int index)
    {
        return edgeValues[index];
    }

    public void Apply(TileTools tileTools,ActorTools actorTools, Tile[][] tiles, DungeonWidgetBuilder widgetBuilder, Dictionary<int, string> spawns, int x, int y)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (blockGrid[i][j] > 0)
                {
                    tileTools.PaintTile(tiles, x + j, i + y, blockGrid[i][j]);
                }
            }
        }
        for (int i = 0; i < dungeonWidgets.Count; i++)
        {
            Widget widget = widgetBuilder.BuildWidget(dungeonWidgets[i].GetFile(), dungeonWidgets[i].GetInfo());
            if (widget != null)
            {
                tiles[x + dungeonWidgets[i].GetPosition().x][y + dungeonWidgets[i].GetPosition().y].SetWidget(widget);
            }
        }
        for (int i=0;i < dungeonNPCs.Count; i++)
        {
            string filename =  spawns[dungeonNPCs[i].index];
            NPC npc = new NPC(ActorTools.loadNPC(filename), filename);
            Vector2Int p = new Vector2Int(dungeonNPCs[i].position.x, dungeonNPCs[i].position.y);
            actorTools.GetActors().Add(new NPC(npc, p + new Vector2Int(x,y)));
        }
    }

}
