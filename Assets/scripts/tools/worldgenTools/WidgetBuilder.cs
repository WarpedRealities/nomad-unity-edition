﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

public class WidgetBuilder
{
    private static readonly string FILEPREFIX = "widgets/";


    public static Widget BuildWidget(string file)
    {
        if ("DAMAGE".Equals(file))
        {
            return new WidgetDamage(0);
        }
        if ("ITEMPILE".Equals(file))
        {
            return new WidgetItemPile();
        }
        XmlDocument xmlDocument = FileTools.GetXmlDocument(FILEPREFIX + file);
        XmlElement element = (XmlElement)xmlDocument.FirstChild.NextSibling;
        if ("widgetHarvestable".Equals(element.Name))
        {
            return BuildWidgetHarvestable(element);
        }
        if ("widgetBreakable".Equals(element.Name))
        {
            return BuildWidgetBreakable(element);
        }
        if ("widgetContainer".Equals(element.Name)) {
            return BuildWidgetContainer(element);
        }
        if ("widgetStash".Equals(element.Name))
        {
            return BuildWidgetStash(element);
        }
        if ("widgetConversation".Equals(element.Name))
        {
            return BuildWidgetConversation(element);
        }
        if ("widgetTalkPoint".Equals(element.Name))
        {
            return BuildWidgetTalkPoint(element);
        }
        if ("widgetSystem".Equals(element.Name))
        {
            return BuildShipSystem(element);
        }
        if ("widgetWorkBench".Equals(element.Name))
        {
            return BuildWorkBench(element);
        }
        if ("widgetShipControl".Equals(element.Name))
        {
            return BuildShipControl(element);
        }
        if ("widgetDoor".Equals(element.Name))
        {
            return BuildDoor(element);
        }
        if ("widgetBarrier".Equals(element.Name))
        {
            return BuildBarrier(element);
        }
        if ("widgetReformer".Equals(element.Name))
        {
            return BuildReformer(element);
        }
        if ("widgetTrap".Equals(element.Name))
        {
            return BuildTrap(element);
        }
        if ("widgetAccommodation".Equals(element.Name))
        {
            return BuildAccommodation(element);
        }
        if ("widgetResearchComputer".Equals(element.Name))
        {
            return BuildResearchComputer(element);
        }
        if ("widgetJail".Equals(element.Name))
        {
            return BuildJail(element);
        }
        Debug.Log("returning null");
        return null;
    }


    private static Widget BuildWidgetStash(XmlElement root)
    {
        BreakableData breakableData = null;
        ContainerData containerData = null;
        string name = root.GetAttribute("name");
        string description = null, revealDesc = null;
        int difficulty = int.Parse(root.GetAttribute("difficulty"));
        int sprite = int.Parse(root.GetAttribute("sprite"));
        bool blocksVision = "true".Equals(root.GetAttribute("blocksVision"));

        XmlNodeList children = root.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("revealDescription".Equals(e.Name))
                {
                    revealDesc = e.InnerText;
                }
                if ("containerData".Equals(e.Name))
                {
                    int openSprite = -1;
                    int.TryParse(e.GetAttribute("openSprite"), out openSprite);
                    int capacity = int.Parse(e.GetAttribute("capacity"));
                    containerData = new ContainerData(capacity, openSprite);
                }
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }
        return new WidgetStash(difficulty, revealDesc, sprite, description, name, containerData, breakableData, blocksVision);
    }

    private static Widget BuildJail(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int capacity = int.Parse(root.GetAttribute("capacity"));
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        WidgetJail widgetJail = new WidgetJail(sprite, description, name, breakableData);
        widgetJail.SetCaptives(capacity);
        return widgetJail;
    }

    private static Widget BuildAccommodation(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int capacity = int.Parse(root.GetAttribute("capacity"));
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        return new WidgetAccommodation(sprite, description, name, breakableData, capacity);
    }

    private static Widget BuildResearchComputer(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        return new WidgetResearchComputer(sprite, description, name, breakableData);
    }

    private static Widget BuildTrap(XmlElement root)
    {
        string name = root.GetAttribute("name");
        string description = null, revealDesc = null;
        int difficulty = int.Parse(root.GetAttribute("difficulty"));
        int disarmCheck = int.Parse(root.GetAttribute("disarm"));

        TriggerResults triggerResults = null;
        XmlNodeList children = root.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("revealDescription".Equals(e.Name))
                {
                    revealDesc = e.InnerText;
                }
                if ("results".Equals(e.Name))
                {
                    triggerResults = new TriggerResults(int.Parse(e.GetAttribute("range")),
                        "true".Equals(e.GetAttribute("cardinal")),
                        EnumTools.strToTrapType(e.GetAttribute("type")));
                }
            }
        }
        return new WidgetTrapSensor(difficulty, disarmCheck, revealDesc, description, triggerResults);
    }

    private static Widget BuildReformer(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        return new WidgetReformer(sprite, description, name, breakableData);
    }

    private static Widget BuildBarrier(XmlElement element)
    {
        string description = element.InnerText;
        int sprite = int.Parse(element.GetAttribute("sprite"));

        return new WidgetBarrier(sprite, description);
    }

    private static Widget BuildDoor(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;
        int lockStrength = 0;
        string lockKey=null;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        return new WidgetDoor(lockStrength,lockKey, sprite, description, name, breakableData);
    }

    private static Widget BuildShipControl(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        return new WidgetShipControl(sprite, description, name, breakableData);
    }

    private static Widget BuildWidgetBreakable(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        bool blockVision = "true".Equals(root.GetAttribute("blocksVision"));
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        return new Widget_Breakable(sprite, description, name, breakableData, blockVision);
    }

    private static Widget BuildWorkBench(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        return new WidgetWorkBench(sprite, description, name, breakableData);
    }

    private static Widget BuildShipSystem(XmlElement root)
    {
        BreakableData breakableData = null;
        ShipSystem[] systems = null;
        RepairRequirement repairRequirement = null;
        string name = root.GetAttribute("name");
        string description = null;
        bool damaged = false;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }
                if ("systems".Equals(e.Name))
                {
                    systems = ShipSystemBuilder.BuildSystems(e);
                }
                if ("repairRequirement".Equals(e.Name))
                {
                    repairRequirement = new RepairRequirement(int.Parse(e.GetAttribute("count")), e.GetAttribute("itemCode"));
                }
                if ("damaged".Equals(e.Name))
                {
                    damaged = true;
                }
            }
        }
        WidgetShipSystem widget = new WidgetShipSystem(sprite, description, name, breakableData,systems, repairRequirement);
        widget.SetDamaged(damaged);
        return widget;
    }

    private static Widget BuildWidgetConversation(XmlElement root)
    {
        BreakableData breakableData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }

            }
        }

        return new WidgetConversation(sprite, description, name, breakableData);
    }

    private static Widget BuildWidgetTalkPoint(XmlElement root)
    {
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }             
            }
        }

        return new WidgetTalkPoint(sprite, description, name);
    }
    private static WidgetContainer BuildWidgetContainer(XmlElement root)
    {
        BreakableData breakableData = null;
        ContainerData containerData = null;
        string name = root.GetAttribute("name");
        string description = null;
        int sprite = int.Parse(root.GetAttribute("sprite"));
        bool blocksVision = "true".Equals(root.GetAttribute("blocksVision"));
        XmlNodeList children = root.ChildNodes;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("breakableData".Equals(e.Name))
                {
                    string item = null;
                    int health = int.Parse(e.GetAttribute("health"));
                    int max = 0, min = 0, sSoak = 0, tSoak = 0, kSoak = 0;
                    if (int.TryParse(e.GetAttribute("max"), out max))
                    {
                        min = int.Parse(e.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(e.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = e.GetAttribute("item");
                    }
                    int.TryParse(e.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(e.GetAttribute("thermal"), out tSoak);
                    int.TryParse(e.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }
                if ("containerData".Equals(e.Name))
                {
                    int openSprite = -1;
                    int.TryParse(e.GetAttribute("openSprite"), out openSprite);
                    int capacity = int.Parse(e.GetAttribute("capacity"));
                    containerData = new ContainerData(capacity, openSprite);
                }
            }
        }

        return new WidgetContainer(sprite, description, name, containerData, breakableData, blocksVision);
    }

    public static WidgetHarvestable BuildWidgetHarvestable(XmlElement element)
    {     
        XmlNodeList children = element.ChildNodes;
        HarvestableData harvestData = null;
        string name = element.GetAttribute("name");
        int readySprite = int.Parse(element.GetAttribute("ready"));
        int harvest = int.Parse(element.GetAttribute("harvested"));
        string description = null;
        bool walkable = "true".Equals(element.GetAttribute("walkable"));
        long regenTime = 0;
        string harvestSuccess = null;
        string noHarvest = null;
        string harvestComplete = null;
        int DC = 0;
        int max = 0;
        string itemCode = null;
        int min = 0;

        for (int i=0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];

                if ("description".Equals(e.Name))
                {
                    description = e.InnerText;
                }
                if ("harvestSuccess".Equals(e.Name))
                {
                    harvestSuccess = e.InnerText;    
                }
                if ("harvestComplete".Equals(e.Name))
                {
                    harvestComplete = e.InnerText;
                }
                if ("noHarvest".Equals(e.Name))
                {
                    noHarvest = e.InnerText;
                }
                if ("harvestData".Equals(e.Name))
                {
                    itemCode = e.GetAttribute("item");
                    min = int.Parse(e.GetAttribute("min"));
                    max = int.Parse(e.GetAttribute("max"));
                    DC = int.Parse(e.GetAttribute("DC"));
                    regenTime = int.Parse(e.GetAttribute("regenTime"));
                }
            }
        }
        harvestData = new HarvestableData(itemCode, min, max, DC, harvestComplete, noHarvest, harvestSuccess, regenTime);
        return new WidgetHarvestable(name, readySprite, harvest, description, walkable, harvestData);
    }
}
