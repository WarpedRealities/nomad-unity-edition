﻿using UnityEngine;
using System.Collections;
using System;

public class ShipConnectorTool
{

    internal static void Disconnect(Zone zone1, Zone zone2)
    {
        DisconnectZone(zone1, zone2.getName());
        DisconnectZone(zone2, zone1.getName());
    }

    internal static void Connect(Zone zone1, Zone zone2, bool primaryOnly=false)
    {

        ConnectZone(zone1, zone2.getName(),primaryOnly);
        ConnectZone(zone2, zone1.getName(),primaryOnly);
    }

    public static bool IsShipHatch(int value)
    {
        Debug.Log("value is ship hatch " + value);
        return (value < -10 && value > -15);
    }

    internal static void DisconnectZone(Zone zone, string destination)
    {
        ZoneContents zoneContents = zone.GetContents();
        for (int i = 0; i < zoneContents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zoneContents.GetZoneParameters().GetHeight(); j++)
            {
                if (zoneContents.getTiles()[i][j] != null &&
                    zoneContents.getTiles()[i][j].GetWidget() != null &&
                    zoneContents.getTiles()[i][j].GetWidget() is WidgetPortal)
                {
                    WidgetPortal p = (WidgetPortal)zoneContents.getTiles()[i][j].GetWidget();

                    if (IsShipHatch(p.getID()))
                    {
                        p.SetDestination(null);
                    }
                }
            }
        }
    }

    internal static void RemovePassengers(int count, Zone shipZone, Zone hostZone)
    {
        for (int i = shipZone.GetContents().GetActors().Count-1; i >= 0; i--)
        {
            if (shipZone.GetContents().GetActors()[i] != null &&
            shipZone.GetContents().GetActors()[i].GetAlive() &&
            shipZone.GetContents().GetActors()[i] is NPC)
            {
                Actor actor = shipZone.GetContents().GetActors()[i];
                //remove from current zone
                shipZone.GetContents().GetTile(actor.GetPosition().x, actor.GetPosition().y).SetActor(null);
                shipZone.GetContents().GetActors().Remove(actor);
                //find a spot to spawn them in next to the portal
                Vector2Int initialPos = FindPortal(hostZone,-11);
                WidgetPortal portal = (WidgetPortal)hostZone.GetContents().GetTile(initialPos.x, initialPos.y).GetWidget();
                Vector2Int offsetPos = GeometryTools.GetPosition(initialPos.x, initialPos.y, portal.getFacing());
                Vector2Int resultPos = new Vector2Int(initialPos.x, initialPos.y);
                int r = DiceRoller.GetInstance().RollDice(8);
                for (int j = 0; j < 8; j++)
                {
                    int d = r + j >= 8 ? r + j - 8 : r+j;
                    Vector2Int finalPos = GeometryTools.GetPosition(offsetPos.x, offsetPos.y, d);
                    Tile t = hostZone.GetContents().GetTile(finalPos.x, finalPos.y);
                    if (t!=null && t.canWalk() && t.GetWidget() == null)
                    {
                        resultPos = finalPos;
                    }
                }
                if (actor.GetUID().Equals(GlobalGameState.GetInstance().GetPlayer().GetCompanionUID()))
                {
                    GlobalGameState.GetInstance().GetPlayer().SetCompanion(-1);
                    ((NPC)actor).SetIsCompanion(false);
                }
                actor.SetPosition(resultPos);
                hostZone.GetContents().GetActors().Add(actor);
                count--;
                if (count <= 0)
                {
                    return;
                }
            }
        }
    }

    internal static Vector2Int FindPortal(Zone zone, int uid)
    {
        ZoneContents zoneContents = zone.GetContents();
        for (int i = 0; i < zoneContents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zoneContents.GetZoneParameters().GetHeight(); j++)
            {
                if (zoneContents.getTiles()[i][j] != null &&
                    zoneContents.getTiles()[i][j].GetWidget() != null &&
                    zoneContents.getTiles()[i][j].GetWidget() is WidgetPortal)
                {
                    WidgetPortal p = (WidgetPortal)zoneContents.getTiles()[i][j].GetWidget();

                    if (p.getID() == uid)
                    {
                        return new Vector2Int(i, j);
                    }
                }
            }
        }
        return new Vector2Int(-1, -1);
    }

    internal static void ConnectZone(Zone zone, string destination, bool primaryOnly)
    {
        ZoneContents zoneContents = zone.GetContents();
        for (int i = 0; i < zoneContents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zoneContents.GetZoneParameters().GetHeight(); j++)
            {
                if (zoneContents.getTiles()[i][j] != null &&
                    zoneContents.getTiles()[i][j].GetWidget() != null &&
                    zoneContents.getTiles()[i][j].GetWidget() is WidgetPortal)
                {
                    WidgetPortal p = (WidgetPortal)zoneContents.getTiles()[i][j].GetWidget();

                    if ((primaryOnly && p.getID()==-11) || (!primaryOnly && IsShipHatch(p.getID())))
                    {

                        p.SetDestination(destination);
                    }
                }
            }
        }
        return;

    }
}
