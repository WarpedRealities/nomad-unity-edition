﻿using System;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class ActorTools
{
    private List<Actor> actors;
    private System.Random random;
    public ActorTools(List<Actor> actors)
    {
        this.random = new System.Random();
        this.actors = actors;
    }

    public List<Actor> GetActors()
    {
        return actors;
    }

    public void seedNPC(XmlElement element, Tile[][] tiles, bool[][] grid)
    {
        int min = int.Parse(element.GetAttribute("min"));
        int max = int.Parse(element.GetAttribute("max"));
        int minDistance = 0;
        int.TryParse(element.GetAttribute("minDistance"), out minDistance);
        int count = min == max ? max : min + (random.Next() % (max - min));
        string filename = element.GetAttribute("npc");
        NPC npc = new NPC(loadNPC(filename), filename);
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                if ("scripts".Equals(e.Name))
                {
                    npc.SetScriptHandler(new NpcScriptHandler(e.GetAttribute("spawn"), e.GetAttribute("death")));
                }
            }
        }
        List<Vector2Int> posList = new List<Vector2Int>();
        for (int i = 0; i < count; i++)
        {
            Vector2Int p = new Vector2Int(random.Next() % tiles.Length, random.Next() % tiles[0].Length);
            if (minDistance == 0)
            {
                while (!CanPlace(p, tiles, grid))
                {
                    p = new Vector2Int(random.Next() % tiles.Length, random.Next() % tiles[0].Length);
                }
            }
            else
            {
                while (!CanPlace(p, tiles, grid) || (!DistanceCheck(p, posList, minDistance)))
                {
                    p = new Vector2Int(random.Next() % tiles.Length, random.Next() % tiles[0].Length);
                }
            }
           
            posList.Add(p);
            actors.Add(new NPC(npc,p));

        }

    }

    private bool DistanceCheck(Vector2Int p, List<Vector2Int> posList,int minDistance)
    {
        for (int i = 0; i < posList.Count; i++)
        {
            if (Vector2Int.Distance(p, posList[i]) < minDistance)
            {
                return false;
            }
        }
        return true;
    }

    private bool CanPlace(Vector2Int p, Tile[][] tiles, bool[][] grid)
    {
        if (!grid[p.x][p.y] || tiles[p.x][p.y] == null || !tiles[p.x][p.y].canWalk())
        {
            return false;
        }
        for (int i = 0; i < actors.Count; i++)
        {
            if (p.x == actors[i].GetPosition().x && p.y == actors[i].GetPosition().y)
            {
                return false;
            }
        }
        return true;
    }

    static public XmlElement loadNPC(string filename)
    {
        XmlDocument doc = FileTools.GetXmlDocument("npcs/" + filename);
        XmlElement rootXML = (XmlElement)doc.FirstChild.NextSibling;
        return rootXML;
    }

    public void placeNPC(XmlElement element, Tile[][] tiles, bool[][] grid, Vector2Int offset)
    {

        Vector2Int p = new Vector2Int(int.Parse(element.GetAttribute("x")), int.Parse(element.GetAttribute("y")));
        string filename = element.GetAttribute("npc");

        NPC npc = new NPC(loadNPC(filename), filename);
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                if ("scripts".Equals(e.Name))
                {
                    npc.SetScriptHandler(new NpcScriptHandler(e.GetAttribute("spawn"), e.GetAttribute("death")));
                }
            }
        }
        actors.Add(new NPC(npc, p+offset));
    }
}