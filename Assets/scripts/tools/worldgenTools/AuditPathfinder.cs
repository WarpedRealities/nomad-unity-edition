﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AuditPathfinder : Pathfinder
{
    int pathTile, overwriteTile;
    bool crossVoid;

    public AuditPathfinder(int width, int height, Tile[][] tiles, bool noDiagonals) : base (width, height, tiles, noDiagonals)
    {

    }

    public void SetupAudit(bool crossVoid, int path, int overwrite)
    {
        this.crossVoid = crossVoid;
        this.pathTile = path;
        this.overwriteTile = overwrite;
    }

    protected override void UpdateOpenList(Path currentNode, Vector2Int target, TileMovement tileMovement)
    {
        if (forbidDiagonals)
        {
            for (int i = 0; i < 4; i++)
            {

                Vector2Int p = GeometryTools.GetPosition(currentNode.position.x, currentNode.position.y, i*2);

                if (BoundsCheck(p.x, p.y) && GridCheck(p.x, p.y) && CheckTile(p.x, p.y))
                {
                    int cost = BuildCost(p.x, p.y);
                    openList.Add(BuildPathNode(p, target, currentNode, i, cost));
                    grid[p.x][p.y] = true;
                }
            }
        }
        else
        {
            for (int i = 0; i < 8; i++)
            {

                Vector2Int p = GeometryTools.GetPosition(currentNode.position.x, currentNode.position.y, i);

                if (BoundsCheck(p.x, p.y) && GridCheck(p.x, p.y) && CheckTile(p.x, p.y))
                {
                    openList.Add(BuildPathNode(p, target, currentNode, i));
                    grid[p.x][p.y] = true;
                }
            }
        }

    }

    private int BuildCost(int x, int y)
    {
        Tile t = tiles[x][y];
        if (t == null)
        {
            return 2;
        }

        if (t.getDefinition().GetIndex() == pathTile)
        {
            return 1;
        }
        if (t.getDefinition().GetIndex() == overwriteTile)
        {
            return 2;
        }
        if (t.canWalk())
        {
            return 1;
        }


        return 1;
    }

    private bool BoundsCheck(int x, int y)
    {
        if (x<0 || x >= width)
        {
            return false;
        }
        if (y<0 || y >= height)
        {
            return false;
        }
        return true;
    }

    private bool CheckTile(int x, int y)
    {
        Tile t = tiles[x][y];
        if (t == null)
        {
            return this.crossVoid;
        }

        if (t.getDefinition().GetIndex() == pathTile)
        {
            return true;
        }
        if (t.getDefinition().GetIndex() == overwriteTile)
        {

            return true;
        }
        if (t.canWalk())
        {
;
            return true;
        }

        return false;
    }
}
