﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class WorldGenTools
{
    private TileTools tileTools;
    private WidgetTools widgetTools;
    private PerlinTools perlinTools;
    private ActorTools actorTools;
    private Entity entity;
    private PregenData pregenData;
    private PointsOfInterest pointsOfInterest;
    private List<Actor> actors;
    public WorldGenTools(ZoneTileset tileset, PregenData pregenData, Entity entity)
    {
        actors = new List<Actor>();
        this.pointsOfInterest = new PointsOfInterest();
        this.widgetTools = new WidgetTools();
        this.tileTools = new TileTools(tileset);
        this.perlinTools = new PerlinTools();
        this.actorTools = new ActorTools(actors);
        this.entity = entity;
        this.pregenData = pregenData;
    }
    private Tile[][] GenerateTiles(ZoneParameters zoneParameters)
    {
        Tile[][] tiles = new Tile[zoneParameters.GetWidth()][];
        for (int i=0;i<zoneParameters.GetWidth();i++)
        {
            tiles[i] = new Tile[zoneParameters.GetHeight()];
        }

        return tiles;
    }

    private bool[][] GenerateGrid(ZoneParameters zoneParameters)
    {
        bool[][] grid = new bool[zoneParameters.GetWidth()][];
        for (int i = 0; i < zoneParameters.GetWidth(); i++)
        {
            grid[i] = new bool[zoneParameters.GetHeight()];
            for (int j=0;j<zoneParameters.GetHeight();j++)
            {
                grid[i][j] = true;
            }
        }
        return grid;
    }

    public Tile[][] Run(XmlElement root, ZoneParameters zoneParameters, out List<Actor> actors, LandingSite landingSite, Vector2S fixedLandingSite) 
    {

        Tile[][] tiles = GenerateTiles(zoneParameters);
        bool[][] grid = GenerateGrid(zoneParameters);
        if (landingSite != null)
        {
            new LandingSiteTool(tiles, fixedLandingSite, zoneParameters,tileTools).Land(landingSite, ref grid,true);
        }
        tiles = TraverseElements(tiles, root,new Vector2Int(0,0), grid);
        tiles = postGeneration(tiles, zoneParameters);
        actors = this.actors;

        return tiles;
    }

    private Tile[][] postGeneration(Tile[][] tiles, ZoneParameters parameters)
    {
        for (int i = 0; i < parameters.GetWidth(); i++)
        {
           
            for (int j = 0; j < parameters.GetHeight(); j++)
            {
                if (tiles[i][j] != null)
                {
                    tiles[i][j].AwarenessPass(tiles);
                }
            }
        }
        return tiles;
    }

    public Tile[][] TraverseElements(Tile [][] tiles, XmlElement root, Vector2Int offset, bool [][] grid)
    {
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("prefab".Equals(element.Name))
                {
                    Vector2Int newOffset= tileTools.prefab(tiles, element, offset, grid);
                    TraverseElements(tiles, element, newOffset, grid);
                }
                if ("randprefab".Equals(element.Name))
                {
                    Vector2Int newOffset = tileTools.randprefab(tiles, element, offset, grid);
                    TraverseElements(tiles, element, newOffset, grid);
                }
                if ("registerPoint".Equals(element.Name))
                {
                    Vector2Int p = new Vector2Int(int.Parse(element.GetAttribute("x")) + offset.x, 
                        int.Parse(element.GetAttribute("y")) + offset.y);
                    pointsOfInterest.AddPointOfInterest(p);
                }
                if ("pregenEdgeConditional".Equals(element.Name))
                {
                    bool inverted= "true".Equals(element.GetAttribute("invert"));
                    int side = EnumTools.strToSide(element.GetAttribute("side"));
                    if (inverted)
                    {
                        if (pregenData == null)
                        {
                            TraverseElements(tiles, element, offset, grid);
                        }
                        else if (!pregenData.HasEdgeValues(side))
                        {
                            TraverseElements(tiles, element, offset, grid);
                        }
                    }
                    else
                    {
                        if (pregenData!=null && pregenData.HasEdgeValues(side))
                        {
                            TraverseElements(tiles, element, offset, grid);
                        }
                    }

                }
                if ("edgePrefab".Equals(element.Name))
                {
                    Debug.Log("edge prefab");
                    int side = EnumTools.strToSide(element.GetAttribute("side"));
                    Vector2Int newOffset = tileTools.edgePrefab(tiles, element, offset, grid, side);
                    TraverseElements(tiles, element, newOffset, grid);
                }
                if ("pregenPrefab".Equals(element.Name))
                {
                    int index = int.Parse(element.GetAttribute("index"));
                    string pregenFor = element.GetAttribute("zone");
                    Vector2Int newOffset;
                    if (pregenData != null)
                    {
                        pregenData.ReadPointValues();
                    }
                    if (pregenData!=null && pregenData.GetPointValues().Count > index)
                    {
                        newOffset = tileTools.prefab(tiles, element, offset, grid, pregenData.GetPointValues()[index].x, pregenData.GetPointValues()[index].y);
                    }
                    else
                    {
                        newOffset = tileTools.randprefab(tiles, element, offset, grid);
                        entity.GetZone(pregenFor).GetPregenData().GetPointValues().Add(new Vector2S(newOffset.x,newOffset.y));
                    }
                    Debug.Log("debug log " + newOffset);
                    TraverseElements(tiles, element, newOffset, grid);
                }
                if ("placeWidget".Equals(element.Name))
                {
                    this.widgetTools.PlaceWidget(tiles, element, offset, grid);
                }
                if ("paintTile".Equals(element.Name))
                {
                    int x = int.Parse(element.GetAttribute("x"));
                    int y = int.Parse(element.GetAttribute("y"));
                    int tile = int.Parse(element.GetAttribute("tile"));
                    this.tileTools.PaintTile(tiles, x+offset.x, y+offset.y, tile);
                }
                if ("seedWidgets".Equals(element.Name))
                {
                    this.widgetTools.SeedWidgets(tiles, element, offset, grid);
                }
                if ("widgetBreakable".Equals(element.Name))
                {
                    this.widgetTools.WidgetBreakable(tiles, element, offset, grid);
                }
                if ("widgetSlot".Equals(element.Name))
                {
                    this.widgetTools.WidgetSlot(tiles, element, offset, grid);
                }
                if ("widgetSprite".Equals(element.Name))
                {
                    this.widgetTools.WidgetSprite(tiles, element, grid, offset);
                }
                if ("widgetDescriber".Equals(element.Name))
                {
                    this.widgetTools.Describer(tiles, element, grid, offset);
                }
                if ("portal".Equals(element.Name))
                {
                    this.widgetTools.Portal(tiles, element, grid, offset);
                }
                if ("conditionalPortal".Equals(element.Name))
                {
                    this.widgetTools.ConditionalPortal(tiles, element, grid, offset);
                }
                if ("scriptedPortal".Equals(element.Name))
                {
                    this.widgetTools.ScriptedPortal(tiles, element, grid, offset);
                }
                if ("perlin".Equals(element.Name))
                {
                    this.Perlin(tiles, element, grid);
                }
                if ("floodFill".Equals(element.Name))
                {
                    this.tileTools.floodFill(tiles, element, grid);
                }
                if ("noise".Equals(element.Name))
                {
                    this.tileTools.Noise(tiles, element, grid);
                }
                if ("walloff".Equals(element.Name))
                {
                    this.tileTools.WallOff(tiles, element, grid);
                }
                if ("partition".Equals(element.Name))
                {
                    Partition(tiles, element, offset, grid);
                }
                if ("seedNPC".Equals(element.Name))
                {
                    this.actorTools.seedNPC(element, tiles, grid);
                }
                if ("placeNPC".Equals(element.Name))
                {
                    this.actorTools.placeNPC(element, tiles, grid,offset);
                }
                if ("placeItemPile".Equals(element.Name))
                {
                    this.widgetTools.PlaceItemPile(element, tiles, grid,offset);
                }
                if ("seedItemPiles".Equals(element.Name))
                {
                    this.widgetTools.SeedItemPile(element, tiles, grid);
                }
                if ("town".Equals(element.Name))
                {
                    TownBuilder townBuilder = new TownBuilder(tiles, grid, tileTools, pointsOfInterest);
                    bool [][]nugrid= townBuilder.BuildTown(element);
                    TraverseElements(tiles, element, offset, nugrid);
                }
                if ("cavegenerator".Equals(element.Name))
                {
                    Debug.Log("cave generator?");
                    int maxNodes = int.Parse(element.GetAttribute("maxNodes"));
                    int minNodes = int.Parse(element.GetAttribute("minNodes"));
                    int minSize = int.Parse(element.GetAttribute("minSize"));
                    int maxSize = int.Parse(element.GetAttribute("maxSize"));
                    int tile = int.Parse(element.GetAttribute("tile"));
                    CaveGenerator caveGenerator = new CaveGenerator(pointsOfInterest);
                    caveGenerator.GenerateNodes(maxNodes, minNodes, minSize, maxSize, grid);
                    caveGenerator.GenerateNeighbourLinks();
                    caveGenerator.JoinNeighbours(tiles.Length,tiles[0].Length,tiles, tileTools, grid, tile);

                }
                if ("AuditTool".Equals(element.Name))
                {
                    AuditTool auditTool = new AuditTool(tiles.Length, tiles[0].Length, tiles);
                    auditTool.Setup(actors, tiles, pointsOfInterest);
                    auditTool.AuditPaths(tileTools, tiles, 
                        int.Parse(element.GetAttribute("carve")), 
                        int.Parse(element.GetAttribute("replace")));
                }
                if ("readEdge".Equals(element.Name))
                {
                    tileTools.ReadEdges(tiles, element, entity);
                }
                if ("pregenEdge".Equals(element.Name))
                {
                    tileTools.PregenEdges(tiles, element, pregenData);
                }
                if ("AuditEdgeConnection".Equals(element.Name))
                {
                    if (pregenData != null)
                    {
                        AuditTool auditTool = new AuditTool(tiles.Length, tiles[0].Length, tiles);
                        int side = EnumTools.strToSide(element.GetAttribute("side"));
                        auditTool.Setup(actors, tiles, pregenData.GetEdgeValues(side), side, pointsOfInterest);
                        auditTool.AuditPaths(tileTools, tiles,
                            int.Parse(element.GetAttribute("carve")),
                            int.Parse(element.GetAttribute("replace")));
                    }
                }
                if ("cellularAutomata".Equals(element.Name))
                {
                    CellularAutomataTool cellularAutomataTool = new CellularAutomataTool(tiles.Length, tiles[0].Length);
                    do
                    {
                        cellularAutomataTool.InitialState(float.Parse(element.GetAttribute("proportion")), grid);
                        int iterations = int.Parse(element.GetAttribute("iterations"));
                        for (int j = 0; j < iterations; j++)
                        {
                            cellularAutomataTool.Iterate(grid, false);
                        }
                        cellularAutomataTool.Iterate(grid, true);
                    }
                    while (!cellularAutomataTool.FloodFillCheck(float.Parse(element.GetAttribute("minimumExpanse"))));
                    cellularAutomataTool.BuildWalls(grid, tiles, tileTools, int.Parse(element.GetAttribute("wall")));
                    bool[][] nugrid = cellularAutomataTool.GenerateGrid();
                    TraverseElements(tiles, element, offset, nugrid);
                }
                if ("rogueBuilder".Equals(element.Name))
                {
                    RogueBuilderTool rogueBuilderTool = new RogueBuilderTool(this.pointsOfInterest);
                    rogueBuilderTool.GenerateRooms(element.GetAttribute("split"), 
                        int.Parse(element.GetAttribute("splitPos")),
                        int.Parse(element.GetAttribute("min")),
                        int.Parse(element.GetAttribute("max")), 
                        int.Parse(element.GetAttribute("minSize")), 
                        int.Parse(element.GetAttribute("maxSize")),
                        grid);
                    rogueBuilderTool.GenerateNeighbours(int.Parse(element.GetAttribute("minConnect")), int.Parse(element.GetAttribute("maxConnect")));
                    rogueBuilderTool.GenerateHallways(int.Parse(element.GetAttribute("floor")),tiles,grid,tileTools);
                    if (element.GetAttribute("register").Equals("true"))
                    {
                        rogueBuilderTool.RegisterPoints();
                    }
                    TraverseElements(tiles, element, offset, rogueBuilderTool.GenerateGrid(grid));
                }

                if ("dungeon".Equals(element.Name))
                {
                    BlockDungeonTool blockDungeonTool = new BlockDungeonTool(tiles, grid, tileTools,actorTools, pointsOfInterest);
                    BlockDungeonRunner.Load(blockDungeonTool, element);
                    bool[][] nugrid = blockDungeonTool.Build();
                    TraverseElements(tiles, element, offset, nugrid);
                }
                if ("horizontalWallbreaker".Equals(element.Name))
                {
                    tileTools.HorizontalWallBreaker(element, grid,tiles);
                }
                if ("verticalWallbreaker".Equals(element.Name))
                {
                    tileTools.VerticalWallBreaker(element,grid,tiles);
                }
                if ("sealvoid".Equals(element.Name))
                {
                    tileTools.SealVoid(element, grid, tiles);
                }
                if ("randomTileReplace".Equals(element.Name))
                {
                    tileTools.RandomTileReplace(element, grid, tiles);
                }
                if ("POIWidget".Equals(element.Name))
                {
                    Vector2Int p= pointsOfInterest.NextPoint();
                    widgetTools.PlaceWidget(tiles, p.x, p.y, element, offset, grid);
                }
            }
        }
        return tiles;
    }

    public List<Actor> GetActors()
    {
        return actors;
    }

    private Tile[][] Partition(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid)
    {
        bool inverted = "true".Equals(root.GetAttribute("inverted"));
        int minX = int.Parse(root.GetAttribute("minX"));
        int minY = int.Parse(root.GetAttribute("minY"));
        int maxX = int.Parse(root.GetAttribute("maxX"));
        int maxY = int.Parse(root.GetAttribute("maxY"));
        bool[][] nugrid = new bool[grid.Length][];
        int tileCount = 0;
        for (int i = 0; i < grid.Length; i++)
        {
            nugrid[i] = new bool[grid[0].Length];
            for (int j = 0; j < grid[0].Length; j++)
            {
                if (grid[i][j] && i>=minX && i<=maxX && j>=minY && j <= maxY)
                {
                    if (!inverted)
                    {
                        nugrid[i][j] = true;
                        tileCount++;
                    }
                    else
                    {
                        nugrid[i][j] = false;
                    }
                }
                else
                {
                    if (!inverted)
                    {
                        nugrid[i][j] = false;
                    }
                    else
                    {
                        nugrid[i][j] = true;
                        tileCount++;
                    }
                }
            }
        }

        return TraverseElements(tiles, root, offset, nugrid);
    }

    private void Perlin(Tile[][] tiles, XmlElement root, bool[][] grid)
    {
        double[][] values = perlinTools.generatePerlin(tiles.Length, tiles[0].Length);
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType== XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("band".Equals(element.Name))
                {
                    float lower = float.Parse(element.GetAttribute("min"));
                    float upper = float.Parse(element.GetAttribute("max"));
                    TraverseElements(tiles, element, new Vector2Int(0, 0), perlinTools.generateGrid(grid, values, lower, upper));
                }
            }
        }
    }
}
