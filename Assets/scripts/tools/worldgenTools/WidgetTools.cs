﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class WidgetTools
{
    private System.Random random;
    private LootTableTool lootTable;

    public WidgetTools()
    {
        this.random = new System.Random();
    }

    public void PlaceWidget(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid)
    {
        Widget widget = SetupWidget(root);
        Vector2Int p = offset + new Vector2Int(int.Parse(root.GetAttribute("x")), int.Parse(root.GetAttribute("y")));

        tiles[p.x][p.y].SetWidget(widget);
    }

    public void PlaceWidget(Tile[][] tiles, int x, int y, XmlElement root, Vector2Int offset, bool[][] grid)
    {
        Widget widget = SetupWidget(root);
        Vector2Int p = offset + new Vector2Int(x+offset.x, y+offset.y);

        tiles[p.x][p.y].SetWidget(widget);
    }
    public Widget SetupWidget(XmlElement root)
    {

        Widget widget = WidgetBuilder.BuildWidget(root.GetAttribute("file"));

        if (widget is WidgetContainer || widget is WidgetStash)
        {        
            lootTable = new LootTableTool(LootTableBuilder.Build(root));
            WidgetContainer widgetContainer = (WidgetContainer)widget;
            widgetContainer.GetContainerData().SetItems(lootTable.GetLoot());
        }
        if (widget is WidgetConversation)
        {    
            WidgetConversation widgetConversation = (WidgetConversation)widget;
            widgetConversation.SetConversation(root.GetAttribute("conversation"));
        }
        if (widget is WidgetTalkPoint)
        {
            WidgetTalkPoint widgetTalkPoint = (WidgetTalkPoint)widget;
            widgetTalkPoint.SetConversation(root.GetAttribute("conversation"));
        }
        if (widget is WidgetShipSystem)
        {
            WidgetShipSystem widgetShipSystem = (WidgetShipSystem)widget;
            ShipSystemSetupTool.SetupSystem(widgetShipSystem,root);

            if ("true".Equals(root.GetAttribute("damaged"))) {
                widgetShipSystem.SetDamaged(true);
            }
        }
        
        if (widget is WidgetDamage)
        {
            WidgetDamage widgetDamage = (WidgetDamage)widget;
            widgetDamage.SetDamage(int.Parse(root.GetAttribute("value")));
        }
        if (widget is WidgetDoor)
        {
            WidgetDoor widgetDoor = (WidgetDoor)widget;
            widgetDoor.SetStrength(int.Parse(root.GetAttribute("strength")));
            widgetDoor.SetKey(root.GetAttribute("key"));
        }
        return widget;
    }

    public void SeedWidgets(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid)
    {
        int min = int.Parse(root.GetAttribute("min"));
        int max = int.Parse(root.GetAttribute("max"));
        int r = min + (min==max ? 0 : random.Next(max - min));

        Widget widget = WidgetBuilder.BuildWidget(root.GetAttribute("file"));

        int widgetMode = 0;
        if (widget is WidgetContainer)
        {
            widgetMode = 1;
            lootTable = new LootTableTool(LootTableBuilder.Build(root));
        }
        if (widget is WidgetConversation)
        {
            WidgetConversation widgetConversation = (WidgetConversation)widget;
            widgetConversation.SetConversation(root.GetAttribute("conversation"));
        }
        for (int i=0;i< r; i++)
        {
            Vector2Int p = new Vector2Int(random.Next() % tiles.Length, random.Next() % tiles[0].Length);
            while (!CanPlace(p.x,p.y, tiles, grid))
            {
                p = GetValidPosition(p, tiles, grid);
                //p = new Vector2Int(random.Next() % tiles.Length, random.Next() % tiles[0].Length);
            }
            //place widget
            Widget instance_widget = widget.Clone();
            switch (widgetMode) {
                case 1:
                    ((WidgetContainer)instance_widget).GetContainerData().SetItems(lootTable.GetLoot());
                    break;
                default:

                    break;
            }
            tiles[p.x][p.y].SetWidget(instance_widget);
        }
    }

    private Vector2Int GetValidPosition(Vector2Int p, Tile[][] tiles, bool[][] grid)
    {
        bool right = DiceRoller.GetInstance().RollOdds() > 0.5F;
        int x = p.x;
        while (x>0 && x < tiles.Length - 1)
        {
            x += right ? 1 : -1;
            if (CanPlace(x, p.y, tiles, grid))
            {
                return new Vector2Int(x, p.y);
            }
        }

        return new Vector2Int(random.Next() % tiles.Length, random.Next() % tiles[0].Length);
    }

    private bool CanPlace(int x, int y, Tile[][] tiles, bool[][] grid)
    {
        if (!grid[x][y] || tiles[x][y] == null || !tiles[x][y].canWalk() || tiles[x][y].GetWidget()!=null)
        {
            return false;
        }
       
        return true;
    }


    public void WidgetBreakable(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid)
    {
        int x = offset.x+ Int32.Parse(root.GetAttribute("x"));
        int y = offset.y+ Int32.Parse(root.GetAttribute("y"));
        string description = "";
        string name = root.GetAttribute("name");
        bool blocksVision = "true".Equals(root.GetAttribute("blocksVision"));
        BreakableData breakableData = null;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("description".Equals(element.Name))
                {
                    description = element.InnerText;
                }
                if ("breakableData".Equals(element.Name))
                {
                    string item = null;
                    int health = int.Parse(element.GetAttribute("health"));
                    int max = 0, min = 0, sSoak=0, tSoak=0, kSoak=0;
                    if (int.TryParse(element.GetAttribute("max"), out max))
                    {
                        min = int.Parse(element.GetAttribute("min"));
                    }
                    else
                    {
                        if (int.TryParse(element.GetAttribute("count"), out max))
                        {
                            min = max;
                        }
                    }
                    if (max > 0)
                    {
                        item = element.GetAttribute("item");
                    }
                    int.TryParse(element.GetAttribute("kinetic"), out kSoak);
                    int.TryParse(element.GetAttribute("thermal"), out tSoak);
                    int.TryParse(element.GetAttribute("shock"), out sSoak);

                    breakableData = new BreakableData(health, kSoak, tSoak, sSoak, min, max, item);
                }
            }
        }
        Widget_Breakable breakable = new Widget_Breakable(Int32.Parse(root.GetAttribute("sprite")),description,name, breakableData, blocksVision);
        if (tiles[x][y] != null)
        {
            tiles[x][y].SetWidget(breakable);
        }
    }


    internal void WidgetSlot(Tile[][] tiles, XmlElement element, Vector2Int offset, bool[][] grid)
    {
        int x = int.Parse(element.GetAttribute("x")) + offset.x;
        int y = int.Parse(element.GetAttribute("y")) + offset.y;
        SLOTTYPE slotType = EnumTools.strToSlotType(element.GetAttribute("type"));
        MountParameters mountParameters = null;
        Widget_Breakable contents = null;
        string component = null;
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)element.ChildNodes[i];
                if ("mount".Equals(xmlElement.Name) && slotType==SLOTTYPE.WEAPON)
                {
                    Vector2FS emitter = new Vector2FS(0, 0);
                    float.TryParse(xmlElement.GetAttribute("x"), out emitter.x);
                    float.TryParse(xmlElement.GetAttribute("y"), out emitter.y);
                    mountParameters = new MountParameters(EnumTools.strToWeaponSize(xmlElement.GetAttribute("size")),
                        EnumTools.strToFiringArc(xmlElement.GetAttribute("arc")),
                        int.Parse(xmlElement.GetAttribute("facing")),
                        emitter);
                }
                if ("contents".Equals(xmlElement.Name))
                {
                    contents = (Widget_Breakable)SetupWidget(xmlElement);
                    component = xmlElement.GetAttribute("component");
                }
            }

        }

        WidgetSlot widget=new WidgetSlot(slotType,mountParameters);
        if (contents != null)
        {
            widget.SetContainedWidget(contents);
            widget.SetComponent(component);
        }
        if (tiles[x][y] != null)
        {
            tiles[x][y].SetWidget(widget);
        }

    }

    public void WidgetSprite(Tile[][] tiles, XmlElement element, bool[][] grid, Vector2Int offset)
    {
        String sprite = element.GetAttribute("sprite");
        int x = int.Parse(element.GetAttribute("x"))+ offset.x;
        int y = int.Parse(element.GetAttribute("y"))+ offset.y;
        int width = int.Parse(element.GetAttribute("width"));
        int height = int.Parse(element.GetAttribute("height"));
        WidgetSprite widget = new WidgetSprite(sprite, width, height);
        if (tiles[x][y] != null)
        {
            tiles[x][y].SetWidget(widget);
        }
    }

    public void Portal(Tile[][] tiles, XmlElement element, bool[][] grid, Vector2Int offset)
    {
        String destination = element.GetAttribute("destination");
        int x = int.Parse(element.GetAttribute("x")) + offset.x;
        int y = int.Parse(element.GetAttribute("y")) + offset.y;

        int uid  = int.Parse(element.GetAttribute("uid"));
        int facing = int.Parse(element.GetAttribute("facing"));
        WidgetPortal widget = new WidgetPortal(destination, uid, facing);
        if (tiles[x][y] != null)
        {
            
            tiles[x][y].SetWidget(widget);
        }
    }

    internal void Describer(Tile[][] tiles, XmlElement element, bool[][] grid, Vector2Int offset)
    {
        int x = int.Parse(element.GetAttribute("x")) + offset.x;
        int y = int.Parse(element.GetAttribute("y")) + offset.y;

        WidgetDescriber widget = new WidgetDescriber(element.InnerText);
        if (tiles[x][y] != null)
        {

            tiles[x][y].SetWidget(widget);
        }
    }

    internal void ConditionalPortal(Tile[][] tiles, XmlElement element, bool[][] grid, Vector2Int offset)
    {
        String destination = element.GetAttribute("destination");
        int x = int.Parse(element.GetAttribute("x")) + offset.x;
        int y = int.Parse(element.GetAttribute("y")) + offset.y;
        int uid = int.Parse(element.GetAttribute("uid"));
        int facing = int.Parse(element.GetAttribute("facing"));
        string forbidText = null;
        GlobalFlagConditional conditional = null;
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                if ("forbidText".Equals(e.Name))
                {
                    forbidText = e.InnerText;
                }
                if ("conditional".Equals(e.Name))
                {
                    conditional = new GlobalFlagConditional(e.GetAttribute("flag"), int.Parse(e.GetAttribute("value")), GlobalFlagConditional.GetOperand(e.GetAttribute("operator")));
                }
            }
        }
        WidgetPortal widget = new WidgetConditionalPortal(destination, uid, facing, forbidText,conditional);
        if (tiles[x][y] != null)
        {

            tiles[x][y].SetWidget(widget);
        }
    }

    internal void ScriptedPortal(Tile[][] tiles, XmlElement element, bool[][] grid, Vector2Int offset)
    {
        Debug.Log("script portal placed");
        String destination = element.GetAttribute("destination");
        int x = int.Parse(element.GetAttribute("x")) + offset.x;
        int y = int.Parse(element.GetAttribute("y")) + offset.y;
        int uid = int.Parse(element.GetAttribute("uid"));
        int facing = int.Parse(element.GetAttribute("facing"));
        string scriptfile = element.GetAttribute("scriptFile");
        WidgetPortal widget = new WidgetScriptPortal(destination, uid, facing, scriptfile);
        if (tiles[x][y] != null)
        {
            tiles[x][y].SetWidget(widget);
        }
    }



    public void PlaceItemPile(XmlElement element, Tile[][] tiles, bool[][] grid, Vector2Int offset)
    {
        Vector2Int p = new Vector2Int(int.Parse(element.GetAttribute("x"))+offset.x, int.Parse(element.GetAttribute("y"))+offset.y);
        lootTable = new LootTableTool(LootTableBuilder.Build(element));

        tiles[p.x][p.y].SetWidget(new WidgetItemPile(lootTable.GetLoot()));
    }

    public void SeedItemPile(XmlElement element, Tile[][] tiles, bool[][] grid)
    {
        lootTable = new LootTableTool(LootTableBuilder.Build(element));
  
        int min = int.Parse(element.GetAttribute("min"));
        int max = int.Parse(element.GetAttribute("max"));
        int r = min + (min == max ? 0 : random.Next(max - min));

        for (int i = 0; i < r; i++)
        {
            Vector2Int p = new Vector2Int(random.Next() % tiles.Length, random.Next() % tiles[0].Length);
            while (!CanPlace(p.x,p.y, tiles, grid))
            {
                p = new Vector2Int(random.Next() % tiles.Length, random.Next() % tiles[0].Length);
            }
            //place widget
            List<Item> items= lootTable.GetLoot();
            if (items.Count > 0)
            {
                tiles[p.x][p.y].SetWidget(new WidgetItemPile(items));
            }
        }
    }
}
