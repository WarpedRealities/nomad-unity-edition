﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;


public class PerlinTools 
{
    float rX, rY;
    System.Random random;
    public PerlinTools ()
    {
        random = new System.Random();
        rX = random.Next(999);
        rY = random.Next(999);
    }

    public double[][] generatePerlin(int width, int height)
    {
        double[][] values = new double[width][];
        for (int i = 0; i < width; i++)
        {
            values[i] = new double[height];
            for (int j = 0; j < height; j++)
            {
                float sampleX = rX + (i *0.1F);
                float sampleY = rY + (j *0.1F);
           
                values[i][j] = Mathf.PerlinNoise(sampleX, sampleY);
                
            }
        }
        return values;   
    }

    public bool[][] generateGrid(bool[][] grid, double[][] values, float lower, float upper)
    {
        bool[][] newGrid = new bool[grid.Length][];
        for (int i = 0; i < newGrid.Length; i++)
        {
            newGrid[i] = new bool[grid[0].Length];
            for (int j=0;j < newGrid[0].Length; j++)
            {
                newGrid[i][j] = values[i][j] > lower && values[i][j] < upper && grid[i][j];
            }
        }

        return newGrid;
    }
}
