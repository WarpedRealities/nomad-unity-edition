﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class TileTools
{
    ZoneTileset tileset;
    System.Random random;
    public TileTools(ZoneTileset tileset)
    {
        random = new System.Random();
        this.tileset = tileset;
    }

    public ZoneTileset GetTileset()
    {
        return tileset;
    }

    public Vector2Int prefab(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid)
    {
        offset = new Vector2Int(offset.x+Int32.Parse(root.GetAttribute("positionX"))
            , offset.x + Int32.Parse(root.GetAttribute("positionY")));
        
        PrefabPainter(tiles, root, offset, grid,!"false".Equals(root.GetAttribute("overwrite")));
        return offset;
    }
    public Vector2Int prefab(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid, int x, int y)
    {
        offset = new Vector2Int(offset.x + x, offset.y + y);
        PrefabPainter(tiles, root, offset, grid);
        return offset;
    }
    private void PrefabPainter(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid, bool overwrite=true)
    {
      
        int yIndex = 0;
        int width = Int32.Parse(root.GetAttribute("width"));
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("row".Equals(element.Name))
                {
                    string textContents = element.InnerText.Replace(" ", "");
                    char[] charArray = textContents.ToCharArray();
                    for (int j = 0; j < width; j++)
                    {
                        //Debug.Log("px" + (j + offset.x) + "py" + (offset.y + yIndex) + "char" + charArray[j]);
                        if (overwrite || tiles[j + offset.x][offset.y + yIndex]==null)
                        {
                            tiles[j + offset.x][offset.y + yIndex] = GetTile(System.Uri.FromHex(charArray[j]), new Vector2S(j + offset.x, offset.y + yIndex));
                        }
                    }
                }
                yIndex++;
            }
        }
    }
    public void floodFill(Tile[][] tiles, XmlElement element, bool[][] grid)
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles.Length; j++)
            {
                if (grid[i][j] && tiles[i][j] == null)
                {
                    tiles[i][j]= GetTile(Int32.Parse(element.GetAttribute("tile")),new Vector2S(i,j));
                }
            }
        }
    }
    public Tile GetTile(int index, Vector2S position)
    {
        if (index==0)
        {
            return null;
        }
        switch (tileset.getDefinition(index - 1).getSmartTile())
        {
            case 1:
                return new SmartTile(tileset.getDefinition(index - 1), position);
            case 2:
                return new SuperSmartTile(tileset.getDefinition(index - 1), position);
        }
        return new Tile(tileset.getDefinition(index - 1), position);
    }

    public void Noise(Tile[][] tiles, XmlElement element, bool[][] grid)
    {
        int tile = Int32.Parse(element.GetAttribute("tile"));
        int scarcity = Int32.Parse(element.GetAttribute("scarcity"));
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[0].Length; j++)
            {
                if (grid[i][j] && tiles[i][j] == null)
                {
                    int value = random.Next(scarcity);

                    if (value == 0)
                    {
                        tiles[i][j] = GetTile(tile, new Vector2S(i, j));
                    }
                }
            }
        }
    }


    public void PaintTile(Tile[][] tiles, int x, int y, int tileIndex)
    {
        tiles[x][y] = GetTile(tileIndex, new Vector2S(x, y));
    }

    internal Vector2Int edgePrefab(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid, int side)
    {
        int width = Int32.Parse(root.GetAttribute("width"));
        int height = int.Parse(root.GetAttribute("height"));
        Vector2Int position = GetEdgePosition(tiles, side, width, height);
        offset = position + offset;
        PrefabPainter(tiles, root, offset, grid);
        Debug.Log("edge prefab placed at "+offset);
        return offset;
    }
    public Vector2Int GetEdgePosition(Tile[][] tiles, int side, int width, int height)
    {
        int x = 0;
        int y = 0;

        if (side == 0)
        {
            y = tiles[0].Length - height;
        }
        if (side == 1)
        {
            x = tiles.Length - width-1;
        }
        if (side == 1 || side == 3)
        {
            y = random.Next() % (tiles[0].Length - height);
        }
        if (side == 0 || side == 2)
        {
            x = random.Next() % (tiles.Length - height);
        }

        return new Vector2Int(x, y);
    }

    public Vector2Int randprefab(Tile[][] tiles, XmlElement root, Vector2Int offset, bool[][] grid)
    {
        int width = Int32.Parse(root.GetAttribute("width"));
        int height = int.Parse(root.GetAttribute("height"));
        //find a space wide enough to put the prefab in
        Vector2Int position = new Vector2Int(random.Next() % (tiles.Length - width), random.Next() % (tiles[0].Length - height));
        int emergencyexit = 256;
        while (!CanFit(tiles, grid, position, width, height) && emergencyexit>0)
        {
            position = new Vector2Int(random.Next() % (tiles.Length - width), random.Next() % (tiles[0].Length - height));
            emergencyexit--;
        }
        offset = position + offset;
        PrefabPainter(tiles, root, offset, grid);
        return offset;
    }


    private bool CanFit(Tile[][] tiles, bool[][] grid, Vector2Int position, int width, int height)
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (tiles[position.x+i][position.y+j]!=null || !grid[i+position.x][j+position.y])
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void WallOff(Tile[][] tiles, XmlElement element, bool[][] grid)
    {
        int tile = Int32.Parse(element.GetAttribute("tile"));
        bool north = false, east = false, west = false, south = false;
        north = element.GetAttribute("sides").Contains("north");
        east = element.GetAttribute("sides").Contains("east");
        west = element.GetAttribute("sides").Contains("west");
        south = element.GetAttribute("sides").Contains("south");

        for (int i = 0; i < tiles.Length; i++)
        {
            if (south)
            {
                tiles[i][0] = GetTile(tile, new Vector2S(i, 0));
            }
            if (north)
            {
                tiles[i][tiles[0].Length-1] = GetTile(tile, new Vector2S(i, tiles[0].Length - 1));
            }
        }
        for (int j = 0; j < tiles[0].Length; j++)
        {
            if (west)
            {
                tiles[0][j] = GetTile(tile, new Vector2S(0, j));
            }
            if (east)
            {
                tiles[tiles.Length-1][j] = GetTile(tile, new Vector2S(tiles.Length-1, j));
            }
        }
    }

    internal void HorizontalWallBreaker(XmlElement element, bool[][] grid, Tile[][]tiles)
    {
        int wallID = int.Parse(element.GetAttribute("wall"));
        int middle = int.Parse(element.GetAttribute("middle"));
        int left = int.Parse(element.GetAttribute("left"));
        int right = int.Parse(element.GetAttribute("right"));
        int chance = int.Parse(element.GetAttribute("chance"));

        WallbreakerTool wallBreaker= new WallbreakerTool(this);
        wallBreaker.BreakWallsHorizontal(tileset.getDefinition(wallID-1), middle, left, right, chance, grid, tiles);
    //    throw new NotImplementedException();
    }

    internal void VerticalWallBreaker(XmlElement element, bool[][] grid, Tile[][] tiles)
    {
        int wallID = int.Parse(element.GetAttribute("wall"));
        int left = int.Parse(element.GetAttribute("left"));
        int right = int.Parse(element.GetAttribute("right"));
        int chance = int.Parse(element.GetAttribute("chance"));
        int middle = int.Parse(element.GetAttribute("middle"));

        WallbreakerTool wallBreaker = new WallbreakerTool(this);
        wallBreaker.BreakWallsVertical(tileset.getDefinition(wallID - 1), middle, left, right, chance, grid, tiles);
        //  throw new NotImplementedException();
    }

    internal void SealVoid(XmlElement element, bool[][] grid, Tile[][] tiles)
    {
        int tile = int.Parse(element.GetAttribute("tile"));
        for (int i = 1; i < tiles.Length - 1; i++)
        {
            for (int j = 1; j < tiles[0].Length - 1; j++)
            {
                if (grid[i][j] && tiles[i][j] == null)
                {
                    if (FloorAdjacent(i, j, tiles))
                    {
                        this.PaintTile(tiles, i, j, tile);
                    }
                }
            }
        }
        for (int i = 1; i < tiles.Length - 1; i++)
        {
            for (int j = 1; j < tiles[0].Length - 1; j++)
            {
                if (grid[i][j] && tiles[i][j] == null)
                {
                    if (TileAdjacent(i, j, tiles, grid, tile-1, 2))
                    {
                        this.PaintTile(tiles, i, j, tile);
                        grid[i][j] = false;
                    }
                }
            }
        }
    }

    internal void RandomTileReplace(XmlElement element, bool[][] grid, Tile[][] tiles)
    {
        int replace = int.Parse(element.GetAttribute("replace"));
        int target = int.Parse(element.GetAttribute("target"));
        int odds = int.Parse(element.GetAttribute("odds"));
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[0].Length; j++)
            {
                if (grid[i][j] && tiles[i][j] != null)
                {
                    if (tiles[i][j].getDefinition().GetIndex() == target 
                        && tiles[i][j].GetWidget()==null 
                        && DiceRoller.GetInstance().RollDice(odds) == 1)
                    {
                        PaintTile(tiles, i, j, replace);
                    }
                }
            }
        }
    }

    private bool TileAdjacent(int x, int y, Tile[][] tiles, bool[][] grid, int tile, int count)
    {
        int c = 0;
        for (int i = 0; i < 4; i++)
        {
            int i0 = i * 2;
            Vector2Int p = GeometryTools.GetPosition(x, y, i0);
            if (grid[p.x][p.y] && tiles[p.x][p.y] != null && tiles[p.x][p.y].getDefinition().GetIndex()==tile)
            {
                c++;
            }
        }
        return c >= count;
    }

    private bool FloorAdjacent(int x, int y, Tile[][] tiles)
    {
        for (int i = 0; i < 8; i++)
        {
            int i0 = i;
            Vector2Int p = GeometryTools.GetPosition(x, y, i0);
            if (tiles[p.x][p.y] != null && tiles[p.x][p.y].canWalk())
            {
                return true;
            }
        }
        return false;
    }

    internal void ReadEdges(Tile[][] tiles, XmlElement element, Entity entity)
    {
        bool[] edge = entity.GetZone(element.GetAttribute("zone")).GetPregenData().GetEdgeValues(int.Parse(element.GetAttribute("altSide")));
        if ("east".Equals(element.GetAttribute("side")))
        {
            int x = tiles.Length - 1;
            for (int i = 0; i < tiles[0].Length; i++)
            {
                edge[i] = (tiles[x][i] != null && tiles[x][i].canWalk() &&
                    tiles[x - 1][i] != null && tiles[x - 1][i].canWalk());
            
            }
        }
        if ("west".Equals(element.GetAttribute("side")))
        {
            for (int i = 0; i < tiles[0].Length; i++)
            {
                edge[i] = (tiles[0][i] != null && tiles[0][i].canWalk() &&
                    tiles[1][i] != null && tiles[1][i].canWalk());
            }
        }
        if ("south".Equals(element.GetAttribute("side")))
        {
            for (int i= 0; i < tiles.Length; i++)
            {
                edge[i] = (tiles[i][0] != null && tiles[i][0].canWalk() &&
                    tiles[i][1] != null && tiles[i][1].canWalk());
            }
        }
        if ("north".Equals(element.GetAttribute("side")))
        {
            int y = tiles[0].Length - 1;
            for (int i = 0; i < tiles.Length; i++)
            {
                edge[i] = (tiles[i][y] != null && tiles[i][y].canWalk() &&
                    tiles[i][y-1] != null && tiles[i][y-1].canWalk());
            }
        }
        //StringBuilder stringBuilder = new StringBuilder();
        //for (int i = 0; i < edge.Length; i++)
        //{
        //    stringBuilder.Append(edge[i] ? "1" : "0");
        //}

    }

    internal void PregenEdges(Tile[][] tiles, XmlElement element, PregenData pregenData)
    {
       if (pregenData == null)
        {
            return;
        }
       int tile = int.Parse(element.GetAttribute("tile"));
        if ("east".Equals(element.GetAttribute("side")))
        {
            if (!pregenData.HasEdgeValues(1))
            {
                return;
            }
            bool[] edge = pregenData.GetEdgeValues(1);
            int x = tiles.Length - 1;
            for (int i = 0; i < tiles[0].Length; i++)
            {
                if (edge[i])
                {
                    this.PaintTile(tiles, x, i, tile);
                    this.PaintTile(tiles, x - 1, i, tile);
                }

            }
        }
        if ("west".Equals(element.GetAttribute("side")))
        {
            if (!pregenData.HasEdgeValues(3))
            {
                return;
            }
            bool[] edge = pregenData.GetEdgeValues(3);
            for (int i = 0; i < tiles[0].Length; i++)
            {
                if (edge[i])
                {
                    this.PaintTile(tiles, 0, i, tile);
                    this.PaintTile(tiles, 1, i, tile);
                }
            }
        }
        if ("south".Equals(element.GetAttribute("side")))
        {
            if (!pregenData.HasEdgeValues(2))
            {
                return;
            }
            bool[] edge = pregenData.GetEdgeValues(2);
            for (int i = 0; i < tiles.Length; i++)
            {
                if (edge[i])
                {
                    this.PaintTile(tiles, i, 0, tile);
                    this.PaintTile(tiles, i, 1, tile);
                }
            }
        }
        if ("north".Equals(element.GetAttribute("side")))
        {
            if (!pregenData.HasEdgeValues(0))
            {
                return;
            }
            bool[] edge = pregenData.GetEdgeValues(0);
            int y = tiles[0].Length - 1;
            for (int i = 0; i < tiles.Length; i++)
            {
                if (edge[i])
                {
                    this.PaintTile(tiles, i, y, tile);
                    this.PaintTile(tiles, i, y - 1, tile);
                }
            }
        }
    }

    internal void ReadTileset()
    {
        for (int i = 0; i < tileset.getCount(); i++)
        {
            Debug.Log("index is " + tileset.getDefinition(i).GetIndex() + " " + tileset.getDefinition(i).getDescription());
        }
    }

}
