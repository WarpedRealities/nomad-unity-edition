﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Neighbour
{
    public bool linked;
    public Room room;

    public Neighbour(Room room)
    {
        this.room = room;
    }
}

public class Room
{
    Vector2Int position;
    int width, height;
    Neighbour[] neighbours;
    int neighbourCount;
    int index = 0;
    public Room(Vector2Int p, int w, int h,int index)
    {
        this.index = index;
        this.position = p;
        this.width = w;
        this.height = h;
        neighbours = new Neighbour[4];
    }

    public int GetIndex()
    {
        return index;
    }

    public int GetNeighbourCount()
    {
        return neighbourCount;
    }

    public void SetNeighbourCount(int count)
    {
        this.neighbourCount = count;
    }

    public Vector2Int GetPosition()
    {
        return position;
    }

    public int GetWidth()
    {
        return width;
    }

    public int GetHeight()
    {
        return height;
    }

    public Neighbour[] GetNeighbours()
    {
        return neighbours;
    }

    internal bool HasOpenNeighbours()
    {
        int c = 0;
        for (int i = 0; i < neighbours.Length; i++)
        {
            if (neighbours[i] != null)
            {
                c++;
            }
        }
        return c < neighbourCount;
    }

    internal bool HasNeighbour(Room current)
    {
        for (int i = 0; i < neighbours.Length; i++)
        {
            if (neighbours[i]!=null && current.Equals(neighbours[i].room))
            {
                return true;
            }
        }
        return false;
    }

    internal void AddNeighbour(Room room)
    {
        for (int i = 0; i < neighbours.Length; i++)
        {
            if (neighbours[i] == null)
            {
                neighbours[i] = new Neighbour(room);
                return;
            }
        }
    }

    internal void LinkRoom(Room room)
    {
        for (int i = 0; i < neighbours.Length; i++)
        {
            if (neighbours[i]!=null && neighbours[i].room.Equals(room))
            {
                neighbours[i].linked = true;
                return;
            }
        }
        throw new NotImplementedException();
    }
}


public class RogueBuilderTool
{
    List<Room> rooms;
    private PointsOfInterest pointsOfInterest;
    public RogueBuilderTool(PointsOfInterest pointsOfInterest)
    {
        rooms = new List<Room>();
        this.pointsOfInterest = pointsOfInterest;
    }

    public void GenerateRooms(string divider, int dividerPoint, int min, int max, int minSize, int maxSize, bool [][] grid)
    {
        int count = DiceRoller.GetInstance().RollDice(max - min) + min;
        Debug.Log("count is " + count);
        if ("vertical".Equals(divider))
        {
            GenerateRoomsVertical(dividerPoint, count / 2, minSize, maxSize, grid);
            DuplicateRoomsVertical(dividerPoint);
        }
    }

    private void DuplicateRoomsVertical(int dividerPoint)
    {
        int limit = rooms.Count;
        for (int i = 0; i < limit; i++)
        {
            int y = dividerPoint-rooms[i].GetPosition().y;
            rooms.Add(new Room(new Vector2Int(rooms[i].GetPosition().x, dividerPoint + y-1)
                , rooms[i].GetWidth(), 
                rooms[i].GetHeight(),
                rooms.Count));
        }
    }

    private void GenerateRoomsVertical(int dividerPoint, int count, int minSize, int maxSize, bool[][] grid)
    {
        int width = grid.Length;
        int height = dividerPoint;

        for (int i = 0; i < count; i++)
        {
            BuildRoom(width, height, minSize, maxSize, grid);

        }

    }

    private void BuildRoom(int width, int height, int minSize, int maxSize, bool[][] grid)
    {
        int escape = 0;
        while (escape < 32)
        {
            int x = DiceRoller.GetInstance().RollDice(width - minSize - 2) + (minSize / 2)+1;
            int y = DiceRoller.GetInstance().RollDice(height - minSize - 2) + (minSize / 2)+1;
            int w = DiceRoller.GetInstance().RollDice(maxSize - minSize) + minSize;
            int h = DiceRoller.GetInstance().RollDice(maxSize - minSize) + minSize;

            if (CanPlace(x, y, w, h, grid))
            {              

                rooms.Add(new Room(new Vector2Int(x, y), w, h,rooms.Count));
                return;
            }

            escape++;
        }
    }

    private bool CanPlace(int x, int y, int w, int h, bool [][] grid)
    {
        int left = x - (w / 2);
        int top = y + (h / 2);
        int right = x + (w / 2);
        int bottom = y - (h / 2);

        if (left<1 || right >= grid.Length-1 || bottom<1 || top >= grid[0].Length-1)
        {

            return false;
        }
        if (!grid[left][top] || !grid[left][bottom] || !grid[right][top] || !grid[right][bottom])
        {

           return false;
        }
        return (OverlapCheck(x, y, w, h));
    }

    private bool OverlapCheck(int x, int y, int w, int h)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            int hDistance = Math.Abs(rooms[i].GetPosition().x - x);
            int vDistance = Math.Abs(rooms[i].GetPosition().y - y);
            int hSpacing = (w / 2) + (rooms[i].GetWidth() / 2)+1;
            int vSpacing = (h / 2) + (rooms[i].GetHeight() / 2)+1;
            if (hDistance<hSpacing && vDistance < vSpacing)
            {
                return false;
            }
        }

        return true;
    }

    internal void GenerateNeighbours(int minConnect, int maxConnect)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            rooms[i].SetNeighbourCount(DiceRoller.GetInstance().RollDice(maxConnect - minConnect) + minConnect);
        }
        Room current = rooms[0];
        List<Room> unlinkedRooms = new List<Room>(rooms);
        Stack<Room> stack = new Stack<Room>();
        stack.Push(current);
        unlinkedRooms.Remove(current);
        int escape = rooms.Count * 4;
        while(escape>0 && unlinkedRooms.Count > 0 && stack.Count > 0)
        {
            Debug.Log("node " + current.GetIndex() + " " + current.HasOpenNeighbours());
            if (current.HasOpenNeighbours())
            {
                current = ExtendNodes(current, stack, unlinkedRooms);
            }
            else
            {
                current = stack.Pop();
            }

            escape--;
        }
        foreach (Room item in unlinkedRooms)
        {
            rooms.Remove(item);
        }

    }

    private Room ExtendNodes(Room current, Stack<Room> stack, List<Room> unlinkedRooms)
    {
        //get the nearest node with an open neighbour slot
        int distance = 999;
        int index = -1;
        for (int i = 0; i < rooms.Count; i++)
        {
            
            if (!current.Equals(rooms[i]) && !rooms[i].HasNeighbour(current) && rooms[i].HasOpenNeighbours())
            {
                int x = Math.Abs(current.GetPosition().x - rooms[i].GetPosition().x);
                int y = Math.Abs(current.GetPosition().y - rooms[i].GetPosition().y);
                int d = unlinkedRooms.Contains(rooms[i]) ? (x+y)/2 : x + y;
                
                if (d < distance)
                {
                    index = i;
                    distance = d;
                }
            }
        }
        if (index != -1)
        {
            current.AddNeighbour(rooms[index]);
            rooms[index].AddNeighbour(current);
            if (unlinkedRooms.Contains(rooms[index]))
            {
                unlinkedRooms.Remove(rooms[index]);
            }
            stack.Push(rooms[index]);
            return rooms[index];
        }
        return current;

    }

    internal void GenerateHallways(int tile,Tile[][] tiles, bool [][] grid,TileTools tileTools)
    {
        Pathfinder pathfinder = new Pathfinder(tiles.Length, tiles[0].Length, tiles, true);
        AuditPathfinder auditPathfinder = new AuditPathfinder(tiles.Length, tiles[0].Length, tiles, true);
        auditPathfinder.SetupAudit(true, tile, -1);
        for (int i = 0; i < rooms.Count; i++)
        {
            for (int j = 0; j < rooms[i].GetNeighbours().Length; j++)
            {
                if (rooms[i].GetNeighbours()[j] != null && !rooms[i].GetNeighbours()[j].linked)
                {
                    Debug.Log("making a connection");
                    Path[] path = null;
                        if (auditPathfinder.FindPath(rooms[i].GetPosition(), rooms[i].GetNeighbours()[j].room.GetPosition(),
                        64, out path, TileMovement.WALK))
                        {
                            rooms[i].GetNeighbours()[j].linked = true;
                        //rooms[i].GetNeighbours()[j].room.LinkRoom(rooms[i]);
                            Debug.Log("making path");
                            for (int k = 0; k < path.Length; k++)
                            {
                                if (path[k] != null)
                                {

                                    tileTools.PaintTile(tiles, path[k].position.x, path[k].position.y, tile);
                                }
                            }
                        }
                }
            }
        }
        //throw new NotImplementedException();
    }

    public bool [][] GenerateGrid(bool [][] grid)
    {
        bool[][] g = new bool[grid.Length][];
        for (int i = 0; i < grid[0].Length; i++)
        {
            g[i] = new bool[grid[0].Length];
        }
        for (int i = 0; i < rooms.Count; i++)
        {
            int offsetX = rooms[i].GetPosition().x - (rooms[i].GetWidth() / 2);
            int offsetY = rooms[i].GetPosition().y - (rooms[i].GetHeight() / 2);
            
            for (int j=0;j<rooms[i].GetWidth(); j++)
            {
                for (int k=0;k<rooms[i].GetHeight(); k++)
                {
                    g[j + offsetX][k + offsetY] = true;
                }
            }
        }
        return g;
    }

    internal void RegisterPoints()
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            pointsOfInterest.AddPointOfInterest(rooms[i].GetPosition());
        }
    }
}
