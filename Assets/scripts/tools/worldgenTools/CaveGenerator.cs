﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CaveNode
{
    Vector2Int position;
    public CaveNode[] neighbours;
    int size;

    public int GetSize()
    {
        return size;
    }

    public Vector2Int GetPosition()
    {
        return position;
    }

    public CaveNode(Vector2Int position, int size)
    {
        neighbours = new CaveNode[4];
        this.position = position;
        this.size = size;
    }

    internal bool HasNeighbour(CaveNode node)
    {
        for (int i = 0; i < neighbours.Length; i++)
        {
            if (this.neighbours[i] == node)
            {
                return true;
            }
        }
        return false;
    }

    internal void AddNeighbour(CaveNode caveNode)
    {
        for (int i = 0; i < neighbours.Length; i++)
        {
            if (neighbours[i] == null)
            {
                neighbours[i] = caveNode;
                return;
            }
        }
    }
}

public class CaveGenerator
{
    PointsOfInterest pointsOfInterest;
    List<CaveNode> caveNodes;

    public CaveGenerator(PointsOfInterest points)
    {
        this.pointsOfInterest = points;
        caveNodes = new List<CaveNode>();
    }
  
    public void GenerateNodes(int maxNodes, int minNodes, int minSize, int maxSize, bool [][] grid)
    {
        int count = minNodes < maxNodes ? DiceRoller.GetInstance().RollDice(maxNodes - minNodes) + minNodes : maxNodes;
        if (pointsOfInterest.GetPoints().Count > 0)
        {
            for (int i = 0; i < pointsOfInterest.GetPoints().Count; i++)
            {
                caveNodes.Add(new CaveNode(pointsOfInterest.GetPoints()[i], maxSize));
            }
        }
        for (int i = 0; i < count; i++)
        {
            Vector2Int p = new Vector2Int(DiceRoller.GetInstance().RollDice(grid.Length),
                DiceRoller.GetInstance().RollDice(grid[0].Length));

            while (!grid[p.x][p.y])
            {
                p = new Vector2Int(DiceRoller.GetInstance().RollDice(grid.Length),
                DiceRoller.GetInstance().RollDice(grid[0].Length));
            }
            int size = minSize < maxSize ? DiceRoller.GetInstance().RollDice(maxSize-minSize)+minSize :maxSize ;
            caveNodes.Add(new CaveNode(p, size));
        }
    }

    public void GenerateNeighbourLinks()
    {
        //for each node, work out the nearest node
        for (int i = 0; i < caveNodes.Count; i++)
        {
            AddNearestNeighbour(caveNodes[i],i);
        }
    }

    public void JoinNeighbours(int width, int height, Tile[][] tiles, TileTools tileTools, bool [][] grid, int floorTile)
    {
        AuditPathfinder auditPathfinder = new AuditPathfinder(width,height,tiles,true);
        auditPathfinder.SetupAudit(true, floorTile, -1);
        for (int i = 0; i < caveNodes.Count; i++)
        {
            for (int j = 0; j < caveNodes[i].neighbours.Length; j++)
            {
                if (caveNodes[i].neighbours[j] != null)
                {
                    Path[] path = null;
                    if (auditPathfinder.FindPath(caveNodes[i].GetPosition(), caveNodes[i].neighbours[j].GetPosition(), 32, out path, TileMovement.WALK))
                    {
                        for (int k = 0; k < path.Length; k++)
                        {
                            if (path[k] != null)
                            {
                                tileTools.PaintTile(tiles, path[k].position.x, path[k].position.y, floorTile);
                            }
                        }
                    }
                    else
                    {
                    }

                }
            }
        }
    }

    private void AddNearestNeighbour(CaveNode caveNode, int index)
    {
        int distance = 999;
        int closest = -1;
        for (int i = 0; i < caveNodes.Count; i++)
        {
            if (i != index)
            {
                if (!caveNode.HasNeighbour(caveNodes[i]))
                {
                    int d = Math.Abs(caveNodes[i].GetPosition().x - caveNode.GetPosition().x) +
                        Math.Abs(caveNodes[i].GetPosition().y - caveNode.GetPosition().y);

                    if (d < distance)
                    {

                        closest = i;
                        distance = d;
                    }
                }
            }
        }

        if (closest != -1)
        {
            //add the neighbour
            caveNode.AddNeighbour(caveNodes[closest]);
            caveNodes[closest].AddNeighbour(caveNode);
        }
    }
}
