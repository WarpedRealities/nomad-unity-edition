﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LaunchLandTool 
{
    


    public void TakeControl(ShipAnalysisTool tool)
    {
        Spaceship ship = tool.GetShip();
        tool.ApplyStats();

        //transition to solar scene
        //SceneManager.LoadScene("SolarScene");
    }

    public void RelinquishControl(ShipAnalysisTool tool)
    {

        tool.ReconcileStats();

        SceneManager.LoadScene("ViewScene");
       
    }

    public void Launch(ShipAnalysisTool tool)
    {
        Spaceship ship = tool.GetShip();
        tool.ApplyStats();
        //disconnect the hatch from the host zone
        Entity entity = GlobalGameState.GetInstance().getCurrentEntity();
        if (entity != ship)
        {
            LandingSite landingSite = entity.GetLandingSite(ship);
            Zone hostZone = entity.GetZone(landingSite.GetZone());
            if (tool.IsCapacityExceeded())
            {
                ShipConnectorTool.RemovePassengers(tool.GetPassengers()-tool.GetSolarStats().GetCapacity(), 
                    ship.GetZone(ship.getName()), 
                    entity.GetZone(landingSite.GetZone()));
            }
            ShipConnectorTool.Disconnect(ship.GetZone(ship.getName()), entity.GetZone(landingSite.GetZone()));
            //remove the exterior from the planet surface
            LandingSiteTool landingSiteTool = new LandingSiteTool(hostZone.GetContents().getTiles(),
                null,
                hostZone.GetContents().GetZoneParameters(),
                new TileTools(hostZone.GetContents().getTileSet()));


            entity.RemoveLandingSite(landingSite);
            if (ship.GetState().Equals(ShipState.LANDED))
            {
                //remove the landed ship from the planet's list
                landingSiteTool.RemoveLanding(landingSite);
                //subtract fuel
                ship.GetResources().GetResource("RES_FUEL").amount -= ship.GetSpaceshipStats().GetFuelCost();
            }
            //move ship into space at a random adjacent tile to the planet center.
            MoveShipToSpace(ship, entity);

            //transition to solar scene
        }
        if (ship.GetWarpHandler() != null)
        {
            WarpToSystem(tool);
        }
        else
        {
            ship.SetState(ShipState.SPACE);
        }
        SceneManager.LoadScene("SolarScene");
    }

    private void WarpToSystem(ShipAnalysisTool tool)
    {
        tool.GetShip().GetWarpHandler().state = WarpState.JUMPIN;
        tool.GetShip().SetAnimation(new ShipWarpAnimation(tool.GetShip().GetWarpHandler()));
        //remove from original system
        GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Remove(tool.GetShip());
        //change to new system
        GlobalGameState.GetInstance().SetCurrentSystem(GlobalGameState.GetInstance().GetUniverse().GetSystem(
            tool.GetShip().GetWarpHandler().destinationName));
        //generate if need be
        if (GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents() == null)
        {
            SystemGeneratorTools systemGeneratorTools = new SystemGeneratorTools();
            StarSystemContents contents= systemGeneratorTools.generateStarSystem(GlobalGameState.GetInstance().getCurrentSystem().GetName(),
                GlobalGameState.GetInstance().GetUniverse().GetUIDGenerator());
            GlobalGameState.GetInstance().getCurrentSystem().setContents(contents);
        }
        //insert into new system
        GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Add(tool.GetShip());
        Vector2 p = GeometryTools.GetPosition(0, 0, tool.GetShip().GetWarpHandler().direction);
        p = (p.normalized)*-64;
        tool.GetShip().setPosition(new Vector2Int((int)p.x, (int)p.y));
    }

    private void MoveShipToSpace(Spaceship ship, Entity entity)
    {
        int r = DiceRoller.GetInstance().RollDice(8);
        Vector2Int p = GeometryTools.GetPosition((int)entity.GetPosition().x, (int)entity.GetPosition().y, r);
        GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Add(ship);
        ship.setPosition(p);
        ship.SetState(ShipState.SPACE);
        GlobalGameState.GetInstance().SetCurrentEntity(ship);
    }

    internal void Land(ShipAnalysisTool tool, Planet planet, Zone zone)
    {
        tool.ReconcileStats();
        List<LandingSite> landingSiteList = planet.GetContents().GetLandingSites() != null ?
                  planet.GetContents().GetLandingSites() : new List<LandingSite>();
        //add a landing site
        LandingSite landingSite = new LandingSite(zone.getName(), tool.GetShip(), true);

        //if the zone is already generated we need to handle the placement into the zone
        //if (!zone.IsGenerated())
        //{
            LandingSiteTool landingSiteTool = new LandingSiteTool(zone.GetContents().getTiles(),
                zone.GetContents().GetFixedLandingSite(), zone.GetContents().GetZoneParameters(),
                new TileTools(zone.GetContents().getTileSet()));
            bool[][] grid = BuildGrid(zone.GetContents().GetZoneParameters());
        if (landingSiteTool.Land(landingSite, ref grid, false))
        {
            landingSiteList.Add(landingSite);
            planet.GetContents().SetLandingSites(landingSiteList);
            Debug.Log("landing landing site position " + landingSite.GetPosition().x + " " + landingSite.GetPosition().y);
            //move the current entity to the planet
            GlobalGameState.GetInstance().SetCurrentEntity(planet);
            //remove ship
            GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Remove(tool.GetShip());
            tool.GetShip().SetState(ShipState.LANDED);
            //also if zone is already generated, handle connectors
            ShipConnectorTool.Connect(tool.GetShip().GetZone(0), zone);
            SceneManager.LoadScene("ViewScene");
        }
        //}

    }

    internal void Dock(ShipAnalysisTool tool, Spaceship spaceship)
    {
        spaceship.SetState(ShipState.DOCKED);
        tool.GetShip().SetState(ShipState.DOCKED);
        tool.ReconcileStats();
        //need to create a virtual entity to hold the docking situation
        DockingHandler handler = new DockingHandler(spaceship.GetPosition());
        GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Add(handler);
        GlobalGameState.GetInstance().SetCurrentEntity(handler);
        //add both ships to the handler as docked vessels
        handler.Land(tool.GetShip(), 0, spaceship.GetZone(0).getName());
        handler.Land(spaceship, 1, tool.GetShip().getName());
        //connect the ships
        ShipConnectorTool.Connect(tool.GetShip().GetZone(0), spaceship.GetZone(0), true);
        //set the entity properly
        GlobalGameState.GetInstance().SetCurrentEntity(handler);        
        //remove from the starsystem
        GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Remove(tool.GetShip());
        GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Remove(spaceship);
        //load scenes

        SceneManager.LoadScene("ViewScene");

    }

    internal void Dock(ShipAnalysisTool tool, Station station, Zone zone)
    {
        tool.ReconcileStats();
        if (!zone.IsGenerated())
        {
            Debug.Log("dock at location generate");
            zone.Generate();
        }
        LandingSite landingSite = GetLandingSite(station.GetContents(),zone.getName());
        GlobalGameState.GetInstance().getCurrentSystem().GetSystemContents().GetEntities().Remove(tool.GetShip());
        landingSite.SetSpaceship(tool.GetShip());
        GlobalGameState.GetInstance().SetCurrentEntity(station);
        tool.GetShip().SetState(ShipState.DOCKED);
        ShipConnectorTool.Connect(tool.GetShip().GetZone(0), zone,true);
        SceneManager.LoadScene("ViewScene");

    }

    private LandingSite GetLandingSite(StationContents contents, string zoneName)
    {
        for (int i = 0; i < contents.GetLandingSites().Count; i++)
        {
            if (contents.GetLandingSites()[i].GetZone().Equals(zoneName))
            {
                return contents.GetLandingSites()[i];
            }
        }
        return null;
    }

    private bool[][] BuildGrid(ZoneParameters zoneParameters)
    {
        bool[][] grid = new bool[zoneParameters.GetWidth()][];
        for (int i = 0; i < zoneParameters.GetWidth(); i++)
        {
            grid[i] = new bool[zoneParameters.GetHeight()];
            for (int j = 0; j < zoneParameters.GetHeight(); j++)
            {
                grid[i][j] = true;
            }
        }
        return grid;
    }

}
