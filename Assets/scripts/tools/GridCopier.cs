﻿using UnityEngine;
using System.Collections;
using System;

public class GridCopier
{
    internal static bool[][] Copy(bool[][] grid)
    {
        bool[][] nugrid = new bool[grid.Length][];
        for (int i = 0; i < nugrid.Length; i++)
        {
            nugrid[i] = new bool[grid[0].Length];
            for (int j = 0; j < nugrid[i].Length; j++)
            {
                nugrid[i][j] = grid[i][j];
            }
        }
        return nugrid;
    }
}
