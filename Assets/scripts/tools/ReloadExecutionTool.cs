﻿using UnityEngine;
using System.Collections;
using System;

public class ReloadExecutionTool
{
    internal static void Execute(Item ammoItem, Item reloadThis, ZoneActor player)
    {
        AmmoBase ammoBase = (AmmoBase)ammoItem;   
        //does the item need reloading?
        ResourceStoreInstance resourceStore = reloadThis.GetDef().GetItemType() == ItemType.EQUIP ? 
            ((EquipInstance)reloadThis).GetResourceStore(): 
            ((AmmoInstance)reloadThis).GetResource();
        if ((resourceStore.GetDef().GetCapacity()>0 && resourceStore.GetAmount() >= resourceStore.GetDef().GetCapacity()) && 
            ammoBase.GetAmmoType().GetCodeName().Equals(resourceStore.GetAmmoType().GetCodeName()))
        {
            return;
        }
        //is this a magazine fed weapon?
        if (resourceStore.GetDef().GetCapacity() == 0)
        {
            ExecuteMagazine(resourceStore, ammoItem, reloadThis, player);
        }
        else
        {
            ExecuteFeed(resourceStore, ammoItem, reloadThis, player);
        }
    }
    private static void ExecuteMagazine(ResourceStoreInstance resourceStore, Item ammoItem, Item reloadThis, ZoneActor player)
    {
        //do we have a magazine already?
        if (resourceStore.GetCapacity() > 0)
        {
            player.GetController().addAction(new ActionReload(null, reloadThis, resourceStore.GetDef().GetReloadCost()));
            //need to unload
        }

        player.GetController().addAction(new ActionReload(ammoItem, reloadThis, resourceStore.GetDef().GetReloadCost()));

    }

    private static void ExecuteFeed(ResourceStoreInstance resourceStore, Item ammoItem, Item reloadThis, ZoneActor player)
    {
        //do we need to empty out the ammo due to incompatible types?
        AmmoBase ammoBase = (AmmoBase)ammoItem;
        AmmoType ammoType = ammoBase.GetAmmoType();
        int totalRounds = resourceStore.GetAmount();
        if (!ammoType.GetCodeName().Equals(resourceStore.GetAmmoType().GetCodeName()))
        {
            player.GetController().addAction(new ActionReload(null, reloadThis, resourceStore.GetDef().GetReloadCost()));
            totalRounds = 0;
        }
        //how many rounds can we fit in
        int emptyChambers = resourceStore.GetDef().GetCapacity() - totalRounds;
        //how many rounds do we have?
        int roundCount = ammoItem.GetItemClass() == ItemClass.AMMOSTACK ? ((ItemAmmoStack)ammoItem).GetCount(): 1;
        int count = GetCount(emptyChambers, roundCount);

        //we need to add this many reload actions to the player character
        for (int i = 0; i < count; i++)
        {
            player.GetController().addAction(new ActionReload(ammoItem, reloadThis, resourceStore.GetDef().GetReloadCost()));
        }

    }

    private static int GetCount(int emptyChambers, int roundCount)
    {
        if (emptyChambers >= roundCount)
        {
            return roundCount;
        }
        else
        {
            return emptyChambers;
        }
    }

    internal static void TryReload(ZoneActor playerInZone)
    {
        Inventory inventory = ((Player)playerInZone.actor).GetInventory();
        //cannot reload your empty hands
        if (inventory.GetSlot(0)== null || !(inventory.GetSlot(0) is EquipInstance))
        {
            return;
        }
        Item reloadThis = inventory.GetSlot(0);
        ResourceStoreInstance resourceStore = reloadThis.GetDef().GetItemType() == ItemType.EQUIP ?
        ((EquipInstance)reloadThis).GetResourceStore() :
        ((AmmoInstance)reloadThis).GetResource();
        if (resourceStore == null)
        {
            return;
        }
        //cannot reload a full weapon
        if (resourceStore.GetAmount()>=resourceStore.GetCapacity())
        {
            return;
        }
        string code = resourceStore.GetAmmoCode();
        string type = resourceStore.GetAmmoType().GetCodeName();
        Item ammo = null;
        for (int i = 0; i < inventory.GetItemCount(); i++)
        {
            if (inventory.GetItem(i).GetDef().GetItemType().Equals(ItemType.AMMO))
            {
                if (inventory.GetItem(i) is AmmoInstance)
                {
                    AmmoInstance ammoInstance = (AmmoInstance)inventory.GetItem(i);
                    if (ammoInstance.GetDef().GetCodeName().Equals(code) && 
                        ammoInstance.GetAmmoType().GetCodeName().Equals(type) && 
                        ammoInstance.GetResource().GetAmount()>0)
                    {
                        ammo = ammoInstance;
                        break;
                    }
                }
                if (inventory.GetItem(i) is ItemAmmoStack)
                {
                    ItemAmmoStack ammoStack = (ItemAmmoStack)inventory.GetItem(i);
                    if (ammoStack.GetDef().GetCodeName().Equals(code) && ammoStack.GetAmmoType().GetCodeName().Equals(type))
                    {
                        ammo = ammoStack;
                        break;
                    }
                }
            }
        }
        if (ammo != null)
        {
            Execute(ammo, reloadThis, playerInZone);
        }
    }
}
