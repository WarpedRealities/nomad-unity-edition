using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationTool 
{
 
    public static WarpHandler GenerateWarp(Spaceship ship)
    {
        int direction = GeometryTools.GetDirection(ship.GetPosition().x, ship.GetPosition().y);
        Vector2Int origin = GlobalGameState.GetInstance().getCurrentSystem().GetPosition();
        Vector2Int target = GeometryTools.GetPosition(origin.x, origin.y, direction);
        StarSystem system = GlobalGameState.GetInstance().GetUniverse().GetSystem(target);
        if (system == GlobalGameState.GetInstance().getCurrentSystem())
        {
            system = null;
        }
        WarpHandler warpHandler = new WarpHandler();
        float distance = 100 - (Vector2.Distance(new Vector2(0, 0), ship.GetPosition()));
        distance = distance < 1 ? 1 : distance;
        float stress = Mathf.Sqrt(distance)/ ship.GetSolarStats().GetFTL();
        float increment = 0.1F /(stress);
        warpHandler.direction = direction;
        warpHandler.destinationX = target.x;
        warpHandler.destinationY = target.y;
        warpHandler.destinationName = system != null ? system.GetName() : null;
        warpHandler.stress = stress;
        warpHandler.chargeIncrement = increment;
        return warpHandler;
    }

    internal static float GetThreshold(int amount)
    {
        float maximumIncrements = amount / 2;
        float minimumIncrement = 1.0F / maximumIncrements;

        return amount / 10;
    }
}
