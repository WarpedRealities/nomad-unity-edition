﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using System.Xml.Linq;

public class StatusEffectLoader
{
    internal static StatusEffect buildModifier(XmlElement root)
    {
        SkillModifier[] skillModifiers=null;
        DefenceResistModifier[] resistModifiers = null;
        HazardModifier[] hazardMods = null;
        int duration = int.Parse(root.GetAttribute("duration"));
        int uid = int.Parse(root.GetAttribute("uid"));
        int icon = -1;
        int.TryParse(root.GetAttribute("icon"), out icon);
        string desc="";
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)root.ChildNodes[i];
                if ("removeDescription".Equals(e.Name))
                {
                    desc = e.InnerText;
                }
                if ("skillModifiers".Equals(e.Name))
                {
                    int count = int.Parse(e.GetAttribute("count"));
                    skillModifiers = new SkillModifier[count];
                    int index = 0;
                    for (int j = 0; j < e.ChildNodes.Count; j++)
                    {
                        if (e.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement x = (XmlElement)e.ChildNodes[j];
                            if ("modifier".Equals(x.Name))
                            {
                                skillModifiers[index] = new SkillModifier(EnumTools.strToSkill(x.GetAttribute("skill")), int.Parse(x.GetAttribute("value")));
                                index++;
                            }
                        }
                    }
                }
                if ("resistModifiers".Equals(e.Name))
                {
                    int count = int.Parse(e.GetAttribute("count"));
                    resistModifiers = new DefenceResistModifier[count];
                    int index = 0;
                    for (int j = 0; j < e.ChildNodes.Count; j++)
                    {
                        if (e.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement x = (XmlElement)e.ChildNodes[j];
                            if ("modifier".Equals(x.Name))
                            {
                                resistModifiers[index] = new DefenceResistModifier(EnumTools.strToDefenceResist(x.GetAttribute("resistance")), int.Parse(x.GetAttribute("value")));
                                index++;
                            }
                        }
                    }
                }
                if ("hazardModifiers".Equals(e.Name))
                {
                    int count = int.Parse(e.GetAttribute("count"));
                    hazardMods = new HazardModifier[count];
                    int index = 0;
                    for (int j = 0; j < e.ChildNodes.Count; j++)
                    {
                        if (e.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement x = (XmlElement)e.ChildNodes[j];
                            if ("modifier".Equals(x.Name))
                            {
                                hazardMods[index] = new HazardModifier(x.GetAttribute("hazard"), float.Parse(x.GetAttribute("strength")));
                                index++;
                            }
                        }
                    }
                }
            }
        }

        return new StatusModifier(skillModifiers, resistModifiers,hazardMods, desc, duration,uid,icon);
    }

    internal static StatusBind buildBind(XmlElement root)
    {
        string resistStr = "";
        string breakStr = "";
        string applyStr = "";
        int strength = int.Parse(root.GetAttribute("strength"));
        int icon = int.Parse(root.GetAttribute("icon"));
        int uid = int.Parse(root.GetAttribute("uid"));
        SK skill= EnumTools.strToSkill(root.GetAttribute("skill"));
        bool dependent= root.GetAttribute("dependent").Equals("true");
        StatusEffect subEffect = null;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)root.ChildNodes[i];
                if ("resist".Equals(e.Name)){
                    resistStr = e.InnerText;
                }
                if ("break".Equals(e.Name))
                {
                    breakStr = e.InnerText;
                }
                if ("apply".Equals(e.Name))
                {
                    applyStr = e.InnerText;
                }
                if ("debuff".Equals(e.Name))
                {
                    subEffect = buildModifier(e);
                }
            }
        }
        return new StatusBind(uid, icon,resistStr, breakStr, applyStr, dependent, strength, skill,subEffect);
    }

    internal static StatusEffect buildDOT(XmlElement root)
    {
        string removeDescription = "";
        string tickDescription = "";
        int duration = int.Parse(root.GetAttribute("duration"));
        int uid = int.Parse(root.GetAttribute("uid"));
        int icon = int.Parse(root.GetAttribute("icon"));
        ST stat = EnumTools.strToStat(root.GetAttribute("stat"));
        DR damageType = root.GetAttribute("type").Length > 2 ? EnumTools.strToDefenceResist(root.GetAttribute("type")): DR.NONE;
        DR resistance = root.GetAttribute("resist").Length > 2 ? EnumTools.strToDefenceResist(root.GetAttribute("resist")) : DR.NONE;
        int strength = 0;
        int.TryParse(root.GetAttribute("strength"), out strength);
        int tickChange = int.Parse(root.GetAttribute("change"));
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)root.ChildNodes[i];
                if ("removeDescription".Equals(e.Name))
                {
                    removeDescription = e.InnerText;
                }
                if ("tickDescription".Equals(e.Name))
                {
                    tickDescription = e.InnerText;
                }
            }
        }
        return new StatusDamageOverTime(removeDescription, tickDescription, duration, uid, icon, stat, tickChange, damageType, resistance, strength);
    }

    internal static StatusEffect buildImmunity(XmlElement root)
    {
        string removeDescription = "";
        int duration = int.Parse(root.GetAttribute("duration"));
        int uid = int.Parse(root.GetAttribute("uid"));
        int[] uids = null;
        int icon = int.Parse(root.GetAttribute("icon"));
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)root.ChildNodes[i];
                if ("removeDescription".Equals(e.Name))
                {
                    removeDescription = e.InnerText;
                }
                if ("immune".Equals(e.Name))
                {

                    uids = new int[int.Parse(e.GetAttribute("count"))];
                    int index = 0;
                    for (int j = 0; j < e.ChildNodes.Count; j++)
                    {
                        if (e.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {

                            XmlElement el = (XmlElement)e.ChildNodes[j];
                            uids[index] = int.Parse(el.GetAttribute("value"));
                            index++;
                        }
                    }
                }
            }
        }
        return new StatusImmunity(removeDescription, duration, uid, icon, uids);

    }
}
