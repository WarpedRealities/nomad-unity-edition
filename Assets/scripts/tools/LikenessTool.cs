﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Xml;

public class LikenessTool
{

    public static int ProcessLikeness(string likenessFile, Player player)
    {
        Appearance appearance = player.GetAppearance();
        int score = 0;
        XmlDocument doc = FileTools.GetXmlDocument("description/likeness/" + likenessFile);
        XmlElement root = (XmlElement)doc.FirstChild.NextSibling;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("hasPart".Equals(element.Name))
                {
                    score += appearance.GetPart(element.GetAttribute("part")) != null ? int.Parse(element.GetAttribute("score")) : 0;
                }
                if ("hasValue".Equals(element.Name))
                {
                    BodyPart part = appearance.GetPart(element.GetAttribute("part"));
                    if (part != null)
                    {
                        int value = part.GetValue(element.GetAttribute("variable"));
                        int op = GetComp(element);
                        int comp = int.Parse(element.GetAttribute("value"));
                        switch (op)
                        {
                            case 0:
                                score += value == comp ? int.Parse(element.GetAttribute("score")) : 0;
                                break;
                            case 1:
                                score += value >= comp ? int.Parse(element.GetAttribute("score")) : 0;
                                break;
                            case -1:
                                score += value < comp ? int.Parse(element.GetAttribute("score")) : 0;
                                break;
                        }
                    }
                }
            }
        }
        return score;
    }

    private static int GetComp(XmlElement element)
    {
        string op = element.GetAttribute("operator");
        if ("GREATERTHAN".Equals(op))
        {
            return 1;
        }
        if ("LESSTHAN".Equals(op))
        {
            return -1;
        }
        return 0;
    }
}
