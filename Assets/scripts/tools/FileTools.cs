﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using System.IO;
using System;

public class FileTools
{
   
    public static XmlReader getXML_reader(string filename)
    {
        Debug.Log(filename);
        if (File.Exists(filename))
        {
            FileStream fileStream = File.Open(filename,FileMode.Open);
            XmlReader xmlReader = XmlReader.Create(fileStream);

            return xmlReader;
        }
        return null;
    }
    public static string GetFileRaw(string filename)
    {
        
        if (File.Exists("gameData/data/" + filename))
        {
          
            FileStream fileStream = File.Open("gameData/data/" + filename, FileMode.Open);
            StreamReader reader = new StreamReader(fileStream);

            string contents = reader.ReadToEnd();
            reader.Close();
         
            return contents;
        }
        Debug.Log("file isn't found "+ "gameData/data/" + filename);
        return null;
    }

    public static XmlDocument GetXmlDocument(string filename)
    {
       
        if (File.Exists("gameData/data/" + filename + ".xml"))
        {
   
            FileStream fileStream = File.Open("gameData/data/"+filename+".xml", FileMode.Open);
            XmlDocument document = new XmlDocument();
            document.Load(fileStream);
            fileStream.Close();
            return document;
        }
        Debug.Log("file does not exist" + filename);
        return null;
    }

    internal static void DeleteContents(string saveFile)
    {
        string[] strings = Directory.GetFiles(saveFile, "*", SearchOption.AllDirectories);
        for (int i = 0; i < strings.Length; i++)
        {
            //  strings[i]=strings[i].Replace("\\","/");
            if (File.Exists(strings[i]))
            {
                File.Delete(strings[i]);
            }
        }
    }
}
