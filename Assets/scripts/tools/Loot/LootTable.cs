﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTable 
{
    string tableName;
    List<Loot> lootEntries;

    public LootTable(List<Loot> lootEntries)
    {
        this.lootEntries = lootEntries;
    }

    public int CountEntries()
    {
        return lootEntries.Count;
    }

    public Loot GetEntry(int index)
    {
        return lootEntries[index];
    }
}
