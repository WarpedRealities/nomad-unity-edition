﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

public class LootItemGenerator
{
    internal static Item BuildItem(string codeID,string addendum)
    {
        if ("QUEST".Equals(codeID))
        {
            return BuildQuestItem(addendum);
        }
        Item item = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(codeID);

        if (item.GetDef().GetItemType() == ItemType.EXPOSITION)
        {
            ItemExpositionInstance itemExposition = (ItemExpositionInstance)item;
            string[] split = addendum.Split('#');
            itemExposition.SetExpositionFile(split[1]);
            itemExposition.SetExpositionName(split[0]);
        }

        if (item.GetDef().GetItemType() == ItemType.KEY)
        {
            ItemKeyInstance itemExposition = (ItemKeyInstance)item;
            string[] split = addendum.Split('#');
            itemExposition.SetKeyCode(split[1]);
            itemExposition.SetKeyName(split[0]);
        }
        if (item.GetDef().GetItemType() == ItemType.BLUEPRINT)
        {
            ItemBlueprintInstance itemBlueprint = (ItemBlueprintInstance)item;
            string[] split = addendum.Split('#');
            itemBlueprint.SetRecipeCode(split[1]);
            itemBlueprint.SetRecipeName(split[0]);
        }
        if (item.GetDef().GetItemType() == ItemType.AMMO)
        {
            AmmoInstance itemAmmo = (AmmoInstance)item;
            if (itemAmmo.GetResource() != null && addendum.Length>3)
            {

                string[] strings = addendum.Split('#');

                itemAmmo.GetResource().SetAmmoCode(strings[0]);
                itemAmmo.GetResource().SetAmmoType(GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(strings[0]));
                itemAmmo.GetResource().SetAmount(int.Parse(strings[1]));
            }
        }

        if (item.GetDef().GetItemType() == ItemType.EQUIP)
        {
            EquipInstance equipInstance = (EquipInstance)item;
            ResourceStoreInstance resourceStore = equipInstance.GetResourceStore();
            if (resourceStore != null)
            {
                resourceStore.SetAmmoType(GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(resourceStore.GetDef().GetDefaultAmmoType()));
                if (resourceStore.GetCapacity() == 0)
                {
                    string[] str = addendum.Split('#');
                    ItemAmmo itemAmmo = (ItemAmmo)GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(str[0]);
                    if (itemAmmo != null)
                    {
                        resourceStore.SetCapacity(itemAmmo.GetResourceStore().GetCapacity());
                        resourceStore.SetAmmoCode(itemAmmo.GetCodeName());
                        resourceStore.SetAmount(int.Parse(str[1]));
                    }
                }
                else
                {
                    if (resourceStore.GetDef().GetCompatabilities().Count > 1)
                    {

                    }
                    else
                    {
                        int amount = 0;
                        int.TryParse(addendum, out amount);
                        resourceStore.SetAmount(amount);
                    }
                }
            }
        }

        return item;
    }

    static Item BuildQuestItem(string filename)
    {
        XmlDocument xmlDocument = FileTools.GetXmlDocument("/questItems/"+filename);
        XmlElement root = (XmlElement)xmlDocument.FirstChild.NextSibling;
        string code = root.GetAttribute("code");
        string name = root.GetAttribute("name");
        float weight = float.Parse(root.GetAttribute("weight"));
        int value = int.Parse(root.GetAttribute("value"));
        string description = root.InnerText;

        ItemDef itemDef = new ItemQuest(code,name,description,value,weight);

        return new ItemQuestInstance(itemDef);
    }
}
