﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class LootTableBuilder
{
    public static LootTable Build(XmlElement root)
    {
        List<Loot> lootEntries = new List<Loot>();
        XmlNodeList children = root.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("lootEntry".Equals(element.Name))
                {
                    lootEntries.Add(BuildEntry(element));
                }
                if ("lootSubTable".Equals(element.Name))
                {
                    lootEntries.Add(BuildLootSubTable(element));
                }
            }
      
        }
        return new LootTable(lootEntries);
    }

    public static LootSubTable BuildLootSubTable(XmlElement element)
    {
        LootQualifier qualifier = default;

        int max = 0;
        int min = 0;
        if (!int.TryParse(element.GetAttribute("max"), out max))
        {
            max = int.Parse(element.GetAttribute("count"));
            min = max;
        }
        else
        {
            min = int.Parse(element.GetAttribute("min"));
        }


        float probability = float.Parse(element.GetAttribute("probability"));
        string ID = element.GetAttribute("id");

        return new LootSubTable(probability, LootSubTableReferenceLibrary.GetInstance().GetSubtTable(ID), qualifier);
    }

    private static LootEntry BuildEntry(XmlElement element)
    {
        LootQualifier qualifier = default;
        string addendum = element.GetAttribute("addendum");
        int max = 0;
        int min = 0;
        if (!int.TryParse(element.GetAttribute("max"),out max))
        {
            max = int.Parse(element.GetAttribute("count"));
            min = max;
        }
        else
        {
            min = int.Parse(element.GetAttribute("min"));
        }

        string qual = element.GetAttribute("qualifier");
        if ("UNIQUE".Equals(qual))
        {
            qualifier = LootQualifier.UNIQUE;
        }
        float probability = float.Parse(element.GetAttribute("probability"));
        string codeID = element.GetAttribute("id");
        return new LootEntry(codeID, probability, min, max, addendum, qualifier);
    }
}
