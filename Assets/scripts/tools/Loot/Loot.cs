﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface Loot
{

    string GetCode();

    void GetItem(List<Item> items);

    float GetProbability();

    LootQualifier GetQualifier();

}
