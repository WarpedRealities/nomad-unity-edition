﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;

public class LootSubTableReferenceLibrary 
{

    private Dictionary<string, LootSubTableReference> references;

    private static LootSubTableReferenceLibrary instance;

    static public LootSubTableReferenceLibrary GetInstance()
    {
        if (instance == null)
        {
            instance = new LootSubTableReferenceLibrary();
        }
        return instance;
    }

    private LootSubTableReferenceLibrary()
    {
        references = new Dictionary<string, LootSubTableReference>();
    }

    public LootSubTableReference GetSubtTable(string tableName)
    {
        if (!references.ContainsKey(tableName))
        {
            references.Add(tableName, BuildTable(tableName));
        }

        return references[tableName];
    }

    private LootSubTableReference BuildTable(string tableName)
    {
        XmlDocument document = FileTools.GetXmlDocument("lootTables/"+tableName);

        XmlElement root = (XmlElement)document.FirstChild.NextSibling;

        XmlNodeList children = root.ChildNodes;
        List<SubTableEntry> subTableEntries = new List<SubTableEntry>(); 
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("entry".Equals(xmlElement.Name)){
                    subTableEntries.Add(new SubTableEntry(xmlElement.GetAttribute("item"), 
                        int.Parse(xmlElement.GetAttribute("count")),
                        int.Parse(xmlElement.GetAttribute("probability")),
                        xmlElement.GetAttribute("addendum")));
                }
            }
        }

        return new LootSubTableReference(subTableEntries);
       
    }
}
