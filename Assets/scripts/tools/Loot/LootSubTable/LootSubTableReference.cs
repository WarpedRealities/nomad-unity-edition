﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LootSubTableReference
{
    private List<SubTableEntry> subTableEntries;
    private int[] diceRollEntries;
    public LootSubTableReference(List<SubTableEntry> subTableEntries)
    {
        this.subTableEntries = subTableEntries;
        BuildDiceRollEntries();
    }

    private void BuildDiceRollEntries()
    {
        int count = 0;
        for (int i = 0; i < subTableEntries.Count; i++)
        {
            count += subTableEntries[i].GetProbability();
        }
        diceRollEntries = new int[count];
        int index = 0;
        for (int i = 0; i < subTableEntries.Count; i++)
        {
            for (int j = 0; j < subTableEntries[i].GetProbability(); j++)
            {
                diceRollEntries[index] = i;
                index++;
            }
        }      
    }

    internal SubTableEntry GetEntry()
    {
        int diceRoll = DiceRoller.GetInstance().RollDice(diceRollEntries.Length);
        return subTableEntries[diceRollEntries[diceRoll]];
    }

}
