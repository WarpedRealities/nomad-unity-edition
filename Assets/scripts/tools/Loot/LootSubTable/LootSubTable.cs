﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LootSubTable : Loot
{
    
    LootSubTableReference reference;
    float probability;
    LootQualifier lootQualifier;

    public LootSubTable(float probability,LootSubTableReference reference, LootQualifier lootQualifier)
    {
        this.reference = reference;
        this.probability = probability;
        this.lootQualifier = lootQualifier;
    }

    public string GetCode()
    {
        return reference.GetEntry().GetCode();
    }

    public Item GenerateItem()
    {

        SubTableEntry entry = reference.GetEntry();

        int number = entry.GetCount();
        ItemDef item = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(entry.GetItemCode());
        if (number == 1)
        {
            return (LootItemGenerator.BuildItem(entry.GetItemCode(), entry.GetAddendum()));
        }
        else
        {

            if (item.IsStackable())
            {
                if (item.GetItemType() == ItemType.AMMO)
                {
                    return (new ItemAmmoStack((ItemAmmo)item,
                        number,
                        GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(entry.GetAddendum())));
                }
                else
                {
                   return (new ItemStack(item, number));
                }
            }
            else
            {
                for (int i = 0; i < number; i++)
                {
                   return (LootItemGenerator.BuildItem(entry.GetItemCode(), entry.GetAddendum()));
                }
            }
        }
        return null;
    }

    public void GetItem(List<Item> items)
    {
    
        SubTableEntry entry = reference.GetEntry();

        int number = entry.GetCount();
        ItemDef item = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(entry.GetItemCode());
        if (number == 1)
        {
            items.Add(LootItemGenerator.BuildItem(entry.GetItemCode(), entry.GetAddendum()));
        }
        else
        {

            if (item.IsStackable())
            {
                if (item.GetItemType() == ItemType.AMMO)
                {
                    items.Add(new ItemAmmoStack((ItemAmmo)item,
                        number,
                        GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(entry.GetAddendum())));
                }
                else
                {
                    items.Add(new ItemStack(item, number));
                }
            }
            else
            {
                for (int i = 0; i < number; i++)
                {
                    items.Add(LootItemGenerator.BuildItem(entry.GetItemCode(), entry.GetAddendum()));
                }
            }
        }
    }

    public float GetProbability()
    {
        return probability;
    }

    public LootQualifier GetQualifier()
    {
        return lootQualifier;
    }
}
