﻿using UnityEngine;
using System.Collections;
using System;

public class SubTableEntry 
{
    int probability,count;
    string itemCode, addendum;
   
    public SubTableEntry(string itemCode, int count, int probability, string addendum)
    {
        this.itemCode = itemCode;
        this.probability = probability;
        this.addendum = addendum;
        this.count = count;
    }


    public string GetCode()
    {
        return itemCode;
    }

    public int GetProbability()
    {
        return probability;
    }

    public string GetItemCode()
    {
        return itemCode;
    }

    public string GetAddendum()
    {
        return addendum;
    }

    public int GetCount()
    {
        return count;
    }

}
