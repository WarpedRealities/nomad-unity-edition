﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTableTool
{
    private LootTable lootTable;
    private DiceRoller diceRoller;
    public LootTableTool(LootTable lootTable)
    {
        this.diceRoller = DiceRoller.GetInstance();
        this.lootTable = lootTable;
    }
   
    public List<Item> GetLoot()
    {
        List<Item> items = new List<Item>();
        for (int i = 0; i < lootTable.CountEntries(); i++)
        {
            Loot lootEntry = lootTable.GetEntry(i);
            if (lootEntry.GetProbability() >= 1 && lootEntry.GetQualifier()!= LootQualifier.PLACED)
            {
                lootEntry.GetItem(items);
                //AddItem(items, lootEntry);
            }
            else
            {
                float r = diceRoller.RollOdds();

                if (r < lootEntry.GetProbability() && lootEntry.GetQualifier() != LootQualifier.PLACED)
                {
                    lootEntry.GetItem(items);
                    //AddItem(items, lootEntry);
                }
            }
           
        }


        return items;
    }
}
