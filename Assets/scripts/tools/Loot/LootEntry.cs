﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootEntry : Loot
{
    string codeID;
    float probability;
    int minCount, maxCount;
    LootQualifier lootQualifier;
    string addendum;

    public LootEntry(string codeID, float probability, int min, int max, string addendum, LootQualifier lootQualifier)
    {
        this.codeID = codeID;
        this.probability = probability;
        this.minCount = min;
        this.maxCount = max;
        this.addendum = addendum;
        this.lootQualifier = lootQualifier;
    }

    public float GetProbability()
    {
        return probability;
    }

    public int GetMin()
    {
        return minCount;
    }

    public int GetMax() {
        return maxCount;
    }

    public LootQualifier GetQualifier()
    {
        return lootQualifier;
    }


    public void GetItem(List<Item> items)
    {
        if (lootQualifier == LootQualifier.UNIQUE)
        {
            lootQualifier = LootQualifier.PLACED;
        }
        int number = minCount==maxCount ? maxCount : minCount+DiceRoller.GetInstance().RollDice(maxCount-minCount);
        ItemDef item = GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetDefByCode(codeID);
        if (number == 1)
        {
            items.Add(LootItemGenerator.BuildItem(this.codeID, this.addendum));
        }
        else
        {

            if (item.IsStackable())
            {
                if (item.GetItemType() == ItemType.AMMO)
                {
                    items.Add(new ItemAmmoStack((ItemAmmo)item,
                        number,
                        GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetAmmoCode(addendum)));
                }
                else
                {
                    items.Add(new ItemStack(item, number));
                }
            }
            else
            {
                for (int i = 0; i < number; i++)
                {
                    items.Add(LootItemGenerator.BuildItem(this.codeID, this.addendum));
                }
            }
        }
    }

    public string GetCode()
    {
        return codeID;
    }
}
