﻿using UnityEngine;
using System.Collections;
using System;

public class CraftingTool
{
    internal static void CraftItem(Inventory inventory, Recipe currentRecipe)
    {
        SubtractResources(inventory, currentRecipe);

        inventory.AddItem(GlobalGameState.GetInstance().GetUniverse().GetItemLibrary().GetItemByCode(currentRecipe.GetOutput()));
        inventory.CalculateWeight();
    }

    private static void SubtractResources(Inventory inventory, Recipe currentRecipe)
    {
        for (int i = 0; i < currentRecipe.GetIngredients().Count; i++)
        {
            SubtractResource(inventory, currentRecipe.GetIngredients()[i]);
        }
     
    }

    private static void SubtractResource(Inventory inventory, CraftingRequirement craftingRequirement)
    {
        int count = craftingRequirement.GetCount();
        for (int i = inventory.GetItemCount() - 1; i >= 0; i--)
        {
            if (inventory.GetItems()[i].GetDef().GetCodeName().Equals(craftingRequirement.GetItemCode()))
            {
                if (inventory.GetItems()[i].GetItemClass() == ItemClass.REGULAR)
                {
                    inventory.RemoveItem(i);
                    count--;
                }
                else if (inventory.GetItem(i).GetItemClass() == ItemClass.STACK)
                {
                    ItemStack stack = (ItemStack)inventory.GetItem(i);
                    if (stack.GetCount() <= count)
                    {
                        count -= stack.GetCount();
                        inventory.GetItems().Remove(stack);
                    }
                    else
                    {
                        stack.SetCount(stack.GetCount() - count);
                        count = 0;
                    }
                }
                if (count <= 0)
                {

                    return;
                }
            }
        }

    }
}
