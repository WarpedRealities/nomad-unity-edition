﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using System.Text;

public class DescriptionBuilder
{
    internal static string BuildText(Player player)
    {
        XmlDocument document = FileTools.GetXmlDocument("description/description");
        XmlNodeList children = document.FirstChild.NextSibling.ChildNodes;
        StringBuilder stringBuilder=new StringBuilder();

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("block".Equals(xmlElement.Name))
                {
                    //got the bodypart?
                    BodyPart part = player.GetAppearance().GetPart(xmlElement.GetAttribute("bodyPart"));

                    if (part != null)
                    {
                        ProcessBlock(xmlElement, part, stringBuilder, player.GetAppearance());
                    }
                }
                if ("worn".Equals(xmlElement.Name))
                {
                    int slot = int.Parse(xmlElement.GetAttribute("slot"));
                    if (player.GetInventory().GetSlot(slot) != null)
                    {
                        ProcessWorn(xmlElement, player.GetInventory().GetSlot(slot), stringBuilder);
                    }
                }

            }
        }
        return stringBuilder.ToString();
    }

    private static void ProcessWorn(XmlElement xmlElement, Item item, StringBuilder stringBuilder)
    {
        ItemEquippable equip = (ItemEquippable)item.GetDef();
        stringBuilder.Append(equip.GetWornDescription());

    }

    private static void ProcessBlock(XmlElement xmlElement, BodyPart part, StringBuilder stringBuilder, Appearance appearance)
    {
        XmlNodeList children = xmlElement.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Text)
            {
                stringBuilder.Append(((XmlText)children[i]).Value.Replace("\n","").Replace("\t",""));
            }
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)children[i];
                if ("value".Equals(e.Name))
                {
                    stringBuilder.Append(ProcessValue(e, part));
                }
                if ("macro".Equals(e.Name))
                {
                    stringBuilder.Append(DescriptionMacroTool.GetMacro(e.GetAttribute("ID"), appearance));
                }
                if ("sp".Equals(e.Name))
                {
                    stringBuilder.Append(" ");
                }
            }
        }
    }

    private static string ProcessValue(XmlElement e, BodyPart part)
    {
        int remain = 0;
        int divide = 1;
        int.TryParse(e.GetAttribute("rem"), out remain);
        int.TryParse(e.GetAttribute("div"), out divide);
        
        if (remain > 0)
        {
            return (part.GetValue(e.GetAttribute("ID")) % remain).ToString();
        }
        if (divide > 0)
        {
            return (part.GetValue(e.GetAttribute("ID")) / divide).ToString();
        }
        return (part.GetValue(e.GetAttribute("ID"))).ToString();
    }
}
