﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;

public class ObserverVoreHandlerBuilder
{
    

    public static ObserverVoreHandler BuildHandler(string filename)
    {
        XmlDocument document = FileTools.GetXmlDocument("observerVore/" + filename);
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        XmlNodeList children = rootXML.ChildNodes;
        ObserverVoreStage[] stages = new ObserverVoreStage[3];
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("insert".Equals(xmlElement.Name))
                {
                    stages[0] = BuildStage(xmlElement);
                }
                if ("digest".Equals(xmlElement.Name))
                {
                    stages[1] = BuildStage(xmlElement);
                }
                if ("finish".Equals(xmlElement.Name))
                {
                    Debug.Log("load finish");
                    stages[2] = BuildStage(xmlElement);
                }
            }
        }
        ObserverVoreHandler handler = new ObserverVoreHandler(stages);

        return handler;
    }

    private static ObserverVoreStage BuildStage(XmlElement root)
    {
        XmlNodeList children = root.ChildNodes;
        List<String> textStrings = new List<string>();
        String script = "";
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("text".Equals(xmlElement.Name))
                {
                    textStrings.Add(xmlElement.InnerText);
                }
                if ("script".Equals(xmlElement.Name))
                {
                    script = xmlElement.GetAttribute("value");
                }
            }
        }
        return new ObserverVoreStage(textStrings.ToArray(), script);
    }
}
