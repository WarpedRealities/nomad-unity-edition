﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CraftableListGenerator 
{


    public static List<RecipeEntry> BuildList(Player player, CraftingLibrary craftingLibrary)
    {
        List<RecipeEntry> recipes = new List<RecipeEntry>();
        for (int i = 0; i < craftingLibrary.RecipeCount(); i++)
        {
            Recipe recipe = craftingLibrary.GetRecipe(i);
            if (CanCraft(recipe, player)) {
                recipes.Add(new RecipeEntry(recipe, recipe.GetTechRequirement() <= player.GetRPG().getSkill(SK.TECH)));
            }
        }

        return recipes;
    }

    private static bool CanCraft(Recipe recipe, Player player)
    {
        if (!recipe.isUnlocked())
        {

            return false;
        }
        //skill check
   
        if (recipe.GetTechRequirement()-1 > player.GetRPG().getSkill(SK.TECH))
        {

            return false;
        }
        //tag check
        if (recipe.GetTokens() != null)
        {
            for (int i = 0; i < recipe.GetTokens().Count; i++)
            {
                if (!((PlayerRPG)player.GetRPG()).HasCraftingToken(recipe.GetTokens()[i]))
                {

                    return false;
                }
            }
        }
        return true;
    }

    static public bool IngredientCheck(Inventory inventory, CraftingRequirement requirement)
    {
        int count = 0;
        for (int i = 0; i < inventory.GetItemCount(); i++)
        {
            Item item = inventory.GetItems()[i];
            if (item.GetDef().GetCodeName().Equals(requirement.GetItemCode()))
            {
                if (item.GetItemClass() == ItemClass.STACK)
                {
                    ItemStack itemStack = (ItemStack)item;
                    count += itemStack.GetCount();
                }
                else if (item.GetItemClass() == ItemClass.AMMOSTACK)
                {
                    ItemAmmoStack itemStack = (ItemAmmoStack)item;
                    count += itemStack.GetCount();
                }
                else
                {
                    count++;
                }
            }
        }

        return count >= requirement.GetCount();
    }
}
