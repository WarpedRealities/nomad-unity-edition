﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ReloadListTool
{
    internal static Item[] BuildList(Item item, List<Item> items)
    {
        List<string> compatabilities = GetCompatabilities(item);

        List<Item> eligibleItems = new List<Item>();
        for (int i = 0; i < items.Count; i++)
        {
            if (EligibilityCheck(items[i], compatabilities))
            {
                eligibleItems.Add(items[i]);
            }
        }
        return eligibleItems.ToArray();
    }

    private static bool EligibilityCheck(Item item, List<string> compatabilities)
    {
        return compatabilities.Contains(item.GetDef().GetCodeName());
    }

    private static List<string> GetCompatabilities(Item item)
    {
        if (item.GetDef().GetItemType() == ItemType.EQUIP)
        {
            EquipInstance equipInstance = (EquipInstance)item;
            if (equipInstance.GetResourceStore() != null)
            {
                return equipInstance.GetResourceStore().GetDef().GetCompatabilities();
            }
        }
        if (item.GetDef().GetItemType() == ItemType.AMMO)
        {
            AmmoInstance ammoInstance = (AmmoInstance)item;
            if (ammoInstance.GetResource() != null)
            {
                return ammoInstance.GetResource().GetDef().GetCompatabilities();
            }
        }
        throw new NotImplementedException();
    }
}
