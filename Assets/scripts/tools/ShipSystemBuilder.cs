﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using System.Collections.Generic;

public class ShipSystemBuilder
{
    internal static ShipSystem[] BuildSystems(XmlElement root)
    {
        XmlNodeList children = root.ChildNodes;
        ShipSystem[] systems = new ShipSystem[int.Parse(root.GetAttribute("count"))];
        int index = 0;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("systemResource".Equals(element.Name))
                {
                    systems[index] = BuildResource(element);
                    index++;
                }
                if ("systemMagazine".Equals(element.Name))
                {
                    systems[index] = BuildMagazine(element);
                    index++;
                }
                if ("systemFiller".Equals(element.Name))
                {
                    systems[index] = BuildFiller(element);
                    index++;
                }
                if ("systemConverter".Equals(element.Name))
                {
                    systems[index] = BuildConverter(element);
                    index++;
                }
                if ("systemModifier".Equals(element.Name))
                {
                    systems[index] = BuildModifier(element);
                    index++;
                }
                if ("systemWeapon".Equals(element.Name))
                {
                    systems[index] = BuildWeapon(element);
                    index++;
                }
            }
        }
        return systems;
    }

    private static ShipSystem BuildWeapon(XmlElement root)
    {
        CommonWeapon weapon = null;
        for (int i=0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("directWeapon".Equals(element.Name))
                {
                    weapon = BuildDirectWeapon(element);
                }
                if ("strikeWeapon".Equals(element.Name))
                {
                    weapon = BuildStrikeWeapon(element);
                }
            }
        }

        return new WeaponSystem(root.GetAttribute("name"), EnumTools.strToFiringArc(root.GetAttribute("arc")), weapon);
    }

    public static CommonWeapon BuildStrikeWeapon(XmlElement root)
    {
        int volley = 1;
        int.TryParse(root.GetAttribute("volley"), out volley);
        volley = volley < 1 ? 1 : volley;
        int range = int.Parse(root.GetAttribute("range"));
        int life = 0, health = 0, skill = 0, defence = 0;
        int magazine = 1;
        int reload = 0;
        float speed = 0;
        StrikerVisuals visuals = null;
        WeaponCost[] costs = null;
        WeaponEffect[] effects = null;
        Dictionary<string, AmmoModifier> modifiers = new Dictionary<string, AmmoModifier>();
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("costs".Equals(element.Name))
                {
                    costs = BuildWeaponCosts(element);
                }
                if ("visuals".Equals(element.Name))
                {
                    visuals = BuildStrikerVisuals(element);
                }
                if ("modifiers".Equals(element.Name))
                {
                    modifiers = BuildAmmoModifiers(element);
                }
                if ("magazine".Equals(element.Name))
                {
                    magazine = int.Parse(element.GetAttribute("amount"));
                    reload = int.Parse(element.GetAttribute("reload"));
                }
                if ("effects".Equals(element.Name))
                {
                    effects = BuildWeaponEffects(element);
                }
                if ("striker".Equals(element.Name))
                {
                    life = int.Parse(element.GetAttribute("lifespan"));
                    health = int.Parse(element.GetAttribute("health"));
                    skill = int.Parse(element.GetAttribute("skill"));
                    defence = int.Parse(element.GetAttribute("defence"));
                    speed = float.Parse(element.GetAttribute("speed"));
                }
            }
        }
        return new StrikeWeapon(speed, life, skill,defence, health, volley, range, magazine,reload, visuals, costs, effects, modifiers);
    }

    private static StrikerVisuals BuildStrikerVisuals(XmlElement element)
    {
        return new StrikerVisuals(element.GetAttribute("spritesheet"));
    }

    public static CommonWeapon BuildDirectWeapon(XmlElement root)
    {
        int volley = 1;
        int.TryParse(root.GetAttribute("volley"), out volley);
        volley = volley < 1 ? 1 : volley;
        int range = int.Parse(root.GetAttribute("range"));
        int accMod = 0;
        int magazine = 1;
        int reload = 0;
        int.TryParse(root.GetAttribute("accuracy"), out accMod);
        float rangeModifier = 0;
        float.TryParse(root.GetAttribute("rangeMod"), out rangeModifier);
        WeaponVisuals visuals = null;
        WeaponCost[] costs = null;
        WeaponEffect[] effects = null;
        Dictionary<string, AmmoModifier> modifiers = new Dictionary<string, AmmoModifier>();
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("costs".Equals(element.Name))
                {
                    costs = BuildWeaponCosts(element);
                }
                if ("visuals".Equals(element.Name))
                {
                    visuals = BuildWeaponVisuals(element);
                }
                if ("modifiers".Equals(element.Name))
                {
                    modifiers = BuildAmmoModifiers(element);
                }
                if ("magazine".Equals(element.Name))
                {
                    magazine = int.Parse(element.GetAttribute("amount"));
                    reload = int.Parse(element.GetAttribute("reload"));
                }
                if ("effects".Equals(element.Name))
                {
                    effects = BuildWeaponEffects(element);
                }
            }
        }

        return new DirectWeapon(range, volley,accMod,rangeModifier, magazine, reload,
            visuals, costs, effects, modifiers);
    }

    private static WeaponEffect[] BuildWeaponEffects(XmlElement element)
    {
        WeaponEffect[] effects = new WeaponEffect[int.Parse(element.GetAttribute("count"))];
        int index = 0;
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                if ("damageEffect".Equals(e.Name))
                {
                    float rangeDecay = 0;
                    float.TryParse(e.GetAttribute("decay"), out rangeDecay);
                    DT d = EnumTools.StrToDamageType(e.GetAttribute("damageType"));
                    int min = int.Parse(e.GetAttribute("min"));
                    int max = int.Parse(e.GetAttribute("max"));
                    effects[index] = new DamageEffect(d,min,max,rangeDecay);
                    index++;
                }
            }
        }
        return effects;
    }

    private static Dictionary<string, AmmoModifier> BuildAmmoModifiers(XmlElement element)
    {
        Dictionary<string, AmmoModifier> modifiers = new Dictionary<string, AmmoModifier>();
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                if ("modifier".Equals(e.Name))
                {
                    Dictionary<string, string> modElements = new Dictionary<string, string>();
                    for (int j = 0; j < e.ChildNodes.Count; j++)
                    {
                        if (e.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement xe = (XmlElement)e.ChildNodes[j];
                            if ("variable".Equals(xe.Name))
                            {
                                modElements.Add(xe.GetAttribute("key"), xe.GetAttribute("value"));
                            }
                        }
                    }
                    AmmoModifier mod = new AmmoModifier(e.GetAttribute("code"), modElements);
                    modifiers.Add(mod.ammoCode, mod);
                }
            }
        }
        return modifiers;
    }

    private static WeaponVisuals BuildWeaponVisuals(XmlElement element)
    {
        float volleyInterval = 0;
        float.TryParse(element.GetAttribute("volley"),out volleyInterval);
        return new WeaponVisuals("true".Equals(element.GetAttribute("isBeam")),
            element.GetAttribute("spritesheet"), float.Parse(element.GetAttribute("speed")), volleyInterval);
    }

    private static WeaponCost[] BuildWeaponCosts(XmlElement element)
    {
        WeaponCost[] costs = new WeaponCost[int.Parse(element.GetAttribute("count"))];
        int index = 0;
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                if ("resourceCost".Equals(e.Name))
                {
                    costs[index] = new ResourceCost(e.GetAttribute("resource"),e.GetAttribute("displayName"), int.Parse(e.GetAttribute("amount")));
                    index++;
                }
                if ("magazineCost".Equals(e.Name))
                {
                    MagazineCost magazineCost = new MagazineCost(int.Parse(e.GetAttribute("amount")),e.GetAttribute("description"));
                    Accept[] accept = new Accept[int.Parse(e.GetAttribute("acceptCount"))];
                    int J = 0;
                    for (int j = 0; j < e.ChildNodes.Count; j++)
                    {
                        if (e.ChildNodes[j].NodeType == XmlNodeType.Element)
                        {
                            XmlElement xe = (XmlElement)e.ChildNodes[j];
                            if ("accept".Equals(xe.Name))
                            {
                                accept[J] = new Accept(xe.GetAttribute("value"), xe.GetAttribute("desc"));
                                J++;
                            }
                        }
                    }
                    magazineCost.SetAccept(accept);
                    costs[index] = magazineCost;
                    index++;
                }
            }
        }
        return costs;

    }

    private static ShipSystem BuildMagazine(XmlElement root)
    {
        XmlNodeList children = root.ChildNodes;
        int capacity = int.Parse(root.GetAttribute("capacity"));
        string[] codes = new string[int.Parse(root.GetAttribute("acceptCount"))];
        int index = 0;
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("accept".Equals(element.Name))
                {
                    codes[index] = element.GetAttribute("code");
                    index++;
                }
            }
        }
        return new MagazineSystem(capacity, codes);

    }

    private static ShipSystem BuildModifier(XmlElement root)
    {
        XmlNodeList children = root.ChildNodes;
        SolarModifiers solarModifiers=null;
        int uid = int.Parse(root.GetAttribute("uid"));
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)children[i];
                if ("solarMods".Equals(element.Name)){
                    float efficiency = 1;
                    float detection = 0;
                    float speed = 1;
                    float sensors = 0;
                    float ftl = 0;
                    if (element.GetAttribute("efficiency").Length > 1)
                    {
                        float.TryParse(element.GetAttribute("efficiency"), out efficiency);
                    }
                    if (element.GetAttribute("detection").Length > 1)
                    {
                        float.TryParse(element.GetAttribute("detection"), out detection);
                    }
                    float.TryParse(element.GetAttribute("speed"), out speed);
                    float.TryParse(element.GetAttribute("sensors"), out sensors);
                    float.TryParse(element.GetAttribute("ftl"), out ftl);
                    solarModifiers = new SolarModifiers(efficiency, detection, speed, sensors, ftl);
                }
            }
        }
        return new ModifierSystem(solarModifiers, uid);
    }

    private static ConversionSystem BuildConverter(XmlElement element)
    {
        string resourceTo = element.GetAttribute("to");
        string resourceFrom = element.GetAttribute("from");
        float rate = float.Parse(element.GetAttribute("rate"));
        float ratio = float.Parse(element.GetAttribute("ratio"));
        string desc = element.InnerText;
        return new ConversionSystem(rate, ratio, resourceTo, resourceFrom, desc);
    }

    private static FillerSystem BuildFiller(XmlElement element)
    {

        int itemCount = int.Parse(element.GetAttribute("slotCount"));
        string resourceID = element.GetAttribute("resource");
        string displayName = element.GetAttribute("displayName");
        float resourceRatio = float.Parse(element.GetAttribute("ratio"));
        float fillRate = float.Parse(element.GetAttribute("rate"));
        return new FillerSystem(resourceID, displayName, resourceRatio, fillRate, itemCount);
    }

    private static ResourceSystem BuildResource(XmlElement element)
    {
        int capacity = int.Parse(element.GetAttribute("capacity"));
        string resId = element.GetAttribute("resource");
        string displayName = element.GetAttribute("displayName");
        return new ResourceSystem(resId, displayName, capacity);
    }
}
