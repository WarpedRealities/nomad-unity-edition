﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

public class BodyPartLoader
{
    public static BodyPart LoadPart(string partName)
    {
    
        XmlDocument document = FileTools.GetXmlDocument("description/parts/"+partName);
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        XmlNodeList children = rootXML.ChildNodes;

        BodyPart bodyPart = new BodyPart(partName);

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];
                if ("variable".Equals(xmlElement.Name))
                {
                    bodyPart.SetValue(xmlElement.GetAttribute("name"), int.Parse(xmlElement.GetAttribute("value")));
                }
            }
        }
        return bodyPart;
    }
}
