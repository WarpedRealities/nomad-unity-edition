﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

public class CraftingRecipeLoader
{
    internal static List<Recipe> LoadRecipes()
    {
        //get all files in this directory
        string[] strings = Directory.GetFiles("gameData/data/recipes", "*", SearchOption.AllDirectories);
        List<Recipe> recipes = new List<Recipe>();
        for (int i = 0; i < strings.Length; i++)
        {
            //  strings[i]=strings[i].Replace("\\","/");

            if (File.Exists(strings[i]))
            {

                FileStream fileStream = File.Open(strings[i], FileMode.Open);
                XmlDocument document = new XmlDocument();
                document.Load(fileStream);
                Recipe recipe = BuildRecipe(document);
                recipes.Add(recipe);
            }
        }
        return recipes;
    }

    internal static void LoadRecipes(List<Recipe> recipeList)
    {
        string[] strings = Directory.GetFiles("gameData/data/recipes", "*", SearchOption.AllDirectories);
        for (int i = 0; i < strings.Length; i++)
        {
            //  strings[i]=strings[i].Replace("\\","/");

            if (File.Exists(strings[i]))
            {

                FileStream fileStream = File.Open(strings[i], FileMode.Open);
                XmlDocument document = new XmlDocument();
                document.Load(fileStream);
                Recipe recipe = BuildRecipe(document);
                if (NoMatch(recipe.GetName(), recipeList))
                {
                    recipeList.Add(recipe);
                }
            }
        }

    }

    private static bool NoMatch(string name, List<Recipe> recipeList)
    {
        for (int i=0; i < recipeList.Count; i++)
        {
            if (recipeList[i].GetName().Equals(name))
            {
                return false;
            }
        }
        return true;
    }

    private static Recipe BuildRecipe(XmlDocument document)
    {
        XmlElement root = (XmlElement)document.FirstChild.NextSibling;
        XmlNodeList children = root.ChildNodes;
        string name = root.GetAttribute("name");
        string description=null;
        string outputItem = null;
        int outputCount = 1;
        bool startunlocked = "true".Equals(root.GetAttribute("startUnlocked"));

        int skillRank = int.Parse(root.GetAttribute("requiredSkill"));
        List<CraftingRequirement> ingredients = new List<CraftingRequirement>();
        List<CraftingToken> tokens = new List<CraftingToken>();
        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];

                if ("description".Equals(xmlElement.Name))
                {
                    description = xmlElement.InnerText;
                }
                if ("ingredient".Equals(xmlElement.Name))
                {
                    ingredients.Add(new CraftingRequirement(xmlElement.GetAttribute("item"), int.Parse(xmlElement.GetAttribute("count"))));
                }
                if ("output".Equals(xmlElement.Name))
                {
                    outputItem = xmlElement.GetAttribute("item");
                    int.TryParse(xmlElement.GetAttribute("count"), out outputCount);
                }
                if ("token".Equals(xmlElement.Name))
                {
                    tokens.Add(new CraftingToken(xmlElement.GetAttribute("ID"), int.Parse(xmlElement.GetAttribute("rank"))));
                }
            }
        }
        Recipe recipe = new Recipe(name, description, outputItem, outputCount, skillRank, startunlocked);
        if (tokens.Count > 0)
        {
            recipe.SetTokens(tokens);
        }
        if (ingredients.Count > 0)
        {
            recipe.SetIngredients(ingredients);
        }
        return recipe;
    }
}
