using System;
using System.Collections.Generic;
using UnityEngine;

public class NuLandingOption
{
    public LandingSite landingSite;
    public Zone zone;
    public LANDINGOPTIONSTATE landingOptionState;
    public Vector2Int p;

    public NuLandingOption(LandingSite landingSite, Zone zone, LANDINGOPTIONSTATE state, 
        Vector2Int p)
    {
        this.p = p;
        this.landingOptionState = state;
        this.landingSite = landingSite;
        this.zone = zone;
    }
}

public class NuLandingOptionsGenerator
{
    Entity entity;

    public NuLandingOptionsGenerator(Entity entity)
    {
        this.entity = entity;
    }
    public NuLandingOption[] GenerateOptions()
    {
        switch (entity.GetEntityType())
        {
            case ENTITYTYPE.PLANET:
                return GeneratePlanetOptions();

            case ENTITYTYPE.STATION:

                break;
        }
        return null;
    }

    internal void GenerateHeightWidth(NuLandingOption[] landingOptions, out float width, out float height)
    {
        int x = 0;
        int y = 0;
        for (int i = 0; i < landingOptions.Length; i++)
        {
            if (landingOptions[i].p.x > x)
            {
                x = landingOptions[i].p.x;
            }
            if (landingOptions[i].p.y > y)
            {
                y = landingOptions[i].p.y;
            }
        }
        width = x;
        height = y;
    }

    private NuLandingOption[] GeneratePlanetOptions()
    {
        List<NuLandingOption> landingOptions = new List<NuLandingOption>();
        Planet planet = (Planet)entity;
        for (int i = 0; i < planet.GetContents().GetZones().Count; i++)
        {
            PlanetZone zone = planet.GetContents().GetZones()[i];
            LandingSite landingSite = GetLandingSite(planet.GetContents().GetZones()[i], planet);
            if (zone.GetZoneType() == ZoneType.SURFACE)
            {
                landingOptions.Add(new NuLandingOption(landingSite, zone.GetZone(), GetState(zone, landingSite),
                    new Vector2Int(zone.GetPosition().x,zone.GetPosition().y)));
            }
        }


        NuLandingOption[] options = new NuLandingOption[landingOptions.Count];
        for (int i = 0; i < landingOptions.Count; i++)
        {
            options[i] = landingOptions[i];
        }
        return options;
    }

    private LandingSite GetLandingSite(PlanetZone planetZone, Planet planet)
    {
        return planet.GetLandingSite(planetZone.GetZone());
    }
    private LANDINGOPTIONSTATE GetState(PlanetZone zone, LandingSite landingSite)
    {
        if (!zone.GetZone().IsGenerated())
        {
            return LANDINGOPTIONSTATE.UNEXPLORED;
        }
        if (landingSite != null)
        {
            return LANDINGOPTIONSTATE.TAKEN;
        }
        return LANDINGOPTIONSTATE.ACCESSIBLE;
    }
}
