using System;
using System.Collections;
using System.Xml;
using UnityEngine;


public class CaptureTool
{
    internal static bool CanCapture(ItemCaptureInstance captureDevice)
    {
        //check for named zone on current entity
        Zone z = GlobalGameState.GetInstance().getCurrentEntity().GetZone(captureDevice.GetShipName());
        if (z != null)
        {
            return HasCapacity(z);
        }
        throw new NotImplementedException();
    }

    public static Zone GetCaptureZone(ItemCaptureInstance captureDevice)
    {
        Zone z = GlobalGameState.GetInstance().getCurrentEntity().GetZone(captureDevice.GetShipName());
        if (z != null)
        {
            return z;
        }

        return null;
    }

    internal static void CaptureNPC(DialogueActor actor, Player player)
    {
        ItemCaptureInstance captureDevice = GetCaptureInstance(player);
        Zone z = GetCaptureZone(captureDevice);
        if (z != null)
        {
            WidgetJail jail = GetJail(z);
            jail.AddCaptive(actor.GetActor());  
        }
    }

    public static ItemCaptureInstance GetCaptureInstance(Player player) {
        Item item = player.GetInventory().GetItem("00CAPTURE");
        if (item is ItemCaptureInstance)
        {
            ItemCaptureInstance captureDevice = (ItemCaptureInstance)item;
            return captureDevice;
        }
        return null;
    }

    private static WidgetJail GetJail(Zone zone)
    {
        if (zone.GetContents() != null)
        {
            for (int i = 0; i < zone.GetContents().GetZoneParameters().GetWidth(); i++)
            {
                for (int j = 0; j < zone.GetContents().GetZoneParameters().GetHeight(); j++)
                {
                    Tile t = zone.GetContents().GetTile(i, j);
                    if (t != null)
                    {
                        if (t.GetWidget() != null && t.GetWidget() is WidgetJail)
                        {

                        }
                        if (t.GetWidget() != null && t.GetWidget() is WidgetSlot)
                        {
                            WidgetSlot widgetSlot = (WidgetSlot)t.GetWidget();
                            if (widgetSlot.GetContainedWidget() != null &&
                                widgetSlot.GetContainedWidget() is WidgetJail)
                            {
                                WidgetJail widgetJail = (WidgetJail)widgetSlot.GetContainedWidget();
                                for (int k = 0; k < widgetJail.GetCaptives().Length; k++)
                                {
                                    if (widgetJail.GetCaptives()[k] == null)
                                    {
                                        return widgetJail;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    private static bool HasCapacity(Zone zone)
    {
        return GetJail(zone)!=null;
    }

    internal static void RemoveNPC(Actor actor, Widget widget)
    {
        WidgetJail widgetJail = (WidgetJail)widget;
        widgetJail.RemoveCaptive(actor);
    }

    internal static Spaceship GetDockedShip()
    {
        Zone z = GlobalGameState.GetInstance().getCurrentZone();
        Entity entity = GlobalGameState.GetInstance().getCurrentEntity();
        if (entity.GetEntityType().Equals(ENTITYTYPE.PLANET))
        {
            Planet planet = (Planet)entity;
            LandingSite landingSite = planet.GetLandingSite(z.getName());
            if (landingSite != null)
            {
                return landingSite.GetSpaceship();
            }
        }
        if (entity.GetEntityType().Equals(ENTITYTYPE.STATION))
        {
            Station station = (Station)entity;
            LandingSite landingSite = station.GetLandingSite(z.getName());
            if (landingSite != null)
            {
                return landingSite.GetSpaceship();
            }
        }
        return null;
    }
}