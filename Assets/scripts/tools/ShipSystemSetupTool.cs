﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;

public class ShipSystemSetupTool
{
    internal static void SetupSystem(WidgetShipSystem widgetShipSystem, XmlElement root)
    {
        XmlNodeList children = root.ChildNodes;

        for (int i = 0; i < children.Count; i++)
        {
            if (children[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)children[i];

                if ("resource".Equals(xmlElement.Name))
                {
                    SetResource(widgetShipSystem, xmlElement.GetAttribute("type"), int.Parse(xmlElement.GetAttribute("amount")));
                }
                if ("magazine".Equals(xmlElement.Name))
                {
                    SetMagazine(widgetShipSystem, xmlElement.GetAttribute("item"), int.Parse(xmlElement.GetAttribute("amount")));
                }
            }

        }
    }

    private static void SetMagazine(WidgetShipSystem widgetShipSystem, string item, int amount)
    {
        for (int i=0;i< widgetShipSystem.GetSystems().Length; i++)
        {
            if (widgetShipSystem.GetSystems()[i].GetSystemType() == SHIPSYSTEMTYPE.MAGAZINE)
            {
                MagazineSystem magazineSystem = (MagazineSystem)widgetShipSystem.GetSystems()[i];
                magazineSystem.SetAmount(amount);
                magazineSystem.SetCode(item);
            }
        }

    }

    private static void SetResource(WidgetShipSystem widgetShipSystem, string resource, int amount)
    {
        for (int i = 0; i < widgetShipSystem.GetSystems().Length; i++)
        {
            if (widgetShipSystem.GetSystems()[i].GetSystemType() == SHIPSYSTEMTYPE.RESOURCE)
            {
          
                ResourceSystem resourceSystem = (ResourceSystem)widgetShipSystem.GetSystems()[i];
                if (resourceSystem.GetContentId().Equals(resource))
                {
                    resourceSystem.SetAmount(amount);
                    break;
                }
            }
        }

    }
}
