﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.XR;

public class ShipAnalysisTool 
{

    private List<WidgetShipSystem> systems;
    private Dictionary<string, ShipResource> resources;
    private Dictionary<string, ShipMagazine> magazines;
    private List<ShipConverter> converters;
    private Dictionary<string, List<ResourceSystem>> resourceSystems;
    private Dictionary<string, List<MagazineSystem>> magazineSystems;
    private List<WeaponSystem> weapons;
    private List<WidgetDamage> damageWidgets;
    private int capacity, passengers;
    SpaceshipStats baseStats;
    CommonStats commonstats;
    SolarStats solarStats;
    CombatStats combatStats;
    SpaceshipResourceStats resourceStats;
    Spaceship ship;
    private int shipState;


    private bool noHull = false, noFuel = false, looseItems = false, hostiles = false, capExceeded = false ;
    private const int NOHULL = 1;
    private const int NOFUEL = 2;
    private const int LOOSEITEMS = 4;
    private const int MINIMUM_FUEL = 2;
    private const string FUEL = "RES_FUEL";
    private const string HULL = "RES_HULL";
    private const string LOOSEITEM_MESSAGE = "Loose items unsecured aboard ship, launch unsafe";
    private const string NOFUEL_MESSAGE = "Insufficient fuel for initial boost, launch impossible";
    private const string NOHULL_MESSAGE = "Hull integrity critical, launch unsafe";
    private const string HOSTILE_MESSAGE = "hostiles aboard, control unsafe";
    private const string TRANSIT_MESSAGE = "ship in FTL";
    private const string CANT_LAUNCH = "ship cannot launch due to an external reason";

    public ShipAnalysisTool(Spaceship ship, bool isPlayer=true)
    {
        capacity = 0;
        resources = new Dictionary<string, ShipResource>();
        magazines = new Dictionary<string, ShipMagazine>();
        converters = new List<ShipConverter>();
        resourceSystems = new Dictionary<string, List<ResourceSystem>>();
        magazineSystems = new Dictionary<string, List<MagazineSystem>>();
        systems = new List<WidgetShipSystem>();
        weapons = new List<WeaponSystem>();
        damageWidgets = new List<WidgetDamage>();
        this.ship = ship;
        baseStats = ship.GetSpaceshipStats();
        AddHeatManagement();
        ZoneScan(ship.GetZone(ship.getName()));
        CalculateResources();
        List<ShipResource> res = new List<ShipResource>();
        List<ShipMagazine> mags = new List<ShipMagazine>();
        foreach(KeyValuePair<string,ShipResource> pair in resources)
        {
            res.Add(pair.Value);
        }
        foreach(KeyValuePair<string,ShipMagazine> pair in magazines)
        {
            mags.Add(pair.Value);
        }
        resourceStats = new SpaceshipResourceStats(resources,res,converters,mags);
        if (!resources.ContainsKey(FUEL) || resources[FUEL].amount <= MINIMUM_FUEL)
        {
            noFuel = true;
        }
        if (commonstats.GetHp()<= 0)
        {
            noHull = true;
        }
        CalculateWeapons();
        CalculateSolarStats(isPlayer);
        CalculateCombatStats(isPlayer);
    }

    private void AddHeatManagement()
    {
        ResourceSystem coolant = new ResourceSystem("RES_COOLANT", "coolant", baseStats.GetHP() / 2);
        coolant.SetAmount(coolant.GetCapacity());
        AddResource(coolant);
        ConversionSystem converter = new ConversionSystem(0.001F*baseStats.GetHP(), 1, "RES_COOLANT", "RES_NOTHING", "SHIP HEAT SINK");
        AddConverter(converter);
    }

    private void CalculateWeapons()
    {
        for (int i = 0; i < systems.Count; i++)
        {
            for (int j = 0; j < systems[i].GetSystems().Length; j++)
            {
                if (systems[i].GetSystems()[j].GetSystemType() == SHIPSYSTEMTYPE.WEAPON)
                {
                    weapons.Add((WeaponSystem)systems[i].GetSystems()[j]);
                }
            }
        }
    }

    private void CalculateCombatStats(bool isPlayer)
    {
        float speed = this.ship.GetSpaceshipStats().GetThrust();
        float manouver = this.ship.GetSpaceshipStats().GetManouver();
        float armour = this.ship.GetSpaceshipStats().GetArmour();
        float countermeasures = 0;
        int piloting = isPlayer ? 0 : BuildNPCPiloting();
        int gunnery = isPlayer ? 0 : BuildNPCGunnery();
        int science = isPlayer ? 0 : BuildNPCSensor();
        int tech = 0;
        this.combatStats = new CombatStats(speed,manouver,countermeasures,armour,piloting,gunnery,tech,science,weapons);
    }

    private int BuildNPCGunnery()
    {
        if (ship.GetController() is NPC_Captain)
        {
            return ((NPC_Captain)ship.GetController()).GetSkills().GetGunnery();
        }
        return 0;
    }

    private int BuildNPCPiloting()
    {
        if (ship.GetController() is NPC_Captain)
        {
            return ((NPC_Captain)ship.GetController()).GetSkills().GetPiloting();
        }
        return 0;
    }

    private void CalculateSolarStats(bool isPlayer)
    {
        float detection = baseStats.GetDetection();
        float sensors = isPlayer ? 0 : BuildNPCSensor();
        float efficiency = baseStats.GetFuelCost();

        float cruise = baseStats.GetCruise();
        float ftl = 0;

        List<int> uidList = new List<int>();
        for (int i = 0; i < systems.Count; i++)
        {
            for (int j = 0; j < systems[i].GetSystems().Length; j++)
            {
                if (systems[i].GetSystems()[j].GetSystemType() == SHIPSYSTEMTYPE.MODIFIER)
                {
                    ModifierSystem modifierSystem = (ModifierSystem)systems[i].GetSystems()[j];
                    if (modifierSystem.GetId()==0|| (!uidList.Contains(modifierSystem.GetId())))
                    {
                        SolarModifiers solarModifiers = modifierSystem.GetSolarModifiers();
                        if (solarModifiers != null)
                        {


                            sensors += solarModifiers.sensorMod;
                            detection += solarModifiers.detectionMod;
                            cruise *= cruise;
                            efficiency *= solarModifiers.efficiencyMod;
                            ftl += solarModifiers.ftl;
                        }
                        if (modifierSystem.GetId() != 0)
                        {
                            uidList.Add(modifierSystem.GetId());
                        }
                    }
                }
            }
        }

        solarStats = new SolarStats(cruise,efficiency, sensors, detection, capacity, ftl);
    }

    private int BuildNPCSensor()
    {
        if (ship.GetController() is NPC_Captain)
        {
            return ((NPC_Captain)ship.GetController()).GetSkills().GetScience();
        }
        return 0;
    }

    internal void ChanceOfDamage()
    {
        int dice = DiceRoller.GetInstance().RollDice(4);
        if (dice == 0)
        {
            int r = systems.Count>1 ? DiceRoller.GetInstance().RollDice(systems.Count): 0;
            if (!systems[r].IsDamaged())
            {
                systems[r].SetDamaged(true);
                TextLog.Log("Overload "+ systems[r].GetName()+ " damaged and non functional");
            }
        }
    }

    public SolarStats GetSolarStats()
    {
        return solarStats;
    }

    public CombatStats GetCombatStats()
    {
        return combatStats;
    }

    public Spaceship GetShip()
    {
        return ship;
    }

    public List<WeaponSystem> GetWeapons()
    {
        return weapons;
    }

    public void ApplyStats()
    {
        //resources
        ship.SetResources(resourceStats);
        //common
        ship.SetCommons(commonstats);
        //solarstats
        ship.setSolarStats(solarStats);

        ship.SetCombatStats(combatStats);
    }


    internal void ProcessPlayer(Player player)
    {
        int s = player.GetRPG().getSkill(SK.SCIENCE) * 2;
        s = s < 0 ? 0 : s;
        this.solarStats.ModSensors(s);
        this.combatStats.SetSkills(player.GetRPG().getSkill(SK.PILOTING), player.GetRPG().getSkill(SK.GUNNERY), player.GetRPG().getSkill(SK.TECH), player.GetRPG().getSkill(SK.SCIENCE));
    }

    public void ReconcileStats()
    {
        //handle resources
        resourceStats = ship.GetResources();
        for (int i = 0; i < resourceStats.GetResourceList().Count; i++)
        {
            if (resourceStats.GetResourceList()[i].amount !=
                resourceStats.GetResourceList()[i].originalAmount)
            {
                //Debug.Log("resource" + resourceStats.GetResourceList()[i].displayName
                //    + " amount "
                //    + resourceStats.GetResourceList()[i].amount
                //    + " original "
                //    + resourceStats.GetResourceList()[i].originalAmount);
                ReconcileResource(resourceStats.GetResourceList()[i].resource,
                    resourceStats.GetResourceList()[i].amount,
                    resourceStats.GetResourceList()[i].originalAmount,
                    resourceStats.GetResourceList()[i].capacity);
            }
        }
        for (int i = 0; i < resourceStats.GetMagazineList().Count; i++)
        {
            if (resourceStats.GetMagazineList()[i].amount!=
                resourceStats.GetMagazineList()[i].originalAmount)
            {
                Debug.Log("resource original amount " + resourceStats.GetMagazineList()[i].originalAmount);
                ReconcileMagazine(resourceStats.GetMagazineList()[i].containedItem,
                    resourceStats.GetMagazineList()[i].amount,
                    resourceStats.GetMagazineList()[i].originalAmount);
            }
        }
        //handle adding damage if necessary
        int extraDamage = commonstats.GetHp()- ship.GetCommon().GetHp();
        Debug.Log("reconcile more damage " + extraDamage);
        if (extraDamage > 0)
        {
            //add more damage as necessary
            AddDamage(extraDamage);
        }
    }

    private void AddDamage(int extraDamage)
    {
        for (int i = 0; i < damageWidgets.Count; i++)
        {
            if (damageWidgets[i].GetDamage() < 25)
            {
                int maxcap = 25-damageWidgets[i].GetDamage();
                if (extraDamage >= maxcap)
                {
                    extraDamage -= maxcap;
                    damageWidgets[i].SetDamage(25);
                }
                else
                {
                    damageWidgets[i].SetDamage(damageWidgets[i].GetDamage() + extraDamage);
                    extraDamage = 0;
                }
                if (extraDamage == 0)
                {
                    break;
                }
            }
        }
        if (extraDamage > 0)
        {
            //add new damage spot
            AddNewDamage(extraDamage, 1+ (extraDamage / 25));
        }
    }

    private void AddNewDamage(int extraDamage, int count)
    {
        Zone z = ship.GetZone(ship.getName());
        for (int i = 0; i < count; i++)
        {
            //need a spot of floor, picked at random
            while (true)
            {
                int x = DiceRoller.GetInstance().RollDice(
                    z.GetContents().GetZoneParameters().GetWidth());
                int y = DiceRoller.GetInstance().RollDice(
                    z.GetContents().GetZoneParameters().GetHeight());
                if (z.GetContents().GetTile(x, y) != null)
                {
                    Tile t = z.GetContents().GetTile(x, y);
                    if (t.GetWidget()==null && t.canWalk())
                    {
                        int d = extraDamage;
                        if (extraDamage >= 25)
                        {
                            d = 25;
                        }
                        WidgetDamage widgetDamage = new WidgetDamage(d);
                        extraDamage -= d;
                        t.SetWidget(widgetDamage);
                        break;
                    }
                }

            }
        }
    }

    private void ReconcileMagazine(string containedItem, int amount, int originalAmount)
    {
        int change = originalAmount - amount;

        if (change > 0)
        {
            List<MagazineSystem> magazines = magazineSystems[containedItem];
            for (int i = 0; i < magazines.Count; i++)
            {
                if (magazines[i].GetAmount() <= change)
                {
                    change -= magazines[i].GetAmount();
                    magazines[i].SetAmount(0);
                }
                else
                {
                    magazines[i].SetAmount(magazines[i].GetAmount() - change);
                    change = 0;
                }
            }
        }

    }

    private void ReconcileResource(string resource, float amount, float originalAmount, int capacity)
    {
        float am = amount - originalAmount;

        //Debug.Log("amount to reduce " + am);
        float at = am;

        if (at>0)
        {
            //adding material
            List<ResourceSystem> resourceSystems = this.resourceSystems[resource];
            for (int i = 0; i < resourceSystems.Count; i++)
            {
                if (at > 0)
                {
                    float p = ((float)resourceSystems[i].GetCapacity()) - resourceSystems[i].GetAmount();
                    if (p > at)
                    {
                        resourceSystems[i].SetAmount(resourceSystems[i].GetAmount() + at);
                        at = 0;
                    }
                    else
                    {
                        resourceSystems[i].SetAmount(resourceSystems[i].GetCapacity());
                        at -= p;
                    }
                }

            }
            if (at > 0)
            {
                Debug.Log("leftover material???");
            }
        }
        else if (at<0)
        {
            //removing material
            at = at * -1;
            List<ResourceSystem> resourceSystems = this.resourceSystems[resource];
            for (int i = 0; i < resourceSystems.Count; i++)
            {
                if (at > 0)
                {
                    float p = resourceSystems[i].GetAmount();
                    if (p > at)
                    {
                        resourceSystems[i].SetAmount(resourceSystems[i].GetAmount() - at);
                        at = 0;
                    }
                    else
                    {
                        resourceSystems[i].SetAmount(0);
                        at -= p;
                    }
                }

            }
            if (at > 0)
            {
                Debug.Log("leftover material???");
            }
        }

    }

    public CommonStats GetCommon()
    {
        return this.commonstats;
    }

    public SpaceshipResourceStats GetResources()
    {
        return resourceStats;
    }

    public bool isLaunchCapable()
    {
        return !noHull && 
            !noFuel && 
            !looseItems && 
            !hostiles && 
            !InTransit() &&
            !(this.ship.GetState()==ShipState.UNUSABLE);
    }

    private bool InTransit()
    {
        if (ship.GetWarpHandler() != null && ship.GetWarpHandler().state==WarpState.TRANSIT)
        {
            long clock = GlobalGameState.GetInstance().GetUniverse().GetClock();
            if (clock < ship.GetWarpHandler().jumpStart + ship.GetWarpHandler().duration)
            {
                return true;
            }
        }
        return false;
    }

    public string GetLaunchRestriction()
    {
        if (looseItems)
        {
            return LOOSEITEM_MESSAGE;
        }
        if (noFuel)
        {
            return NOFUEL_MESSAGE;
        }
        if (noHull)
        {
            return NOHULL_MESSAGE;
        }
        if (hostiles)
        {
            return HOSTILE_MESSAGE;
        }
        if (InTransit())
        {
            return TRANSIT_MESSAGE + " " + TransitPercentage() + "%";
        }
        if (ship.GetState() == ShipState.UNUSABLE)
        {
            return CANT_LAUNCH;
        }
        return "";
    }

    private string TransitPercentage()
    {
        float c = GlobalGameState.GetInstance().GetUniverse().GetClock() - ship.GetWarpHandler().jumpStart;
        float proportion = c / ship.GetWarpHandler().duration;
        return (proportion * 100).ToString("0");
    }

    public bool IsCapacityExceeded()
    {
        return capExceeded;
    }

    public int GetPassengers()
    {
        return passengers;
    }

    private void ZoneScan(Zone zone)
    {
        int totalDamage = 0;

        int width = zone.GetContents().GetZoneParameters().GetWidth();
        int height = zone.GetContents().GetZoneParameters().GetHeight();
        Tile[][] tiles  = zone.GetContents().getTiles();
        for (int i = 0; i < width; i++)
        {
            for (int j=0;j<height; j++)
            {
                if (tiles[i][j]!=null && tiles[i][j].GetWidget() != null)
                {
                    //widget slot or widget system?

                    //item pile?
                    if (tiles[i][j].GetWidget() is WidgetItemPile)
                    {
                        looseItems = true;
                    }
                    Widget ws = tiles[i][j].GetWidget();
                    if (ws is WidgetShipSystem)
                    {
                        if (!((WidgetShipSystem)ws).IsDamaged()){
                            systems.Add((WidgetShipSystem)ws);
                        }
                    }
                    if (ws is WidgetSlot)
                    {
                        WidgetSlot slot = (WidgetSlot)ws;
                        if (slot.GetContainedWidget() != null)
                        {
                            if (slot.GetContainedWidget() is WidgetShipSystem)
                            {
                                if (!((WidgetShipSystem)slot.GetContainedWidget()).IsDamaged())
                                {
                                    systems.Add((WidgetShipSystem)slot.GetContainedWidget());
                                }
                            }
                            if (slot.GetContainedWidget() is WidgetAccommodation)
                            {
                                capacity += ((WidgetAccommodation)slot.GetContainedWidget()).GetCapacity();
                            }
                        }
                    }
                    if (ws is WidgetDamage)
                    {
                        WidgetDamage dmg = (WidgetDamage)ws;
                        damageWidgets.Add(dmg);
                        totalDamage += dmg.GetDamage();
                    }
                }
            }
        }
        for (int i = 0; i < zone.GetContents().GetActors().Count; i++)
        {
            if (zone.GetContents().GetActors()[i]!=null && 
                zone.GetContents().GetActors()[i].GetAlive() && 
                zone.GetContents().GetActors()[i] is NPC)
            {
                NPC npc = (NPC)zone.GetContents().GetActors()[i];
                if (!npc.IsPeaceful() && npc.GetFaction().getRelationship("player") <= 50)
                {
                    hostiles = true;
                    break;
                } 
                else
                {
                    passengers++;
                }
            }
        }
        if (passengers > capacity)
        {
            capExceeded = true;
        }
        commonstats = new CommonStats(baseStats.GetHP(),baseStats.GetHP()-totalDamage);

    }

    private void CalculateResources()
    {
        for (int i = 0; i < systems.Count; i++)
        {
            for (int j = 0; j < systems[i].GetSystems().Length; j++)
            {
                if (systems[i].GetSystems()[j].GetSystemType() == SHIPSYSTEMTYPE.RESOURCE)
                {
                    //resources
                    ResourceSystem resourceSystem = (ResourceSystem)systems[i].GetSystems()[j];
                    AddResource(resourceSystem);
                }
                if (systems[i].GetSystems()[j].GetSystemType() == SHIPSYSTEMTYPE.CONVERTER)
                {
                    AddConverter((ConversionSystem)systems[i].GetSystems()[j]);
                }
                if (systems[i].GetSystems()[j].GetSystemType() == SHIPSYSTEMTYPE.MAGAZINE)
                {
                    AddMagazine((MagazineSystem)systems[i].GetSystems()[j]);
                }
            }
        }
    }

    private void AddMagazine(MagazineSystem magazineSystem)
    {

        if (magazineSystem.GetAmmoCode() == null)
        {
            return;
        }
        if (magazines.ContainsKey(magazineSystem.GetAmmoCode()))
        {
            magazines[magazineSystem.GetAmmoCode()].amount += magazineSystem.GetAmount();
        }
        else
        {
            ShipMagazine mag = new ShipMagazine();
            mag.containedItem = magazineSystem.GetAmmoCode();
            mag.amount = magazineSystem.GetAmount();
            mag.originalAmount = mag.amount;
            magazines.Add(magazineSystem.GetAmmoCode(), mag);
        }
        if (magazineSystems.ContainsKey(magazineSystem.GetAmmoCode()))
        {
            magazineSystems[magazineSystem.GetAmmoCode()].Add(magazineSystem);
        }
        else
        {
            magazineSystems.Add(magazineSystem.GetAmmoCode(), new List<MagazineSystem>());
            magazineSystems[magazineSystem.GetAmmoCode()].Add(magazineSystem);
        }
    }

    private void AddConverter(ConversionSystem conversionSystem)
    {
        ShipConverter shipConverter = new ShipConverter(conversionSystem);
        converters.Add(shipConverter);
    }

    private void AddResource(ResourceSystem resourceSystem)
    {
        if (resources.ContainsKey(resourceSystem.GetContentId()))
        {
            ShipResource shipResource = resources[resourceSystem.GetContentId()];
            shipResource.amount += resourceSystem.GetAmount();
            shipResource.originalAmount = shipResource.amount;
            shipResource.capacity += resourceSystem.GetCapacity();
            resourceSystems[resourceSystem.GetContentId()].Add(resourceSystem);
        }
        else
        {
            List<ResourceSystem> resourceSystemList = new List<ResourceSystem>();
            resourceSystemList.Add(resourceSystem);
            resourceSystems.Add(resourceSystem.GetContentId(), resourceSystemList);
            ShipResource shipResource = new ShipResource();
            shipResource.displayName = resourceSystem.GetDisplay();
            shipResource.resource = resourceSystem.GetContentId();
            shipResource.capacity = resourceSystem.GetCapacity();
            shipResource.amount = resourceSystem.GetAmount();
            shipResource.originalAmount = shipResource.amount;
            resources.Add(resourceSystem.GetContentId(), shipResource);
        }
    }

    internal void SuppressReformers()
    {
        Zone zone = this.ship.GetZone(0);
        int width = zone.GetContents().GetZoneParameters().GetWidth();
        int height = zone.GetContents().GetZoneParameters().GetHeight();
        Tile[][] tiles = zone.GetContents().getTiles();
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (tiles[i][j] != null && tiles[i][j].GetWidget() != null)
                {
                    //widget slot or widget system?

                    Widget ws = tiles[i][j].GetWidget();
                    if (ws is WidgetSlot)
                    {
                        WidgetSlot slot = (WidgetSlot)ws;
                        if (slot.GetContainedWidget() != null)
                        {
                            if (slot.GetContainedWidget() is WidgetReformer)
                            {
                                ((WidgetReformer)slot.GetContainedWidget()).SetSuppression(true);
                            }
                        }
                    }
                }
            }
        }
    }

    internal void ActivateConverters()
    {
        for (int i = 0; i < converters.Count; i++)
        {
            converters[i].SetActive(true);
        }
    }

    internal bool IsTransitEnd()
    {
        if (ship.GetWarpHandler() != null && ship.GetWarpHandler().state == WarpState.TRANSIT)
        {
            long clock = GlobalGameState.GetInstance().GetUniverse().GetClock();
            if (clock >= ship.GetWarpHandler().jumpStart + ship.GetWarpHandler().duration)
            {
                return true;
            }
        }
        return false;
    }
}
