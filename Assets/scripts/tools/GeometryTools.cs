﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryTools 
{
 
    public static Vector2 getCornerLos(int x, int y, int index)
    {
        switch (index)
        {
            case 0:
                return new Vector2(x + 0.5F, y + 0.5F);
            case 1:
                return new Vector2(x+0.2F, y+0.2F);
            case 2:
                return new Vector2(x + 0.8F, y + 0.2F);
            case 3:
                return new Vector2(x + 0.8F, y + 0.8F);
            case 4:
                return new Vector2(x + 0.2F, y + 0.8F);

        }
        return new Vector2(x, y + 1);
    }

    private static float GetIntercept(float x0, float y0, float m)
    {
        return (x0 * -m)+y0;
    }

    public static SlopeIntercept GetSlopeIntercept(float x0, float y0, float x1, float y1)
    {
        float slope = (y1 - y0) / Math.Abs(x1 - x0);
        float yIntersect = GetIntercept(x0,y0,slope);
        return new SlopeIntercept(slope, yIntersect);
    }

    public static int GetDirection(int x0, int y0, int x1, int y1)
    {
        if (x0 == x1)
        {
            return y1 > y0 ? 0 : 4;
        }
        if (y0 == y1)
        {

            return x1 > x0 ? 2 : 6;
        }
        if (x1 > x0)
        {
            return y1 > y0 ? 1 : 3;
        }
        return y1 > y0 ? 7 : 5;
    }

    public static Vector2Int GetPosition(int x, int y, int index)
    {
        switch (index)
        {
            case 0:
                return new Vector2Int(x, y + 1);
            case 1:
                return new Vector2Int(x + 1, y + 1);
            case 2:
                return new Vector2Int(x + 1, y);
            case 3:
                return new Vector2Int(x + 1, y - 1);
            case 4:
                return new Vector2Int(x, y - 1);
            case 5:
                return new Vector2Int(x - 1, y - 1);
            case 6:
                return new Vector2Int(x - 1, y);
            case 7:
                return new Vector2Int(x - 1, y + 1);
        }
        return new Vector2Int(x, y);
    }

    internal static bool GetSpawnPosition(Vector2 center, int angle, int min, int max, Zone zone, bool hiddenOnly, out Vector2 position)
    {
        position = center;

        Vector2 offset = GeometryTools.GetPosition((int)center.x, (int)center.y, angle * 2);
        int steps = (max - min) + 1;
        Vector2 start = new Vector2(offset.x * min, offset.y * min);
        for (int i = 0; i < steps; i++)
        {
            Vector2 p = new Vector2(start.x + (offset.x * i), start.y+(offset.y * i));
            if (CheckSpawnPosition(p, zone, hiddenOnly))
            {
                position = p;
                return true;
            }
        }

        return false;
    }

    private static bool CheckSpawnPosition(Vector2 p, Zone zone, bool hiddenOnly)
    {
        Tile t = zone.GetContents().GetTile((int)p.x, (int)p.y);
        if (t!=null && t.canWalk() && t.GetActorInTile() == null)
        {
            if (hiddenOnly)
            {
                if (!t.isVisible())
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
            
        }
        return false;
    }

    public static int GetDirection(float x, float y)
    {
        Vector2 p = new Vector2(x, y);
        for (int i= 0;i< 8; i++)
        {
            Vector2Int ci = GeometryTools.GetPosition(0, 0, i);
            Vector2 c = new Vector2(ci.x, ci.y);
            float angle = Vector2.Angle(p.normalized, c.normalized);
            if (angle < 22)
            {
                return i;
            }
        }
        return -1;
    }

    internal static bool isSpawnableAngle(int angle, int directions)
    {
        switch (angle)
        {
            case 0:
                return (directions & 1) >0 ;

            case 1:
                return (directions & 2) > 0;

            case 2:
                return (directions & 4) > 0;

            case 3:
                return (directions & 8) > 0;

        }
        throw new NotImplementedException();
    }
}
