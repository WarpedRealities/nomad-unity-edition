﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml;
using System.Collections.Generic;

public class PlanetContentTool
{
    public string generateSolar(string filename, out float size)
    {
        XmlDocument document = FileTools.GetXmlDocument("worlds/" + filename);
        
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        XmlElement xmlElement = (XmlElement)rootXML.GetElementsByTagName("entity").Item(0);


        size = float.Parse(xmlElement.GetAttribute("size"));
        String sprite = xmlElement.GetAttribute("sprite");
        return sprite;
    }

    public PlanetContents generateContents(string filename, Entity entity)
    {

        XmlDocument document = FileTools.GetXmlDocument("worlds/" + filename);
        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        List<PlanetZone> zones = new List<PlanetZone>();
        List<LandingSite> landingSites = new List<LandingSite>();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("zone".Equals(element.Name))
                {
                    Vector2S p = new Vector2S(Int32.Parse(element.GetAttribute("x")), Int32.Parse(element.GetAttribute("y")));
                    ZoneType zoneType = EnumTools.strToZoneType(element.GetAttribute("type"));
                    String name = element.InnerText;
                    zones.Add(new PlanetZone(p, zoneType,new Zone("worlds/"+filename+"/",name,entity)));
                }
                if ("landedShip".Equals(element.Name))
                {
                    
                    Spaceship ship = new SpaceshipGenerator().GenerateShip(element.GetAttribute("file"),
                        GlobalGameState.GetInstance().GetUniverse().GetUIDGen());
                    ship.SetState("true".Equals(element.GetAttribute("unusable")) ? 
                        ShipState.UNUSABLE : 
                        ShipState.LANDED);
                    landingSites.Add(new LandingSite(element.GetAttribute("zone"),ship,true));
                }

            }
        }
        PlanetContents planetContents = new PlanetContents(zones);
        if (landingSites.Count > 0)
        {
            planetContents.SetLandingSites(landingSites);
        }

        return planetContents;
    }
}
