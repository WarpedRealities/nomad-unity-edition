﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Xml;

public class DescriptionMacroTool
{
    static Dictionary<string, DescriptionMacro> macros;

    internal static string GetMacro(string macroID, Appearance appearance)
    {
        Debug.Log("get macro macro id " + macroID);
        if (macros == null)
        {
            macros = new Dictionary<string, DescriptionMacro>();
        }

        if (!macros.ContainsKey(macroID))
        {
            //load a macro
            macros.Add(macroID, MacroLoader(macroID));
        }

        if (macros.ContainsKey(macroID))
        {

            return macros[macroID].GetMacroValue(appearance);
        }
        return "MACRO NOT LOADED";
    }

    private static DescriptionMacro MacroLoader(string macroID)
    {

        XmlDocument xmlDocument = FileTools.GetXmlDocument("description/macros/" + macroID);
        if (xmlDocument == null)
        {
            Debug.Log("failed to load macro with macro id " + macroID);
        }
        XmlElement root = (XmlElement)xmlDocument.FirstChild.NextSibling;
        if ("macroValue".Equals(root.Name))
        {
            Debug.Log("macro value " + macroID);
            return LoadMacroValue(macroID, root);
        }
        if ("macroRange".Equals(root.Name))
        {
            Debug.Log("macro Range " + macroID);
            return LoadMacroRange(macroID, root);
        }
        Debug.Log("fail?" + macroID);
        return null;
    }

    private static DescriptionMacro LoadMacroRange(string macroID, XmlElement root)
    {
        RangeMacro rangeMacro = new RangeMacro(macroID, root.GetAttribute("part"), root.GetAttribute("value"));

        for (int i = 0; i < root.ChildNodes.Count; i++) {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)root.ChildNodes[i];

                if ("range".Equals(xmlElement.Name))
                {
                    rangeMacro.AddRange(new MacroRange(int.Parse(xmlElement.GetAttribute("lowerBound")),
                        int.Parse(xmlElement.GetAttribute("upperBound")),
                        xmlElement.GetAttribute("string")));
                }

            }
        }
        return rangeMacro;
    }

    private static DescriptionMacro LoadMacroValue(string macroID, XmlElement root)
    {
        ValueMacro valueMacro = new ValueMacro(macroID,root.GetAttribute("part"),root.GetAttribute("value"));

        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement xmlElement = (XmlElement)root.ChildNodes[i];
                if ("translate".Equals(xmlElement.Name))
                {
                    valueMacro.AddValue(int.Parse(xmlElement.GetAttribute("value")), xmlElement.GetAttribute("string"));

                }
            }
        }

        return valueMacro;
    }
}
