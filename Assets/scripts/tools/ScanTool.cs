using System;
using UnityEngine;



public class ScanTool
{

    public VisitDelegate VisitDelegate;
    public BlockRay blockDelegate;
    ViewInterface view;
    LineOfSight2 sight2;
    Player player;
    ZoneContents contents;
    bool ignoreWalls;
    int bonus;
    bool[][] grid; 
    public ScanTool(ViewInterface view, int bonus)
    {
        this.sight2 = new LineOfSight2();
        this.VisitDelegate = Visit;
        this.blockDelegate = BlockRay;
        this.bonus = bonus;
        contents = GlobalGameState.GetInstance().getCurrentZone().GetContents();
        player = GlobalGameState.GetInstance().GetPlayer();
        grid = BuildGrid(contents.GetZoneParameters().GetWidth(), contents.GetZoneParameters().GetHeight());
        this.view = view;
    }

    private bool[][] BuildGrid(int w, int h)
    {
        bool[][] g = new bool[w][];
        for (int i = 0; i < w; i++) {
            g[i] = new bool[h];
        }
        return g;
    }

    internal static void RunScan(EffectScan effectScan, ActionDoMove action, Actor actor, Vector2Int position, ViewInterface view)
    {
        ScanTool scanTool= new ScanTool(view, effectScan.GetBonus());
        Vector2 p = ((Vector2)(position - actor.GetPosition())).normalized;

        int offset = (int)-effectScan.GetWidth();
        for (int i = offset; i <= effectScan.GetWidth(); i+=5) {
            float o = i;
            Vector2 r = Quaternion.Euler(0, 0, o) * p;
            Vector2 c = r * effectScan.GetRange();
            scanTool.GenerateRay(actor.GetPosition(), c + actor.GetPosition(), effectScan);
        }
    }

    private void GenerateRay(Vector2 source, Vector2 end, EffectScan effectScan)
    {
        sight2.GenerateRay(source, end, VisitDelegate, effectScan.isIgnoringWalls() ? null : blockDelegate);
    }

    public bool BlockRay(Vector2Int position)
    {
        Tile t = contents.GetTile(position.x, position.y);
        if (t != null)
        {
            return t.IsVisionBlocking();
        }
        return true;
    }

    public void Visit(Vector2Int position)
    {
        if (!grid[position.x][position.y])
        {
            grid[position.x][position.y] = true;
            view.DrawImpact(new Vector2Int(position.x, position.y), 3);
            Tile t = contents.GetTile(position.x, position.y);
            if (t != null)
            {
                t.Scan(bonus, view, player);
                if (t.GetActorInTile()!=null && !t.GetActorInTile().IsInvisible() && !t.isVisible())
                {
                    view.DrawImpact(position, 4);
                }
            }
        }
    }
}