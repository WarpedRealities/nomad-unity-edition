﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml;
using UnityEngine;

public class ZoneContentsGenerator
{
    public ZoneContents generateContents(XmlElement root, LandingSite landingSite, PregenData pregenData, Entity entity)
    {
        ZoneContents zoneContents = new ZoneContents();
        zoneContents.setZoneParameters(generateParameters(root));
        string[] transitions = null;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
          
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("tileset".Equals(element.Name))
                {
                    zoneContents.setTileSet(generateTileSet(element));
                }
                if ("description".Equals(element.Name))
                {
                    zoneContents.setDescription(element.InnerText);
                }
                if ("transition".Equals(element.Name))
                {
                    if (transitions == null)
                    {
                        transitions = new string[4];
                    }
                    transitions = AddTransition(transitions,element);
                }
                if ("fixedLandingSite".Equals(element.Name))
                {
                    zoneContents.SetFixedLandingSite(new Vector2S(int.Parse(element.GetAttribute("x")),
                        int.Parse(element.GetAttribute("y"))));
                }
                if ("zoneConditions".Equals(element.Name))
                {
                    zoneContents.SetConditions(GenerateConditions(element));
                }
                if ("mapgen".Equals(element.Name))
                {
                    List<Actor> actors = null;
                    WorldGenTools worldGenTools = new WorldGenTools(zoneContents.getTileSet(), pregenData, entity);
                    zoneContents.setTiles(worldGenTools.Run(element,
                        zoneContents.GetZoneParameters(),
                        out actors,
                        landingSite,
                        zoneContents.GetFixedLandingSite()));
                    zoneContents.SetActors(actors);
                }
            }
        }

        zoneContents.setTransitions(transitions);
        return zoneContents;
    }

    private ZoneConditions GenerateConditions(XmlElement root)
    {
        ZoneCondition[] condition = new ZoneCondition[int.Parse(root.GetAttribute("count"))];
        int index = 0;
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            condition[index] = ZoneCondition.Build((XmlElement)root.ChildNodes[i]);
            index++;
        }
        ZoneConditions zoneConditions = new ZoneConditions(condition);
        return zoneConditions;
    }

    private string[] AddTransition(string [] transitions, XmlElement element)
    {
        switch (element.GetAttribute("direction"))
        {
            case "north":
                transitions[0] = element.InnerText;
                break;
            case "east":
                transitions[1] = element.InnerText;
                break;
            case "south":
                transitions[2] = element.InnerText;
                break;
            case "west":
                transitions[3] = element.InnerText;
                break;
        }
        return transitions;
    }

    private ZoneParameters generateParameters(XmlElement root)
    {
        int width = Int32.Parse(root.GetAttribute("width"));
        int height = Int32.Parse(root.GetAttribute("height"));
        string spriteSet = root.GetAttribute("spriteset");
        return new ZoneParameters(width, height, spriteSet);
    }

    private ZoneTileset generateTileSet(XmlElement root)
    {
        TileDef[] tiledefs = new TileDef[root.ChildNodes.Count];
        int width = 4; 
        int height = 4;
        Int32.TryParse(root.GetAttribute("count-x"), out width);
        Int32.TryParse(root.GetAttribute("count-y"), out height);
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                int smartTile = 0;
                Int32.TryParse(element.GetAttribute("smartTile"), out smartTile);
                int tileSprite = Int32.Parse(element.GetAttribute("sprite"));
                int tileOverlay = -1;
                Int32.TryParse(element.GetAttribute("overlay"), out tileOverlay);
                int smartTileGroup = 0;
                Int32.TryParse(element.GetAttribute("smartGroup"), out smartTileGroup);
                int smartTileAlias = 0;
                Int32.TryParse(element.GetAttribute("smartAlias"), out smartTileAlias);
                int tileDecoration = 0;
                Int32.TryParse(element.GetAttribute("decoration"), out tileDecoration);
                string description = element.InnerText;
                TileMovement tileMovement=EnumTools.StrToMovement(element.GetAttribute("movement"));
                TileVision tileVision= EnumTools.StrToVision(element.GetAttribute("vision"));
                tiledefs[i] = new TileDef(smartTile,tileSprite,tileOverlay,smartTileGroup, smartTileAlias, tileDecoration,description,tileMovement,tileVision);
                tiledefs[i].SetIndex(i);
            }
        }

        return new ZoneTileset(tiledefs,width, height);
    }
 
}
