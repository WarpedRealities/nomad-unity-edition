using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
class CargoTool
{

    public static float AddStack(ItemStack item, List<WidgetContainer> containers, DelegateLogText spaceLog)
    {
        float f = 0;
        int count = 0;
        for (int i = 0; i < containers.Count; i++)
        {
            float free = containers[i].GetContainerData().GetCapacity() - containers[i].GetContainerData().GetWeight();

            int m = (int)(free / item.GetDef().GetWeight());

            if (m >= item.GetCount())
            {
                f += item.GetCount() * item.GetDef().GetWeight();
                containers[i].GetContainerData().AddItems(item.TakeItem(), item.GetCount());
                count += item.GetCount();
                item.SetCount(0);
            }
            else
            {
                f += item.GetCount() * item.GetDef().GetWeight();
                containers[i].GetContainerData().AddItems(item.TakeItem(), m);
                item.SetCount(item.GetCount() - m);
                count += m;
            }

        }
        spaceLog("you have recovered " + count + " " + item.GetDef().GetName());
        return f;
    }

    public static float AddItems(Item item, List<WidgetContainer> containers, DelegateLogText spaceLog)
    {
        for (int i = 0; i < containers.Count; i++)
        {
            float free = containers[i].GetContainerData().GetCapacity() - containers[i].GetContainerData().GetWeight();
            if (free >= item.GetWeight())
            {
                containers[i].GetContainerData().AddItem(item);
                spaceLog("recovered item " + item.GetName());
                return item.GetWeight();
            }
        }
        return 0;
    }

    public static float CountSpace(List<WidgetContainer> containers)
    {
        float free = 0;
        for (int i = 0; i < containers.Count; i++)
        {
            free += containers[i].GetContainerData().GetCapacity() - containers[i].GetContainerData().GetWeight();
        }
        return free;
    }

    public static List<WidgetContainer> ContainerScan(Zone zone)
    {
        List<WidgetContainer> containers = new List<WidgetContainer>();
        int width = zone.GetContents().GetZoneParameters().GetWidth();
        int height = zone.GetContents().GetZoneParameters().GetHeight();

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Tile t = zone.GetContents().GetTile(i, j);
                if (t != null && t.GetWidget() != null)
                {
                    if (t.GetWidget() is WidgetSlot)
                    {
                        WidgetSlot ws = (WidgetSlot)t.GetWidget();
                        if (ws.GetContainedWidget() != null && ws.GetContainedWidget() is WidgetContainer)
                        {
                            containers.Add((WidgetContainer)ws.GetContainedWidget());
                        }
                    }
                    if (t.GetWidget() is WidgetContainer)
                    {
                        containers.Add((WidgetContainer)t.GetWidget());
                    }
                }
            }
        }

        return containers;
    }

}