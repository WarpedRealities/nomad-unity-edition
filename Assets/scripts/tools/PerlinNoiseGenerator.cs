﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;


public class PerlinNoiseGenerator 
{
    float rX, rY;
    System.Random random;
    public PerlinNoiseGenerator ()
    {
        random = new System.Random();
        rX = random.Next();
        rY = random.Next();
    }

    public double[][] generatePerlin(int width, int height)
    {
        double[][] values = new double[width][];
        for (int i = 0; i < width; i++)
        {
            values[i] = new double[height];
            for (int j = 0; j < height; j++)
            {
                values[i][j] = Mathf.PerlinNoise(i + rX, j + rY);
            }
        }
        rX = random.Next();
        rY = random.Next();
        return values;   
    }
}
