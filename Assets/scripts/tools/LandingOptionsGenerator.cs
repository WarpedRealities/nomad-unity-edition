﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class LandingOptionsGenerator 
{
    Entity entity;

    public LandingOptionsGenerator(Entity entity)
    {
        this.entity = entity;
    }

    public LandingOption [] GenerateOptions()
    {
        switch (entity.GetEntityType())
        {
            case ENTITYTYPE.PLANET:
                return GeneratePlanetOptions();

            case ENTITYTYPE.STATION:

                break;
        }


        return null;
    }

    private LandingOption[] GeneratePlanetOptions()
    {
        List<LandingOption> landingOptions = new List<LandingOption>();
        Planet planet = (Planet)entity;
        for (int i = 0; i < planet.GetContents().GetZones().Count; i++)
        {
            PlanetZone zone = planet.GetContents().GetZones()[i];
            LandingSite landingSite = GetLandingSite(planet.GetContents().GetZones()[i], planet);
            if (zone.GetZoneType() == ZoneType.SURFACE && zone.GetZone().IsGenerated())
            {
                landingOptions.Add(new LandingOption(landingSite, zone.GetZone(), GetState(zone, landingSite)));
            }
        }


        LandingOption[] options = new LandingOption[landingOptions.Count];
        for (int i = 0; i < landingOptions.Count; i++)
        {
            options[i] = landingOptions[i];
        }
        return options;
    }

    private LandingSite GetLandingSite(PlanetZone planetZone, Planet planet)
    {
        return planet.GetLandingSite(planetZone.GetZone());
    }

    private LANDINGOPTIONSTATE GetState(PlanetZone zone, LandingSite landingSite)
    {
        if (!zone.GetZone().IsGenerated())
        {
            return LANDINGOPTIONSTATE.UNEXPLORED;
        }
        if (landingSite != null)
        {
            return LANDINGOPTIONSTATE.TAKEN;
        }
        return LANDINGOPTIONSTATE.ACCESSIBLE;
    }
}
