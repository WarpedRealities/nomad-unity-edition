﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpriteLibrary
{

    private static SpriteLibrary instance;

    public static SpriteLibrary getInstance()
    {
        if (instance == null)
        {
            instance = new SpriteLibrary();
        }
        return instance;
    }

    Dictionary<string, Material> library;
    Material spriteMaterial;
    private SpriteLibrary()
    {
        library = new Dictionary<string, Material>();
        spriteMaterial = (Material)Resources.Load("materials/spritematerial");

    }

    public Material GetMaterial(string filename, string prefix = "sprites/")
    {
        Material mat;
        library.TryGetValue(filename, out mat);
        if (mat == null)
        {
            mat = new Material(spriteMaterial);
            mat.mainTexture = TextureManagementTools.loadTexture("gameData/art/" + prefix + filename + ".png");
            
            library.Add(filename, mat);
        }
        return mat;
    }

}
