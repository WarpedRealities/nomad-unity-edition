﻿using UnityEngine;
using System.Collections;
using System;

public class GridTools
{

  

    public static bool [][] BuildGrid(int width, int height)
    {
        bool[][] grid = new bool[width][];
        for (int i = 0; i < width; i++)
        {
            grid[i] = new bool[height];
            for (int j = 0; j < height; j++)
            {
                grid[i][j] = false;
            }
        }
        return grid;
    }

    public static bool [][] Circle(bool[][]grid, int radius, bool remove)
    {
        int mid = grid.Length/2;

        for (int i = mid-radius; i < mid+radius; i++)
        {
            int v = Mathf.Abs(i - mid);
            int w = Pythagoras(radius, v);

            int start = (grid[0].Length / 2) - w;
            for (int j = start; j < start+(w*2); j++)
            {
                grid[i][j] = remove ? false : true;
            }
        }
        return grid;
    }

    private static int Pythagoras(int side0, int side1)
    {

        int s0 = side0 * side0;
        int s1 = side1 * side1;
        int s01 = s0>s1 ? s0 - s1: s1 -s0;
        float s = Mathf.Sqrt(s01);
        return (int)s;
    }
}
