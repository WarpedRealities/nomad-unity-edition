﻿using UnityEngine;
using System.Collections;
using System;

public class PreSaveTool
{
    internal static void PreSaveSystem(StarSystem starSystem)
    {
        for (int i = 0; i < starSystem.GetSystemContents().GetEntities().Count; i++)
        {
            Entity entity = starSystem.GetSystemContents().GetEntities()[i];
            switch (entity.GetEntityType())
            {
                case ENTITYTYPE.PLANET:
                    PreSavePlanet((Planet)entity);
                    break;
            }
        }
    }

    private static void PreSavePlanet(Planet entity)
    {
        PlanetContents contents = entity.GetContents();
        if (contents==null || contents.GetZones() == null)
        {
            return;
        }
        for (int i = 0; i < contents.GetZones().Count; i++)
        {
            if (contents.GetZones()[i].GetZone().GetContents() != null)
            {
                contents.GetZones()[i].GetZone().CleanActorInTile();
            }
        }
    }
}
