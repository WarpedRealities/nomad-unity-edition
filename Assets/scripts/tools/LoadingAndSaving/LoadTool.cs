﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEditor;


public class LoadTool
{

    private static string SAVES = "saves/";

    internal static bool Load(string filename, bool forceReset=false)
    {
        string savefile = SAVES + filename;
        SaveState state = null;
        //load the main.sav file
        using (Stream stream = File.Open(savefile + "/main.sav", FileMode.Open))
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            state = (SaveState)binaryFormatter.Deserialize(stream);
        }
        //check for a version shift
        if (state.GetVersionNumber()!= Config.GetInstance().GetVersion() || forceReset)
        {

            return VersionShiftProcess(state, savefile);
        }

        HandleSaveState(state);

        //load all the systems!
        LoadSystems(savefile,GlobalGameState.GetInstance().GetUniverse());

        //apply this to the universe and global game state

        
        return FinalizeLoading(state, GlobalGameState.GetInstance());
    }

    private static bool VersionShiftProcess(SaveState state, String saveFile)
    {
        UniverseGeneratorTools generatorTools = new UniverseGeneratorTools();
        GlobalGameState globalGameState = GlobalGameState.GetInstance();
        globalGameState.SetPlayer(state.GetPlayer());
        globalGameState.GetUniverse().SetUID(state.GetUIDGenerator());
        globalGameState.GetUniverse().GetCraftingLibrary().ProcessUnlocks(state.GetUnlockedRecipes());
        state.GetPlayer().GetFlagField().Clear();
        globalGameState.GetUniverse().getFactionLibrary().Clear();
        globalGameState.GetUniverse().GetShopLibrary().Clear();
        File.SetAttributes(saveFile, FileAttributes.Normal);
        FileTools.DeleteContents(saveFile);
        //Directory.Delete(saveFile);
        globalGameState.GetUniverse().setStarSystems(generatorTools.buildSystems());
        NewGameTool newGameTool = new NewGameTool();
        if (state.GetShipEntity() != null)
        {
            //need to initialize in space
            newGameTool.SpaceStart(state);
        }
        else
        {
            //need to initialize planet side
            newGameTool.ShiftStart(state);
        }
        globalGameState.SetCurrentSystem(newGameTool.system);
        globalGameState.SetCurrentEntity(newGameTool.entity);
        globalGameState.setCurrentZone(newGameTool.zone);

        return true;
    }

    private static void HandleSaveState(SaveState state)
    {
        GlobalGameState globalGameState = GlobalGameState.GetInstance();
        globalGameState.SetPlayer(state.GetPlayer());
        globalGameState.GetUniverse().SetFactionLibrary(state.GetFactions());
        globalGameState.GetUniverse().SetUID(state.GetUIDGenerator());
        globalGameState.GetUniverse().SetTimeKeeper(state.GetTimeKeeper());
        globalGameState.GetUniverse().SetClock(state.GetClock());
        globalGameState.GetUniverse().SetRuleLibrary(state.GetRuleLibrary());
        globalGameState.GetUniverse().SetShopLibrary(state.GetShopLibrary());
        globalGameState.GetUniverse().GetCraftingLibrary().ProcessUnlocks(state.GetUnlockedRecipes());
    }

    private static void LoadSystems(string savefile, Universe universe)
    {
        for (int i = 0; i < universe.GetSystems().Count; i++)
        {
            if (File.Exists(savefile + "/" + universe.GetSystems()[i].GetName() + ".sav"))
            {
                using (Stream stream = File.Open(savefile + "/" + universe.GetSystems()[i].GetName() + ".sav", FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    StarSystemContents contents= (StarSystemContents)binaryFormatter.Deserialize(stream);
                    contents.PostLoad();
                    universe.GetSystems()[i].setContents(contents);
                    
                }
            }
        }
      //  throw new NotImplementedException();
    }

    private static bool FinalizeLoading(SaveState state, GlobalGameState globalGameState)
    {

        //set universe
        globalGameState.SetCurrentSystem(state.GetSystem());
        globalGameState.SetCurrentEntity(state.GetEntity());
        globalGameState.setCurrentZone(state.GetZone());
        return true;
    }

}
