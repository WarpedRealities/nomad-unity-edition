﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class SaveTool
{
    private static string SAVES = "saves/";
    private static string DEFAULTSAVENAME = "default_save";
    internal static bool Save(string filename)
    {
        if (filename.Length < 1)
        {
            filename = DEFAULTSAVENAME;
        }
        string savefile = SAVES + filename;
        //clear folder
        if (Directory.Exists(savefile))
        {
            Directory.Delete(savefile, true);
        }

        Directory.CreateDirectory(savefile);
        Spaceship ship = GetShipForPorting();
        //build savestate
        SaveState saveState = new SaveState(GlobalGameState.GetInstance().GetPlayer(),
            GlobalGameState.GetInstance().GetUniverse().getFactionLibrary(),
            GlobalGameState.GetInstance().GetUniverse().GetFactionRuleLibrary(),
            GlobalGameState.GetInstance().GetUniverse().GetShopLibrary(),
            GlobalGameState.GetInstance().GetUniverse().GetCraftingLibrary().GetUnlocks(),
            GlobalGameState.GetInstance().GetUniverse().GetClock(),
            GlobalGameState.GetInstance().GetUniverse().GetUIDGen(),
            GlobalGameState.GetInstance().GetUniverse().GetTimeKeeper(),
            GlobalGameState.GetInstance().getCurrentSystem().GetName(),
            GlobalGameState.GetInstance().getCurrentEntity().getName(),
            GlobalGameState.GetInstance().getCurrentZone().getName(),
            GlobalGameState.GetInstance().GetCurrentShip(),
            ship);
        using (Stream stream = File.Open(savefile+"/main.sav", FileMode.Create))
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            binaryFormatter.Serialize(stream, saveState);
        }

        VerifyCurrentZone();

        SaveSystems(savefile);

        return true;

    }

    private static Spaceship GetShipForPorting()
    {
        if (GlobalGameState.GetInstance().getCurrentEntity() is Spaceship)
        {
            Spaceship ship = (Spaceship)GlobalGameState.GetInstance().getCurrentEntity();
            if (ship.getName().Equals(GlobalGameState.GetInstance().GetCurrentShip()))
            {
                //gonna assume the ship is safe, this might be exploitable
                return ship;
            }
        }
        return null;
    }

    private static void VerifyCurrentZone()
    {
        ZoneContents contents = GlobalGameState.GetInstance().getCurrentZone().GetContents();
        for (int i = 0; i < contents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < contents.GetZoneParameters().GetHeight(); j++)
            {
                Tile t = contents.getTiles()[i][j];
                
                if (t!=null && t.getDefinition() == null)
                {
                    Debug.Log("holy shit why" + t.GetDefinitionIndex());
                }
            }
        }
    }

    private static void SaveSystems(string savefile)
    {
        for (int i = 0; i < GlobalGameState.GetInstance().GetUniverse().GetSystems().Count; i++)
        {
            if (GlobalGameState.GetInstance().GetUniverse().GetSystems()[i].GetSystemContents() != null)
            {
                //save this system
                SaveSystem(savefile, GlobalGameState.GetInstance().GetUniverse().GetSystems()[i]);
            }
        }
       // throw new NotImplementedException();
    }

    private static void SaveSystem(string savefile, StarSystem starSystem)
    {
        //clear zoneactors from all zones
        PreSaveTool.PreSaveSystem(starSystem);
        using (Stream stream = File.Open(savefile + "/"+starSystem.GetName()+".sav", FileMode.Create))
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            binaryFormatter.Serialize(stream, starSystem.GetSystemContents());
         //   binaryFormatter.Serialize(stream, saveState);
        }
       // throw new NotImplementedException();
    }
}
