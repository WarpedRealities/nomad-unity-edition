﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class SaveState 
{

    Player player;
    FactionLibrary factionLibrary;
    FactionRuleLibrary ruleLibrary;
    List<string> recipes;
    ShopLibrary shopLibrary;
    long clock;
    UIDGenerator uIDGenerator;
    TimeKeeper timeKeeper;
    string currentSystem, currentEntity, currentZone, currentShip;
    Spaceship shipEntity;
    int versionNumber;

    public SaveState(Player player, FactionLibrary factionLibrary, 
        FactionRuleLibrary ruleLibrary, ShopLibrary shopLibrary,
        List<string> recipes,
        long clock, UIDGenerator uIDGenerator, TimeKeeper timekeeper, 
        string system, string entity, string zone, string ship, Spaceship shipEntity)
    {
        this.player = player;
        this.factionLibrary = factionLibrary;
        this.ruleLibrary = ruleLibrary;
        this.shopLibrary = shopLibrary;
        this.recipes = recipes;
        this.clock = clock;
        this.uIDGenerator = uIDGenerator;
        this.timeKeeper = timekeeper;
        this.currentSystem = system;
        this.currentEntity = entity;
        this.currentZone = zone;
        this.currentShip = ship;
        this.shipEntity = shipEntity;
        this.versionNumber = Config.GetInstance().GetVersion();
    }

    public UIDGenerator GetUIDGenerator()
    {
        return uIDGenerator;
    }

    public TimeKeeper GetTimeKeeper()
    {
        return timeKeeper;
    }

    public Spaceship GetShipEntity()
    {
        return shipEntity;
    }

    public long GetClock()
    {
        return clock;
    }

    public Player GetPlayer()
    {
        return player;
    }

    public string GetSystem()
    {
        return currentSystem;
    }

    public string GetEntity()
    {
        return currentEntity;
    }

    public string GetCurrentShip()
    {
        return currentShip;
    }

    public string GetZone()
    {
        return currentZone;
    }

    public int GetVersionNumber()
    {
        return versionNumber;
    }

    public FactionLibrary GetFactions()
    {
        return factionLibrary;
    }

    public FactionRuleLibrary GetRuleLibrary()
    {
        return ruleLibrary;
    }

    public ShopLibrary GetShopLibrary()
    {
        return shopLibrary;
    }

    internal List<string> GetUnlockedRecipes()
    {
        return recipes;
    }
}
