﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class ShipSystemFastForwardTool
{

    private List<WidgetShipSystem> systems;
    private Dictionary<string, ShipResource> resources;
    private Dictionary<string, List<ResourceSystem>> resourceSystems;
    private WidgetShipSystem widgetShipSystem;
    private Zone zone;

    public ShipSystemFastForwardTool(WidgetShipSystem widgetShipSystem, Zone zone)
    {
        resources = new Dictionary<string, ShipResource>();
        resourceSystems = new Dictionary<string, List<ResourceSystem>>();
        systems = new List<WidgetShipSystem>();
        if (widgetShipSystem != null)
            systems.Add(widgetShipSystem);
        this.zone = zone;
        PopulateSystems();
        CalculateResources();
    }

    private void CalculateResources()
    {
        for (int i = 0; i < systems.Count; i++)
        {
            for (int j = 0; j < systems[i].GetSystems().Length; j++)
            {
                if (systems[i].GetSystems()[j].GetSystemType() == SHIPSYSTEMTYPE.RESOURCE)
                {
                    //resources
                    ResourceSystem resourceSystem = (ResourceSystem)systems[i].GetSystems()[j];
                    AddResource(resourceSystem);
                }
            }
        }
    }

    private void AddResource(ResourceSystem resourceSystem)
    {
        if (resources.ContainsKey(resourceSystem.GetContentId()))
        {
            ShipResource shipResource = resources[resourceSystem.GetContentId()];
            shipResource.amount += resourceSystem.GetAmount();
            shipResource.originalAmount = shipResource.amount;
            shipResource.capacity += resourceSystem.GetCapacity();
            resourceSystems[resourceSystem.GetContentId()].Add(resourceSystem);
        }
        else
        {
            List<ResourceSystem> resourceSystemList = new List<ResourceSystem>();
            resourceSystemList.Add(resourceSystem);
            resourceSystems.Add(resourceSystem.GetContentId(), resourceSystemList);
            ShipResource shipResource = new ShipResource();
            shipResource.resource = resourceSystem.GetContentId();
            shipResource.capacity = resourceSystem.GetCapacity();
            shipResource.amount = resourceSystem.GetAmount();
            shipResource.originalAmount = shipResource.amount;
            resources.Add(resourceSystem.GetContentId(), shipResource);
        }
    }

    private void ApplyResources()
    {
        //for each resource, see if its larger or smaller
        foreach ( var key in resources.Keys)
        {        
            ShipResource res = resources[key];

            if (res.amount > res.originalAmount)
            {
                IncreaseResources(res.amount - res.originalAmount, resourceSystems[key]);
            }
            if (res.amount < res.originalAmount)
            {
                DecreaseResources(res.originalAmount-res.amount, resourceSystems[key]);
            }
        }

    }

    private void IncreaseResources(float amount, List<ResourceSystem> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            float freecap = list[i].GetCapacity() - list[i].GetAmount();
            if (freecap >= amount)
            {
                list[i].SetAmount(list[i].GetAmount() + amount);
                return;
            }
            else
            {
                amount -= freecap;
                list[i].SetAmount(list[i].GetCapacity());
            }
        }
        if (amount > 0)
        {
            Debug.Log("amount left is above zero it is in fact " + amount);
        }
    }

    private void DecreaseResources(float amount, List<ResourceSystem> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            float am = list[i].GetAmount();
            if (am >= amount)
            {
                list[i].SetAmount(am - amount);
                return;
            }
            else
            {
                list[i].SetAmount(0);
                amount -= am;
            }
        }
        if (amount > 0)
        {
            Debug.Log("amount left is above zero it is in fact " + amount);
        }
    }

    private void PopulateSystems()
    {
        //find all widgets and add them to this list
        ZoneContents zoneContents = this.zone.GetContents();
        for (int i = 0; i < zoneContents.GetZoneParameters().GetWidth(); i++)
        {
            for (int j = 0; j < zoneContents.GetZoneParameters().GetHeight(); j++)
            {
                if (zoneContents.getTiles()[i][j] != null && zoneContents.getTiles()[i][j].GetWidget() != null)
                {
                    Widget ws = zoneContents.getTiles()[i][j].GetWidget();
                    if (ws is WidgetShipSystem && !systems.Contains(ws))
                    {
                        systems.Add((WidgetShipSystem)ws);
                    }
                    if (ws is WidgetSlot)
                    {
                        WidgetSlot slot = (WidgetSlot)ws;
                        if (slot.GetContainedWidget()!=null && 
                            slot.GetContainedWidget() is WidgetShipSystem &&
                            !systems.Contains(slot.GetContainedWidget()))
                        {
                            systems.Add((WidgetShipSystem)slot.GetContainedWidget());
                        }
                    }
                }
            }
        }
    }

    internal void RunFastForward(long clock)
    {
        long duration = CalcDuration(clock);
        long halfDuration = duration / 2;

        for (int j = 0; j < 2; j++)
        {
            for (int i = 0; i < systems.Count; i++)
            {
                systems[i].RunSystems(halfDuration, resources);
            }
        }
        for (int i = 0; i < systems.Count; i++)
        {
            systems[i].SetLastInteraction(clock);
        }
        ApplyResources();
    }


    private long CalcDuration(long clock)
    {
        long amount = 0;
        for (int i = 0; i < systems.Count; i++)
        {
            long gap = clock - systems[i].GetLastInteraction();
            if (amount < gap)
            {
                amount = gap;
            }
        }
        return amount;
    }
}
