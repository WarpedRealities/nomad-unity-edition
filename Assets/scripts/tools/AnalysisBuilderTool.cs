using System;
using System.Collections;
using System.Text;
using System.Xml;
using UnityEngine;





public class AnalysisBuilderTool
{
    internal static int BuildAnalysis(EffectAnalyse effect, ZoneActor actor, StringBuilder stringBuilder, int strength)
    {
        int data = 0;
        if (effect.GetHealthThreshold(0) != -1)
        {
            data += BuildHealthAnalysis(effect.GetHealthThreshold(), actor, stringBuilder, strength);
        }
        if (effect.GetResolveThreshold(0) != -1)
        {
            data += BuildResolveAnalysis(effect.GetResolveThreshold(), actor, stringBuilder, strength);
        }
        if (effect.GetDefenceThreshold(0) != -1)
        {
            data += BuildDefenceAnalysis(effect.GetDefenceThreshold(), actor, stringBuilder, strength);
        }
        if (effect.GetResistThreshold(0) != -1)
        {
            data += BuildResistAnalysis(effect.GetResistThreshold(), actor, stringBuilder, strength); 
        }
        return data;
    }

    private static int BuildResistAnalysis(int[] ints, ZoneActor actor, StringBuilder stringBuilder, int strength)
    {
        int level = GetLevel(ints, strength);
        switch (level)
        {
            case 0:
                stringBuilder.Append(":" + GetHighestRes(actor));
                break;
            case 1:
                stringBuilder.Append(":" + GetHighestRes(actor)+ ":" + GetLowestRes(actor));
                break;
            case 2:
                stringBuilder.Append(":" + GetHighestResvalue(actor) + ":" + GetLowestResValue(actor));
                break;
        }
        return level > -1 ? 1 : 0;
    }

    private static string GetLowestResValue(ZoneActor actor)
    {
        int index = -1;
        int value = 999;
        for (int i = 6; i < 9; i++)
        {
            if (actor.GetActor().GetRPG().getDefenceResist(i) < value)
            {
                index = i;
                value = actor.GetActor().GetRPG().getDefenceResist(i);
            }
        }
        switch (index)
        {
            case 6:
                return actor.GetActor().GetRPG().getDefenceResist(DR.POISE) + " poise";
            case 7:
                return actor.GetActor().GetRPG().getDefenceResist(DR.EGO) + " ego";
            case 8:
                return actor.GetActor().GetRPG().getDefenceResist(DR.METABOLISM) + " metabolism";
        }
        return "no weak resistance";
    }

    private static string GetHighestResvalue(ZoneActor actor)
    {
        int index = -1;
        int value = 0;
        for (int i = 6; i < 9; i++)
        {
            if (actor.GetActor().GetRPG().getDefenceResist(i) > value)
            {
                index = i;
                value = actor.GetActor().GetRPG().getDefenceResist(i);
            }
        }
        switch (index)
        {
            case 6:
                return actor.GetActor().GetRPG().getDefenceResist(DR.POISE) + " poise";
            case 7:
                return actor.GetActor().GetRPG().getDefenceResist(DR.EGO) + " ego";
            case 8:
                return actor.GetActor().GetRPG().getDefenceResist(DR.METABOLISM) + " metabolism";
        }
        return "no strong resistance";;
    }

    private static string GetLowestRes(ZoneActor actor)
    {
        int index = -1;
        int value = 999;
        for (int i = 6; i < 9; i++)
        {
            if (actor.GetActor().GetRPG().getDefenceResist(i) < value)
            {
                index = i;
                value = actor.GetActor().GetRPG().getDefenceResist(i);
            }
        }
        switch (index)
        {
            case 6:
                return "flawed poise";
            case 7:
                return "weak ego";
            case 8:
                return "sickly metabolism";
        }
        return "no weak resistance";
    }

    private static string GetHighestRes(ZoneActor actor)
    {
        int index = -1;
        int value = 0;
        for (int i = 6; i < 9; i++)
        {
            if (actor.GetActor().GetRPG().getDefenceResist(i) > value)
            {
                index = i;
                value = actor.GetActor().GetRPG().getDefenceResist(i);
            }
        }
        switch (index)
        {
            case 6:
                return "good poise";
            case 7:
                return "strong ego";
            case 8:
                return "hardy metabolism";
        }
        return "no strong resistance";
    }

    private static int BuildDefenceAnalysis(int[] ints, ZoneActor actor, StringBuilder stringBuilder, int strength)
    {
        int level = GetLevel(ints, strength);
        switch (level)
        {
            case 0:
                stringBuilder.Append(" :" + GetHighestDef(actor));

                break;
            case 1:
                stringBuilder.Append(" :" + GetHighestDef(actor) + ":" +GetLowestDef(actor));
                break;
            case 2:
                stringBuilder.Append(" :" + GetHighestDefValue(actor) + ":" + GetLowestDefValue(actor));
                break;
        }
        return level > -1 ? 1 : 0;

    }

    private static string GetLowestDefValue(ZoneActor actor)
    {
        int index = -1;
        int value = 0;
        for (int i = 0; i < 6; i++)
        {
            value += actor.GetActor().GetRPG().getDefenceResist(i);
        }
        value = value / 6;
        for (int i = 0; i < 6; i++)
        {
            if (actor.GetActor().GetRPG().getDefenceResist(i) < value)
            {
                index = i;
                value = actor.GetActor().GetRPG().getDefenceResist(i);
            }
        }
        switch (index)
        {
            case 0:
                return actor.GetActor().GetRPG().getDefenceResist(DR.KINETIC) + " kinetic defence";
            case 1:
                return actor.GetActor().GetRPG().getDefenceResist(DR.THERMAL) + " thermal defence";
            case 2:
                return actor.GetActor().GetRPG().getDefenceResist(DR.SHOCK) + " shock defence";
            case 3:
                return actor.GetActor().GetRPG().getDefenceResist(DR.TEASE) + " tease defence";
            case 4:
                return actor.GetActor().GetRPG().getDefenceResist(DR.PHEROMONE) + " pheromone defence";
            case 5:
                return actor.GetActor().GetRPG().getDefenceResist(DR.PSI) + " psi defence";
        }
        return "no weak defence";
    }

    private static string GetHighestDefValue(ZoneActor actor)
    {
        int index = -1;
        int value = 0;
        for (int i = 0; i < 6; i++)
        {
            if (actor.GetActor().GetRPG().getDefenceResist(i) > value)
            {
                index = i;
                value = actor.GetActor().GetRPG().getDefenceResist(i);
            }
        }
        switch (index)
        {
            case 0:
                return actor.GetActor().GetRPG().getDefenceResist(DR.KINETIC)+" kinetic defence";
            case 1:
                return actor.GetActor().GetRPG().getDefenceResist(DR.THERMAL) + " thermal defence";
            case 2:
                return actor.GetActor().GetRPG().getDefenceResist(DR.SHOCK) + " shock defence";
            case 3:
                return actor.GetActor().GetRPG().getDefenceResist(DR.TEASE) + " tease defence";
            case 4:
                return actor.GetActor().GetRPG().getDefenceResist(DR.PHEROMONE) + " pheromone defence";
            case 5:
                return actor.GetActor().GetRPG().getDefenceResist(DR.PSI) + " psi defence";
        }
        return "no strong defence";
    }

    private static string GetLowestDef(ZoneActor actor)
    {
        int index = -1;
        int value = 0;
        for (int i = 0; i < 6; i++)
        {
            value += actor.GetActor().GetRPG().getDefenceResist(i);
        }
        value = value / 6;
        for (int i = 0; i < 6; i++)
        {
            if (actor.GetActor().GetRPG().getDefenceResist(i) < value)
            {
                index = i;
                value = actor.GetActor().GetRPG().getDefenceResist(i);
            }
        }
        switch (index)
        {
            case 0:
                return "weak kinetic defence";
            case 1:
                return "weak thermal defence";
            case 2:
                return "weak shock defence";
            case 3:
                return "weak tease defence";
            case 4:
                return "weak pheromone defence";
            case 5:
                return "weak psi defence";
        }
        return "no weak defence";
    }

    private static string GetHighestDef(ZoneActor actor)
    {
        int index = -1;
        int value = 0;
        for (int i = 0; i < 6; i++)
        {
            if (actor.GetActor().GetRPG().getDefenceResist(i) > value)
            {
                index = i;
                value = actor.GetActor().GetRPG().getDefenceResist(i);
            }
        }
        switch (index)
        {  
            case 0:
                return "strong kinetic defence";
            case 1:
                return "strong thermal defence";
            case 2:
                return "strong shock defence";
            case 3:
                return "strong tease defence";
            case 4:
                return "strong pheromone defence";
            case 5:
                return "strong psi defence";
        }
        return "no strong defence";
    }

    private static int BuildResolveAnalysis(int[] ints, ZoneActor actor, StringBuilder stringBuilder, int strength)
    {
        int level = GetLevel(ints, strength);
        switch (level)
        {
            case 0:
                stringBuilder.Append(" :" + GetString(1,
                    GetVagueValue(actor.GetActor().GetRPG().GetStat(ST.RESOLVE),
                    actor.GetActor().GetRPG().GetStatMax(ST.RESOLVE))));
                break;
            case 1:
                stringBuilder.Append("  :" + GetPercentage(actor.GetActor().GetRPG().GetStat(ST.RESOLVE),
                    actor.GetActor().GetRPG().GetStatMax(ST.RESOLVE)));
                break;

            case 2:
                stringBuilder.Append("  :" + GetExact(actor.GetActor().GetRPG().GetStat(ST.RESOLVE),
                    actor.GetActor().GetRPG().GetStatMax(ST.RESOLVE)));
                break;
        }
        return level > -1 ? 1 : 0;
    }

    private static int BuildHealthAnalysis(int[] ints, ZoneActor actor, StringBuilder stringBuilder, int strength)
    {
        int level = GetLevel(ints, strength);
        switch (level)
        {
            case 0:
                stringBuilder.Append("  :" + GetString(0,
                    GetVagueValue(actor.GetActor().GetRPG().GetStat(ST.HEALTH),
                    actor.GetActor().GetRPG().GetStatMax(ST.HEALTH))));
                break;
            case 1:
                stringBuilder.Append("  :"+GetPercentage(actor.GetActor().GetRPG().GetStat(ST.HEALTH),
                    actor.GetActor().GetRPG().GetStatMax(ST.HEALTH)));
                break;

            case 2:
                stringBuilder.Append(" :" + GetExact(actor.GetActor().GetRPG().GetStat(ST.HEALTH),
                    actor.GetActor().GetRPG().GetStatMax(ST.HEALTH)));
                break;
        }
        return level > -1 ? 1 : 0;
    }

    private static string GetExact(float current, float max)
    {
        return current.ToString() + "/" + max.ToString();
    }

    private static string GetPercentage(float current, float max)
    {
        float p = (current / max) * 100;
        int pi = (int)p;
        return pi.ToString() + "%";
    }

    private static int GetVagueValue(float current, float max)
    {
        if (current >= max)
        {
            return 0;
        }
        if (current > max * 0.75F)
        {
            return 1;
        }
        if (current > max * 0.5F)
        {
            return 2;
        }
        if (current > max * 0.25F)
        {
            return 3;
        }
        return 4;
    }

    private static string GetString(int set, int index)
    {
        if (set == 0)
        {
            switch(index)
            {
                case 0:
                    return "healthy";

                case 1:
                    return "scratched";

                case 2:
                    return "hurt";

                case 3:
                    return "wounded";

                case 4:
                    return "critical";
            }
        }
        else
        {
            switch (index)
            {
                case 0:
                    return "fine";

                case 1:
                    return "unfocused";

                case 2:
                    return "weak";

                case 3:
                    return "compromised";

                case 4:
                    return "succumbing";
            }
        }
        return "";
    }

    private static int GetLevel(int[] ints, int strength)
    {
        int level = -1;
        for (int i = 0; i < 3; i++)
        {
            if (ints[i]!=-1 && strength >= ints[i])
            {
                level = i;
            }
        }
        return level;
    }
}