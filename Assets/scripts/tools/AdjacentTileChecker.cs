﻿using UnityEngine;
using System.Collections;

public class AdjacentTileChecker 
{

    static private int[] valueMap = { 1, 16, 2, 32, 4, 64, 8, 128 };

    static public int AdjacencyCheck(int x, int y, TileDef tile, Tile[][] tiles)
    {
        int value = 0;
        for (int i = 0; i < 8; i++)
        {
            Vector2Int p = GeometryTools.GetPosition(x, y, i);

            if (tiles[p.x][p.y]!=null && 
                (tiles[p.x][p.y].getDefinition().Equals(tile)||
                tiles[p.x][p.y].getDefinition().getSmartTileGroup()==tile.getSmartTileGroup()))
            {
                value += valueMap[i];
            }
        }
   
        return value;
    }

}
