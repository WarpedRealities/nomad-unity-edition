﻿using UnityEngine;
using System.Collections;

public class ZoneRestoreTool
{
    public static void ZoneChecker(Zone zone)
    {
        if (zone.GetContents() != null)
        {
            for (int i = 0; i < zone.GetContents().GetZoneParameters().GetWidth(); i++)
            {
                for (int j = 0; j < zone.GetContents().GetZoneParameters().GetHeight(); j++)
                {
                    if (zone.GetContents().getTiles()[i][j] != null)
                    {
                        zone.GetContents().getTiles()[i][j].Restore(zone.GetContents().getTileSet());
                    }
                }
            }
        }
    }
}
