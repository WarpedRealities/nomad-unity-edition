﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MoonSharp.Interpreter;

public class RunnableScript
{
    private Script script;
    private DynValue mainFunction;
    private object[] arguments;

    public RunnableScript(string scriptName)
    {
        this.script = new Script();
        String fileContents = FileTools.GetFileRaw("scripts/" + scriptName + ".lua");

        this.script.DoString(fileContents);
        mainFunction = this.script.Globals.Get("main");
        arguments = new object[3];
    }

    public bool RunScript(FlagData flagData)
    {
        arguments[0] = flagData;
        DynValue rValue = this.script.Call(mainFunction, arguments);
        return rValue.Boolean;
    }

    public bool RunScript(FlagData flagData, ToolsLua tools)
    {
        arguments[0] = flagData;
        arguments[1] = tools;
        DynValue rValue = this.script.Call(mainFunction, arguments);
        return rValue.Boolean;
    }
    public bool RunScript(FlagData flagData, ViewLua view)
    {
        arguments[0] = flagData;
        arguments[1] = view;
        DynValue rValue = this.script.Call(mainFunction, arguments);
        return rValue.Boolean;
    }
    public bool RunScript(FlagData flagData, ViewLua view, LuaActor actor)
    {
        arguments[0] = flagData;
        arguments[1] = view;
        arguments[2] = actor;
        DynValue rValue = this.script.Call(mainFunction, arguments);
        return rValue.Boolean;
    }
}

public class NpcScriptRunnerTool
{
    private static NpcScriptRunnerTool instance;

    public static NpcScriptRunnerTool GetInstance()
    {
        if (instance == null)
        {
            instance = new NpcScriptRunnerTool();
        }
        return instance;
    }

    private Dictionary<string, RunnableScript> scripts;

    public NpcScriptRunnerTool()
    {
        scripts = new Dictionary<string, RunnableScript>();
    }

    public RunnableScript GetScript(string scriptName)
    {
        if (!scripts.ContainsKey(scriptName))
        {
            scripts.Add(scriptName, new RunnableScript(scriptName));
        }
        return scripts[scriptName];
    }

    internal bool RunSpawn(NPC nPC, string scriptName)
    {
        FlagField globalFlags = GlobalGameState.GetInstance().GetPlayer().GetFlagField();
        FlagField factionFlags = nPC.GetFaction().GetFlagField();
        FlagField npcFlags = nPC.GetFlagField();
        FlagData flagData = new FlagData(globalFlags, factionFlags, npcFlags);
        RunnableScript script = GetScript(scriptName);
        return script.RunScript(flagData);
    }

    internal void RunDeath(NPC nPC, string scriptName)
    {
        FlagField globalFlags = GlobalGameState.GetInstance().GetPlayer().GetFlagField();
        FlagField factionFlags = nPC.GetFaction().GetFlagField();
        FlagField npcFlags = nPC.GetFlagField();
        FlagData flagData = new FlagData(globalFlags, factionFlags, npcFlags);
        ToolsLua toolsLua = new ToolsLua();
        RunnableScript script = GetScript(scriptName);
        script.RunScript(flagData, toolsLua);
    }
}
