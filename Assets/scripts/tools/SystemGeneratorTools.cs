﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;

public class SystemGeneratorTools 
{


    public SystemGeneratorTools()
    {

    }

    public StarSystemContents generateStarSystem(string filename, UIDGenerator uIDGenerator)
    {
        StarSystemContents systemContents = new StarSystemContents();
        List<Entity> entities = new List<Entity>();
        XmlDocument document = FileTools.GetXmlDocument("systems/"+ filename);
        ZoneContentsGenerator zoneContentsGenerator = new ZoneContentsGenerator();
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        DistressData distressData = null;
        float[] colour = new float[4];
        float[] highlight = new float[4];
        SpaceGrid spaceGrid = new SpaceGrid();
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("highlight".Equals(element.Name))
                {
                    highlight[0] = float.Parse(element.GetAttribute("r")) / 256;
                    highlight[1] = float.Parse(element.GetAttribute("g")) / 256;
                    highlight[2] = float.Parse(element.GetAttribute("b")) / 256;
                    highlight[3] = float.Parse(element.GetAttribute("a")) / 256;
                }
                if ("color".Equals(element.Name))
                {
                    colour[0] = float.Parse(element.GetAttribute("r")) / 256;
                    colour[1] = float.Parse(element.GetAttribute("g")) / 256;
                    colour[2] = float.Parse(element.GetAttribute("b")) / 256;
                    colour[3] = float.Parse(element.GetAttribute("a")) / 256;
                }
                if ("distress".Equals(element.Name))
                {
                    distressData = BuildDistress(element);
                }
                if ("star".Equals(element.Name))
                {
                    Vector2Int position = new Vector2Int(Int32.Parse(element.GetAttribute("x")), 
                        Int32.Parse(element.GetAttribute("y"))); ;
                    float size= float.Parse(element.GetAttribute("size"));
                    float intensity= float.Parse(element.GetAttribute("intensity"));
                    string sprite = element.GetAttribute("sprite");
                    entities.Add(new Star_Entity(position,size,intensity,sprite));
                }
                if ("planet".Equals(element.Name))
                {
                    Vector2Int position = new Vector2Int(Int32.Parse(element.GetAttribute("x")),
                      Int32.Parse(element.GetAttribute("y"))); ;
                    string fname = element.GetAttribute("name");
                    entities.Add(new Planet(position, fname));
                }
                if ("station".Equals(element.Name))
                {
                    Vector2Int position = new Vector2Int(Int32.Parse(element.GetAttribute("x")),
                      Int32.Parse(element.GetAttribute("y"))); ;
                    string fname = element.GetAttribute("name");
                    Debug.Log("fname " + fname);
                    entities.Add(new Station(position, fname));
                }
                if ("spaceship".Equals(element.Name))
                {
                    Spaceship spaceship = new SpaceshipGenerator().GenerateShip(element.GetAttribute("file"), uIDGenerator);
                    spaceship.setPosition(new Vector2Int(int.Parse(element.GetAttribute("x")), int.Parse(element.GetAttribute("y"))));
                    spaceship.SetFacing(DiceRoller.GetInstance().RollDice(8));
                    if (element.GetAttribute("controller").Length > 2)
                    {
                        spaceship.SetController(BuildController(element.GetAttribute("controller")));
                        ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(spaceship, false);
                        shipAnalysisTool.ApplyStats();
                    }
                    entities.Add(spaceship);
                }
                if ("creature".Equals(element.Name))
                {
                    Creature creature = new CreatureGeneratorTool().GenerateCreature(element.GetAttribute("file"));
                    creature.setPosition(new Vector2Int(int.Parse(element.GetAttribute("x")), int.Parse(element.GetAttribute("y"))));
                    creature.SetFacing(DiceRoller.GetInstance().RollDice(8));
                    if (element.GetAttribute("controller").Length > 2)
                    {
                        creature.SetController(BuildController(element.GetAttribute("controller")));
                    }
                    entities.Add(creature);
                }
                if ("ring".Equals(element.Name))
                {
                    ApplyRing(element, spaceGrid);
                }
                if ("cloud".Equals(element.Name))
                {
                    ApplyCloud(element, spaceGrid);
                }
                if ("randomSpawn".Equals(element.Name)) {
                    RandomSpawn(element, spaceGrid, entities, uIDGenerator);
                }
            }
        }
        systemContents.SetSpaceGrid(spaceGrid);
        systemContents.SetColours(colour, highlight);
        systemContents.setEntities(entities);
        if (distressData != null)
        {
            systemContents.SetDistressData(distressData);
        }
        return systemContents;
    }

    private DistressData BuildDistress(XmlElement rootXML)
    {
        List<DistressCall> distressCalls = new List<DistressCall>();
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("distressCall".Equals(element.Name))
                {
                    int chance = int.Parse(element.GetAttribute("chance"));
                    string condition = element.GetAttribute("condition");
                    string script = element.GetAttribute("script");
                    distressCalls.Add(new DistressCall(chance, condition, script));
                }
            }
        }
        return new DistressData(distressCalls);
    }
    public static Base_Captain BuildController(string filename)
    {
        XmlDocument document = FileTools.GetXmlDocument("controllers/" + filename);
        XmlElement rootXML = (XmlElement)document.FirstChild.NextSibling;
        string entry="", solar="", win="", lose="", contact="", combat="";
        int experience = int.Parse(rootXML.GetAttribute("experienceValue"));
        Faction faction = GlobalGameState.GetInstance().GetUniverse().getFactionLibrary().GetFaction(rootXML.GetAttribute("faction"));
        NPC_Skills skills = null;
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("skills".Equals(element.Name))
                {
                    int piloting = int.Parse(element.GetAttribute("pilot"));
                    int gunnery = int.Parse(element.GetAttribute("gunnery"));
                    int tech = int.Parse(element.GetAttribute("tech"));
                    int science = int.Parse(element.GetAttribute("science"));
                    skills = new NPC_Skills(piloting, gunnery, science, tech);
                }
                if ("entryScript".Equals(element.Name))
                {
                    entry = element.GetAttribute("value");
                }
                if ("solarScript".Equals(element.Name))
                {
                    solar = element.GetAttribute("value");
                }
                if ("winScript".Equals(element.Name))
                {
                    win = element.GetAttribute("value");
                }
                if ("lossScript".Equals(element.Name))
                {
                    lose = element.GetAttribute("value");
                }
                if ("contactScript".Equals(element.Name))
                {
                    contact = element.GetAttribute("value");
                }
                if ("combatScript".Equals(element.Name))
                {
                    combat = element.GetAttribute("value");
                }
            }
        }
        ActiveData activeData = new ActiveData(experience, faction, solar, combat, entry, contact, win, lose);
        return new NPC_Captain(activeData, skills);
    }

    private void RandomSpawn(XmlElement rootXML, SpaceGrid spaceGrid, List<Entity> entities, UIDGenerator uIDGenerator)
    {
        int min = int.Parse(rootXML.GetAttribute("min"));
        int max = int.Parse(rootXML.GetAttribute("max"));
        int x = int.Parse(rootXML.GetAttribute("x"));
        int y = int.Parse(rootXML.GetAttribute("y"));
        float d = min+ (DiceRoller.GetInstance().RollOdds() * (max - min));
        float r = DiceRoller.GetInstance().RollOdds() * (float)(360);
        Vector3 v = new Vector2(d, 0);
        Vector3 n = Quaternion.AngleAxis(r, Vector3.forward) * v;
        n.x += x;
        n.y += y;
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("station".Equals(element.Name))
                {
                    Vector2Int position = new Vector2Int((int)n.x,(int)n.y);
                    string fname = element.GetAttribute("name");
                    entities.Add(new Station(position, fname));
                }
                if ("signature".Equals(element.Name))
                {
                    Vector2Int position = new Vector2Int((int)n.x, (int)n.y);
                    entities.Add(BuildSignature(element, position));
                }
                if ("creature".Equals(element.Name))
                {
                    Creature creature = new CreatureGeneratorTool().GenerateCreature(element.GetAttribute("file"));
                    Vector2Int position = new Vector2Int((int)n.x, (int)n.y);
                    creature.setPosition(position);
                    creature.SetFacing(DiceRoller.GetInstance().RollDice(8));
                    if (element.GetAttribute("controller").Length > 2)
                    {
                        creature.SetController(BuildController(element.GetAttribute("controller")));
                    }
                    entities.Add(creature);
                }
                if ("spaceship".Equals(element.Name))
                {
                    Spaceship spaceship = new SpaceshipGenerator().GenerateShip(element.GetAttribute("file"), uIDGenerator);
                    Vector2Int position = new Vector2Int((int)n.x, (int)n.y);
                    spaceship.setPosition(position);
                    spaceship.SetFacing(DiceRoller.GetInstance().RollDice(8));
                    if (element.GetAttribute("controller").Length > 2)
                    {
                        spaceship.SetController(BuildController(element.GetAttribute("controller")));
                        ShipAnalysisTool shipAnalysisTool = new ShipAnalysisTool(spaceship, false);
                        shipAnalysisTool.ApplyStats();
                    }
                    entities.Add(spaceship);
                }
            }
        }
    }

    private SignatureEntity BuildSignature(XmlElement rootXML, Vector2Int position)
    {
        int detection = int.Parse(rootXML.GetAttribute("detection"));
        SignatureEffect signatureEffect = null;
        for (int i = 0; i < rootXML.ChildNodes.Count; i++)
        {
            if (rootXML.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement element = (XmlElement)rootXML.ChildNodes[i];
                if ("cargo".Equals(element.Name))
                {
                    signatureEffect = BuildCargo(element);
                }
                if ("monsters".Equals(element.Name))
                {
                    signatureEffect = BuildMonsters(element);
                }
            }
        }
        return new SignatureEntity(detection,position,signatureEffect);
    }

    private SignatureEffect BuildMonsters(XmlElement root)
    {
        string description=null;
        int index = 0;
        string[] monsterFiles = new string[int.Parse(root.GetAttribute("count"))];
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            if (root.ChildNodes[i].NodeType.Equals(XmlNodeType.Element))
            {
                XmlElement element = (XmlElement)root.ChildNodes[i];
                if ("description".Equals(element.Name))
                {
                    description = element.InnerText;
                }
                if ("monster".Equals(element.Name))
                {
                    monsterFiles[index] = element.GetAttribute("file");
                    index++;
                }
            }
        }
        return new MonsterEffect(description, monsterFiles);

    }

    private SignatureEffect BuildCargo(XmlElement element)
    {
        LootTable lootTable = LootTableBuilder.Build(element);
        LootTableTool lootTableTool = new LootTableTool(lootTable);
        ItemList itemList = new ItemList(lootTableTool.GetLoot());
        String text = element.InnerText;
        return new CargoEffect(itemList,text);
    }

    private void ApplyCloud(XmlElement element, SpaceGrid spaceGrid)
    {
        Vector2Int p = new Vector2Int(int.Parse(element.GetAttribute("x")), int.Parse(element.GetAttribute("y")));
        throw new NotImplementedException();
    }

    private void ApplyRing(XmlElement element, SpaceGrid spaceGrid)
    {
        Vector2Int p = new Vector2Int(int.Parse(element.GetAttribute("x"))+128, int.Parse(element.GetAttribute("y"))+128);
        int inner = int.Parse(element.GetAttribute("inner"));
        int outer = int.Parse(element.GetAttribute("outer"));
        float density = float.Parse(element.GetAttribute("density"));
        bool additive = "true".Equals(element.GetAttribute("additive"));
        bool[][] grid = GridTools.BuildGrid(outer*2, outer*2);
        grid = GridTools.Circle(grid, outer, false);
        grid = GridTools.Circle(grid, inner, true);
        int minHaz = 0, maxHaz = 0, minObf = 0, maxObf = 0;
        for (int i = 0; i < element.ChildNodes.Count; i++)
        {
            if (element.ChildNodes[i].NodeType == XmlNodeType.Element)
            {
                XmlElement e = (XmlElement)element.ChildNodes[i];
                if ("hazard".Equals(e.Name))
                {
                    int.TryParse(e.GetAttribute("min"),out minHaz);
                    int.TryParse(e.GetAttribute("max"), out maxHaz);
                }
                if ("obfuscation".Equals(e.Name))
                {
                    int.TryParse(e.GetAttribute("min"), out minObf);
                    int.TryParse(e.GetAttribute("max"), out maxObf);
                }
                if ("apply".Equals(e.Name))
                {
                    ApplyTiles(spaceGrid, grid, p, density, minHaz, maxHaz, minObf, maxObf, additive);
                }
            }
        }

    }

    private void ApplyTiles(SpaceGrid spaceGrid, bool[][] grid, Vector2Int p, float density, int minHaz, int maxHaz, int minObf, int maxObf, bool additive)
    {
        int x = p.x - (grid.Length / 2);
        int y = p.y - (grid[0].Length / 2);
        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid[0].Length; j++)
            {
                if (grid[i][j])
                {
                    if (density>=1 || DiceRoller.GetInstance().RollOdds() < density)
                    {
                        int hazard = maxHaz == 0 ? 0 : minHaz + DiceRoller.GetInstance().RollDice(maxHaz - minHaz);
                        int obfuscation = maxObf == 0 ? 0 : minObf + DiceRoller.GetInstance().RollDice(maxObf - minObf);
                        if (hazard>0|| obfuscation > 0)
                        {
                            spaceGrid.ModTile(x + i, y + j, hazard, obfuscation, additive);
                        }
                    }
                }
            }
        }
    }
}
