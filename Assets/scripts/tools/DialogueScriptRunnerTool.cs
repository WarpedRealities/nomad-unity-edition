﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;
using MoonSharp.Interpreter;

public class DialogueScriptRunnerTool
{
    private static DialogueScriptRunnerTool _instance;
    public static DialogueScriptRunnerTool GetInstance()
    {
        if (_instance == null)
        {
            _instance = new DialogueScriptRunnerTool();
        }
        return _instance;
    }

    internal int RunScript(string scriptName, DialogueActor actor, Player player)
    {
        Script script;
        DynValue mainFunction;
        object[] arguments=new object[3];
        script = new Script();
        String fileContents = FileTools.GetFileRaw("conversations/" + scriptName + ".lua");

        script.DoString(fileContents);
        mainFunction = script.Globals.Get("main");
        arguments[0] = new FlagDataAdv(player.GetFlagField(),GlobalGameState.GetInstance().GetUniverse().getFactionLibrary());
        arguments[1] = new PlayerLua(player);
        DynValue rValue = script.Call(mainFunction, arguments);
        return (int)rValue.Number;
    }
}
